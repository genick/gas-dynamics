\chapter*{Preface}
\textsc{
"In the beginning, the Potto project was without form, and void; and
emptiness was upon the face of the bits and files.
And the Fingers of the Author moved upon the face of the keyboard.
And the Author said, Let there be words, and there were
words."}\footnote{To the power and glory of the mighty God.
This book is only to explain his power.}

This book, Fundamentals of Compressible  Flow, describes the fundamentals
of compressible flow phenomena for engineers.
This book is designed to replace the book(s) or instructor
notes for the compressible flow in undergraduate classes
(mostly) for engineer students.
It is hoped that the book could be used as reference book for
people who have at least have some knowledge of
the basics of fundamental fluid mechanics, and basic science
such as calculus, physics etc.
It is hoped that the computer program enclosed in the book
will take life of its own and developed into open content source
project. 

The structure of this book is such that many of the chapters could
be usable independently.
For example, if you need information about, say, Fanno flow,
you can read just chapter \ref{chap:fanno}.
I hope this makes the book easier to use as a reference manual.
However, this manuscript is first and foremost a
text book, and secondly a reference manual only as a lucky coincidence.

I have tried to describe why the theories are the way they are, rather than
just listing ``seven easy steps'' for each task.
This means a lot of information is presented which is not necessary for
everyone.
These explanations have been marked as such and can be skipped.\footnote{%
At present, the book is not well organized.
You have to remember that this book is work in progress.}
Reading everything will, naturally, increase your understanding of the
fundamentals of compressible fluid flow.

This work was done on a volunteer basis:
I believe professionals working in the mechanical
or chemical engineering fields will benefit from this information.
This book is written and maintained on a volunteer base.
Like all volunteer work, there is a limit to how much effort
I have been able to put into book and its' organization.
Moreover, due to my poor English and time limitation,
the explanations are not as good as if I had a few years to perfect them.
I have left some issues which have unsatisfactory explanations
in the book, however marked with a Mata mark.
I hope to improve or to add these areas in the near future.
Furthermore, I hope that others will be part of this project and
will contribute to this book (even small contribution such example
not clear explanations etc. Even photos of experiments are needed.).

I have tried to make this text of the highest quality possible
and am interested
in your comments and ideas on how to make it better.
Bad language, errors, ideas for new areas to cover, rewritten sections,
more fundamental material, more mathematics (or less mathematics);
I am interested in all.
If you want to be involved in the editing, graphic design, or proofreading, 
please drop me a line.
You may contact me via eMail at \emailAddress.{}\par 

Several people have helped me with this book, directly or indirectly.
I would like to especially thank to my adviser Dr.~R.~E.~G.~Eckert
whose work was the inspiration for this book.
I would like to thank Prof.~N.~Brauner for her advice and ideas.


The symbol META need to add typographical conventions for blurb here.
This is mostly for the author's purposes and also for your amusement.
There are also notes in the margin, but those are for the author's purposes
only, please ignore them.
They will be removed gradually as the version number advances.

I encourage anyone with a penchant for writing, editing, graphic ability,
\LaTeX{} knowledge, and a desire to provide free content 
text book and to improve them 
to join me in this project.
If you have Internet e-mail access, you can contact me at
\emailAddress.
\par



