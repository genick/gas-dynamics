%begin{latexonly}
\DeclareFixedFont{\xviisf}{U}{eus}{m}{n}{27}
\DeclareFixedFont{\xsf}{OT1}{cmss}{m}{n}{10}
\DeclareFixedFont{\viiisf}{OT1}{cmss}{m}{n}{8}
\DeclareFixedFont{\yswabxxx}{U}{yswab}{m}{n}{24.88}
\DeclareFixedFont{\eufxvii}{U}{euf}{m}{n}{17}
\DeclareFixedFont{\eufxxx}{U}{euf}{m}{n}{24.88}

\thispagestyle{empty}

% to insert the following message into the postcript file and pdffiles.

\special{! [ /Author (Genick Bar-Meir)
               /Title (Fundamentals of compressible Flow )
               /Keywords (compressible flow, Fanno flow,
                        rayleigh flow )}


\pretitle{\begin{center}\Huge}
\posttitle{ \\ \par\end{center}\vskip 0.5em 
\vspace{0.5in}
}
\preauthor{\begin{center}
           \Large \lineskip 0.5em%
           \begin{tabular}[t]{c}}
\postauthor{\end{tabular}\par\end{center}}
%\predate{\begin{center}\large}
%\postdate{\par\end{center}}
\predate{\begin{center}\LARGE}
\postdate{\par\end{center}}

\title{Fundamentals of Compressible Fluid Mechanics}
\author{%
%\thaddress{
	Genick Bar--Meir, Ph.~D.\\[5pt]
     1107 $16^{\underline{th}}$ Ave S.~E.\\
     Minneapolis, MN 55414-2411 \\
	email:barmeir@gmail.com \\[4cm]
    Copyright \copyright{} 2006, 2005, and 2004 by Genick Bar-Meir  \\
	See the file copying.fdl or copyright.tex for copying conditions.
    %}
}
\date{\Large Version (\ver{})}

\begin{titlingpage}
%\setlength{\droptitle}{30pt} % lower the title
\maketitle
% \begin{abstract}...\end{abstract}
\end{titlingpage}

%end{latexonly}

\begin{htmlonly}
	\begin{center}
	\huge
	 Fundamentals of compressible Flow Mechanics  \\\\ 
	\Large
	Genick Bar-Meir, Ph.D \\[1.cm]
	Version 0.4.3.1rc2 \today \\[1.cm] 
	email:barmeir@gmail.com \\
	\large
    Copyright \copyright{} 2006, 2005, and 2004 by Genick Bar-Meir  \\
	See the file copying.fdl or copyright.tex for copying
conditions.\\[1.5cm]
\end{center}
\end{htmlonly}

\begin{rawhtml} 
<center>
<font color="red">
<A NAME="pdfVersion" HREF="../text.pdf"> To download the pdf
version
</a>
</font>
</center>
\end{rawhtml} 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{htmlonly}
\newcommand{\myalign}{center}
\newcommand{\mylist}{UL}
\newcommand{\myitem}[2]{\HTMLcode[disc]{LI}{\simpletest{#1}{#2}}}
\newcommand{\simpletest}[2]{%
 \HTMLcode{#1}{ a simple test of ``#2'',} using
\HTMLcode{CODE}{<#1>} .}
\newcommand{\tableopts}{10,border=5}

\newcommand{\tablelist}[4][left]{\HTMLcode[#1]{DIV}{
\HTMLcode[\tableopts]{TABLE}{
\HTMLcode[bottom]{CAPTION}{
#3
}\HTMLcode{TR}{\HTMLcode{TD}{
\HTMLcode{#2}{
#4
}}}
}}\HTMLcode[all]{BR}}

\tablelist[\myalign]{\mylist}{%
\textbf{A listing of the different text styles available in HTML
3.2}}{%
\myitem{B}{bold-face}
\myitem{I}{italics}
\myitem{TT}{teletype-text}
\myitem{U}{underlining}
\HTMLcode[circle]{LI}{\simpletest{STRIKE}{strikeout}}
\myitem{EM}{emphasis style}
\myitem{STRONG}{strong style}
\myitem{CODE}{code style}
\myitem{CITE}{citation style}
\myitem{DFN}{definition style}
\HTMLcode[square]{LI}{\simpletest{SAMP}{sample style}}
\HTMLcode[square]{LI}{\simpletest{KBD}{keyboard style}}
\myitem{VAR}{variable style}}
\end{htmlonly}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%begin{latexonly}
\parindent 0pt 
{
$\;$
\vspace{3.0 in}
%end{latexonly}

\Large
`We are like dwarfs sitting on the shoulders of giants''\\[1cm]  
	
\qquad \qquad {from The Metalogicon by John in 1159}
%\setcounter{page}{1}
%begin{latexonly}
}
%end{latexonly}

