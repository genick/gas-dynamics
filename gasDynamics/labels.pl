# LaTeX2HTML 2002-2-1 (1.71)
# Associate labels original text with physical files.


$key = q/shock:fig:shockDrag/;
$external_labels{$key} = "$URL/" . q|node105.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:delta-thetaA/;
$external_labels{$key} = "$URL/" . q|node204.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:MachDef/;
$external_labels{$key} = "$URL/" . q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:fig:comperTandP/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:govD/;
$external_labels{$key} = "$URL/" . q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:mach/;
$external_labels{$key} = "$URL/" . q|node54.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:yetMtoUfunD/;
$external_labels{$key} = "$URL/" . q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:PdP2/;
$external_labels{$key} = "$URL/" . q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:APtotal/;
$external_labels{$key} = "$URL/" . q|node85.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:fig:reversed/;
$external_labels{$key} = "$URL/" . q|node226.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:fig:basic/;
$external_labels{$key} = "$URL/" . q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:piston/;
$external_labels{$key} = "$URL/" . q|node186.html|; 
$noresave{$key} = "$nosave";

$key = q/variableShock:ex:superSonicTunnel/;
$external_labels{$key} = "$URL/" . q|node124.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:openValvePipeA/;
$external_labels{$key} = "$URL/" . q|node120.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:MstarReduced/;
$external_labels{$key} = "$URL/" . q|node77.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:X/;
$external_labels{$key} = "$URL/" . q|node73.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:tc/;
$external_labels{$key} = "$URL/" . q|node171.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:PvPT/;
$external_labels{$key} = "$URL/" . q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/fluid:eq:dEta/;
$external_labels{$key} = "$URL/" . q|node65.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:cv/;
$external_labels{$key} = "$URL/" . q|node94.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:tab:basic/;
$external_labels{$key} = "$URL/" . q|node134.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:fig:nozzle/;
$external_labels{$key} = "$URL/" . q|node74.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Mz_1/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:sec:PartiallyCloseValve/;
$external_labels{$key} = "$URL/" . q|node114.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:M2t/;
$external_labels{$key} = "$URL/" . q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:densityPbar/;
$external_labels{$key} = "$URL/" . q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:solSemiRigid/;
$external_labels{$key} = "$URL/" . q|node183.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:mass/;
$external_labels{$key} = "$URL/" . q|node170.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MypMax/;
$external_labels{$key} = "$URL/" . q|node112.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:solutionXi1/;
$external_labels{$key} = "$URL/" . q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:impulsDefStar/;
$external_labels{$key} = "$URL/" . q|node90.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:combine1/;
$external_labels{$key} = "$URL/" . q|node79.html|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:massMon1/;
$external_labels{$key} = "$URL/" . q|node126.html|; 
$noresave{$key} = "$nosave";

$key = q/obalique:fig:detachWedge/;
$external_labels{$key} = "$URL/" . q|node213.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:densityMsx/;
$external_labels{$key} = "$URL/" . q|node111.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:mem/;
$external_labels{$key} = "$URL/" . q|node68.html|; 
$noresave{$key} = "$nosave";

$key = q/thermo:eq:maxwell/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:openValvePipeC/;
$external_labels{$key} = "$URL/" . q|node120.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dPzero-fld/;
$external_labels{$key} = "$URL/" . q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:solRigitFannoFast/;
$external_labels{$key} = "$URL/" . q|node180.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:paritalyOpenUSa/;
$external_labels{$key} = "$URL/" . q|node113.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:x3/;
$external_labels{$key} = "$URL/" . q|node200.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:M-Tzcritical/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:solD/;
$external_labels{$key} = "$URL/" . q|node202.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:rhoToP/;
$external_labels{$key} = "$URL/" . q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:combinedState/;
$external_labels{$key} = "$URL/" . q|node73.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoPzeroRatioStarMax/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:workingFLDAppSol/;
$external_labels{$key} = "$URL/" . q|node131.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:shock/;
$external_labels{$key} = "$URL/" . q|node94.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:acceleratingPiston/;
$external_labels{$key} = "$URL/" . q|node115.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:sound/;
$external_labels{$key} = "$URL/" . q|node69.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:Ubar/;
$external_labels{$key} = "$URL/" . q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:Mtheta/;
$external_labels{$key} = "$URL/" . q|node221.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:solRigitFannoFastb/;
$external_labels{$key} = "$URL/" . q|node181.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:P0R/;
$external_labels{$key} = "$URL/" . q|node204.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:massFlowRate1/;
$external_labels{$key} = "$URL/" . q|node83.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govsemiRigid/;
$external_labels{$key} = "$URL/" . q|node183.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:x/;
$external_labels{$key} = "$URL/" . q|node200.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:PoverT/;
$external_labels{$key} = "$URL/" . q|node172.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:improvedFLD/;
$external_labels{$key} = "$URL/" . q|node159.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:isothermalNozzlea/;
$external_labels{$key} = "$URL/" . q|node178.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:fig:chamber/;
$external_labels{$key} = "$URL/" . q|node170.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dRdU/;
$external_labels{$key} = "$URL/" . q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:fig:machLineAngle/;
$external_labels{$key} = "$URL/" . q|node219.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dA_w/;
$external_labels{$key} = "$URL/" . q|node139.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:fnA/;
$external_labels{$key} = "$URL/" . q|node83.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:ex:largeFLD-SS/;
$external_labels{$key} = "$URL/" . q|node157.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:smallMachMaxDeflction/;
$external_labels{$key} = "$URL/" . q|node202.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:fld2/;
$external_labels{$key} = "$URL/" . q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:conversionUx/;
$external_labels{$key} = "$URL/" . q|node106.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:gibbsCpds0/;
$external_labels{$key} = "$URL/" . q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:pressureMx/;
$external_labels{$key} = "$URL/" . q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:Cv/;
$external_labels{$key} = "$URL/" . q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:solution/;
$external_labels{$key} = "$URL/" . q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:tmaxtcAAC/;
$external_labels{$key} = "$URL/" . q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:2law/;
$external_labels{$key} = "$URL/" . q|node73.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:TtoPz1/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:cubic/;
$external_labels{$key} = "$URL/" . q|node200.html|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:bullet/;
$external_labels{$key} = "$URL/" . q|node54.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:maxMassFlowRateOpenValve/;
$external_labels{$key} = "$URL/" . q|node112.html|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:energy3/;
$external_labels{$key} = "$URL/" . q|node126.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:energyInt/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Fn-fun/;
$external_labels{$key} = "$URL/" . q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:fig:heatTube/;
$external_labels{$key} = "$URL/" . q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:example1/;
$external_labels{$key} = "$URL/" . q|node145.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:conversionMx/;
$external_labels{$key} = "$URL/" . q|node106.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:tab:suddenlyOpened/;
$external_labels{$key} = "$URL/" . q|node121.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:areaChangeMach/;
$external_labels{$key} = "$URL/" . q|node79.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:epiloguePotto/;
$external_labels{$key} = "$URL/" . q|node12.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:fnDef/;
$external_labels{$key} = "$URL/" . q|node83.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:maxMa/;
$external_labels{$key} = "$URL/" . q|node202.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:epsilonFA/;
$external_labels{$key} = "$URL/" . q|node202.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:TR/;
$external_labels{$key} = "$URL/" . q|node204.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:reducedPressure/;
$external_labels{$key} = "$URL/" . q|node205.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoAratio/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:mvU-P-Results/;
$external_labels{$key} = "$URL/" . q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:fig:cv/;
$external_labels{$key} = "$URL/" . q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:stangtionMach/;
$external_labels{$key} = "$URL/" . q|node98.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:subM1P2P1-D/;
$external_labels{$key} = "$URL/" . q|node160.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:stagnationTempDiff2a/;
$external_labels{$key} = "$URL/" . q|node108.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:isothermalNozzleInt/;
$external_labels{$key} = "$URL/" . q|node178.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:momentumD1/;
$external_labels{$key} = "$URL/" . q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:temp-M/;
$external_labels{$key} = "$URL/" . q|node80.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:conversionP0P/;
$external_labels{$key} = "$URL/" . q|node106.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:cvCombined/;
$external_labels{$key} = "$URL/" . q|node68.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:dTds/;
$external_labels{$key} = "$URL/" . q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:energy2/;
$external_labels{$key} = "$URL/" . q|node126.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:fmtheta1/;
$external_labels{$key} = "$URL/" . q|node201.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Uz/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MsxPistonD/;
$external_labels{$key} = "$URL/" . q|node111.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:stagnationTempDiff/;
$external_labels{$key} = "$URL/" . q|node106.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:ex:openValve/;
$external_labels{$key} = "$URL/" . q|node120.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoTPRatio/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno_eq:fDef/;
$external_labels{$key} = "$URL/" . q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:MaxTurning/;
$external_labels{$key} = "$URL/" . q|node223.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:impulsDef/;
$external_labels{$key} = "$URL/" . q|node90.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:e9d/;
$external_labels{$key} = "$URL/" . q|node185.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:tab:basic/;
$external_labels{$key} = "$URL/" . q|node121.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:isothermalNozzle/;
$external_labels{$key} = "$URL/" . q|node178.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:MtoUfunD/;
$external_labels{$key} = "$URL/" . q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:solRigitFannoFasta/;
$external_labels{$key} = "$URL/" . q|node180.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:math:m2express/;
$external_labels{$key} = "$URL/" . q|node132.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:m2/;
$external_labels{$key} = "$URL/" . q|node132.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MxlimitOpen/;
$external_labels{$key} = "$URL/" . q|node112.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:fig:cv/;
$external_labels{$key} = "$URL/" . q|node164.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:tab:unchoked/;
$external_labels{$key} = "$URL/" . q|node136.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isentropicPratio/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:tank/;
$external_labels{$key} = "$URL/" . q|node169.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:energy0/;
$external_labels{$key} = "$URL/" . q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:workingFLDApprox/;
$external_labels{$key} = "$URL/" . q|node131.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:tempSimple3/;
$external_labels{$key} = "$URL/" . q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:tab:basicIsentropic/;
$external_labels{$key} = "$URL/" . q|node86.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:momentum/;
$external_labels{$key} = "$URL/" . q|node129.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:gibbsCv/;
$external_labels{$key} = "$URL/" . q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:galileo/;
$external_labels{$key} = "$URL/" . q|node53.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:ImpulseRatio/;
$external_labels{$key} = "$URL/" . q|node90.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:PratioDimensional/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:R/;
$external_labels{$key} = "$URL/" . q|node200.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:e9e/;
$external_labels{$key} = "$URL/" . q|node185.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:D-theta-My-Mx_3/;
$external_labels{$key} = "$URL/" . q|node203.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:isoMix/;
$external_labels{$key} = "$URL/" . q|node73.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:fannoM1fM2/;
$external_labels{$key} = "$URL/" . q|node151.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:SepGenGovSemiRigit/;
$external_labels{$key} = "$URL/" . q|node184.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:continutyNonD/;
$external_labels{$key} = "$URL/" . q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:shock:entropy/;
$external_labels{$key} = "$URL/" . q|node94.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:maxSpeed/;
$external_labels{$key} = "$URL/" . q|node98.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:massAnother/;
$external_labels{$key} = "$URL/" . q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:minikoffSol/;
$external_labels{$key} = "$URL/" . q|node202.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:conversionT/;
$external_labels{$key} = "$URL/" . q|node106.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:solutionXi/;
$external_labels{$key} = "$URL/" . q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:variableArea/;
$external_labels{$key} = "$URL/" . q|node74.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:P5P1/;
$external_labels{$key} = "$URL/" . q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:ds-fld/;
$external_labels{$key} = "$URL/" . q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Fn-funRho0T/;
$external_labels{$key} = "$URL/" . q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:massFlowRate2/;
$external_labels{$key} = "$URL/" . q|node83.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:energyNI/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:TtoPz/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:massTotalPressure/;
$external_labels{$key} = "$URL/" . q|node85.html|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:eq:difuserEfficiency/;
$external_labels{$key} = "$URL/" . q|node124.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:tcSimple/;
$external_labels{$key} = "$URL/" . q|node176.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:dless-P/;
$external_labels{$key} = "$URL/" . q|node171.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:simpleEnergy/;
$external_labels{$key} = "$URL/" . q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:paritalyOpenUS/;
$external_labels{$key} = "$URL/" . q|node113.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:conversionUy/;
$external_labels{$key} = "$URL/" . q|node106.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:delta-theta/;
$external_labels{$key} = "$URL/" . q|node204.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:ex:openValveContinue/;
$external_labels{$key} = "$URL/" . q|node115.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:entrceL/;
$external_labels{$key} = "$URL/" . q|node133.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:localFar/;
$external_labels{$key} = "$URL/" . q|node208.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:mvUs2/;
$external_labels{$key} = "$URL/" . q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:momentum/;
$external_labels{$key} = "$URL/" . q|node139.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:generalPV/;
$external_labels{$key} = "$URL/" . q|node184.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:twoAngle/;
$external_labels{$key} = "$URL/" . q|node213.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:Cp/;
$external_labels{$key} = "$URL/" . q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/varibleShock:eq:mvUs/;
$external_labels{$key} = "$URL/" . q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:stagnationPressreRatio1/;
$external_labels{$key} = "$URL/" . q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:mixSound/;
$external_labels{$key} = "$URL/" . q|node73.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:TrhoSolutionDless/;
$external_labels{$key} = "$URL/" . q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:FLDretreat/;
$external_labels{$key} = "$URL/" . q|node157.html|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:eq:actualEnergy/;
$external_labels{$key} = "$URL/" . q|node123.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoRatioIdealgasISO/;
$external_labels{$key} = "$URL/" . q|node91.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:nDef/;
$external_labels{$key} = "$URL/" . q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MxpOpened/;
$external_labels{$key} = "$URL/" . q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:energyAfterInt1/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:bool/;
$external_labels{$key} = "$URL/" . q|node217.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:fmtheta1a/;
$external_labels{$key} = "$URL/" . q|node201.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:MtoUfun/;
$external_labels{$key} = "$URL/" . q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:momentum/;
$external_labels{$key} = "$URL/" . q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:theta/;
$external_labels{$key} = "$URL/" . q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:timeLinearHight2/;
$external_labels{$key} = "$URL/" . q|node69.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MsyMsx/;
$external_labels{$key} = "$URL/" . q|node106.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:PR/;
$external_labels{$key} = "$URL/" . q|node204.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:angle/;
$external_labels{$key} = "$URL/" . q|node220.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:stagnationTempDiff1/;
$external_labels{$key} = "$URL/" . q|node106.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:expession/;
$external_labels{$key} = "$URL/" . q|node132.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:stagnationPressreRatio/;
$external_labels{$key} = "$URL/" . q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:tbl:liquidSpeed/;
$external_labels{$key} = "$URL/" . q|node71.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:menikoff/;
$external_labels{$key} = "$URL/" . q|node202.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:dT0T/;
$external_labels{$key} = "$URL/" . q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:DzeroAlphaZero/;
$external_labels{$key} = "$URL/" . q|node203.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:MyforMxDelta/;
$external_labels{$key} = "$URL/" . q|node201.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MsxMyp/;
$external_labels{$key} = "$URL/" . q|node112.html|; 
$noresave{$key} = "$nosave";

$key = q/thermo:eq:dhPT/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:insontropic1/;
$external_labels{$key} = "$URL/" . q|node68.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:incompressibleP-Loss/;
$external_labels{$key} = "$URL/" . q|node132.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:Pbar/;
$external_labels{$key} = "$URL/" . q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:shockStrengthMx/;
$external_labels{$key} = "$URL/" . q|node103.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:weakTheta/;
$external_labels{$key} = "$URL/" . q|node207.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:ShockMove/;
$external_labels{$key} = "$URL/" . q|node106.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:dhNonID/;
$external_labels{$key} = "$URL/" . q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:fig:nozzleWithShock/;
$external_labels{$key} = "$URL/" . q|node122.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:M2n/;
$external_labels{$key} = "$URL/" . q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:math:m2express5/;
$external_labels{$key} = "$URL/" . q|node132.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:mass/;
$external_labels{$key} = "$URL/" . q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:dTdM/;
$external_labels{$key} = "$URL/" . q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:timeTank/;
$external_labels{$key} = "$URL/" . q|node176.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:energy/;
$external_labels{$key} = "$URL/" . q|node139.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:m2express/;
$external_labels{$key} = "$URL/" . q|node132.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArrea:eq:massFlowRatez/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:fig:compressiblityChart/;
$external_labels{$key} = "$URL/" . q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:wilson/;
$external_labels{$key} = "$URL/" . q|node71.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:bernoulli/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:tab:basicIsothermal/;
$external_labels{$key} = "$URL/" . q|node92.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:TratioExample/;
$external_labels{$key} = "$URL/" . q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:intro/;
$external_labels{$key} = "$URL/" . q|node37.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:crPbarn_1/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:entropyIdeal/;
$external_labels{$key} = "$URL/" . q|node94.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:math:m2express4/;
$external_labels{$key} = "$URL/" . q|node132.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:stangnationTempGov/;
$external_labels{$key} = "$URL/" . q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:mm/;
$external_labels{$key} = "$URL/" . q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:PoverSqrtT/;
$external_labels{$key} = "$URL/" . q|node172.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:AstarReduced/;
$external_labels{$key} = "$URL/" . q|node77.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:2law/;
$external_labels{$key} = "$URL/" . q|node129.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:item:fldSuper/;
$external_labels{$key} = "$URL/" . q|node157.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:PDiffReduced/;
$external_labels{$key} = "$URL/" . q|node77.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:chamberBlance/;
$external_labels{$key} = "$URL/" . q|node170.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:mvdU-rho/;
$external_labels{$key} = "$URL/" . q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:fld-M/;
$external_labels{$key} = "$URL/" . q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:logT0T/;
$external_labels{$key} = "$URL/" . q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:energyDless/;
$external_labels{$key} = "$URL/" . q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:gamma/;
$external_labels{$key} = "$URL/" . q|node73.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:deltaDelta/;
$external_labels{$key} = "$URL/" . q|node217.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:ATtotal/;
$external_labels{$key} = "$URL/" . q|node85.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:Tbar/;
$external_labels{$key} = "$URL/" . q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govDless6b/;
$external_labels{$key} = "$URL/" . q|node172.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:bulkModulus/;
$external_labels{$key} = "$URL/" . q|node71.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:en:subChoked/;
$external_labels{$key} = "$URL/" . q|node159.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:DrhoTOPFinal/;
$external_labels{$key} = "$URL/" . q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:mvdU-rho/;
$external_labels{$key} = "$URL/" . q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:ex:openValveAAA/;
$external_labels{$key} = "$URL/" . q|node115.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:solRigitFannoFastd/;
$external_labels{$key} = "$URL/" . q|node181.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:mixTwoPhases/;
$external_labels{$key} = "$URL/" . q|node73.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:Uratio/;
$external_labels{$key} = "$URL/" . q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:massNonD2/;
$external_labels{$key} = "$URL/" . q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:zeroMachWave/;
$external_labels{$key} = "$URL/" . q|node203.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:regions/;
$external_labels{$key} = "$URL/" . q|node196.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govRigitFannoFasta/;
$external_labels{$key} = "$URL/" . q|node180.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:epiloqueThisBook/;
$external_labels{$key} = "$URL/" . q|node13.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MypOpened/;
$external_labels{$key} = "$URL/" . q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:TPratio/;
$external_labels{$key} = "$URL/" . q|node171.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:d-Nu/;
$external_labels{$key} = "$URL/" . q|node220.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:mvRho/;
$external_labels{$key} = "$URL/" . q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:tmp1/;
$external_labels{$key} = "$URL/" . q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:energyAfterInt0/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:omega/;
$external_labels{$key} = "$URL/" . q|node224.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isentropicTratio/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:shockTube/;
$external_labels{$key} = "$URL/" . q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govDless1/;
$external_labels{$key} = "$URL/" . q|node171.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:f/;
$external_labels{$key} = "$URL/" . q|node139.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:massFlowRateStar/;
$external_labels{$key} = "$URL/" . q|node83.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dU-fld/;
$external_labels{$key} = "$URL/" . q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MypPbar/;
$external_labels{$key} = "$URL/" . q|node112.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:shock:state/;
$external_labels{$key} = "$URL/" . q|node94.html|; 
$noresave{$key} = "$nosave";

$key = q/varibleArea:fig:exmpleNozzle/;
$external_labels{$key} = "$URL/" . q|node90.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:cvMass1/;
$external_labels{$key} = "$URL/" . q|node68.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:massFlowRatezn_1/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:fig:MvA/;
$external_labels{$key} = "$URL/" . q|node81.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:D/;
$external_labels{$key} = "$URL/" . q|node200.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:mvDrho/;
$external_labels{$key} = "$URL/" . q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:massImp/;
$external_labels{$key} = "$URL/" . q|node90.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isodAdM/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:Pratioa/;
$external_labels{$key} = "$URL/" . q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:MassFlowRatenonIdeal1/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/eq::varibleArea:combine3/;
$external_labels{$key} = "$URL/" . q|node79.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:densityDless/;
$external_labels{$key} = "$URL/" . q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:areaChangeVelocity/;
$external_labels{$key} = "$URL/" . q|node79.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govDless6a/;
$external_labels{$key} = "$URL/" . q|node172.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govRigitFannoFast/;
$external_labels{$key} = "$URL/" . q|node181.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:fig:turnAngle/;
$external_labels{$key} = "$URL/" . q|node220.html|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:eq:coefficeintDischarge/;
$external_labels{$key} = "$URL/" . q|node123.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoPratio/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:TrhoSolutionDlessA/;
$external_labels{$key} = "$URL/" . q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:iserhoToP/;
$external_labels{$key} = "$URL/" . q|node69.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:NetForce/;
$external_labels{$key} = "$URL/" . q|node90.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:maxMassFlowRateOpenValveSimplified/;
$external_labels{$key} = "$URL/" . q|node112.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:LargeSymmetrialAngle/;
$external_labels{$key} = "$URL/" . q|node211.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:mass/;
$external_labels{$key} = "$URL/" . q|node79.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dRho-fld/;
$external_labels{$key} = "$URL/" . q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Fnzn_1/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:dless/;
$external_labels{$key} = "$URL/" . q|node171.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:govDxi/;
$external_labels{$key} = "$URL/" . q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:entropySolution/;
$external_labels{$key} = "$URL/" . q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:M1-FLD/;
$external_labels{$key} = "$URL/" . q|node161.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:ex:simpleExample/;
$external_labels{$key} = "$URL/" . q|node226.html|; 
$noresave{$key} = "$nosave";

$key = q/ref:fdl/;
$external_labels{$key} = "$URL/" . q|node2.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:fmMax/;
$external_labels{$key} = "$URL/" . q|node83.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:mDotFLD/;
$external_labels{$key} = "$URL/" . q|node151.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:T3T4R/;
$external_labels{$key} = "$URL/" . q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:appoximaetionR/;
$external_labels{$key} = "$URL/" . q|node73.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:T/;
$external_labels{$key} = "$URL/" . q|node200.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:mvU-rhoResults/;
$external_labels{$key} = "$URL/" . q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:Mach/;
$external_labels{$key} = "$URL/" . q|node221.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:dless-t/;
$external_labels{$key} = "$URL/" . q|node171.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:momentum/;
$external_labels{$key} = "$URL/" . q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:fullFld/;
$external_labels{$key} = "$URL/" . q|node174.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:thetaR/;
$external_labels{$key} = "$URL/" . q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dT-fld/;
$external_labels{$key} = "$URL/" . q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:massMon2/;
$external_labels{$key} = "$URL/" . q|node126.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:partialyOpenValvea/;
$external_labels{$key} = "$URL/" . q|node113.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:T0bar/;
$external_labels{$key} = "$URL/" . q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govIsoDless1/;
$external_labels{$key} = "$URL/" . q|node182.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:smallMaxDefA/;
$external_labels{$key} = "$URL/" . q|node202.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:timeLinearHight/;
$external_labels{$key} = "$URL/" . q|node69.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:wedgeEnd/;
$external_labels{$key} = "$URL/" . q|node206.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:energyDE/;
$external_labels{$key} = "$URL/" . q|node221.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:solRigitFannoFastc/;
$external_labels{$key} = "$URL/" . q|node181.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:sec:stationaryShockDrag/;
$external_labels{$key} = "$URL/" . q|node105.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:nozzleTankSolution/;
$external_labels{$key} = "$URL/" . q|node176.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:bernoulliStagnation/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:heatTranser/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:conversionP/;
$external_labels{$key} = "$URL/" . q|node106.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:energy/;
$external_labels{$key} = "$URL/" . q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:fannoM1M2fld/;
$external_labels{$key} = "$URL/" . q|node151.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:tbl:gasSpeed/;
$external_labels{$key} = "$URL/" . q|node71.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:longDLDwithShock/;
$external_labels{$key} = "$URL/" . q|node156.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:fld1/;
$external_labels{$key} = "$URL/" . q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:rayleigh/;
$external_labels{$key} = "$URL/" . q|node164.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoTzeroRatioStar/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:math:m2express6/;
$external_labels{$key} = "$URL/" . q|node132.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:fM/;
$external_labels{$key} = "$URL/" . q|node171.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:energy/;
$external_labels{$key} = "$URL/" . q|node79.html|; 
$noresave{$key} = "$nosave";

$key = q/veriableAreaeq:MassFlowRatezn_1/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:rhoNewDef/;
$external_labels{$key} = "$URL/" . q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:MBAdiabaticSimple/;
$external_labels{$key} = "$URL/" . q|node176.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:limitedTheta/;
$external_labels{$key} = "$URL/" . q|node204.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:idealGas1/;
$external_labels{$key} = "$URL/" . q|node69.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoRatioIdealgasStar/;
$external_labels{$key} = "$URL/" . q|node91.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:mvU2/;
$external_labels{$key} = "$URL/" . q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:universalIntropy/;
$external_labels{$key} = "$URL/" . q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:solRigitFannoFastbb/;
$external_labels{$key} = "$URL/" . q|node180.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:temperature2/;
$external_labels{$key} = "$URL/" . q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:dUrUtheta/;
$external_labels{$key} = "$URL/" . q|node221.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:M2_0/;
$external_labels{$key} = "$URL/" . q|node204.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:math:m2express3/;
$external_labels{$key} = "$URL/" . q|node132.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:workingFLD/;
$external_labels{$key} = "$URL/" . q|node131.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govDless2/;
$external_labels{$key} = "$URL/" . q|node171.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:cvMass2/;
$external_labels{$key} = "$URL/" . q|node68.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govDless6c/;
$external_labels{$key} = "$URL/" . q|node172.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:linear/;
$external_labels{$key} = "$URL/" . q|node184.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:massD/;
$external_labels{$key} = "$URL/" . q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:menikoffLarge/;
$external_labels{$key} = "$URL/" . q|node202.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:tab:Fn/;
$external_labels{$key} = "$URL/" . q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:suddenlyOpen/;
$external_labels{$key} = "$URL/" . q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:QdeltaZero1/;
$external_labels{$key} = "$URL/" . q|node203.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:solSemiRigida/;
$external_labels{$key} = "$URL/" . q|node184.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:ex:impulse/;
$external_labels{$key} = "$URL/" . q|node90.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:AstarTotalPressure/;
$external_labels{$key} = "$URL/" . q|node85.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:nonIdealRelation/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:energy/;
$external_labels{$key} = "$URL/" . q|node126.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dM2-fld/;
$external_labels{$key} = "$URL/" . q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:Ac/;
$external_labels{$key} = "$URL/" . q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:Q/;
$external_labels{$key} = "$URL/" . q|node200.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:fig:compareNozzle/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MyfMxopened/;
$external_labels{$key} = "$URL/" . q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:RH/;
$external_labels{$key} = "$URL/" . q|node103.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:conversionMy/;
$external_labels{$key} = "$URL/" . q|node106.html|; 
$noresave{$key} = "$nosave";

$key = q/varibleShock:eq:mvPbar/;
$external_labels{$key} = "$URL/" . q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MsxPiston/;
$external_labels{$key} = "$URL/" . q|node111.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:openedDeltaTr/;
$external_labels{$key} = "$URL/" . q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:fld-dM1/;
$external_labels{$key} = "$URL/" . q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:gibbs/;
$external_labels{$key} = "$URL/" . q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:movingShockDrag/;
$external_labels{$key} = "$URL/" . q|node107.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:stangnationPressureGov/;
$external_labels{$key} = "$URL/" . q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:energySolution/;
$external_labels{$key} = "$URL/" . q|node221.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:temperatureDlessModifiedB/;
$external_labels{$key} = "$URL/" . q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:Dzero/;
$external_labels{$key} = "$URL/" . q|node203.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:stationaryMx/;
$external_labels{$key} = "$URL/" . q|node108.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:kM2/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:verticalLine/;
$external_labels{$key} = "$URL/" . q|node220.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:OpenSuddenlyConvergence/;
$external_labels{$key} = "$URL/" . q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:e9f/;
$external_labels{$key} = "$URL/" . q|node185.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:a1/;
$external_labels{$key} = "$URL/" . q|node200.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:temperatureDlessModifiedA/;
$external_labels{$key} = "$URL/" . q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/varibleArea:fig:superSonicTunnel/;
$external_labels{$key} = "$URL/" . q|node124.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:fig:overMaxAngle/;
$external_labels{$key} = "$URL/" . q|node223.html|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:eq:difinitionRatio/;
$external_labels{$key} = "$URL/" . q|node123.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:P0ratio/;
$external_labels{$key} = "$URL/" . q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:densityMx/;
$external_labels{$key} = "$URL/" . q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:stationaryMy/;
$external_labels{$key} = "$URL/" . q|node108.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:veloctyRadious/;
$external_labels{$key} = "$URL/" . q|node221.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:Vtimea/;
$external_labels{$key} = "$URL/" . q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:momentumC/;
$external_labels{$key} = "$URL/" . q|node100.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:PVlinear/;
$external_labels{$key} = "$URL/" . q|node183.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:PeffectsM-asFun-frac__16310__4fL__16310____16311__D__16311__/;
$external_labels{$key} = "$URL/" . q|node154.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:TstarTzero/;
$external_labels{$key} = "$URL/" . q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:massBallanceInt/;
$external_labels{$key} = "$URL/" . q|node176.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:rhoRa/;
$external_labels{$key} = "$URL/" . q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:maxM1fld2/;
$external_labels{$key} = "$URL/" . q|node157.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isentropicDiff/;
$external_labels{$key} = "$URL/" . q|node80.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:temperatureDlessModified/;
$external_labels{$key} = "$URL/" . q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:PzeroReduced/;
$external_labels{$key} = "$URL/" . q|node77.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:gibbs/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:CV/;
$external_labels{$key} = "$URL/" . q|node129.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:sol/;
$external_labels{$key} = "$URL/" . q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:sondLiquid/;
$external_labels{$key} = "$URL/" . q|node71.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:rU-kMP/;
$external_labels{$key} = "$URL/" . q|node81.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:inlet/;
$external_labels{$key} = "$URL/" . q|node212.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:isentripicRetio/;
$external_labels{$key} = "$URL/" . q|node172.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:thetaAlpha/;
$external_labels{$key} = "$URL/" . q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:deltaFMonly/;
$external_labels{$key} = "$URL/" . q|node211.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:RzeroReduced2/;
$external_labels{$key} = "$URL/" . q|node77.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:tab:max/;
$external_labels{$key} = "$URL/" . q|node209.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:mnu1/;
$external_labels{$key} = "$URL/" . q|node219.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:fig:vacuum/;
$external_labels{$key} = "$URL/" . q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:workingEq/;
$external_labels{$key} = "$URL/" . q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:PstarPzero1/;
$external_labels{$key} = "$URL/" . q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:RzeroReduced/;
$external_labels{$key} = "$URL/" . q|node77.html|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:massMon5/;
$external_labels{$key} = "$URL/" . q|node126.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:a3/;
$external_labels{$key} = "$URL/" . q|node200.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govDless3/;
$external_labels{$key} = "$URL/" . q|node171.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Fn-funRho0P/;
$external_labels{$key} = "$URL/" . q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:fig:rigida/;
$external_labels{$key} = "$URL/" . q|node181.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:PratioOld/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:RgasSide/;
$external_labels{$key} = "$URL/" . q|node73.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:shockVariable/;
$external_labels{$key} = "$URL/" . q|node122.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:nonDimMass/;
$external_labels{$key} = "$URL/" . q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:starMtoM/;
$external_labels{$key} = "$URL/" . q|node100.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:mainT-P/;
$external_labels{$key} = "$URL/" . q|node101.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:horizontalLine/;
$external_labels{$key} = "$URL/" . q|node220.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:PzeroD/;
$external_labels{$key} = "$URL/" . q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:HD/;
$external_labels{$key} = "$URL/" . q|node139.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:compressedGasD/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:nuInfty/;
$external_labels{$key} = "$URL/" . q|node221.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:mvU2simplified/;
$external_labels{$key} = "$URL/" . q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:openedDeltaT/;
$external_labels{$key} = "$URL/" . q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:energyNonIdeal/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:CloseValveUx/;
$external_labels{$key} = "$URL/" . q|node114.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:toBeSolved/;
$external_labels{$key} = "$URL/" . q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:solRigitFannoFastReversed/;
$external_labels{$key} = "$URL/" . q|node180.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:stagnationMachNumber/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:develpSub/;
$external_labels{$key} = "$URL/" . q|node150.html|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:fannoFun/;
$external_labels{$key} = "$URL/" . q|node46.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:PstarPzero/;
$external_labels{$key} = "$URL/" . q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:entroyS/;
$external_labels{$key} = "$URL/" . q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:massR/;
$external_labels{$key} = "$URL/" . q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:mass/;
$external_labels{$key} = "$URL/" . q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:stagnationSoundSpeed/;
$external_labels{$key} = "$URL/" . q|node98.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:MomMassImp/;
$external_labels{$key} = "$URL/" . q|node90.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:suddenlyOpenst/;
$external_labels{$key} = "$URL/" . q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:P-M/;
$external_labels{$key} = "$URL/" . q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dP-dM/;
$external_labels{$key} = "$URL/" . q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:tangentU/;
$external_labels{$key} = "$URL/" . q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:temp-M1/;
$external_labels{$key} = "$URL/" . q|node80.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:energy/;
$external_labels{$key} = "$URL/" . q|node174.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:combineDimMassEnergy/;
$external_labels{$key} = "$URL/" . q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:deLavalNozzle/;
$external_labels{$key} = "$URL/" . q|node44.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:RdeltaZero/;
$external_labels{$key} = "$URL/" . q|node203.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:entropy/;
$external_labels{$key} = "$URL/" . q|node139.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:tab:suddenlyClosed/;
$external_labels{$key} = "$URL/" . q|node121.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:M1t/;
$external_labels{$key} = "$URL/" . q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:fliegner/;
$external_labels{$key} = "$URL/" . q|node83.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dP-fld/;
$external_labels{$key} = "$URL/" . q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:momentumD1aa/;
$external_labels{$key} = "$URL/" . q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:seperatedSemiRigid/;
$external_labels{$key} = "$URL/" . q|node183.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:main/;
$external_labels{$key} = "$URL/" . q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:momentumD1a/;
$external_labels{$key} = "$URL/" . q|node139.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:Prandtl-Meyer/;
$external_labels{$key} = "$URL/" . q|node218.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MsxPistonGeneral/;
$external_labels{$key} = "$URL/" . q|node111.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:Uthetac/;
$external_labels{$key} = "$URL/" . q|node221.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:tanMU/;
$external_labels{$key} = "$URL/" . q|node220.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:tab:basic/;
$external_labels{$key} = "$URL/" . q|node163.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:thermo2/;
$external_labels{$key} = "$URL/" . q|node79.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:gamma1/;
$external_labels{$key} = "$URL/" . q|node73.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoMomentum/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:cvMomentumEx/;
$external_labels{$key} = "$URL/" . q|node68.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Fn-funF/;
$external_labels{$key} = "$URL/" . q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:M-A/;
$external_labels{$key} = "$URL/" . q|node81.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoMass/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/varialbeArea:eq:bernnolliZP/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:M-Tzn_1/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:largeM1theta/;
$external_labels{$key} = "$URL/" . q|node205.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:cvCombined2/;
$external_labels{$key} = "$URL/" . q|node68.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:Tbar/;
$external_labels{$key} = "$URL/" . q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:relationshipP-rho/;
$external_labels{$key} = "$URL/" . q|node80.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:DrhoTOP/;
$external_labels{$key} = "$URL/" . q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govDless5/;
$external_labels{$key} = "$URL/" . q|node171.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:ditachment/;
$external_labels{$key} = "$URL/" . q|node210.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:solidExampl/;
$external_labels{$key} = "$URL/" . q|node72.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:combAllR/;
$external_labels{$key} = "$URL/" . q|node100.html|; 
$noresave{$key} = "$nosave";

$key = q/veriableAreaeq:MassFlowRatez/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:thetaMax/;
$external_labels{$key} = "$URL/" . q|node202.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:PfunFLDb/;
$external_labels{$key} = "$URL/" . q|node153.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:nuTheta/;
$external_labels{$key} = "$URL/" . q|node221.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Mz/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:fig:M1-fld/;
$external_labels{$key} = "$URL/" . q|node136.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:a2/;
$external_labels{$key} = "$URL/" . q|node200.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Uzn_1/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:energy/;
$external_labels{$key} = "$URL/" . q|node94.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:pressureMsx/;
$external_labels{$key} = "$URL/" . q|node112.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:Mmax-kFanno/;
$external_labels{$key} = "$URL/" . q|node147.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:fliegnerAir/;
$external_labels{$key} = "$URL/" . q|node83.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:fig:rigid/;
$external_labels{$key} = "$URL/" . q|node180.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:Tratioa/;
$external_labels{$key} = "$URL/" . q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:energyDEa/;
$external_labels{$key} = "$URL/" . q|node221.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:reducedDensity/;
$external_labels{$key} = "$URL/" . q|node205.html|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:mass/;
$external_labels{$key} = "$URL/" . q|node126.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:Punarranged/;
$external_labels{$key} = "$URL/" . q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Mach/;
$external_labels{$key} = "$URL/" . q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:movingShockDrag/;
$external_labels{$key} = "$URL/" . q|node215.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:fig:impulse/;
$external_labels{$key} = "$URL/" . q|node90.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:momentumD1/;
$external_labels{$key} = "$URL/" . q|node139.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:stagnationTempDiff2/;
$external_labels{$key} = "$URL/" . q|node106.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:maxUisotermalNozzle/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:bullet/;
$external_labels{$key} = "$URL/" . q|node210.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govDless4/;
$external_labels{$key} = "$URL/" . q|node171.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:insontropic2/;
$external_labels{$key} = "$URL/" . q|node68.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:temperaturePbar/;
$external_labels{$key} = "$URL/" . q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:fig:pmResults/;
$external_labels{$key} = "$URL/" . q|node224.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:sndRlawExDrivative/;
$external_labels{$key} = "$URL/" . q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:prandtl/;
$external_labels{$key} = "$URL/" . q|node58.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:CpCvRN/;
$external_labels{$key} = "$URL/" . q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:TrhoSolution/;
$external_labels{$key} = "$URL/" . q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:gibbsCp/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:fanno/;
$external_labels{$key} = "$URL/" . q|node137.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:RankineHugoniot/;
$external_labels{$key} = "$URL/" . q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:rhoR/;
$external_labels{$key} = "$URL/" . q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:MassFlowRatenonIdeal/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:max/;
$external_labels{$key} = "$URL/" . q|node212.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:TOTtmp/;
$external_labels{$key} = "$URL/" . q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:shockStrength/;
$external_labels{$key} = "$URL/" . q|node103.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:state/;
$external_labels{$key} = "$URL/" . q|node139.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:starMach/;
$external_labels{$key} = "$URL/" . q|node99.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dPoP/;
$external_labels{$key} = "$URL/" . q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:Isothermal/;
$external_labels{$key} = "$URL/" . q|node128.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:emanuel/;
$external_labels{$key} = "$URL/" . q|node201.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:inletEx/;
$external_labels{$key} = "$URL/" . q|node213.html|; 
$noresave{$key} = "$nosave";

$key = q/varialbeArea:eq:bernnolliZ/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:rayleigh/;
$external_labels{$key} = "$URL/" . q|node55.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:fig:fig3/;
$external_labels{$key} = "$URL/" . q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/varibleShock:eq:mvMs/;
$external_labels{$key} = "$URL/" . q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:massFlowRate/;
$external_labels{$key} = "$URL/" . q|node83.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:CloseValveUy/;
$external_labels{$key} = "$URL/" . q|node114.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:math:m2express2/;
$external_labels{$key} = "$URL/" . q|node132.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:program/;
$external_labels{$key} = "$URL/" . q|node228.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:Tratio/;
$external_labels{$key} = "$URL/" . q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:item:CloseValvePa/;
$external_labels{$key} = "$URL/" . q|node114.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:PzeroReduced1/;
$external_labels{$key} = "$URL/" . q|node77.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:massNonD1/;
$external_labels{$key} = "$URL/" . q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:sound/;
$external_labels{$key} = "$URL/" . q|node66.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:dless-T/;
$external_labels{$key} = "$URL/" . q|node171.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:2ndLaw/;
$external_labels{$key} = "$URL/" . q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea_eq:massPAequal/;
$external_labels{$key} = "$URL/" . q|node85.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:S/;
$external_labels{$key} = "$URL/" . q|node200.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:HDdef/;
$external_labels{$key} = "$URL/" . q|node139.html|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:isoChokingPoint/;
$external_labels{$key} = "$URL/" . q|node126.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:integralMach/;
$external_labels{$key} = "$URL/" . q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:noInclation/;
$external_labels{$key} = "$URL/" . q|node193.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:solutionIntergral/;
$external_labels{$key} = "$URL/" . q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:thermo/;
$external_labels{$key} = "$URL/" . q|node79.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:solution2/;
$external_labels{$key} = "$URL/" . q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/eq::varibleArea:combine2/;
$external_labels{$key} = "$URL/" . q|node79.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:M1/;
$external_labels{$key} = "$URL/" . q|node205.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:HDdef/;
$external_labels{$key} = "$URL/" . q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:temperatureDless/;
$external_labels{$key} = "$URL/" . q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:massMon0/;
$external_labels{$key} = "$URL/" . q|node126.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:PfunFLDa/;
$external_labels{$key} = "$URL/" . q|node152.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:momentum/;
$external_labels{$key} = "$URL/" . q|node94.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:M2/;
$external_labels{$key} = "$URL/" . q|node204.html|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:fanno/;
$external_labels{$key} = "$URL/" . q|node57.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:fig:angleDef/;
$external_labels{$key} = "$URL/" . q|node219.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:M-Tzcriticaln_1/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:M-A-0/;
$external_labels{$key} = "$URL/" . q|node81.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:fig:coor/;
$external_labels{$key} = "$URL/" . q|node221.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:state/;
$external_labels{$key} = "$URL/" . q|node129.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:averageVelocity/;
$external_labels{$key} = "$URL/" . q|node73.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:stateDless/;
$external_labels{$key} = "$URL/" . q|node79.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:stabiltyShock/;
$external_labels{$key} = "$URL/" . q|node217.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:Rindenty/;
$external_labels{$key} = "$URL/" . q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:t2t1a/;
$external_labels{$key} = "$URL/" . q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:cv_flow/;
$external_labels{$key} = "$URL/" . q|node137.html|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:eq:idealGasDifuserEfficiency/;
$external_labels{$key} = "$URL/" . q|node124.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:conversionT0T/;
$external_labels{$key} = "$URL/" . q|node106.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:math:def/;
$external_labels{$key} = "$URL/" . q|node132.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:fig:stationary/;
$external_labels{$key} = "$URL/" . q|node68.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:speedSoundNonIdealGas/;
$external_labels{$key} = "$URL/" . q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:x1/;
$external_labels{$key} = "$URL/" . q|node200.html|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:massMon3/;
$external_labels{$key} = "$URL/" . q|node126.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:ex:openValveBB/;
$external_labels{$key} = "$URL/" . q|node120.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:temperatureRho/;
$external_labels{$key} = "$URL/" . q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:deltaFLD/;
$external_labels{$key} = "$URL/" . q|node157.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:compressedGas/;
$external_labels{$key} = "$URL/" . q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:genGovSemiRigit/;
$external_labels{$key} = "$URL/" . q|node184.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:subFLDP2P1/;
$external_labels{$key} = "$URL/" . q|node159.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:reverseTheta/;
$external_labels{$key} = "$URL/" . q|node221.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:energyDerivative/;
$external_labels{$key} = "$URL/" . q|node139.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:math2/;
$external_labels{$key} = "$URL/" . q|node132.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:crPbar/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:openValveEx/;
$external_labels{$key} = "$URL/" . q|node120.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:forced/;
$external_labels{$key} = "$URL/" . q|node125.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:fig:basic/;
$external_labels{$key} = "$URL/" . q|node167.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:mu2/;
$external_labels{$key} = "$URL/" . q|node219.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:massMomOFS/;
$external_labels{$key} = "$URL/" . q|node100.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:pressureGov/;
$external_labels{$key} = "$URL/" . q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:solution/;
$external_labels{$key} = "$URL/" . q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:seperatedSemiRigida/;
$external_labels{$key} = "$URL/" . q|node184.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:ex:ex2/;
$external_labels{$key} = "$URL/" . q|node145.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:Pratio/;
$external_labels{$key} = "$URL/" . q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:maxKSuddenlyOpen/;
$external_labels{$key} = "$URL/" . q|node112.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:vanWylen/;
$external_labels{$key} = "$URL/" . q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:stateD/;
$external_labels{$key} = "$URL/" . q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/ch:fluid/;
$external_labels{$key} = "$URL/" . q|node61.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:bernolliSound/;
$external_labels{$key} = "$URL/" . q|node221.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:dless-V/;
$external_labels{$key} = "$URL/" . q|node171.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:T0T/;
$external_labels{$key} = "$URL/" . q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:M-Tz/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:U3-2/;
$external_labels{$key} = "$URL/" . q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:sndRlawEx/;
$external_labels{$key} = "$URL/" . q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:sharpNose/;
$external_labels{$key} = "$URL/" . q|node212.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:Rhoratio/;
$external_labels{$key} = "$URL/" . q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:ex:inletEx/;
$external_labels{$key} = "$URL/" . q|node213.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:MachReflection/;
$external_labels{$key} = "$URL/" . q|node211.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:tab:suddenlyOpened1.3/;
$external_labels{$key} = "$URL/" . q|node121.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:generalSolution/;
$external_labels{$key} = "$URL/" . q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:increaseFLD/;
$external_labels{$key} = "$URL/" . q|node149.html|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:eq:maxEnergy/;
$external_labels{$key} = "$URL/" . q|node123.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:smallPresSolution/;
$external_labels{$key} = "$URL/" . q|node132.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:energyDE0/;
$external_labels{$key} = "$URL/" . q|node221.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoAratioStar/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:fig:dalembert/;
$external_labels{$key} = "$URL/" . q|node225.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:weakThompson/;
$external_labels{$key} = "$URL/" . q|node201.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:fig:angleDefa/;
$external_labels{$key} = "$URL/" . q|node225.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:whatFn2/;
$external_labels{$key} = "$URL/" . q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:mass/;
$external_labels{$key} = "$URL/" . q|node94.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:fig:basic/;
$external_labels{$key} = "$URL/" . q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Fn-funFS/;
$external_labels{$key} = "$URL/" . q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:ex:reversed/;
$external_labels{$key} = "$URL/" . q|node226.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:fMsimple/;
$external_labels{$key} = "$URL/" . q|node176.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:fig:simpleExample/;
$external_labels{$key} = "$URL/" . q|node226.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:flowView/;
$external_labels{$key} = "$URL/" . q|node201.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:starSpeedSound/;
$external_labels{$key} = "$URL/" . q|node99.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MsxPistonLimit/;
$external_labels{$key} = "$URL/" . q|node111.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:cvEnergy1/;
$external_labels{$key} = "$URL/" . q|node68.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:fig:twoConnection/;
$external_labels{$key} = "$URL/" . q|node170.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Ucomparison/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:CpCvR/;
$external_labels{$key} = "$URL/" . q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:T0ratio/;
$external_labels{$key} = "$URL/" . q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Fnz/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:unPr/;
$external_labels{$key} = "$URL/" . q|node100.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:fig:isoBasic/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:fig:atmos/;
$external_labels{$key} = "$URL/" . q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:fmG/;
$external_labels{$key} = "$URL/" . q|node83.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:soundDless/;
$external_labels{$key} = "$URL/" . q|node171.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:isonropic/;
$external_labels{$key} = "$URL/" . q|node73.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:momentumD/;
$external_labels{$key} = "$URL/" . q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoRatio/;
$external_labels{$key} = "$URL/" . q|node91.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:item:openValvePa/;
$external_labels{$key} = "$URL/" . q|node113.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:Msolution1/;
$external_labels{$key} = "$URL/" . q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:U3U2/;
$external_labels{$key} = "$URL/" . q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:totalPressureRatio/;
$external_labels{$key} = "$URL/" . q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:eckert/;
$external_labels{$key} = "$URL/" . q|node59.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:f/;
$external_labels{$key} = "$URL/" . q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:initialCondition/;
$external_labels{$key} = "$URL/" . q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govsemiRigid1/;
$external_labels{$key} = "$URL/" . q|node183.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:math:series/;
$external_labels{$key} = "$URL/" . q|node132.html|; 
$noresave{$key} = "$nosave";

$key = q/computer:fig:structure/;
$external_labels{$key} = "$URL/" . q|node229.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:P-M1/;
$external_labels{$key} = "$URL/" . q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:ex:twoPistons/;
$external_labels{$key} = "$URL/" . q|node115.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:relationshipU-rho/;
$external_labels{$key} = "$URL/" . q|node80.html|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:massMon4/;
$external_labels{$key} = "$URL/" . q|node126.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:x2/;
$external_labels{$key} = "$URL/" . q|node200.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:gibbsCp/;
$external_labels{$key} = "$URL/" . q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:eq:idealGasDifuserEfficiencyIni/;
$external_labels{$key} = "$URL/" . q|node124.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MyfMx/;
$external_labels{$key} = "$URL/" . q|node108.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Fn-funFSa/;
$external_labels{$key} = "$URL/" . q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:twoPistons/;
$external_labels{$key} = "$URL/" . q|node115.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:partialyOpenValve/;
$external_labels{$key} = "$URL/" . q|node114.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:nozzleComparison/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:t2t1b/;
$external_labels{$key} = "$URL/" . q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:tbl:solidSpeed/;
$external_labels{$key} = "$URL/" . q|node72.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:massFlowRateRatio/;
$external_labels{$key} = "$URL/" . q|node83.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:bernoulliStagnation1/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:anotherMtoU/;
$external_labels{$key} = "$URL/" . q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:tmaxtc/;
$external_labels{$key} = "$URL/" . q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:ex:tunnel2deflection/;
$external_labels{$key} = "$URL/" . q|node213.html|; 
$noresave{$key} = "$nosave";

$key = q/iso:fig:cv/;
$external_labels{$key} = "$URL/" . q|node128.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:fliegnerk/;
$external_labels{$key} = "$URL/" . q|node83.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:tab:basic/;
$external_labels{$key} = "$URL/" . q|node167.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:massFlowRateID/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:fig:cv/;
$external_labels{$key} = "$URL/" . q|node78.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:epsilonF/;
$external_labels{$key} = "$URL/" . q|node202.html|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:fig:nozzle/;
$external_labels{$key} = "$URL/" . q|node122.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoRatioIdealgas/;
$external_labels{$key} = "$URL/" . q|node91.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MyfMxs/;
$external_labels{$key} = "$URL/" . q|node108.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:tmp3/;
$external_labels{$key} = "$URL/" . q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:M1n/;
$external_labels{$key} = "$URL/" . q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:fig:difsure_efficiency/;
$external_labels{$key} = "$URL/" . q|node124.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:massNonD0/;
$external_labels{$key} = "$URL/" . q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/varibleShock:eq:mvUbar/;
$external_labels{$key} = "$URL/" . q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:T2T1R/;
$external_labels{$key} = "$URL/" . q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:pressureDless/;
$external_labels{$key} = "$URL/" . q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:energy/;
$external_labels{$key} = "$URL/" . q|node221.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:CloseValveMy/;
$external_labels{$key} = "$URL/" . q|node114.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:Pbar/;
$external_labels{$key} = "$URL/" . q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:general/;
$external_labels{$key} = "$URL/" . q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:ex:throat/;
$external_labels{$key} = "$URL/" . q|node213.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:MBAdiabatic/;
$external_labels{$key} = "$URL/" . q|node176.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:TxTyInformal/;
$external_labels{$key} = "$URL/" . q|node96.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:energy/;
$external_labels{$key} = "$URL/" . q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:mass/;
$external_labels{$key} = "$URL/" . q|node129.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:ex2/;
$external_labels{$key} = "$URL/" . q|node145.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:mass/;
$external_labels{$key} = "$URL/" . q|node139.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:fig:piston/;
$external_labels{$key} = "$URL/" . q|node68.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:monImp/;
$external_labels{$key} = "$URL/" . q|node90.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:tmp1a/;
$external_labels{$key} = "$URL/" . q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:prandtl/;
$external_labels{$key} = "$URL/" . q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:adiabatic/;
$external_labels{$key} = "$URL/" . q|node220.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:fannoGraph/;
$external_labels{$key} = "$URL/" . q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:nu/;
$external_labels{$key} = "$URL/" . q|node220.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:machsSuddenStop/;
$external_labels{$key} = "$URL/" . q|node108.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:gov/;
$external_labels{$key} = "$URL/" . q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:maxM1fld1/;
$external_labels{$key} = "$URL/" . q|node156.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:intersection/;
$external_labels{$key} = "$URL/" . q|node94.html|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:rankine/;
$external_labels{$key} = "$URL/" . q|node56.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MyLimit/;
$external_labels{$key} = "$URL/" . q|node102.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoTzeroR1/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:PstarPzeroINI/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoIdealgas/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:beforeDefa/;
$external_labels{$key} = "$URL/" . q|node90.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:stateMassD/;
$external_labels{$key} = "$URL/" . q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:strongThompson/;
$external_labels{$key} = "$URL/" . q|node201.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:PrDless/;
$external_labels{$key} = "$URL/" . q|node100.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:rhoBar/;
$external_labels{$key} = "$URL/" . q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:eq:nozzleEfficiency/;
$external_labels{$key} = "$URL/" . q|node123.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:shockPartialOpen/;
$external_labels{$key} = "$URL/" . q|node113.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:memSimple/;
$external_labels{$key} = "$URL/" . q|node68.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoTzeroR/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:tempSimple/;
$external_labels{$key} = "$URL/" . q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:CloseValveMx/;
$external_labels{$key} = "$URL/" . q|node114.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:exitU/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dT_0dT/;
$external_labels{$key} = "$URL/" . q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:actualCalM-FLD_0.3/;
$external_labels{$key} = "$URL/" . q|node155.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:press-M/;
$external_labels{$key} = "$URL/" . q|node81.html|; 
$noresave{$key} = "$nosave";

$key = q/variableShock:ex:tank/;
$external_labels{$key} = "$URL/" . q|node122.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Ucomparison1/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:solutionChemical/;
$external_labels{$key} = "$URL/" . q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:govDxi1/;
$external_labels{$key} = "$URL/" . q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:movingShockDrag2/;
$external_labels{$key} = "$URL/" . q|node107.html|; 
$noresave{$key} = "$nosave";

$key = q/math:eq:binomal/;
$external_labels{$key} = "$URL/" . q|node77.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:Vtime/;
$external_labels{$key} = "$URL/" . q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:stateDa/;
$external_labels{$key} = "$URL/" . q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:subBranch/;
$external_labels{$key} = "$URL/" . q|node159.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:timeFilling/;
$external_labels{$key} = "$URL/" . q|node176.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoPratioStar/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:RR/;
$external_labels{$key} = "$URL/" . q|node204.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:momentumD1b/;
$external_labels{$key} = "$URL/" . q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoPzeroRatioStar/;
$external_labels{$key} = "$URL/" . q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isentropicR/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:fig:Ts/;
$external_labels{$key} = "$URL/" . q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:P0P0starmid/;
$external_labels{$key} = "$URL/" . q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:state/;
$external_labels{$key} = "$URL/" . q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:sec:movingShockDrag/;
$external_labels{$key} = "$URL/" . q|node215.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govEq/;
$external_labels{$key} = "$URL/" . q|node170.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:entropyA/;
$external_labels{$key} = "$URL/" . q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:oblique/;
$external_labels{$key} = "$URL/" . q|node192.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:pressureRatio/;
$external_labels{$key} = "$URL/" . q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:smallMaxDefA1/;
$external_labels{$key} = "$URL/" . q|node202.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:sec:movingShockDrag/;
$external_labels{$key} = "$URL/" . q|node107.html|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:nozzlePressure/;
$external_labels{$key} = "$URL/" . q|node44.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:ex:reservoir/;
$external_labels{$key} = "$URL/" . q|node145.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:RzeroReduced1/;
$external_labels{$key} = "$URL/" . q|node77.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:Pratio/;
$external_labels{$key} = "$URL/" . q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:fig:model/;
$external_labels{$key} = "$URL/" . q|node170.html|; 
$noresave{$key} = "$nosave";

1;


# LaTeX2HTML 2002-2-1 (1.71)
# labels from external_latex_labels array.


$key = q/shock:fig:shockDrag/;
$external_latex_labels{$key} = q|5.5|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:delta-thetaA/;
$external_latex_labels{$key} = q|13.52|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:MachDef/;
$external_latex_labels{$key} = q|8.29|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:fig:comperTandP/;
$external_latex_labels{$key} = q|4.8|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:govD/;
$external_latex_labels{$key} = q|12.4|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:mach/;
$external_latex_labels{$key} = q|1.6|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:yetMtoUfunD/;
$external_latex_labels{$key} = q|8.14|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:PdP2/;
$external_latex_labels{$key} = q|9.22|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:APtotal/;
$external_latex_labels{$key} = q|4.74|; 
$noresave{$key} = "$nosave";

$key = q/pm:fig:reversed/;
$external_latex_labels{$key} = q|14.10|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:fig:basic/;
$external_latex_labels{$key} = q|4.3|; 
$noresave{$key} = "$nosave";

$key = q/sub_shock:fig:partialyCloseMv/;
$external_latex_labels{$key} = q|(b)|; 
$noresave{$key} = "$nosave";

$key = q/chap:piston/;
$external_latex_labels{$key} = q|12|; 
$noresave{$key} = "$nosave";

$key = q/variableShock:ex:superSonicTunnel/;
$external_latex_labels{$key} = q|6.3|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:MstarReduced/;
$external_latex_labels{$key} = q|4.20|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:X/;
$external_latex_labels{$key} = q|3.50|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:tc/;
$external_latex_labels{$key} = q|11.5|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:PvPT/;
$external_latex_labels{$key} = q|3.24|; 
$noresave{$key} = "$nosave";

$key = q/fluid:eq:dEta/;
$external_latex_labels{$key} = q|2.1|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:cv/;
$external_latex_labels{$key} = q|5.1|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:mass/;
$external_latex_labels{$key} = q|14.10|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:tab:basic/;
$external_latex_labels{$key} = q|8.1|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:fig:nozzle/;
$external_latex_labels{$key} = q|4.1|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Mz_1/;
$external_latex_labels{$key} = q|4.145|; 
$noresave{$key} = "$nosave";

$key = q/shock:sec:PartiallyCloseValve/;
$external_latex_labels{$key} = q|5.3.5|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:M2t/;
$external_latex_labels{$key} = q|13.11|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:densityPbar/;
$external_latex_labels{$key} = q|5.26|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:solSemiRigid/;
$external_latex_labels{$key} = q|11.46|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:mass/;
$external_latex_labels{$key} = q|11.2|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MypMax/;
$external_latex_labels{$key} = q|5.73|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:combine1/;
$external_latex_labels{$key} = q|4.30|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:solutionXi1/;
$external_latex_labels{$key} = q|12.8|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:impulsDefStar/;
$external_latex_labels{$key} = q|4.109|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:massMon1/;
$external_latex_labels{$key} = q|7.6|; 
$noresave{$key} = "$nosave";

$key = q/obalique:fig:detachWedge/;
$external_latex_labels{$key} = q|13.20|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:densityMsx/;
$external_latex_labels{$key} = q|5.64|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:mem/;
$external_latex_labels{$key} = q|3.9|; 
$noresave{$key} = "$nosave";

$key = q/thermo:eq:maxwell/;
$external_latex_labels{$key} = q|4.118|; 
$noresave{$key} = "$nosave";

$key = q/sub_shock:fig:partialyOpenSt/;
$external_latex_labels{$key} = q|(a)|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dPzero-fld/;
$external_latex_labels{$key} = q|9.30|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:solRigitFannoFast/;
$external_latex_labels{$key} = q|11.34|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:paritalyOpenUSa/;
$external_latex_labels{$key} = q|5.77|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:x3/;
$external_latex_labels{$key} = q|13.25|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:M-Tzcritical/;
$external_latex_labels{$key} = q|4.148|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:solD/;
$external_latex_labels{$key} = q|13.37|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:rhoToP/;
$external_latex_labels{$key} = q|3.33|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:combinedState/;
$external_latex_labels{$key} = q|3.42|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoPzeroRatioStarMax/;
$external_latex_labels{$key} = q|4.93|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:workingFLDAppSol/;
$external_latex_labels{$key} = q|8.38|; 
$noresave{$key} = "$nosave";

$key = q/chap:shock/;
$external_latex_labels{$key} = q|5|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:acceleratingPiston/;
$external_latex_labels{$key} = q|5.18|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:sound/;
$external_latex_labels{$key} = q|3.13|; 
$noresave{$key} = "$nosave";

$key = q/sub_variableArea:fig:ComparisonFunA/;
$external_latex_labels{$key} = q|(b)|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:Ubar/;
$external_latex_labels{$key} = q|8.30|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:Mtheta/;
$external_latex_labels{$key} = q|14.31|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:solRigitFannoFastb/;
$external_latex_labels{$key} = q|11.39|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:P0R/;
$external_latex_labels{$key} = q|13.58|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:massFlowRate1/;
$external_latex_labels{$key} = q|4.45|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govsemiRigid/;
$external_latex_labels{$key} = q|11.43|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:mom1Theta/;
$external_latex_labels{$key} = q|14.15|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:x/;
$external_latex_labels{$key} = q|13.19|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:PoverT/;
$external_latex_labels{$key} = q|11.15|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:improvedFLD/;
$external_latex_labels{$key} = q|9.53|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:openValvePipe/;
$external_latex_labels{$key} = q|5.24|; 
$noresave{$key} = "$nosave";

$key = q/tank:fig:chamber/;
$external_latex_labels{$key} = q|11.4|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:isothermalNozzlea/;
$external_latex_labels{$key} = q|11.29|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dRdU/;
$external_latex_labels{$key} = q|9.18|; 
$noresave{$key} = "$nosave";

$key = q/pm:fig:machLineAngle/;
$external_latex_labels{$key} = q|14.2|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dA_w/;
$external_latex_labels{$key} = q|9.9|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:fnA/;
$external_latex_labels{$key} = q|4.52|; 
$noresave{$key} = "$nosave";

$key = q/fanno:ex:largeFLD-SS/;
$external_latex_labels{$key} = q|9.3|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:smallMachMaxDeflction/;
$external_latex_labels{$key} = q|13.42|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:fld2/;
$external_latex_labels{$key} = q|9.50|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:conversionUx/;
$external_latex_labels{$key} = q|5.44|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:gibbsCpds0/;
$external_latex_labels{$key} = q|3.28|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:pressureMx/;
$external_latex_labels{$key} = q|5.23|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:Cv/;
$external_latex_labels{$key} = q|3.22|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:solution/;
$external_latex_labels{$key} = q|12.9|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:tmaxtcAAC/;
$external_latex_labels{$key} = q|12.15|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:2law/;
$external_latex_labels{$key} = q|3.44|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:TtoPz1/;
$external_latex_labels{$key} = q|4.143|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:cubic/;
$external_latex_labels{$key} = q|13.18|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:bullet/;
$external_latex_labels{$key} = q|1.7|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:maxMassFlowRateOpenValve/;
$external_latex_labels{$key} = q|5.74|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:energy3/;
$external_latex_labels{$key} = q|7.3|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:energyInt/;
$external_latex_labels{$key} = q|4.123|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:ShockMoveAA/;
$external_latex_labels{$key} = q|5.10|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Fn-fun/;
$external_latex_labels{$key} = q|4.63|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:fig:heatTube/;
$external_latex_labels{$key} = q|4.2|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:example1/;
$external_latex_labels{$key} = q|9.3|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:conversionMx/;
$external_latex_labels{$key} = q|5.46|; 
$noresave{$key} = "$nosave";

$key = q/shock:tab:suddenlyOpened/;
$external_latex_labels{$key} = q|5.3|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:areaChangeMach/;
$external_latex_labels{$key} = q|4.34|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:fnDef/;
$external_latex_labels{$key} = q|4.53|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:maxMa/;
$external_latex_labels{$key} = q|13.39|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:epsilonFA/;
$external_latex_labels{$key} = q|13.44|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:TR/;
$external_latex_labels{$key} = q|13.55|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:reducedPressure/;
$external_latex_labels{$key} = q|13.60|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoAratio/;
$external_latex_labels{$key} = q|4.87|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:mvU-P-Results/;
$external_latex_labels{$key} = q|5.95|; 
$noresave{$key} = "$nosave";

$key = q/piston:fig:cv/;
$external_latex_labels{$key} = q|12.1|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:stangtionMach/;
$external_latex_labels{$key} = q|5.29|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:subM1P2P1-D/;
$external_latex_labels{$key} = q|9.20|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:stagnationTempDiff2a/;
$external_latex_labels{$key} = q|5.56|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:isothermalNozzleInt/;
$external_latex_labels{$key} = q|11.31|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:momentumD1/;
$external_latex_labels{$key} = q|8.9|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:temp-M/;
$external_latex_labels{$key} = q|4.39|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:conversionP0P/;
$external_latex_labels{$key} = q|5.50|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:cvCombined/;
$external_latex_labels{$key} = q|3.4|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:dTds/;
$external_latex_labels{$key} = q|10.14|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:energy2/;
$external_latex_labels{$key} = q|7.2|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:fmtheta1/;
$external_latex_labels{$key} = q|13.34|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Uz/;
$external_latex_labels{$key} = q|4.134|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MsxPistonD/;
$external_latex_labels{$key} = q|5.67|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:stagnationTempDiff/;
$external_latex_labels{$key} = q|5.51|; 
$noresave{$key} = "$nosave";

$key = q/shock:ex:openValve/;
$external_latex_labels{$key} = q|5.12|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:openValvePipeB/;
$external_latex_labels{$key} = q|5.22|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoTPRatio/;
$external_latex_labels{$key} = q|4.103|; 
$noresave{$key} = "$nosave";

$key = q/fanno_eq:fDef/;
$external_latex_labels{$key} = q|9.37|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:MaxTurning/;
$external_latex_labels{$key} = q|14.36|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:impulsDef/;
$external_latex_labels{$key} = q|4.108|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:e9d/;
$external_latex_labels{$key} = q|11.54|; 
$noresave{$key} = "$nosave";

$key = q/shock:tab:basic/;
$external_latex_labels{$key} = q|5.1|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:isothermalNozzle/;
$external_latex_labels{$key} = q|11.30|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:MtoUfunD/;
$external_latex_labels{$key} = q|8.13|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:solRigitFannoFasta/;
$external_latex_labels{$key} = q|11.33|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:math:m2express/;
$external_latex_labels{$key} = q|8.43|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MxlimitOpen/;
$external_latex_labels{$key} = q|5.69|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:m2/;
$external_latex_labels{$key} = q|8.40|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isentropicPratio/;
$external_latex_labels{$key} = q|4.102|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:tab:unchoked/;
$external_latex_labels{$key} = q|8.2|; 
$noresave{$key} = "$nosave";

$key = q/ray:fig:cv/;
$external_latex_labels{$key} = q|10.1|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:energy0/;
$external_latex_labels{$key} = q|4.1|; 
$noresave{$key} = "$nosave";

$key = q/chap:tank/;
$external_latex_labels{$key} = q|11|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:workingFLDApprox/;
$external_latex_labels{$key} = q|8.37|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:tempSimple3/;
$external_latex_labels{$key} = q|4.7|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:tab:basicIsentropic/;
$external_latex_labels{$key} = q|4.2|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:momentum/;
$external_latex_labels{$key} = q|8.2|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:gibbsCv/;
$external_latex_labels{$key} = q|3.29|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:galileo/;
$external_latex_labels{$key} = q|1.5|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:ImpulseRatio/;
$external_latex_labels{$key} = q|4.110|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:PratioDimensional/;
$external_latex_labels{$key} = q|4.82|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:R/;
$external_latex_labels{$key} = q|13.30|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:e9e/;
$external_latex_labels{$key} = q|11.53|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:D-theta-My-Mx_3/;
$external_latex_labels{$key} = q|13.9|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:isoMix/;
$external_latex_labels{$key} = q|3.45|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:fannoM1fM2/;
$external_latex_labels{$key} = q|9.10|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:SepGenGovSemiRigit/;
$external_latex_labels{$key} = q|11.49|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:continutyNonD/;
$external_latex_labels{$key} = q|5.8|; 
$noresave{$key} = "$nosave";

$key = q/eq:shock:entropy/;
$external_latex_labels{$key} = q|5.5|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:maxSpeed/;
$external_latex_labels{$key} = q|5.27|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:massAnother/;
$external_latex_labels{$key} = q|8.21|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:minikoffSol/;
$external_latex_labels{$key} = q|13.41|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:conversionT/;
$external_latex_labels{$key} = q|5.43|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:solutionXi/;
$external_latex_labels{$key} = q|12.7|; 
$noresave{$key} = "$nosave";

$key = q/chap:variableArea/;
$external_latex_labels{$key} = q|4|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:P5P1/;
$external_latex_labels{$key} = q|5.98|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:ds-fld/;
$external_latex_labels{$key} = q|9.34|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Fn-funRho0T/;
$external_latex_labels{$key} = q|4.70|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:massFlowRate2/;
$external_latex_labels{$key} = q|4.46|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:energyNI/;
$external_latex_labels{$key} = q|4.124|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:TtoPz/;
$external_latex_labels{$key} = q|4.142|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:massTotalPressure/;
$external_latex_labels{$key} = q|4.72|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:eq:difuserEfficiency/;
$external_latex_labels{$key} = q|6.6|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:partialyOpenMv/;
$external_latex_labels{$key} = q|5.15(b)|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:tcSimple/;
$external_latex_labels{$key} = q|11.21|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:partialyCloseSt/;
$external_latex_labels{$key} = q|5.17(a)|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:dless-P/;
$external_latex_labels{$key} = q|11.4c|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:simpleEnergy/;
$external_latex_labels{$key} = q|4.10|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:paritalyOpenUS/;
$external_latex_labels{$key} = q|5.76|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:conversionUy/;
$external_latex_labels{$key} = q|5.45|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:delta-theta/;
$external_latex_labels{$key} = q|13.51|; 
$noresave{$key} = "$nosave";

$key = q/shock:ex:openValveContinue/;
$external_latex_labels{$key} = q|5.8|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:entrceL/;
$external_latex_labels{$key} = q|8.53|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:localFar/;
$external_latex_labels{$key} = q|13.12|; 
$noresave{$key} = "$nosave";

$key = q/sub_shock:fig:OpenSuddenlyConvergence0.3/;
$external_latex_labels{$key} = q|(a)|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:mvUs2/;
$external_latex_labels{$key} = q|5.86|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:momentum/;
$external_latex_labels{$key} = q|9.6|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:generalPV/;
$external_latex_labels{$key} = q|11.47|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:twoAngle/;
$external_latex_labels{$key} = q|13.21|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:Cp/;
$external_latex_labels{$key} = q|3.21|; 
$noresave{$key} = "$nosave";

$key = q/varibleShock:eq:mvUs/;
$external_latex_labels{$key} = q|5.84|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:stagnationPressreRatio1/;
$external_latex_labels{$key} = q|9.46|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:mixSound/;
$external_latex_labels{$key} = q|3.48|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:TrhoSolutionDless/;
$external_latex_labels{$key} = q|4.62|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:FLDretreat/;
$external_latex_labels{$key} = q|9.52|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:eq:actualEnergy/;
$external_latex_labels{$key} = q|6.2|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoRatioIdealgasISO/;
$external_latex_labels{$key} = q|4.114|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:nDef/;
$external_latex_labels{$key} = q|3.32|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MxpOpened/;
$external_latex_labels{$key} = q|5.59|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:energyAfterInt1/;
$external_latex_labels{$key} = q|4.126|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:bool/;
$external_latex_labels{$key} = q|13.23|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:fmtheta1a/;
$external_latex_labels{$key} = q|13.35|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:MtoUfun/;
$external_latex_labels{$key} = q|8.11|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:momentum/;
$external_latex_labels{$key} = q|10.2|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:theta/;
$external_latex_labels{$key} = q|13.5|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:timeLinearHight2/;
$external_latex_labels{$key} = q|3.17|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MsyMsx/;
$external_latex_labels{$key} = q|5.48|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:PR/;
$external_latex_labels{$key} = q|13.53|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:angle/;
$external_latex_labels{$key} = q|14.3|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:stagnationTempDiff1/;
$external_latex_labels{$key} = q|5.52|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:expession/;
$external_latex_labels{$key} = q|8.51|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:stagnationPressreRatio/;
$external_latex_labels{$key} = q|9.45|; 
$noresave{$key} = "$nosave";

$key = q/sound:tbl:liquidSpeed/;
$external_latex_labels{$key} = q|3.5|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:menikoff/;
$external_latex_labels{$key} = q|13.36|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:dT0T/;
$external_latex_labels{$key} = q|8.17|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:DzeroAlphaZero/;
$external_latex_labels{$key} = q|13.47|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:MyforMxDelta/;
$external_latex_labels{$key} = q|13.6|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MsxMyp/;
$external_latex_labels{$key} = q|5.71|; 
$noresave{$key} = "$nosave";

$key = q/thermo:eq:dhPT/;
$external_latex_labels{$key} = q|4.117|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:insontropic1/;
$external_latex_labels{$key} = q|3.6|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:incompressibleP-Loss/;
$external_latex_labels{$key} = q|8.39|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:Pbar/;
$external_latex_labels{$key} = q|13.13|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:shockStrengthMx/;
$external_latex_labels{$key} = q|5.40|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:weakTheta/;
$external_latex_labels{$key} = q|13.63|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:ShockMove/;
$external_latex_labels{$key} = q|5.6|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:dhNonID/;
$external_latex_labels{$key} = q|3.25|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:fig:nozzleWithShock/;
$external_latex_labels{$key} = q|6.2|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:M2n/;
$external_latex_labels{$key} = q|13.9|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:math:m2express5/;
$external_latex_labels{$key} = q|8.49|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:mass/;
$external_latex_labels{$key} = q|13.1|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:dTdM/;
$external_latex_labels{$key} = q|10.13|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:timeTank/;
$external_latex_labels{$key} = q|11.26|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:energy/;
$external_latex_labels{$key} = q|9.3|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:m2express/;
$external_latex_labels{$key} = q|8.41|; 
$noresave{$key} = "$nosave";

$key = q/variableArrea:eq:massFlowRatez/;
$external_latex_labels{$key} = q|4.136|; 
$noresave{$key} = "$nosave";

$key = q/sound:fig:compressiblityChart/;
$external_latex_labels{$key} = q|3.3|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:wilson/;
$external_latex_labels{$key} = q|3.37|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:bernoulli/;
$external_latex_labels{$key} = q|4.78|; 
$noresave{$key} = "$nosave";

$key = q/shock:item:openValve/;
$external_latex_labels{$key} = q|b|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:tab:basicIsothermal/;
$external_latex_labels{$key} = q|4.3|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:TratioExample/;
$external_latex_labels{$key} = q|9.48|; 
$noresave{$key} = "$nosave";

$key = q/chap:intro/;
$external_latex_labels{$key} = q|1|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:crPbarn_1/;
$external_latex_labels{$key} = q|4.141|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:entropyIdeal/;
$external_latex_labels{$key} = q|5.6|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:math:m2express4/;
$external_latex_labels{$key} = q|8.48|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:stangnationTempGov/;
$external_latex_labels{$key} = q|8.26|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:mm/;
$external_latex_labels{$key} = q|8.23|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:PoverSqrtT/;
$external_latex_labels{$key} = q|11.16|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:AstarReduced/;
$external_latex_labels{$key} = q|4.24|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:2law/;
$external_latex_labels{$key} = q|8.3|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:tangialVelocity/;
$external_latex_labels{$key} = q|14.20|; 
$noresave{$key} = "$nosave";

$key = q/fanno:item:fldSuper/;
$external_latex_labels{$key} = q|b|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:PDiffReduced/;
$external_latex_labels{$key} = q|4.19|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:chamberBlance/;
$external_latex_labels{$key} = q|11.1|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:fig:ComparisonFunA/;
$external_latex_labels{$key} = q|4.7(b)|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:mvdU-rho/;
$external_latex_labels{$key} = q|5.93|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:mom1/;
$external_latex_labels{$key} = q|14.11|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:fld-M/;
$external_latex_labels{$key} = q|9.25|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:logT0T/;
$external_latex_labels{$key} = q|8.19|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:energyDless/;
$external_latex_labels{$key} = q|5.13|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:gamma/;
$external_latex_labels{$key} = q|3.46|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:deltaDelta/;
$external_latex_labels{$key} = q|13.66|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:ATtotal/;
$external_latex_labels{$key} = q|4.75|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:Tbar/;
$external_latex_labels{$key} = q|9.41|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:bulkModulus/;
$external_latex_labels{$key} = q|3.35|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govDless6b/;
$external_latex_labels{$key} = q|11.17b|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:DrhoTOPFinal/;
$external_latex_labels{$key} = q|3.31|; 
$noresave{$key} = "$nosave";

$key = q/fanno:en:subChoked/;
$external_latex_labels{$key} = q|6|; 
$noresave{$key} = "$nosave";

$key = q/shock:ex:openValveAAA/;
$external_latex_labels{$key} = q|5.7|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:mvdU-rho/;
$external_latex_labels{$key} = q|5.92|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:mixTwoPhases/;
$external_latex_labels{$key} = q|3.49|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:solRigitFannoFastd/;
$external_latex_labels{$key} = q|11.38|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:Uratio/;
$external_latex_labels{$key} = q|9.43|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:massNonD2/;
$external_latex_labels{$key} = q|5.11|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govRigitFannoFasta/;
$external_latex_labels{$key} = q|11.32|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:regions/;
$external_latex_labels{$key} = q|13.2|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:zeroMachWave/;
$external_latex_labels{$key} = q|13.8|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MypOpened/;
$external_latex_labels{$key} = q|5.60|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:TPratio/;
$external_latex_labels{$key} = q|11.11|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:d-Nu/;
$external_latex_labels{$key} = q|14.8|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:mvRho/;
$external_latex_labels{$key} = q|5.91|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:tmp1/;
$external_latex_labels{$key} = q|9.20|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:energyAfterInt0/;
$external_latex_labels{$key} = q|4.125|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:omega/;
$external_latex_labels{$key} = q|14.37|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isentropicTratio/;
$external_latex_labels{$key} = q|4.101|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:shockTube/;
$external_latex_labels{$key} = q|5.20|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govDless1/;
$external_latex_labels{$key} = q|11.6|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:f/;
$external_latex_labels{$key} = q|9.10|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:massFlowRateStar/;
$external_latex_labels{$key} = q|4.47|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dU-fld/;
$external_latex_labels{$key} = q|9.27|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MypPbar/;
$external_latex_labels{$key} = q|5.72|; 
$noresave{$key} = "$nosave";

$key = q/eq:shock:state/;
$external_latex_labels{$key} = q|5.4|; 
$noresave{$key} = "$nosave";

$key = q/varibleArea:fig:exmpleNozzle/;
$external_latex_labels{$key} = q|4.10|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:cvMass1/;
$external_latex_labels{$key} = q|3.1|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:massFlowRatezn_1/;
$external_latex_labels{$key} = q|4.137|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:fig:MvA/;
$external_latex_labels{$key} = q|4.5|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:D/;
$external_latex_labels{$key} = q|13.28|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:mvDrho/;
$external_latex_labels{$key} = q|5.90|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:massImp/;
$external_latex_labels{$key} = q|4.105|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:mom2Theta/;
$external_latex_labels{$key} = q|14.17|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isodAdM/;
$external_latex_labels{$key} = q|4.94|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:Pratioa/;
$external_latex_labels{$key} = q|10.15|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:MassFlowRatenonIdeal1/;
$external_latex_labels{$key} = q|4.129|; 
$noresave{$key} = "$nosave";

$key = q/eq::varibleArea:combine3/;
$external_latex_labels{$key} = q|4.32|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:densityDless/;
$external_latex_labels{$key} = q|4.12|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:areaChangeVelocity/;
$external_latex_labels{$key} = q|4.33|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govDless6a/;
$external_latex_labels{$key} = q|11.17a|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govRigitFannoFast/;
$external_latex_labels{$key} = q|11.37|; 
$noresave{$key} = "$nosave";

$key = q/pm:fig:turnAngle/;
$external_latex_labels{$key} = q|14.3|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:eq:coefficeintDischarge/;
$external_latex_labels{$key} = q|6.5|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoPratio/;
$external_latex_labels{$key} = q|4.84|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:TrhoSolutionDlessA/;
$external_latex_labels{$key} = q|4.65|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:iserhoToP/;
$external_latex_labels{$key} = q|3.11|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:NetForce/;
$external_latex_labels{$key} = q|4.111|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:maxMassFlowRateOpenValveSimplified/;
$external_latex_labels{$key} = q|5.75|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:LargeSymmetrialAngle/;
$external_latex_labels{$key} = q|13.14|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:mass/;
$external_latex_labels{$key} = q|4.26|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dRho-fld/;
$external_latex_labels{$key} = q|9.29|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Fnzn_1/;
$external_latex_labels{$key} = q|4.139|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:dless/;
$external_latex_labels{$key} = q|11.4|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:govDxi/;
$external_latex_labels{$key} = q|12.5|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:entropySolution/;
$external_latex_labels{$key} = q|9.47|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:M1-FLD/;
$external_latex_labels{$key} = q|9.21|; 
$noresave{$key} = "$nosave";

$key = q/pm:ex:simpleExample/;
$external_latex_labels{$key} = q|14.1|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:fmMax/;
$external_latex_labels{$key} = q|4.55|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:mDotFLD/;
$external_latex_labels{$key} = q|9.8|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:T3T4R/;
$external_latex_labels{$key} = q|5.100|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:appoximaetionR/;
$external_latex_labels{$key} = q|3.41|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:T/;
$external_latex_labels{$key} = q|13.27|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:mvU-rhoResults/;
$external_latex_labels{$key} = q|5.94|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:Mach/;
$external_latex_labels{$key} = q|14.30|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:dless-t/;
$external_latex_labels{$key} = q|11.4d|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:momentum/;
$external_latex_labels{$key} = q|13.2|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:fullFld/;
$external_latex_labels{$key} = q|11.18|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:thetaR/;
$external_latex_labels{$key} = q|13.7|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dT-fld/;
$external_latex_labels{$key} = q|9.28|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:massMon2/;
$external_latex_labels{$key} = q|7.7|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:partialyOpenValvea/;
$external_latex_labels{$key} = q|5.15|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:T0bar/;
$external_latex_labels{$key} = q|8.35|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govIsoDless1/;
$external_latex_labels{$key} = q|11.41|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:smallMaxDefA/;
$external_latex_labels{$key} = q|13.45|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:timeLinearHight/;
$external_latex_labels{$key} = q|3.16|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:wedgeEnd/;
$external_latex_labels{$key} = q|13.11|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:energyDE/;
$external_latex_labels{$key} = q|14.26|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:solRigitFannoFastc/;
$external_latex_labels{$key} = q|11.40|; 
$noresave{$key} = "$nosave";

$key = q/shock:sec:stationaryShockDrag/;
$external_latex_labels{$key} = q|5.2.4|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:nozzleTankSolution/;
$external_latex_labels{$key} = q|11.25|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:bernoulliStagnation/;
$external_latex_labels{$key} = q|4.81|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:heatTranser/;
$external_latex_labels{$key} = q|4.98|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:conversionP/;
$external_latex_labels{$key} = q|5.42|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:energy/;
$external_latex_labels{$key} = q|10.1|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:fannoM1M2fld/;
$external_latex_labels{$key} = q|9.9|; 
$noresave{$key} = "$nosave";

$key = q/sound:tbl:gasSpeed/;
$external_latex_labels{$key} = q|3.5|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:longDLDwithShock/;
$external_latex_labels{$key} = q|9.15|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:fld1/;
$external_latex_labels{$key} = q|9.49|; 
$noresave{$key} = "$nosave";

$key = q/chap:rayleigh/;
$external_latex_labels{$key} = q|10|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoTzeroRatioStar/;
$external_latex_labels{$key} = q|4.91|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:math:m2express6/;
$external_latex_labels{$key} = q|8.50|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:fM/;
$external_latex_labels{$key} = q|11.12|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:energy/;
$external_latex_labels{$key} = q|4.25|; 
$noresave{$key} = "$nosave";

$key = q/veriableAreaeq:MassFlowRatezn_1/;
$external_latex_labels{$key} = q|4.151|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:rhoNewDef/;
$external_latex_labels{$key} = q|4.61|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:MBAdiabaticSimple/;
$external_latex_labels{$key} = q|11.23|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:limitedTheta/;
$external_latex_labels{$key} = q|13.10|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:idealGas1/;
$external_latex_labels{$key} = q|3.12|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoRatioIdealgasStar/;
$external_latex_labels{$key} = q|4.115|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:mvU2/;
$external_latex_labels{$key} = q|5.88|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:suddenlyOpenSt/;
$external_latex_labels{$key} = q|5.12(a)|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:universalIntropy/;
$external_latex_labels{$key} = q|9.31|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:solRigitFannoFastbb/;
$external_latex_labels{$key} = q|11.36|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:temperature2/;
$external_latex_labels{$key} = q|4.2|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:dUrUtheta/;
$external_latex_labels{$key} = q|14.21|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:M2_0/;
$external_latex_labels{$key} = q|13.56|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:math:m2express3/;
$external_latex_labels{$key} = q|8.45|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:workingFLD/;
$external_latex_labels{$key} = q|8.36|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govDless2/;
$external_latex_labels{$key} = q|11.8|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:cvMass2/;
$external_latex_labels{$key} = q|3.2|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govDless6c/;
$external_latex_labels{$key} = q|11.17c|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:linear/;
$external_latex_labels{$key} = q|11.52|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:massD/;
$external_latex_labels{$key} = q|8.15|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:menikoffLarge/;
$external_latex_labels{$key} = q|13.40|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:tab:Fn/;
$external_latex_labels{$key} = q|4.1|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:suddenlyOpen/;
$external_latex_labels{$key} = q|5.12|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:QdeltaZero1/;
$external_latex_labels{$key} = q|13.49|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:solSemiRigida/;
$external_latex_labels{$key} = q|11.51|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:ex:impulse/;
$external_latex_labels{$key} = q|4.7|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:AstarTotalPressure/;
$external_latex_labels{$key} = q|4.73|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:nonIdealRelation/;
$external_latex_labels{$key} = q|4.131|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:energy/;
$external_latex_labels{$key} = q|7.1|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dM2-fld/;
$external_latex_labels{$key} = q|9.26|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:Ac/;
$external_latex_labels{$key} = q|12.13|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:Q/;
$external_latex_labels{$key} = q|13.29|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:fig:compareNozzle/;
$external_latex_labels{$key} = q|4.7(a)|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MyfMxopened/;
$external_latex_labels{$key} = q|5.63|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:RH/;
$external_latex_labels{$key} = q|5.41|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:conversionMy/;
$external_latex_labels{$key} = q|5.47|; 
$noresave{$key} = "$nosave";

$key = q/varibleShock:eq:mvPbar/;
$external_latex_labels{$key} = q|5.82|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MsxPiston/;
$external_latex_labels{$key} = q|5.66|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:openedDeltaTr/;
$external_latex_labels{$key} = q|5.62|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:fld-dM1/;
$external_latex_labels{$key} = q|9.36|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:gibbs/;
$external_latex_labels{$key} = q|3.20|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:movingShockDrag/;
$external_latex_labels{$key} = q|5.7|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:stangnationPressureGov/;
$external_latex_labels{$key} = q|8.25|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:energySolution/;
$external_latex_labels{$key} = q|14.28|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:temperatureDlessModifiedB/;
$external_latex_labels{$key} = q|4.58|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:Dzero/;
$external_latex_labels{$key} = q|13.50|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:stationaryMx/;
$external_latex_labels{$key} = q|5.54|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:kM2/;
$external_latex_labels{$key} = q|4.95|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:verticalLine/;
$external_latex_labels{$key} = q|14.5|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:OpenSuddenlyConvergence/;
$external_latex_labels{$key} = q|5.13|; 
$noresave{$key} = "$nosave";

$key = q/eq:e9f/;
$external_latex_labels{$key} = q|11.55|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:a1/;
$external_latex_labels{$key} = q|13.20|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:temperatureDlessModifiedA/;
$external_latex_labels{$key} = q|4.57|; 
$noresave{$key} = "$nosave";

$key = q/varibleArea:fig:superSonicTunnel/;
$external_latex_labels{$key} = q|6.4|; 
$noresave{$key} = "$nosave";

$key = q/pm:fig:overMaxAngle/;
$external_latex_labels{$key} = q|14.5|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:eq:difinitionRatio/;
$external_latex_labels{$key} = q|6.4|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:P0ratio/;
$external_latex_labels{$key} = q|10.19|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:densityMx/;
$external_latex_labels{$key} = q|5.24|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:stationaryMy/;
$external_latex_labels{$key} = q|5.55|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:veloctyRadious/;
$external_latex_labels{$key} = q|14.29|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:Vtimea/;
$external_latex_labels{$key} = q|12.2|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:momentumC/;
$external_latex_labels{$key} = q|5.33|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:ShockMoveA/;
$external_latex_labels{$key} = q|5.9|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:PVlinear/;
$external_latex_labels{$key} = q|11.42|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:TstarTzero/;
$external_latex_labels{$key} = q|4.13|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:massBallanceInt/;
$external_latex_labels{$key} = q|11.24|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:rhoRa/;
$external_latex_labels{$key} = q|10.17|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:maxM1fld2/;
$external_latex_labels{$key} = q|9.17|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isentropicDiff/;
$external_latex_labels{$key} = q|4.37|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:temperatureDlessModified/;
$external_latex_labels{$key} = q|4.56|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:PzeroReduced/;
$external_latex_labels{$key} = q|4.21|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:gibbs/;
$external_latex_labels{$key} = q|4.119|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:CV/;
$external_latex_labels{$key} = q|8.1|; 
$noresave{$key} = "$nosave";

$key = q/shock:list:mvshock/;
$external_latex_labels{$key} = q|b|; 
$noresave{$key} = "$nosave";

$key = q/sub_shock:fig:OpenSuddenlyConvergence1.3/;
$external_latex_labels{$key} = q|(b)|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:sol/;
$external_latex_labels{$key} = q|13.12|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:sondLiquid/;
$external_latex_labels{$key} = q|3.36|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:rU-kMP/;
$external_latex_labels{$key} = q|4.42|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:inlet/;
$external_latex_labels{$key} = q|13.18|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:isentripicRetio/;
$external_latex_labels{$key} = q|11.14|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:massTheta1/;
$external_latex_labels{$key} = q|14.13|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:thetaAlpha/;
$external_latex_labels{$key} = q|13.6|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:deltaFMonly/;
$external_latex_labels{$key} = q|13.65|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:RzeroReduced2/;
$external_latex_labels{$key} = q|4.22|; 
$noresave{$key} = "$nosave";

$key = q/oblique:tab:max/;
$external_latex_labels{$key} = q|13.1|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:mnu1/;
$external_latex_labels{$key} = q|14.1|; 
$noresave{$key} = "$nosave";

$key = q/piston:fig:vacuum/;
$external_latex_labels{$key} = q|12.3|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:workingEq/;
$external_latex_labels{$key} = q|8.28|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:PstarPzero1/;
$external_latex_labels{$key} = q|4.14|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:RzeroReduced/;
$external_latex_labels{$key} = q|4.23|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:massMon5/;
$external_latex_labels{$key} = q|7.10|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:a3/;
$external_latex_labels{$key} = q|13.22|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govDless3/;
$external_latex_labels{$key} = q|11.9|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Fn-funRho0P/;
$external_latex_labels{$key} = q|4.69|; 
$noresave{$key} = "$nosave";

$key = q/tank:fig:rigida/;
$external_latex_labels{$key} = q|11.6|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:PratioOld/;
$external_latex_labels{$key} = q|4.83|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:RgasSide/;
$external_latex_labels{$key} = q|3.39|; 
$noresave{$key} = "$nosave";

$key = q/chap:shockVariable/;
$external_latex_labels{$key} = q|6|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:nonDimMass/;
$external_latex_labels{$key} = q|5.12|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:starMtoM/;
$external_latex_labels{$key} = q|5.32|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:mainT-P/;
$external_latex_labels{$key} = q|5.4|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:horizontalLine/;
$external_latex_labels{$key} = q|14.4|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:PzeroD/;
$external_latex_labels{$key} = q|8.16|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:HD/;
$external_latex_labels{$key} = q|9.8|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:compressedGasD/;
$external_latex_labels{$key} = q|4.116|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:nuInfty/;
$external_latex_labels{$key} = q|14.33|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:mvU2simplified/;
$external_latex_labels{$key} = q|5.89|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:openedDeltaT/;
$external_latex_labels{$key} = q|5.61|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:energyNonIdeal/;
$external_latex_labels{$key} = q|4.121|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:CloseValveUx/;
$external_latex_labels{$key} = q|5.78|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:toBeSolved/;
$external_latex_labels{$key} = q|5.19|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:solRigitFannoFastReversed/;
$external_latex_labels{$key} = q|11.35|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:stagnationMachNumber/;
$external_latex_labels{$key} = q|4.130|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:develpSub/;
$external_latex_labels{$key} = q|9.7|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:fannoFun/;
$external_latex_labels{$key} = q|1.4|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:entroyS/;
$external_latex_labels{$key} = q|9.35|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:PstarPzero/;
$external_latex_labels{$key} = q|4.15|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:massR/;
$external_latex_labels{$key} = q|5.87|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:mass/;
$external_latex_labels{$key} = q|10.3|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:stagnationSoundSpeed/;
$external_latex_labels{$key} = q|5.28|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:MomMassImp/;
$external_latex_labels{$key} = q|4.106|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:P-M/;
$external_latex_labels{$key} = q|9.14|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dP-dM/;
$external_latex_labels{$key} = q|9.39|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:openValveExBB/;
$external_latex_labels{$key} = q|5.21|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:tangentU/;
$external_latex_labels{$key} = q|13.3|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:temp-M1/;
$external_latex_labels{$key} = q|4.38|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:energy/;
$external_latex_labels{$key} = q|11.19|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:combineDimMassEnergy/;
$external_latex_labels{$key} = q|5.18|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:deLavalNozzle/;
$external_latex_labels{$key} = q|1.2|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:RdeltaZero/;
$external_latex_labels{$key} = q|13.48|; 
$noresave{$key} = "$nosave";

$key = q/oblique:tab:delataZero/;
$external_latex_labels{$key} = q|13.7|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:entropy/;
$external_latex_labels{$key} = q|9.13|; 
$noresave{$key} = "$nosave";

$key = q/shock:tab:suddenlyClosed/;
$external_latex_labels{$key} = q|5.2|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:M1t/;
$external_latex_labels{$key} = q|13.10|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:fliegner/;
$external_latex_labels{$key} = q|4.49|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dP-fld/;
$external_latex_labels{$key} = q|9.24|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:momentumD1aa/;
$external_latex_labels{$key} = q|9.16|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:seperatedSemiRigid/;
$external_latex_labels{$key} = q|11.45|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:main/;
$external_latex_labels{$key} = q|5.3|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:OpenSuddenlyConvergence0.3/;
$external_latex_labels{$key} = q|5.13(a)|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:momentumD1a/;
$external_latex_labels{$key} = q|9.12|; 
$noresave{$key} = "$nosave";

$key = q/chap:Prandtl-Meyer/;
$external_latex_labels{$key} = q|14|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MsxPistonGeneral/;
$external_latex_labels{$key} = q|5.65|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:Uthetac/;
$external_latex_labels{$key} = q|14.22|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:tanMU/;
$external_latex_labels{$key} = q|14.6|; 
$noresave{$key} = "$nosave";

$key = q/fanno:tab:basic/;
$external_latex_labels{$key} = q|9.1|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:thermo2/;
$external_latex_labels{$key} = q|4.28|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:gamma1/;
$external_latex_labels{$key} = q|3.47|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoMomentum/;
$external_latex_labels{$key} = q|4.77|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:cvMomentumEx/;
$external_latex_labels{$key} = q|3.8|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Fn-funF/;
$external_latex_labels{$key} = q|4.64|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:M-A/;
$external_latex_labels{$key} = q|4.43|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoMass/;
$external_latex_labels{$key} = q|4.86|; 
$noresave{$key} = "$nosave";

$key = q/varialbeArea:eq:bernnolliZP/;
$external_latex_labels{$key} = q|4.133|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:M-Tzn_1/;
$external_latex_labels{$key} = q|4.147|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:largeM1theta/;
$external_latex_labels{$key} = q|13.62|; 
$noresave{$key} = "$nosave";

$key = q/tank:fig:twoModel/;
$external_latex_labels{$key} = q|11.1|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:cvCombined2/;
$external_latex_labels{$key} = q|3.5|; 
$noresave{$key} = "$nosave";

$key = q/sub_shock:fig:partialyOpenMv/;
$external_latex_labels{$key} = q|(b)|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:Tbar/;
$external_latex_labels{$key} = q|13.15|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:relationshipP-rho/;
$external_latex_labels{$key} = q|4.36|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:DrhoTOP/;
$external_latex_labels{$key} = q|3.30|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govDless5/;
$external_latex_labels{$key} = q|11.13|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:ditachment/;
$external_latex_labels{$key} = q|13.64|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:solidExampl/;
$external_latex_labels{$key} = q|3.38|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:combAllR/;
$external_latex_labels{$key} = q|5.35|; 
$noresave{$key} = "$nosave";

$key = q/veriableAreaeq:MassFlowRatez/;
$external_latex_labels{$key} = q|4.150|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:PfunFLDb/;
$external_latex_labels{$key} = q|9.12|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:thetaMax/;
$external_latex_labels{$key} = q|13.38|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:fig:M1-fld/;
$external_latex_labels{$key} = q|8.3|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Mz/;
$external_latex_labels{$key} = q|4.144|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:nuTheta/;
$external_latex_labels{$key} = q|14.35|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:a2/;
$external_latex_labels{$key} = q|13.21|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Uzn_1/;
$external_latex_labels{$key} = q|4.135|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:pressureMsx/;
$external_latex_labels{$key} = q|5.70|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:energy/;
$external_latex_labels{$key} = q|5.3|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:fliegnerAir/;
$external_latex_labels{$key} = q|4.51|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:Mmax-kFanno/;
$external_latex_labels{$key} = q|9.5|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:massMom/;
$external_latex_labels{$key} = q|14.18|; 
$noresave{$key} = "$nosave";

$key = q/tank:fig:rigid/;
$external_latex_labels{$key} = q|11.5|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:Tratioa/;
$external_latex_labels{$key} = q|10.16|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:energyDEa/;
$external_latex_labels{$key} = q|14.27|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:reducedDensity/;
$external_latex_labels{$key} = q|13.61|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:massTheta/;
$external_latex_labels{$key} = q|14.14|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:mass/;
$external_latex_labels{$key} = q|7.4|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:Punarranged/;
$external_latex_labels{$key} = q|5.15|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Mach/;
$external_latex_labels{$key} = q|4.8|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:movingShockDrag/;
$external_latex_labels{$key} = q|13.22|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:fig:impulse/;
$external_latex_labels{$key} = q|4.9|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:momentumD1/;
$external_latex_labels{$key} = q|9.11|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:stagnationTempDiff2/;
$external_latex_labels{$key} = q|5.53|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:maxUisotermalNozzle/;
$external_latex_labels{$key} = q|4.96|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:bullet/;
$external_latex_labels{$key} = q|13.13|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govDless4/;
$external_latex_labels{$key} = q|11.10|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:insontropic2/;
$external_latex_labels{$key} = q|3.7|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:temperaturePbar/;
$external_latex_labels{$key} = q|5.25|; 
$noresave{$key} = "$nosave";

$key = q/pm:fig:pmResults/;
$external_latex_labels{$key} = q|14.6|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:sndRlawExDrivative/;
$external_latex_labels{$key} = q|10.12|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:prandtl/;
$external_latex_labels{$key} = q|1.11|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:CpCvRN/;
$external_latex_labels{$key} = q|4.4|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:TrhoSolution/;
$external_latex_labels{$key} = q|4.60|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:gibbsCp/;
$external_latex_labels{$key} = q|4.120|; 
$noresave{$key} = "$nosave";

$key = q/chap:fanno/;
$external_latex_labels{$key} = q|9|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:RankineHugoniot/;
$external_latex_labels{$key} = q|13.17|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:mom2/;
$external_latex_labels{$key} = q|14.12|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:rhoR/;
$external_latex_labels{$key} = q|10.7|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:MassFlowRatenonIdeal/;
$external_latex_labels{$key} = q|4.128|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:max/;
$external_latex_labels{$key} = q|13.17|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:TOTtmp/;
$external_latex_labels{$key} = q|8.18|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:shockStrength/;
$external_latex_labels{$key} = q|5.39|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:state/;
$external_latex_labels{$key} = q|9.5|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:starMach/;
$external_latex_labels{$key} = q|5.31|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dPoP/;
$external_latex_labels{$key} = q|9.23|; 
$noresave{$key} = "$nosave";

$key = q/chap:Isothermal/;
$external_latex_labels{$key} = q|8|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:emanuel/;
$external_latex_labels{$key} = q|13.31|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:inletEx/;
$external_latex_labels{$key} = q|13.19|; 
$noresave{$key} = "$nosave";

$key = q/varialbeArea:eq:bernnolliZ/;
$external_latex_labels{$key} = q|4.132|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:rayleigh/;
$external_latex_labels{$key} = q|1.8|; 
$noresave{$key} = "$nosave";

$key = q/piston:fig:fig3/;
$external_latex_labels{$key} = q|12.4|; 
$noresave{$key} = "$nosave";

$key = q/varibleShock:eq:mvMs/;
$external_latex_labels{$key} = q|5.83|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:massFlowRate/;
$external_latex_labels{$key} = q|4.44|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:CloseValveUy/;
$external_latex_labels{$key} = q|5.79|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:math:m2express2/;
$external_latex_labels{$key} = q|8.44|; 
$noresave{$key} = "$nosave";

$key = q/chap:program/;
$external_latex_labels{$key} = q|A|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:Tratio/;
$external_latex_labels{$key} = q|10.6|; 
$noresave{$key} = "$nosave";

$key = q/shock:item:CloseValvePa/;
$external_latex_labels{$key} = q|b|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:PzeroReduced1/;
$external_latex_labels{$key} = q|4.17|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:massNonD1/;
$external_latex_labels{$key} = q|5.10|; 
$noresave{$key} = "$nosave";

$key = q/chap:sound/;
$external_latex_labels{$key} = q|3|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:dless-T/;
$external_latex_labels{$key} = q|11.4a|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:2ndLaw/;
$external_latex_labels{$key} = q|10.10|; 
$noresave{$key} = "$nosave";

$key = q/variableArea_eq:massPAequal/;
$external_latex_labels{$key} = q|4.71|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:S/;
$external_latex_labels{$key} = q|13.26|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:HDdef/;
$external_latex_labels{$key} = q|9.7|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:isoChokingPoint/;
$external_latex_labels{$key} = q|7.11|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:integralMach/;
$external_latex_labels{$key} = q|8.27|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:noInclation/;
$external_latex_labels{$key} = q|13.1|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:solutionIntergral/;
$external_latex_labels{$key} = q|12.10|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:thermo/;
$external_latex_labels{$key} = q|4.27|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:solution2/;
$external_latex_labels{$key} = q|5.22|; 
$noresave{$key} = "$nosave";

$key = q/eq::varibleArea:combine2/;
$external_latex_labels{$key} = q|4.31|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:M1/;
$external_latex_labels{$key} = q|13.59|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:HDdef/;
$external_latex_labels{$key} = q|8.7|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:temperatureDless/;
$external_latex_labels{$key} = q|4.9|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:massMon0/;
$external_latex_labels{$key} = q|7.5|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:PfunFLDa/;
$external_latex_labels{$key} = q|9.11|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:momentum/;
$external_latex_labels{$key} = q|5.2|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:M2/;
$external_latex_labels{$key} = q|13.57|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:fanno/;
$external_latex_labels{$key} = q|1.10|; 
$noresave{$key} = "$nosave";

$key = q/pm:fig:angleDef/;
$external_latex_labels{$key} = q|14.1|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:M-Tzcriticaln_1/;
$external_latex_labels{$key} = q|4.149|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:M-A-0/;
$external_latex_labels{$key} = q|4.41|; 
$noresave{$key} = "$nosave";

$key = q/pm:fig:coor/;
$external_latex_labels{$key} = q|14.4|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:state/;
$external_latex_labels{$key} = q|8.5|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:averageVelocity/;
$external_latex_labels{$key} = q|3.40|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:stateDless/;
$external_latex_labels{$key} = q|4.29|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:stabiltyShock/;
$external_latex_labels{$key} = q|13.24|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:Rindenty/;
$external_latex_labels{$key} = q|5.14|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:t2t1a/;
$external_latex_labels{$key} = q|10.8|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:cv_flow/;
$external_latex_labels{$key} = q|9.1|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govDless6/;
$external_latex_labels{$key} = q|11.17|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:eq:idealGasDifuserEfficiency/;
$external_latex_labels{$key} = q|6.8|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:conversionT0T/;
$external_latex_labels{$key} = q|5.49|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:math:def/;
$external_latex_labels{$key} = q|8.42|; 
$noresave{$key} = "$nosave";

$key = q/sound:fig:stationary/;
$external_latex_labels{$key} = q|3.2|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:speedSoundNonIdealGas/;
$external_latex_labels{$key} = q|3.34|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:x1/;
$external_latex_labels{$key} = q|13.23|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:massMon3/;
$external_latex_labels{$key} = q|7.8|; 
$noresave{$key} = "$nosave";

$key = q/shock:ex:openValveBB/;
$external_latex_labels{$key} = q|5.11|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:temperatureRho/;
$external_latex_labels{$key} = q|4.59|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:deltaFLD/;
$external_latex_labels{$key} = q|9.51|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:compressedGas/;
$external_latex_labels{$key} = q|3.19|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:genGovSemiRigit/;
$external_latex_labels{$key} = q|11.48|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:subFLDP2P1/;
$external_latex_labels{$key} = q|9.19|; 
$noresave{$key} = "$nosave";

$key = q/sub_variableArea:fig:compareNozzle/;
$external_latex_labels{$key} = q|(a)|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:reverseTheta/;
$external_latex_labels{$key} = q|14.32|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:energyDerivative/;
$external_latex_labels{$key} = q|9.4|; 
$noresave{$key} = "$nosave";

$key = q/eq:math2/;
$external_latex_labels{$key} = q|8.47|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:crPbar/;
$external_latex_labels{$key} = q|4.140|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:openValveEx/;
$external_latex_labels{$key} = q|5.23|; 
$noresave{$key} = "$nosave";

$key = q/chap:forced/;
$external_latex_labels{$key} = q|7|; 
$noresave{$key} = "$nosave";

$key = q/ray:fig:basic/;
$external_latex_labels{$key} = q|10.3|; 
$noresave{$key} = "$nosave";

$key = q/sub_shock:fig:suddenlyOpenSt/;
$external_latex_labels{$key} = q|(a)|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:mu2/;
$external_latex_labels{$key} = q|14.2|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:massMomOFS/;
$external_latex_labels{$key} = q|5.34|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:pressureGov/;
$external_latex_labels{$key} = q|8.24|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:solution/;
$external_latex_labels{$key} = q|9.38|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:seperatedSemiRigida/;
$external_latex_labels{$key} = q|11.50|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:Pratio/;
$external_latex_labels{$key} = q|9.40|; 
$noresave{$key} = "$nosave";

$key = q/fanno:ex:ex2/;
$external_latex_labels{$key} = q|9.2|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:maxKSuddenlyOpen/;
$external_latex_labels{$key} = q|5.14|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:vanWylen/;
$external_latex_labels{$key} = q|3.23|; 
$noresave{$key} = "$nosave";

$key = q/sub_shock:fig:suddenlyOpenMv/;
$external_latex_labels{$key} = q|(b)|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:shock/;
$external_latex_labels{$key} = q|1.1|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:stateD/;
$external_latex_labels{$key} = q|8.20|; 
$noresave{$key} = "$nosave";

$key = q/ch:fluid/;
$external_latex_labels{$key} = q|2|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:bernolliSound/;
$external_latex_labels{$key} = q|14.24|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:dless-V/;
$external_latex_labels{$key} = q|11.4b|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:T0T/;
$external_latex_labels{$key} = q|4.6|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:M-Tz/;
$external_latex_labels{$key} = q|4.146|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:U3-2/;
$external_latex_labels{$key} = q|5.96|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:sndRlawEx/;
$external_latex_labels{$key} = q|10.11|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:sharpNose/;
$external_latex_labels{$key} = q|13.16|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:Rhoratio/;
$external_latex_labels{$key} = q|9.42|; 
$noresave{$key} = "$nosave";

$key = q/oblique:ex:inletEx/;
$external_latex_labels{$key} = q|13.4|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:MachReflection/;
$external_latex_labels{$key} = q|13.15|; 
$noresave{$key} = "$nosave";

$key = q/shock:tab:suddenlyOpened1.3/;
$external_latex_labels{$key} = q|5.4|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:generalSolution/;
$external_latex_labels{$key} = q|5.21|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:increaseFLD/;
$external_latex_labels{$key} = q|9.6|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:eq:maxEnergy/;
$external_latex_labels{$key} = q|6.1|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:smallPresSolution/;
$external_latex_labels{$key} = q|8.52|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:energyDE0/;
$external_latex_labels{$key} = q|14.25|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoAratioStar/;
$external_latex_labels{$key} = q|4.90|; 
$noresave{$key} = "$nosave";

$key = q/pm:fig:dalembert/;
$external_latex_labels{$key} = q|14.7|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:weakThompson/;
$external_latex_labels{$key} = q|13.32|; 
$noresave{$key} = "$nosave";

$key = q/pm:fig:angleDefa/;
$external_latex_labels{$key} = q|14.8|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:whatFn2/;
$external_latex_labels{$key} = q|4.67|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:mass/;
$external_latex_labels{$key} = q|5.1|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:fig:basic/;
$external_latex_labels{$key} = q|8.2|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Fn-funFS/;
$external_latex_labels{$key} = q|4.66|; 
$noresave{$key} = "$nosave";

$key = q/pm:ex:reversed/;
$external_latex_labels{$key} = q|14.2|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:fMsimple/;
$external_latex_labels{$key} = q|11.22|; 
$noresave{$key} = "$nosave";

$key = q/pm:fig:simpleExample/;
$external_latex_labels{$key} = q|14.9|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:dragG/;
$external_latex_labels{$key} = q|14.38|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:flowView/;
$external_latex_labels{$key} = q|13.5|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:starSpeedSound/;
$external_latex_labels{$key} = q|5.30|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MsxPistonLimit/;
$external_latex_labels{$key} = q|5.68|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:cvEnergy1/;
$external_latex_labels{$key} = q|3.3|; 
$noresave{$key} = "$nosave";

$key = q/tank:fig:twoConnection/;
$external_latex_labels{$key} = q|11.2|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Ucomparison/;
$external_latex_labels{$key} = q|4.99|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:CpCvR/;
$external_latex_labels{$key} = q|4.3|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:T0ratio/;
$external_latex_labels{$key} = q|10.18|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Fnz/;
$external_latex_labels{$key} = q|4.138|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:unPr/;
$external_latex_labels{$key} = q|5.36|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:fig:isoBasic/;
$external_latex_labels{$key} = q|4.6|; 
$noresave{$key} = "$nosave";

$key = q/piston:fig:atmos/;
$external_latex_labels{$key} = q|12.2|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:fmG/;
$external_latex_labels{$key} = q|4.54|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:soundDless/;
$external_latex_labels{$key} = q|11.7|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:isonropic/;
$external_latex_labels{$key} = q|3.43|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:momentumD/;
$external_latex_labels{$key} = q|8.10|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoRatio/;
$external_latex_labels{$key} = q|4.112|; 
$noresave{$key} = "$nosave";

$key = q/shock:item:openValvePa/;
$external_latex_labels{$key} = q|b|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:Msolution1/;
$external_latex_labels{$key} = q|5.20|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:partialyOpenSt/;
$external_latex_labels{$key} = q|5.15(a)|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:U3U2/;
$external_latex_labels{$key} = q|5.97|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:totalPressureRatio/;
$external_latex_labels{$key} = q|5.17|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:eckert/;
$external_latex_labels{$key} = q|1.12|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:f/;
$external_latex_labels{$key} = q|8.8|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:initialCondition/;
$external_latex_labels{$key} = q|12.11|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govsemiRigid1/;
$external_latex_labels{$key} = q|11.44|; 
$noresave{$key} = "$nosave";

$key = q/eq:math:series/;
$external_latex_labels{$key} = q|8.46|; 
$noresave{$key} = "$nosave";

$key = q/computer:fig:structure/;
$external_latex_labels{$key} = q|A.1|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:P-M1/;
$external_latex_labels{$key} = q|9.15|; 
$noresave{$key} = "$nosave";

$key = q/shock:ex:twoPistons/;
$external_latex_labels{$key} = q|5.9|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:relationshipU-rho/;
$external_latex_labels{$key} = q|4.35|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:massMon4/;
$external_latex_labels{$key} = q|7.9|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:x2/;
$external_latex_labels{$key} = q|13.24|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:gibbsCp/;
$external_latex_labels{$key} = q|3.27|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:eq:idealGasDifuserEfficiencyIni/;
$external_latex_labels{$key} = q|6.7|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:TokerBullet/;
$external_latex_labels{$key} = q|13.4|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MyfMx/;
$external_latex_labels{$key} = q|5.57|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Fn-funFSa/;
$external_latex_labels{$key} = q|4.68|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:twoPistons/;
$external_latex_labels{$key} = q|5.19|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:partialyOpenValve/;
$external_latex_labels{$key} = q|5.17|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:nozzleComparison/;
$external_latex_labels{$key} = q|4.7|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:t2t1b/;
$external_latex_labels{$key} = q|10.9|; 
$noresave{$key} = "$nosave";

$key = q/sound:tbl:solidSpeed/;
$external_latex_labels{$key} = q|3.6|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:massFlowRateRatio/;
$external_latex_labels{$key} = q|4.48|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:bernoulliStagnation1/;
$external_latex_labels{$key} = q|4.80|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:anotherMtoU/;
$external_latex_labels{$key} = q|8.12|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:tmaxtc/;
$external_latex_labels{$key} = q|12.14|; 
$noresave{$key} = "$nosave";

$key = q/oblique:ex:tunnel2deflection/;
$external_latex_labels{$key} = q|13.13|; 
$noresave{$key} = "$nosave";

$key = q/iso:fig:cv/;
$external_latex_labels{$key} = q|8.1|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:massMom1/;
$external_latex_labels{$key} = q|14.19|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:fliegnerk/;
$external_latex_labels{$key} = q|4.50|; 
$noresave{$key} = "$nosave";

$key = q/ray:tab:basic/;
$external_latex_labels{$key} = q|10.1|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:massFlowRateID/;
$external_latex_labels{$key} = q|4.127|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:fig:cv/;
$external_latex_labels{$key} = q|4.4|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:partialyCloseMv/;
$external_latex_labels{$key} = q|5.17(b)|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:epsilonF/;
$external_latex_labels{$key} = q|13.43|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:fig:nozzle/;
$external_latex_labels{$key} = q|6.1|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoRatioIdealgas/;
$external_latex_labels{$key} = q|4.113|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MyfMxs/;
$external_latex_labels{$key} = q|5.58|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:tmp3/;
$external_latex_labels{$key} = q|9.21|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:M1n/;
$external_latex_labels{$key} = q|13.8|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:fig:difsure_efficiency/;
$external_latex_labels{$key} = q|6.3|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:massNonD0/;
$external_latex_labels{$key} = q|5.9|; 
$noresave{$key} = "$nosave";

$key = q/varibleShock:eq:mvUbar/;
$external_latex_labels{$key} = q|5.85|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:mom2Theta1/;
$external_latex_labels{$key} = q|14.16|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:T2T1R/;
$external_latex_labels{$key} = q|5.99|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:pressureDless/;
$external_latex_labels{$key} = q|4.11|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:energy/;
$external_latex_labels{$key} = q|14.23|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:CloseValveMy/;
$external_latex_labels{$key} = q|5.81|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:Pbar/;
$external_latex_labels{$key} = q|8.32|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:PeffectsM-asFun-frac__32924__4fL__32924____32925__D__32925__/;
$external_latex_labels{$key} = q|9.13|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:general/;
$external_latex_labels{$key} = q|13.3|; 
$noresave{$key} = "$nosave";

$key = q/oblique:ex:throat/;
$external_latex_labels{$key} = q|13.5|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:MBAdiabatic/;
$external_latex_labels{$key} = q|11.20|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:TxTyInformal/;
$external_latex_labels{$key} = q|5.7|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:energy/;
$external_latex_labels{$key} = q|13.4|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:mass/;
$external_latex_labels{$key} = q|8.4|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:ex2/;
$external_latex_labels{$key} = q|9.4|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:mass/;
$external_latex_labels{$key} = q|9.2|; 
$noresave{$key} = "$nosave";

$key = q/sound:fig:piston/;
$external_latex_labels{$key} = q|3.1|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:monImp/;
$external_latex_labels{$key} = q|4.104|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:tmp1a/;
$external_latex_labels{$key} = q|9.19|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:prandtl/;
$external_latex_labels{$key} = q|13.16|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:adiabatic/;
$external_latex_labels{$key} = q|14.7|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:fannoGraph/;
$external_latex_labels{$key} = q|9.2|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:nu/;
$external_latex_labels{$key} = q|14.9|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:machsSuddenStop/;
$external_latex_labels{$key} = q|5.11|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:gov/;
$external_latex_labels{$key} = q|12.3|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:maxM1fld1/;
$external_latex_labels{$key} = q|9.16|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:intersection/;
$external_latex_labels{$key} = q|5.2|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:rankine/;
$external_latex_labels{$key} = q|1.9|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MyLimit/;
$external_latex_labels{$key} = q|5.38|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoTzeroR1/;
$external_latex_labels{$key} = q|4.85|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:PstarPzeroINI/;
$external_latex_labels{$key} = q|4.97|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoIdealgas/;
$external_latex_labels{$key} = q|4.76|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:beforeDefa/;
$external_latex_labels{$key} = q|4.107|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:stateMassD/;
$external_latex_labels{$key} = q|8.22|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:strongThompson/;
$external_latex_labels{$key} = q|13.33|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:PrDless/;
$external_latex_labels{$key} = q|5.37|; 
$noresave{$key} = "$nosave";

$key = q/sub_shock:fig:partialyCloseSt/;
$external_latex_labels{$key} = q|(a)|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:rhoBar/;
$external_latex_labels{$key} = q|13.14|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:eq:nozzleEfficiency/;
$external_latex_labels{$key} = q|6.3|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:shockPartialOpen/;
$external_latex_labels{$key} = q|5.16|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:memSimple/;
$external_latex_labels{$key} = q|3.10|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoTzeroR/;
$external_latex_labels{$key} = q|4.88|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:tempSimple/;
$external_latex_labels{$key} = q|4.5|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:CloseValveMx/;
$external_latex_labels{$key} = q|5.80|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:exitU/;
$external_latex_labels{$key} = q|4.79|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dT_0dT/;
$external_latex_labels{$key} = q|9.32|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:actualCalM-FLD_0.3/;
$external_latex_labels{$key} = q|9.14|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:press-M/;
$external_latex_labels{$key} = q|4.40|; 
$noresave{$key} = "$nosave";

$key = q/variableShock:ex:tank/;
$external_latex_labels{$key} = q|6.1|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Ucomparison1/;
$external_latex_labels{$key} = q|4.100|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:solutionChemical/;
$external_latex_labels{$key} = q|12.12|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:govDxi1/;
$external_latex_labels{$key} = q|12.6|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:movingShockDrag2/;
$external_latex_labels{$key} = q|5.8|; 
$noresave{$key} = "$nosave";

$key = q/math:eq:binomal/;
$external_latex_labels{$key} = q|4.16|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:Vtime/;
$external_latex_labels{$key} = q|12.1|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:suddenlyOpenMv/;
$external_latex_labels{$key} = q|5.12(b)|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:stateDa/;
$external_latex_labels{$key} = q|8.6|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:subBranch/;
$external_latex_labels{$key} = q|9.18|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:timeFilling/;
$external_latex_labels{$key} = q|11.27|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoPratioStar/;
$external_latex_labels{$key} = q|4.89|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:RR/;
$external_latex_labels{$key} = q|13.54|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:OpenSuddenlyConvergence1.3/;
$external_latex_labels{$key} = q|5.13(b)|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:momentumD1b/;
$external_latex_labels{$key} = q|9.17|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoPzeroRatioStar/;
$external_latex_labels{$key} = q|4.92|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isentropicR/;
$external_latex_labels{$key} = q|4.122|; 
$noresave{$key} = "$nosave";

$key = q/ray:fig:Ts/;
$external_latex_labels{$key} = q|10.2|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:P0P0starmid/;
$external_latex_labels{$key} = q|9.44|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:state/;
$external_latex_labels{$key} = q|10.4|; 
$noresave{$key} = "$nosave";

$key = q/oblique:sec:movingShockDrag/;
$external_latex_labels{$key} = q|13.5.5|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govEq/;
$external_latex_labels{$key} = q|11.3|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:entropyA/;
$external_latex_labels{$key} = q|9.33|; 
$noresave{$key} = "$nosave";

$key = q/chap:oblique/;
$external_latex_labels{$key} = q|13|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:pressureRatio/;
$external_latex_labels{$key} = q|5.16|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:smallMaxDefA1/;
$external_latex_labels{$key} = q|13.46|; 
$noresave{$key} = "$nosave";

$key = q/shock:sec:movingShockDrag/;
$external_latex_labels{$key} = q|5.3.1|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:nozzlePressure/;
$external_latex_labels{$key} = q|1.3|; 
$noresave{$key} = "$nosave";

$key = q/fanno:ex:reservoir/;
$external_latex_labels{$key} = q|9.1|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:RzeroReduced1/;
$external_latex_labels{$key} = q|4.18|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:Pratio/;
$external_latex_labels{$key} = q|10.5|; 
$noresave{$key} = "$nosave";

$key = q/tank:fig:model/;
$external_latex_labels{$key} = q|11.3|; 
$noresave{$key} = "$nosave";

1;

