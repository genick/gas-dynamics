# LaTeX2HTML 2002-2-1 (1.70)
# Associate internals original text with physical files.


$key = q/shock:fig:shockDrag/;
$ref_files{$key} = "$dir".q|node105.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:delta-thetaA/;
$ref_files{$key} = "$dir".q|node204.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:MachDef/;
$ref_files{$key} = "$dir".q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:fig:comperTandP/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:govD/;
$ref_files{$key} = "$dir".q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:mach/;
$ref_files{$key} = "$dir".q|node54.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:yetMtoUfunD/;
$ref_files{$key} = "$dir".q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:PdP2/;
$ref_files{$key} = "$dir".q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:APtotal/;
$ref_files{$key} = "$dir".q|node85.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:fig:reversed/;
$ref_files{$key} = "$dir".q|node226.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:fig:basic/;
$ref_files{$key} = "$dir".q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:piston/;
$ref_files{$key} = "$dir".q|node186.html|; 
$noresave{$key} = "$nosave";

$key = q/variableShock:ex:superSonicTunnel/;
$ref_files{$key} = "$dir".q|node124.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:openValvePipeA/;
$ref_files{$key} = "$dir".q|node120.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:MstarReduced/;
$ref_files{$key} = "$dir".q|node77.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:X/;
$ref_files{$key} = "$dir".q|node73.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:tc/;
$ref_files{$key} = "$dir".q|node171.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:PvPT/;
$ref_files{$key} = "$dir".q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/fluid:eq:dEta/;
$ref_files{$key} = "$dir".q|node65.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:cv/;
$ref_files{$key} = "$dir".q|node94.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:tab:basic/;
$ref_files{$key} = "$dir".q|node134.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:fig:nozzle/;
$ref_files{$key} = "$dir".q|node74.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Mz_1/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:sec:PartiallyCloseValve/;
$ref_files{$key} = "$dir".q|node114.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:M2t/;
$ref_files{$key} = "$dir".q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:densityPbar/;
$ref_files{$key} = "$dir".q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:solSemiRigid/;
$ref_files{$key} = "$dir".q|node183.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:mass/;
$ref_files{$key} = "$dir".q|node170.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MypMax/;
$ref_files{$key} = "$dir".q|node112.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:solutionXi1/;
$ref_files{$key} = "$dir".q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:impulsDefStar/;
$ref_files{$key} = "$dir".q|node90.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:combine1/;
$ref_files{$key} = "$dir".q|node79.html|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:massMon1/;
$ref_files{$key} = "$dir".q|node126.html|; 
$noresave{$key} = "$nosave";

$key = q/obalique:fig:detachWedge/;
$ref_files{$key} = "$dir".q|node213.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:densityMsx/;
$ref_files{$key} = "$dir".q|node111.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:mem/;
$ref_files{$key} = "$dir".q|node68.html|; 
$noresave{$key} = "$nosave";

$key = q/thermo:eq:maxwell/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:openValvePipeC/;
$ref_files{$key} = "$dir".q|node120.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dPzero-fld/;
$ref_files{$key} = "$dir".q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:solRigitFannoFast/;
$ref_files{$key} = "$dir".q|node180.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:paritalyOpenUSa/;
$ref_files{$key} = "$dir".q|node113.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:x3/;
$ref_files{$key} = "$dir".q|node200.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:M-Tzcritical/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:solD/;
$ref_files{$key} = "$dir".q|node202.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:rhoToP/;
$ref_files{$key} = "$dir".q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:combinedState/;
$ref_files{$key} = "$dir".q|node73.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoPzeroRatioStarMax/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:workingFLDAppSol/;
$ref_files{$key} = "$dir".q|node131.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:shock/;
$ref_files{$key} = "$dir".q|node94.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:acceleratingPiston/;
$ref_files{$key} = "$dir".q|node115.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:sound/;
$ref_files{$key} = "$dir".q|node69.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:Ubar/;
$ref_files{$key} = "$dir".q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:Mtheta/;
$ref_files{$key} = "$dir".q|node221.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:solRigitFannoFastb/;
$ref_files{$key} = "$dir".q|node181.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:P0R/;
$ref_files{$key} = "$dir".q|node204.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:massFlowRate1/;
$ref_files{$key} = "$dir".q|node83.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govsemiRigid/;
$ref_files{$key} = "$dir".q|node183.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:x/;
$ref_files{$key} = "$dir".q|node200.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:PoverT/;
$ref_files{$key} = "$dir".q|node172.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:improvedFLD/;
$ref_files{$key} = "$dir".q|node159.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:isothermalNozzlea/;
$ref_files{$key} = "$dir".q|node178.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:fig:chamber/;
$ref_files{$key} = "$dir".q|node170.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dRdU/;
$ref_files{$key} = "$dir".q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:fig:machLineAngle/;
$ref_files{$key} = "$dir".q|node219.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dA_w/;
$ref_files{$key} = "$dir".q|node139.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:fnA/;
$ref_files{$key} = "$dir".q|node83.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:ex:largeFLD-SS/;
$ref_files{$key} = "$dir".q|node157.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:smallMachMaxDeflction/;
$ref_files{$key} = "$dir".q|node202.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:fld2/;
$ref_files{$key} = "$dir".q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:conversionUx/;
$ref_files{$key} = "$dir".q|node106.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:gibbsCpds0/;
$ref_files{$key} = "$dir".q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:pressureMx/;
$ref_files{$key} = "$dir".q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:Cv/;
$ref_files{$key} = "$dir".q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:solution/;
$ref_files{$key} = "$dir".q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:tmaxtcAAC/;
$ref_files{$key} = "$dir".q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:2law/;
$ref_files{$key} = "$dir".q|node73.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:TtoPz1/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:cubic/;
$ref_files{$key} = "$dir".q|node200.html|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:bullet/;
$ref_files{$key} = "$dir".q|node54.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:maxMassFlowRateOpenValve/;
$ref_files{$key} = "$dir".q|node112.html|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:energy3/;
$ref_files{$key} = "$dir".q|node126.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:energyInt/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Fn-fun/;
$ref_files{$key} = "$dir".q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:fig:heatTube/;
$ref_files{$key} = "$dir".q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:example1/;
$ref_files{$key} = "$dir".q|node145.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:conversionMx/;
$ref_files{$key} = "$dir".q|node106.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:tab:suddenlyOpened/;
$ref_files{$key} = "$dir".q|node121.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:areaChangeMach/;
$ref_files{$key} = "$dir".q|node79.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:epiloguePotto/;
$ref_files{$key} = "$dir".q|node12.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:fnDef/;
$ref_files{$key} = "$dir".q|node83.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:maxMa/;
$ref_files{$key} = "$dir".q|node202.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:epsilonFA/;
$ref_files{$key} = "$dir".q|node202.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:TR/;
$ref_files{$key} = "$dir".q|node204.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:reducedPressure/;
$ref_files{$key} = "$dir".q|node205.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoAratio/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:mvU-P-Results/;
$ref_files{$key} = "$dir".q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:fig:cv/;
$ref_files{$key} = "$dir".q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:stangtionMach/;
$ref_files{$key} = "$dir".q|node98.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:subM1P2P1-D/;
$ref_files{$key} = "$dir".q|node160.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:stagnationTempDiff2a/;
$ref_files{$key} = "$dir".q|node108.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:isothermalNozzleInt/;
$ref_files{$key} = "$dir".q|node178.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:momentumD1/;
$ref_files{$key} = "$dir".q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:temp-M/;
$ref_files{$key} = "$dir".q|node80.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:conversionP0P/;
$ref_files{$key} = "$dir".q|node106.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:cvCombined/;
$ref_files{$key} = "$dir".q|node68.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:dTds/;
$ref_files{$key} = "$dir".q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:energy2/;
$ref_files{$key} = "$dir".q|node126.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:fmtheta1/;
$ref_files{$key} = "$dir".q|node201.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Uz/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MsxPistonD/;
$ref_files{$key} = "$dir".q|node111.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:stagnationTempDiff/;
$ref_files{$key} = "$dir".q|node106.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:ex:openValve/;
$ref_files{$key} = "$dir".q|node120.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoTPRatio/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno_eq:fDef/;
$ref_files{$key} = "$dir".q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:MaxTurning/;
$ref_files{$key} = "$dir".q|node223.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:impulsDef/;
$ref_files{$key} = "$dir".q|node90.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:e9d/;
$ref_files{$key} = "$dir".q|node185.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:tab:basic/;
$ref_files{$key} = "$dir".q|node121.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:isothermalNozzle/;
$ref_files{$key} = "$dir".q|node178.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:MtoUfunD/;
$ref_files{$key} = "$dir".q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:solRigitFannoFasta/;
$ref_files{$key} = "$dir".q|node180.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:math:m2express/;
$ref_files{$key} = "$dir".q|node132.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:m2/;
$ref_files{$key} = "$dir".q|node132.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MxlimitOpen/;
$ref_files{$key} = "$dir".q|node112.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:fig:cv/;
$ref_files{$key} = "$dir".q|node164.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:tab:unchoked/;
$ref_files{$key} = "$dir".q|node136.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isentropicPratio/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:tank/;
$ref_files{$key} = "$dir".q|node169.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:energy0/;
$ref_files{$key} = "$dir".q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:workingFLDApprox/;
$ref_files{$key} = "$dir".q|node131.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:tempSimple3/;
$ref_files{$key} = "$dir".q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:tab:basicIsentropic/;
$ref_files{$key} = "$dir".q|node86.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:momentum/;
$ref_files{$key} = "$dir".q|node129.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:gibbsCv/;
$ref_files{$key} = "$dir".q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:galileo/;
$ref_files{$key} = "$dir".q|node53.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:ImpulseRatio/;
$ref_files{$key} = "$dir".q|node90.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:PratioDimensional/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:R/;
$ref_files{$key} = "$dir".q|node200.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:e9e/;
$ref_files{$key} = "$dir".q|node185.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:D-theta-My-Mx_3/;
$ref_files{$key} = "$dir".q|node203.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:isoMix/;
$ref_files{$key} = "$dir".q|node73.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:fannoM1fM2/;
$ref_files{$key} = "$dir".q|node151.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:SepGenGovSemiRigit/;
$ref_files{$key} = "$dir".q|node184.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:continutyNonD/;
$ref_files{$key} = "$dir".q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:shock:entropy/;
$ref_files{$key} = "$dir".q|node94.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:maxSpeed/;
$ref_files{$key} = "$dir".q|node98.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:massAnother/;
$ref_files{$key} = "$dir".q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:minikoffSol/;
$ref_files{$key} = "$dir".q|node202.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:conversionT/;
$ref_files{$key} = "$dir".q|node106.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:solutionXi/;
$ref_files{$key} = "$dir".q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:variableArea/;
$ref_files{$key} = "$dir".q|node74.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:P5P1/;
$ref_files{$key} = "$dir".q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:ds-fld/;
$ref_files{$key} = "$dir".q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Fn-funRho0T/;
$ref_files{$key} = "$dir".q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:massFlowRate2/;
$ref_files{$key} = "$dir".q|node83.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:energyNI/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:TtoPz/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:massTotalPressure/;
$ref_files{$key} = "$dir".q|node85.html|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:eq:difuserEfficiency/;
$ref_files{$key} = "$dir".q|node124.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:tcSimple/;
$ref_files{$key} = "$dir".q|node176.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:dless-P/;
$ref_files{$key} = "$dir".q|node171.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:simpleEnergy/;
$ref_files{$key} = "$dir".q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:paritalyOpenUS/;
$ref_files{$key} = "$dir".q|node113.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:conversionUy/;
$ref_files{$key} = "$dir".q|node106.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:delta-theta/;
$ref_files{$key} = "$dir".q|node204.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:ex:openValveContinue/;
$ref_files{$key} = "$dir".q|node115.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:entrceL/;
$ref_files{$key} = "$dir".q|node133.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:localFar/;
$ref_files{$key} = "$dir".q|node208.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:mvUs2/;
$ref_files{$key} = "$dir".q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:momentum/;
$ref_files{$key} = "$dir".q|node139.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:generalPV/;
$ref_files{$key} = "$dir".q|node184.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:twoAngle/;
$ref_files{$key} = "$dir".q|node213.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:Cp/;
$ref_files{$key} = "$dir".q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/varibleShock:eq:mvUs/;
$ref_files{$key} = "$dir".q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:stagnationPressreRatio1/;
$ref_files{$key} = "$dir".q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:mixSound/;
$ref_files{$key} = "$dir".q|node73.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:TrhoSolutionDless/;
$ref_files{$key} = "$dir".q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:FLDretreat/;
$ref_files{$key} = "$dir".q|node157.html|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:eq:actualEnergy/;
$ref_files{$key} = "$dir".q|node123.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoRatioIdealgasISO/;
$ref_files{$key} = "$dir".q|node91.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:nDef/;
$ref_files{$key} = "$dir".q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MxpOpened/;
$ref_files{$key} = "$dir".q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:energyAfterInt1/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:bool/;
$ref_files{$key} = "$dir".q|node217.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:fmtheta1a/;
$ref_files{$key} = "$dir".q|node201.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:MtoUfun/;
$ref_files{$key} = "$dir".q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:momentum/;
$ref_files{$key} = "$dir".q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:theta/;
$ref_files{$key} = "$dir".q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:timeLinearHight2/;
$ref_files{$key} = "$dir".q|node69.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MsyMsx/;
$ref_files{$key} = "$dir".q|node106.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:PR/;
$ref_files{$key} = "$dir".q|node204.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:angle/;
$ref_files{$key} = "$dir".q|node220.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:stagnationTempDiff1/;
$ref_files{$key} = "$dir".q|node106.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:expession/;
$ref_files{$key} = "$dir".q|node132.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:stagnationPressreRatio/;
$ref_files{$key} = "$dir".q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:tbl:liquidSpeed/;
$ref_files{$key} = "$dir".q|node71.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:menikoff/;
$ref_files{$key} = "$dir".q|node202.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:dT0T/;
$ref_files{$key} = "$dir".q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:DzeroAlphaZero/;
$ref_files{$key} = "$dir".q|node203.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:MyforMxDelta/;
$ref_files{$key} = "$dir".q|node201.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MsxMyp/;
$ref_files{$key} = "$dir".q|node112.html|; 
$noresave{$key} = "$nosave";

$key = q/thermo:eq:dhPT/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:insontropic1/;
$ref_files{$key} = "$dir".q|node68.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:incompressibleP-Loss/;
$ref_files{$key} = "$dir".q|node132.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:Pbar/;
$ref_files{$key} = "$dir".q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:shockStrengthMx/;
$ref_files{$key} = "$dir".q|node103.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:weakTheta/;
$ref_files{$key} = "$dir".q|node207.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:ShockMove/;
$ref_files{$key} = "$dir".q|node106.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:dhNonID/;
$ref_files{$key} = "$dir".q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:fig:nozzleWithShock/;
$ref_files{$key} = "$dir".q|node122.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:M2n/;
$ref_files{$key} = "$dir".q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:math:m2express5/;
$ref_files{$key} = "$dir".q|node132.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:mass/;
$ref_files{$key} = "$dir".q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:dTdM/;
$ref_files{$key} = "$dir".q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:timeTank/;
$ref_files{$key} = "$dir".q|node176.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:energy/;
$ref_files{$key} = "$dir".q|node139.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:m2express/;
$ref_files{$key} = "$dir".q|node132.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArrea:eq:massFlowRatez/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:fig:compressiblityChart/;
$ref_files{$key} = "$dir".q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:wilson/;
$ref_files{$key} = "$dir".q|node71.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:bernoulli/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:tab:basicIsothermal/;
$ref_files{$key} = "$dir".q|node92.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:TratioExample/;
$ref_files{$key} = "$dir".q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:intro/;
$ref_files{$key} = "$dir".q|node37.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:crPbarn_1/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:entropyIdeal/;
$ref_files{$key} = "$dir".q|node94.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:math:m2express4/;
$ref_files{$key} = "$dir".q|node132.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:stangnationTempGov/;
$ref_files{$key} = "$dir".q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:mm/;
$ref_files{$key} = "$dir".q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:PoverSqrtT/;
$ref_files{$key} = "$dir".q|node172.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:AstarReduced/;
$ref_files{$key} = "$dir".q|node77.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:2law/;
$ref_files{$key} = "$dir".q|node129.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:item:fldSuper/;
$ref_files{$key} = "$dir".q|node157.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:PDiffReduced/;
$ref_files{$key} = "$dir".q|node77.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:chamberBlance/;
$ref_files{$key} = "$dir".q|node170.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:mvdU-rho/;
$ref_files{$key} = "$dir".q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:fld-M/;
$ref_files{$key} = "$dir".q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:logT0T/;
$ref_files{$key} = "$dir".q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:energyDless/;
$ref_files{$key} = "$dir".q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:gamma/;
$ref_files{$key} = "$dir".q|node73.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:deltaDelta/;
$ref_files{$key} = "$dir".q|node217.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:ATtotal/;
$ref_files{$key} = "$dir".q|node85.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:Tbar/;
$ref_files{$key} = "$dir".q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govDless6b/;
$ref_files{$key} = "$dir".q|node172.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:bulkModulus/;
$ref_files{$key} = "$dir".q|node71.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:en:subChoked/;
$ref_files{$key} = "$dir".q|node159.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:DrhoTOPFinal/;
$ref_files{$key} = "$dir".q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:mvdU-rho/;
$ref_files{$key} = "$dir".q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:ex:openValveAAA/;
$ref_files{$key} = "$dir".q|node115.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:solRigitFannoFastd/;
$ref_files{$key} = "$dir".q|node181.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:mixTwoPhases/;
$ref_files{$key} = "$dir".q|node73.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:Uratio/;
$ref_files{$key} = "$dir".q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:massNonD2/;
$ref_files{$key} = "$dir".q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:zeroMachWave/;
$ref_files{$key} = "$dir".q|node203.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:regions/;
$ref_files{$key} = "$dir".q|node196.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govRigitFannoFasta/;
$ref_files{$key} = "$dir".q|node180.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:epiloqueThisBook/;
$ref_files{$key} = "$dir".q|node13.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MypOpened/;
$ref_files{$key} = "$dir".q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:TPratio/;
$ref_files{$key} = "$dir".q|node171.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:d-Nu/;
$ref_files{$key} = "$dir".q|node220.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:mvRho/;
$ref_files{$key} = "$dir".q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:tmp1/;
$ref_files{$key} = "$dir".q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:energyAfterInt0/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:omega/;
$ref_files{$key} = "$dir".q|node224.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isentropicTratio/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:shockTube/;
$ref_files{$key} = "$dir".q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govDless1/;
$ref_files{$key} = "$dir".q|node171.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:f/;
$ref_files{$key} = "$dir".q|node139.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:massFlowRateStar/;
$ref_files{$key} = "$dir".q|node83.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dU-fld/;
$ref_files{$key} = "$dir".q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MypPbar/;
$ref_files{$key} = "$dir".q|node112.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:shock:state/;
$ref_files{$key} = "$dir".q|node94.html|; 
$noresave{$key} = "$nosave";

$key = q/varibleArea:fig:exmpleNozzle/;
$ref_files{$key} = "$dir".q|node90.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:cvMass1/;
$ref_files{$key} = "$dir".q|node68.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:massFlowRatezn_1/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:fig:MvA/;
$ref_files{$key} = "$dir".q|node81.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:D/;
$ref_files{$key} = "$dir".q|node200.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:mvDrho/;
$ref_files{$key} = "$dir".q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:massImp/;
$ref_files{$key} = "$dir".q|node90.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isodAdM/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:Pratioa/;
$ref_files{$key} = "$dir".q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:MassFlowRatenonIdeal1/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/eq::varibleArea:combine3/;
$ref_files{$key} = "$dir".q|node79.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:densityDless/;
$ref_files{$key} = "$dir".q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:areaChangeVelocity/;
$ref_files{$key} = "$dir".q|node79.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govDless6a/;
$ref_files{$key} = "$dir".q|node172.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govRigitFannoFast/;
$ref_files{$key} = "$dir".q|node181.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:fig:turnAngle/;
$ref_files{$key} = "$dir".q|node220.html|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:eq:coefficeintDischarge/;
$ref_files{$key} = "$dir".q|node123.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoPratio/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:TrhoSolutionDlessA/;
$ref_files{$key} = "$dir".q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:iserhoToP/;
$ref_files{$key} = "$dir".q|node69.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:NetForce/;
$ref_files{$key} = "$dir".q|node90.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:maxMassFlowRateOpenValveSimplified/;
$ref_files{$key} = "$dir".q|node112.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:LargeSymmetrialAngle/;
$ref_files{$key} = "$dir".q|node211.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:mass/;
$ref_files{$key} = "$dir".q|node79.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dRho-fld/;
$ref_files{$key} = "$dir".q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Fnzn_1/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:dless/;
$ref_files{$key} = "$dir".q|node171.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:govDxi/;
$ref_files{$key} = "$dir".q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:entropySolution/;
$ref_files{$key} = "$dir".q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:M1-FLD/;
$ref_files{$key} = "$dir".q|node161.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:ex:simpleExample/;
$ref_files{$key} = "$dir".q|node226.html|; 
$noresave{$key} = "$nosave";

$key = q/ref:fdl/;
$ref_files{$key} = "$dir".q|node2.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:fmMax/;
$ref_files{$key} = "$dir".q|node83.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:mDotFLD/;
$ref_files{$key} = "$dir".q|node151.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:T3T4R/;
$ref_files{$key} = "$dir".q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:appoximaetionR/;
$ref_files{$key} = "$dir".q|node73.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:T/;
$ref_files{$key} = "$dir".q|node200.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:mvU-rhoResults/;
$ref_files{$key} = "$dir".q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:Mach/;
$ref_files{$key} = "$dir".q|node221.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:dless-t/;
$ref_files{$key} = "$dir".q|node171.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:momentum/;
$ref_files{$key} = "$dir".q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:fullFld/;
$ref_files{$key} = "$dir".q|node174.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:thetaR/;
$ref_files{$key} = "$dir".q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dT-fld/;
$ref_files{$key} = "$dir".q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:massMon2/;
$ref_files{$key} = "$dir".q|node126.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:partialyOpenValvea/;
$ref_files{$key} = "$dir".q|node113.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:T0bar/;
$ref_files{$key} = "$dir".q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govIsoDless1/;
$ref_files{$key} = "$dir".q|node182.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:smallMaxDefA/;
$ref_files{$key} = "$dir".q|node202.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:timeLinearHight/;
$ref_files{$key} = "$dir".q|node69.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:wedgeEnd/;
$ref_files{$key} = "$dir".q|node206.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:energyDE/;
$ref_files{$key} = "$dir".q|node221.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:solRigitFannoFastc/;
$ref_files{$key} = "$dir".q|node181.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:sec:stationaryShockDrag/;
$ref_files{$key} = "$dir".q|node105.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:nozzleTankSolution/;
$ref_files{$key} = "$dir".q|node176.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:bernoulliStagnation/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:heatTranser/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:conversionP/;
$ref_files{$key} = "$dir".q|node106.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:energy/;
$ref_files{$key} = "$dir".q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:fannoM1M2fld/;
$ref_files{$key} = "$dir".q|node151.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:tbl:gasSpeed/;
$ref_files{$key} = "$dir".q|node71.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:longDLDwithShock/;
$ref_files{$key} = "$dir".q|node156.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:fld1/;
$ref_files{$key} = "$dir".q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:rayleigh/;
$ref_files{$key} = "$dir".q|node164.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoTzeroRatioStar/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:math:m2express6/;
$ref_files{$key} = "$dir".q|node132.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:fM/;
$ref_files{$key} = "$dir".q|node171.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:energy/;
$ref_files{$key} = "$dir".q|node79.html|; 
$noresave{$key} = "$nosave";

$key = q/veriableAreaeq:MassFlowRatezn_1/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:rhoNewDef/;
$ref_files{$key} = "$dir".q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:MBAdiabaticSimple/;
$ref_files{$key} = "$dir".q|node176.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:limitedTheta/;
$ref_files{$key} = "$dir".q|node204.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:idealGas1/;
$ref_files{$key} = "$dir".q|node69.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoRatioIdealgasStar/;
$ref_files{$key} = "$dir".q|node91.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:mvU2/;
$ref_files{$key} = "$dir".q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:universalIntropy/;
$ref_files{$key} = "$dir".q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:solRigitFannoFastbb/;
$ref_files{$key} = "$dir".q|node180.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:temperature2/;
$ref_files{$key} = "$dir".q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:dUrUtheta/;
$ref_files{$key} = "$dir".q|node221.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:M2_0/;
$ref_files{$key} = "$dir".q|node204.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:math:m2express3/;
$ref_files{$key} = "$dir".q|node132.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:workingFLD/;
$ref_files{$key} = "$dir".q|node131.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govDless2/;
$ref_files{$key} = "$dir".q|node171.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:cvMass2/;
$ref_files{$key} = "$dir".q|node68.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govDless6c/;
$ref_files{$key} = "$dir".q|node172.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:linear/;
$ref_files{$key} = "$dir".q|node184.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:massD/;
$ref_files{$key} = "$dir".q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:menikoffLarge/;
$ref_files{$key} = "$dir".q|node202.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:tab:Fn/;
$ref_files{$key} = "$dir".q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:suddenlyOpen/;
$ref_files{$key} = "$dir".q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:QdeltaZero1/;
$ref_files{$key} = "$dir".q|node203.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:solSemiRigida/;
$ref_files{$key} = "$dir".q|node184.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:ex:impulse/;
$ref_files{$key} = "$dir".q|node90.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:AstarTotalPressure/;
$ref_files{$key} = "$dir".q|node85.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:nonIdealRelation/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:energy/;
$ref_files{$key} = "$dir".q|node126.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dM2-fld/;
$ref_files{$key} = "$dir".q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:Ac/;
$ref_files{$key} = "$dir".q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:Q/;
$ref_files{$key} = "$dir".q|node200.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:fig:compareNozzle/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MyfMxopened/;
$ref_files{$key} = "$dir".q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:RH/;
$ref_files{$key} = "$dir".q|node103.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:conversionMy/;
$ref_files{$key} = "$dir".q|node106.html|; 
$noresave{$key} = "$nosave";

$key = q/varibleShock:eq:mvPbar/;
$ref_files{$key} = "$dir".q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MsxPiston/;
$ref_files{$key} = "$dir".q|node111.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:openedDeltaTr/;
$ref_files{$key} = "$dir".q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:fld-dM1/;
$ref_files{$key} = "$dir".q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:gibbs/;
$ref_files{$key} = "$dir".q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:movingShockDrag/;
$ref_files{$key} = "$dir".q|node107.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:stangnationPressureGov/;
$ref_files{$key} = "$dir".q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:energySolution/;
$ref_files{$key} = "$dir".q|node221.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:temperatureDlessModifiedB/;
$ref_files{$key} = "$dir".q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:Dzero/;
$ref_files{$key} = "$dir".q|node203.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:stationaryMx/;
$ref_files{$key} = "$dir".q|node108.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:kM2/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:verticalLine/;
$ref_files{$key} = "$dir".q|node220.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:OpenSuddenlyConvergence/;
$ref_files{$key} = "$dir".q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:e9f/;
$ref_files{$key} = "$dir".q|node185.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:a1/;
$ref_files{$key} = "$dir".q|node200.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:temperatureDlessModifiedA/;
$ref_files{$key} = "$dir".q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/varibleArea:fig:superSonicTunnel/;
$ref_files{$key} = "$dir".q|node124.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:fig:overMaxAngle/;
$ref_files{$key} = "$dir".q|node223.html|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:eq:difinitionRatio/;
$ref_files{$key} = "$dir".q|node123.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:P0ratio/;
$ref_files{$key} = "$dir".q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:densityMx/;
$ref_files{$key} = "$dir".q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:stationaryMy/;
$ref_files{$key} = "$dir".q|node108.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:veloctyRadious/;
$ref_files{$key} = "$dir".q|node221.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:Vtimea/;
$ref_files{$key} = "$dir".q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:momentumC/;
$ref_files{$key} = "$dir".q|node100.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:PVlinear/;
$ref_files{$key} = "$dir".q|node183.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:PeffectsM-asFun-frac__16310__4fL__16310____16311__D__16311__/;
$ref_files{$key} = "$dir".q|node154.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:TstarTzero/;
$ref_files{$key} = "$dir".q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:massBallanceInt/;
$ref_files{$key} = "$dir".q|node176.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:rhoRa/;
$ref_files{$key} = "$dir".q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:maxM1fld2/;
$ref_files{$key} = "$dir".q|node157.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isentropicDiff/;
$ref_files{$key} = "$dir".q|node80.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:temperatureDlessModified/;
$ref_files{$key} = "$dir".q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:PzeroReduced/;
$ref_files{$key} = "$dir".q|node77.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:gibbs/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:CV/;
$ref_files{$key} = "$dir".q|node129.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:sol/;
$ref_files{$key} = "$dir".q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:sondLiquid/;
$ref_files{$key} = "$dir".q|node71.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:rU-kMP/;
$ref_files{$key} = "$dir".q|node81.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:inlet/;
$ref_files{$key} = "$dir".q|node212.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:isentripicRetio/;
$ref_files{$key} = "$dir".q|node172.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:thetaAlpha/;
$ref_files{$key} = "$dir".q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:deltaFMonly/;
$ref_files{$key} = "$dir".q|node211.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:RzeroReduced2/;
$ref_files{$key} = "$dir".q|node77.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:tab:max/;
$ref_files{$key} = "$dir".q|node209.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:mnu1/;
$ref_files{$key} = "$dir".q|node219.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:fig:vacuum/;
$ref_files{$key} = "$dir".q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:workingEq/;
$ref_files{$key} = "$dir".q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:PstarPzero1/;
$ref_files{$key} = "$dir".q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:RzeroReduced/;
$ref_files{$key} = "$dir".q|node77.html|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:massMon5/;
$ref_files{$key} = "$dir".q|node126.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:a3/;
$ref_files{$key} = "$dir".q|node200.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govDless3/;
$ref_files{$key} = "$dir".q|node171.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Fn-funRho0P/;
$ref_files{$key} = "$dir".q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:fig:rigida/;
$ref_files{$key} = "$dir".q|node181.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:PratioOld/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:RgasSide/;
$ref_files{$key} = "$dir".q|node73.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:shockVariable/;
$ref_files{$key} = "$dir".q|node122.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:nonDimMass/;
$ref_files{$key} = "$dir".q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:starMtoM/;
$ref_files{$key} = "$dir".q|node100.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:mainT-P/;
$ref_files{$key} = "$dir".q|node101.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:horizontalLine/;
$ref_files{$key} = "$dir".q|node220.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:PzeroD/;
$ref_files{$key} = "$dir".q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:HD/;
$ref_files{$key} = "$dir".q|node139.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:compressedGasD/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:nuInfty/;
$ref_files{$key} = "$dir".q|node221.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:mvU2simplified/;
$ref_files{$key} = "$dir".q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:openedDeltaT/;
$ref_files{$key} = "$dir".q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:energyNonIdeal/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:CloseValveUx/;
$ref_files{$key} = "$dir".q|node114.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:toBeSolved/;
$ref_files{$key} = "$dir".q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:solRigitFannoFastReversed/;
$ref_files{$key} = "$dir".q|node180.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:stagnationMachNumber/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:develpSub/;
$ref_files{$key} = "$dir".q|node150.html|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:fannoFun/;
$ref_files{$key} = "$dir".q|node46.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:PstarPzero/;
$ref_files{$key} = "$dir".q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:entroyS/;
$ref_files{$key} = "$dir".q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:massR/;
$ref_files{$key} = "$dir".q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:mass/;
$ref_files{$key} = "$dir".q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:stagnationSoundSpeed/;
$ref_files{$key} = "$dir".q|node98.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:MomMassImp/;
$ref_files{$key} = "$dir".q|node90.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:suddenlyOpenst/;
$ref_files{$key} = "$dir".q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:P-M/;
$ref_files{$key} = "$dir".q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dP-dM/;
$ref_files{$key} = "$dir".q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:tangentU/;
$ref_files{$key} = "$dir".q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:temp-M1/;
$ref_files{$key} = "$dir".q|node80.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:energy/;
$ref_files{$key} = "$dir".q|node174.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:combineDimMassEnergy/;
$ref_files{$key} = "$dir".q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:deLavalNozzle/;
$ref_files{$key} = "$dir".q|node44.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:RdeltaZero/;
$ref_files{$key} = "$dir".q|node203.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:entropy/;
$ref_files{$key} = "$dir".q|node139.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:tab:suddenlyClosed/;
$ref_files{$key} = "$dir".q|node121.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:M1t/;
$ref_files{$key} = "$dir".q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:fliegner/;
$ref_files{$key} = "$dir".q|node83.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dP-fld/;
$ref_files{$key} = "$dir".q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:momentumD1aa/;
$ref_files{$key} = "$dir".q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:seperatedSemiRigid/;
$ref_files{$key} = "$dir".q|node183.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:main/;
$ref_files{$key} = "$dir".q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:momentumD1a/;
$ref_files{$key} = "$dir".q|node139.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:Prandtl-Meyer/;
$ref_files{$key} = "$dir".q|node218.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MsxPistonGeneral/;
$ref_files{$key} = "$dir".q|node111.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:Uthetac/;
$ref_files{$key} = "$dir".q|node221.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:tanMU/;
$ref_files{$key} = "$dir".q|node220.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:tab:basic/;
$ref_files{$key} = "$dir".q|node163.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:thermo2/;
$ref_files{$key} = "$dir".q|node79.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:gamma1/;
$ref_files{$key} = "$dir".q|node73.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoMomentum/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:cvMomentumEx/;
$ref_files{$key} = "$dir".q|node68.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Fn-funF/;
$ref_files{$key} = "$dir".q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:M-A/;
$ref_files{$key} = "$dir".q|node81.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoMass/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/varialbeArea:eq:bernnolliZP/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:M-Tzn_1/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:largeM1theta/;
$ref_files{$key} = "$dir".q|node205.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:cvCombined2/;
$ref_files{$key} = "$dir".q|node68.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:Tbar/;
$ref_files{$key} = "$dir".q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:relationshipP-rho/;
$ref_files{$key} = "$dir".q|node80.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:DrhoTOP/;
$ref_files{$key} = "$dir".q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govDless5/;
$ref_files{$key} = "$dir".q|node171.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:ditachment/;
$ref_files{$key} = "$dir".q|node210.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:solidExampl/;
$ref_files{$key} = "$dir".q|node72.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:combAllR/;
$ref_files{$key} = "$dir".q|node100.html|; 
$noresave{$key} = "$nosave";

$key = q/veriableAreaeq:MassFlowRatez/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:thetaMax/;
$ref_files{$key} = "$dir".q|node202.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:PfunFLDb/;
$ref_files{$key} = "$dir".q|node153.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:nuTheta/;
$ref_files{$key} = "$dir".q|node221.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Mz/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:fig:M1-fld/;
$ref_files{$key} = "$dir".q|node136.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:a2/;
$ref_files{$key} = "$dir".q|node200.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Uzn_1/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:energy/;
$ref_files{$key} = "$dir".q|node94.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:pressureMsx/;
$ref_files{$key} = "$dir".q|node112.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:Mmax-kFanno/;
$ref_files{$key} = "$dir".q|node147.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:fliegnerAir/;
$ref_files{$key} = "$dir".q|node83.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:fig:rigid/;
$ref_files{$key} = "$dir".q|node180.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:Tratioa/;
$ref_files{$key} = "$dir".q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:energyDEa/;
$ref_files{$key} = "$dir".q|node221.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:reducedDensity/;
$ref_files{$key} = "$dir".q|node205.html|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:mass/;
$ref_files{$key} = "$dir".q|node126.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:Punarranged/;
$ref_files{$key} = "$dir".q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Mach/;
$ref_files{$key} = "$dir".q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:movingShockDrag/;
$ref_files{$key} = "$dir".q|node215.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:fig:impulse/;
$ref_files{$key} = "$dir".q|node90.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:momentumD1/;
$ref_files{$key} = "$dir".q|node139.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:stagnationTempDiff2/;
$ref_files{$key} = "$dir".q|node106.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:maxUisotermalNozzle/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:bullet/;
$ref_files{$key} = "$dir".q|node210.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govDless4/;
$ref_files{$key} = "$dir".q|node171.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:insontropic2/;
$ref_files{$key} = "$dir".q|node68.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:temperaturePbar/;
$ref_files{$key} = "$dir".q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:fig:pmResults/;
$ref_files{$key} = "$dir".q|node224.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:sndRlawExDrivative/;
$ref_files{$key} = "$dir".q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:prandtl/;
$ref_files{$key} = "$dir".q|node58.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:CpCvRN/;
$ref_files{$key} = "$dir".q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:TrhoSolution/;
$ref_files{$key} = "$dir".q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:gibbsCp/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:fanno/;
$ref_files{$key} = "$dir".q|node137.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:RankineHugoniot/;
$ref_files{$key} = "$dir".q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:rhoR/;
$ref_files{$key} = "$dir".q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:MassFlowRatenonIdeal/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:max/;
$ref_files{$key} = "$dir".q|node212.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:TOTtmp/;
$ref_files{$key} = "$dir".q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:shockStrength/;
$ref_files{$key} = "$dir".q|node103.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:state/;
$ref_files{$key} = "$dir".q|node139.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:starMach/;
$ref_files{$key} = "$dir".q|node99.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dPoP/;
$ref_files{$key} = "$dir".q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:Isothermal/;
$ref_files{$key} = "$dir".q|node128.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:emanuel/;
$ref_files{$key} = "$dir".q|node201.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:inletEx/;
$ref_files{$key} = "$dir".q|node213.html|; 
$noresave{$key} = "$nosave";

$key = q/varialbeArea:eq:bernnolliZ/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:rayleigh/;
$ref_files{$key} = "$dir".q|node55.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:fig:fig3/;
$ref_files{$key} = "$dir".q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/varibleShock:eq:mvMs/;
$ref_files{$key} = "$dir".q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:massFlowRate/;
$ref_files{$key} = "$dir".q|node83.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:CloseValveUy/;
$ref_files{$key} = "$dir".q|node114.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:math:m2express2/;
$ref_files{$key} = "$dir".q|node132.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:program/;
$ref_files{$key} = "$dir".q|node228.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:Tratio/;
$ref_files{$key} = "$dir".q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:item:CloseValvePa/;
$ref_files{$key} = "$dir".q|node114.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:PzeroReduced1/;
$ref_files{$key} = "$dir".q|node77.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:massNonD1/;
$ref_files{$key} = "$dir".q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:sound/;
$ref_files{$key} = "$dir".q|node66.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:dless-T/;
$ref_files{$key} = "$dir".q|node171.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:2ndLaw/;
$ref_files{$key} = "$dir".q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea_eq:massPAequal/;
$ref_files{$key} = "$dir".q|node85.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:S/;
$ref_files{$key} = "$dir".q|node200.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:HDdef/;
$ref_files{$key} = "$dir".q|node139.html|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:isoChokingPoint/;
$ref_files{$key} = "$dir".q|node126.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:integralMach/;
$ref_files{$key} = "$dir".q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:noInclation/;
$ref_files{$key} = "$dir".q|node193.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:solutionIntergral/;
$ref_files{$key} = "$dir".q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:thermo/;
$ref_files{$key} = "$dir".q|node79.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:solution2/;
$ref_files{$key} = "$dir".q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/eq::varibleArea:combine2/;
$ref_files{$key} = "$dir".q|node79.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:M1/;
$ref_files{$key} = "$dir".q|node205.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:HDdef/;
$ref_files{$key} = "$dir".q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:temperatureDless/;
$ref_files{$key} = "$dir".q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:massMon0/;
$ref_files{$key} = "$dir".q|node126.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:PfunFLDa/;
$ref_files{$key} = "$dir".q|node152.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:momentum/;
$ref_files{$key} = "$dir".q|node94.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:M2/;
$ref_files{$key} = "$dir".q|node204.html|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:fanno/;
$ref_files{$key} = "$dir".q|node57.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:fig:angleDef/;
$ref_files{$key} = "$dir".q|node219.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:M-Tzcriticaln_1/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:M-A-0/;
$ref_files{$key} = "$dir".q|node81.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:fig:coor/;
$ref_files{$key} = "$dir".q|node221.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:state/;
$ref_files{$key} = "$dir".q|node129.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:averageVelocity/;
$ref_files{$key} = "$dir".q|node73.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:stateDless/;
$ref_files{$key} = "$dir".q|node79.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:stabiltyShock/;
$ref_files{$key} = "$dir".q|node217.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:Rindenty/;
$ref_files{$key} = "$dir".q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:t2t1a/;
$ref_files{$key} = "$dir".q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:cv_flow/;
$ref_files{$key} = "$dir".q|node137.html|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:eq:idealGasDifuserEfficiency/;
$ref_files{$key} = "$dir".q|node124.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:conversionT0T/;
$ref_files{$key} = "$dir".q|node106.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:math:def/;
$ref_files{$key} = "$dir".q|node132.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:fig:stationary/;
$ref_files{$key} = "$dir".q|node68.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:speedSoundNonIdealGas/;
$ref_files{$key} = "$dir".q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:x1/;
$ref_files{$key} = "$dir".q|node200.html|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:massMon3/;
$ref_files{$key} = "$dir".q|node126.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:ex:openValveBB/;
$ref_files{$key} = "$dir".q|node120.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:temperatureRho/;
$ref_files{$key} = "$dir".q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:deltaFLD/;
$ref_files{$key} = "$dir".q|node157.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:compressedGas/;
$ref_files{$key} = "$dir".q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:genGovSemiRigit/;
$ref_files{$key} = "$dir".q|node184.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:subFLDP2P1/;
$ref_files{$key} = "$dir".q|node159.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:reverseTheta/;
$ref_files{$key} = "$dir".q|node221.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:energyDerivative/;
$ref_files{$key} = "$dir".q|node139.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:math2/;
$ref_files{$key} = "$dir".q|node132.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:crPbar/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:openValveEx/;
$ref_files{$key} = "$dir".q|node120.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:forced/;
$ref_files{$key} = "$dir".q|node125.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:fig:basic/;
$ref_files{$key} = "$dir".q|node167.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:mu2/;
$ref_files{$key} = "$dir".q|node219.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:massMomOFS/;
$ref_files{$key} = "$dir".q|node100.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:pressureGov/;
$ref_files{$key} = "$dir".q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:solution/;
$ref_files{$key} = "$dir".q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:seperatedSemiRigida/;
$ref_files{$key} = "$dir".q|node184.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:ex:ex2/;
$ref_files{$key} = "$dir".q|node145.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:Pratio/;
$ref_files{$key} = "$dir".q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:maxKSuddenlyOpen/;
$ref_files{$key} = "$dir".q|node112.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:vanWylen/;
$ref_files{$key} = "$dir".q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:stateD/;
$ref_files{$key} = "$dir".q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/ch:fluid/;
$ref_files{$key} = "$dir".q|node61.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:bernolliSound/;
$ref_files{$key} = "$dir".q|node221.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:dless-V/;
$ref_files{$key} = "$dir".q|node171.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:T0T/;
$ref_files{$key} = "$dir".q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:M-Tz/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:U3-2/;
$ref_files{$key} = "$dir".q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:sndRlawEx/;
$ref_files{$key} = "$dir".q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:sharpNose/;
$ref_files{$key} = "$dir".q|node212.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:Rhoratio/;
$ref_files{$key} = "$dir".q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:ex:inletEx/;
$ref_files{$key} = "$dir".q|node213.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:MachReflection/;
$ref_files{$key} = "$dir".q|node211.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:tab:suddenlyOpened1.3/;
$ref_files{$key} = "$dir".q|node121.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:generalSolution/;
$ref_files{$key} = "$dir".q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:increaseFLD/;
$ref_files{$key} = "$dir".q|node149.html|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:eq:maxEnergy/;
$ref_files{$key} = "$dir".q|node123.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:smallPresSolution/;
$ref_files{$key} = "$dir".q|node132.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:energyDE0/;
$ref_files{$key} = "$dir".q|node221.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoAratioStar/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:fig:dalembert/;
$ref_files{$key} = "$dir".q|node225.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:weakThompson/;
$ref_files{$key} = "$dir".q|node201.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:fig:angleDefa/;
$ref_files{$key} = "$dir".q|node225.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:whatFn2/;
$ref_files{$key} = "$dir".q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:mass/;
$ref_files{$key} = "$dir".q|node94.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:fig:basic/;
$ref_files{$key} = "$dir".q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Fn-funFS/;
$ref_files{$key} = "$dir".q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:ex:reversed/;
$ref_files{$key} = "$dir".q|node226.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:fMsimple/;
$ref_files{$key} = "$dir".q|node176.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:fig:simpleExample/;
$ref_files{$key} = "$dir".q|node226.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:flowView/;
$ref_files{$key} = "$dir".q|node201.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:starSpeedSound/;
$ref_files{$key} = "$dir".q|node99.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MsxPistonLimit/;
$ref_files{$key} = "$dir".q|node111.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:cvEnergy1/;
$ref_files{$key} = "$dir".q|node68.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:fig:twoConnection/;
$ref_files{$key} = "$dir".q|node170.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Ucomparison/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:CpCvR/;
$ref_files{$key} = "$dir".q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:T0ratio/;
$ref_files{$key} = "$dir".q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Fnz/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:unPr/;
$ref_files{$key} = "$dir".q|node100.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:fig:isoBasic/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:fig:atmos/;
$ref_files{$key} = "$dir".q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:fmG/;
$ref_files{$key} = "$dir".q|node83.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:soundDless/;
$ref_files{$key} = "$dir".q|node171.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:isonropic/;
$ref_files{$key} = "$dir".q|node73.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:momentumD/;
$ref_files{$key} = "$dir".q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoRatio/;
$ref_files{$key} = "$dir".q|node91.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:item:openValvePa/;
$ref_files{$key} = "$dir".q|node113.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:Msolution1/;
$ref_files{$key} = "$dir".q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:U3U2/;
$ref_files{$key} = "$dir".q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:totalPressureRatio/;
$ref_files{$key} = "$dir".q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:eckert/;
$ref_files{$key} = "$dir".q|node59.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:f/;
$ref_files{$key} = "$dir".q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:initialCondition/;
$ref_files{$key} = "$dir".q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govsemiRigid1/;
$ref_files{$key} = "$dir".q|node183.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:math:series/;
$ref_files{$key} = "$dir".q|node132.html|; 
$noresave{$key} = "$nosave";

$key = q/computer:fig:structure/;
$ref_files{$key} = "$dir".q|node229.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:P-M1/;
$ref_files{$key} = "$dir".q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:ex:twoPistons/;
$ref_files{$key} = "$dir".q|node115.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:relationshipU-rho/;
$ref_files{$key} = "$dir".q|node80.html|; 
$noresave{$key} = "$nosave";

$key = q/forced:eq:massMon4/;
$ref_files{$key} = "$dir".q|node126.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:x2/;
$ref_files{$key} = "$dir".q|node200.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:gibbsCp/;
$ref_files{$key} = "$dir".q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:eq:idealGasDifuserEfficiencyIni/;
$ref_files{$key} = "$dir".q|node124.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MyfMx/;
$ref_files{$key} = "$dir".q|node108.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Fn-funFSa/;
$ref_files{$key} = "$dir".q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:twoPistons/;
$ref_files{$key} = "$dir".q|node115.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:partialyOpenValve/;
$ref_files{$key} = "$dir".q|node114.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:nozzleComparison/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:t2t1b/;
$ref_files{$key} = "$dir".q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:tbl:solidSpeed/;
$ref_files{$key} = "$dir".q|node72.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:massFlowRateRatio/;
$ref_files{$key} = "$dir".q|node83.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:bernoulliStagnation1/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:anotherMtoU/;
$ref_files{$key} = "$dir".q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:tmaxtc/;
$ref_files{$key} = "$dir".q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:ex:tunnel2deflection/;
$ref_files{$key} = "$dir".q|node213.html|; 
$noresave{$key} = "$nosave";

$key = q/iso:fig:cv/;
$ref_files{$key} = "$dir".q|node128.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:fliegnerk/;
$ref_files{$key} = "$dir".q|node83.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:tab:basic/;
$ref_files{$key} = "$dir".q|node167.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:massFlowRateID/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:fig:cv/;
$ref_files{$key} = "$dir".q|node78.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:epsilonF/;
$ref_files{$key} = "$dir".q|node202.html|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:fig:nozzle/;
$ref_files{$key} = "$dir".q|node122.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoRatioIdealgas/;
$ref_files{$key} = "$dir".q|node91.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MyfMxs/;
$ref_files{$key} = "$dir".q|node108.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:tmp3/;
$ref_files{$key} = "$dir".q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:M1n/;
$ref_files{$key} = "$dir".q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:fig:difsure_efficiency/;
$ref_files{$key} = "$dir".q|node124.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:massNonD0/;
$ref_files{$key} = "$dir".q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/varibleShock:eq:mvUbar/;
$ref_files{$key} = "$dir".q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:T2T1R/;
$ref_files{$key} = "$dir".q|node116.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:pressureDless/;
$ref_files{$key} = "$dir".q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:energy/;
$ref_files{$key} = "$dir".q|node221.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:CloseValveMy/;
$ref_files{$key} = "$dir".q|node114.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:Pbar/;
$ref_files{$key} = "$dir".q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:fig:general/;
$ref_files{$key} = "$dir".q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:ex:throat/;
$ref_files{$key} = "$dir".q|node213.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:MBAdiabatic/;
$ref_files{$key} = "$dir".q|node176.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:TxTyInformal/;
$ref_files{$key} = "$dir".q|node96.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:energy/;
$ref_files{$key} = "$dir".q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:mass/;
$ref_files{$key} = "$dir".q|node129.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:ex2/;
$ref_files{$key} = "$dir".q|node145.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:mass/;
$ref_files{$key} = "$dir".q|node139.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:fig:piston/;
$ref_files{$key} = "$dir".q|node68.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:monImp/;
$ref_files{$key} = "$dir".q|node90.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:tmp1a/;
$ref_files{$key} = "$dir".q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:prandtl/;
$ref_files{$key} = "$dir".q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:adiabatic/;
$ref_files{$key} = "$dir".q|node220.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:fannoGraph/;
$ref_files{$key} = "$dir".q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:nu/;
$ref_files{$key} = "$dir".q|node220.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:machsSuddenStop/;
$ref_files{$key} = "$dir".q|node108.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:gov/;
$ref_files{$key} = "$dir".q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:maxM1fld1/;
$ref_files{$key} = "$dir".q|node156.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:intersection/;
$ref_files{$key} = "$dir".q|node94.html|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:rankine/;
$ref_files{$key} = "$dir".q|node56.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:MyLimit/;
$ref_files{$key} = "$dir".q|node102.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoTzeroR1/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:PstarPzeroINI/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoIdealgas/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:beforeDefa/;
$ref_files{$key} = "$dir".q|node90.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:stateMassD/;
$ref_files{$key} = "$dir".q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:strongThompson/;
$ref_files{$key} = "$dir".q|node201.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:PrDless/;
$ref_files{$key} = "$dir".q|node100.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:rhoBar/;
$ref_files{$key} = "$dir".q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/shockVariable:eq:nozzleEfficiency/;
$ref_files{$key} = "$dir".q|node123.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:shockPartialOpen/;
$ref_files{$key} = "$dir".q|node113.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:memSimple/;
$ref_files{$key} = "$dir".q|node68.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoTzeroR/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:tempSimple/;
$ref_files{$key} = "$dir".q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:CloseValveMx/;
$ref_files{$key} = "$dir".q|node114.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:exitU/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:dT_0dT/;
$ref_files{$key} = "$dir".q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:actualCalM-FLD_0.3/;
$ref_files{$key} = "$dir".q|node155.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:press-M/;
$ref_files{$key} = "$dir".q|node81.html|; 
$noresave{$key} = "$nosave";

$key = q/variableShock:ex:tank/;
$ref_files{$key} = "$dir".q|node122.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:Ucomparison1/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:solutionChemical/;
$ref_files{$key} = "$dir".q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:govDxi1/;
$ref_files{$key} = "$dir".q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:fig:movingShockDrag2/;
$ref_files{$key} = "$dir".q|node107.html|; 
$noresave{$key} = "$nosave";

$key = q/math:eq:binomal/;
$ref_files{$key} = "$dir".q|node77.html|; 
$noresave{$key} = "$nosave";

$key = q/piston:eq:Vtime/;
$ref_files{$key} = "$dir".q|node188.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:stateDa/;
$ref_files{$key} = "$dir".q|node130.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:fig:subBranch/;
$ref_files{$key} = "$dir".q|node159.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:timeFilling/;
$ref_files{$key} = "$dir".q|node176.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoPratioStar/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:RR/;
$ref_files{$key} = "$dir".q|node204.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:momentumD1b/;
$ref_files{$key} = "$dir".q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoPzeroRatioStar/;
$ref_files{$key} = "$dir".q|node88.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isentropicR/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:fig:Ts/;
$ref_files{$key} = "$dir".q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:P0P0starmid/;
$ref_files{$key} = "$dir".q|node144.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:state/;
$ref_files{$key} = "$dir".q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:sec:movingShockDrag/;
$ref_files{$key} = "$dir".q|node215.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:eq:govEq/;
$ref_files{$key} = "$dir".q|node170.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:entropyA/;
$ref_files{$key} = "$dir".q|node140.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:oblique/;
$ref_files{$key} = "$dir".q|node192.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:pressureRatio/;
$ref_files{$key} = "$dir".q|node97.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:smallMaxDefA1/;
$ref_files{$key} = "$dir".q|node202.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:sec:movingShockDrag/;
$ref_files{$key} = "$dir".q|node107.html|; 
$noresave{$key} = "$nosave";

$key = q/intro:fig:nozzlePressure/;
$ref_files{$key} = "$dir".q|node44.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:ex:reservoir/;
$ref_files{$key} = "$dir".q|node145.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:RzeroReduced1/;
$ref_files{$key} = "$dir".q|node77.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:Pratio/;
$ref_files{$key} = "$dir".q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/tank:fig:model/;
$ref_files{$key} = "$dir".q|node170.html|; 
$noresave{$key} = "$nosave";

1;

