# LaTeX2HTML 2002-2-1 (1.71)
# Associate images original text with physical files.


$key = q/displaystyleT;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img269.png"
 ALT="$\displaystyle T$">|; 

$key = q/20[bar];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="52" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img87.png"
 ALT="$ 20[bar]$">|; 

$key = q/R_{mix};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img178.png"
 ALT="$ R_{mix}$">|; 

$key = q/displaystyle{(c-dU)^2-c^{2}over2}+{dPoverrho}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="182" HEIGHT="57" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img63.png"
 ALT="$\displaystyle { ( c - dU)^2 - c^{2} \over 2} + {dP \over \rho} = 0$">|; 

$key = q/displaystyle{T_0overT}=1+{k-1over2}left(dot{m}overArhocright)^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="177" HEIGHT="61" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img304.png"
 ALT="$\displaystyle {T_0 \over T} = 1 + { k -1 \over 2} \left(\dot{m} \over A \rho c \right)^2$">|; 

$key = q/displaystyle{RT_0overP^2}left(dot{m}overAright)^2={Fn^2overk}left(A^{*}P_0overAPright)^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="216" HEIGHT="61" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img313.png"
 ALT="$\displaystyle { R T_0 \over P^2} \left( \dot{m} \over A \right) ^2 = {Fn^2\over k} \left( A^{*} P_0 \over A P \right)^2$">|; 

$key = q/dT_0=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="58" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img252.png"
 ALT="$ dT_0 = 0$">|; 

$key = q/displaystyleC_p=left(partialhoverpartialTright)_P=Tleft(partialsoverpartialTright)_P;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="197" HEIGHT="54" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img112.png"
 ALT="$\displaystyle C_p = \left( \partial h \over \partial T \right)_P = T \left( \partial s \over \partial T \right)_P$">|; 

$key = q/displaystyleleft.{P_{0}}right|_{B}=51.25781291times{1.1761671over2.6367187}times{0.01over0.015}approx15.243[Bar];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="398" HEIGHT="50" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img341.png"
 ALT="$\displaystyle \left.{P_{0}}\right\vert _{B} = 51.25781291 \times { 1.1761671 \over 2.6367187 } \times {0.01 \over 0.015} \approx 15.243 [Bar]$">|; 

$key = q/displaystylec=sqrt{elastic;propertyoverinertial;property}=sqrt{Boverrho};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="226" HEIGHT="68" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img145.png"
 ALT="$\displaystyle c = \sqrt{elastic\; property \over inertial\; property} = \sqrt{B \over \rho}$">|; 

$key = q/displaystylen=overbrace{C_poverC_v}^{k}left(z+Tleft(partialzoverpartialTright)_{rho}overz+Tleft(partialzoverpartialTright)_{P}right);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="183" HEIGHT="99" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img129.png"
 ALT="$\displaystyle n = \overbrace{C_p \over C_v}^{k} \left( z + T \left( \partial z ...
...ight)_{\rho} \over z + T \left( \partial z \over \partial T \right)_{P} \right)$">|; 

$key = q/P_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img3.png"
 ALT="$ P_0$">|; 

$key = q/displaystyle{dPoverrho}+UdU=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="111" HEIGHT="52" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img222.png"
 ALT="$\displaystyle {dP \over \rho} + U dU = 0$">|; 

$key = q/displaystylec=sqrt{kRT}=sqrt{1.407times287times300}sim348.1[mslashsec];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="346" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img135.png"
 ALT="$\displaystyle c = \sqrt{kRT} = \sqrt{1.407 \times 287 \times 300} \sim 348.1[m/sec]$">|; 

$key = q/displaystyle=0.86420338times(273+21)=254.076K;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="276" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img270.png"
 ALT="$\displaystyle = 0.86420338 \times (273+ 21) = 254.076 K$">|; 

$key = q/displaystyle{AoverA^{*}}={1overM}left({1+{k-1over2}M^{2}over{k+1over2}}right)^{k+1over2(k-1)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="219" HEIGHT="77" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img292.png"
 ALT="$\displaystyle {A \over A^{*}} = { 1 \over M} \left( { 1 + {k -1 \over 2} M^{2} \over {k +1\over 2}} \right) ^ {k+ 1 \over 2 (k -1 )}$">|; 

$key = q/{M_y}^{'}=1.3;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="74" HEIGHT="42" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img6.png"
 ALT="$ {M_y}^{'}=1.3$">|; 

$key = q/displaystyleT_0rho^2=overbrace{Trho}^{poverR}rho+overbrace{left(Toverc^2right)}^{1slashkR}{k-1over2}left(dot{m}overAright)^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="251" HEIGHT="106" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img305.png"
 ALT="$\displaystyle T_0 \rho^2 = \overbrace{T \rho}^{p \over R} \rho + \overbrace{\left( T \over c^2 \right)}^{1/kR} {k -1 \over 2} \left( \dot{m} \over A \right) ^2$">|; 

$key = q/nsim1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="43" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img139.png"
 ALT="$ n\sim 1$">|; 

$key = q/[Bar];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img276.png"
 ALT="$ [Bar]$">|; 

$key = q/displaystyleleft(dot{m}overA^{*}right)_{max}{P_0oversqrt{T_0}}sim{0.68473oversqrt{R}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="191" HEIGHT="54" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img294.png"
 ALT="$\displaystyle \left(\dot{m} \over A^{*}\right)_{max} {P_0 \over \sqrt{T_0}} \sim {0.68473 \over \sqrt{R}}$">|; 

$key = q/displaystylerho;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img271.png"
 ALT="$\displaystyle \rho$">|; 

$key = q/c^2={kRT};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="72" HEIGHT="16" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img191.png"
 ALT="$ c^2 = {kRT}$">|; 

$key = q/{figure}htmlimage{scale=1.8,thumbnail=1.1}{centerline{includegraphics{calculationsslashfannoP2P1}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure16668"
 HREF="img1136.png"><IMG
 
 SRC="|."$dir".q|Timg1136.png"
 ALT="\begin{figure}\centerline{\includegraphics
{calculations/fannoP2P1}}\end{figure}"></A>|; 

$key = q/slash;;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img29.png"
 ALT="$ /;$">|; 

$key = q/18[bar];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="52" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img91.png"
 ALT="$ 18[bar]$">|; 

$key = q/{figure}htmlimage{scale=0.8,thumbnail=1.6}{centerline{includegraphics{contslashsoundslashillustrationsslashstationaryWave}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure1573"
 HREF="img59.png"><IMG
 WIDTH="342" HEIGHT="138"
 SRC="|."$dir".q|Timg59.png"
 ALT="\begin{figure}\centerline{\includegraphics
{cont/sound/illustrations/stationaryWave}}
\end{figure}"></A>|; 

$key = q/Mleft(1+{k-1over2}M^2right);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="118" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img261.png"
 ALT="$ M \left( 1 + {k-1 \over 2} M^2 \right)$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img262.png"
 ALT="$ A$">|; 

$key = q/displaystyledh+UdU=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="104" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img218.png"
 ALT="$\displaystyle dh + U dU = 0$">|; 

$key = q/T_{B}=0.60315132;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="122" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img280.png"
 ALT="$ T_{B} = 0.60315132$">|; 

$key = q/{figure}htmlimage{scale=0.8,thumbnail=0.7}{centerline{includegraphics{calculationsslashfiguresslashnozzleCompareArea}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure3739"
 HREF="img378.png"><IMG
 
 SRC="|."$dir".q|Timg378.png"
 ALT="\begin{figure}\centerline{\includegraphics{calculations/figures/nozzleCompareArea}}
\end{figure}"></A>|; 

$key = q/displaystylec=sqrt{kRT};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="78" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img84.png"
 ALT="$\displaystyle c = \sqrt{ k R T}$">|; 

$key = q/dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img54.png"
 ALT="$ dt$">|; 

$key = q/displaystyle=41.1416left[{kgoverm^3}right];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="118" HEIGHT="54" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img273.png"
 ALT="$\displaystyle = 41.1416 \left[{kg \over m^3 }\right]$">|; 

$key = q/displaystyle{dot{m}overA^{*}}=left({sqrt{k}P_0oversqrt{RT_{0}}}right)left(1+{k-1over2}right)^{-{k+1over2(k-1)}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="262" HEIGHT="67" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img291.png"
 ALT="$\displaystyle {\dot{m} \over A^{*}} = \left( {\sqrt{k} P_0 \over \sqrt{R T_{0}}} \right) \left( 1 + {k -1 \over 2} \right)^ {-{ k+ 1 \over 2(k -1)}}$">|; 

$key = q/C_p+mCover1+m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="55" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img175.png"
 ALT="$ C_p + mC \over 1 + m$">|; 

$key = q/displaystyle{drhooverrho}{RoverC_v}left[z+{T}left(partialzoverpartialTright)_{rh{dPoverP}{RoverC_p}left[z+{T}left(partialzoverpartialTright)_Pright];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="354" HEIGHT="64" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img126.png"
 ALT="$\displaystyle {d\rho \over \rho} {R \over C_v} \left[ z + { T }\left( \partial ...
...R \over C_p} \left[ z + {T }\left(\partial z \over \partial T \right)_P \right]$">|; 

$key = q/displaystyle{t_{corrected}overt}=sqrt{T_Aoverbar{T}}{2over3}{T_Aover(T_B-T_A)}left(left(T_BoverT_Aright)^{3over2}-1right);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="329" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img107.png"
 ALT="$\displaystyle {t_{corrected} \over t } = \sqrt{T_A \over \bar{T}} {2 \over 3} { T_A \over ( T_B - T_A )} \left( \left(T_B \over T_A\right)^{3\over 2}- 1 \right)$">|; 

$key = q/{meta}samplethispart.{meta};AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="116" HEIGHT="16" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img33.png"
 ALT="\begin{meta}
sample this part.
\end{meta}">|; 

$key = q/displaystyleM^{*}={Uoverc^{*}}=sqrt{k+1over2}Mleft(1-{k-1over4}M^2+cdotsright);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="321" HEIGHT="63" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img212.png"
 ALT="$\displaystyle M^{*} = {U \over c^{*} } = \sqrt{k+1 \over 2} M \left( 1 - {k -1 \over 4} M^2 + \cdots \right)$">|; 

$key = q/{figure}htmlimage{scale=1.8,thumbnail=1.3}{centerline{includegraphics{contslashvariableAreaslashexampleNozzle}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure3956"
 HREF="img414.png"><IMG
 
 SRC="|."$dir".q|Timg414.png"
 ALT="\begin{figure}\centerline{\includegraphics{cont/variableArea/exampleNozzle}}
\end{figure}"></A>|; 

$key = q/xi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img162.png"
 ALT="$ \xi$">|; 

$key = q/displaystyleX={{s-s_f(P_B})overs_{fg}(P_B)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="120" HEIGHT="55" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img181.png"
 ALT="$\displaystyle X = {{s - s_f (P_B}) \over s_{fg}(P_B) }$">|; 

$key = q/displaystyle{rhooverrho_a}=1+m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="87" HEIGHT="43" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img163.png"
 ALT="$\displaystyle {\rho \over \rho_a} = 1 + m$">|; 

$key = q/displaystyle{left.{T_{0}}right|_{A}overleft.{T_{0}}right|_{B}}=left[{{left.{BoveM_{A}}}strut}{{left.Aright|_{B}strut}overleft.Aright|_{A}}right]^{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="181" HEIGHT="72" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img338.png"
 ALT="$\displaystyle {\left.{T_{0}}\right\vert _{A} \over \left.{T_{0}}\right\vert _{B...
...{\left. A \right\vert _{B} \strut} \over \left. A \right\vert _{A}} \right]^{2}$">|; 

$key = q/displaystyle{dPoverrho}left(1-M^{2}right)=U^2{dAoverA};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="163" HEIGHT="52" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img230.png"
 ALT="$\displaystyle {dP \over \rho } \left( 1 -M^{2} \right) = U^2 {dA \over A}$">|; 

$key = q/displaystyle{RTover{P_0}^2}left(dot{m}overAright)^2=left(A^{*}P_0overAPright)^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="176" HEIGHT="61" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img318.png"
 ALT="$\displaystyle { R T \over {P_0}^2} \left( \dot{m} \over A \right) ^2 = \left( A^{*} P_0 \over A P \right)^2$">|; 

$key = q/displaystylerhoc=(rho+drho)(c-dU);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="157" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img60.png"
 ALT="$\displaystyle \rho c = (\rho + d\rho)(c-dU)$">|; 

$key = q/displaystyle{dot{m}overA}={PoverP_{0}};{P_{0}Uoversqrt{kRT}}sqrt{koverR}sqrt{{T_T_0}}Msqrt{koverR}overbrace{{PoverP_{0}}sqrt{{T_{0}overT}}}^{f(M,k)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="389" HEIGHT="113" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img288.png"
 ALT="$\displaystyle {\dot{m} \over A} = { P \over P_{0} } \; {P_{0} U \over \sqrt{kRT...
...rt{k \over R } \overbrace{{ P \over P_{0} } \sqrt{ {T_{0} \over T}} } ^{f(M,k)}$">|; 

$key = q/displaystyle{RT_0overP^2}left(dot{m}overAright)^2={1overk}overbrace{{{c_0}^2overeft(A^{*}overAright)^2left(P_0overPright)^2}^{{A^{*}P_0overAP}=f(M)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="320" HEIGHT="124" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img312.png"
 ALT="$\displaystyle { R T_0 \over P^2} \left( \dot{m} \over A \right) ^2 = { 1 \over ...
... \over A \right)^2 \left( P_0 \over P \right)^2}^{ {A^{*}P_0 \over AP} = f(M) }$">|; 

$key = q/mathaccentV{bar}016{t};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="10" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img20.png"
 ALT="$ \mathaccentV {bar}016{t}$">|; 

$key = q/displaystyleFn=kMleft(1+{k-1over2}M^2right)^{-{k+1over2(k-1)}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="240" HEIGHT="67" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img299.png"
 ALT="$\displaystyle Fn = kM\left( 1+ {k -1 \over 2}M^2 \right)^ {-{ k+ 1 \over 2(k -1)}}$">|; 

$key = q/displaystyle1+{kRU^{2}over2C_{p}c^{2}}={T_{0}overT};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="118" HEIGHT="57" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img193.png"
 ALT="$\displaystyle 1 + {k R U ^{2} \over 2 C_{p} c^{2} } = { T_{0} \over T}$">|; 

$key = q/{examl}Demonstratethatequationeqref{sound:eq:cvCombined2}canbederivedfromthemomentumequation.{examl};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="546" HEIGHT="16" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img73.png"
 ALT="\begin{examl}
Demonstrate that equation \eqref{sound:eq:cvCombined2} can be
derived from the momentum equation.
\end{examl}">|; 

$key = q/left[kJoverK;kgright];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="52" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img89.png"
 ALT="$ \left[ kJ \over K\; kg\right]$">|; 

$key = q/{figure}htmlimage{scale=1.8,thumbnail=1.7}{centerline{includegraphics{contslashobliqueslashinletSuction}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure23072"
 HREF="img1471.png"><IMG
 
 SRC="|."$dir".q|Timg1471.png"
 ALT="\begin{figure}\centerline{\includegraphics
{cont/oblique/inletSuction}}
\end{figure}"></A>|; 

$key = q/displaystyle{T_0overT}=1+{k-1over2}M^{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="138" HEIGHT="52" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img196.png"
 ALT="$\displaystyle {T_0 \over T} = 1 + { k -1 \over 2 } M^{2}$">|; 

$key = q/gamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img174.png"
 ALT="$ \gamma$">|; 

$key = q/335.7celsius;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="39" HEIGHT="16" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img93.png"
 ALT="$ 335.7 \celsius$">|; 

$key = q/{examl}Calculatethespeedofsoundofairat{30celsius{andatmosphericpressure{sim1[bardel(compressibilityfactor).Assumethat{R=287[jslashkgslashK]{.{examl};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="555" HEIGHT="73" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img134.png"
 ALT="\begin{examl}
Calculate the speed of sound of air
at $30\celsius$ and atmospher...
...as model
(compressibility factor).
Assume that $R = 287 [j /kg /K]$.
\end{examl}">|; 

$key = q/rhoRT;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="36" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img224.png"
 ALT="$ \rho R T$">|; 

$key = q/c_S;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img153.png"
 ALT="$ c_S$">|; 

$key = q/M<1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="51" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img235.png"
 ALT="$ M&lt;1$">|; 

$key = q/displaystylehat{rho}={1over2}left(1+sqrt{1+2{(k-1)Fn^2overk^2}left(A^{*}P_0overAPright)^2}right);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="310" HEIGHT="75" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img314.png"
 ALT="$\displaystyle \hat{\rho} = {1 \over 2} \left( 1 + \sqrt{1 + 2 {(k -1) Fn^2 \over k^2} \left( A^{*} P_0 \over A P \right) ^2 } \right)$">|; 

$key = q/{figure}htmlimage{scale=2.8,thumbnail=1.2}{centerline{includegraphics{contslashobliqueslashinletEx}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure23083"
 HREF="img1472.png"><IMG
 
 SRC="|."$dir".q|Timg1472.png"
 ALT="\begin{figure}\centerline{\includegraphics{cont/oblique/inletEx}}
\end{figure}"></A>|; 

$key = q/%latex2htmlidmarker37420displaystyle{P_{A}overP_{0}}=overbrace{P_{B}overP_{0}}^{c}@M=2}}times{P_{A}overP_{B}}=0.12780453times{2.0over1.5}=0.17040604;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="443" HEIGHT="143" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img279.png"
 ALT="% latex2html id marker 37420
$\displaystyle {P_{A} \over P_{0}} = \overbrace{P_...
...2}} \times {P_{A} \over P_{B}} = 0.12780453 \times {2.0 \over 1.5} = 0.17040604$">|; 

$key = q/displaystyleP=zrhoRT;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="78" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img110.png"
 ALT="$\displaystyle P = z\rho R T$">|; 

$key = q/displaystyledh=C_pdT-left[{Tvoverz}left(partialzoverpartialTright)_Pright]dP=C_pdT-{Toverz}left(partialzoverpartialTright)_P{dPoverrho}%label{eq:};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="409" HEIGHT="54" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img118.png"
 ALT="$\displaystyle dh = C_p dT - \left[ {T v \over z} \left( \partial z \over \parti...
...ver z}\left(\partial z \over \partial T \right)_P {dP \over \rho} %\label{eq:}
$">|; 

$key = q/drhoslashrho;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="37" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img124.png"
 ALT="$ d\rho/ \rho$">|; 

$key = q/displaystylerhodU=cdrhoLongrightarrowdU={cdrhooverrho};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="182" HEIGHT="52" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img62.png"
 ALT="$\displaystyle \rho d U = c d\rho \Longrightarrow dU = {c d \rho \over \rho}$">|; 

$key = q/M>1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="51" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img239.png"
 ALT="$ M&gt; 1$">|; 

$key = q/{figure}htmlimage{scale=2.8,thumbnail=1.9}{centerline{includegraphics{contslashPrandtl-MeyerslashthinAttack}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure28771"
 HREF="img1585.png"><IMG
 
 SRC="|."$dir".q|Timg1585.png"
 ALT="\begin{figure}\centerline{\includegraphics
{cont/Prandtl-Meyer/thinAttack}}
\end{figure}"></A>|; 

$key = q/displaystyle{1overR{rho_0}P}left(dot{m}overAright)^2={overbrace{kRT_0}^{{c_0}^{2^2{PoverP_0}}left(dot{m}overAright)^2={Fn^2overk}left(P_0overPright);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="490" HEIGHT="106" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img320.png"
 ALT="$\displaystyle { 1 \over R {\rho_0}P } \left( \dot{m} \over A \right) ^2 = {\ove...
...} \left( \dot{m} \over A \right) ^2 = {Fn^2 \over k} \left( P_0 \over P \right)$">|; 

$key = q/{figure}htmlimage{scale=2.8,thumbnail=1.4}{centerline{includegraphics{contslashshockVariableslashnozzle}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure10025"
 HREF="img665.png"><IMG
 
 SRC="|."$dir".q|Timg665.png"
 ALT="\begin{figure}\centerline{\includegraphics{cont/shockVariable/nozzle}}
\end{figure}"></A>|; 

$key = q/kequivC_{p}divC_{v};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="92" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img187.png"
 ALT="$ k \equiv C_{p} \div C_{v}$">|; 

$key = q/{figure}htmlimage{scale=0.8,thumbnail=0.8}{centerline{includegraphics{calculationsslashisothermalP2P1}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure13821"
 HREF="img917.png"><IMG
 
 SRC="|."$dir".q|Timg917.png"
 ALT="\begin{figure}\centerline{\includegraphics {calculations/isothermalP2P1}}
\end{figure}"></A>|; 

$key = q/displaystyle-cdU+{dPoverrho}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="118" HEIGHT="52" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img65.png"
 ALT="$\displaystyle -c dU + {dP \over \rho} = 0$">|; 

$key = q/M^2-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img264.png"
 ALT="$ M^2 -1$">|; 

$key = q/displaystyledt={dxoversqrt{kRT(x)}}={dxoversqrt{kRT_Aleft({(T_B-T_A)xoverT_Ah}+1right)}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="309" HEIGHT="82" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img104.png"
 ALT="$\displaystyle d t = {d x \over \sqrt{kRT(x)} } = {dx \over \sqrt{ kRT_A \left( {(T_B - T_A) x \over T_A h} +1 \right) } }$">|; 

$key = q/displaystyleleft(partialvoverpartialTright)_P=left(partialleft(zRToverPright)oveialTright)_P+{zRoverP}cancelto{1}{left(partialToverpartialTright)_P};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="400" HEIGHT="64" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img116.png"
 ALT="$\displaystyle \left( \partial v \over \partial T \right)_P = \left( \partial \l...
..._P + {zR \over P} \cancelto {1} { \left( \partial T \over \partial T \right)_P}$">|; 

$key = q/T=c^2slashkR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="80" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img192.png"
 ALT="$ T = c^2 /k R$">|; 

$key = q/{figure}htmlimage{scale=2.8,thumbnail=1.8}{centerline{includegraphics{contslashrayleighslashTs}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure19220"
 HREF="img1159.png"><IMG
 
 SRC="|."$dir".q|Timg1159.png"
 ALT="\begin{figure}\centerline{\includegraphics
{cont/rayleigh/Ts}}
\end{figure}"></A>|; 

$key = q/T_A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img99.png"
 ALT="$ T_A$">|; 

$key = q/displaystyle{A_{B}overA{*}}={A_{B}overA_{A}}times{A_{A}overA^{*}}={40over50}time{hbox{fromtheTableeqref{variableArea:tab:basicIsentropic}}}=1.272112;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="404" HEIGHT="88" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img285.png"
 ALT="$\displaystyle {A_{B} \over A{*} } = {A _{B} \over A_{A} } \times {A_{A} \over A...
...4 }^{\hbox{from the Table \eqref{variableArea:tab:basicIsentropic}}} = 1.272112$">|; 

$key = q/{figure}htmlimage{scale=2.8,thumbnail=1.9}{centerline{includegraphics{contslashPrandtl-MeyerslashangleDef}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure28557"
 HREF="img1529.png"><IMG
 
 SRC="|."$dir".q|Timg1529.png"
 ALT="\begin{figure}\centerline{\includegraphics
{cont/Prandtl-Meyer/angleDef}}
\end{figure}"></A>|; 

$key = q/displaystyleA={dot{m}overrhoU}=8.26times10^{-5}[m^{3}];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="194" HEIGHT="51" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img275.png"
 ALT="$\displaystyle A = {\dot {m} \over \rho U} = 8.26 \times 10^{-5} [m^{3}]
$">|; 

$key = q/displaystyle{rho_0-rhooverrho}={M^2over2}left(1-{kM^2over4}+cdotsright);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="232" HEIGHT="57" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img214.png"
 ALT="$\displaystyle {\rho_0 -\rho \over \rho} = {M^2 \over 2} \left( 1 - {kM^2 \over 4} + \cdots \right)$">|; 

$key = q/25^{circ}C;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img148.png"
 ALT="$ 25 ^{\circ}C$">|; 

$key = q/0geqdelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="41" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img25.png"
 ALT="$ 0 \geq \delta $">|; 

$key = q/displaystyle{P_0overP}=1+{(k-1)M^2over4}+{kM^4over8}+{2(2-k)M^6over48}cdots;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="335" HEIGHT="57" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img208.png"
 ALT="$\displaystyle {P_0\over P} = 1 + {(k -1) M^2 \over 4} + {k M^4\over 8} + {2(2-k)M^6 \over 48} \cdots$">|; 

$key = q/{figure}htmlimage{scale=2.8,thumbnail=1.8}{centerline{includegraphics{contslashPrandtl-MeyerslashoverMaxAngle}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure28747"
 HREF="img1582.png"><IMG
 
 SRC="|."$dir".q|Timg1582.png"
 ALT="\begin{figure}\centerline{\includegraphics
{cont/Prandtl-Meyer/overMaxAngle}}
\end{figure}"></A>|; 

$key = q/20^{circ}C;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img147.png"
 ALT="$ 20^{\circ}C$">|; 

$key = q/displaystylerho^2-{PrhooverT_0R}-{k-1over2kRT_0}left(dot{m}overAright)^2=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="221" HEIGHT="61" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img307.png"
 ALT="$\displaystyle \rho^2 - {P \rho \over T_0 R} - {k -1 \over 2kRT_0} \left( \dot{m} \over A \right) ^2 = 0$">|; 

$key = q/{figure}htmlimage{scale=2.2,thumbnail=1.9}{centerline{includegraphics{contslashtankslashtwoModel}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure20489"
 HREF="img1201.png"><IMG
 
 SRC="|."$dir".q|Timg1201.png"
 ALT="\begin{figure}\centerline{\includegraphics{cont/tank/twoModel}}
\end{figure}"></A>|; 

$key = q/dM=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="59" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img246.png"
 ALT="$ dM= 0$">|; 

$key = q/{figure}htmlimage{scale=0.8,thumbnail=0.6}{centerline{includegraphics{contslashsoundslashCompressiblityChart}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure1711"
 HREF="img109.png"><IMG
 WIDTH="365" HEIGHT="439"
 SRC="|."$dir".q|Timg109.png"
 ALT="\begin{figure}\centerline{\includegraphics
{cont/sound/CompressiblityChart}}
\end{figure}"></A>|; 

$key = q/displaystyle{dPoverrho}-U^{2}overbrace{left[{dAoverA}+{drhooverrho}right]}^{dUoverU}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="181" HEIGHT="111" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img226.png"
 ALT="$\displaystyle {dP \over \rho} - U^{2} \overbrace{\left[ {dA \over A} +{d\rho \over \rho} \right]}^{dU \over U} = 0$">|; 

$key = q/{figure}htmlimage{scale=2.8,thumbnail=0.7}{centerline{includegraphics{contslashobliqueslashbullet}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure22935"
 HREF="img1443.png"><IMG
 
 SRC="|."$dir".q|Timg1443.png"
 ALT="\begin{figure}\centerline{\includegraphics{cont/oblique/bullet}}
\end{figure}"></A>|; 

$key = q/displaystyleTds=dh-{dPoverrho};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="114" HEIGHT="52" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img111.png"
 ALT="$\displaystyle Tds = dh - {dP \over \rho}$">|; 

$key = q/sim;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img94.png"
 ALT="$ \sim$">|; 

$key = q/c_{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img152.png"
 ALT="$ c_{T}$">|; 

$key = q/displaystyle{dot{m}sqrt{T_0}overA^{*}P_0}={dot{m}overbrace{sqrt{kRT_0}}^{c_0}oveA^{*}P_0}=overbrace{dot{m}c_0oversqrt{R}A^{*}P_0}^{Fn}{1oversqrt{k}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="263" HEIGHT="104" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img297.png"
 ALT="$\displaystyle { \dot{m} \sqrt{T_0}\over A^{*} P_0 } = {\dot{m} \overbrace{\sqrt...
...} P_0} = \overbrace{\dot{m} c_0 \over \sqrt{R}A^{*} P_0 }^{Fn} {1\over\sqrt{k}}$">|; 

$key = q/dPrightarrowinfty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="66" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img244.png"
 ALT="$ dP \rightarrow \infty$">|; 

$key = q/displaystyledT_0=0=dTleft(1+{k-1over2}M^2right)+T(k-1)MdM;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="339" HEIGHT="54" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img253.png"
 ALT="$\displaystyle dT_0 = 0 = dT \left( 1 + { k - 1 \over 2 } M^2 \right) + T (k -1) M dM$">|; 

$key = q/dA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img234.png"
 ALT="$ dA$">|; 

$key = q/displaystyle0=overbrace{mC{dToverT}}^{droplets}+overbrace{C_p{dToverT}-R{dPoverP}}^{gas}={(C_p+mC)dToverT}-R{dPoverP};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="378" HEIGHT="101" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img169.png"
 ALT="$\displaystyle 0 = \overbrace{mC{dT \over T}}^{droplets} + \overbrace{C_p {dT \over T} -R {dP \over P}}^{gas} = {(C_p + mC) dT \over T} -R {dP \over P}$">|; 

$key = q/displaystyle{P_0overP}=left({T_0overT}right)^{koverk-1}=left(1+{k-1over2}M^{2}right)^{koverk-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="277" HEIGHT="66" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img199.png"
 ALT="$\displaystyle {P_0 \over P } = \left( { T_0 \over T} \right) ^ {k \over k -1} = \left( 1 + { k -1 \over 2 } M^{2} \right)^ {k \over k -1}$">|; 

$key = q/tanbeta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img39.png"
 ALT="$ \tan\beta$">|; 

$key = q/{figure}htmlimage{scale=0.8,thumbnail=1.2}{centerline{includegraphics{photosslasheckertW}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure1066"
 HREF="img52.png"><IMG
 WIDTH="284" HEIGHT="189"
 SRC="|."$dir".q|Timg52.png"
 ALT="\begin{figure}\centerline{\includegraphics
{photos/eckertW}}
\end{figure}"></A>|; 

$key = q/displaystyle{Poverrho}={Rover1+m}T;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="99" HEIGHT="53" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img165.png"
 ALT="$\displaystyle {P \over \rho} = { R \over 1 + m} T$">|; 

$key = q/dUdrho;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img61.png"
 ALT="$ dUd\rho$">|; 

$key = q/displaystyle=left.{P_{0}}{A^{*}}right|_{B};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="75" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img333.png"
 ALT="$\displaystyle = \left. {P_{0}} {A^{*}}\right\vert _{B}$">|; 

$key = q/displaystylec=sqrt{DeltaPoverDeltarho}_{s=constant};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="145" HEIGHT="68" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img86.png"
 ALT="$\displaystyle c = \sqrt{\Delta P \over \Delta \rho}_{s=constant}$">|; 

$key = q/P_2overP_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img16.png"
 ALT="$ P_2 \over P_1$">|; 

$key = q/sqrt{0.3over1.4};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="41" HEIGHT="49" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img140.png"
 ALT="$ \sqrt{0.3 \over 1.4}$">|; 

$key = q/displaystyleT_{B}=T_{A}timesoverbrace{T_{0}overT_{A}}^{M=2}timesoverbrace{T_{B}o=1.81..}=250[K]times{1over0.55555556}times{0.60315132}simeq271.42[K];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="538" HEIGHT="99" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img283.png"
 ALT="$\displaystyle T_{B} = T_{A}\times \overbrace{T_{0} \over T_{A} }^{M=2} \times \...
.....} = 250 [K] \times {1 \over 0.55555556} \times {0.60315132} \simeq 271.42 [K]$">|; 

$key = q/{examl}Gasflowsthroughaconverging--divergingduct.Atpoint``A''thecrosssectionareaAssumethattheflowisisentropicandthegasspecificheatratiois1.4.{examl};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="554" HEIGHT="73" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img284.png"
 ALT="\begin{examl}
Gas flows through a converging-diverging duct.
At point \lq\lq A'' the...
...that the flow is isentropic and the gas specific heat ratio
is 1.4.
\end{examl}">|; 

$key = q/RT;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="28" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img83.png"
 ALT="$ RT$">|; 

$key = q/displaystylec=sqrt{gammaR_{mix}T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="105" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img177.png"
 ALT="$\displaystyle c = \sqrt{\gamma R_{mix} T}$">|; 

$key = q/{figure}htmlimage{scale=0.8,thumbnail=0.6}{centerline{includegraphics{calculationsslashfiguresslashisoNozzle}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure3700"
 HREF="img372.png"><IMG
 
 SRC="|."$dir".q|Timg372.png"
 ALT="\begin{figure}\centerline{\includegraphics
{calculations/figures/isoNozzle}}
\end{figure}"></A>|; 

$key = q/unhboxvoidb@xhbox{frac{4fL}{D}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="52" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img11.png"
 ALT="$ \unhbox \voidb@x \hbox {$\frac {4fL}{D}$}$">|; 

$key = q/{figure}htmlimage{scale=0.8,thumbnail=0.7}{centerline{includegraphics{contslashobliqueslashdetachWedge.eps}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure23280"
 HREF="img1478.png"><IMG
 
 SRC="|."$dir".q|Timg1478.png"
 ALT="\begin{figure}\centerline{\includegraphics {cont/oblique/detachWedge.eps}}
\end{figure}"></A>|; 

$key = q/displaystyledh=C_pdT+left[v-Tleft(partialvoverpartialTright)_Pright];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="219" HEIGHT="54" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img114.png"
 ALT="$\displaystyle dh = C_p dT + \left[ v -T \left( \partial v \over \partial T \right)_P \right]$">|; 

$key = q/M_{upstream};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="76" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img4.png"
 ALT="$ M_{upstream}$">|; 

$key = q/s;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img70.png"
 ALT="$ s$">|; 

$key = q/displaystyledP={rhoU^{2}overA};{dAover1-M^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="138" HEIGHT="57" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img231.png"
 ALT="$\displaystyle dP = {\rho U^{2} \over A} \; {dA \over 1 -M^2}$">|; 

$key = q/(T=constant);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="111" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img9.png"
 ALT="$ (T=constant)$">|; 

$key = q/T_{0}=constant;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="103" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img266.png"
 ALT="$ T_{0} = constant$">|; 

$key = q/{figure}htmlimage{scale=2.4,thumbnail=1.2}{centerline{includegraphics{contslashobliqueslashregions}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure22297"
 HREF="img1326.png"><IMG
 
 SRC="|."$dir".q|Timg1326.png"
 ALT="\begin{figure}\centerline{\includegraphics{cont/oblique/regions}}
\end{figure}"></A>|; 

$key = q/{figure}htmlimage{scale=2.8,thumbnail=1.9}{centerline{includegraphics{contslashPrandtl-MeyerslashMachLineAngle}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure28567"
 HREF="img1530.png"><IMG
 
 SRC="|."$dir".q|Timg1530.png"
 ALT="\begin{figure}\centerline{\includegraphics{cont/Prandtl-Meyer/MachLineAngle}}
\end{figure}"></A>|; 

$key = q/dm;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img55.png"
 ALT="$ dm$">|; 

$key = q/displaystyle{Pover{rho_a}^{k}}=constant;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="114" HEIGHT="53" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img168.png"
 ALT="$\displaystyle {P \over {\rho_a}^{k} } = constant$">|; 

$key = q/A^{*};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img290.png"
 ALT="$ A^{*}$">|; 

$key = q/displaystyleP_0={Pover{PslashP_0}}=1.5slash0.98116=1.5288[Bar];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="289" HEIGHT="53" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img327.png"
 ALT="$\displaystyle P_0 = {P \over {P / P_0}} = 1.5 / 0.98116 = 1.5288 [Bar]
$">|; 

$key = q/displaystylebar{P}={PoverP_0}={3[MPa]over5[MPa]}=0.6;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="181" HEIGHT="55" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img268.png"
 ALT="$\displaystyle \bar{P} = {P \over P_0} = {3 [MPa] \over 5 [MPa] } = 0.6
$">|; 

$key = q/M_{exit};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="43" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img2.png"
 ALT="$ M_{exit}$">|; 

$key = q/displaystyledA<0LongrightarrowdP<0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="138" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img238.png"
 ALT="$\displaystyle dA &lt; 0 \Longrightarrow dP&lt; 0$">|; 

$key = q/{figure}htmlimage{scale=2.8,thumbnail=1.9}{centerline{includegraphics{scanedslashzucorow}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure6655"
 HREF="img545.png"><IMG
 
 SRC="|."$dir".q|Timg545.png"
 ALT="\begin{figure}\centerline{\includegraphics {scaned/zucorow}}
\end{figure}"></A>|; 

$key = q/displaystyle=ktimes{overbrace{constanttimesrho^k}^{P}overrho}hfill;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="150" HEIGHT="106" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img80.png"
 ALT="$\displaystyle =k \times {\overbrace{ constant \times \rho^k}^{P} \over \rho} \hfill$">|; 

$key = q/z=0.3;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="54" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img138.png"
 ALT="$ z=0.3$">|; 

$key = q/PAoverP_0A^{*};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="39" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img319.png"
 ALT="$ P A \over P_0 A^{*}$">|; 

$key = q/displaystylec(S,T,P)=c_0+c_T+c_S+c_P+c_{STP},;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="274" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img150.png"
 ALT="$\displaystyle c(S,T,P) = c_0 + c_T + c_S + c_P + c_{STP},$">|; 

$key = q/{rhoU^{2}slashA};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="51" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img232.png"
 ALT="$ {\rho U^{2} / A}$">|; 

$key = q/m={dot{m}_boverdot{m}_a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="62" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img164.png"
 ALT="$ m = {\dot{m}_b \over \dot{m}_a}$">|; 

$key = q/{figure}htmlimage{scale=0.8,thumbnail=0.6}{centerline{includegraphics{calculationsslashfannoM1M2Supper}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure16254"
 HREF="img1064.png"><IMG
 
 SRC="|."$dir".q|Timg1064.png"
 ALT="\begin{figure}\centerline{\includegraphics
{calculations/fannoM1M2Supper}}\end{figure}"></A>|; 

$key = q/displaystyle{drhooverrho}+{dAoverA}+{dUoverU}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="142" HEIGHT="52" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img220.png"
 ALT="$\displaystyle {d\rho \over \rho} + { dA \over A} + {dU \over U} = 0$">|; 

$key = q/displaystyleR=C_{p}-C_{v};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="95" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img186.png"
 ALT="$\displaystyle R = C_{p} -C_{v}$">|; 

$key = q/{figure}htmlimage{scale=0.8,thumbnail=0.7}{centerline{includegraphics{scanedslashde}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure911"
 HREF="img40.png"><IMG
 WIDTH="256" HEIGHT="243"
 SRC="|."$dir".q|Timg40.png"
 ALT="\begin{figure}\centerline{\includegraphics {scaned/de}}
\end{figure}"></A>|; 

$key = q/M=dot{m}slashArhoc;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="91" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img303.png"
 ALT="$ M = \dot{m} / A \rho c$">|; 

$key = q/displaystylec^{2}=Pxy{P}{rho}=Pxy{P[f(X)]}{rho};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="147" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img179.png"
 ALT="$\displaystyle c^{2} = \Pxy{P}{\rho} = \Pxy{P[f(X)]} {\rho}$">|; 

$key = q/displaystyle{{Fn}^2overk}=left(ToverT_0right)left(P_0overPright)^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="157" HEIGHT="61" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img317.png"
 ALT="$\displaystyle {{Fn}^2 \over k} = \left( T \over T_0 \right) \left( P_0 \over P \right)^2$">|; 

$key = q/displaystylec=sqrt{kRT}simsqrt{1.327times461times(350+273)}sim771.5left[moversecright];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="395" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img97.png"
 ALT="$\displaystyle c = \sqrt{kRT} \sim \sqrt{ 1.327 \times 461 \times
(350 + 273)} \sim
771.5 \left[ m \over sec \right]
$">|; 

$key = q/P=P(rho,s);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="86" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img69.png"
 ALT="$ P= P(\rho, s)$">|; 

$key = q/displaystyledP=left.{Pxy{P}{rho}}right|_{s}drho+left.{Pxy{P}{s}}right|_{rho}ds;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="171" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img71.png"
 ALT="$\displaystyle dP = \left. {\Pxy{P}{\rho} } \right\vert _{s} d\rho + \left. {\Pxy{P}{s} } \right\vert _{\rho} ds$">|; 

$key = q/{figure}htmlimage{scale=1.8,thumbnail=1.6}{centerline{includegraphics{contslashisothermalslashcv}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure12530"
 HREF="img769.png"><IMG
 
 SRC="|."$dir".q|Timg769.png"
 ALT="\begin{figure}\centerline{\includegraphics{cont/isothermal/cv}}
\end{figure}"></A>|; 

$key = q/{figure}htmlimage{scale=1.8,thumbnail=1.5}{centerline{includegraphics{scanedslashfanno}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure946"
 HREF="img43.png"><IMG
 WIDTH="246" HEIGHT="246"
 SRC="|."$dir".q|Timg43.png"
 ALT="\begin{figure}\centerline{\includegraphics{scaned/fanno}}
\end{figure}"></A>|; 

$key = q/M_{in};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="33" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img12.png"
 ALT="$ M_{in}$">|; 

$key = q/displaystyle={left.{A^*}right|_{A}overleft.{A^*}right|_{B}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="62" HEIGHT="54" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img335.png"
 ALT="$\displaystyle = {\left.{A^*} \right\vert _{A} \over \left.{A^*} \right\vert _{B}}$">|; 

$key = q/{A_{B}overA{*}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img286.png"
 ALT="$ {A_{B} \over A{*} } $">|; 

$key = q/displaystyle{rho_0overrho}=left({T_0overT}right)^{1overk-1}=left(1+{k-1over2}M^{2}right)^{1overk-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="275" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img200.png"
 ALT="$\displaystyle {\rho_0 \over \rho } = \left( { T_0 \over T} \right) ^ {1 \over k -1} = \left( 1 + { k -1 \over 2 } M^{2} \right)^ {1 \over k -1}$">|; 

$key = q/displaystylegamma={C_p+mCoverC_v+mC};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="103" HEIGHT="53" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img173.png"
 ALT="$\displaystyle \gamma = {C_p + mC \over C_v + mC}$">|; 

$key = q/R=287jslashkgK;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="109" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img295.png"
 ALT="$ R=287 j/kg K$">|; 

$key = q/1slashsqrt{k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img41.png"
 ALT="$ 1/\sqrt{k}$">|; 

$key = q/{figure}htmlimage{scale=0.8,thumbnail=1.5}{centerline{includegraphics{contslashpistonslashcv}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure21861"
 HREF="img1292.png"><IMG
 
 SRC="|."$dir".q|Timg1292.png"
 ALT="\begin{figure}\centerline{\includegraphics
{cont/piston/cv}}
\end{figure}"></A>|; 

$key = q/displaystyledA>0RightarrowdP<0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="128" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img241.png"
 ALT="$\displaystyle dA &gt; 0 \Rightarrow dP&lt; 0$">|; 

$key = q/M_y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="28" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img27.png"
 ALT="$ M_y$">|; 

$key = q/{longtable}%latex2htmlidmarker3338{|l|l|l|l|l|l|l|}{caption[Fliegner'snumberafun&4.804hline1.000&2.419&2.046&3.583&14.98&7.913&5.016hline{longtable};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="557" HEIGHT="2683" BORDER="0"
 SRC="|."$dir".q|img322.png"
 ALT="\begin{longtable}
% latex2html id marker 3338
{\vert l\vert l\vert l\vert l\vert...
...\hline 1.000&amp; 2.419&amp; 2.046&amp;
3.583&amp; 14.98&amp; 7.913&amp; 5.016 \ \hline
\end{longtable}">|; 

$key = q/C_p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img34.png"
 ALT="$ C_p$">|; 

$key = q/{figure}htmlimage{scale=0.8,thumbnail=0.9}{centerline{includegraphics{contslashvariableAreaslashnozzle}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure2567"
 HREF="img182.png"><IMG
 WIDTH="249" HEIGHT="297"
 SRC="|."$dir".q|Timg182.png"
 ALT="\begin{figure}\centerline{\includegraphics {cont/variableArea/nozzle}}
\end{figure}"></A>|; 

$key = q/4times10^7;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="55" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img143.png"
 ALT="$ 4 \times 10^7$">|; 

$key = q/2.2times10^9;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="67" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img141.png"
 ALT="$ 2.2 \times 10^9$">|; 

$key = q/m=constant;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="101" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img167.png"
 ALT="$ m=constant$">|; 

$key = q/{figure}htmlimage{scale=1.0,thumbnail=0.6}{centerline{includegraphics{photosslashrankine}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure1033"
 HREF="img49.png"><IMG
 WIDTH="107" HEIGHT="130"
 SRC="|."$dir".q|Timg49.png"
 ALT="\begin{figure}\centerline{\includegraphics
{photos/rankine}}
\end{figure}"></A>|; 

$key = q/displaystyleU=Mc=sqrt{kRT}M=0.17timessqrt{1.3times287times300times}sim56.87[mslashsec];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="458" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img326.png"
 ALT="$\displaystyle U = Mc = \sqrt{kRT} M = 0.17\times \sqrt{1.3\times 287 \times 300\times }
\sim 56.87 [m/sec]
$">|; 

$key = q/displaystyledot{m}=A^{*}sqrt{{koverRT_{0}}left(2overk+1right)^{k+1overk-1}}P_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="216" HEIGHT="76" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img331.png"
 ALT="$\displaystyle \dot{m} = A ^{*} \sqrt{{k \over R T_{0} } \left( 2 \over k +1 \right)^{k+1 \over k -1}} P_0$">|; 

$key = q/10^9;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="28" HEIGHT="16" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img156.png"
 ALT="$ 10^9$">|; 

$key = q/dU;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img66.png"
 ALT="$ dU$">|; 

$key = q/T_{0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img185.png"
 ALT="$ T_{0}$">|; 

$key = q/APoverA^{*}P_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="39" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img315.png"
 ALT="$ A P \over A^{*} P_0$">|; 

$key = q/displaystyleC_{p}T+{U^2over2}=C_{p}T_{0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="134" HEIGHT="57" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img184.png"
 ALT="$\displaystyle C_{p} T + {U^2 \over 2} = C_{p} T_{0}$">|; 

$key = q/350celsius;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="28" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img88.png"
 ALT="$ 350 \celsius$">|; 

$key = q/dTslashT;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="43" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img122.png"
 ALT="$ dT / T$">|; 

$key = q/{figure}htmlimage{scale=0.8,thumbnail=0.76}{centerline{includegraphics{photosslashEMach}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure1007"
 HREF="img46.png"><IMG
 WIDTH="230" HEIGHT="190"
 SRC="|."$dir".q|Timg46.png"
 ALT="\begin{figure}\centerline{\includegraphics
{photos/EMach}}
\end{figure}"></A>|; 

$key = q/displaystyleleft({kP_0A^{*}overRT_{0}}right)left(1+{k-1over2}M^{2}right)^{-{k-1over2(k-1)}}=constant%{left({kP_0A^{*}overRT_{0}}right);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="326" HEIGHT="67" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img329.png"
 ALT="$\displaystyle \left( {k P_0 A^{*}\over R T_{0}} \right) \left( 1 + {k -1 \over ...
... k- 1 \over 2(k -1)}} = constant %{\left( {k P_0 A^{*} \over R T_{0}} \right)
$">|; 

$key = q/displaystyle{drhooverrho}={dPoverP}{C_voverC_p}left[z+{T}left(partialzoverpartialTright)_Poverz+{T}left(partialzoverpartialTright)_{rho}right];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="206" HEIGHT="64" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img127.png"
 ALT="$\displaystyle {d\rho \over \rho} = {dP \over P } {C_v \over C_p } \left[ z + {T...
...ght)_P \over z + { T }\left( \partial z \over \partial T \right)_{\rho} \right]$">|; 

$key = q/M_2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img15.png"
 ALT="$ M_2$">|; 

$key = q/displaystyle1+{U^{2}over2C_{p}T}={T_{0}overT};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="115" HEIGHT="57" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img190.png"
 ALT="$\displaystyle 1 + { U ^{2} \over 2 C_{p} T} = { T_{0} \over T}$">|; 

$key = q/rho={PoverRT_0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="61" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img309.png"
 ALT="$ \rho = {P \over R T_0}$">|; 

$key = q/{P_{B}overP_{0}}=0.17040879;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="123" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img281.png"
 ALT="$ {P_{B} \over P_{0} }= 0.17040879$">|; 

$key = q/30^circ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="28" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img23.png"
 ALT="$ 30^\circ $">|; 

$key = q/{figure}htmlimage{scale=0.8,thumbnail=0.5}{centerline{includegraphics{photosslashprandtl}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure1051"
 HREF="img51.png"><IMG
 WIDTH="109" HEIGHT="127"
 SRC="|."$dir".q|Timg51.png"
 ALT="\begin{figure}\centerline{\includegraphics
{photos/prandtl}}
\end{figure}"></A>|; 

$key = q/1-M^{2}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="85" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img243.png"
 ALT="$ 1 - M^{2}= 0$">|; 

$key = q/sintheta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img38.png"
 ALT="$ \sin\theta$">|; 

$key = q/{figure}htmlimage{scale=2.8,thumbnail=1.3}{centerline{includegraphics{contslashvariableAreaslashMvA}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure2967"
 HREF="img260.png"><IMG
 WIDTH="370" HEIGHT="319"
 SRC="|."$dir".q|Timg260.png"
 ALT="\begin{figure}\centerline{\includegraphics {cont/variableArea/MvA}}
\end{figure}"></A>|; 

$key = q/rhoU^{2}=kMP;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="92" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img257.png"
 ALT="$ \rho U ^{2} = k M P$">|; 

$key = q/mathaccentV{bar}016{P};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="16" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img19.png"
 ALT="$ \mathaccentV {bar}016{P}$">|; 

$key = q/displaystyleh+{U^2over2}=h_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="94" HEIGHT="57" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img183.png"
 ALT="$\displaystyle h +{U^2 \over 2} = h_0$">|; 

$key = q/dA=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="54" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img245.png"
 ALT="$ dA = 0$">|; 

$key = q/{figure}htmlimage{scale=8.8,thumbnail=1.9}{centerline{includegraphics{contslashobliqueslashlimitedTheta}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure22847"
 HREF="img1435.png"><IMG
 
 SRC="|."$dir".q|Timg1435.png"
 ALT="\begin{figure}\centerline{ \includegraphics
{cont/oblique/limitedTheta}}\end{figure}"></A>|; 

$key = q/R=C_p-C_v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="95" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img172.png"
 ALT="$ R = C_p - C_v$">|; 

$key = q/displaystylec=sqrt{Dxy{P}{rho}}=ktimesconstanttimesrho^{k-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="231" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img79.png"
 ALT="$\displaystyle c = \sqrt{\Dxy{P}{\rho}} = k \times constant \times \rho^{k-1}$">|; 

$key = q/displaystyle{T^{*}overT_0}={{c^{*}}^2over{c_0}^2}={2overk+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="135" HEIGHT="58" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img202.png"
 ALT="$\displaystyle {T^{*} \over T_0} = {{c^{*}}^2 \over {c_0}^2} = {2 \over k+1}$">|; 

$key = q/{figure}htmlimage{scale=0.8,thumbnail=0.7}{centerline{includegraphics{photosslashfanno}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure1043"
 HREF="img50.png"><IMG
 WIDTH="111" HEIGHT="135"
 SRC="|."$dir".q|Timg50.png"
 ALT="\begin{figure}\centerline{\includegraphics
{photos/fanno}}
\end{figure}"></A>|; 

$key = q/displaystylekM^2P=koverbrace{U^{2}overc^2}^{M^2}overbrace{rhoRT}^{P}=k{U^{2}overkRT}overbrace{rhoRT}^{P}=rhoU^{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="296" HEIGHT="108" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img258.png"
 ALT="$\displaystyle k M^2 P = k \overbrace{ U^{2} \over c^2}^{M^2} \overbrace{\rho R T}^{P} = k { U ^{2} \over kRT} \overbrace{\rho R T}^{P} = \rho U ^{2}$">|; 

$key = q/displaystyle{dToverT}={dPoverP}{RoverC_p}left[z+{T}left(partialzoverpartialTright)_Pright];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="216" HEIGHT="54" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img121.png"
 ALT="$\displaystyle {dT \over T} = {dP \over P } {R \over C_p} \left[ z + {T }\left(\partial z \over \partial T \right)_P \right]$">|; 

$key = q/{figure}htmlimage{scale=0.8,thumbnail=0.7}{centerline{includegraphics{calculationsslashfannoM1M2}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure16241"
 HREF="img1062.png"><IMG
 
 SRC="|."$dir".q|Timg1062.png"
 ALT="\begin{figure}\centerline{\includegraphics
{calculations/fannoM1M2}}\end{figure}"></A>|; 

$key = q/m^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="16" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img157.png"
 ALT="$ m^2$">|; 

$key = q/{figure}htmlimage{scale=1.8,thumbnail=0.8}{centerline{includegraphics{contslashobliqueslashflowView.eps}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure22530"
 HREF="img1378.png"><IMG
 
 SRC="|."$dir".q|Timg1378.png"
 ALT="\begin{figure}\centerline{\includegraphics
{cont/oblique/flowView.eps}}
\end{figure}"></A>|; 

$key = q/displaystylec=sqrt{200000over0.32823}=780.5left[moversecright];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="205" HEIGHT="62" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img96.png"
 ALT="$\displaystyle c = \sqrt{ 200000 \over 0.32823} = 780.5 \left[ m \over sec \right]$">|; 

$key = q/{figure}htmlimage{scale=2.8,thumbnail=0.9}{centerline{includegraphics{calculationsslashfanno}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure15801"
 HREF="img986.png"><IMG
 
 SRC="|."$dir".q|Timg986.png"
 ALT="\begin{figure}\centerline{\includegraphics
{calculations/fanno}}
\end{figure}"></A>|; 

$key = q/displaystyleleft.{P_{0}}right|_{A}=left.Pover{underbrace{left({PoverP_{0}}right)}_{M=2.5}}right|_{A}={3over0.058527663}=51.25781291[Bar];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="378" HEIGHT="140" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img340.png"
 ALT="$\displaystyle \left. {P_{0}}\right\vert _{A} = \left. P \over {\underbrace{\lef...
...right)}_{M=2.5} } \right\vert _{A} = { 3 \over 0.058527663 } = 51.25781291[Bar]$">|; 

$key = q/T_B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img100.png"
 ALT="$ T_B$">|; 

$key = q/displaystyleleft(dot{m}overA^{*}right)_{max}{P_0oversqrt{T_0}}=sqrt{koverR}left({k+1over2}right)^{-{k+1over2(k-1)}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="281" HEIGHT="67" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img293.png"
 ALT="$\displaystyle \left(\dot{m} \over A^{*}\right)_{max} { P_0 \over \sqrt{T_0}} = \sqrt{k \over R} \left( {k +1 \over 2} \right)^ {-{ k+ 1 \over 2(k -1)}}$">|; 

$key = q/displaystyle1+{{k-1}over2};{U^{2}overc^{2}}={T_{0}overT};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="141" HEIGHT="57" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img194.png"
 ALT="$\displaystyle 1 + {{k -1} \over 2} \; { U ^{2} \over c^{2} } = { T_{0} \over T}$">|; 

$key = q/displaystyledot{Q}=C_{p}({T_{0}}_{B}-{T_{0}}_{A})dot{m};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="157" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img198.png"
 ALT="$\displaystyle \dot{Q} = C_{p} ({T_{0}}_{B} - {T_{0}}_{A}) \dot{m}$">|; 

$key = q/displaystyle{1overrho_m}={xioverrho_a}+{1-xioverrho_b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="128" HEIGHT="52" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img160.png"
 ALT="$\displaystyle {1 \over \rho_m} = { \xi \over \rho_a} + { 1 - \xi \over \rho_b}$">|; 

$key = q/displaystyle{rho^{*}overrho_0}=left(2overk+1right)^{1overk-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="133" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img204.png"
 ALT="$\displaystyle {\rho^{*} \over \rho_0} = \left(2 \over k+1 \right)^{1 \over k-1}$">|; 

$key = q/v=zRTslashP;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="86" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img115.png"
 ALT="$ v = zRT/P$">|; 

$key = q/rhoAU=dot{m}=constant;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="155" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img219.png"
 ALT="$ \rho A U = \dot{m} = constant$">|; 

$key = q/{examl}Calculatethespeedofsoundinwatervaporat{20[bar]{and{350celsius{,(a)utilizesthesteamtable(b)assumingidealgas.{examl};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="554" HEIGHT="35" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img85.png"
 ALT="\begin{examl}
Calculate the speed of sound in water vapor at $20 [bar]$ and $350
\celsius$, (a) utilizes the steam table
(b) assuming ideal gas.
\end{examl}">|; 

$key = q/{figure}htmlimage{scale=2.8,thumbnail=1.5}{centerline{includegraphics{scanedslashshock}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure889"
 HREF="img37.png"><IMG
 WIDTH="290" HEIGHT="320"
 SRC="|."$dir".q|Timg37.png"
 ALT="\begin{figure}\centerline{\includegraphics{scaned/shock}}
\end{figure}"></A>|; 

$key = q/{figure}htmlimage{scale=1.8,thumbnail=1.2}{centerline{includegraphics{contslashfannoslashtubePressureLoss}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure16285"
 HREF="img1070.png"><IMG
 
 SRC="|."$dir".q|Timg1070.png"
 ALT="\begin{figure}\centerline{\includegraphics
{cont/fanno/tubePressureLoss}}\end{figure}"></A>|; 

$key = q/A^{*}={A^{*}overA}A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="77" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img336.png"
 ALT="$ A^{*} = {A^{*} \over A} A$">|; 

$key = q/T;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img302.png"
 ALT="$ T$">|; 

$key = q/displaystylerho=halfleft[{PoverRT_0}+sqrt{left(PoverRT_0right)^2+underbrace{2{k-ot{m}overAright)^2}_{hookrightarrow(Mrightarrow0)rightarrow0}}right];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="307" HEIGHT="113" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img308.png"
 ALT="$\displaystyle \rho = \half \left[ { P \over R T_0} + \sqrt{ \left( P \over RT_0...
...} \over A \right)^2}_{\hookrightarrow (M\rightarrow 0 ) \rightarrow 0}} \right]$">|; 

$key = q/f(P_{0},T_{0},A^{*})=constant;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="176" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img330.png"
 ALT="$ f(P_{0}, T_{0},A^{*}) = constant$">|; 

$key = q/{figure}htmlimage{scale=0.8,thumbnail=0.9}{centerline{includegraphics{contslashvariableAreaslashcv}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure2782"
 HREF="img217.png"><IMG
 WIDTH="275" HEIGHT="129"
 SRC="|."$dir".q|Timg217.png"
 ALT="\begin{figure}\centerline{\includegraphics
{cont/variableArea/cv}}
\end{figure}"></A>|; 

$key = q/300celsius;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="28" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img92.png"
 ALT="$ 300 \celsius$">|; 

$key = q/{examl}AtpointAofthetubethepressureis{3[Bar]{,Machnumberis2.5,andtheductsectionastandadiabaticsteadystateflow,calculatedthetotalpressurelost.{examl};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="555" HEIGHT="73" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img339.png"
 ALT="\begin{examl}
At point A of the tube the pressure is $3[Bar]$, Mach number is
2....
...and adiabatic steady state flow,
calculated the total pressure lost.
\end{examl}">|; 

$key = q/*;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img201.png"
 ALT="$ *$">|; 

$key = q/M<0.1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="63" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img205.png"
 ALT="$ M&lt;0.1$">|; 

$key = q/displaystyle=ktimes{Poverrho};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="66" HEIGHT="53" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img81.png"
 ALT="$\displaystyle = k \times {P \over \rho }$">|; 

$key = q/displaystyleP=constanttimesrho^{k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="135" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img78.png"
 ALT="$\displaystyle P= constant\times \rho^{k}$">|; 

$key = q/displaystyle{RTover{P_0}^2}left(dot{m}overAright)^2={Fn^2overk}left(A^{*}P_0overAPright)^2left(ToverT_0right)left(P_0overPright)^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="312" HEIGHT="61" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img316.png"
 ALT="$\displaystyle { R T \over {P_0}^2} \left( \dot{m} \over A \right) ^2 = {Fn^2\ov...
...P_0 \over A P \right)^2 \left( T \over T_0 \right) \left( P_0 \over P \right)^2$">|; 

$key = q/{figure}htmlimage{scale=0.8,thumbnail=1.2}{centerline{includegraphics{contslashfannoslashjumpFLD}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure16152"
 HREF="img1036.png"><IMG
 
 SRC="|."$dir".q|Timg1036.png"
 ALT="\begin{figure}\centerline{\includegraphics
{cont/fanno/jumpFLD}}
\end{figure}"></A>|; 

$key = q/eta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img56.png"
 ALT="$ \eta$">|; 

$key = q/Fn;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img300.png"
 ALT="$ Fn$">|; 

$key = q/(C_pT);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="47" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img189.png"
 ALT="$ (C_p T)$">|; 

$key = q/displaystyle{dPoverrho}=U^{2}left({dAoverA}+{drhooverrho}{dPoverdP}right)=U^{2}left({dAoverA}+overbrace{drhooverdP}^{1overc^{2}}{dPoverrho}right);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="355" HEIGHT="113" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img227.png"
 ALT="$\displaystyle {dP \over \rho} = U^{2} \left( {dA \over A} +{d\rho \over \rho} {...
...A \over A} + \overbrace{d\rho \over dP}^{1 \over c^{2}} {dP \over \rho} \right)$">|; 

$key = q/{figure}htmlimage{scale=0.8,thumbnail=0.7}{centerline{includegraphics{calculationsslashfannoMfldShockPP}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure16328"
 HREF="img1076.png"><IMG
 
 SRC="|."$dir".q|Timg1076.png"
 ALT="\begin{figure}\centerline{\includegraphics
{calculations/fannoMfldShockPP}}
\end{figure}"></A>|; 

$key = q/{figure}htmlimage{scale=1.8,thumbnail=1.5}{centerline{includegraphics{contslashshockslashcv}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure5983"
 HREF="img479.png"><IMG
 
 SRC="|."$dir".q|Timg479.png"
 ALT="\begin{figure}\centerline{\includegraphics {cont/shock/cv} }
\end{figure}"></A>|; 

$key = q/zrightarrow1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="45" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img130.png"
 ALT="$ z\rightarrow1$">|; 

$key = q/left[kgoverm^3right];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="41" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img90.png"
 ALT="$ \left[ kg \over m^3 \right]$">|; 

$key = q/theta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img26.png"
 ALT="$ \theta $">|; 

$key = q/displaystyleFn=kleft({k+1over2}right)^{-{k+1over2(k-1)}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="171" HEIGHT="67" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img301.png"
 ALT="$\displaystyle Fn = k\left( {k +1 \over 2} \right)^ {-{ k+ 1 \over 2(k -1)}}$">|; 

$key = q/{M_y}^{'}=0.3;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="74" HEIGHT="42" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img5.png"
 ALT="$ {M_y}^{'} = 0.3$">|; 

$key = q/R;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img166.png"
 ALT="$ R$">|; 

$key = q/{examl}Airisallowedtoflowfromareservoirwithtemperatureof{21^{circ}C{andwithpressasured.Assumethattheratioofspecificheatis{k=C_pslashC_v=1.4{.{examl};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="555" HEIGHT="126" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img265.png"
 ALT="\begin{examl}
Air is allowed to flow from a reservoir with temperature
of $21^{\...
...ed.
Assume that the ratio of specific heat is $ k=C_p / C_v = 1.4$.
\end{examl}">|; 

$key = q/displaystyleds=0=C_p{dToverT}-R{dPoverP};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="169" HEIGHT="52" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img250.png"
 ALT="$\displaystyle ds = 0 = C_p {dT \over T} - R {dP \over P}$">|; 

$key = q/c_0=1449.14[mslashsec];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="144" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img151.png"
 ALT="$ c_0 = 1449.14[m/sec]$">|; 

$key = q/frac{4fL}{D};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="31" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img10.png"
 ALT="$ \frac {4fL}{D}$">|; 

$key = q/{figure}htmlimage{scale=2.8,thumbnail=1.5}{centerline{includegraphics{contslashobliqueslashMachReflection}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure22963"
 HREF="img1448.png"><IMG
 
 SRC="|."$dir".q|Timg1448.png"
 ALT="\begin{figure}\centerline{\includegraphics {cont/oblique/MachReflection}}
\end{figure}"></A>|; 

$key = q/displaystyle{P_0-PoverP}={kM^2over2}left(1+{M^2over4}+cdotsright);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="238" HEIGHT="57" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img213.png"
 ALT="$\displaystyle {P_0 -P \over P} = {kM^2 \over 2} \left( 1 + {M^2 \over 4} + \cdots \right)$">|; 

$key = q/displaystyledP=c^{2}drho;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="77" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img76.png"
 ALT="$\displaystyle dP = c ^{2} d \rho$">|; 

$key = q/{figure}htmlimage{scale=2.8,thumbnail=1.2}{centerline{includegraphics{contslashshockVariableslashsuperSonicTunnel}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure10444"
 HREF="img721.png"><IMG
 
 SRC="|."$dir".q|Timg721.png"
 ALT="\begin{figure}\centerline{\includegraphics
{cont/shockVariable/superSonicTunnel}}
\end{figure}"></A>|; 

$key = q/displaystyle{P^{gamma-1overgamma}overT}=constant;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="128" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img170.png"
 ALT="$\displaystyle {P^{\gamma - 1 \over \gamma} \over T} = constant$">|; 

$key = q/dPslashdrho=c^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="84" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img228.png"
 ALT="$ dP/d\rho = c^2$">|; 

$key = q/displaystyleleadsto{left.{P_0}right|_{A}overleft.{P_0}right|_{B}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="63" HEIGHT="54" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img334.png"
 ALT="$\displaystyle \leadsto {\left.{P_0} \right\vert _{A} \over \left.{P_0} \right\vert _{B}}$">|; 

$key = q/P_{0}=constant;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="104" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img267.png"
 ALT="$ P_{0} = constant$">|; 

$key = q/C_v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img123.png"
 ALT="$ C_v$">|; 

$key = q/{figure}htmlimage{scale=0.8,thumbnail=0.5}{centerline{includegraphics{photosslashrayleigh}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure1026"
 HREF="img48.png"><IMG
 WIDTH="201" HEIGHT="274"
 SRC="|."$dir".q|Timg48.png"
 ALT="\begin{figure}\centerline{\includegraphics{photos/rayleigh}}
\end{figure}"></A>|; 

$key = q/c_{STP};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img155.png"
 ALT="$ c_{STP}$">|; 

$key = q/displaystyle{P^{*}overP_0}=left(2overk+1right)^{koverk-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="137" HEIGHT="66" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img203.png"
 ALT="$\displaystyle {P^{*} \over P_0} = \left(2 \over k+1 \right)^{k \over k-1}$">|; 

$key = q/m_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img53.png"
 ALT="$ m_0$">|; 

$key = q/displaystylec=sqrt{znRT}=sqrt{1.403times0.995times287times300}=346.7[mslashsec];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="431" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img137.png"
 ALT="$\displaystyle c = \sqrt{znRT} = \sqrt{1.403 \times 0.995 times 287 \times 300} = 346.7 [m/sec]$">|; 

$key = q/{figure}htmlimage{scale=0.8,thumbnail=1.4}{centerline{includegraphics{contslashfannoslashcv}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure15359"
 HREF="img919.png"><IMG
 
 SRC="|."$dir".q|Timg919.png"
 ALT="\begin{figure}\centerline{\includegraphics{cont/fanno/cv}}
\end{figure}"></A>|; 

$key = q/xi={dot{m}_boverdot{m}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="54" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img161.png"
 ALT="$ \xi = {\dot{m}_b \over \dot{m}} $">|; 

$key = q/Pslashrho;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="32" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img82.png"
 ALT="$ P / \rho $">|; 

$key = q/T_BlongrightarrowT_A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="79" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img108.png"
 ALT="$ T_B \longrightarrow T_A$">|; 

$key = q/{figure}htmlimage{scale=1.8,thumbnail=0.9}{centerline{includegraphics{contslashobliqueslashtwoAngle}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure23341"
 HREF="img1480.png"><IMG
 
 SRC="|."$dir".q|Timg1480.png"
 ALT="\begin{figure}\centerline{\includegraphics{cont/oblique/twoAngle}}
\end{figure}"></A>|; 

$key = q/P=rhoRT;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="70" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img223.png"
 ALT="$ P = \rho R T$">|; 

$key = q/n=1.403;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="72" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img136.png"
 ALT="$ n = 1.403 $">|; 

$key = q/{figure}htmlimage{scale=2.8,thumbnail=1.9}{centerline{includegraphics{contslashPrandtl-MeyerslashMachLineIncrease}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure28584"
 HREF="img1534.png"><IMG
 
 SRC="|."$dir".q|Timg1534.png"
 ALT="\begin{figure}\centerline{\includegraphics
{cont/Prandtl-Meyer/MachLineIncrease}}
\end{figure}"></A>|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img103.png"
 ALT="$ x$">|; 

$key = q/displaystyle{gamma-1overgamma}={RoverC_p+mC};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="134" HEIGHT="53" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img171.png"
 ALT="$\displaystyle {\gamma -1 \over \gamma} = {R \over C_p + mC}$">|; 

$key = q/{figure}htmlimage{scale=1.8,thumbnail=1.2}{centerline{includegraphics{contslashfannoslashexample1}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure15837"
 HREF="img990.png"><IMG
 
 SRC="|."$dir".q|Timg990.png"
 ALT="\begin{figure}\centerline{\includegraphics {cont/fanno/example1}}
\end{figure}"></A>|; 

$key = q/dsequiv0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="49" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img221.png"
 ALT="$ ds \equiv 0$">|; 

$key = q/k=1.4;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="55" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img1.png"
 ALT="$ k=1.4$">|; 

$key = q/{figure}htmlimage{scale=1.8,thumbnail=0.7}{centerline{includegraphics{calculationsslashfiguresslashobliqueMax}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure23066"
 HREF="img1470.png"><IMG
 
 SRC="|."$dir".q|Timg1470.png"
 ALT="\begin{figure}\centerline{\includegraphics
{calculations/figures/obliqueMax}}
\end{figure}"></A>|; 

$key = q/k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img7.png"
 ALT="$ k$">|; 

$key = q/displaystyledot{m}=rhoUA={PoverRT}UA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="146" HEIGHT="53" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img287.png"
 ALT="$\displaystyle \dot{m} = \rho U A = {P \over RT} U A$">|; 

$key = q/Mlongrightarrow0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="64" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img211.png"
 ALT="$ M\longrightarrow 0$">|; 

$key = q/displaystyle{dToverT}=-{(k-1)MdMover1+{k-1over2}M^{2}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="157" HEIGHT="55" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img254.png"
 ALT="$\displaystyle { dT \over T } = - {(k -1) M dM \over 1 + { k - 1 \over 2 } M^{2} }$">|; 

$key = q/{examl}index{speedofsound!lineartemperature}Thetemperatureintheatmospherecanbeasittakeforsoundtotravelfrompoint``A''topoint``B''underthisassumption.?{examl};AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="555" HEIGHT="54" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img98.png"
 ALT="\begin{examl}
\index{speed of sound!linear temperature}
The temperature in the a...
...d to travel from point \lq\lq A'' to
point \lq\lq B'' under this assumption.?
\end{examl}">|; 

$key = q/{figure}htmlimage{scale=1.8,thumbnail=0.8}{centerline{includegraphics{calculationsslashisothermal}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure12858"
 HREF="img821.png"><IMG
 
 SRC="|."$dir".q|Timg821.png"
 ALT="\begin{figure}\centerline{\includegraphics {calculations/isothermal}}
\end{figure}"></A>|; 

$key = q/dP;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img233.png"
 ALT="$ dP$">|; 

$key = q/0.27240307times15.243=4.146;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="202" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img343.png"
 ALT="$ 0.27240307 \times 15.243= 4.146$">|; 

$key = q/displaystyleleft.{dN_s}over{dt}right)~=~lim_{Deltatrightarrow0}{{N_s(t_0+Deltat)-N_s(t_0)}over{Deltat}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="274" HEIGHT="54" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img57.png"
 ALT="$\displaystyle \left. {dN_s} \over {dt} \right)&nbsp;=&nbsp;\lim_{\Delta t \rightarrow 0} {{N_s(t_0+\Delta t) - N_s(t_0)} \over {\Delta t}}$">|; 

$key = q/displaystyleleft.{P_{0}}right|_{A}-left.{P_{0}}right|_{B}=51.257-15.243=36.013[Bar];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="325" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img342.png"
 ALT="$\displaystyle \left.{P_{0}}\right\vert _{A} -
\left.{P_{0}}\right\vert _{B} =
51.257 - 15.243 = 36.013[Bar]
$">|; 

$key = q/{figure}htmlimage{scale=2.8,thumbnail=1.6}{centerline{includegraphics{contslashrayleighslashcv}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure19141"
 HREF="img1149.png"><IMG
 
 SRC="|."$dir".q|Timg1149.png"
 ALT="\begin{figure}\centerline{\includegraphics{cont/rayleigh/cv}}
\end{figure}"></A>|; 

$key = q/{figure}htmlimage{scale=2.8,thumbnail=1.5}{centerline{includegraphics{contslashobliqueslashLargeSymmetrialAngle}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure22953"
 HREF="img1446.png"><IMG
 
 SRC="|."$dir".q|Timg1446.png"
 ALT="\begin{figure}\centerline{\includegraphics{cont/oblique/LargeSymmetrialAngle}}
\end{figure}"></A>|; 

$key = q/M=2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="51" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img278.png"
 ALT="$ M=2$">|; 

$key = q/{figure}htmlimage{scale=2.8,thumbnail=1.2}{centerline{includegraphics{contslashobliqueslashball}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure24341"
 HREF="img1524.png"><IMG
 
 SRC="|."$dir".q|Timg1524.png"
 ALT="\begin{figure}\centerline{\includegraphics {cont/oblique/ball}}
\end{figure}"></A>|; 

$key = q/mathbf{B};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img282.png"
 ALT="$ \mathbf{B}$">|; 

$key = q/1slash(1-M^2);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="85" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img236.png"
 ALT="$ 1 /(1 -M^2)$">|; 

$key = q/sqrt{k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img36.png"
 ALT="$ \sqrt{k}$">|; 

$key = q/{dot{m}}_{A}={dot{m}}_{B};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="75" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img328.png"
 ALT="$ {\dot{m}}_{A}= {\dot{m}}_{B}$">|; 

$key = q/mathaccentV{dot}05F{m};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img13.png"
 ALT="$ \mathaccentV {dot}05F{m}$">|; 

$key = q/displaystyleDxy{P}{rho}=left.{Pxy{P}{rho}}right|_{s};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="80" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img72.png"
 ALT="$\displaystyle \Dxy{P}{\rho} = \left. {\Pxy{P}{\rho} } \right\vert _{s}$">|; 

$key = q/displaystyleC_{p}={kRoverk-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="84" HEIGHT="52" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img188.png"
 ALT="$\displaystyle C_{p} = {k R \over k -1 }$">|; 

$key = q/C_v+mCover1+m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="55" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img176.png"
 ALT="$ C_v + mC \over 1 + m$">|; 

$key = q/displaystylec=sqrt{2.2times10^9Nslashm^2over1000kgslashm^3}=1493mslashs%label{sound:eq:waterSpeed};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="240" HEIGHT="68" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img146.png"
 ALT="$\displaystyle c = \sqrt{2.2 \times 10^9 N /m^2 \over 1000 kg /m^3} = 1493 m/s %\label{sound:eq:waterSpeed}
$">|; 

$key = q/dU^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="32" HEIGHT="16" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img64.png"
 ALT="$ dU^2$">|; 

$key = q/displaystyledrho={1overc^2}dP;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="81" HEIGHT="50" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img249.png"
 ALT="$\displaystyle d\rho = { 1 \over c^2} dP$">|; 

$key = q/displaystyle={rhooverrho_0}overbrace{P_{0}overRT_0}^{rho_0}=0.69428839times{5times10^6[Pa]over287.0left[{JoverkgK}right]times294[K]};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="346" HEIGHT="95" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img272.png"
 ALT="$\displaystyle = {\rho \over \rho_0} \overbrace{P_{0}\over R T_0}^{\rho_0}= 0.69...
...es {5 \times 10^6 [Pa] \over 287.0 \left[{J \over kg K}\right] \times 294 [K] }$">|; 

$key = q/displaystyle{dPoverP}={drhooverrho}+{dToverT};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="114" HEIGHT="52" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img225.png"
 ALT="$\displaystyle {dP \over P} = {d\rho \over \rho} + {dT \over T}$">|; 

$key = q/displaystyle{rho_0overrho}=1+{(k-1)M^2over4}+{kM^4over8}+{2(2-k)M^6over48}cdots;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="333" HEIGHT="57" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img209.png"
 ALT="$\displaystyle {\rho_0\over \rho} = 1 + {(k -1) M^2 \over 4} + {k M^4\over 8} + {2(2-k)M^6 \over 48} \cdots$">|; 

$key = q/displaystyleMequiv{Uoverc};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="59" HEIGHT="53" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img195.png"
 ALT="$\displaystyle M \equiv {U \over c}$">|; 

$key = q/displaystyle{1overk}{{rhoU^{2}overA};{dAover1-M^2}overP}=-{MdMover1+{k-1over2}M^{2}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="208" HEIGHT="68" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img256.png"
 ALT="$\displaystyle {1 \over k} { {\rho U^{2} \over A} \; {dA \over 1 -M^2} \over P } = - { M dM \over 1 + { k - 1 \over 2 } M^{2} }$">|; 

$key = q/displaystyledA<0RightarrowdP>0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="129" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img242.png"
 ALT="$\displaystyle dA &lt; 0 \Rightarrow dP&gt; 0$">|; 

$key = q/ToverT_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img206.png"
 ALT="$ T \over T_0$">|; 

$key = q/PU;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="30" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img248.png"
 ALT="$ PU$">|; 

$key = q/{figure}htmlimage{scale=0.8,thumbnail=0.4}{centerline{includegraphics{photosslashgalileo}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure997"
 HREF="img45.png"><IMG
 WIDTH="238" HEIGHT="320"
 SRC="|."$dir".q|Timg45.png"
 ALT="\begin{figure}\centerline{\includegraphics {photos/galileo}}
\end{figure}"></A>|; 

$key = q/Nslashm^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="47" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img142.png"
 ALT="$ N/m^2$">|; 

$key = q/c_P;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img154.png"
 ALT="$ c_P$">|; 

$key = q/displaystyleTds=overbrace{C_pdT-{Toverz}left(partialzoverpartialTright)_P{dPoveroverrho}^{zRT}left[{Toverz}left(partialzoverpartialTright)_P+1right];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="734" HEIGHT="102" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img119.png"
 ALT="$\displaystyle Tds = \overbrace{C_p dT - {T \over z} \left(\partial z \over \par...
...{zRT} \left[ {T \over z}\left(\partial z \over \partial T \right)_P + 1 \right]$">|; 

$key = q/displaystyleoverbrace{(P+dP)-P}^{sumF}=overbrace{(rho+drho)(c-dU)^{2}-rhoc^2}^{int_{cs}U(rhoUdA)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="288" HEIGHT="93" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img74.png"
 ALT="$\displaystyle \overbrace{(P + dP) - P}^{\sum F} = \overbrace{(\rho +d\rho)(c - dU)^{2} - \rho c^2}^ {\int_{cs} U(\rho U dA)}$">|; 

$key = q/displaystylerho^2={PrhooverT_0R}+{k-1over2kRT_0}left(dot{m}overAright)^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="193" HEIGHT="61" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img306.png"
 ALT="$\displaystyle \rho^2 = {P \rho \over T_0 R} + {k -1 \over 2kRT_0} \left( \dot{m} \over A \right) ^2$">|; 

$key = q/displaystyle{1overR{rho_0}^2T}left(dot{m}overAright)^2={overbrace{kRT_0}^{{c_0}^erkR{P_0}^2T}left(dot{m}overAright)^2={Fn^2overk}left(T_0overTright);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="472" HEIGHT="106" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img321.png"
 ALT="$\displaystyle { 1 \over R {\rho_0}^2T } \left( \dot{m} \over A \right) ^2 = {\o...
...} \left( \dot{m} \over A \right) ^2 = {Fn^2 \over k} \left( T_0 \over T \right)$">|; 

$key = q/displaystyle{AoverA^{*}}=left(2overk+1right)^{k+1over2(k-1)}left({1overM}+{k+1over4}M+{(3-k)(k+1)over32}M^3+cdotsright);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="460" HEIGHT="67" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img216.png"
 ALT="$\displaystyle {A \over A^{*}} = \left(2 \over k +1 \right)^{k +1 \over 2 (k-1)} \left( {1\over M} + {k+1 \over 4}M + {(3-k) (k+1)\over 32 } M^3 + \cdots \right)$">|; 

$key = q/displaystyleB=rho{Dxy{P}{rho}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="67" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img144.png"
 ALT="$\displaystyle B = \rho {\Dxy{P}{\rho}}$">|; 

$key = q/displaystylet={hoversqrt{kRbar{T}}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="81" HEIGHT="52" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img106.png"
 ALT="$\displaystyle t = { h \over \sqrt{kR\bar{T}} }$">|; 

$key = q/displaystyle{dPoverrho}left[1-left(Uovercright)^2right]=U^2{dAoverA};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="190" HEIGHT="64" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img229.png"
 ALT="$\displaystyle {dP \over \rho } \left[ 1 - \left(U \over c\right)^2 \right] = U^2 {dA \over A}$">|; 

$key = q/rhosim;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img95.png"
 ALT="$ \rho \sim$">|; 

$key = q/tmspace+thickmuskip{.2777em};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="9" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img30.png"
 ALT="$ \tmspace +\thickmuskip {.2777em}$">|; 

$key = q/z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img131.png"
 ALT="$ z$">|; 

$key = q/displaystyledA>0LongrightarrowdP>0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="138" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img237.png"
 ALT="$\displaystyle dA &gt; 0 \Longrightarrow dP&gt; 0$">|; 

$key = q/ds=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="49" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img120.png"
 ALT="$ ds = 0$">|; 

$key = q/300K;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="43" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img325.png"
 ALT="$ 300K$">|; 

$key = q/displaystyle{dot{m}overA}=left(kMP_0oversqrt{kRT_{0}}right)left(1+{k-1over2}M^{2}right)^{-{k+1over2(k-1)}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="288" HEIGHT="67" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img289.png"
 ALT="$\displaystyle {\dot{m} \over A} = \left(k M P_0 \over \sqrt{kR T_{0}}\right) \left( 1 + {k -1 \over 2} M^{2} \right)^ {-{ k+ 1 \over 2(k -1)}}$">|; 

$key = q/displaystyleP_a=rho_aRT_a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="89" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img159.png"
 ALT="$\displaystyle P_a = \rho_a R T_a$">|; 

$key = q/displaystylet={2hT_Aover3sqrt{kRT_A};(T_B-T_A)}left(left(T_BoverT_Aright)^{3over2}-1right);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="286" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img105.png"
 ALT="$\displaystyle t = {2 h T_A \over 3 \sqrt{kRT_A} \; (T_B - T_A) } \left( \left(T_B \over T_A\right)^{3\over 2}- 1 \right)$">|; 

$key = q/M_{1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img18.png"
 ALT="$ M_{1}$">|; 

$key = q/displaystyle{P_0-PoverhalfrhoU^2}=1+overbrace{{M^2over4}+{(2-k)M^4over24}+cdots}^{compressibility;correction};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="268" HEIGHT="105" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img210.png"
 ALT="$\displaystyle {P_0 -P \over \half \rho U^2} = 1 + \overbrace{{ M^2 \over 4} + {(2-k) M^4\over 24} + \cdots}^{compressibility\; correction}$">|; 

$key = q/{figure}htmlimage{scale=1.8,thumbnail=1.1}{centerline{includegraphics{contslashobliqueslashgeneral}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure22310"
 HREF="img1328.png"><IMG
 
 SRC="|."$dir".q|Timg1328.png"
 ALT="\begin{figure}\centerline{\includegraphics{cont/oblique/general}}
\end{figure}"></A>|; 

$key = q/{figure}htmlimage{scale=0.8,thumbnail=0.7}{centerline{includegraphics{calculationsslashfiguresslashnozzleCompare}}{<comment_mark>473{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure3733"
 HREF="img377.png"><IMG
 
 SRC="|."$dir".q|Timg377.png"
 ALT="\begin{figure}\centerline{\includegraphics{calculations/figures/nozzleCompare}}
%
\end{figure}"></A>|; 

$key = q/{examl}TheMachnumberatpointAontubeismeasuredtobeM=2footnotemark{}andthestaticpretpointB.Assumethatthespecificheatratiok=1.4andassumeaperfectgasmodel.{examl};AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="555" HEIGHT="94" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img277.png"
 ALT="\begin{examl}
The Mach number at point A on tube is measured to be
$M=2$\footno...
...hat the specific heat ratio $k=1.4$ and assume a perfect
gas model.
\end{examl}">|; 

$key = q/displaystyleleft(rho_1overrho_2right)^n={P_1overP_2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="98" HEIGHT="56" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img132.png"
 ALT="$\displaystyle \left(\rho_1 \over \rho_2 \right)^n = {P_1 \over P_2}$">|; 

$key = q/displaystylehat{rho}={1over2}left(1+sqrt{1+2{(k-1)RT_0overkP^2}left(dot{m}overAright)^2}right);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="287" HEIGHT="75" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img311.png"
 ALT="$\displaystyle \hat{\rho} = {1 \over 2} \left( 1 + \sqrt{1 + 2 {(k -1) R T_0 \over k P^2} \left( \dot{m} \over A \right) ^2 } \right)$">|; 

$key = q/displaystyle{dToverT}={drhooverrho}{RoverC_v}left[z+{T}left(partialzoverpartialTright)_{rho}right];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="211" HEIGHT="64" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img125.png"
 ALT="$\displaystyle {dT \over T} = {d\rho \over \rho} {R \over C_v} \left[ z + { T }\left( \partial z \over \partial T \right)_{\rho} \right]$">|; 

$key = q/displaystyle{dPoverdrho}=nzRT;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="92" HEIGHT="52" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img133.png"
 ALT="$\displaystyle {dP \over d\rho} = nz R T$">|; 

$key = q/P;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img68.png"
 ALT="$ P$">|; 

$key = q/c=sqrt{kRT};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="78" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img35.png"
 ALT="$ c=\sqrt{kRT}$">|; 

$key = q/displaystyleU=Moverbrace{sqrt{kRT}}^{c}=0.88638317timessqrt{1.4times287times294}=304[mslashsec];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="438" HEIGHT="83" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img274.png"
 ALT="$\displaystyle U = M \overbrace{\sqrt{kRT}}^{c} = 0.88638317 \times
\sqrt { 1.4 \times 287 \times 294} = 304 [m /sec]
$">|; 

$key = q/{figure}htmlimage{scale=2.8,thumbnail=1.2}{centerline{includegraphics{contslashobliqueslashreflectiveWall}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure23683"
 HREF="img1504.png"><IMG
 
 SRC="|."$dir".q|Timg1504.png"
 ALT="\begin{figure}\centerline{\includegraphics{cont/oblique/reflectiveWall}}
\end{figure}"></A>|; 

$key = q/{figure}htmlimage{scale=1.8,thumbnail=0.7}{centerline{includegraphics{scanedslashnozzlePressure}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure937"
 HREF="img42.png"><IMG
 WIDTH="294" HEIGHT="407"
 SRC="|."$dir".q|Timg42.png"
 ALT="\begin{figure}\centerline{\includegraphics {scaned/nozzlePressure}}
\end{figure}"></A>|; 

$key = q/Q=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="46" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img8.png"
 ALT="$ Q=0$">|; 

$key = q/fld;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="5" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img31.png"
 ALT="$ \fld$">|; 

$key = q/displaystyle{dot{m}overA}={0.1slash0.001}=100.0[kgslashsecslashm^2];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="250" HEIGHT="51" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img324.png"
 ALT="$\displaystyle {\dot{m} \over A } = {0.1 / 0.001} = 100.0 [kg/sec/m^2]
$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img128.png"
 ALT="$ n$">|; 

$key = q/frac{4fL}{D}=0.3;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="72" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img17.png"
 ALT="$ \frac {4fL}{D} = 0.3$">|; 

$key = q/{figure}htmlimage{scale=2.8,thumbnail=1.9}{centerline{includegraphics{contslashPrandtl-Meyerslashdalembert}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure28764"
 HREF="img1584.png"><IMG
 
 SRC="|."$dir".q|Timg1584.png"
 ALT="\begin{figure}\centerline{\includegraphics {cont/Prandtl-Meyer/dalembert}}
\end{figure}"></A>|; 

$key = q/displaystyleT=(T_B-T_A){xoverh}+T_A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="161" HEIGHT="43" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img102.png"
 ALT="$\displaystyle T = (T_B - T_A) {x \over h} + T_A$">|; 

$key = q/displaystyle{dot{m}overA}=sqrt{k{P_0}^2M^2overRT_0}left(1+{k-1over4}M^2+cdotsright);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="279" HEIGHT="73" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img215.png"
 ALT="$\displaystyle {\dot{m} \over A} = \sqrt{k {P_0}^2 M^2 \over RT_0} \left( 1 + {k-1 \over 4}M^2 + \cdots \right)$">|; 

$key = q/X;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img180.png"
 ALT="$ X$">|; 

$key = q/delta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img21.png"
 ALT="$ \delta $">|; 

$key = q/displaystyleC_v=left(partialuoverpartialTright)_{rho}=Tleft(partialsoverpartialTright)_{rho};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="191" HEIGHT="54" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img113.png"
 ALT="$\displaystyle C_v = \left( \partial u \over \partial T \right)_{\rho} = T \left( \partial s \over \partial T \right)_{\rho}$">|; 

$key = q/displaystyle{(k-1)overk}{dPoverP}=-{(k-1)MdMover1+{k-1over2}M^{2}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="210" HEIGHT="55" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img255.png"
 ALT="$\displaystyle {(k - 1) \over k} { dP \over P } = - { (k -1) M dM \over 1 + { k - 1 \over 2 } M^{2} }$">|; 

$key = q/Dgeq0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="47" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img24.png"
 ALT="$ D\geq 0$">|; 

$key = q/{figure}htmlimage{scale=0.8,thumbnail=0.6}centerlineline{includegraphics{calculationsslashfiguresslashnozzleCompareAreaPropety}}{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure3771"
 HREF="img383.png"><IMG
 
 SRC="|."$dir".q|Timg383.png"
 ALT="\begin{figure}\centerlineline{\includegraphics
{calculations/figures/nozzleCompareAreaPropety}}
\end{figure}"></A>|; 

$key = q/{figure}htmlimage{scale=2.8,thumbnail=1.4}{centerline{includegraphics{contslashshockVariableslashnozzleWithShock}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure10035"
 HREF="img670.png"><IMG
 
 SRC="|."$dir".q|Timg670.png"
 ALT="\begin{figure}\centerline{\includegraphics{cont/shockVariable/nozzleWithShock}}
\end{figure}"></A>|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img44.png"
 ALT="$ f$">|; 

$key = q/0^{circ}C;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="32" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img149.png"
 ALT="$ 0 ^{\circ}C$">|; 

$key = q/M;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img263.png"
 ALT="$ M$">|; 

$key = q/{figure}htmlimage{scale=1.4,thumbnail=1.5}{centerline{includegraphics{contslashfannoslashlongFLDwithShock}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure16335"
 HREF="img1077.png"><IMG
 
 SRC="|."$dir".q|Timg1077.png"
 ALT="\begin{figure}\centerline{\includegraphics
{cont/fanno/longFLDwithShock}}
\end{figure}"></A>|; 

$key = q/{figure}htmlimage{scale=1.8,thumbnail=1.2}{centerline{includegraphics{contslashsrline{includegraphics{contslashshockslashcomparisonMove}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure6515"
 HREF="img529.png"><IMG
 
 SRC="|."$dir".q|Timg529.png"
 ALT="\begin{figure}\centerline{\includegraphics{cont/shock/comparison}}
\centerline{\includegraphics{cont/shock/comparisonMove}}
\end{figure}"></A>|; 

$key = q/{examl}Agasflowsinthetubewithmassflowrateof0.1[kgslashsec]andtubecrosssectionis0tropic,<verbatim_mark>rawhtml517#,<verbatim_mark>rawhtml518#.{examl};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="555" HEIGHT="92" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img323.png"
 ALT="\begin{examl}
A gas flows in the tube with mass flow rate of 0.1 [kg/sec]
and tu...
...i&gt;\end{rawhtml},
\begin{rawhtml}
&lt;i&gt;R=287[j/kg K]&lt;/i&gt;\end{rawhtml}.
\end{examl}">|; 

$key = q/h;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img101.png"
 ALT="$ h$">|; 

$key = q/displaystylec^{2}left({drhooverrho}right)={dPoverrho}Longrightarrowc^2={dPoverdrho};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="205" HEIGHT="54" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img67.png"
 ALT="$\displaystyle c^{2} \left( { d\rho \over \rho } \right) = {dP \over \rho} \Longrightarrow c^2 = {dP \over d\rho}$">|; 

$key = q/displaystylec=sqrt{Eoverrho}=sqrt{160times10^{9}Nslashm^{2}over7860Kgslashm^3}=4512mslashs;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="297" HEIGHT="68" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img158.png"
 ALT="$\displaystyle c = \sqrt{ E \over \rho} = \sqrt{160 \times 10^{9} N/m^{2} \over 7860 Kg /m^3} = 4512 m/s$">|; 

$key = q/{figure}htmlimage{scale=0.8,thumbnail=0.8}{centerline{includegraphics{contslashvariableAreaslashtube}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure2651"
 HREF="img197.png"><IMG
 WIDTH="228" HEIGHT="94"
 SRC="|."$dir".q|Timg197.png"
 ALT="\begin{figure}\centerline{\includegraphics {cont/variableArea/tube}}
\end{figure}"></A>|; 

$key = q/displaystyle{left.{P_{0}}right|_{A}overleft.{P_{0}}right|_{B}}={{left.{AoverA^{*}}right|_{M_{B}}}strut}{{left.Aright|_{A}strut}overleft.Aright|_{B}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="155" HEIGHT="64" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img337.png"
 ALT="$\displaystyle {\left.{P_{0}}\right\vert _{A} \over \left.{P_{0}}\right\vert _{B...
...}}}\strut} {{\left. A \right\vert _{A} \strut} \over \left. A \right\vert _{B}}$">|; 

$key = q/rho;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img77.png"
 ALT="$ \rho$">|; 

$key = q/{figure}htmlimage{scale=2.8,thumbnail=1.9}{centerline{includegraphics{contslashPrandtl-Meyerslashcoor}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure28627"
 HREF="img1551.png"><IMG
 
 SRC="|."$dir".q|Timg1551.png"
 ALT="\begin{figure}\centerline{\includegraphics {cont/Prandtl-Meyer/coor}}
\end{figure}"></A>|; 

$key = q/displaystyle(1+x)^n=1+nx+{n(n-1)x^2over2!}+{n(n-1)(n-2)x^3over3!}+cdots;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="416" HEIGHT="57" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img207.png"
 ALT="$\displaystyle (1+ x)^n = 1 + n x + {n(n-1) x^2\over 2!} + {n(n-1)(n-2) x^3\over 3!} + \cdots$">|; 

$key = q/displaystyledU=-{dPoverPU};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="88" HEIGHT="52" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img247.png"
 ALT="$\displaystyle dU = - { dP \over P U}$">|; 

$key = q/{figure}htmlimage{scale=1.2,thumbnail=0.7}{centerline{includegraphics{photosslashbullet}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure1016"
 HREF="img47.png"><IMG
 WIDTH="383" HEIGHT="260"
 SRC="|."$dir".q|Timg47.png"
 ALT="\begin{figure}\centerline{\includegraphics{photos/bullet}}
\end{figure}"></A>|; 

$key = q/M_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img14.png"
 ALT="$ M_1$">|; 

$key = q/displaystyledP=(rho+drho)left(c^{2}-cancelto{sim0}{2cdU}+cancelto{sim0}{rule{20pt}{0pt}dU^{2}}qquad;right)-rhoc^{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="396" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img75.png"
 ALT="$\displaystyle dP = (\rho + d\rho) \left( c^{2} - \cancelto{\sim 0}{2cdU} + \cancelto{\sim 0}{\rule{20pt}{0pt} dU^{2}}\qquad\;\right) - \rho c^{2}$">|; 

$key = q/1slash1-M^{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="72" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img240.png"
 ALT="$ 1 / 1 - M^{2}$">|; 

$key = q/displaystylehat{rho}={rhoover{poverRT_0}}={rhoRT_0overP}={1overbar{T}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="163" HEIGHT="53" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img310.png"
 ALT="$\displaystyle \hat{\rho} = {\rho \over {p \over R T_0 }} = {\rho R T_0 \over P} = { 1 \over \bar{T}}$">|; 

$key = q/displaystyledh=C_pdT+left[v-Tleft(overbrace{RToverP}^{voverz}left(partialzoverpartialTright)_P+overbrace{zRoverP}^{voverT}right)right]dP;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="353" HEIGHT="104" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img117.png"
 ALT="$\displaystyle dh = C_p dT + \left[ v - T \left( \overbrace{RT \over P}^{v \over...
...er \partial T \right)_P + \overbrace{zR \over P}^{v \over T} \right) \right] dP$">|; 

$key = q/{figure}htmlimage{scale=2.8,thumbnail=1.2}{centerline{includegraphics{contslashobliqueslashstablityShock}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure24348"
 HREF="img1525.png"><IMG
 
 SRC="|."$dir".q|Timg1525.png"
 ALT="\begin{figure}\centerline{\includegraphics {cont/oblique/stablityShock}}
\end{figure}"></A>|; 

$key = q/M_1=3;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img28.png"
 ALT="$ M_1=3$">|; 

$key = q/{figure}htmlimage{scale=2.8,thumbnail=1.6}{centerline{includegraphics{contslashsoundslashillustrationsslashpiston}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure1565"
 HREF="img58.png"><IMG
 WIDTH="320" HEIGHT="138"
 SRC="|."$dir".q|Timg58.png"
 ALT="\begin{figure}\centerline{\includegraphics
{cont/sound/illustrations/piston}}
\end{figure}"></A>|; 

$key = q/;;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="9" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img32.png"
 ALT="$ \;$">|; 

$key = q/displaystyle{dot{m}sqrt{T_0}overA^{*}P_0}=0.040418;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="133" HEIGHT="57" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img296.png"
 ALT="$\displaystyle { \dot{m} \sqrt{T_0}\over A^{*} P_0 } = 0.040418$">|; 

$key = q/D>0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="47" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img22.png"
 ALT="$ D&gt;0$">|; 

$key = q/displaystyleleft.{P_{0}}{A^{*}}right|_{A};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="58" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img332.png"
 ALT="$\displaystyle \left. {P_{0}} {A^{*}}\right\vert _{A}$">|; 

$key = q/{figure}htmlimage{scale=2.8,thumbnail=1.4}{centerline{includegraphics{contslashshockVariableslashdiffuserDiagram}}{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<A NAME="figure10410"
 HREF="img716.png"><IMG
 
 SRC="|."$dir".q|Timg716.png"
 ALT="\begin{figure}\centerline{\includegraphics {cont/shockVariable/diffuserDiagram}}
\end{figure}"></A>|; 

$key = q/displaystyle{dAoverA}={M^2-1overMleft(1+{k-1over2}M^2right)}dM;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="192" HEIGHT="57" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img259.png"
 ALT="$\displaystyle {dA \over A} = { M^2 -1 \over M \left( 1 + {k-1 \over 2} M^2 \right)} dM$">|; 

$key = q/displaystyle{Fn}equiv{dot{m}c_0oversqrt{R}A^{*}P_0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="114" HEIGHT="51" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img298.png"
 ALT="$\displaystyle {Fn} \equiv {\dot{m} c_0 \over \sqrt{R} A^{*} P_0 }$">|; 

$key = q/displaystyle{dToverT}={k-1overk};{dPoverP};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="118" HEIGHT="52" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img251.png"
 ALT="$\displaystyle {dT \over T} = {k - 1 \over k } \; {dP \over P}$">|; 

1;

