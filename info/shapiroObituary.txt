.

Doctor Shapiro died at his Jamaica Plain, Massachusetts, home, on Friday,
November 27,
2004, at the age of 80.

Dr. Ascher Shapiro wanted to get a handle on how fluids move -- whether
they were swirling
down the bathtub drain, or flowing through the human body. For more than a
half-century,
he ran tests, invented scientific tools, and wrote about the subject while
teaching at the
Massachusetts Institute of Technology.

For the first half of his career, he studied power production and
high-speed flight, but
after he shifted his focus, he helped promote the field of biomedical
engineering. He used
his knowledge of the movement of fluids to make advances in medical
technology.

He became an emeritus professor at MIT in 1986.

"It was never an easy course that he would give, but it was a brilliant
course," longtime
colleague Ronald Probstein said yesterday.

A series of films that Dr. Shapiro put together on fluid mechanics -- and
an accompanying
textbook translated into various languages -- became a staple of teaching
in the field.
The videos documented a range of fluid-related matters and were released
by a group he
founded, the National Committee for Fluid Mechanics Films.

The New York City native studied at City College in New York before
transferring to MIT,
where he received his bachelor's degree in 1938 and doctorate in 1946. He
became an
associate professor at MIT in 1947, and was named a full professor three
years later.

As a student, he headed a Navy laboratory in charge of developing turbine
propulsion
engines for aircraft-borne torpedoes.

As part of the Lexington Project, he helped evaluate nuclear-powered
aircraft and helped
invent a propulsion system for the vehicles.

He later directed the Atomic Energy Commission's Project Dynamo, which
evaluated nuclear
power as a source of civilian electricity production in 1953.

In the 1960s, he became fascinated with the flow of fluids inside the
human body and
applied what he knew about the movement of fluids to the medical sciences
at a time when
biomedical engineering was in its infancy.

His laboratory started working on the use of intra-aortic balloons for
patients with heart
conditions, which allowed doctors to use a far less invasive procedure to
fix various
cardiovascular problems. He also helped develop techniques for preventing
blood clots in
the legs.

In 1962, he conducted an experiment that made headlines and helped solve
mysteries about
the Coriolis forces involved in fluids heading down the drain. Filling a
circular tank, he
had water circulate in a clockwise direction, unlike the normal
counterclockwise direction
usually observed as water drains in the Northern Hemisphere. Covering it
with a plastic
sheet, after the circulating had stopped, he later pulled the plug and
watched as the
water drained clockwise. When he performed the same experiment but pulled
the plug after
24 hours, the water reverted to the counterclockwise direction -- proving
that variations
in bathtubs and sinks can alter Coriolis forces.

He served on dozens of advisory boards and professional committees,
including the National
Institutes of Health, the Atomic Energy Commission, and the National
Academy of Sciences.
He was elected to the American Academy of Arts and Sciences in 1952 and to
the National
Academy of Sciences in 1974.

He became chairman of the MIT faculty in 1964 and left that post after a
year to head the
department of mechanical engineering.

In the late 1970s, Dr. Shapiro was granted a patent for a recipe
conversion calculator.
"My friend mentioned how difficult it was to convert recipes from a given
quantity to the
required quantity, and it occurred to me that it ought to be possible to
invent a
calculator to help people who don't have a working knowledge of
mathematics or of
conversion factors," he told the Globe at the time.

A skilled dancer, he also was a talented amateur athlete, often beating
fellow
academicians in tennis and racquetball.

Boston Globe 
