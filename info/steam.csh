#!/bin/csh
#
echo "Split the file."
#
mkdir temp
cd temp
rm *
f90split ../steam.f90
#
echo "Compile the routines."
#
foreach FILE (`ls -1 *.f90`)
  f90 -c -g $FILE >& compiler.out
  if ( $status != 0 ) then
    echo "Errors compiling " $FILE
    exit
  endif
  rm compiler.out
end
rm *.f90
#
echo "Create the archive."
ar qc libsteam.a *.o
rm *.o
#
echo "Store the archive."
mv libsteam.a ~/lib/$ARCH
cd ..
rmdir temp
#
echo "A new version of steam has been created."
