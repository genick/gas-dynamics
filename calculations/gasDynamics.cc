# # Author: Genick Bar-Meir  
# # Date: Thu Mar 24  2005
# # Description: make file for gasDynamics program
# #
CXX = g++
CXXFLAGS = -g -c -Wall
LD = g++
LDFLAGS = -g -Wall

objects =  gasDynamics.o stagnation.o definition.o fanno.o \
		externalFun.o compressibleFlow.o pipeFlow.o \
		shock.o isothermal.o

headers = compressibleFlow.h definition.h externalFun.h \
			fanno.h gasDynamics.h isothermal.h isothermalNozzle.h \
			pipeFlow.h shock.h stagnation.h

gd:  $(objects)
	${LD} ${LDFLAGS}  $(objects) -o gd  

gasDynamics.o : gasDynamics.cc  $(headers)
	${CXX} ${CXXFLAGS} gasDynamics.cc 

definition.o : definition.cc  $(headers)
	${CXX} ${CXXFLAGS} definition.cc 

stagnation.o : stagnation.cc  $(headers)
	${CXX} ${CXXFLAGS} stagnation.cc 

fanno.o : fanno.cc  $(headers)
	${CXX} ${CXXFLAGS} fanno.cc 

isothermal.o : isothermal.cc  $(headers)
	${CXX} ${CXXFLAGS} isothermal.cc 

externalFun.o : externalFun.cc  $(headers)
	${CXX} ${CXXFLAGS} externalFun.cc 

compressibleFlow.o : compressibleFlow.cc  $(headers)
	${CXX} ${CXXFLAGS} compressibleFlow.cc 

pipeFlow.o : pipeFlow.cc  $(headers)
	${CXX} ${CXXFLAGS} pipeFlow.cc 

shock.o : shock.cc  $(headers)
	${CXX} ${CXXFLAGS} shock.cc 

clean:
	rm -rf *.o gd core*
/********************************************************
	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/
#ifndef _COMPRESSIBLEflow_STDLIB
#define _COMPRESSIBLEflow_STDLIB

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip> 
#  include "definition.h" 

class compressibleFlow{
		protected:
			//to move all the class variable to upper case
			//remove these lines when done
			int isFlowReverse; // check is the direction of the flow
			int isSubSonic; // is no Shock possible  
			int isFLDknown; // check if FLD was calculated/known?  no?  yes
			int isChoked;
			int isPipeFlow;
			int whatInfo; // what  stuff is needed to print
			int drivedClassName;
			double k	; // ratio of specific heat
			double fld; // the maximum length resistance 4fl/D
			double fld1; // the  fld for the tube entrance tube
			double fld2; //  the  fld for the tube exit
			double Area ; // relative area A/A* 
			double M1 ; // Mach number at the entrance
			double M2 ; // Mach number at the exit of the tube
			double Mfld[7][50];//tube profile 0)fld 1)Mx 2)My 3)pxpy
								//4)pxp* 5)pyp* 6) p1/p(i) 
			double M2upper ; // The tube Exit Mach number  after shock at exit
			double M2lower ;// tube Exit Mach number  after shock at entrance
			double fldUp; //the fld before the shock 
			double fldDown; //the ''left over'' fld after the shock 
			double M ;//Mach number general use i.e. isentropic relationship 
			double TzeroRstar; // temperature ratio T0/T0*
			double PRstar ; // pressure ratio P/P* anywhere on the tube
			double P1Pstar ; // pressure ratio P1/P* entrance to the tube
			double P2Pstar ; // pressure ratio P2/P* exit of the tube
			double PzeroRstar ; // pressure ratio P0/P0*
			double RRstar ; // relative density rho/rho*
			double URstar ; // relative velocity U/U*
			double TRstar ; // relative Temperature  T/T*
			double p2p1; // ratio of pressure between two sides of tube p1/p2
			double Pbar ; // stagnation pressure ratio 
			double Tbar ; // stagnation temperature ratio 
			double Rbar ; // stagnation density ratio 
			double PAR ; // stagnation mass conservation "ratio" 
			double Pbar1 ; // stagnation pressure ratio @ entrance p1/P1_0 
			double Pbar2 ; // stagnation pressure ratio @ exit  p2/P2_0 
			double M1maxSub; // the maximum ``subsonic''
			double singularPoint; // the similar as M1maxSub normally 1
			double superSonicFLD; // the maximum fld of supersonic branch
			double maxPossibleFLDsup; // for the supersonic branch (fanno flow)
			double maxPossibleFLDsub; // for the subsonic branch (fanno flow)
			double minM1exit, minM1entrance;//the reverse limit of constant fld
			// variables that related to the shock itself
            double TyTx; // temperature ratio Ty/Tx
            double PyPx ; // pressure ratio Py/Px
            double RyRx ; // relative density rhoY/rhox
            double P0yP0x ; // pressure ratio P0y/P0x
            double Mx ; // Mx number before the normal shock
            double My ; // My number after the normal shock

		public:
			// initialization of the base class
			compressibleFlow (double spesificHeatRatio, int dcn){ 
                    k = spesificHeatRatio;
 					drivedClassName = dcn ; 
					isFlowReverse = no; 
					isSubSonic = yes; 
            		isFLDknown=no;
					isPipeFlow = no;
			};
			// no use for destruction of the class
			virtual ~compressibleFlow (){};

			//these functions to be supplied by a specific class for
			// the specific flow model
			virtual void setMach(double){};
			virtual void calPressureRatio(){};
			virtual double calPressureRatio(double){return 0.0;};
			virtual void calFLD (void) {};
			virtual double calFLD(double) {return 0.0;} ; // cal FLD = f( M)
			virtual void calDensityRatio (void) {};
			virtual void calTotalPressureRatio (void) {};
			virtual double calTotalPressureRatio (double) {return 0.0;};
			virtual void calTemperatureRatio (void) {};
			virtual void calVelocityRatio (void) {};
			virtual void calTotalTemperatureRatio (void) {};
            virtual void calMy() {} ;
            virtual void calAreaRatio () {} ;
            virtual void calPARRatio () {} ;
            virtual void calMach (int, double *) {} ;
            virtual void calM1 (int, double *) {} ;
            virtual void calMx (int, double *) {} ;
            virtual int calAll (int, int, double *) {return no;} ;

			//calculate the new variable based on the non-standard variable 
            virtual void findMForFLD(double, double *) {};

			// obtain the values of the variables
			double getPressure() {return Pbar; };
            double getFLD (){return fld  ; };
            double getArea (){return Area ; };
            double getPAR (){return PAR ; };
            double getM1 (){return M1 ; };
            double getM2 (){return M2 ; };
			double getPRstar(){return PRstar ; };
            double getTemperatue (){return TzeroRstar ; };
            double getDensity (){return RRstar ; };
            double getT_P_ratio (){return PzeroRstar  ; };
            double getP2P1 (){return p2p1 ; };
            double getMy (){return My ; };
            double getk (){return k ; };
            double getmaxPossibleFLDsup (){return maxPossibleFLDsup ; };
            double getP0yP0x (){return P0yP0x ; };

			// change the values of the variables
			void changeMach ( double Mach) {M = Mach; } ;
			
			//user interface for Tex functions
			int makeTable (int, int, int, int, double*);
					// is BasicInfo (yes, no)
					// is Range (yes, no)
					// is TeX (yes, no) 
					//variable type(M, fld,etc), 
					//their values

			//show the results with TeX
			int makeTexTableLine (int, int , double * ); // variableName,
								//variables themselves
			virtual void makeTexTableHead(int ) {} ; // whatInfo variable
			virtual void makeTexTableLine(int){};// whatInfo variable
			void makeTexTableTail();
			// show the results without TeX
			virtual void showHead(int ){} ;
			virtual void showResults(int ){};
			//internal functions for printing variables (base class
			//functions)
			void showVariableHead (char **, int );
			void showVariable(double *, int);
			void showVariableTeXHead(char **, int);
			void showVariableTeXLine(double *, int);


			//general functions used by all the models of flow
			int calM1forFLDp2p1(double,double);// find M1 for given fld, p2p1
			int calM1forM2(double, double, double *);// cal M1 f(fld,M2,*M1) 
			int calM2forM1(double, double, double *);// cal M2 f(fld,M1,*M2)

			//functions for managing the calculations
			int calTableLine(int, int, double *);
			int calTableLine( void);
			// functions to generate part table (with and without TeX)
			int	 makeLines(int, int, int, double, double );
			int	 makeLines(int, int,int, double, double, int, double * );
			int  manegerCalTableLine(int,  int );//manager for calculating
											// whole line of the table with
											// parameters 1. isTex 2. whatInfo
			int  manegerCalTableLine(int, int, int,  double *);//manager for
									// calculating whole table
									// 1. isTeX, 2. whatInfo,(can be
									// used for basic table building) 
									// 3. variblelName p2p1, fld, etc,
									// 4. variableValues 
			void calP2P1();
			double calMForFLD ( double, double );
};
#endif  /* _COMPRESSIBLEflow_STDLIB */
/********************************************************
	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/
#ifndef _DEFINITION_STDLIB
#define _DEFINITION_STDLIB

# 	define	PI 3.14159265358979310
# 	define	infinity   1.79769313486231570E+308 
#   define EPS  1e-6
# 	define yes  1
# 	define no  0
#   define sonic 3 // Mach = 1 
//what info is given/sought
#   define machV 710 // name of variable  Mach 
#   define fldV 711 // name of variable  fld 
#   define p2p1FLDV 702 // name of variable  p2p1 
#   define M1V 713 // name of variable  M1 to the tube choked flow 
#   define M1ShockV 714//variable's name M1 with both sides shock limiting case
#   define FLDShockV 715 //variable's name FLD with shock in the middle 
#   define M1fldV 716 //variable's name FLD with shock in the middle 
#   define M1fldP2P1V 717 //variable's name FLD with shock in the middle 
#   define MxV 718 // Mx or My
#   define MFLDshockLocationV 719 // the shock location  
#   define PbarV 720 // pressure ratio
#   define AstarV 721 // area ratio A/A^*
#   define PARV 722 // AP over P0P^* 
#   define P0yP0xV 723 // Py0 over Px0 
	
//what information is sought. Note different derived class yields different
//results 
#   define infoStagnation 920 // print standard (stagnation) info
#   define infoStandard 921 // standard info for (fanno, etc) 
#   define infoTube 922 // print tube side info for (fanno, etc) including shock
#   define infoShock 923 // print shock sides info 
#   define infoTubeShock 924 // print tube info shock main info
#	define infoTubeProfile 925// the Mach number and pressure ratio profiles 
#   define infoTubeShockLimits 926 // print tube with shock extreme cases info 
// variables number 
extern  int  m1N 	 ; // M1 
extern int  fldN 	; // location of fld in the array 
extern int  p2p1N 	 ; // location of p2/p1 in the array 
extern int  m2N  	 ; // M2 
extern int  mN 	 ; // location of M in the array 
extern int  shockLocationN 	 ; // location of shock in the tube 
extern int  MxN 	 ; // Mx
extern int  PbarN 	 ; // P over P_0
extern int  AstarN	 ; // A over A^*
extern int  PARN	 ; // PAR 
extern int  P0yP0xN	 ; // Py0 over Px0 

//names of the [derived]  classes 
extern int fannoClass ; // 
extern int stagnationClass ; // 
extern int raylieghClass ; // 
extern int isothermalClass ; //  
extern int obliqueClass ; // 
extern int isoThermalNozzleClass ;//

extern int shockClass ;
extern int stagnationClass; 

//precition of the results 
extern int precision;

// variables number for the tube profile
extern int numberNodes ; // number of nodes in tube
extern int  fldP ;  // location of fld in the array 
extern int  MxP ;  ; // location of Mx in the array 
extern int  MyP ;  // location of My in the array 
extern int  PyPxP  ; // 
extern int  PxPsP  ; // 
extern int  PyPsP  ; // 
extern int  P1PiP  ; // 


#endif  /* _DEFINITION_STDLIB */
/********************************************************
	FILE: externalFun.h

	Thi cone is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/
#ifndef _EXTERNAL_FUN_STDLIB
#define _EXTERNAL_FUN_STDLIB

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip> 
#  include "definition.h" 
#  include "compressibleFlow.h"
//#  include "pipeFlow.h"
//#  include "shock.h"

extern double calP0yP0xEstmt (compressibleFlow * , double , double  ) ;
extern double calPAREstmt (compressibleFlow * , double , double  ) ;
extern double calAstarEstmt (compressibleFlow * c, double , double  ) ;
extern double calPbarEstmt (compressibleFlow * , double , double  ) ;
extern double calShockLocationM_Estmt (compressibleFlow * , double , double  ) ;
extern double calShockLocation_Estmt (compressibleFlow * , double , double  ) ;
extern double calFLD_Estmt (compressibleFlow * ,  double , double  ) ;
extern double calP2P1_Estmt (  compressibleFlow * , double , double )  ;
extern double root( double f (compressibleFlow *, double , double ),
            compressibleFlow *  , double  , double , double )  ;

#endif  /* _EXTERNAL_FUN_STDLIB */
/********************************************************
	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip> 
#  include "compressibleFlow.h"
#  include "pipeFlow.h"
#  include "shock.h"
#  include "externalFun.h" 
#  include "definition.h" 

#ifndef _FANNO_STDLIB
#define _FANNO_STDLIB

class fanno: public pipeFlow {
        protected:
        public:
			// to change the initialization later on
			// so that base class initialization will be considered
			//fanno(double Mach, double spesificHeatRatio ):
			fanno(double spesificHeatRatio ):
					//inilazation of base class
				//compressibleFlow(Mach,spesificHeatRatio)
				pipeFlow (spesificHeatRatio, fannoClass)
					 { 
						// specific initialization process for derived class
						//the speed of the chocked flow is 1 for fanno flow
						M1maxSub = 1.0;
						singularPoint = M1maxSub ; 
						//to change with k
						superSonicFLD = 0.821508116;
			  		};
			~fanno(){};
			//spesific functions of this model of flow to calculate
			//spesific parameters of this flow model.
			// 
			void calFLD();
			void calPressureRatio() ;
			void calDensityRatio();
			void calVelocityRatio();
			void calTotalPressureRatio() ;
			void calTotalTemperatureRatio();
			void calTemperatureRatio();
			void calArea();
			//functions that will not touch the class variables
			double calFLD(double) ;
			double calPressureRatio(double) ;
			double calDensityRatio(double);
			double calVelocityRatio(double);
			double calTotalPressureRatio(double) ;
			double calTotalTemperatureRatio(double);
			double calTemperatureRatio(double);
			double calArea(double);

			// specific functions to fanno flow to be add in presenting
			// the results

			void showHead(int );
			void showResults(int);
			void makeTexTableHead(int ); 
			void makeTexTableLine (int );
};

#endif  /* _FANNO_STDLIB */
/********************************************************
	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip> 
#  include "compressibleFlow.h"
#  include "shock.h"
#  include "pipeFlow.h"
#  include "fanno.h"
#  include "definition.h"

#  include "isothermal.h"
#  include "stagnation.h"
#  include "isothermalNozzle.h"
#  include "externalFun.h"


/********************************************************
	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/
#ifndef _ISOTHERMAL_STDLIB
#define _ISOTHERMAL_STDLIB

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip> 
#  include "definition.h" 
#  include "compressibleFlow.h"
#  include "pipeFlow.h"
#  include "shock.h"



class isothermal : public pipeFlow {
        protected:
        public:
			isothermal(double spesificHeatRatio ):
					//inilazation of base class
				//compressibleFlow(Mach,spesificHeatRatio)
				pipeFlow (spesificHeatRatio, isothermalClass)
					 { 
						// specific initialization process for derived class
						//the speed of the chocked flow is 1 for fanno flow
						M1maxSub = 1./ sqrt(spesificHeatRatio);
						singularPoint = M1maxSub ; 
						superSonicFLD = 0.821508116;
			  		};
			~isothermal(){};
			//spesific functions of this model of flow to calculate
			//spesific parameters of this flow model.
			//void calMach( int, double * ) ;
			void calFLD();
			void calPressureRatio() ;
			//void calDensityRatio();
			//void calVelocityRatio();
			void calTotalPressureRatio() ;
			void calTotalTemperatureRatio();
			void calArea();
			//functions that will not touch the class variables
			double calFLD(double) ;
			double calPressureRatio(double) ;
			//double calDensityRatio(double);
			//double calVelocityRatio(double);
			//double calTotalPressureRatio(double) ;
			//double calTotalTemperatureRatio(double);
			//double calArea(double);

			// specific functions to fanno flow to be add in presenting
			// the results
			void showHead(int );
			void showResults(int);
			void makeTexTableHead(int ); 
			void makeTexTableLine (int );
};

#endif  /* _ISOTHERMAL_STDLIB */
/********************************************************
	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, isthermalNozzle and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#ifndef _ISOTHERMALnozzle_STDLIB
#define _ISOTHERMALnozzle_STDLIB

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip> 
#  include "compressibleFlow.h"
#  include "shock.h"
#  include "stagnation.h"

class isothermalNozzle : public compressibleFlow {
        protected:
        public:
			// to change the initialization later on
			// so that base class initialization will be considered
			isothermalNozzle(double spesificHeatRatio ):
				//initialization of base class
				compressibleFlow(spesificHeatRatio, isoThermalNozzleClass)
					 { 
					// specific initialization process for the derived class
						M1maxSub = 1./ sqrt(spesificHeatRatio);
						singularPoint = M1maxSub ; 
			  		};
			~isothermalNozzle(){};
			//spesific functions of this model of flow to calculate
			//spesific parameters of this flow model.
			// 
			void setMach(double Mach) {M = Mach ;};
			void calMach( int, double * ) ;
            void calPressureRatio() ;
            void calTemperatureRatio();
            void calDensityRatio();
            void calAreaRatio();
            void calPARRatio();
            void makeTexTableHead(int );
            void makeTexTableLine (int );
            double calPressureRatio(double ) ;
			int calAll(int, int, double *);

};

#endif  /* _ISOTHERMALnozzle_STDLIB */
/********************************************************
	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/
#ifndef _PIPEflow_STDLIB
#define _PIPEflow_STDLIB

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip> 
#  include "definition.h" 
#  include "compressibleFlow.h" 
#  include "shock.h"

class pipeFlow:public compressibleFlow {
        protected:
        public:
			// to change the initialization later on
			// so that base class initialization will be considered
			//pipeFlow(double Mach, double spesificHeatRatio ):
			pipeFlow(double spesificHeatRatio , int dcn):
					//inilazation of base class
				//compressibleFlow(Mach,spesificHeatRatio)
				compressibleFlow(spesificHeatRatio, dcn)
					 { 
						isPipeFlow = yes;
						// specific initialization process for derived class
			  		};
			~pipeFlow(){};
			//spesific functions of this model of flow to calculate
			//spesific parameters of this flow model.
			void calMach( int, double * ) ;
			void calM1( int, double * ) ;
			//functions that will not touch the class variables

			// specific functions to fanno flow to be add in presenting
			// the results
			//void showHead(int );
			//void showResults(int);
			//void makeTexTableHead(int ); 
			//void makeTexTableLine (int );
};

#endif  /* _PIPEflow_STDLIB */
/********************************************************
	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/
#ifndef _SHOCK_STDLIB
#define _SHOCK_STDLIB

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip> 
#  include "definition.h"
#  include "compressibleFlow.h"

class shock: public compressibleFlow {
        protected:
        public:
			// to change the initialization later on
			// so that base class initialization will be considered
			shock(double spesificHeatRatio ):
				//initialization of base class
				compressibleFlow(spesificHeatRatio, shockClass)
					 { 
						// specific initialization process for the derived class
						//the critical M is 1 for shock flow
						M1maxSub = 1.0;
						singularPoint = M1maxSub ; 
			  		};
			~shock(){};
			//spesific functions of this model of flow to calculate
			//spesific parameters of this flow model.
			// 
            void setMach(double Mach) {Mx = Mach ;};
			void calMach( int, double * ) ;
			void calMy() ;
			double calMy(double);
			void calPressureRatio() ;
			double calPressureRatio(double, double ) ; //PyPx f (Mx,My)
			void calDensityRatio();
			//void calVelocityRatio();
			void calTotalPressureRatio() ;
			//void calTotalTemperatureRatio();
			void calTemperatureRatio();
			void calArea();
            void changeMx ( double mach) {Mx = mach; } ;
            void makeTexTableHead(int );
            void makeTexTableLine (int );
            void calMx (int, double *) ;
			int calAll(int, int, double *);

			//functions that do not touch the class variables
			//double calFLD(double) ;
			//double calPressureRatio(double) ;
			//double calDensityRatio(double);
			//double calVelocityRatio(double);
			//double calTotalPressureRatio(double) ;
			//double calTotalTemperatureRatio(double);
			//double calTemperatureRatio(double);
			//double calArea(double);
            //double getPressure(){return PyPx ; };
            //double getTemperatue (){return TyTx ; };
            //double getDensity (){return RyRx ; };
            //double getArea (){return Area ; };

};
#endif  /* _SHOCK_STDLIB */
/********************************************************
	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip> 
#  include "definition.h" 
#  include "compressibleFlow.h"
#  include "shock.h"

#ifndef _STAGNATION_STDLIB
#define _STAGNATION_STDLIB


class stagnation : public compressibleFlow {
        protected:
        public:
			// to change the initialization later on
			// so that base class initialization will be considered
			stagnation(double spesificHeatRatio ):
				//initialization of base class
				compressibleFlow(spesificHeatRatio, stagnationClass)
					 { 
					// specific initialization process for the derived class
						M1maxSub = 1.0;
						singularPoint = M1maxSub ; 
			  		};
			~stagnation(){};
			//spesific functions of this model of flow to calculate
			//spesific parameters of this flow model.
			// 
			void setMach(double Mach) {M = Mach ;};
			void calMach( int, double * ) ;
            void calPressureRatio() ;
            void calTemperatureRatio();
            void calDensityRatio();
            void calAreaRatio();
            void calPARRatio();
            void makeTexTableHead(int );
            void makeTexTableLine (int );
            double calPressureRatio(double ) ;
			int calAll(int, int, double *);

};

#endif  /* _STAGNATION_STDLIB */
/********************************************************
	FILE:compressibleFlow.cc

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip>
#  include "stagnation.h"
#  include "externalFun.h"
#  include "compressibleFlow.h" 
#  include "gasDynamics.h" 
using namespace std;

// compressible Flow function definitions
//*******************************************

void 	compressibleFlow::makeTexTableTail() {
	cout << "\\end{longtable}" << endl ;
};


int 	compressibleFlow::calM1forFLDp2p1(double FLD, double P2P1){
	double M1max ; // the maximum value of Mach entrance to tube.
	double M1TMP ; //the TMP holder of M1
	//double M2est ; //the estimate of current M2
	// the first guess the addition of fld 	
	// find a better guess for this process
	fld = FLD;// change to the class variable 
	p2p1 = P2P1; //change to class variable
	/*  the main routine */
	/*  initialization process  and guessing */
	// first guess what is the range that M1 can be ... i.e. 0.0001 to M1max	
	/* calculate M1max */
	M1TMP = 0.1 ; // just to indicate that it is subsonic flow
	M1max = calMForFLD(FLD,M1TMP );
	// check the whether the flow is chocked.
	// check whether p2/p1 is below the critical pressure ratio.
	M1TMP = root ( calP2P1_Estmt, this, .00001,  M1maxSub, FLD) ;
	M = M1max; // to make tmp assigned to M  
	calPressureRatio();
	if ((1./PRstar) >= p2p1 ||  M1TMP < 0.0 ) {
					//the flow is chocked
		M1 = M1max;
		M2 = M1maxSub;
		fld1 = FLD;
		fld2 = 0.0;
		isChoked = yes;
		p2p1 = 1./PRstar;
		return yes;
	}
    isChoked = no;
	// now the pair fld and M1max are known.
	// try to calculate/estimate the M1 for this pair 
	// including p2p1 pass trough the class variable 

	// insert the information about M2 and fld1 and fld2
	fld1 = calFLD(M1TMP);
	if ( fld1 < FLD) { // numerical error 
		fld1 = FLD;
		fld2 =0.0;
		M1 = M1max;
		M2 = M1maxSub;
		return yes;
	}
	fld2 = fld1 - FLD;
	// insert the information about M2 and fld1 and fld2
	M2 = calMForFLD(fld2,M1max ); 
//	M2 = M1 ; 
	if ( M2 <= 0.0001) {// incompressible flow
	cout << " this flow is incompressible" << endl;
	return no; // no solution 
	}
	// note that M1 is lost and has to be put again from tmp place holder  
	M1 = M1TMP;
	// and again return given value of fld
	fld = FLD;
	
	// just checking to see if working to clean later
	//cout << "p2/p2 = " << p2p1<< "\n" ;
	//cout << "calculated M1 = " << M1 << "\n" ;
	return yes;
} ;

int 	compressibleFlow::calM1forM2(double FLD, double m2, double * m1){
	double FLD2tmp ; // fld2 for the pair info (FLD, m2) 
	FLD2tmp = calFLD(m2) ;
	if (m2 <= 1.0 ) { // sub sonic flow 
		fld1 = FLD2tmp + FLD;
		*m1= calMForFLD (fld1,m2);
		if ( *m1 > 0.0 ) { 
			return yes;
		}
		else {
			return no;
		}
	}
	else if (m2 > 1.0 ) { // super sonic flow 
		if (FLD < superSonicFLD) {// within possible range	
			fld1 = FLD2tmp + FLD;
			*m1= calMForFLD (fld1,m2);
			return yes;
		}
		else if (FLD == superSonicFLD) {// the maximum possible range
			 *m1= infinity; // 1.79769313486231570E+308 ;// m1 = infinity
			return yes;
		}
		else { // the above the maximum range
			return no;
		}
	}
	return yes;
} ;

int 	compressibleFlow::calM2forM1(double FLD, double m1, double * m2){
	if (isChoked == yes) {
		*m2 = M1maxSub ; 
		return yes;
	}
    double FLD1tmp ; // fld2 for the pair info (FLD, m2)
    FLD1tmp = calFLD(m1) ;
    if (m1 <= M1maxSub ) { // subsonic flow
        fld2 = FLD1tmp - FLD;
        *m2= calMForFLD (fld2,m1);
		if ( *m2 > 0.0 ) {
        	return yes;
		}
		else {
        	return no;
		}
    }
    else if (m1 > M1maxSub ) { // super sonic flow
        if (FLD < superSonicFLD) {// within max possible range sup flow
			if ( FLD1tmp > FLD ) { //shockless and chokless
				fld2 = FLD1tmp - FLD;
				*m2= calMForFLD (fld2,m1);
				return yes;
			}
			else { // choked and must be a shock somewhere in tube
				*m2 = 1;
				return yes;
			}
        }
        else if (FLD == superSonicFLD) {// the maximum possible range
             *m2= infinity; // 1.79769313486231570E+308 ;// m1 = infinity
            return yes;
        }
        else { // the above the maximum range
            return no;
        }
    }
	return yes;
};

void 	compressibleFlow::showVariable(double * variableToBePrinted,
			int numberVariables){
	int space;
	space = 10;
    int stdPrecision;
    stdPrecision = 4;
    cout.setf (  std::ios_base::left, 
                     std::ios_base::basefield );
    for (int i=0; i < numberVariables ; i++){
    	if ( fabs( variableToBePrinted[i]) < 10.0 )
       		{cout.width(space); cout.precision(4);}
        else {cout.width(space);    cout.precision(3);};
            cout << showpoint << variableToBePrinted[i] ; // << "\t" ;
        }
        cout << endl ;
};

void 	compressibleFlow::showVariableTeXLine (double * variableToBePrinted,
			int numberVariables){
	int space;
	space = 9;
    int stdPrecision;
	//move from the global variable to local 
	//future more smart manipulation for formating 
    stdPrecision = precision;
    cout.setf ( std::ios_base::right,
                     std::ios_base::basefield );
	cout << setiosflags(ios::uppercase);
	//cout << setfill('0');

    for (int i=0; i < numberVariables ; i++){
    	if ( fabs( variableToBePrinted[i]) >= 100.0 ) { // three digits	
            cout << setiosflags(ios::fixed | ios::showpoint);
			cout.width(space);    
			cout.precision(stdPrecision- 2);
		}
        else if ( fabs( variableToBePrinted[i]) >= 10.0 ) {
			cout << setiosflags(ios::fixed | ios::showpoint);
			cout.width(space);
			cout.precision(stdPrecision);
		}
		else if ( fabs( variableToBePrinted[i]) >= 1.0 ){
			cout << setiosflags(ios::fixed | ios::showpoint);
			cout.width(space);
            cout.precision(stdPrecision );
		}
		else if ( fabs( variableToBePrinted[i]) >= 0.01 ){
			cout << setiosflags(ios::fixed | ios::showpoint);
			cout.width(space +1 );
            cout.precision(stdPrecision+1);
		}
		else if ( fabs( variableToBePrinted[i]) >= 1.25E-6 ){
			cout.setf(ios::scientific);
			cout.width(space +1 );
            cout.precision(stdPrecision -1 );
		}
		else if ( fabs( variableToBePrinted[i]) < 1.25E-6 ){
            cout.setf(ios::scientific);
            cout.width(space  );
            cout.precision(stdPrecision -2 );
			//make variable zero
			variableToBePrinted[i] = 0.0;
        };
		//cout <<  showpoint << variableToBePrinted[i] ;
		cout <<  variableToBePrinted[i] ;
		if (i +1 < numberVariables) 
			{cout   << "&" ; }
	}
        cout  << " \\\\ \\hline" ;
        cout << endl ;

};

void compressibleFlow::showVariableHead (char ** variableNames,
					 int numberVariables ) {
	int space = 10;
    cout.setf ( std::ios_base::right,
                     std::ios_base::basefield );
    for (int i=0; i < numberVariables ; i++){
		cout.width(space);
		cout << variableNames[i] ;// << "\t" ;
     }
        cout << endl ;
};

void compressibleFlow :: showVariableTeXHead (char ** variableNames,
					 int numberVariables ) {
    cout << endl << endl;
    cout << "*******************************************" << endl ;
    cout << "The following stuff is the same as above/below" << endl ;
    cout << "if you use showResults with showHeads but the " << endl ;
    cout << "information is setup for the latex text processing." << endl ;
    cout << "You can just can cut and paste it in your latex file." << endl
;
    cout << "You must use longtable style file and dcolumn " << endl ;
    cout << "style files." << endl  << endl << endl;
    cout << "*******************************************" << endl ;
    cout << "\\setlongtables" << endl ;
    cout << "\\begin{longtable}" << endl ;
	cout << "{" ; // before the line
	//calculated the width of the table
	int tableWidthSuppliment;
	if ( numberVariables - 7 ) {
		tableWidthSuppliment = 7 - numberVariables; 
	}
	for (int i=0; i <  numberVariables; i++) {
    	cout << "|D..{" << precision - 3 + tableWidthSuppliment
				  << "." << precision + tableWidthSuppliment << "}";
	};
	cout << "|}" << endl ; //end of the line
    cout << "\\caption{ ??  \\label{?:tab:?}}\\\\"  << endl;
    cout << "\\hline" << endl ;

    for (int i=0; i < numberVariables ; i++){
		cout << "\\multicolumn{1}{|c|} {$" ; // the same line
		if (i == 0 ) { cout << "\\rule[-0.1in]{0.pt}{0.3 in}" ;}
		cout <<  "\\mathbf{"  ; // the same line
		cout << variableNames[i];
		cout  << "} $} ";// continue line
		if (i+1 < numberVariables ) { cout << " & ";}
		cout  << endl;
	}
    cout << endl << "\\\\\\hline" << endl;
    cout << endl ;
    cout << "\\endfirsthead" << endl ;
    cout << "\\caption{ ?? (continue)} \\\\\\hline" << endl ;

    for (int i=0; i < numberVariables ; i++){
		cout << "\\multicolumn{1}{|c|} {$" ; // the same line
		if (i == 0 ) { cout << "\\rule[-0.1in]{0.pt}{0.3 in}" ;}
		cout <<  "\\mathbf{"  ; // the same line
		cout << variableNames[i];
		cout  << "} $} ";// continue line
		if (i+1 < numberVariables ) { cout << " & ";}
		cout  << endl;
	}
    cout << endl << "\\\\\\hline" << endl;
    cout << "\\endhead" << endl;
};

int compressibleFlow:: calTableLine (int whatInfo,  int variableName, double * variableValue){
	double m1tmp; //tmp m1 place holder
	double m2tmp; //tmp m2 place holder
    double m2, m1 ; //  local m2, m1
	double FLD ; // local fld
	//perhaps to put isRange variable check
	if (whatInfo == infoStagnation ){
		//cal the stagnation regular  variables
		return calAll(whatInfo, variableName,variableValue);
	}
	else if (whatInfo == infoStandard ){
		calMach(variableName, variableValue);
		if (variableName == MxV){
			return calAll(whatInfo, variableName,variableValue);
		}
		else if (variableName != fldV){
			calFLD();
		}
		if (drivedClassName != isothermalClass){
        	calDensityRatio();
			calTemperatureRatio();
		}
		else{
        	calTotalTemperatureRatio();
		}
        calTotalPressureRatio();
        calPressureRatio();
        calVelocityRatio();
        return yes;
	}
	else if (whatInfo == infoTube ){
		calM1(variableName, variableValue);
		if (calM2forM1(variableValue[fldN], M1, &m2tmp) == yes) {
            M2 = m2tmp;
			if(variableName == p2p1FLDV) {
            	fld = variableValue[fldN];
			}
			if (isChoked == yes){
            	p2p1 = calPressureRatio (M2)/calPressureRatio (M1);
				fld1 = variableValue[fldN] ;
            	fld2 = 0.0;
			}
			else {
				p2p1 = variableValue[p2p1N];  
            	fld1 = calFLD(M1);
            	fld2 = calFLD(M2);
			}
			return yes;	
		}
		else {
			return no;
		}
	}
	else if (whatInfo == infoShock ){
		return calAll(whatInfo, variableName,variableValue);
    }
	else if (whatInfo == infoTubeShock ){
		//double leftFLD, rightFLD, extraFLD, shockLocation;
		double  extraFLD, shockLocation;
		calM1(variableName, variableValue);
		//  the shock at the entrance: check if supersonic flow possible
		if (M1 > 1.0 ) { //supper sonic flow
			maxPossibleFLDsup = calFLD (M1);			
			shock s(k);
			m1tmp = s.calMy(M1);// shock at the entrance (M1y)
            maxPossibleFLDsub = calFLD (m1tmp);
			//calM2forM1(fld,m1tmp, &M2upper) ;
			if (fld<=maxPossibleFLDsub){//is subSonic possible entire tube 
				extraFLD = fld - maxPossibleFLDsup;
				shockLocation = root (calShockLocationM_Estmt, this, 0.,
							 maxPossibleFLDsup - EPS, extraFLD ) ;
				M2 = 1.0;
				fldUp =  shockLocation;
				fldDown = fld - fldUp;
				My =  calMForFLD (fldDown , 0.1 ); //subsonic branch
				Mx =  s.calMy(My) ; //symmetrical function
				return yes;
			}
			else {// not possible not enough fld
				 return no;
			}
        }
        else { //the flow is subsonic
            return no;
        }
	}
    else if (whatInfo == infoTubeProfile){

		//double leftFLD, rightFLD, extraFLD; 
		double  extraFLD; 
		double shockLocation;
		shock s(k);
		//cal left side
		fld = FLD = variableValue[fldN];
		M1 = variableValue[m1N]; 
		maxPossibleFLDsup = calFLD (M1);
		extraFLD = fld - maxPossibleFLDsup;
		shockLocation =
		 root (calShockLocationM_Estmt, this, 0., maxPossibleFLDsup - EPS, extraFLD ) ;
		M2 = 1.0;
		fldUp =  shockLocation;
		fldDown = fld - fldUp;
        My =  calMForFLD (fldDown , 0.1 ); //subsonic branch
        Mx =  s.calMy(My) ; //symmetrical function
		return yes;
	}
    else if (variableName == M1fldV){ // 
		//cal the Mach and pressure profiles for M1 (supSonic) 
		double deltaFLD ;// tube variable (xi)
		int i, shockLocation;
        fld = FLD = variableValue[fldN];
        M1 =  m1 = variableValue[m1N];
		p2p1 =  variableValue[p2p1N];
		shockLocation =  (int) variableValue[shockLocationN];
		//calculate the max fld (m1)
        maxPossibleFLDsup = calFLD (m1);
        shock s(k);
        m1tmp = s.calMy(m1);//(M1y) to use lower limit entrance shock

		//to be remove
        //maxPossibleFLDsub = calFLD (m1tmp);
        //calM2forM1(FLD,m1tmp, &M2upper);

		deltaFLD = FLD / fabs((double) (numberNodes -1 )) ; 
		double p1pStar; // , pxpStar, pxpy, pypStar, p2pStar; 
		
		// cal the entrance pressure ratio
		p1pStar = calPressureRatio(m1) ;
		//use the calculations of supSonic branch to shock
		//assign new fldx;  cal shock relationship fldy, My etc   
		//shockLocation = 1;	
		Mfld[fldP][0]= 0. ;
		Mfld[MxP][0] = M1;
		Mfld[MyP][0] = 0.0;
		Mfld[PyPxP][0] = 0.0;
		Mfld[PxPsP][0] = p1pStar;
		Mfld[PyPsP][0] = 0.0;
		Mfld[P1PiP][0] = 1.0 ;
		Mfld[P1PiP][1 ] = 1.0;
		double Lfld [numberNodes]; //local variable for leftover fld
		//The section before the shockLocation
        for (i = 1; i <shockLocation    ; i++){
            //upstream "before" shock location
            Mfld[fldP][i] = Mfld[fldP][i-1] + deltaFLD;
            // M = f(fld, branch)
            Lfld [i] =  maxPossibleFLDsup -  Mfld[fldP][i]; 
            Mfld[MxP][i] = calMForFLD(  Lfld [i], 10.0 );
            Mfld[MyP][i] = 0.0 ;
            Mfld[PyPxP][i] = 0.0 ;
            Mfld[PxPsP][i] = calPressureRatio( Mfld[MxP][i]);//p(i+1)/p*
            Mfld[PyPsP][i] = 0.0;//p(i+1)/p*
            Mfld[P1PiP][i] =  p1pStar/Mfld[PxPsP][i];//[p1/p(i)]*[p(i-1)/p(i)]
			//tmp reverse
			Mfld[P1PiP][i] = 1./ Mfld[P1PiP][i] ;
        }

		//@ shock location calculation
		//fld
		Mfld[fldP][shockLocation] = ((double) shockLocation) * deltaFLD;
		//Mx
		Mfld[MxP][shockLocation] =
			 calMForFLD(maxPossibleFLDsup - Mfld[fldP][shockLocation],
							 10.0 ); //sup branch
		//cal My
		Mfld[MyP][shockLocation] = s.calMy( Mfld[MxP][shockLocation] ); 
		//cal pypx
		Mfld[PyPxP][shockLocation] = s.calPressureRatio(
				 Mfld[MxP][shockLocation], Mfld[MyP][shockLocation] ); 
		//cal pxpStar
		Mfld[PxPsP][shockLocation] = calPressureRatio(Mfld[MxP][shockLocation]);
		//cal pypStar 
		Mfld[PyPsP][shockLocation] = calPressureRatio(Mfld[MyP][shockLocation]);
		//cal p1/p(i)
		Mfld[P1PiP][shockLocation] =  p1pStar/Mfld[PxPsP][shockLocation]
				/ Mfld[PyPxP][shockLocation];
 

		//After the shock
		Lfld [shockLocation] = calFLD(Mfld[MyP][shockLocation] );
		for (i= (shockLocation+1); i   < numberNodes   ; i++){
			//downstream "after" shock location
	        Mfld[fldP][i ] = Mfld[fldP][i-1] + deltaFLD;
			// M = f(fld, branch) 
			Lfld [i] =  Lfld [shockLocation] -
					 (double) (i - shockLocation) * deltaFLD;	
			Mfld[MxP][i ] = calMForFLD(  Lfld [i], 0.1 );
			Mfld[MyP][i ] = 0.0 ;
			Mfld[PyPxP][i] = 0.0 ;
			Mfld[PxPsP][i] = calPressureRatio( Mfld[MxP][i]);//p(i+1)/p*
			Mfld[PyPsP][i] = 0.0;//p(i+1)/p*
			Mfld[P1PiP][i] =  Mfld[P1PiP][shockLocation]*
					Mfld[PyPsP][shockLocation] /Mfld[PxPsP][i] ; 
          //tmp reverse pressure ratio 
			Mfld[P1PiP][i] = 1.0 /   Mfld[P1PiP][i]; 
		}
          //tmp reverse pressure ratio 
		 Mfld[P1PiP][shockLocation] = 1./ Mfld[P1PiP][shockLocation];
		return yes;
	}
    if (variableName == machV){
        M = variableValue[mN] ;
        calPressureRatio();
        calDensityRatio();
		calTemperatureRatio();
		if (drivedClassName == stagnationClass){
			calAreaRatio();
			calPARRatio();
		}
		else {
			calFLD();
			calTotalPressureRatio();
			calVelocityRatio();
		}
        return yes;
    }
	else if (variableName == fldV ) {
		fld = variableValue[fldN] ;			
        M = calMForFLD(variableValue[fldN],variableValue[mN]) ;
        calDensityRatio();
        calTotalPressureRatio();
        calPressureRatio();
        calTemperatureRatio();
        calVelocityRatio();
        return yes;
    }
	// to fix this problem the same ******************fldV
	else if (variableName == fldV) {
		calM1forFLDp2p1( variableValue [fldN], variableValue [p2p1N] );
	}
	else if (variableName == M1V){
		if ( calM2forM1(variableValue[fldN], variableValue[m1N],
				 &m2tmp) == yes)//calculate M1 f(fld, M2)
		{
			M1= variableValue[m1N];
			M2 = m2tmp;
			fld = variableValue[fldN];
			fld1 = calFLD(M1);
			fld2 = calFLD(M2);
    		p2p1 = calPressureRatio (M2)/calPressureRatio (M1);

			return yes ;
		}		
		else {
			return no;
		}
	}
    else if (variableName == M1ShockV){
        FLD = variableValue[fldN] ;
		m1 = variableValue[m1N];
		// just after the shock at the entrance
		// check if supper sonic flow
		if (m1 > 1.0 ) { //supper sonic flow
			maxPossibleFLDsup = calFLD (m1);			
			shock s(k);
			m1tmp = s.calMy(m1);// shock at the entrance (M1y)
            //maxPossibleFLDsub = calFLD (m1tmp);

			M1 = m1;// place into the class variable
			calM2forM1(FLD,m1tmp, &M2upper) ;
		
			if (FLD<=maxPossibleFLDsup){//is supSonic possible for whole tube
									//shock can occur at exit
				calM2forM1(FLD,m1, &m2);  // what to do if not?   
    			M2lower = s.calMy(m2);
				return yes;
			}
			else {// no shock at the entrance possible
				// to check if the shock is before entrance
			//M1 = m1;// place into the class variable
				M2lower = -1.0;
				if (M2upper> 0.0) {return yes;} 
				else { return no;}
			}
        }
        else {
            return no;
        }
    }
	else if (variableName == FLDShockV){
        FLD = variableValue[fldN];
		m1 =  variableValue[m1N];
		//tobecontinue
		//find mim possible m1  sub and sup 
		//min
    	maxPossibleFLDsup = calFLD (m1);
    	shock s(k);
    	m1tmp = s.calMy(m1);// shock at the entrance (M1y)
    	//maxPossibleFLDsub = calFLD (m1tmp);
    	                                                                      
    	M1 = m1;// place into the class variable
    	calM2forM1(FLD,m1tmp, &M2upper) ;
    	                                                                      
    	if (FLD<=maxPossibleFLDsup){//is supSonic possible for whole tube
    	                        //shock can occur at exit
    	    calM2forM1(FLD,m1, &m2);  // what to do if not?
    	    M2lower = s.calMy(m2);
    	    return yes;
    	}
    	else {// no shock at the entrance possible
    	    // to check if the shock is before entrance
    	//M1 = m1;// place into the class variable
    	    M2lower = -1.0;
    	    if (M2upper> 0.0) {return yes;}
    	    else { return no;}
    	}
    }
    else if (variableName == MFLDshockLocationV ){ // 
		//double leftFLD, rightFLD, extraFLD; 
		double  extraFLD; 
		double shockLocation;
		shock s(k);
		//cal left side
		fld = FLD = variableValue[fldN];
		M1 = variableValue[m1N]; 
		maxPossibleFLDsup = calFLD (M1);
		extraFLD = fld - maxPossibleFLDsup;
		shockLocation =
		 root (calShockLocationM_Estmt, this, 0., maxPossibleFLDsup - EPS, extraFLD ) ;
		M2 = 1.0;
		fldUp =  shockLocation;
		fldDown = fld - fldUp;
        My =  calMForFLD (fldDown , 0.1 ); //subsonic branch
        Mx =  s.calMy(My) ; //symmetrical function
		return yes;
	}
    else if (variableName == M1fldV){ // 
		//cal the Mach and pressure profiles for M1 (supSonic) 
		double deltaFLD ;// tube variable (xi)
		int i, shockLocation;
        fld = FLD = variableValue[fldN];
        M1 =  m1 = variableValue[m1N];
		p2p1 =  variableValue[p2p1N];
		shockLocation =  (int) variableValue[shockLocationN];
		//calculate the max fld (m1)
        maxPossibleFLDsup = calFLD (m1);
        shock s(k);
        m1tmp = s.calMy(m1);//(M1y) to use lower limit entrance shock

		//to be remove
        //maxPossibleFLDsub = calFLD (m1tmp);
        //calM2forM1(FLD,m1tmp, &M2upper);

		deltaFLD = FLD / fabs((double) (numberNodes -1 )) ; 
		double p1pStar; //, pxpStar, pxpy, pypStar, p2pStar; 
		
		// cal the entrance pressure ratio
		p1pStar = calPressureRatio(m1) ;
		//use the calculations of supSonic branch to shock
		//assign new fldx;  cal shock relationship fldy, My etc   
		//shockLocation = 1;	
		Mfld[fldP][0]= 0. ;
		Mfld[MxP][0] = M1;
		Mfld[MyP][0] = 0.0;
		Mfld[PyPxP][0] = 0.0;
		Mfld[PxPsP][0] = p1pStar;
		Mfld[PyPsP][0] = 0.0;
		Mfld[P1PiP][0] = 1.0 ;
		Mfld[P1PiP][1 ] = 1.0;
		double Lfld [numberNodes]; //local variable for leftover fld
		//The section before the shockLocation
        for (i = 1; i <shockLocation    ; i++){
            //upstream "before" shock location
            Mfld[fldP][i] = Mfld[fldP][i-1] + deltaFLD;
            // M = f(fld, branch)
            Lfld [i] =  maxPossibleFLDsup -  Mfld[fldP][i]; 
            Mfld[MxP][i] = calMForFLD(  Lfld [i], 10.0 );
            Mfld[MyP][i] = 0.0 ;
            Mfld[PyPxP][i] = 0.0 ;
            Mfld[PxPsP][i] = calPressureRatio( Mfld[MxP][i]);//p(i+1)/p*
            Mfld[PyPsP][i] = 0.0;//p(i+1)/p*
            Mfld[P1PiP][i] =  p1pStar/Mfld[PxPsP][i];//[p1/p(i)]*[p(i-1)/p(i)]
			//tmp reverse
			Mfld[P1PiP][i] = 1./ Mfld[P1PiP][i] ;
        }

		//@ shock location calculation
		//fld
		Mfld[fldP][shockLocation] = ((double) shockLocation) * deltaFLD;
		//Mx
		Mfld[MxP][shockLocation] =
			 calMForFLD(maxPossibleFLDsup - Mfld[fldP][shockLocation],
							 10.0 ); //sup branch
		//cal My
		Mfld[MyP][shockLocation] = s.calMy( Mfld[MxP][shockLocation] ); 
		//cal pypx
		Mfld[PyPxP][shockLocation] = s.calPressureRatio(
				 Mfld[MxP][shockLocation], Mfld[MyP][shockLocation] ); 
		//cal pxpStar
		Mfld[PxPsP][shockLocation] = calPressureRatio(Mfld[MxP][shockLocation]);
		//cal pypStar 
		Mfld[PyPsP][shockLocation] = calPressureRatio(Mfld[MyP][shockLocation]);
		//cal p1/p(i)
		Mfld[P1PiP][shockLocation] =  p1pStar/Mfld[PxPsP][shockLocation]
				/ Mfld[PyPxP][shockLocation];
 

		//After the shock
		Lfld [shockLocation] = calFLD(Mfld[MyP][shockLocation] );
		for (i= (shockLocation+1); i   < numberNodes   ; i++){
			//downstream "after" shock location
	        Mfld[fldP][i ] = Mfld[fldP][i-1] + deltaFLD;
			// M = f(fld, branch) 
			Lfld [i] =  Lfld [shockLocation] -
					 (double) (i - shockLocation) * deltaFLD;	
			Mfld[MxP][i ] = calMForFLD(  Lfld [i], 0.1 );
			Mfld[MyP][i ] = 0.0 ;
			Mfld[PyPxP][i] = 0.0 ;
			Mfld[PxPsP][i] = calPressureRatio( Mfld[MxP][i]);//p(i+1)/p*
			Mfld[PyPsP][i] = 0.0;//p(i+1)/p*
			Mfld[P1PiP][i] =  Mfld[P1PiP][shockLocation]*
					Mfld[PyPsP][shockLocation] /Mfld[PxPsP][i] ; 
          //tmp reverse pressure ratio 
			Mfld[P1PiP][i] = 1.0 /   Mfld[P1PiP][i]; 
		}
          //tmp reverse pressure ratio 
		 Mfld[P1PiP][shockLocation] = 1./ Mfld[P1PiP][shockLocation];
		return yes;
    }
	else if (variableName == M1fldP2P1V) { //find shock location
		double P2Pstar1; // P2/Pstar1 = (P2/P1)/( P^*1/P1)  
		double Pstar1P1; // P^*1/P1  
		double shockLocation;
		stagnation st(k);
		shock s(k);
        fld = FLD = variableValue[fldN];
        M1 =  m1 = variableValue[m1N];
        p2p1 =  variableValue[p2p1N];
		Pstar1P1 = st.calPressureRatio(M1); 
		P2Pstar1 = p2p1/Pstar1P1;
		shockLocation = 
			root ( calShockLocation_Estmt,  this,  0., fld ,  P2Pstar1  ) ;
		M2 = 1.0;
		fldUp = fld * shockLocation;
		fldDown = fld - fldUp;
		My =  calMForFLD (fldDown , 0.1 ); //subsonic branch
		Mx =  s.calMy(My) ; //symmetrical function 

		return yes;	
	}
	return no;
};


int   compressibleFlow::makeTexTableLine (int whatInfo, int variblelName, 
			double * variableValue) {
	// make the necessarily calculation	
	calTableLine (whatInfo, variblelName, variableValue);
	//make the LaTeX table
	makeTexTableHead(yes ); // whatInfo variable
	makeTexTableLine(yes); //
	makeTexTableTail();
	return yes;
};


int   compressibleFlow::makeTable (int whatInfo, int isRange, int isTeX,
		  int variableName, double * variableValue ) {
    // make the necessary calculations
    if ( isRange == no ) {
        if (calTableLine (whatInfo, variableName,  variableValue) == no) {
			return no;
		}
        if (isTeX == yes){
        //make the LaTeX table
            makeTexTableHead(whatInfo); 
            makeTexTableLine(whatInfo); 
            makeTexTableTail();
        }
        else {
            showHead(whatInfo);
            showResults(whatInfo);
        }
    }
    else { // isRange==yes
		manegerCalTableLine(isTeX, whatInfo, variableName, variableValue);
    }
    return yes;
};


//when M is known use this function
int compressibleFlow:: calTableLine (void ){
    // calculated  all the important parameters and see if all exist.
    // to be complete later
	// calculate the M2 by find fld(M1) and subtract fld [given]
		
	fld1 = fld ;
    fld = calFLD(M1) ;
	fld2 = fld1 - fld ;	
	// the fld as fun of M2 
	
    calPressureRatio() ;
    calTotalPressureRatio() ;
    calTotalTemperatureRatio() ;

    return 1;
};

int compressibleFlow:: makeLines ( int whatInfo, int isTex,
	 int numberOFsteps, double startingPoint, double lastPoint,
	 int variableName, double * variableValue ){
	int givenVariable;
	int isItWorking = yes;
    double h, range, delta ;
	range = fabs(startingPoint - lastPoint); //regardless the end point 	
							// if startingPoint >  lastPoint positive  
	delta = range /((double) numberOFsteps); 
	h = startingPoint;
	if (variableName == MxV) { givenVariable = MxN; }
	else if (variableName == machV) {givenVariable = mN; } 
	else if (variableName == M1V) {givenVariable = mN; } 
	else if (variableName == p2p1FLDV  ) {
		givenVariable = fldN ;
		//givenVariable + 0 ;
	} // for unknown reason to make it work.
	else {
		return no;
	}
    for (int i = 0 ; (i <= abs(numberOFsteps) ) ; i++ ) {
		variableValue[givenVariable] = h ;
        if ( calTableLine(whatInfo, variableName, variableValue) == yes ){
			if (isTex == yes) {
				//changeMach ((double) h );
				makeTexTableLine(whatInfo);
			}
			else {
				showResults(whatInfo);
			};
		}
		else{
			cout << "no results are possible too large 4fL/D"<< endl; 
			isItWorking = no;
			//return no;
		};
        h = h + delta;
    };
	if (isItWorking == no) {
		cout << "some runs didn't work" << endl;
    	return no;
	}
	else {
    	return yes;
	}
};

int compressibleFlow:: makeLines (int whatInfo, int isTex, int numberOFsteps,
				double startingPoint, double lastPoint ){
    double h, range, delta;
	double variableValue[7];
	range = fabs(startingPoint - lastPoint); //make the range positive
	delta = range /((double) numberOFsteps); 
	h = startingPoint;
    for (int i = 0 ; (i <= abs(numberOFsteps)) ; i++ ) {
		variableValue[mN] = h; // much number
        calTableLine(whatInfo, machV, variableValue);
        if (isTex == yes) {
            makeTexTableLine(whatInfo);
        }
        else {
            showResults(whatInfo);
        };
        h = h + delta;
    };
    return yes;
};

int compressibleFlow :: manegerCalTableLine (int isTex, int whatInfo,
				int variableName,  double * variableValue){
    // manage the lines calculations  
		// first calculate the parameters 
		// now print either for TeX or without TeX
	//first print the header depending on the TeX or ''plain'' table
	//rework (remove) this function for providing the limits by other function
	if (isTex == yes) {
		makeTexTableHead(whatInfo);
	}
	else{
		showHead(whatInfo);
	};
	if (whatInfo == infoStandard ) {
		if (drivedClassName == isothermalClass ){
			makeLines(whatInfo,isTex, 7,0.03,  0.1, 
				variableName, variableValue );
			makeLines(whatInfo,isTex, 12,0.2,  0.8, 
				variableName, variableValue );
			makeLines(whatInfo,isTex, 4,0.81,  singularPoint, 
				variableName, variableValue );
		}
		else if (drivedClassName == fannoClass ){
            makeLines(whatInfo,isTex, 7,0.03,  0.1,
                variableName, variableValue );
            makeLines(whatInfo,isTex, 16,0.2,  1.0,
                variableName, variableValue );
            makeLines(whatInfo,isTex, 8,2.,10  ,
                variableName, variableValue );
            makeLines(whatInfo,isTex, 10,20.,70  ,
                variableName, variableValue );
        }
		goto outForNow;
	}
	else if (whatInfo == infoTube ) {
		if (drivedClassName == isothermalClass ){
		  makeLines(whatInfo,isTex,98,2.0,100.0, variableName, variableValue);
		}
		else if  (drivedClassName == fannoClass ){
          makeLines(whatInfo,isTex,98,2.0,100.0, variableName, variableValue);
        }

		goto outForNow;
	}
	if ( variableName == MxV){
		makeLines(whatInfo,isTex, 20,singularPoint+EPS, 2., variableName,
                variableValue );
		makeLines(whatInfo, isTex, 10,2.25,4.75, variableName,
                variableValue );
		makeLines(whatInfo, isTex, 10,5.,10., variableName,
                variableValue );
	}
	//makeLines(isTex, whatInfo, 20,0.05,(singularPoint -EPS ), 
	//				variableName, variableValue);
	//makeLines(isTex, whatInfo, 20,(singularPoint +EPS ), 2.0, 
	//				variableName, variableValue);
	//check the special cases are need to the limiting borders
	else if ( variableName == M1ShockV){
		//calculated the minimum entrance and exit Mach number 
		shock s(k);
		minM1entrance = calMForFLD(variableValue[fldN], 0.1); // tmp storage
		minM1entrance = s.calMy(minM1entrance) + 5*EPS;
		minM1exit = calMForFLD(variableValue[fldN], 10.0); //supper sonic flow no
									//shock at 1.0
    	makeLines(isTex, whatInfo, 2, minM1exit, minM1entrance,
                variableName, variableValue);

	}
	else if ( variableName == FLDShockV) {
		//the boundaries of aren't relevant since the output m=(fld
		//or x)
		makeLines(isTex, whatInfo, 2, 0., 0., variableName, variableValue);
	} 
	else if ( variableName == M1fldV) {
        makeLines(isTex, whatInfo, 2, 0., 0., variableName, variableValue);
    }

	//makeLines(isTex, 30, 110, 1000,variableName,  variableValue);
	if (isTex == yes) {// the tail for TeX if needed
        makeTexTableTail();
    };
    return 1;
	outForNow:  if (isTex == yes) {
					makeTexTableTail();
				}
	return yes;
};

int compressibleFlow :: manegerCalTableLine (int isTex, int whatInfo){
    // manage the lines calculations.  first if all exist.
    // to be complete later
		// first calculate the parameters 
		// now print either for TeX or without TeX
	//first print the header depending on the TeX or ''graph'' table
	if (isTex == yes) {
		makeTexTableHead(whatInfo);
	}
	else{
		showHead(whatInfo);
	};
	//make the line or the lines
	makeLines(whatInfo, isTex, 18,	0.05, 0.95);
	makeLines(whatInfo, isTex, 2,0.96,(singularPoint -0.001 ));
	makeLines(whatInfo, isTex, 20,singularPoint+ 0.0000001,2.);
	makeLines(whatInfo, isTex, 10,2.5,10.);
	// print the tail for TeX if needed
	if (isTex == yes) {
        makeTexTableTail();
    };
    return 1;
};


void  compressibleFlow :: calP2P1(){
	// redo when P2star isn't known
	//P2Pstar = calPressureRatio (M2) ;  
	//P1Pstar = calPressureRatio (M1) ; 		
    p2p1 = P2Pstar/P1Pstar;

};


double  compressibleFlow :: calMForFLD(double FLD, double mach){
	//what do when there is no solution ?
	if ((mach < singularPoint) && (FLD > EPS ) ) { // sub sonic flow 
		return  root ( calFLD_Estmt,  this, .00001,  (M1maxSub-EPS), FLD  ) ;
	}
	else if ((mach > singularPoint )  && (FLD > EPS ) ) { // super sonic flow
		return  root ( calFLD_Estmt,  this,  (M1maxSub+EPS), 100.,  FLD  ) ;
	}
	else if (FLD == 0.0 )  {
		return 0.0;
	}
	else { // no possible negative fld
		return -1.0;
	} 
};

//double fPbar (double Mach, double k){
//	return  pow (1 / ( 1 + (( k - 1) / 2 ) * Mach*Mach),(k / (k - 1) ) ) ;
//}; 

double calP_ratioEstmt ( compressibleFlow c, double estimatedMach, double value
) {
    c.changeMach (estimatedMach) ;
    //c.calArea();
    c.calFLD() ;
    c.calPressureRatio() ;
    c.calTotalPressureRatio() ;
    //s.calTotalTemperatureRatio() ;
    //s.calDensityRatio() ;
    return (value - c.getFLD() ) ;

};

/********************************************************
	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/
#ifndef _DEFINITION_STDLIB
#define _DEFINITION_STDLIB

#  include "definition.h"

int  m1N    =   13 ; // M1
int  fldN   =   11 ; // location of fld in the array
int  p2p1N  =   2 ; // location of p2/p1 in the array
int  m2N    =   3 ; // M2
int  mN     =   4 ; // location of M in the array
int  shockLocationN     =   5 ; // location of shock in the tube
int  MxN    =   18 ; // Mx
int  PbarN  =   20 ; // P over P_0
int  AstarN =   21 ; // A over A^*
int  PARN   =   22 ; // PAR
int  P0yP0xN    =   23 ; // Py0 over Px0

int fannoClass = 100;
int shockClass = 101;
int stagnationClass = 102;
int raylieghClass = 103;
int isothermalClass = 104;
int obliqueClass = 105;
int isoThermalNozzleClass = 106;

int  fldP =0 ; // location of fld in the array
int  MxP = 1 ; // location of Mx in the array
int  MyP = 2 ; // location of My in the array
int  PyPxP = 3 ; //
int  PxPsP = 4 ; //
int  PyPsP = 5 ; //
int  P1PiP = 6 ; //

#endif  /* _DEFINITION_STDLIB  */
/********************************************************
	FILE:externalFun.cc

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip>
#  include "stagnation.h"
#  include "externalFun.h"
#  include "fanno.h" 
#  include "gasDynamics.h" 
using namespace std;

//general functions utilizing the class definitions
//**************************************************
double calP0yP0xEstmt
		(compressibleFlow * c, double estimatedMM, double  value){
	c->setMach (estimatedMM) ;
	c->calMy () ;
	c->calPressureRatio () ;
	c->calTotalPressureRatio();
	return(value - c->getP0yP0x() );
}

double calPAREstmt 
		(compressibleFlow * c, double estimatedMM, double  value){
    c->setMach (estimatedMM) ;
	c->calAreaRatio();
	c->calPressureRatio();
    c->calPARRatio();
	return(value - c->getPAR() );	
};
double calAstarEstmt 
		(compressibleFlow * c, double estimatedMM, double  value){
    c->setMach (estimatedMM) ;
    c->calAreaRatio();
	return(value - c->getArea() );	
};

double calPbarEstmt 
		(compressibleFlow * c, double estimatedMM, double  value){
    c->setMach (estimatedMM) ;
    c->calPressureRatio();
    return (value - c->getPressure() ) ;
};

double calShockLocationM_Estmt 
		(compressibleFlow * c, double estimatedShockLocation, double  value){
	//calculated a correcting to estimated Shock Location 
	double FLD; //the total FLD given
	double upStreamFLD;//,downStreamFLD;// estimated fld where shock occurs
	double  downStreamFLDcal;// cal extra fld
	double k, Mx, My;
	//double M1cal, M1original;
	double maxPossibleFLDsup;
	FLD = c->getFLD();
	k =c->getk();		
	shock s(k);
	stagnation st(k);
	//cal left side
	maxPossibleFLDsup = c->getmaxPossibleFLDsup();
	upStreamFLD = maxPossibleFLDsup - estimatedShockLocation ;
	Mx = c->calMForFLD(upStreamFLD, 10. ); //in sup branch 
	My = s.calMy(Mx);
	downStreamFLDcal = c->calFLD( My); //after the shock
	return (downStreamFLDcal + estimatedShockLocation-maxPossibleFLDsup
		- value); 
	
	//downStreamFLD = FLD - upStreamFLD;
	//start with downstream branch 
	//My = c->calMForFLD(downStreamFLD, 0.1 ); //in sub branch 
	//Mx = s.calMy(My);
	//superSonic branch 
	//calculated the FLD if this Mx is correct
	//downStreamFLDcal = c->calFLD( Mx);
	//FLDcal = upStreamFLD + downStreamFLDcal;
	//M1cal = c->calMForFLD(FLDcal, 10.0 ); //in sup branch
	//M1original = c->getM1();

	//return (M1original - M1cal);
};

double calShockLocation_Estmt 
		(compressibleFlow * c, double estimatedShockLocation, double  value){
	//calculated a corrding to estimated Shock Location 
	double FLD; //the total FLD given
	double upStreamFLD, downStreamFLD;// estimated fld(s) where shock occurs
	double k, Mx, My;
	double downStreamFLDcal; //calculated the FLD for this shockLocation 
	double M1cal, M1original;
	double FLDcal; 		
	FLD = c->getFLD();
	upStreamFLD = estimatedShockLocation * FLD;
	downStreamFLD = FLD - upStreamFLD;
	k =c->getk();		
	shock s(k);
	stagnation st(k);
	//start with downstream branch 
	My = c->calMForFLD(downStreamFLD, 0.1 ); //in sub branch 
	Mx = s.calMy(My);
	//superSonic branch 
	//calculated the FLD if this Mx is correct
	downStreamFLDcal = c->calFLD( Mx);
	FLDcal = upStreamFLD + downStreamFLDcal;
	M1cal = c->calMForFLD(FLDcal, 10.0 ); //in sup branch
	M1original = c->getM1();

	return (M1original - M1cal);
};

double calFLD_Estmt (compressibleFlow * c,  double estimatedMach, double  value){
	double tmpFLD;
    //c->changeMach (estimatedMach) ;
    //s.calArea();
    tmpFLD = c->calFLD(estimatedMach) ;
    //s.calPressureRatio() ;
    //s.calTotalPressureRatio() ;
    //s.calTotalTemperatureRatio() ;
    //s.calDensityRatio() ;
    return (value - tmpFLD ) ;
};

double calP2P1_Estmt (  compressibleFlow * c, double M1est, double fld) {

	double  M2est; //the value est of the mach at the exit;
	double P1PstarEst, P2PstarEst ;// the estimated p1/P*, p2/P* 
	double tmpFLD ; // estimated FLD
	double newFLD ; // the new estimated  FLD for M2
	double p2p1est;
	double p2p1Given; // the given p2/p1
	
	//calculate the P1PstarT for M1
    c->changeMach(M1est);
	c->calPressureRatio(); //calculate the pressure ratio p1/p*
    P1PstarEst  = c->getPRstar(); // assigned the ratio to a tmp variable
	
	// need to find M2
	// calculate the "leftover" of  fld to M2
	tmpFLD = c->calFLD(M1est);
	newFLD = tmpFLD - fld ;
		
	// calculate M2 
	M2est = c->calMForFLD(newFLD,M1est);
	//note that the function calculate relationship M to fld
	// not M1 or M2
	c->changeMach(M2est);
    c->calPressureRatio(); // p2/p*
    P2PstarEst= c->getPRstar() ;
    p2p1est = P2PstarEst / P1PstarEst;
	p2p1Given = c->getP2P1() ;	
	return ( p2p1est - p2p1Given ) ;	

};

double root( double f (compressibleFlow *, double , double ),
            compressibleFlow * c , double a , double b, double value) {

   double Af =  f(c,a, value) ;
  double Bf =  f(c, b, value) ;

    double m = ( a + b ) / 2.0 ;
   double Mf =  f(c, m, value ) ;
	// if root is exist
	if ( Af * Bf  > 0.0 ) {
		// no root
	//try Secant Method
		return -1.0; 
	}

   if ( Mf == 0.0 || (b - a ) <= EPS ) {
        return m;
    }
    else if ( Af * Mf  < 0.0 ) {
        return root (f, c, a, m, value ) ;
    }
    else {
        return root(f, c, m, b, value);
    }
}

/********************************************************
	FILE:fanno.cc

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip>
#  include "stagnation.h"
#  include "externalFun.h"
#  include "fanno.h" 
#  include "gasDynamics.h" 
using namespace std;


//***************************************************************************
//fanno class functions definitions 
//******************************
void 	fanno::makeTexTableLine(int whatInfo) {
    int numberVariables;

	if (whatInfo == infoStandard) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M ;
        variableToBePrinted[1] = fld;
        variableToBePrinted[2] = PRstar;
        variableToBePrinted[3] = PzeroRstar;
        variableToBePrinted[4] = RRstar ;
        variableToBePrinted[5] = URstar;
        variableToBePrinted[6] = TRstar;
		showVariableTeXLine (variableToBePrinted, numberVariables);
	}
	else if (whatInfo == infoTube) {
		numberVariables = 6;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M1 ;
        variableToBePrinted[1] = M2;
        variableToBePrinted[2] = fld;
        variableToBePrinted[3] = fld1;
        variableToBePrinted[4] = fld2 ;
        variableToBePrinted[5] = p2p1;

		showVariableTeXLine (variableToBePrinted, numberVariables);
	}
    else if (whatInfo == infoTubeShock) {
        numberVariables = 6;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M1;
        variableToBePrinted[1] = M2;
        variableToBePrinted[2] = fldUp;
        variableToBePrinted[3] = fldDown;
        variableToBePrinted[4] = Mx;
        variableToBePrinted[5] = My;

        showVariableTeXLine (variableToBePrinted, numberVariables);
    }

};

void 	fanno::makeTexTableHead(int whatInfo) {

    int variableNumber;

	if (whatInfo == infoStandard) {
		variableNumber = 7;
		char * variableNames[variableNumber];
		variableNames[0] = "M";
		variableNames[1] = "4fL \\over D";
		variableNames[2] = "P \\over P^{*}";
		variableNames[3] = "P_0 \\over {P_0}^{*}";
		variableNames[4] = "\\rho \\over \\rho^{*}";
		variableNames[5] = "U \\over {U}^{*}";
		variableNames[6] = "T \\over T^{*}";

		showVariableTeXHead(variableNames,variableNumber);
	
	}
	else if (whatInfo == infoTube) { 
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "M_1";
        variableNames[1] = "M_2";
        variableNames[2] = "4fL \\over D";
        variableNames[3] = "\\left.{ 4fL \\over D }\\right|_{1}";
        variableNames[4] = "\\left.{ 4fL \\over D }\\right|_{2}";
        variableNames[5] = "P_2 \\over P_1";
                                                                                  
        showVariableTeXHead(variableNames,variableNumber);
	}
	else if  (whatInfo == infoTubeProfile) {
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "M_1";
        variableNames[1] = "M_2";
        variableNames[2] = "4fL \\over D";
        variableNames[3] = "\\left.{ 4fL \\over D }\\right|_{1}";
        variableNames[4] = "\\left.{ 4fL \\over D }\\right|_{2}";
        variableNames[5] = "P_2 \\over P_1";
                                                                               
                                                                               
        showVariableTeXHead(variableNames,variableNumber);
    }
    else if  (whatInfo == infoTubeShock) {
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "M_1";
        variableNames[1] = "M_2";
        variableNames[2] = "\\left.{ 4fL \\over D }\\right|_{up}";
        variableNames[3] = "\\left.{ 4fL \\over D }\\right|_{down}";
        variableNames[4] = "M_x";
        variableNames[5] = "M_y";

        showVariableTeXHead(variableNames,variableNumber);
    }
};

void	fanno::showHead(int whatInfo){
	
	cout << endl << endl <<  "Fanno flow" << "\t\t"  ;
	cout << "k = " << k << endl << endl ;

	if (whatInfo == infoStandard) {
        int variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = "M  ";
        variableNames[1] = "fld";
        variableNames[2] = "P/P*";
        variableNames[3] = "P0/P0*";
        variableNames[4] = "R/R*";
        variableNames[5] = "U/U*";
        variableNames[6] = "T/T*";
                                                                                  
        showVariableHead( variableNames, variableNumber);

	}
	else if ( whatInfo == infoTube)  {
        int variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "M1 ";
        variableNames[1] = "M2 ";
        variableNames[2] = "fld";
        variableNames[3] = "fld1";
        variableNames[4] = "fld2";
        variableNames[5] = "p2p1";
                                                                                  
        showVariableHead( variableNames, variableNumber);
	}
	else if ( whatInfo == infoShock) {
        int variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "Mx";
        variableNames[1] = "My";
        variableNames[2] = "TyTx";
        variableNames[3] = "RyRx";
        variableNames[4] = "PyPx";
        variableNames[5] = "P0yP0x";

		showVariableHead( variableNames, variableNumber);
	}
	else if (whatInfo == infoTubeShockLimits){
		int variableNumber = 3;
		char * variableNames[variableNumber];
		variableNames[0] = "M1";
		variableNames[1] = "M2upper";
		variableNames[2] = "M2lower";

		showVariableHead( variableNames, variableNumber);
	}
	else if (whatInfo == infoTubeProfile){
        int variableNumber = 7;
        char * variableNames[variableNumber];
		
        variableNames[0] = "fld";
        variableNames[1] = "Mx";
        variableNames[2] = "My";
        variableNames[3] = "Py/Px";
        variableNames[4] = "Px/P*";
        variableNames[5] = "Py/P*";
        variableNames[6] = "P1/P(i)";
		cout <<  "Fanno flow profile" << "\t"  ;
		cout << "M1 = " << M1 <<
				 "\t" << endl <<  "FLD = " <<  fld 
					<<endl << endl ;

        showVariableHead( variableNames, variableNumber);
    }
	else if (whatInfo == infoTubeShock){
        int variableNumber = 6;
        char * variableNames[variableNumber];
                                                                               
        variableNames[0] = "M1";
        variableNames[1] = "M2";
        variableNames[2] = "fldUp";
        variableNames[3] = "fldDown";
        variableNames[4] = "Mx";
        variableNames[5] = "My";
        cout <<  "Fanno flow profile" << "\t"  ;
        cout << "M1 = " << M1 <<
                 "\t" << endl <<  "FLD = " <<  fld
                    <<endl << endl ;
                                                                               
        showVariableHead( variableNames, variableNumber);
    };

};

void 	fanno::showResults(int whatInfo) {
	int numberVariables;
	if (whatInfo == infoStandard){
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M ;
        variableToBePrinted[1] = fld;
        variableToBePrinted[2] = PRstar;
        variableToBePrinted[3] = PzeroRstar;
        variableToBePrinted[4] = RRstar ;
        variableToBePrinted[5] = URstar;
        variableToBePrinted[6] = TRstar;

        showVariable(variableToBePrinted, numberVariables);

	}
	else if (whatInfo == infoTube) {
		numberVariables = 6;
		double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M1;
        variableToBePrinted[1] = M2;
        variableToBePrinted[2] = fld;
        variableToBePrinted[3] = fld1;
        variableToBePrinted[4] = fld2;
        variableToBePrinted[5] = p2p1;

        showVariable(variableToBePrinted, numberVariables);

	}
	else if ( whatInfo == infoShock) {
		numberVariables = 6;
		double variableToBePrinted[numberVariables];
		variableToBePrinted[0] = Mx;
		variableToBePrinted[1] = My;
		variableToBePrinted[2] = TyTx;
		variableToBePrinted[3] = RyRx;
		variableToBePrinted[4] = PyPx;
		variableToBePrinted[5] = P0yP0x;
		
		showVariable(variableToBePrinted, numberVariables);
	}
	else if  ( whatInfo == infoTubeShock) {
        numberVariables = 6;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M1;
        variableToBePrinted[1] = M2;
        variableToBePrinted[2] = fldUp;
        variableToBePrinted[3] = fldDown;
        variableToBePrinted[4] = Mx;
        variableToBePrinted[5] = My;
	
		showVariable(variableToBePrinted, numberVariables);
    }
    else if  ( whatInfo == infoTubeProfile) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
		for (int j=0; j < numberNodes  ; j++){
			for (int i=0; i < numberVariables ;i++){
				variableToBePrinted[i] = Mfld[i][j];
			}

        	showVariable(variableToBePrinted, numberVariables);
		}
    }

};

double  fanno::calFLD(double Mach){
	M=Mach;

	if (M < .00) {
		//flow is reversed ?
		if (fabs( M) >  1.0 ) {
		 isSubSonic=yes;
		}
		isFlowReverse = yes;
		return 1.0;
	}
	else if (M > 1.0 ) {
		//supersonic flow
		isSubSonic=no;
		isFLDknown = yes;
		isFlowReverse = no;
		return (1. - M *M ) / k /M/ M  + 
			0.5*(k+1.)/k *log 
					( 0.5*(k+1.) * M * M/(1.+ 0.5* (k-1.)*M*M));
	}
	else if (M < EPS ) {
		// for case almost incompressible flow
		isSubSonic=yes;
		isFLDknown = no;
		isFlowReverse = no;
		// to put the approximation formula
		// not done yet
		return M;
	}	
	else {
	// the subsonic flow and ``normal'' range
		isSubSonic=no;
		isFLDknown = no ;
		isFlowReverse = no;
        return (1. - M *M ) / k /M/ M +
            0.5*(k+1.)/k*log ( 0.5*(k+1.) * M * M/(1.+ 0.5* (k-1.)*M*M));
	}
};

void  fanno::calFLD(){

	if (M < .00) {
		//flow is reversed ?
        if (fabs( M) >  1.0 ) {
         isSubSonic=yes;
        }
		else {
			isSubSonic=no;
		}
		isFlowReverse = yes;
		return;
	}
	else if (M > 1.0 ) {
		//supersonic flow
        fld = (1. - M *M ) / k /M/ M +
            0.5*(k+1.)/k*log ( 0.5*(k+1.) * M * M/(1.+ 0.5* (k-1.)*M*M));
		isSubSonic=no;
		isFLDknown = yes;
		isFlowReverse = no;
		return;
	}
	else if (M < 0.00001 ) {
		// for case almost incompressible flow
		fld = M ;
		isSubSonic=yes;
		isFLDknown = no;
		isFlowReverse = no;
		return;
	}	
	else {
	// the subsonic flow and ``normal'' range
        fld = (1. - M *M ) / k /M/ M +
            0.5*(k+1.)/k*log ( 0.5*(k+1.) * M * M/(1.+ 0.5* (k-1.)*M*M));
		isSubSonic=no;
		isFLDknown = no ;
		isFlowReverse = no;
	}
};

void  fanno::calArea(){
	Area =  pow((( 1 + (( k - 1) / 2 ) * M*M)/((k+1)/2)), 
			((k+1) / 2/(k-1) ))
				/ M;
};
	
double  fanno::calArea(double MM){
    return  pow((( 1 + (( k - 1) / 2 ) * MM*MM)/((k+1)/2)),
            ((k+1) / 2/(k-1) ))
                / MM;
};

void  fanno::calTemperatureRatio(){
	TRstar = 0.5* (k+1. )/ 
		(1. + (0.5*(k -1. ) *M*M) ) ;
};

double  fanno::calTemperatureRatio(double MM){
	return 0.5* (k+1. )/ 
		(1. + (0.5*(k -1. ) *MM*MM) ) ;
};

void  fanno::calTotalTemperatureRatio(){
	TzeroRstar = (k+1. )/ 
		(1. + (0.5*(k -1. ) *M*M) )*0.5 ;
};

double  fanno::calTotalTemperatureRatio(double MM){
	return  (k+1. )/ 
		(1. + (0.5*(k -1. ) *MM*MM) )*0.5 ;
};

void  fanno::calPressureRatio(){
	// calculate the pressure ratio
	PRstar =   sqrt(0.5*(k + 1.) /( 1.+ 0.5*(k-1.)*M*M ))/M ;
};

double  fanno::calPressureRatio(double MM){
	// calculate the pressure ratio
	return   sqrt(0.5*(k + 1.) /( 1.+ 0.5*(k-1.)*MM*MM ))/MM ;
};

void  fanno::calVelocityRatio(){
    // calculate the velocity ratio
	URstar = M * sqrt ( 0.5 * (k+1. )/ 
		(1. + (0.5*(k -1. ) *M*M) ) ) ;
};

double  fanno::calVelocityRatio(double MM){
    // calculate the velocity ratio
	return MM * sqrt ( 0.5 * (k+1. )/ 
		(1. + (0.5*(k -1. ) *MM*MM) ) ) ;
};

void  fanno::calDensityRatio(){
    // calculate the density ratio
    RRstar =   sqrt(2.*(1. + 0.5* (k -1.0) *M*M) /( 1.+ k))/M ;
};

double  fanno::calDensityRatio(double MM){
    // calculate the density ratio
    return  sqrt(2.*(1. + 0.5* (k -1.0) *MM*MM) /( 1.+ k))/MM ;
};

void  fanno::calTotalPressureRatio(){

	if (isFlowReverse == no ) {
		PzeroRstar =  sqrt(
			pow((2.*(1. + 0.5* (k -1.0) *M*M) /( 1.+ k)),
				(( k+1.0)/(k-1.0))  )
		)/M;
		return ;
	}
	else {
		return ;
	}
};
 
double  fanno::calTotalPressureRatio(double MM){
	return  sqrt(
			pow((2.*(1. + 0.5* (k -1.0) *MM*MM) /( 1.+ k)),
				(( k+1.0)/(k-1.0))  )
		)/MM;  
};
/********************************************************
	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/
using namespace std;
int precision;
int numberNodes;

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip> 
#  include "gasDynamics.h" 

int main( void )
{
	double M, M1 , FLD, p2p1, Mx, Pbar, Astar, PAR, P0yP0x;
	double k = 1.4;
	// fan is the name of the actual object with k = 1.4.
	fanno fan(k) ; 
	shock s(k);
	stagnation G(k);
	isothermal iso(k);
	//compressibleFlow * fanC = &fan;
	//compressibleFlow * sC = &s;
	//compressibleFlow * PG = &G;
	//compressibleFlow * Piso = &iso;
	int whatInfo;
	int isTex = yes;
	int isRange = no;
	int variableName; 
	int shockLocation;
	double variableValue[50];
/* the kind of information needed */
 	whatInfo = infoStagnation ; // total properties (stagnation)
 	//whatInfo = infoStandard ; //standard tube flow and shock (point) 
 	//whatInfo = infoTube ; //print two side of tube flow 
 	//whatInfo = infoShock ; // two side of shock
 	//whatInfo = infoTubeProfile ; // the tube profile
 	//whatInfo = infoTubeShock ; // two sides and shock area
/* the kind of information supplied */
	//variableName =  M1fldV; // M1 & fld given
	//variableName =  p2p1FLDV; // p2/p1, M1 & fld given
	//variableName =  MxV; // what is (are) variable(s) given
	//variableName =  fldV; // what is (are) variable(s) given
	//variableName =  M1V; // what is (are) variable(s) given
	//variableName =  machV; // Mach number
	variableName =  AstarV; // the star area ratio
	//variableName =  M1ShockV; //    
	//variableName =  M1fldP2P1V; // what is (are) variable(s) given
	//variableName =  MFLDshockLocationV; // what is (are) variable(s) given
	//variableName =  PbarV; // what is (are) variable(s) given
	//variableName =  PARV; // what is (are) variable(s) given
	//variableName =  P0yP0xV; // what is (are) variable(s) given

	M1 = 2.0;
	FLD = 40.0 ;
	p2p1 = 0.5 ; 
	//M =  3.0 ;
	M = .20 ;
	Mx = 3.0 ;
	Pbar = 0.6;
	Astar = 1.272112;
	PAR = 1.5 ;
	P0yP0x = 0.5525;
		
	numberNodes = 40;
	shockLocation = 50; // number of percents
	//resutls precision how many digits after the point.
	precision = 4 ;

	//to build a function for setting the variables  

	variableValue[PbarN]=Pbar;
	variableValue[m1N]=M1;
	variableValue[fldN]=FLD;
	variableValue[p2p1N]=p2p1;
	variableValue[mN]=M;
	variableValue[MxN]=Mx;
	variableValue[AstarN]=Astar;
	variableValue[PARN]=PAR;
	variableValue[P0yP0xN]=P0yP0x;
	variableValue[shockLocationN]= ( (double) shockLocation) /100.0
					*numberNodes ;
	if  (variableValue[shockLocationN] < 1.) {
		variableValue[shockLocationN] = 1.;
	}

	//the following line is an example for using the class as a pointer
	//fanC->makeTable(whatInfo,isRange, isTex, machV, M);
	//fan.makeTable(whatInfo, isRange, isTex,  variableName, variableValue);
	//s.makeTable(whatInfo, isRange, isTex,  variableName, variableValue);
	G.makeTable(whatInfo, isRange, isTex,  variableName, variableValue);
	//iso.makeTable(whatInfo, isRange, isTex,  variableName, variableValue);

	return 0;
}



/********************************************************
	FILE:isthermal.cc

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip>
#  include "stagnation.h"
#  include "externalFun.h"
#  include "isothermal.h" 
#  include "gasDynamics.h" 
using namespace std;

/*isothermal functions */
//******************************************************************
void 	isothermal::makeTexTableHead(int whatInfo) {

    int variableNumber;

	if (whatInfo == infoStandard) {
		variableNumber = 6;
		char * variableNames[variableNumber];
		variableNames[0] = "M";
		variableNames[1] = "4fL \\over D";
		variableNames[2] = "P \\over P^{*}";
		variableNames[3] = "P_0 \\over {P_0}^{*}";
		variableNames[4] = "\\rho \\over \\rho^{*}";
		variableNames[5] = "T_0 \\over {T_0}^{*}";

		showVariableTeXHead(variableNames,variableNumber);

	}
	else if (whatInfo == infoTube) { 
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "M_1";
        variableNames[1] = "M_2";
        variableNames[2] = "4fL \\over D";
        variableNames[3] = "\\left.{ 4fL \\over D }\\right|_{1}";
        variableNames[4] = "\\left.{ 4fL \\over D }\\right|_{2}";
        variableNames[5] = "P_2 \\over P_1";

        showVariableTeXHead(variableNames,variableNumber);
	}
	else if  (whatInfo == infoTubeProfile) {
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "M_1";
        variableNames[1] = "M_2";
        variableNames[2] = "4fL \\over D";
        variableNames[3] = "\\left.{ 4fL \\over D }\\right|_{1}";
        variableNames[4] = "\\left.{ 4fL \\over D }\\right|_{2}";
        variableNames[5] = "P_2 \\over P_1";
                                                                               
                                                                               
        showVariableTeXHead(variableNames,variableNumber);
    }
    else if  (whatInfo == infoTubeShock) {
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "M_1";
        variableNames[1] = "M_2";
        variableNames[2] = "\\left.{ 4fL \\over D }\\right|_{up}";
        variableNames[3] = "\\left.{ 4fL \\over D }\\right|_{down}";
        variableNames[4] = "M_x";
        variableNames[5] = "M_y";

        showVariableTeXHead(variableNames,variableNumber);
    }
    else if (whatInfo == infoTube) {
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "M_1";
        variableNames[1] = "M_2";
        variableNames[2] = "4fL \\over D";
        variableNames[3] = "\\left.{ 4fL \\over D }\\right|_{1}";
        variableNames[4] = "\\left.{ 4fL \\over D }\\right|_{2}";
        variableNames[5] = "P_2 \\over P_1";
        showVariableTeXHead(variableNames,variableNumber);
    }

};

void    isothermal::showHead(int whatInfo){
    cout << endl << endl <<  "Isothermal flow" << "\t\t"  ;
    cout << "k = " << k << endl << endl ;
                                                                               
                                                                               
    if (whatInfo == infoStandard) {
        int variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = "M  ";
        variableNames[1] = "fld";
        variableNames[2] = "P/P*";
        variableNames[3] = "P0/P0*";
        variableNames[4] = "R/R*";
        variableNames[5] = "U/U*";
        variableNames[6] = "T/T*";

        showVariableHead( variableNames, variableNumber);
    }
    else if (whatInfo == infoTube) {
        int variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "M1";
        variableNames[1] = "M2";
        variableNames[2] = "fLD";
        variableNames[3] = "fLD1";
        variableNames[4] = "fLD2";
        variableNames[5] = "P2/P1";

        showVariableHead( variableNames, variableNumber);
    }
};

void    isothermal::showResults(int whatInfo) {
    int numberVariables;
    if (whatInfo == infoStandard){
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M ;
        variableToBePrinted[1] = fld;
        variableToBePrinted[2] = PRstar;
        variableToBePrinted[3] = PzeroRstar;
        variableToBePrinted[4] = RRstar ;
        variableToBePrinted[5] = URstar;
        variableToBePrinted[6] = TRstar;
                                                                               
        showVariable(variableToBePrinted, numberVariables);
                                                                             
    }
    else if (whatInfo == infoTube) {
        numberVariables = 6;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M1;
        variableToBePrinted[1] = M2;
        variableToBePrinted[2] = fld;
        variableToBePrinted[3] = fld1;
        variableToBePrinted[4] = fld2;
        variableToBePrinted[5] = p2p1;
                                                                               
        showVariable(variableToBePrinted, numberVariables);
                                                                               
    }
};

void 	isothermal::makeTexTableLine(int whatInfo) {

    int numberVariables;

	if (whatInfo == infoStandard) {
        numberVariables = 6;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M ;
        variableToBePrinted[1] = fld;
        variableToBePrinted[2] = PRstar;
        variableToBePrinted[3] = PzeroRstar;
        variableToBePrinted[4] = RRstar ;
        variableToBePrinted[5] = TzeroRstar;
		showVariableTeXLine (variableToBePrinted, numberVariables);
	}
	else if (whatInfo == infoTube) {
		numberVariables = 6;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M1 ;
        variableToBePrinted[1] = M2;
        variableToBePrinted[2] = fld;
        variableToBePrinted[3] = fld1;
        variableToBePrinted[4] = fld2 ;
        variableToBePrinted[5] = p2p1;

		showVariableTeXLine (variableToBePrinted, numberVariables);
	}
};


void  isothermal::calTotalTemperatureRatio(){
    TzeroRstar = (2.*k / (3.*k - 1.) ) *
        (1. + (0.5 * (k -1. ) )* M*M ) ;
};

void  isothermal::calPressureRatio(){
    // calculate the pressure and density ratio
    PRstar =  1. / sqrt(k)/M ;
    RRstar = PRstar;
};

double  isothermal::calPressureRatio(double MM){
    // calculate the pressure and density ratio
    return  (1. / sqrt(k)/MM) ;
};

void  isothermal::calTotalPressureRatio(){
    if (isFlowReverse == no ) {
        PzeroRstar =  (pow((2.*k/(3.*k -1.)),(k/(k -1.)))/sqrt(k))*
                pow((1. + (0.5*(k-1.))*M*M),(k/(k-1.)))/M ;
    }
    else {
        return ;
    }
};

void  isothermal::calArea(){
    Area =  pow((( 1. + (0.5*( k - 1.)  ) * M*M)/(0.5*(k+1.))),
            ((k+1.) / 2./(k-1.) ))
                / M;
};

void  isothermal::calFLD(){
    if (M < .00) {
        //flow is reversed ?
        fld = -1.0 ;
        isFLDknown= no;
        isSubSonic=no;
        isFlowReverse = yes;
        return;
    }
    else if (M > M1maxSub  ) {
        //supersonic flow
        fld = (1 -k *M *M ) / k /M/ M + log (k * M * M);
        isSubSonic=no;
        isFLDknown = no;
        isFlowReverse = no;
        return;
    }
    else if (M < 0.0000001 ) {
        // for case almost incompressible flow
		//unfinished
        fld = M ;
        isSubSonic=yes;
        isFLDknown = yes;
        isFlowReverse = no;
        return;
    }
    else {
    // the subsonic flow and ``normal'' range
        fld = (1. -k *M *M ) / k /M/ M + log (k * M * M) ;
        isSubSonic=yes;
        isFLDknown = yes ;
        isFlowReverse = no;
    }
};

double  isothermal::calFLD(double MM){
    if (MM < .00) {
        return -1.0;
    }
    else if (MM > M1maxSub  ) {
        //supersonic flow
        return -1.;
    }
    else if (MM < 0.0000001 ) {
        // for case almost incompressible flow
		//unfinished
        return MM ;
    }
    else {
    // the subsonic flow and ``normal'' range
        return (1. -k *MM *MM ) / k /MM/ MM + log (k * MM * MM) ;
    }
};

/********************************************************
	FILE:isothermalNozzle.cc

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, isothermalNozzle and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip> 
#  include "isothermalNozzle.h" 
#  include "externalFun.h" 

using namespace std;

// the isothermalNozzle class functions definitions
//**********************************************//

void  isothermalNozzle::calMach(int variableName, double * variableValue){
	if (variableName == machV) {
		M = variableValue[mN];
	}
	else if (variableName == PbarV) {
		Pbar = variableValue[PbarN];
		M = root  ( calPbarEstmt, this, .00001,  M1maxSub, Pbar) ; 
	}
	else if (variableName == AstarV) {
        Area = variableValue[AstarN];
		if ( variableValue[mN] <1. ) {
        	M = root  ( calAstarEstmt, this, .00001,  M1maxSub, Area) ;
		}
		else {
        	M = root  ( calAstarEstmt, this, M1maxSub, 20., Area) ;
		}
    }
	else if (variableName == PARV) {
		PAR = variableValue[PARN];
		M = root  ( calPAREstmt, this, .00001,  M1maxSub, PAR) ;
	}
};

void  isothermalNozzle::calPressureRatio(){
	Pbar =  pow (1. / ( 1. + (( k - 1.) / 2. ) * M*M),(k / (k - 1.) ) ) ;
};

double   isothermalNozzle::calPressureRatio(double MM){
    return  pow (1. / ( 1. + (( k - 1.) / 2. ) * MM*MM),(k / (k - 1.) ) ) ;
};

void  isothermalNozzle::calTemperatureRatio(){
	Tbar = 1. / ( 1. + (( k - 1.) / 2. ) * M*M);
};
void  isothermalNozzle::calDensityRatio(){ 
	 Rbar = pow ( Pbar, (1./k)) ;
};
void  isothermalNozzle::calAreaRatio(){
	    Area =  pow((( 1.0 + (( k - 1.0) / 2. ) * M*M)/((k+1.)/2.)),
            ((k+1.) / 2./(k-1.) ))
                / M;
};
void  isothermalNozzle::calPARRatio(){
	PAR =  Area * Pbar;
};

void  isothermalNozzle::makeTexTableHead(int whatInfo){ 
                                                                               
    int variableNumber;
                                                                               
    if (whatInfo == infoStandard || whatInfo == infoStagnation ) {
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "M";
        variableNames[1] = "T \\over T_0";
        variableNames[2] = "\\rho \\over \\rho_0";
        variableNames[3] = "A \\over A^{\\star}";
        variableNames[4] = "P \\over P_0";
        variableNames[5] = "A\\times P \\over A^{*} \\times P_0";
                                                                               
        showVariableTeXHead(variableNames,variableNumber);
    }
}

void  isothermalNozzle::makeTexTableLine(int whatInfo){ 
    int numberVariables;
                                                                               
    if (whatInfo == infoStandard  || whatInfo == infoStagnation ) {
        numberVariables = 6;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M ;
        variableToBePrinted[1] = Tbar;
        variableToBePrinted[2] = Rbar;
        variableToBePrinted[3] = Area;
        variableToBePrinted[4] = Pbar ;
        variableToBePrinted[5] = PAR;
                                                                               
        showVariableTeXLine (variableToBePrinted, numberVariables);
    }
}

int   isothermalNozzle::calAll(int whatInfo, int variableName,
			 double * variableValue ){ 
        calMach(variableName, variableValue);
        calAreaRatio();
        calPressureRatio();
        calTemperatureRatio();
        calTotalPressureRatio();
        calDensityRatio ();
        calPARRatio();
        return yes ;
}

/********************************************************
	FILE:pipeFlow.cc

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip>
#  include "stagnation.h"
#  include "externalFun.h"
#  include "pipeFlow.h" 
#  include "gasDynamics.h" 
using namespace std;

// pipeFlow functions 
//****************************************************************
void  pipeFlow::calMach(int variableName, double * variableValue){
    if (variableName == machV) {
        M = variableValue[mN];
		isFlowReverse = no;
    }
    else if (variableName == fldV) {
        fld = variableValue[fldN] ;
        M = calMForFLD(variableValue[fldN],variableValue[mN]) ;
        if ( M > 0 && M <= M1maxSub){
            isFLDknown= yes;
            isSubSonic=yes;
            isFlowReverse = no;
        }
    }
    else if (variableName == PbarV) {
        Pbar = variableValue[PbarN];
        M = root  ( calPbarEstmt, this, .00001,  M1maxSub, Pbar) ;
    }
	else if (variableName == M1ShockV) {
		M1 = variableValue[m1N];
		fld = variableValue[fldN] ;
	}
};

void  pipeFlow::calM1(int variableName, double * variableValue){
	if (variableName == M1fldV){
		M1 =   variableValue[m1N];
		fld = variableValue[fldN];
	}
	else if (variableName == p2p1FLDV){
        fld = variableValue[fldN];
		p2p1 = variableValue[p2p1N];
 		if (calM1forFLDp2p1(variableValue[fldN],variableValue[p2p1N]) == yes){
		}
	}
	else if (variableName ==  M1ShockV) {
        M1 = variableValue[m1N];
        fld = variableValue[fldN] ;
	}
};

/********************************************************
	FILE:shock.cc

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip>
#  include "stagnation.h"
#  include "externalFun.h"
#  include "shock.h" 
#  include "gasDynamics.h" 
using namespace std;

/// shock class definition 
//******************************************************************

void  shock::calMach(int variableName, double * variableValue){
    if (variableName == MxV) {
        Mx = variableValue[MxN];
    }
};

void  shock::calMx(int variableName, double * variableValue){
    if (variableName == P0yP0xV) {
        P0yP0x = variableValue[P0yP0xN];
        M = root  ( calP0yP0xEstmt, this, M1maxSub, 100.0, P0yP0x) ;
		return;
    }
	else if (variableName == MxV){ 
		double tmpMx;
		Mx = variableValue[MxN] ;
		calMy();
		if ( variableValue[MxN] < 1.0) {
			tmpMx = Mx ;
			Mx = My;
			My = tmpMx;
		}
	}
}

void  shock::calMy(){
    My = sqrt((Mx*Mx + 2.0/(k -1.0)) /
        ((2.0*k / (k-1.0) ) *Mx*Mx -1.0)) ;
};

                                                                                  
double  shock::calMy(double m){
    return  sqrt((m*m + 2.0/(k -1.0)) / ((2.0*k / (k-1.0) ) *m*m -1.)) ;
};
                                                                                  

void  shock::calPressureRatio(void){
    PyPx =  (1. + k*Mx*Mx) / (1. + k*My*My) ;
};

double   shock::calPressureRatio(double MMx, double MMy){
    return  (1. + k*MMx*MMx) / (1. + k*MMy*MMy) ;
};

void  shock:: calDensityRatio (){
    RyRx = PyPx/TyTx;
};
                                                                                
void  shock::calTotalPressureRatio(){
    P0yP0x =  (PyPx)*
    pow((1 + ((k-1)/2)*My*My),(k/(k-1))) /
    pow((1 + ((k-1)/2)*Mx*Mx),(k/(k-1))) ;
};

void  shock::calTemperatureRatio(){
    TyTx = PyPx *PyPx * My*My/Mx/Mx;
};
                                                                                
void  shock::calArea(){
    Area =  pow((( 1 + (( k - 1) / 2 ) * Mx*Mx)/((k+1)/2)),
            ((k+1) / 2/(k-1) ))
                / Mx;
};

void    shock::makeTexTableHead(int whatInfo) {
                                                                               
    int variableNumber;
                                                                               
    if (whatInfo == infoStandard || whatInfo == infoShock) {
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "M_x";
        variableNames[1] = "M_y";
        variableNames[2] = "T_y \\over T_x";
        variableNames[3] = "\\rho_y \\over \\rho_x";
        variableNames[4] = "P_y \\over P_x";
        variableNames[5] = "{P_0}_y \\over {P_0}_x";
                                                                               
        showVariableTeXHead(variableNames,variableNumber);
    }
}

void    shock::makeTexTableLine(int whatInfo) {
    int numberVariables;

    if (whatInfo == infoStandard || whatInfo == infoShock) {
        numberVariables = 6;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx ;
        variableToBePrinted[1] = My;
        variableToBePrinted[2] = TyTx;
        variableToBePrinted[3] = RyRx;
        variableToBePrinted[4] = PyPx ;
        variableToBePrinted[5] = P0yP0x;

        showVariableTeXLine (variableToBePrinted, numberVariables);
    }
}

int   shock::calAll(int whatInfo, int variableName,
				 double * variableValue ){
    calMx(variableName, variableValue);
	calAreaRatio();
	calPressureRatio();
	calTemperatureRatio();
	calTotalPressureRatio();
	calDensityRatio ();
	return yes;
}

/********************************************************
	FILE:stagnation.cc

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip> 
#  include "stagnation.h" 
#  include "externalFun.h" 

using namespace std;

// the stagnation class functions definitions
//**********************************************//

void  stagnation::calMach(int variableName, double * variableValue){
	if (variableName == machV) {
		M = variableValue[mN];
	}
	else if (variableName == PbarV) {
		Pbar = variableValue[PbarN];
		M = root  ( calPbarEstmt, this, .00001,  M1maxSub, Pbar) ; 
	}
	else if (variableName == AstarV) {
        Area = variableValue[AstarN];
		if ( variableValue[mN] <1. ) {
        	M = root  ( calAstarEstmt, this, .00001,  M1maxSub, Area) ;
		}
		else {
        	M = root  ( calAstarEstmt, this, M1maxSub, 20., Area) ;
		}
    }
	else if (variableName == PARV) {
		PAR = variableValue[PARN];
		M = root  ( calPAREstmt, this, .00001,  M1maxSub, PAR) ;
	}
};

void  stagnation::calPressureRatio(){
	Pbar =  pow (1. / ( 1. + (( k - 1.) / 2. ) * M*M),(k / (k - 1.) ) ) ;
};

double   stagnation::calPressureRatio(double MM){
    return  pow (1. / ( 1. + (( k - 1.) / 2. ) * MM*MM),(k / (k - 1.) ) ) ;
};

void  stagnation::calTemperatureRatio(){
	Tbar = 1. / ( 1. + (( k - 1.) / 2. ) * M*M);
};
void  stagnation::calDensityRatio(){ 
	 Rbar = pow ( Pbar, (1./k)) ;
};
void  stagnation::calAreaRatio(){
	    Area =  pow((( 1.0 + (( k - 1.0) / 2. ) * M*M)/((k+1.)/2.)),
            ((k+1.) / 2./(k-1.) ))
                / M;
};
void  stagnation::calPARRatio(){
	PAR =  Area * Pbar;
};

void  stagnation::makeTexTableHead(int whatInfo){ 
                                                                               
    int variableNumber;
                                                                               
    if (whatInfo == infoStandard || whatInfo == infoStagnation ) {
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "M";
        variableNames[1] = "T \\over T_0";
        variableNames[2] = "\\rho \\over \\rho_0";
        variableNames[3] = "A \\over A^{\\star}";
        variableNames[4] = "P \\over P_0";
        variableNames[5] = "A\\times P \\over A^{*} \\times P_0";
                                                                               
        showVariableTeXHead(variableNames,variableNumber);
    }
}

void  stagnation::makeTexTableLine(int whatInfo){ 
    int numberVariables;
                                                                               
    if (whatInfo == infoStandard  || whatInfo == infoStagnation ) {
        numberVariables = 6;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M ;
        variableToBePrinted[1] = Tbar;
        variableToBePrinted[2] = Rbar;
        variableToBePrinted[3] = Area;
        variableToBePrinted[4] = Pbar ;
        variableToBePrinted[5] = PAR;
                                                                               
        showVariableTeXLine (variableToBePrinted, numberVariables);
    }
}

int   stagnation::calAll(int whatInfo, int variableName,
			 double * variableValue ){ 
        calMach(variableName, variableValue);
        calAreaRatio();
        calPressureRatio();
        calTemperatureRatio();
        calTotalPressureRatio();
        calDensityRatio ();
        calPARRatio();
        return yes ;
}

