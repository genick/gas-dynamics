/* keenan.c
   Steam table formulas.

   This C program computes thermodynamic properties of water in the liquid
   or vapor phase.  Independent variables are the temperature and either the
   specific volume or the pressure.  Outputs are the pressure or specific
   volume and the specific internal energy, specific enthalpy, specific
   entropy, and saturation pressure.  The method used is from J. Keenan
   et al., _Steam Tables_, 1969.

   Reference:
   Keenan, Joseph H., Frederick G. Keyes, Philip G. Hill, Joan G. Moore,
   Steam Tables. Thermodynamic Properties of Water Including Vapor,
   Liquid, and Solid Phases_, Wiley, 1969.
   Appendix, pp 134 ff.

   Outputs from this program have been spot checked for about 100 p,T pairs
   against the tables provided.  Be aware, however, that this formulation
   is not identical to ASME.

   Program by Stephen L. Moshier
   January, 2001  */


/* Compilation:
   This source file is self contained, but requires standard math library
   functions.  Compile with a command like  'cc keenan.c -lm'.  */

/* Define 1 for English input and output units
   (Fahrenheit, Btu, pounds, inches).  */
#define ENGLISH 1

/* Define 1 for debug printouts.  */
#define DEBUG 0

#include <stdio.h>
extern double log (double);
extern double exp (double);
extern double floor (double);

/*
 * Auxiliary function
 *
 *           5
 *           -        i
 *  psi0  =  >  C  / t    +   C  ln T   +   C  (ln T) / t
 *           -   i             6             7
 *          i=0
 */

/* All coefficients here are from Keenan et al.  */
static double C[8] = {
  1857.065, 3229.12, -419.465, 36.6649, -20.5516, 4.85233, 46., -1011.249
};

static double
psi0 (double T)
{
  double z, s, y;

  z = T / 1000.0;		/* 1/tau */
  y = ((((C[5] * z + C[4]) * z + C[3]) * z + C[2]) * z + C[1]) * z + C[0];
  s = log (T);
  y = y + C[6] * s + C[7] * s * z;
  return y;
}


/*
 * Auxiliary function
 *
 *                6             [  7                         9         ]
 *                -             [  -               i     -Er -     i-8 ]
 *  Q = (t - t )  >  (t - t  )  [  >  A   (r - r  )  +  e    > A  r    ]
 *            c   -        aj   [  -   ij       aj           -  ij     ]
 *               j=0            [ i=0                       i=8        ]
 *
 *  r is the density, rho, in grams per cubic centimeter
 *  t is 1000 / T
 *  E = 4.8
 *
 *  t  = 1000/Tcrit = 1.544912
 *   c
 *
 *  t   = 2.5 if j > 0, else t  if j = 0.
 *   aj                       c
 *
 *  r   = 1.0 if j > 0, else 0.634 if j = 0.
 *   aj
 */

static double A[10][7] = {

  {29.492937, -5.1985860, 6.8335354, -0.1564104, -6.3972405, -3.9661401,
   -0.69048554},
  {-132.13917, 7.7779182, -26.149751, -0.72546108, 26.409282, 15.453061,
   2.7407416},
  {274.64632, -33.301902, 65.326396, -9.2734289, -47.740374, -29.142470,
   -5.1028070},
  {-360.93828, -16.254622, -26.181978, 4.3125840, 56.323130, 29.568796,
   3.9636085},
  {342.18431, -177.31074, 0.0, 0.0, 0.0, 0.0, 0.0},
  {-244.50042, 127.48742, 0.0, 0.0, 0.0, 0.0, 0.0},
  {155.18535, 137.46153, 0.0, 0.0, 0.0, 0.0, 0.0},
  {5.9728487, 155.97836, 0.0, 0.0, 0.0, 0.0, 0.0},

  {-410.30848, 337.31180, -137.46618, 6.7874983, 136.87317, 79.847970,
   13.041253},
  {-416.05860, -209.88866, -733.96848, 10.401717, 645.81880, 399.17570,
   71.531353}
};

#define tauc 1.544912		/* 1000/Tcrit */
#define E 4.8

static double
Q (double rho, double tau)
{
  double e, p, q, s, y;
  double u[7];
  int j;

  e = exp (-E * rho);
  for (j = 0; j < 7; j++)
    {
      if (j > 0)
	p = rho - 1.0;
      else
	p = rho - 0.634;
      s = ((((((A[7][j] * p + A[6][j]) * p + A[5][j]) * p + A[4][j]) * p
	     + A[3][j]) * p + A[2][j]) * p + A[1][j]) * p + A[0][j];
      u[j] = s + (A[9][j] * rho + A[8][j]) * e;
    }

  p = tau - 2.5;
  q = tau - tauc;
  y =
    ((((u[6] * p + u[5]) * p + u[4]) * p + u[3]) * p + u[2]) * p + u[1] +
    u[0] / q;
  y = q * y;
  return y;
}


/*
 * Helmholtz free energy
 *
 *  psi = psi0(T) + R T [ ln r + r Q(r, t)
 *
 *  r is the density, rho, in grams per cubic centimeter
 *  t is 1000 / T
 *  Temperature T is in degrees Kelvin
 */

#define R 0.46151		/* J / g K */

double
hfe (double rho, double T)
{
  double y;
  y = psi0 (T) + R * T * (log (rho) + rho * Q (rho, 1000.0 / T));
  return y;
}


/* Derivatives.  */


/* Partial derivative of Q with respect to rho.  */

double
dQ_drho (double rho, double tau)
{
  double e, p, q, s, y;
  double u[7];
  int j;

  e = exp (-E * rho);
  for (j = 0; j < 7; j++)
    {

      if (j > 0)
	p = rho - 1.0;
      else
	p = rho - 0.634;

      s =
	(((((7.
	     * A[7][j] * p + 6. * A[6][j]) * p + 5. * A[5][j]) * p +
	   4. * A[4][j]) * p + 3. * A[3][j]) * p + 2. * A[2][j]) * p +
	A[1][j];
      u[j] = s + A[9][j] * (e - E * e * rho) - E * e * A[8][j];
    }
  p = tau - 2.5;
  q = tau - tauc;
  y =
    ((((u[6] * p + u[5]) * p + u[4]) * p + u[3]) * p + u[2]) * p + u[1] +
    u[0] / q;
  y = q * y;
  return y;
}


/* Partial derivative of Q with respect to tau.  */

double
dQ_dtau (double rho, double tau)
{
  double e, p, q, s, r, dr, y;
  double u[7];
  int j;

  e = exp (-E * rho);
  for (j = 0; j < 7; j++)
    {
      if (j > 0)
	p = rho - 1.0;
      else
	p = rho - 0.634;
      s = ((((((A[7][j] * p + A[6][j]) * p + A[5][j]) * p + A[4][j]) * p
	     + A[3][j]) * p + A[2][j]) * p + A[1][j]) * p + A[0][j];
      u[j] = s + (A[9][j] * rho + A[8][j]) * e;
    }

  p = tau - 2.5;
  q = tau - tauc;
  r =
    ((((u[6] * p + u[5]) * p + u[4]) * p + u[3]) * p + u[2]) * p + u[1] +
    u[0] / q;
  dr =
    (((5. * u[6] * p + 4. * u[5]) * p + 3. * u[4]) * p + 2. * u[3]) * p +
    u[2] - u[0] / (q * q);
  y = q * dr + r;
  return y;
}


/* Derivative of psi0 with respect to tau.  */

static double
dpsi0_dtau (double T)
{
  double z, y;

  z = T / 1000.0;		/* 1/tau */
  y =
    -((((5. * C[5] * z + 4. * C[4]) * z + 3. * C[3]) * z + 2. * C[2]) * z +
      C[1]) * z * z;
  y = y - C[6] * z - C[7] * z * z * (log (T) + 1.0);
  return y;
}


/* Derivative of psi0 with respect to T.  */

static double
dpsi0_dT (double T)
{
  double z, s, y;

  z = T / 1000.0;		/* 1/tau */
  y =
    ((((5. * C[5] * z + 4. * C[4]) * z + 3. * C[3]) * z + 2. * C[2]) * z +
     C[1]) / 1000.0;
  s = log (T);
  y = y + C[6] / T + C[7] * (1.0 + s) / 1000.0;
#if 0
  printf ("dpsi0_dT = %.5f\n", y);
#endif
  return y;
}


/*
 * Pressure.
 *                                2
 * p = rho R T { 1  +  rho Q + rho  dQ/drho
 *
 */

/* Pressure conversion factor related to lbf/in^2 per bar */
#define ATM 14.5038
/* 1 bar = 1.01.. kg / cm^2 */
#define BARKG 1.01972
double
pressure (double rho, double T)
{
  double p, tau;

#if ENGLISH
  /* Fahrenheit to Kelvin */
  T = (T - 32.0) / 1.8 + 273.15;
  /* Specific volume
     1ft^3 / lb = 62.428 cm^3 / g */
  rho = rho / 62.428;
#else
  /* 1 kg/m^3 = .001 g / cm^3 */
  rho = 0.001 * rho;
#endif
  tau = 1000.0 / T;
#if DEBUG
  printf ("rho = %.5f\n", rho);
  printf ("tau = %.5f\n", 1000.0 / T);
  printf ("dQ_drho = %.5f\n", dQ_drho (rho, tau));
  p = (Q (1.0001 * rho, tau) - Q (rho, tau)) / (0.0001 * rho);
  printf ("deltaQ_deltarho = %.5f\n", p);
  printf ("Q = %.5f\n", Q (rho, tau));
  p = rho * rho * (hfe (1.0001 * rho, T) - hfe (rho, T)) / (0.0001 * rho);
  printf ("rho^2 delta psi/ delte rho = %.5f =?= p\n", ATM * p);
#endif
  p = (dQ_drho (rho, tau) * rho + Q (rho, tau)) * rho + 1.0;
  p = 10.0 * rho * R * T * p;
#if ENGLISH
  return ATM * p;
#else
  return p;
#endif
}


/* Convert joules per gram to Btu per pound.  */
#define BTULB 2.326

/* Returns joules per gram.  */
double
internal_energy (double rho, double T)
{
  double tau, u;

#if ENGLISH
  T = (T - 32.0) / 1.8 + 273.15;
  rho = rho / 62.428;
#else
  rho = 0.001 * rho;
#endif
  tau = 1000.0 / T;
#if DEBUG
  u = tau * (hfe (rho, T / 1.0001) - hfe (rho, T)) / (0.0001 * tau)
    + hfe (rho, T);
#if ENGLISH
  u /= BTULB;
#endif
  printf ("d(psi tau)/dtau = %.5f\n", u);
  printf ("dpsi0_dtau = %.5f\n", dpsi0_dtau (T));
#endif
  u = tau * dpsi0_dtau (T) + psi0 (T);
  u = u + rho * tau * R * T * dQ_dtau (rho, tau);
#if ENGLISH
  return (u / BTULB);
#else
  return u;
#endif
}


/*
 *  specific enthalpy = u + p v
 *  v = specific volume
 */

double
specific_enthalpy (double rho, double T)
{
  double h;

#if 0
  T = (T - 32.0) / 1.8 + 273.15;
  rho = rho / 62.428;
  printf ("r = %.5f, p/r %.5f\n", rho, pressure (rho, T) / rho);
#endif
#if 0
  printf ("r = %.5f, p/r %.5f\n", rho, pressure (rho, T) / rho);
#endif
#if ENGLISH
  /* The coefficient converts between lbf/in^2 x ft^3 and Btu.  */
  h = internal_energy (rho, T) + pressure (rho, T) / (5.40395 * rho);
#else
  h = internal_energy (rho, T) + 100.0 * pressure (rho, T) / rho;
#endif
  return h;
}


#define JTOCAL 4.1868
double
specific_entropy (double rho, double T)
{
  double tau, s;

#if ENGLISH
  T = (T - 32.0) / 1.8 + 273.15;
  rho = rho / 62.428;
#else
  rho = 0.001 * rho;
#endif
  tau = 1000.0 / T;
#if DEBUG
  s = -(hfe (rho, 1.0001 * T) - hfe (rho, T)) / (0.0001 * T);
#if ENGLISH
  s /= JTOCAL;
#endif
  printf ("-dpsi/dT = %.5f\n", s);
#endif
  s = log (rho) + rho * Q (rho, tau) - rho * tau * dQ_dtau (rho, tau);
  s = -R * s - dpsi0_dT (T);
#if ENGLISH
  return (s / JTOCAL);
#else
  return s;
#endif
}


/* Saturation vapor pressure.
 * Keenan et al, op cit, p 141.
 *
 *                                   7
 *                      -5           -                    k
 * ln (p / p )  = tau 10   (t  - t)  >  F  (0.65 - 0.01 t)
 *      s   c                c       -   k
 *                                  k=0
 *
 * p  = critical pressure
 *  c
 *
 * t  = critical temparature
 *  c
 *
 * t = temperature, degrees Centigrade
 *
 * tau = 1000/T, T = Kelvin temperature
 *
 */

double F[8] = {
  -741.9242, -29.72100, -11.55286, -0.8685635, 0.1094098, 0.439993,
  0.2520658, 0.05218684
};

/* Critical temperature, degrees C */
#define TC 374.136

/* Critical pressure, bars */
#define PC 220.88

/* lbf/in^2 per bar.  */
#define BAR 14.5038

double
psat (double T)
{
  double tau, t, f, p;

#if ENGLISH
  T = (T - 32.0) / 1.8 + 273.15;
  /*  rho = rho / 62.428; */
#endif
  tau = 1000.0 / T;
  t = T - 273.15;		/* Centigrade */
  f = (0.65 - 0.01 * t);
  p =
    ((((((F[7] * f + F[6]) * f + F[5]) * f + F[4]) * f + F[3]) * f + F[2]) *
     f + F[1]) * f + F[0];
  p = 1e-5 * tau * (TC - t) * p;
  p = PC * exp (p);
#if ENGLISH
  return (BAR * p);
#else
  return p;
#endif
}


/* Iterate to find density as a function of pressure and temperature.  */

double
specific_volume (double p0, double T, int vapor)
{
  double lr, r, hr, lp, p, hp, z;
  int iter;

  /* Boyle's law approximation for the vapor phase.  */
  /* FIXME: assuming English units input.  */
  if (vapor)
    {
#if ENGLISH
      r = 0.94 * p0 / T;
#else
      r = 220. * p0 / T;
#endif
      p = pressure (r, T);
      z = 0.707;
    }
  else
    {
#if ENGLISH
      r = 62.;
#else
      r = 1000.;		/* kg / m^3 */
#endif
      p = pressure (r, T);
      z = 0.99;
    }
  /* Bracket the solution. */
  iter = 0;
  if (p > p0)
    {
      hp = p;
      hr = r;
      while (p > p0)
	{
	  r = z * r;
	  z = z * z;
	  p = pressure (r, T);
	  if (++iter > 10)
	    {
	      printf ("iter > 10");
	      return -1;
	    }
	}
      lp = p;
      lr = r;
    }
  else
    {
      lp = p;
      lr = r;
      z = 1.0 / z;
      while (p < p0)
	{
	  r = z * r;
	  z = z * z;
	  p = pressure (r, T);
	  if (++iter > 10)
	    {
	      printf ("iter > 10");
	      return -1;
	    }
	}
      hp = p;
      hr = r;
    }
  /*  printf ("bracket %d, ", iter); */
  do
    {
      /* New guess.  */
      z = (p0 - lp) / (hp - lp);
      if (z < 0.1)
	z = 0.3;
      if (z > 0.9)
	z = 0.7;
      r = lr + z * (hr - lr);
      p = pressure (r, T);
      if (p > p0)
	{
	  hp = p;
	  hr = r;
	}
      else
	{
	  lp = p;
	  lr = r;
	}
      z = (hp - lp) / p0;
      if (++iter > 30)
	{
	  printf ("iter > 30");
	  break;
	}
    }
  while (z > 1.0e-6);
#if DEBUG
  printf ("iter = %d\n", iter);
#endif

  return (1.0 / r);
}



/* Test program.  */
int
main ()
{
  double T, rho, p, u, s, v, h, ps;
  int vapor, input_pressure;

  printf ("\nSteam tables according to Keenan et al, 1969.\n");
#if ENGLISH
  printf ("  p: Pressure in pounds-force per square inch.\n");
  printf ("  v: Specific volume, cubic feet per pound.\n");
  printf ("  u: Specific internal energy, Btu per pound.\n");
  printf ("  h: Specific enthalpy, Btu per pound.\n");
  printf ("  s: Specific entropy, Btu per pound degree Rankine.\n");
  printf ("  psat: Saturation pressure, pounds per square inch.\n");
#else
  printf ("  p: Pressure in bars.\n");
  printf ("  v: Specific volume, cubic meters per kilogram.\n");
  printf ("  u: Specific internal energy, kilojoules per kilogram.\n");
  printf ("  h: Specific enthalpy, kilojoules per kilogram.\n");
  printf ("  s: Specific entropy, kilojoules per kilogram degree Kelvin.\n");
  printf ("  psat: Saturation pressure, bars.\n");
#endif
  printf ("\nEnter 1 for vapor, 0 for liquid phase ? \n");
  scanf ("%d", &vapor);
  printf ("\nEnter 1 to input the pressure, 0 to input specific volume ? \n");
  scanf ("%d", &input_pressure);
loop:
#if ENGLISH
  printf ("Temperature (deg F) ? ");
#else
  printf ("Temperature (deg K) ? ");
#endif
  scanf ("%lf", &T);

  if (input_pressure == 0)
    {
      /* Input is specific volume.  */
#if ENGLISH
      printf ("Specific volume (cubic feet per pound) ? ");
#else
      printf ("Specific volume (cubic meters per kilogram) ? ");
#endif
      scanf ("%lf", &rho);
      rho = 1.0 / rho;
      p = pressure (rho, T);
      printf ("p = %.5g\n", p);
    }
  else
    {
      /* Input is pressure.  */
#if ENGLISH
      printf ("Pressure (pounds/in^2) ? ");
#else
      printf ("Pressure (bars) ? ");
#endif
      scanf ("%lf", &p);
    }

  /* Domain checks.  */
#if ENGLISH
  if (vapor && ((T < 0) || (T > 2400) || (p < 0) || (p > 15000)))
    {
      printf ("Limits are T <= 2400 deg F, p < 15000 psi.\n");
      goto loop;
    }
  if ((vapor == 0) && ((T < 0) || (T > 710) || (p < 0) || (p > 20000)))
    {
      printf ("Limits are T <= 710 deg F, p < 20000 psi.\n");
      goto loop;
    }
#else
  if (vapor && ((T < 255) || (T > 1588) || (p < 0) || (p > 1034)))
    {
      printf ("Limits are T <= 1588 deg K, p < 1034 bar.\n");
      goto loop;
    }
  if ((vapor == 0) && ((T < 255) || (T > 650) || (p < 0) || (p > 1378)))
    {
      printf ("Limits are T <= 650 deg K, p < 1378 bar.\n");
      goto loop;
    }
#endif

  v = specific_volume (p, T, vapor);
  rho = 1.0 / v;
  u = internal_energy (rho, T);
  h = specific_enthalpy (rho, T);
  s = specific_entropy (rho, T);
  printf ("v %.5g\n", v);
  printf ("u %.5g\n", u);
  printf ("h %.5g\n", h);
  printf ("s %.5g\n", s);
  ps = psat (T);
  printf ("psat = %.5f\n", ps);
  if (vapor && (p > ps))
    printf ("Pressure of vapor exceeds saturatation pressure.\n");
  if ((vapor == 0) && (p < ps))
    printf ("Pressure of liquid is less than saturation pressure.\n");
  goto loop;
}
