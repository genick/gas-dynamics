/********************************************************
	This code is copyrighted by Genick Bar_Meir 
	and has no  warrenty of any kind.
	Use this code at your own risk. 
	
	This program is open source under the same lisence as
	all the others material in potto project.
	By:
	Genick Bar-Meir

	Inputes:
	n/a

	Returens:
	prints table for Mach and 4FL/D_Max and the other ratio
	for the isothermal flow.
	It assume perfect gas with k=1.4
	Oh, well, k also can be change via the class declation.
	
	Description:
	This code includes classes to calculate the various parameter
	of the isothermal flow.  
********************************************************/

# include <cstdio>
# include <cmath>
# include <iostream>
# include <iomanip> // what replace this file in the new system?

# 	define	PI 3.14159265358979310
#   define EPS  1e-4
# 	define yes  1
# 	define no  0
#   define sonic 3 // Mach = 1 

using namespace std;

class isothermal {
        protected:
			int isFlowReverse; // check is the direction of the flow
			int isSubSonic; // is no Shock possible  
			int isFLDknown; // check if FLD was calculated/known?  no?  yes
			double fld; // the maximum length resistence 4fl/D
			double TSoverTSstarT; // temperature ratio T0/T0*T
			double PRstarT ; // pressure ratio P/P*t
			double PzeroRstarT ; // pressure ratio P0/P0*T
			double Area ; // relative area 
			double M ; // Mach nubmer 
			double RRstarT ; // relative density rho/rho*t 
			double k	; // ratio of spesific heat
        public:
			isothermal(double Mach, double spesificHeatRatio ) { 
					//initialization process
					M = Mach ;
					k = spesificHeatRatio; 
					isSubSonic = no; 
            		isFLDknown=no;
					isFlowReverse = no;  };
			~isothermal(){};
			void calFLD() ;
			void calFLD(double) ;
			void calPressureRatio() ;
			void calTotalPressureRatio() ;
			void calTotalTemperatureRatio();
			//void calDensityRatio();
			void calArea();
			void findMxForP_ratio(double);
			void findMForFLD(double);
			void changeMach ( double Mach) {Mach = M; } ;
			void showHead();
			void showResults();
			void makeTexTableHead();
			void makeTexTableLine();
			void makeTexTableTail();
			int	calTableLine(double ); // calculate the maneger
			int	calTableLine(void ); // calculate the maneger when all known
			int	 makeLines(int, int, double, double );//generate of part table
			int  manegerCalTableLine(int);//manager for calcaulating whole table
			double getPressure(){return PRstarT ; };
            double getTemperatue (){return TSoverTSstarT ; };
            double getDensity (){return RRstarT ; };
            double getArea (){return Area ; };
            double getT_P_ratio (){return PzeroRstarT  ; };
            double getFLD (){return fld  ; };

};

double root( double f (isothermal, double , double ),
             isothermal s , double a , double b, double value) {
                                                                                
    double Af =  f(s,a, value) ;
    double Bf =  f(s, b, value) ;
                                                                                
    double m = ( a + b ) / 2.0 ;
    double Mf =  f(s, m, value ) ;
                                                                                
    if ( Mf == 0.0 || (b - a ) <= EPS ) {
        return m;
    }
    else if ( Af * Mf  < 0.0 ) {
        return root (f, s, a, m, value ) ;
    }
    else {
        return root(f, s, m, b, value);
    }
}

void 	isothermal::makeTexTableHead() {

	cout << endl << endl;
	cout << "*******************************************" << endl ;
	cout << "The following stuff is the same as above/below" << endl ;
	cout << "information but is setup for the latex text processing" << endl ;
	cout << "you can just can cut and paste it in your latex file" << endl ;
	cout << "you must use longtable style file and dcolumn " << endl ;
	cout << "style file." << endl  << endl << endl;
	cout << "*******************************************" << endl ;
	cout << "\\setlongtables" << endl ;
	cout << "\\begin{longtable}" << endl ;
	cout << "{|D..{-1}|D..{2.7}|D..{2.7}|D..{3.7}|D..{2.7}|D..{2.7}|}" << endl ;
	cout << "\\caption{ ??  \\label{?:tab:?}}\\\\"  ;
	cout << "\\hline" << endl ;
	cout << "\\multicolumn{1}{|c|} " ; // the same line
	cout << "{$\\rule[-0.1in]{0.pt}{0.3 in} \\mathbf{M} $} ";// continue  line
	cout << " & " << endl;
	cout << "\\multicolumn{1}{|c|} " ; // the same line
	cout << "{$\\mathbf{\\fld} $} ";// continue  line
	cout << " & " << endl;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{T_0 \\over {T_0}^{*T}} $      } & " ;
	cout << endl ;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{\\rho \\over \\rho^{*T}} $ } & ";
	cout << endl ;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{P \\over P^{*T}} $ } &";
	cout << endl ;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{P_{0} \\over {P_{0}}^{*T}} $ }";
	cout << endl << "\\\\\\hline" << endl;
	cout << endl ;
	cout << "\\endfirsthead" << endl ;
	cout << "\\caption{ ?? (continue)} \\\\\\hline" << endl ;
    cout << "\\multicolumn{1}{|c|} " ; // the same line
    cout << "{$\\rule[-0.1in]{0.pt}{0.3 in} \\mathbf{M} $} ";// continue line 
	cout << " & " << endl;
	cout << "\\multicolumn{1}{|c|} " ; // the same line
	cout << "{$\\mathbf{\\fld} $} ";// continue line 
	cout << " & " << endl;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{T_0 \\over {T_{0}}^{*T}} $      } & " ;
	cout << endl ;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{\\rho \\over \\rho^{*T}} $ } & ";
	cout << endl ;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{P \\over P^{*T}} $ } &";
	cout << endl ;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{P_{0} \\over {P_{0}}^{*T} }$ }";
	cout << endl << "\\\\\\hline" << endl;
	cout << "\\endhead" << endl;

};

void 	isothermal::makeTexTableTail() {
	cout << "\\end{longtable}" << endl ;
};

void 	isothermal::makeTexTableLine() {

	    cout << setfill (' ') << setw (10);
		cout.setf(ios::fixed | ios::showpoint);
		//cout.setf ( cout::ios_base::right, cout::ios_base::showpoint, 
		//			 cout::ios_base::basefield );
        cout.precision(2);
        if (fabs (M) < 0.1) {cout.width(6); cout.precision(4); }
		else{ cout.width(4);} ;
        cout << M << "&\t" ;
		// if (M < 10) cout << "\t" ;
        cout.precision(5);
        cout.width(6);
        cout << fld << "&\t" ;
        cout.width(6);
        cout << TSoverTSstarT << "&\t" ;
		//if (TSoverTSstarT < 10) cout << "\t" ;
        cout.width(6);
        cout << RRstarT << "&\t" ;
		//if (RRstarT < 10) cout << "\t" ;
        cout.width(6);
        cout << PRstarT << "&\t"  ;
		//if (PRstarT < 10) cout << "&\t" ;
        cout.width(6);
        cout << PzeroRstarT  << " \\\\ \\hline" ;
        cout << endl ;

};


void 	isothermal::showResults() {

	    cout << setfill (' ') << setw (10);
		cout.setf(ios::fixed | ios::showpoint);
		//cout.setf ( cout::ios_base::right, cout::ios_base::showpoint, 
		//			 cout::ios_base::basefield );
        cout.precision(2);
        cout.width(4);
        cout << M << "\t" ;
		// if (M < 10) cout << "\t" ;
        cout.precision(5);
        cout.width(6);
        cout << fld << "\t" ;
		if (fld < 10.0 ) cout  << "\t" ; 
        cout.width(6);
        cout << TSoverTSstarT << "\t" ;
		if (TSoverTSstarT < 10) cout << "\t" ;
        cout.width(6);
        cout << RRstarT << "\t" ;
		if (RRstarT < 10) cout << "\t" ;
        cout.width(6);
        cout << PRstarT << "\t"  ;
		if (PRstarT < 10) cout << "\t" ;
        cout.width(6);
        cout << PzeroRstarT  ;
        cout << endl ;

};

void	isothermal::showHead(){

    cout << setfill (' ') << setw (10);
    cout.setf ( std::ios_base::right,
                     std::ios_base::basefield );
    cout.width(6);
	cout << "k = "  << k <<  "\n\n" ;
    cout.width(4);
	cout << "M " << "\t"  ;
    cout.width(6);
	cout << "fld " << "\t"  ;
    cout.width(6);
    cout << "TSoverTSstarT" << "\t\t" ;
    cout.width(6);
    cout << "RRstarT" << "\t\t" ;
    cout.width(6);
    cout << "PRstarT"  << "\t\t" ;
    cout.width(6);
    cout << "PzeroRstarT"   ;
    cout << endl ;

};


double calFLD_Estmt ( isothermal s, double estimatedMach, double value
) {
    s.changeMach (estimatedMach) ;
    //s.calArea();
    s.calFLD(estimatedMach) ;
    //s.calPressureRatio() ;
    //s.calTotalPressureRatio() ;
    //s.calTotalTemperatureRatio() ;
    //s.calDensityRatio() ;
    return (value - s.getFLD() ) ;
};

double calP_ratioEstmt ( isothermal s, double estimatedMach, double value
) {
    s.changeMach (estimatedMach) ;
    s.calArea();
    s.calFLD() ;
    s.calPressureRatio() ;
    s.calTotalPressureRatio() ;
    //s.calTotalTemperatureRatio() ;
    //s.calDensityRatio() ;
    return (value - s.getFLD() ) ;
};


void  isothermal::calFLD(double Mach){
	M=Mach;

	if (M < .00) {
		//flow is reversed ?
		fld = 1.0 ;
		isFLDknown= yes;
		isSubSonic=yes;
		isFlowReverse = no;
		return;
	}
	else if (M > 1.0 ) {
		//suppersonic flow
		fld = (1 -k *M *M ) / k /M/ M + log (k * M * M);
		isSubSonic=no;
		isFLDknown = yes;
		isFlowReverse = no;
		return;
	}
	else if (M < 0.0000001 ) {
		// for case alsomt incompressible flow
		fld = M ;
		isSubSonic=yes;
		isFLDknown = no;
		isFlowReverse = no;
		return;
	}	
	else {
	// the subsonic flow and ``normal'' range
		fld = (1 -k *M *M ) / k /M/ M + log (k * M * M) ;
		isSubSonic=no;
		isFLDknown = no ;
		isFlowReverse = yes;
	}
};

void  isothermal::calFLD(){

	if (M < .00) {
		//flow is reversed ?
		fld = 1.0 ;
		isFLDknown= yes;
		isSubSonic=yes;
		isFlowReverse = no;
		return;
	}
	else if (M > 1.0 ) {
		//suppersonic flow
		fld = (1. -k *M *M ) / k /M/ M + log (k * M * M);
		isSubSonic=no;
		isFLDknown = yes;
		isFlowReverse = no;
		return;
	}
	else if (M < 0.00001 ) {
		// for case alsomt incompressible flow
		fld = M ;
		isSubSonic=yes;
		isFLDknown = no;
		isFlowReverse = no;
		return;
	}	
	else {
	// the subsonic flow and ``normal'' range
		fld = (1 -k *M *M ) / k /M/ M + log (k * M * M) ;
		isSubSonic=no;
		isFLDknown = no ;
		isFlowReverse = yes;
	}
};

void  isothermal::findMxForP_ratio(double T_P_ratio ){
    M = root ( calP_ratioEstmt, *this, 1.00001,  10.0, (double) T_P_ratio) ;
};

void  isothermal::findMForFLD(double FLD ){
    M = root ( calFLD_Estmt, *this, 1.,  100000, (double) FLD) ;
    //M = root ( calFLD_Estmt, *this, .00001,  .8, (double) FLD) ;
};


void  isothermal::calArea(){
	Area =  pow((( 1 + (( k - 1) / 2 ) * M*M)/((k+1)/2)), 
			((k+1) / 2/(k-1) ))
				/ M;
};

void  isothermal::calTotalTemperatureRatio(){
	TSoverTSstarT = (2*k / (3*k - 1) ) *
		(1 + ((k -1 ) / 2)* M*M ) ;
};

void  isothermal::calPressureRatio(){
	// calculate the pressure and density ratio
	PRstarT =  1 / sqrt(k)/M ;
	RRstarT = PRstarT; 
};

void  isothermal::calTotalPressureRatio(){
		PzeroRstarT =  (pow((2*k/(3*k -1)),(k/(k -1)))/sqrt(k))*
                pow((1 + ((k-1)/2)*M*M),(k/(k-1)))/M ;

	if (isFlowReverse == no ) {
		if (isFLDknown == yes) { 
			if (isSubSonic == no ) {
				// a change in the order of the formula from the book
				PzeroRstarT =  (pow((2*k/(3*k -1)),(k/(k -1)))/sqrt(k))* 
				pow((1 + ((k-1)/2)*M*M),(k/(k-1)))/M ;
			}
			else {
				 PzeroRstarT = 1.0;
			}
		}
		else{
			PzeroRstarT = 1.0;
		}
	}
	else {
		return ;
	}
};
 
int isothermal:: calTableLine (double Mach){
	// calculated  all the important parameters and see if
	// all exist.
	// to be complete later
	M= Mach ; 
	calFLD() ;
    calPressureRatio() ;
    //iso.calDensityRatio() ;
    calTotalPressureRatio() ;
    calTotalTemperatureRatio() ;

	return 1;
};

//when M is known use this funciton
int isothermal:: calTableLine (void ){
    // calculated  all the important parameters and see if
    // all exist.
    // to be complete later
    calFLD() ;
    calPressureRatio() ;
    //iso.calDensityRatio() ;
    calTotalPressureRatio() ;
    calTotalTemperatureRatio() ;
                                                                                
    return 1;
};


int isothermal:: makeLines (int isTex, int numberOFsteps,  double
			startingPoint, double lastPoint ){
    int limit;
    double h, range, delta ;
    limit = numberOFsteps;
	range = fabs(startingPoint - lastPoint); //regrdless the end point 	
	delta = range /((double) limit); 
	h = startingPoint;
    for (int i = 0 ; (i <= limit) ; i++ ) {
        calTableLine(h);
        if (isTex == yes) {
            //changeMach ((double) h );
            makeTexTableLine();
        }
        else {
            showResults();
        };
        h = h + delta;
    };
    return 1;
};


int isothermal:: manegerCalTableLine (int isTex){
    // manage the lines calcuations  
    // ferst if all exist.
    // to be complete later
		// first calcalute the parameters 
		// now print either for tex or without tex
	//first print the header depending on the tex or ''graph'' table
	if (isTex == yes) {
		makeTexTableHead();
	}
	else{
		showHead();
	};
	//make the line or the lines
	double singularPoint;
	singularPoint = 1/sqrt(k) ; 
	makeLines(isTex, 20,0.05,(singularPoint -0.001 ));
	makeLines(isTex, 30,singularPoint+ 0.0000001,10);
	// the tail for tex if needed
	if (isTex == yes) {
        makeTexTableTail();
    };

    return 1;
};


int main( void )
{
	// iso is the name of the actual object and 1.4 is k.
	isothermal iso(0.0845, 1.4) ; // the value of first varible isn't used
	// when the manager in control  
	// the main function of calcualtion of p
	//iso.findMxForP_ratio(0.552);
	//iso.findMxForP_ratio(0.552);
	iso.findMForFLD(10.0);
	iso.makeTexTableHead(); // turn off when the manager is in control
	//iso.manegerCalTableLine (yes); //yes with tex no without it.
	//iso.calTableLine( 0.00028 );	
	iso.calTableLine( );	
	iso.makeTexTableLine();
	iso.makeTexTableTail(); // turn off when the manager is in control

	// iso.showHead();
    //iso.showResults(); //just for table in (mx)grace
	return 0;
}
