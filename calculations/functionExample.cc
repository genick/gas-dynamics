// pointer to functions
#include <iostream>

                                                                                    
using namespace std;


int addition (int a, int b)
{ return (a+b); }

int subtraction (int a, int b)
{ return (a-b); }

int (*subtract)(int,int) = subtraction;

int operation (int * x, int y, int (*functocall)(int,int))
{
  int g,  *p, v;
	v = * x + 1;
	* x =  v;
  g = (*functocall)(v,y);
  return (g);
}

int main ()
{
  int m,n, v;
	v = 7;
  m = operation (&v, 5, addition);
	v =20;
  n = operation (&v, m, subtract);
  cout <<n << endl ;
  return 0;
}
