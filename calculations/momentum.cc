/********************************************************
	this program calculates Fr_mom and Fr_mass.
	The code includes classes to simplified the programming
	in the comparison between the circular geometry and
	two plates
	also this program give the results for karni figure
	also include the calculation of volume percentage to
	height
********************************************************/

# include <stdio.h>
# include <math.h>
# include <iostream.h>

# 	define	PI 3.14159265358979310
#   define EPS  1e-4


class geometry {
        protected:
			double h2; // the low side 
			double h1 ; // the hight side 
			double area[2];
			double center[2];
			double fh1h2;
			double v;
			double Fr_mom;
			double Fr_mass;
			int above_r[2]; // 0 not reasonable 1 below 2 above
        public:
			geometry(double h) { h2 = h ; h1 = 2.0 ;};
			~geometry(){};
			virtual void cal_center() {};
			virtual void cal_area(){};
			virtual void cal_fh1h2 () {};
			virtual void cal_v () {};
			void cal_Fr_mom ();
			void cal_Fr_mass ();
			void change_h2(double h) { h2 = h ;};
			void change_h1(double h) { h1 = h ;};
			double * get_center1(){return center ; };
			double * get_area1(){return area; };
			void show_head();
			void show_results();
			double get_center(){return center[1] ; };
            double get_height (){return h2 ; };
            double get_Fr_mon (){return Fr_mom ; };
            double get_area(){return area[1]; };
            double get_Vp(){return sqrt ( 981.0 * h2 / Fr_mom ) ; };

};

void 	geometry::show_results() {

        cout.precision(4);
        cout.width(6);
        cout << h2 << "   " ;
        cout.width(6);
        cout << area[1] << "   " ;
        cout.width(6);
        cout << fh1h2 << "   " ;
        cout.width(6);
        cout << v <<  "   " ;
        cout.width(6);
        cout << Fr_mass  << "   " ;
        //cout << sqrt (387 / Fr_mom)  << "   " ;
        cout.width(6);
        cout << Fr_mom << endl ;

};

void	geometry::show_head(){

    cout.width(6);
    cout << "h1" << "   " ;
    //cout.width(6);
    //cout << "theta" << "   " ;
    //cout.width(6);
    //cout << "center"   << "   " ;
    cout.width(6);
    cout << "area[0]"  << "   " ;
    cout.width(6);
    cout << "fh1h2"  << "   " ;
    cout.width(6);
    cout << "v"  <<  "   " ;
    cout.width(6);
    cout << "Fr_mss"  << "   " ;
    //cout << "Vp"  << "   " ;
    cout.width(6);
    cout << "Fr_mom"  << endl ;

};

void  geometry::cal_Fr_mom(){
	cal_fh1h2 () ;
	cal_v () ;
	double v2 = v * v ;
	Fr_mom = ( v2 - fh1h2 * ( v - 1 ) * ( v - 1 ) ) / 
            	(fh1h2 * center[0]  -  center[1]) ; // note yc1 = 1
}

void  geometry::cal_Fr_mass(){
    cal_fh1h2 () ;
    cal_v () ;
    Fr_mass = 0.5 * ( 2.0 * v - 1.0 ) /  ( center[0]  - center[1] ) ;
}


class circle : public geometry{
	private:
		double theta[2];
	public:
		circle(double h) : geometry (h) {}  ;
		int cal_theta() ;
		void cal_center() ;
		void cal_area();
		void cal_fh1h2 () ;
		void cal_percentage( double percent);
		void cal_v () ;

};

double root( double f (circle, double, double ), circle c , double a ,
					 double b, double value) {

	double Af =  f(c,a, value) ;
	double Bf =  f(c, b, value) ;

	double m = ( a + b ) / 2.0 ;
	double Mf =  f(c, m, value) ;

	if ( Mf == 0.0 || (b - a ) <= EPS ) {
		return m;
	}
	else if ( Af * Mf  < 0.0 ) {
		return root (f, c, a, m, value ) ;
	}
	else {
		return root(f, c, m, b, value);
	}
}


int circle::cal_theta() {
	float h[2] ;
	h[0] = (float) h1 ;
	h[1] = (float) h2 ;

	for (int i =0 ; i < 2 ; i++ ) {
		if ( h[i] ==  1.0 ) {
			theta[i] = PI / 2.0  ;
			above_r[i] = 1;
		}
		else if ( (h[i] < 0.0 ) || ( h[i] > 2.0 ) ) {
			above_r[i] = 0;
		}
		else if ( h[i] < 1 ) {
			theta[i] = ( double ) acos ( 1.0 - h[i] ) ;
			above_r[i] = 1;   // to subtract area
		}
		else if ( h[i] == 2.0 ) {
			theta[i] = 0.0 ;
			above_r[i] = 2 ;
		}
		else {
			theta[i] = (double) acos( h[i] - 1.0 );
			above_r[i] = 2;  // to add area
		}
	}

};


void circle::cal_center () {
	float h[2];
	h[0] = (float)  h1 ;
	h[1] = (float)  h2 ;
	cal_theta();
	// cos (theta) = 1 - h
	double sint; // sint is the sin (theta) 
	double cost; // cost is the cos (theta) 

    for (int i =0 ; i < 2 ; i++ ) {
		if ( above_r[i] <= 1 ) {
			cost = 1.0 - h[i] ;
			sint =  sqrt ( 1.0 - cost * cost )   ; // 1 = sin ^2(t) + cos^2(t)
			center[i] = 1 - 0.66666666667 * sint * sint * sint /
						(theta[i] - sint * cost )    ;
		}
		else if ( above_r[i]  > 2 ) {
			if ( h[i] > 2.0 ){ //in case that 2.0 has small addition to be removed
				cerr << "you have height greater than 2 r" << "\n" ;
			}
			else {
				cost = h[i] - 1.0 ;
				sint =  sqrt ( 1.0 - cost * cost )   ; // 1 = sin ^2(t) + cos^2(t)
				center[i] = 1 - 0.66666666667 * sint * sint * sint /
						(PI - theta[i] + sint * cost )    ;
			}	
		}
		else {
			cost = h[i] - 1.0 ;
			sint =  sqrt ( 1.0 - cost * cost )   ; // 1 = sin ^2(t) + cos^2(t)
			center[i] = 1 - 0.66666666667 * sint * sint * sint /
						(PI - theta[i] + sint * cost )    ;
		}
	}
}

void circle::cal_area ( ) {
    float h[2];
    h[0] = (float)  h1 ;
    h[1] = (float)  h2 ;
    cal_theta();


    for (int i =0 ; i < 2 ; i++ ) {
		double complementary_to_theta;	
		complementary_to_theta = PI/2.0 - theta[i] ;
		if ( above_r[i] == 0 ){
			cout <<  "this is an error" << "\n" ;
		}
		if ( above_r[i] == 1 ) { // h2 is below center
			area[i] =  0.5 * PI - complementary_to_theta - 
					( 1.0 - h[i]) * sin (theta[i] ) ; // cos(t) = 1 - h  
		}
		else {
			area[i] = 0.5 * PI + complementary_to_theta + ( h[i] - 1.0 ) *
				sin (theta[i] ) ;
		} 
	}
}

double cal_area_est ( circle c, double estimated_height, double vol_percent) {
	c.change_h2 (estimated_height) ;
	c.cal_area();
	double A = c.get_area() ;
	return ( PI*vol_percent - c.get_area()) ;
};

void circle::cal_percentage (double  percent) {
	float  estimated_height = 2.0 * (float) percent ;
	if ( (float) percent == 0.5 ){
		h2 = 1.0 ; // 50% is r = 1.0
	}
	else if ( (float ) percent == 1.0 ) {
		h2 = 2.0 ;
	}
	else if ( percent == 0.0 ) {
		h2 = 0.0 ;
	}
	else if ( percent < 0.5 ) {
		h2 = root (cal_area_est,*this, 0.0, 1.0, percent);
	}
	else {
		h2 = root (cal_area_est,*this, 1.0, 2.0, percent);
	}
	
};

void circle::cal_fh1h2 () {
	cal_area();
	cal_center();
	fh1h2 = area[0] /  area[1] ;
};

void circle::cal_v () {
	v =  fh1h2 / ( fh1h2 - 1.0 ) ;
};



class twoPlate : public geometry{
	public:
		twoPlate(double h) : geometry (h) {}  ;
		void cal_center() ;
		void cal_area();
        void cal_fh1h2 () ;
        void cal_v () ;
};


void twoPlate::cal_center () {
    float h[2] ;
    h[0] = (float) h1 ;
    h[1] = (float) h2 ;

    for (int i =0 ; i < 2 ; i++ ) {
		center[i] = h[i] / 2.0 ;
	}
}

void twoPlate::cal_area ( )
{
        float h[2] ;
    h[0] = (float) h1 ;
    h[1] = (float) h2 ;

    for (int i =0 ; i < 2 ; i++ ) {

	    area[i] = h[i] / 2.0;
	}
	
}

void twoPlate::cal_fh1h2 () {
	cal_area();
	cal_center();
	fh1h2 = h1 / h2 ;
};

void twoPlate::cal_v () {
	v =  fh1h2 / ( fh1h2 - 1.0 ) ;
};


int main(void )
{
	circle c(0.05), cc(0.05);
//	twoPlate p(0.05);
	float h ;
	float h2 ;
//	float dammy;
//	cout << "what is the value for h2:  " ;
//	cin >> h2 ;
//	cout << endl ;
//	h = (float) h2  ;
//	cc.cal_percentage(h) ;
//	dammy = cc.get_height ();	
//	c.change_h1((double) (h) );
//	p.change_h1((double) (h) );
//	c.change_h2((double) (dammy) );
//	p.change_h2((double) (h) );

	h = 0.05 ;
	int limit ;
	limit = -1 + (int) (  (2.0 - h ) * 20  +  0.0001 ) ;
	cout << "this is limit : " << limit << endl ; 
	cout << "this is the h2 : " << h2 << endl ;
	
	c.show_head();
	c.change_h1((double) (  2.0 ) );

	for (int i = 0 ; (i <= limit) ; i++ ) {
		h = h + 0.05;	
	    //cc.cal_percentage(h) ;
		//dammy = cc.get_height ();
		//c.change_h1((double) (  dammy ) );
		c.change_h2((double) (  h ) );
		c.cal_Fr_mom ();
        c.cal_Fr_mass();
        c.show_results();
		//p.change_h1( (double) h );
		//p.cal_Fr_mom ();
        //p.cal_Fr_mass();
        //p.show_results();
	}
	return 0;
}
