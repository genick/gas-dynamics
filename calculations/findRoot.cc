/********************************************************

	This code is copyrighted by Genick Bar_Meir 
	and has no  warrenty of any kind.
	Use this code at yourown risk. 
	
	This program is open source under the same lisence as
	all the others material in potto project.
	By:
	Genick Bar-Meir

	Inputes:
	n/a

	Returens:
	prints the stagnation and static values of compressible flow
	also find the Mach number for given Pressure ratio. 
	
	Description:
	This code includes classe(s) to calculate the stagnation
	and static conditions for various Mach numbers.
********************************************************/

# include <stdio.h>
# include <math.h>
# include <iostream.h>

# 	define	PI 3.14159265358979310
#   define EPS  1e-4


class stagnation {
        protected:
			double Tbar; // relative temperature
			double Pbar ; // relative pressure
			double Area ; // relative area 
			double Mach ; // Mach nubmer
			double Rbar ; // relative density 
			double k	; // ratio of spesific heat
        public:
			stagnation(double M, double spesificHeat ) { Mach = M ;
					k = spesificHeat; };
			~stagnation(){};
			void calPressure() ;
			void calTemperature();
			void calDensity();
			void calArea();
			void findMforP(double);
			void changeMach ( double M) {Mach = M; } ;
			void changePressure ( double P) {Pbar = P; } ;
			void showHead();
			void showResults();
			double getPressure(){return Pbar ; };
            double getTemperatue (){return Tbar ; };
            double getDensity (){return Rbar ; };
            double getArea (){return Area ; };
};

void 	stagnation::showResults() {

        cout.precision(8);
        cout.width(8);
        cout << Mach << "   " ;
        cout.width(8);
        cout << Tbar << "   " ;
        cout.width(8);
        cout << Rbar << "   " ;
        cout.width(8);
        cout << Area << "   " ;
        cout.width(8);
        cout << Pbar << "   " ;
        cout << endl ;

};

void	stagnation::showHead(){

    cout.width(8);
	cout << "Mach " << "   "  ;
    cout.width(8);
    cout << "Tbar" << "   " ;
    cout.width(8);
    cout << "Rbar" << "   " ;
    cout.width(8);
    cout << "Area" << "   " ;
    cout.width(8);
    cout << "Pbar"  << "   " ;
    cout.width(8);
	cout << "k = "  << k << "    ";
    cout.width(8);
    cout << endl ;

};

//calculate the diffrence the estimated Pbar and known Pbar
//double calMachEstmt ( stagnation c, double estimatedMach, double value ) {
double calPressureEstmt ( stagnation c, double estimatedMach, double value ) {
    c.changeMach (estimatedMach) ;
    c.calPressure();
    return (value - c.getPressure() ) ;
};


double root( double f (stagnation, double , double ),
			 stagnation c , double a , double b, double value) {

    double Af =  f(c,a, value) ;
    double Bf =  f(c, b, value) ;
                                                                                
    double m = ( a + b ) / 2.0 ;
    double Mf =  f(c, m, value ) ;
                                                                                
    if ( Mf == 0.0 || (b - a ) <= EPS ) {
        return m;
    }
    else if ( Af * Mf  < 0.0 ) {
        return root (f, c, a, m, value ) ;
    }
    else {
        return root(f, c, m, b, value);
    }
}


void  stagnation::findMforP(double p ){
    Mach = root ( calPressureEstmt, *this, 0.000001,  1.0, (double) p) ;    
};

void  stagnation::calArea(){
	Area =  pow((( 1 + (( k - 1) / 2 ) * Mach*Mach)/((k+1)/2)), 
			((k+1) / 2/(k-1) ))
				/ Mach;
};

void  stagnation::calTemperature(){
	Tbar = 1 / ( 1 + (( k - 1) / 2 ) * Mach*Mach);
};

void  stagnation::calPressure(){
	Pbar =  pow (1 / ( 1 + (( k - 1) / 2 ) * Mach*Mach),(k / (k - 1) ) ) ;
};

void  stagnation:: calDensity (){
	Rbar = pow ( Pbar, (1/k)) ; 
};


int main( void )
{
	stagnation c(1.0, 1.4) ;
	c.showHead();
	//c.changePressure(0.6);
	c.findMforP(0.17040604);
    c.calTemperature() ;
    c.calPressure() ;
    c.calDensity() ;
    c.calArea() ;
	c.showResults();

	return 0;
}
