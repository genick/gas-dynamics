/********************************************************

	This code is copyrighted by Genick Bar_Meir 
	and has no  warrenty of any kind.
	Use this code at yourown risk. 
	
	This program is open source under the same lisence as
	all the others material in potto project.
	By:
	Genick Bar-Meir

	Inputes:
	n/a

	Returens:
	prints the stagnation and static values of compressible flow
	also find the Mach number for given Pressure ratio. 
	
	Description:
	This code includes classe(s) to calculate the stagnation
	and static conditions for various Mach numbers.
********************************************************/

# include <stdio.h>
# include <math.h>
# include <iostream.h>
# include <iomanip.h>

# 	define	PI 3.14159265358979310
#   define EPS  1e-4


class stagnation {
        protected:
			double Tbar; // relative temperature
			double Pbar ; // relative pressure
			double Area ; // relative area (A /A*) 
			double Mach ; // Mach nubmer
			double Rbar ; // relative density 
			double k	; // ratio of spesific heat
			double PAR	; // ratio of (AP / A*P0) 
        public:
			stagnation(double M, double spesificHeat ) { Mach = M ;
					k = spesificHeat; };
			~stagnation(){};
			void calPressure() ;
			void calTemperature();
			void calDensity();
			void calArea();
			void calPAR();
			void findMforP(double);
			void findMforA(double);
			void findMforPAR(double);
			void changeMach ( double M) {Mach = M; } ;
			void changePressure ( double P) {Pbar = P; } ;
			void showHead();
			void showResults();
            void makeTexTable();
            void makeTexTableHead();
            void makeTexTableTail();
			double getPressure(){return Pbar ; };
            double getTemperatue (){return Tbar ; };
            double getDensity (){return Rbar ; };
            double getArea (){return Area ; };
            double getPAR (){return PAR ; };
};

void 	stagnation::makeTexTableHead() {
	
	cout << endl << endl;
	cout << "*******************************************" << endl ;
	cout << "The following stuff is the same as above" << endl ;
	cout << "information but is setup for the latex text processing" << endl ;
	cout << "you can just can cut and paste it in your latex file" << endl ;
	cout << "you must use longtable style file and dcolumn " << endl ;
	cout << "style file." << endl  << endl << endl;
	cout << "*******************************************" << endl ;
	cout << "\\setlongtables" << endl ;
	cout << "\\begin{longtable}" << endl ;
	cout << "{|D..{-1}|D..{3.7}|D..{3.7}|D..{3.7}|D..{3.7}|D..{3.7}|}" << endl ;
	cout << "\\caption{ ??  \\label{?:tab:?}}\\\\"  ;
	cout << "\\hline" << endl ;
 	cout << "\\multicolumn{1}{|c|} " ; // the same line
	cout << "{$\\rule[-0.1in]{0.pt}{0.3 in} \\mathbf{M} $} ";// continue  line
	cout << " & " << endl;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{T \\over T_0} $      } & " ;
	cout << endl ;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{\\rho \\over \\rho_0} $ } & ";
	cout << endl ;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{A \\over A^{\\star}} $    } & ";
	cout << endl ;
   	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{P \\over P_0} $ } &";
	cout << endl ;
   	cout << "\\multicolumn{1}{|c|}{$";
	cout << "  \\mathbf{A\\times P \\over A^{*} \\times P_0 } $ }";
	cout << endl ;
	cout << "\\\\\\hline" << endl ;
	cout << "\\endfirsthead" << endl ;
	cout << "\\caption{ ?? (continue)} \\\\" << endl ;
	cout << "\\hline" << endl ;
    cout << "\\multicolumn{1}{|c}" ;
	cout << "{$   \\rule[-0.1in]{0.pt}{0.3 in} \\mathbf{M} $} &" << endl ;
    cout << "\\multicolumn{1}{|c}{$   \\mathbf{ T \\over T_0} $     } & ";
	cout << endl;
    cout << "\\multicolumn{1}{|c}{$   \\mathbf{\\rho \\over \\rho_0} $ } &";
	cout << endl;
    cout <<"\\multicolumn{1}{|c}{$   \\mathbf{A \\over A^{\\star}} $  } &";
	cout << endl;
    cout <<"\\multicolumn{1}{|c|}{$  \\mathbf{P \\over P_0} $    } & ";
    cout << endl ; 
   	cout << "\\multicolumn{1}{|c|}{$" ;
	cout << "  \\mathbf{A\\times P \\over A^{*} \\times P_0 } $ }";
	cout << endl ;
	cout << "\\\\\\hline" << endl;
	cout << "\\endhead" << endl;

};
void 	stagnation::makeTexTableTail() {
	cout << "\\end{longtable}" << endl ;
};

void 	stagnation::makeTexTable() {

  	cout << setfill (' ') << setw (10);
        cout.setf(ios::fixed | ios::showpoint);

        cout.precision(3);
        cout.width(4);
        cout << Mach << "&\t" ;
		 // if (Mx < 10) cout << "\t" ;
        cout.width(6);
        cout << Tbar << "&\t" ;
        cout.width(6);
        cout << Rbar << "&\t" ;
        cout.width(6);
        cout << Area << "&\t" ;
        cout.width(6);
        cout << Pbar << "&\t" ;
        cout.width(6);
		cout << PAR << "\t \\\\ \\hline" ;
        cout << endl ;
};


void 	stagnation::showResults() {

  cout << setfill (' ') << setw (10);
        cout.setf(ios::fixed | ios::showpoint);

        cout.precision(3);
        cout.width(4);
        cout << Mach << "\t" ;
		 // if (Mx < 10) cout << "\t" ;
        cout.width(6);
        cout << Tbar << "\t" ;
        cout.width(6);
        cout << Rbar << "\t" ;
        cout.width(6);
        cout << Area << "\t" ;
        cout.width(6);
        cout << Pbar << "\t" ;
        cout.width(6);
        cout << PAR << "\t" ;
        cout << endl ;

};

void	stagnation::showHead(){

    cout << setfill (' ') << setw (10);
    cout.setf ( std::ios_base::right,
                     std::ios_base::basefield );
    cout.width(6);
    cout << "k = "  << k <<  "\n\n" ;

    cout.width(6);
	cout << "Mach " << "\t"  ;
    cout.width(6);
    cout << "Tbar" << "\t" ;
    cout.width(6);
    cout << "Rbar" << "\t" ;
    cout.width(6);
    cout << "Area" << "\t" ;
    cout.width(6);
    cout << "Pbar"  <<  "\t" ;
	cout.width(6);
	cout << "PAR" <<  endl ;

};

//calculate the diffrence the estimated Pbar and known Pbar
//double calMachEstmt ( stagnation c, double estimatedMach, double value ) {
double calPressureEstmt ( stagnation c, double estimatedMach, double value ) {
    c.changeMach (estimatedMach) ;
    c.calPressure();
    return (value - c.getPressure() ) ;
};

double calAreaEstmt ( stagnation c, double estimatedMach, double value
) {
    c.changeMach (estimatedMach) ;
    c.calArea();
    return (value - c.getArea() ) ;
};

double calPAREstmt ( stagnation c, double estimatedMach, double value
) {
    c.changeMach (estimatedMach) ;
    c.calArea();
    c.calPressure();
    c.calPAR();
    return (value - c.getPAR() ) ;
};



double root( double f (stagnation, double , double ),
			 stagnation c , double a , double b, double value) {

    double Af =  f(c,a, value) ;
    double Bf =  f(c, b, value) ;
                                                                                
    double m = ( a + b ) / 2.0 ;
    double Mf =  f(c, m, value ) ;
                                                                                
    if ( Mf == 0.0 || (b - a ) <= EPS ) {
        return m;
    }
    else if ( Af * Mf  < 0.0 ) {
        return root (f, c, a, m, value ) ;
    }
    else {
        return root(f, c, m, b, value);
    }
}


void  stagnation::findMforP(double p ){
    Mach = root ( calPressureEstmt, *this, 1.000001,  10.0, (double) p) ;    
	
};

void  stagnation::findMforA(double A ){
    Mach = root ( calAreaEstmt, *this, .000001,  1.0, (double) A) ;
                                                                                
};

void  stagnation::findMforPAR(double PAR ){
    Mach = root ( calPAREstmt, *this, .000001,  1.0, (double) PAR) ;
                                                                                
};



void  stagnation::calArea(){
	Area =  pow((( 1 + (( k - 1) / 2 ) * Mach*Mach)/((k+1)/2)), 
			((k+1) / 2/(k-1) ))
				/ Mach;
};


void  stagnation::calPAR(){
	PAR =  Area * Pbar;
};

void  stagnation::calTemperature(){
	Tbar = 1 / ( 1 + (( k - 1) / 2 ) * Mach*Mach);
};

void  stagnation::calPressure(){
	Pbar =  pow (1 / ( 1 + (( k - 1) / 2 ) * Mach*Mach),(k / (k - 1) ) ) ;
};

void  stagnation:: calDensity (){
	Rbar = pow ( Pbar, (1/k)) ; 
};

int main( void )
{
	stagnation c(2.37, 1.4) ;
	c.showHead();
	//c.changePressure(0.6);
	//c.findMforP(0.17040604);
	//c.findMforA(3.00);
	//c.findMforPAR(1.5);
    c.calTemperature() ;
    c.calPressure() ;
    c.calDensity() ;
    c.calArea() ;
	c.calPAR() ;
	c.showResults();
    c.makeTexTableHead();
    c.makeTexTable();
    c.makeTexTableTail();

	return 0;
}
