/********************************************************

	This code is copyrighted by Genick Bar_Meir 
	and has no  warrenty of any kind.
	Use this code at your own risk. 
	
	This program is open source under the same lisence as
	all the others material in potto project.
	By:
	Genick Bar-Meir

	Inputes:
	n/a

	Returens:
	prints table for Mx and My and the other ratio
	for the normal shock wave.
	It assume perfect gas with k=1.4
	
	Description:
	This code includes classes to calculate the Mx number from
	both side of the shock wave and the other parameters.
********************************************************/

# include <stdio.h>
# include <math.h>
# include <iostream.h>
# include <iomanip.h>

# 	define	PI 3.14159265358979310
#   define EPS  1e-4


class shock {
        protected:
			double TyTx; // temperature ratio Ty/Tx
			double PyPx ; // pressure ratio Py/Px
			double P0yP0x ; // pressure ratio P0y/P0x
			double Area ; // relative area 
			double Mx ; // Mx nubmer befor the normal shock
			double My ; // My nubmer after the normal shock
			double RyRx ; // relative density rhoY/rhox 
			double k	; // ratio of spesific heat
        public:
			shock(double M, double spesificHeatRatio ) { Mx = M ;
					k = spesificHeatRatio; };
			~shock(){};
			void calMy() ;
			void calPressureRatio() ;
			void calTotalPressureRatio() ;
			void calTemperatureRatio();
			void calDensityRatio();
			void calArea();
			void changeMx ( double M) {Mx = M; } ;
			void showHead();
			void showResults();
			double getPressure(){return PyPx ; };
            double getTemperatue (){return TyTx ; };
            double getDensity (){return RyRx ; };
            double getArea (){return Area ; };
};

void 	shock::showResults() {

	    cout << setfill (' ') << setw (10);
		cout.setf(ios::fixed | ios::showpoint);
		//cout.setf ( std::ios_base::right, std::ios_base::showpoint, 
		//			 std::ios_base::basefield );
        cout.precision(4);
        cout.width(6);
        cout << Mx << "\t" ;
		// if (Mx < 10) cout << "\t" ;
        cout.precision(5);
        cout.width(6);
        cout << My << "\t\t" ;
        cout.width(6);
        cout << TyTx << "\t" ;
		if (TyTx < 10) cout << "\t" ;
        cout.width(6);
        cout << RyRx << "\t" ;
		if (RyRx < 10) cout << "\t" ;
        cout.width(6);
        cout << PyPx << "\t"  ;
		if (PyPx < 10) cout << "\t" ;
        cout.width(6);
        cout << P0yP0x  ;
        cout << endl ;

};

void	shock::showHead(){

    cout << setfill (' ') << setw (10);
	cout.setf ( std::ios_base::right, 
					 std::ios_base::basefield );
    cout.width(6);
	cout << "k = "  << k <<  "\n\n" ;
    cout.width(6);
	cout << "Mx " << "\t"  ;
    cout.width(6);
	cout << "My " << "\t\t"  ;
    cout.width(6);
    cout << "TyTx" << "\t\t" ;
    cout.width(6);
    cout << "RyRx" << "\t\t" ;
    cout.width(6);
    cout << "PyPx"  << "\t\t" ;
    cout.width(6);
    cout << "P0yP0x"   ;
    cout << endl ;

};


void  shock::calMy(){
	My = sqrt((Mx*Mx + 2.0/(k -1)) / 
		((2*k / (k-1) ) *Mx*Mx -1)) ; 
};

void  shock::calArea(){
	Area =  pow((( 1 + (( k - 1) / 2 ) * Mx*Mx)/((k+1)/2)), 
			((k+1) / 2/(k-1) ))
				/ Mx;
};

void  shock::calTemperatureRatio(){
	TyTx = PyPx *PyPx * My*My/Mx/Mx;
};

void  shock::calPressureRatio(){
	PyPx =  (1 + k*Mx*Mx) / (1 + k*My*My) ;
};

void  shock::calTotalPressureRatio(){
	P0yP0x =  (PyPx)* 
	pow((1 + ((k-1)/2)*My*My),(k/(k-1))) / 
	pow((1 + ((k-1)/2)*Mx*Mx),(k/(k-1))) ;
};

void  shock:: calDensityRatio (){
	RyRx = PyPx/TyTx; 
};


int main( void )
{
	double R=2.197;
	shock c(R, 1.4) ;
	c.showHead();
	c.calMy() ;
	c.calPressureRatio() ;
	c.calTotalPressureRatio() ;
	c.calTemperatureRatio() ;
	c.calDensityRatio() ;
	c.showResults();

	return 0;
}
