#  include <cstdio>
#  include <iostream>
#  include <cmath>

using namespace std;
                                                                                


double third=0.33333333333333333333333333333333333 ;
double x2[2], x3[2];

int cubicRoot(double a, double b, double c, double d, double * x1){
	double f, g, h; 
	g = (2.*b*b*b/(a*a*a)-9.*b*c/(a*a) + 27.*d/a)/27. ;
	f = (((3.*c)/a) - (((b*b)/(a*a))))/3.0 ;
	h = g*g/4 + f*f*f/27. ;

	if (h > 0) {
	double m, m2,  n , n2;
	m = (-(g/2)+ sqrt(h)) ;
	int k ;
	k=1 ;
	if (m < 0) k=-1; else k=1 ;
	m2 = pow((m*k),third);
	m2 = m2*k ;
	k=1 ;
	n = (-(g/2)- (sqrt(h))) ;
	if (n < 0) k=-1; else k=1 ;
	n2 = pow((n*k),third) ;
	n2 = n2*k ;
	k=1 ;
	*x1= ((m2 + n2) - (b/(3*a))) ;
////      ((S+U)     - (b/(3*a)))-->

	x2[0]=-(m2 + n2)/2 - (b/(3*a)) ;
	x2[1]= (m2 - n2)/2*pow(3,.5); 
////      -(S + U)/2  - (b/3a) + i*(S-U)*(3)^.5-->

	x3[0]=-(m2 + n2)/2 - (b/(3*a))  ; 
	x3[1]= -0.5*(m2 - n2)*pow(3,.5);
////          -(S + U)/2  - (b/3a) - i*(S-U)*(3)^.5-->
	k=1;
}


else if (h<=0) {
	double r ;
	int k;
	r = sqrt((g*g/4.)-h) ;
	k=1 ;

	if (r<0) k=-1  ;
//<!-- rc is the cube root of 'r' -->

	double rc ;
	rc = pow((r*k),(third))*k ;
	k=1;
	double  theta, x2a, x2b, x2c, x2d;
	theta =acos((-g/(2.*r)));
	*x1=(2.*(rc*cos(theta/3.))-(b/(3.*a))) ;
	x2a=-rc;
	x2b= cos(theta/3.);
	x2c= sqrt(3.)*(sin(theta/3.)) ;
	x2d= (b/3*a)*-1. ;

	x2[0]=(x2a*(x2b + x2c))-(b/(3.*a)) ;
	x3[0]=(x2a*(x2b - x2c))-(b/(3.*a)) ;
	x2[1]=0.;
	x3[1]=0.;
}

else if ((f+g+h)==0) {
	int sign;
	double dans;
	if (d<0) {sign=-1;} 
	else {sign=1;};
	if (sign>0){dans=pow((d/a),(1/3));dans= -dans;} 
	else if (sign<0){d=d*-1;dans=pow((d/a),(1/3));};
	*x1=dans;
	x2[0]=dans;
	x3[0]=dans;
	x2[1]=0.;
	x3[1]=0.;
}
	return 1;

}

int main ( void ) {
	double a, b, c, d; 
	a = 1.;
	b =  -1.6740601803568262 ;     
	c = 0.70345851015910743;   
	d =  -0.0023742740503436763;    
	double x1 ;
	cubicRoot(a,b,c,d, &x1 );
	cout << "x1= " << x1 << endl ;
	cout << "x2= " << x2[0] << "+i" << x2[1] << endl ;
	cout << "x3= " << x3[0] << "+i" << x3[1] << endl ;
	return (int) x1;
}
