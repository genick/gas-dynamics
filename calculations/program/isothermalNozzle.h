/********************************************************
	file: isothermalNozzle.h
	******************
	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, isthermalNozzle and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#ifndef _ISOTHERMALnozzle_STDLIB
#define _ISOTHERMALnozzle_STDLIB

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip> 
#  include "compressibleFlow.h"
#  include "shock.h"
#  include "stagnation.h"

class isothermalNozzle : public compressibleFlow {
        protected:
        public:
			// to change the initialization later on
			// so that base class initialization will be considered
			isothermalNozzle(double spesificHeatRatio ):
				//initialization of base class
				compressibleFlow(spesificHeatRatio, isothermalNozzleFlow)
					 { 
					// specific initialization process for the derived class
						M1maxSub = 1./ sqrt(spesificHeatRatio);
						singularPoint = M1maxSub ; 
			  		};
			~isothermalNozzle(){};
			//spesific functions of this model of flow to calculate
			//spesific parameters of this flow model.
			// 
			void setMach(double Mach) {M = Mach ;};
			void calMach( inputType, double * ) ;
            void calPressureRatio() ;
            void calTemperatureRatio();
            void calTotalTemperatureRatio();
            void calTotalPressureRatio();
            void calDensityRatio();
            void calAreaRatio();
            void calPARRatio();
            double calPressureRatio(double ) ;
			int calAll( inputType, double *);
			void  calImpulse();

            // specific functions to fanno flow to be add in presenting
            // the results
            void showHead();
            void showResults();
            void makeTexTableHead();
            void makeTexTableLine ();
			int  calTableLine ( inputType , double * );
			int  doAllLines (int , inputType , double * ) ;

};

#endif  /* _ISOTHERMALnozzle_STDLIB */
