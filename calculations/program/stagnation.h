/********************************************************
	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip> 
#  include "definition.h" 
#  include "compressibleFlow.h"
#  include "shock.h"

#ifndef _STAGNATION_STDLIB
#define _STAGNATION_STDLIB


class stagnation : public compressibleFlow {
        protected:
        public:
			// to change the initialization later on
			// so that base class initialization will be considered
			stagnation(double spesificHeatRatio ):
				//initialization of base class
				compressibleFlow(spesificHeatRatio, stagnationFlow)
					 { 
					// specific initialization process for the derived class
						M1maxSub = 1.0;
						singularPoint = M1maxSub ; 
			  		};
			~stagnation(){};
			//spesific functions of this model of flow to calculate
			//spesific parameters of this flow model.
			double getDensity (){return Rbar ; };
            int  calTableLine ( inputType, double * );
            int  calAll( inputType, double *) ;
			void setMach(double Mach) {M = Mach ;};
			void calMach( inputType, double * ) ;
            void calPressureRatio() ;
            void calTemperatureRatio();
            void calDensityRatio();
            void calAreaRatio();
            void calPARRatio();
            void calFn();
            void calFnB();
            void calRhat();
			void calMforRhat();
            void calTotalTemperatureRatio (void) ;
			void calTotalPressureRatio (void);
			void calImpulse(void);

			void showHead() ;
			void showResults() ;
            void makeTexTableHead();
            void makeTexTableLine ();
            double calPressureRatio(double ) ;
            virtual int  manegerCalTableLine(int, inputType,  double *);
            int  doAllLines(int, inputType, double *);

};

#endif  /* _STAGNATION_STDLIB */
