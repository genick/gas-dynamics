/********************************************************
	FILE:isothermalNozzle.cc

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, isothermalNozzle and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip> 
#  include "isothermalNozzle.h" 
#  include "externalFun.h" 

using namespace std;

// the isothermalNozzle class functions definitions
//**********************************************//

void  isothermalNozzle::calMach(inputType variableName, double * variableValue){
	if (variableName == machV) {
		M = variableValue[machV];
	}
	else if (variableName == TRzeroStarV) {
		TRzeroStar = variableValue[TRzeroStarV];
		M = root  ( calTRzeroStarEstmt, this, EPS,  500., TRzeroStar) ;
    }
    else if (variableName == PRzeroStarV) {
        PRzeroStar = variableValue[PRzeroStarV];
        M = variableValue[machV];
        if ( M > 1. )
            M = root(calPRzeroStarEstmt,this,1.0,300.,PRzeroStar);
        else  if (M < 1. )
            M = root(calPRzeroStarEstmt,this,.00001, 1.0,PRzeroStar);        else
            M = 1.0;
    }
	else if (variableName == PbarV) {
		Pbar = variableValue[PbarV];
		M = variableValue[machV];
		M = root  ( calPbarEstmt, this, .00001,  100., Pbar) ; 
	}
	else if (variableName == TbarV) {
		//Tbar = variableValue[TbarV];
		TRzeroStar = variableValue[TbarV];
		M = variableValue[machV];
		M = root  ( calTRzeroStarEstmt, this, .00001,  100.,
TRzeroStar) ; 
	}
	else if (variableName == AstarV) {
        Area = variableValue[AstarV];
		if ( variableValue[machV] < singularPoint ) {
        	M = root  ( calAstarEstmt, this, .00001,  M1maxSub, Area) ;
		}
		else {
        	M = root  ( calAstarEstmt, this, M1maxSub, 20., Area) ;
		}
    }
	else if (variableName == PARV) {
		PAR = variableValue[PARV];
		M = root  ( calPAREstmt, this, .00001,  M1maxSub, PAR) ;
	}
};

void  isothermalNozzle::calPressureRatio(){
	Pbar = exp((1-M*M  )*k*0.5) ;
};

double   isothermalNozzle::calPressureRatio(double MM){
    return  pow (1. / ( 1. + (( k - 1.) / 2. ) * MM*MM),(k / (k - 1.) ) ) ;
};

void  isothermalNozzle::calTemperatureRatio(){
	Tbar = 1. / ( 1. + (( k - 1.) / 2. ) * M*M);
};

void  isothermalNozzle::calDensityRatio(){ 
	 Rbar = pow ( Pbar, (1./k)) ;
};
void  isothermalNozzle::calAreaRatio(){
	    Area = exp((M*M -1 )*k*0.5) / M;
};

void  isothermalNozzle::calPARRatio(){
	PAR =  Area * Pbar;
};

void isothermalNozzle::calTotalTemperatureRatio(){
	TRzeroStar = pow(((2. * (1.0 + 0.5 * (k -1) *M*M ))/(k+1.0)),
			(k / (k -1.0))) ;
	//double tmpTzR = TRzeroStar; 
	// (2. * (1.0 + 0.5 * (k -1) *M*M ))/(k+1.0) ;
} 

void  isothermalNozzle::calImpulse(){
    FRstar = (1. + k*M*M)/(1.+k)/M ;
}

void isothermalNozzle::calTotalPressureRatio(){
	PRzeroStar = exp((1 - M*M  )*k*0.5) *  
			pow(((2. * (1.0 + 0.5 * (k -1) *M*M ))/(k+1.0)),
			(k / (k -1.0))) ;
} 

void    isothermalNozzle::showHead(){

    cout << endl << endl <<  "Isothermal Nozzle" << "\t\t"  ;
    cout << "k = " << k << endl << endl ;

    if (outputInfo == infoStandard || outputInfo == infoStagnation ) {
        int variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = "M  ";
        variableNames[1] = "T0/T0*";
        variableNames[2] = "P0/P0*";
        variableNames[3] = "A/A*";
        variableNames[4] = "P/P*";
        variableNames[5] = "PAR";
        variableNames[6] = "F/F*";


        showVariableHead( variableNames, variableNumber);
    }
	if (outputInfo == infoTubeProfile ) {
        int variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = "z";
        variableNames[1] = "M";
        variableNames[2] = "T0/T0*";
        variableNames[3] = "P0/P0*";
        variableNames[4] = "A/A*";
        variableNames[5] = "P/P*";
        variableNames[6] = "PAR";
                                                                               
        showVariableHead( variableNames, variableNumber);
    }

}

void    isothermalNozzle::showResults() {
    int numberVariables;
    if (outputInfo == infoStandard || outputInfo == infoStagnation ) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M ;
        variableToBePrinted[1] = TRzeroStar;
        variableToBePrinted[2] = PRzeroStar;
        variableToBePrinted[3] = Area;
        variableToBePrinted[4] = Pbar ;
        variableToBePrinted[5] = PAR;
        variableToBePrinted[6] = FRstar;

        showVariable(variableToBePrinted, numberVariables);
    }
	if (outputInfo == infoTubeProfile ) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
		if (M < singularPoint) {
        	variableToBePrinted[0] = 1.0509734 - Area* 0.052548607  ;
		}
		else {
            variableToBePrinted[0] = 0.949027851 + Area* 0.052548607 ;
        }
        variableToBePrinted[1] = M ;
        variableToBePrinted[2] = TRzeroStar;
        variableToBePrinted[3] = PRzeroStar;
        variableToBePrinted[4] = Area;
        variableToBePrinted[5] = Pbar ;
		double tmpA, tmpB;
		tmpA = pow((1.0+0.5*(k-1)*M*M),(k / (k - 1.) ));
		tmpB = exp(0.5*k) * pow((2.0/(k+1.)),(k/(k-1.0))); 

        variableToBePrinted[6] =  PRzeroStar/ tmpA /tmpB; //PAR;

        showVariable(variableToBePrinted, numberVariables);
    }

}

void  isothermalNozzle::makeTexTableHead(){ 
                                                                               
    int variableNumber;
                                                                               
    if (outputInfo == infoStandard || outputInfo == infoStagnation ) {
        variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = "M";
        variableNames[1] = "T_0 \\over {T_0}^{\\star}";
        variableNames[2] = "P_0 \\over {P_0}^{\\star}";
        variableNames[3] = "A \\over A^{\\star}";
        variableNames[4] = "P \\over P^{\\star}";
        variableNames[5] = "A\\times P \\over A^{*} \\times P_0";
        variableNames[6] = "F \\over F^{*}";

                                                                               
        showVariableTeXHead(variableNames,variableNumber);
    }
}

void  isothermalNozzle::makeTexTableLine(){ 
    int numberVariables;
                                                                               
    if (outputInfo == infoStandard  || outputInfo == infoStagnation ) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M ;
        variableToBePrinted[1] = TRzeroStar;
        variableToBePrinted[2] = PRzeroStar;
        variableToBePrinted[3] = Area;
        variableToBePrinted[4] = Pbar ;
        variableToBePrinted[5] = PAR;
        variableToBePrinted[6] = FRstar;
                                                                               
        showVariableTeXLine (variableToBePrinted, numberVariables);
    }
}

int   isothermalNozzle::calAll(inputType variableName,
			 double * variableValue ){ 
        calMach(variableName, variableValue);
        calAreaRatio();
        calPressureRatio();
        calTotalTemperatureRatio();
        calTotalPressureRatio();
        calDensityRatio ();
        calPARRatio();
        calImpulse();
        return yes ;
}


int isothermalNozzle:: calTableLine (
		inputType variableName, double * variableValue){
    if (outputInfo == infoStandard || outputInfo == infoStagnation){
        return calAll(variableName,variableValue);
    }
    else  {
        return no;
    }
};

int   isothermalNozzle::doAllLines (int isTex,
	inputType variableName, double * variableValue ){
    int howManyPoints ;
    if (outputInfo == infoStandard ){
                                                                                
        howManyPoints =  2 ;
        makeLines(isTex,howManyPoints, (.0+EPS), 0.1,
                                 variableName, variableValue);
        howManyPoints =  7 ;
        makeLines(isTex,howManyPoints, 0.2, 0.9,
                                 variableName, variableValue);
        howManyPoints =  9 ;
        makeLines(isTex,howManyPoints, 1.0, 1.9,
                                 variableName, variableValue);
        howManyPoints =  16 ;
        makeLines(isTex,howManyPoints, 2., 10.,
                             variableName, variableValue);
    }
    return yes;
}

