/********************************************************
	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/
#ifndef _DEFINITION_STDLIB
#define _DEFINITION_STDLIB

# 	define	PI 3.1415926535897932384626433832795028
# 	define	infinity   1.79769313486231570E+308 
#   define EPS  1e-6
#  	define THIRD 0.33333333333333333333333333333333333333333    
# 	define yes  1
# 	define no  0
#   define sonic 3 // Mach = 1 
//what info is given/sought
enum inputType{ _noV, machV, fldV, p2p1FLDV, angleV, M1ShockV, FLDShockV,
	M1fldV, M1fldP2P1V, MxV, MyV, MFLDshockLocationV, PbarV, AstarV,
	PARV, P0yP0xV, PyPxV,  TbarV, TRstarV, TRzeroStarV, deltaMV, thetaMV,
	deltaThetaV, omegaV, RbarV, T0yT0xV, R0yR0xV, PRzeroStarV,
	UxpV, UyOpenV, UypV, UsV, MxsV, MypV, MxpV, MypOpenV, AV, 
	P0barV, T0barV, R0barV,
	MxOpenV, MyOpenV, PbarOpenV, PyPxOpenV, kV, p2p1V, M1V, mDotV,
	thetaV, shockLocationV, UsOpenV, M1M2V, UypTxOV, FnBV, T0V, PV,
	M2V, TxV, TyV, TyTxV, MxGV, MxpMypPOV, MxpMyPOV, MxpMypPCV };

//#   define machV 710 // name of variable  Mach 
//#   define fldV 711 // name of variable  fld 
//#   define p2p1FLDV 702 // name of variable  p2p1 
//#   define angleV 713 // angle depends on the implimentaion class 
//#   define M1ShockV 714//variable's name M1 both sides shock limiting case
//#   define FLDShockV 715 //variable's name FLD with shock in the middle 
//#   define M1fldV 716 //variable's name FLD with shock in the middle 
//#   define M1fldP2P1V 717 //variable's name FLD with shock in the middle 
//#   define MxV 718 // Mx or My
//#   define MyV 712 // Mx or My
//#   define MFLDshockLocationV 719 // the shock location  
//#   define PbarV 720 // pressure ratio
//#   define AstarV 721 // area ratio A/A^*
//#   define PARV 722 // AP over P0P^* 
//#   define P0yP0xV 723 // Py0 over Px0 
//#   define TbarV 724 // pressure ratio
//#   define TRstarV 725 // temperature ratio
//#   define TRzeroStarV 726 // total temperature ratio
//#   define deltaMV 727 // oblique shock delta and Mach
//#   define thetaMV 728 // oblique shock theta and Mach
//#   define deltaThetaV 729 // oblique shock theta and delta
//#   define omegaV 730 // oblique shock theta and Mach
//#   define RbarV 731 // density ratio
//#   define T0yT0xV 732 // T0y over T0x 
//#   define R0yR0xV 733 // R0y over R0x 
//#   define PRzeroStarV 734 // total temperature ratio
//#   define UxV 735 // moving shock uptream velocity
//#   define UyV 736 // moving shock uptream velocity
//#   define UsV 737 // moving shock uptream velocity
//	
//what information is sought. Note different derived class yields different
//results 
enum outputType { infoStagnation, infoStandard ,  infoTube , 
	infoShock, infoTubeShock,  infoTubeProfile, infoFn,
	infoTubeShockLimits, infoObliqueMax,   infoObliqueMin ,
	infoMinimal, infoD0 , infoMax, infoIteration } ;

//#   define infoStagnation 920 // print standard (stagnation) info
//#   define infoStandard 921 // standard info for (fanno, etc) 
//#   define infoTube 922 // print tube sides for (fanno, etc) including shock
//#   define infoShock 923 // print shock sides info 
//#   define infoTubeShock 924 // print tube info shock main info
//#	define infoTubeProfile 925// the Mach number and pressure ratio profiles 
//#   define infoTubeShockLimits 926 // print tube with shock extreme cases info 
//#   define infoObliqueMax 927 // prameters associated max Oblique 
//#   define infoObliqueMin 928 // prameters associated mininum Oblique 
//#   define infoMinimal 929 // prameters associated mininal information 
//#   define infoD0 930 // just to check D at delta 0  
//#   define infoMax 931 // additional information i.e. prameters ratios    

// variables number 

enum  InputType{kN, m1N, fldN, p2p1N, m2N, mN, shockLocationN,  MxN,
		MxpN,  MyN, MypN, PbarN, TbarN, TRstarN, TRzeroStarN, PRzeroStarN,
		AstarN, PARN,  P0yP0xN, R0yR0xN, thetaN, deltaMN, angleN,
		RbarN, UyN, UxN, UsN, TyTxN, TxN, T0yT0xN  };

//extern int  kN 	 ; // k 
//extern int  m1N 	 ; // M1 
//extern int  fldN 	; // location of fld in the array 
//extern int  p2p1N 	 ; // location of p2/p1 in the array 
//extern int  m2N  	 ; // M2 
//extern int  mN 	 ; // location of M in the array 
//extern int  shockLocationN 	 ; // location of shock in the tube 
//extern int  MxN 	 ; // Mx
//extern int  MyN 	 ; // My
//extern int  PbarN 	 ; // P over P_0
//extern int  TbarN 	 ; // T over T_0
//extern int  TRstarN 	 ; // T over T^*
//extern int  TRzeroStarN 	 ; // T0 over T0^*
//extern int  PRzeroStarN 	 ; // P0 over P0^*
//extern int  AstarN	 ; // A over A^*
//extern int  PARN	 ; // PAR 
//extern int  P0yP0xN	 ; // Py0 over Px0 
//extern int  T0yT0xN	 ; // Ty0 over Tx0 
//extern int  R0yR0xN	 ; // Ty0 over Tx0 
//extern int  thetaN	 ; // Oblique shock angle & Mach 
//extern int  deltaMN	 ; // Oblique deflection angle   & Mach 
//extern int  angleN	 ; // Prandlt-Meyer angle nu
//extern int  RbarN 	 ; // R over R_0 or "similar"
//extern int  UyN 	 ; // moving shock y side 

//data info output in array
enum DataInfo { whatInfoN,	 variableNameN,  isTexN,  isRangeN,
	classNameN, isBothBranchesN}	 ; // 

enum FlowName{ fannoFlow , stagnationFlow, rayleighFlow, isothermalFlow,
	  obliqueFlow, shockFlow, pmFlow, isothermalNozzleFlow,
		shockDynamicsFlow }; 


//precition of the results 
extern int precision;

// variables number for the tube profile
extern int numberNodes ; // number of nodes in tube
extern int  fldP ;  // location of fld in the array 
extern int  MxP ;  ; // location of Mx in the array 
extern int  MyP ;  // location of My in the array 
extern int  PyPxP  ; // 
extern int  PxPsP  ; // 
extern int  PyPsP  ; // 
extern int  P1PiP  ; // 

#endif  /* _DEFINITION_STDLIB */
