/********************************************************
	file: oblique.h

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/
#ifndef _OBLIQUE_STDLIB
#define _OBLIQUE_STDLIB

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip> 
#  include "definition.h"
#  include "compressibleFlow.h"

class oblique: public discontinuity {
        protected:
			//local functions and veriables to be access via other
			//functions 
			double  a1, a2, a3 ,Mx2, Mx4, sinDelta, cosDelta, R, Q, D, S ;
    		double x1, x2, x3, chi;
    		int Rsign;
			int isObliquePossible, isDknown; 
			int  calTheta(void) ;
			void calNormalComponent(void);
        public:
			double Myn[2]; //My normal components weak and strong accordently
			double Mxn[2]; //Mx normal components weak and strong accordently 
			double maxDelta; //max Delta for given Mx 
			double minMx; //minimum Mx for given delta and theta 

			// to change the initialization later on
			// so that base class initialization will be considered
			oblique(double spesificHeatRatio ):
				//initialization of base class
				discontinuity (spesificHeatRatio, obliqueFlow )
					 { 
						//specific initialization process for derived class
						//the critical M is ? for oblique flow
						M1maxSub = 1.0;
						singularPoint = M1maxSub ; 
						isObliquePossible = yes;
						isDknown = no;
			  		};
			~oblique(){};
			//spesific functions of this model of flow to calculate
			//spesific parameters of this flow model.
			// 
            void setMach(double Mach) {Mx = Mach ;};
			void setDelta(double dd) {delta = dd;};
			void calMach( inputType, double * ) ;
			int  calTheta(inputType, double * ) ;
			void calDelta();
			void calM1n(); //menikoff's eq
			void calD();
			double getD(){return D;};
			void calMy() ;
			double calMy(double);
			//void calVelocityRatio();
			//void calTotalTemperatureRatio();
            void makeTexTableHead();
            void makeTexTableLine ();
            void calMx (inputType, double *) ;
			int calAll(inputType, double *);
            int  doAllLines(int, inputType, double *);
            int  calTableLine (inputType, double * );
			void showResults();
			void showHead() ;
            inputType selectVariable (inputType)  ;
			void calMaxDelta(inputType, double *) ;
			void calMinDelta(inputType, double *) ;
			int calMinMx();
			int calMxForDeltaTheta();


			//functions that do not touch the class variables
			//double calFLD(double) ;
			//double calPressureRatio(double) ;
			//double calDensityRatio(double);
			//double calVelocityRatio(double);
			//double calTotalPressureRatio(double) ;
			//double calTotalTemperatureRatio(double);
			//double calTemperatureRatio(double);
			//double calArea(double);
            //double getPressure(){return PyPx ; };
            //double getTemperatue (){return TyTx ; };
            //double getDensity (){return RyRx ; };
            //double getArea (){return Area ; };

};
#endif  /* _OBLIQUE_STDLIB */
