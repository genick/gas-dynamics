/********************************************************
	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/
#ifndef _ISOTHERMAL_STDLIB
#define _ISOTHERMAL_STDLIB

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip> 
#  include "definition.h" 
#  include "compressibleFlow.h"
#  include "pipeFlow.h"
#  include "shock.h"


class isothermal : public pipeFlow {
        protected:
        public:
			isothermal(double spesificHeatRatio ):
					//inilazation of base class
				//compressibleFlow(Mach,spesificHeatRatio)
				pipeFlow (spesificHeatRatio, isothermalFlow)
					 { 
						// specific initialization process for derived class
						//the speed of the chocked flow is 1 for fanno flow
						M1maxSub = 1./ sqrt(spesificHeatRatio);
						singularPoint = M1maxSub ; 
						superSonicFLD = 0.821508116;
			  		};
			~isothermal(){};
			//spesific functions of this model of flow to calculate
			//spesific parameters of this flow model.
			//void calMach( int, double * ) ;
			void calFLD();
			void calPressureRatio() ;
			void calDensityRatio();
			void calVelocityRatio();
			void calTotalPressureRatio() ;
			void calTotalTemperatureRatio();
			void calArea();
			//functions that will not touch the class variables
			double calFLD(double) ;
			double calPressureRatio(double) ;
			//double calDensityRatio(double);
			//double calVelocityRatio(double);
			//double calTotalPressureRatio(double) ;
			//double calTotalTemperatureRatio(double);
			//double calArea(double);

			// specific functions to fanno flow to be add in presenting
			// the results
			void showHead();
			void showResults();
			void makeTexTableHead(); 
			void makeTexTableLine ();
			int calTableLine ( inputType , double * );
			int calAll( inputType, double *);
			int doAllLines ( int, inputType, double *);
            virtual double calMForFLD ( double, double );

};

#endif  /* _ISOTHERMAL_STDLIB */
