/********************************************************
	FILE:oblique.cc

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  oblique wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and oblique wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip>
#  include "stagnation.h"
#  include "externalFun.h"
#  include "oblique.h" 
#  include "gasDynamics.h" 
using namespace std;

/// oblique class definition 
//******************************************************************

void    oblique::showResults() {
    int numberVariables;
    if (outputInfo == infoStandard && isObliquePossible == yes  ) {
        numberVariables = 8;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx ;
        variableToBePrinted[1] = Myn[1];
        variableToBePrinted[2] = Myn[0];
        variableToBePrinted[3] = theta[1]*180./PI;
        variableToBePrinted[4] = theta[0]*180./PI ;
        variableToBePrinted[5] = delta*180./PI;
        variableToBePrinted[6] = D;
        variableToBePrinted[7] = P0yP0x;

        showVariable(variableToBePrinted, numberVariables);
		return;
    }
    else if (outputInfo == infoMax && isObliquePossible == yes  ) {
        numberVariables = 7;
		// only weak shock for now
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx ;
        variableToBePrinted[1] = Myn[0];
        variableToBePrinted[2] = theta[0]*180./PI ;
        variableToBePrinted[3] = delta*180./PI;
        variableToBePrinted[4] = PyPx;
        variableToBePrinted[5] = TyTx;
        variableToBePrinted[6] = P0yP0x;

        showVariable(variableToBePrinted, numberVariables);
		return;
    }
    else  if (outputInfo == infoMinimal ) {
		        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx ;
        variableToBePrinted[1] = Myn[0];
        variableToBePrinted[2] = theta[0]*180./PI ;
        variableToBePrinted[3] = delta*180./PI;
        variableToBePrinted[4] = PyPx;
        variableToBePrinted[5] = TyTx;
        variableToBePrinted[6] = P0yP0x;
                                                                                
        showVariable(variableToBePrinted, numberVariables);
        return;
	}
    else  if (outputInfo == infoObliqueMax ) {
        numberVariables = 4;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx  ;
        variableToBePrinted[1] = Myn[0];
        variableToBePrinted[2] = maxDelta* 180./PI ;
        variableToBePrinted[3] = theta[1]* 180./PI;

        showVariable(variableToBePrinted, numberVariables);
		return;
	}
    else  if (outputInfo == infoObliqueMin ) {
        numberVariables = 4;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx  ;
        variableToBePrinted[1] = My ;
        variableToBePrinted[2] = delta* 180./PI ;
        variableToBePrinted[3] = theta[0]* 180./PI;

        showVariable(variableToBePrinted, numberVariables);
        return;
    }
    else  if (outputInfo == infoD0 ) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx  ;
        variableToBePrinted[1] = D ;
        variableToBePrinted[2] = theta[0]* 180./PI;

        showVariable(variableToBePrinted, numberVariables);
        return;
    }
	else if (outputInfo == infoStandard && isObliquePossible == no  ) {
		cout << "Mx=" << Mx 
			<< " delta=" << delta*180./PI ;  
		cout << " :: No oblique shock possible. " << endl ;
		if (delta ==0.0) {
			cout << " For normal shock My =" << Myn[1];
		}
		cout  << endl ;
		return ;
	}
};

void    oblique::showHead(){
    cout << endl << endl <<  "Oblique Shock" << "\t\t"  ;
    cout << "k = " << k << endl << endl ;
                                                                               
    if (outputInfo == infoStandard ) {
        int variableNumber = 8;
        char * variableNames[variableNumber];
        variableNames[0] = "Mx  ";
        variableNames[1] = "My_s";
        variableNames[2] = "My_w";
        variableNames[3] = "Theta_s";
        variableNames[4] = "Theta_w";
        variableNames[5] = "detla";
        variableNames[6] = "D";
        variableNames[7] = "P0yP0x";
                                                                               
        showVariableHead( variableNames, variableNumber);
    }
    else if (outputInfo == infoMax ) {
        int variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = "Mx  ";
        variableNames[1] = "My_w";
        variableNames[2] = "Theta_w";
        variableNames[3] = "detla"; 
        variableNames[4] = "Pbar" ; 
        variableNames[5] =  "Tbar" ; 
        variableNames[6] = "P0yP0x"; 
        //variableNames[7] = "P0yP0x";
                                                                               
        showVariableHead( variableNames, variableNumber);
    }
    else if (outputInfo == infoMinimal ) {
        int variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = "Mx  ";
        variableNames[1] = "My_w";
        variableNames[2] = "Theta_w";
        variableNames[3] = "detla";
        variableNames[4] = "Pbar" ; 
        variableNames[5] =  "Tbar" ; 
        variableNames[6] = "P0yP0x"; 
                                                                               
        showVariableHead( variableNames, variableNumber);
    }
	else  if (outputInfo == infoObliqueMax ) {
        int variableNumber = 4;
        char * variableNames[variableNumber];
		cout << "The maximum values are:" << endl;
        variableNames[0] = "Mx  ";
        variableNames[1] = "My";
        variableNames[2] = "delta";
        variableNames[3] = "theta";

        showVariableHead( variableNames, variableNumber);
    }
    else  if (outputInfo == infoObliqueMin ) {
        int variableNumber = 4;
        char * variableNames[variableNumber];
        cout << "The values at minimum delta are:" << endl;
        variableNames[0] = "Mx  ";
        variableNames[1] = "My";
        variableNames[2] = "delta";
        variableNames[3] = "theta";

        showVariableHead( variableNames, variableNumber);
    }
    else  if (outputInfo == infoD0 ) {
        int variableNumber = 3;
        char * variableNames[variableNumber];
        cout << "The values at zero delta are:" << endl;
        variableNames[0] = "Mx  ";
        variableNames[1] = "D";
        variableNames[2] = "theta";

        showVariableHead( variableNames, variableNumber);
    }

};

void  oblique::calMach(inputType variableName, double * variableValue){
    if (variableName == MxV) {
        Mx = variableValue[MxV];
    }
	else if (variableName == deltaThetaV) {
		M = 2. *(1.0/tan(theta[0]) + tan(delta))/
			(sin (2.* theta[0]) - tan(delta)*
					(k + cos (2.*theta[0]))) ;
		if (M > 0.) {
			M = sqrt(M);
			Mx = M *sin(theta[0]);
		}
		else {
		//no solution 
			
		}
	}
};

void  oblique::calMx(inputType variableName, double * variableValue){
	double tmpMx;
    if (variableName == P0yP0xV) {
        P0yP0x = variableValue[P0yP0xV];
        M = root  ( calP0yP0xEstmt, this, M1maxSub, 100.0, P0yP0x) ;
		Mx = M * sin (theta[0]);
		return;
    }
	else if (variableName == MxV){ 
		Mx = variableValue[MxV] ;
		calMy();
		if ( Mx < 1.0) {
			tmpMx = Mx ;
			Mx = My;
			My = tmpMx;
		}
	}
    else if (variableName == machV){
        Mx = variableValue[machV] ;
        calMy();
        if ( Mx < 1.0) {
            tmpMx = Mx ;
            Mx = My;
            My = tmpMx;
        }
    }
}

void  oblique::calMy(){
    My = sqrt((Mx*Mx + 2.0/(k -1.0)) /
        ((2.0*k / (k-1.0) ) *Mx*Mx -1.0)) ;
};

double  oblique::calMy(double m){
    return  sqrt((m*m + 2.0/(k -1.0)) / ((2.0*k / (k-1.0) ) *m*m -1.)) ;
};

void  oblique::calDelta(void){
	double a, b, sinTheta, sinTheta2;
	// double c;
	double Mx2;
	//theta[0] = 50.0*PI  /180. ; 
    sinTheta= sin(theta[0]);
	sinTheta2 = sinTheta*sinTheta;
    //Mx = 2.0;
    Mx2 = Mx*Mx;
	a = tan (theta[0]) *(0.5*(k+1.)*Mx2/(Mx2*sinTheta2 -1.) -1.0 ) ;
	if ( a <= 0.0 ){
		b = -1.;
		cout << "there is no possible solution for this theta "
			<< "or Mx" << endl;
	}
	else {
		b = atan(1./a) ;
	}
	//c = b* 180. /PI ;
	//delta = c;
	delta = b;
	return;
};

int  oblique::calTheta(inputType variableName, double * variableValue){
	if (variableName == deltaMV) {
		delta  = PI * variableValue[deltaMV] /180. ;
		Mx = variableValue[MxV] ;
		calTheta();
		return yes; 
    }
	else if (variableName == thetaMV) {
		theta[0]  = PI * variableValue[thetaV] /180. ;
		calMinMx();
		Mx = variableValue[MxV] ;
		if (minMx > Mx ){
			cout <<"no possible solution!! too low Mx or change Theta"<< endl;
			cout << "The minimum Mx is "<< minMx << endl;
			return no;
		}
		calDelta ();
		return yes; 
    }
    else if (variableName == deltaThetaV) {
		theta[0]  = PI * variableValue[thetaV] /180. ;
		delta =  PI * variableValue[deltaMV] /180. ;
		return yes;
	}
	else if ( variableName == MxV){
		Mx = variableValue[MxV];
		calM1n();
		calDelta();
		return yes;
	}
	else { // default if not regular variable try delta and Mx
		variableName = deltaMV;
        delta  = PI * variableValue[deltaMV] /180. ;
        Mx = variableValue[MxV] ;
        calTheta();
        return yes;
	}
};

void  oblique::calD(){
	Mx2 = Mx*Mx;
	Mx4 = Mx2*Mx2;
	double tmpDelta;
	tmpDelta = delta;
	sinDelta= sin(delta);
	cosDelta= cos(delta);
	a1 = - (Mx2 + 2.0 )/Mx2 - k * sinDelta*sinDelta;
	a2 = (2.0*Mx2 +1.) /Mx4 + (0.25*(k+1.)*(k+1.) + (k -1.)/Mx2 )* 
				sinDelta*sinDelta  ;
	a3  = - cosDelta*cosDelta / Mx4;   
	R = (9.0 * a1*a2 - 27*a3 - 2.0 *a1*a1*a1)/ 54.0; //change 54 to multiply
	Q = ( 3.*a2- a1*a1) / 9.0 ;
	D = Q*Q*Q + R*R ;
	if (isDknown == no) {
		D = Q*Q*Q + R*R ;
	}
	if (outputInfo == infoObliqueMin || outputInfo == infoD0 ){
		return ;
	}
	else if (delta == 0.) {
		D = 0.0;
		return;
	}
};

int   oblique::calTheta(void){
	//int sign;
	if (delta == 0.0){
		theta[0]= theta[1] = 0.0;
		isObliquePossible = no;
		return no;
	}
	calD();
	if (D <= 0){
		chi = acos ( R / sqrt(-Q*Q*Q));
		x1 = asin(sqrt(2.*sqrt(-Q)*cos(THIRD*chi) - THIRD*a1)) ;
		x2 = asin(sqrt(2.* sqrt(-Q)* cos (THIRD*(chi + 2.*PI) ) 
					- THIRD*a1)) ;
		x3 = asin(sqrt(2.* sqrt(-Q)* cos (THIRD*(chi + 4.0*PI) ) 
					- THIRD*a1)) ;
		theta[1] = x1 ; //strong solution
		x2 = x2 ; //not possible solution
		theta[0] = x3 ; // weak solution
		isObliquePossible = yes;
		return yes;
	}
	else {
		isObliquePossible = no;
		return no;
	}	
} ;

inputType oblique::selectVariable (inputType variableName) {
    if  (variableName == deltaMV  ) { return deltaMV ; }  
    if  (variableName == MxV  ) { return MxV ; }  
	else { return _noV;};

} ;

void    oblique::makeTexTableHead() {
                                                                               
    int variableNumber;
	if (isObliquePossible == no) {
		cout << " no oblique shock is possible for this case" << endl ;
		return ;
	}
                                                                               
    if (outputInfo == infoStandard || outputInfo == infoShock) {
        variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = "M_x";
        variableNames[1] = "{M_y}_s";
        variableNames[2] = "{M_y}_w";
        variableNames[3] = "\\theta_s";
        variableNames[4] = "\\theta_w";
        variableNames[5] = "\\delta ";
        variableNames[6] = "{P_0}_y \\over {P_0}_x  ";

        showVariableTeXHead(variableNames,variableNumber);
		return ;
    }
    else if (outputInfo == infoMax && isObliquePossible == yes  ) {
        variableNumber = 7;
		// only weak shock for now
		char * variableNames[variableNumber];
		variableNames[0] = "M_x";
        variableNames[1] = "{M_y}_{max}";
        variableNames[2] = "\\theta_{max}";
        variableNames[3] = "\\delta ";
        variableNames[4] = "{P_y} \\over {P_x}  ";
        variableNames[5] = "{T_y} \\over {T_x}  ";
		variableNames[6] = "{P_0}_y \\over {P_0}_x  ";

		showVariableTeXHead(variableNames,variableNumber);
		return;
    }
    else if (outputInfo == infoMinimal ) {
        variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = "M_x";
        variableNames[1] = "{M_y}_w";
        variableNames[2] = "\\theta_w";
        variableNames[3] = "\\delta ";
        variableNames[4] = "{P_y} \\over {P_x}  ";
        variableNames[5] = "{T_y} \\over {T_x}  ";
		variableNames[6] = "{P_0}_y \\over {P_0}_x  ";

        showVariableTeXHead(variableNames,variableNumber);
		return ;
    }
    else  if (outputInfo == infoObliqueMax ) {
        int variableNumber = 4;
        char * variableNames[variableNumber];
        cout << "The maximum values are:" << endl;
        variableNames[0] = "M_x";
        variableNames[1] = "M_y";
        variableNames[2] = "\\delta_{max}";
        variableNames[3] = "\\theta_{max}";

        showVariableTeXHead ( variableNames, variableNumber);
    }

	
}

void    oblique::makeTexTableLine() {
    int numberVariables;

    if (isObliquePossible == no) {
        cout << "no oblique shock is possible for this case" << endl ;
        return ;
    }

    if (outputInfo == infoStandard || outputInfo == infoShock) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx ;
        variableToBePrinted[1] = Myn[1];
        variableToBePrinted[2] = Myn[0];
        variableToBePrinted[3] = theta[1]*180./PI;
        variableToBePrinted[4] = theta[0]*180./PI ;
        variableToBePrinted[5] = delta*180./PI;
        variableToBePrinted[6] = P0yP0x;

        showVariableTeXLine (variableToBePrinted, numberVariables);
    }
    else if (outputInfo == infoMax && isObliquePossible == yes  ) {
        numberVariables = 7;
		// only weak shock for now
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx ;
        variableToBePrinted[1] = Myn[0];
        variableToBePrinted[2] = theta[0]*180./PI ;
        variableToBePrinted[3] = delta*180./PI;
        variableToBePrinted[4] = PyPx;
        variableToBePrinted[5] = TyTx;
        variableToBePrinted[6] = P0yP0x;

        showVariableTeXLine(variableToBePrinted, numberVariables);
		return;
    }
    else if (outputInfo == infoMinimal ) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx ;
        variableToBePrinted[1] = Myn[0];
        variableToBePrinted[2] = theta[0]*180./PI ;
        variableToBePrinted[3] = delta*180./PI;
        variableToBePrinted[4] = PyPx;
        variableToBePrinted[5] = TyTx;
        variableToBePrinted[6] = P0yP0x;
                                                                                
        showVariableTeXLine (variableToBePrinted, numberVariables);
    }

	else  if (outputInfo == infoObliqueMax ) {
        numberVariables = 4;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx;
        variableToBePrinted[1] = Myn[0];
        variableToBePrinted[2] = delta*180./PI;
        variableToBePrinted[3] = theta[0]*180./PI;

        showVariableTeXLine ( variableToBePrinted, numberVariables);
    }

}

void oblique::calNormalComponent(void){
	for (int i = 0 ; i < 2 ; i++) {
		Mxn[i] = Mx*sin(theta[i]);
		if (theta[i] == 0.0 ){
			Myn[i] = calMy(Mx);
		}
		else {
			Myn[i] = calMy(Mxn[i])/sin((theta[i]-delta));
		}
	}
	P0yP0x = calTotalPressureRatio( Mxn[0] );
	return;
};

int   oblique::calAll(inputType variableName,
						double * variableValue ){
	if ( calTheta(variableName, variableValue) == no ){
		return no;
	} 
	//calMinMx();
	if  (variableName == deltaThetaV) {
		calMach(variableName, variableValue) ;
	}
	else if (variableName == MxV) {
		//all is done	
	}
	else if (variableName != deltaMV)  {
		calDelta() ;
	}
	calNormalComponent();
		
	//calAreaRatio();
	if (variableName != PbarV){ 
		My = Myn[0];
		calPressureRatio();
	}
	if (variableName != TbarV){
		My = Myn[0];
		calTemperatureRatio();
	}
//	calTotalPressureRatio();
	calDensityRatio ();
	return yes;
}

int oblique:: calTableLine (inputType variableName,
            double * variableValue){
	if (outputInfo == infoStandard || outputInfo == infoMinimal ){
		return calAll( variableName,variableValue);
	}
	else if (outputInfo == infoMax ) {
		return calAll(variableName,variableValue);
	}
	else if (outputInfo == infoObliqueMax ) {
		calMaxDelta(variableName, variableValue );
		return yes;
	}
    else if (outputInfo == infoObliqueMin ) {
        calMinDelta(variableName, variableValue );
        return yes;
    }
    else if (outputInfo == infoD0 ) {
		Mx = variableValue[machV];
		delta =0.0 ;
		calD ();
        //calMinDelta(variableName, variableValue );
        return yes;
    }

    return no;
};

int oblique::calMxForDeltaTheta(){
	double overMx2, sinTheta, sinTheta2, tanTheta, cotDelta;
	if (theta[0] < delta) {
		return no;
	}
	else{
		sinTheta = sin(theta[0]) ;
		sinTheta2 = sinTheta * sinTheta;
		tanTheta = tan (theta[0]) ;
		cotDelta = 1./tan(delta) ;
		overMx2 = sinTheta2 - 0.5*(k+1)* (tanTheta/(tanTheta+cotDelta));
		double tmpMx;
		Mx = sqrt(1./overMx2);
		tmpMx = Mx;
		minMx = Mx ; //for min calculation of Mx
		tmpMx += 0.;
		return yes;
	}
}

int oblique::calMinMx()
{
	//select small angle 
	delta = 0.1*PI/180. ;
	//theta[0] =  19.*PI /180.;
	calMxForDeltaTheta(); 
	return yes;
}

void oblique::calMaxDelta(inputType variableName,
             double * variableValue ){
	Mx = variableValue[MxV]; 
	//double tmpDelta;
	delta =  root ( calMaxDeltaEstmt, this, 0.001, PI*0.25 , Mx);
	do { // garentee negative D
		calD();
		delta = delta - 0.05 *EPS ;
	} while ( D >= 0.0 ) ;
	maxDelta = delta ;
	double maxDeltaTMP;
	maxDeltaTMP =  delta *180./PI;
	isDknown = yes;
	//calD();
	//D = 0 ;
	calTheta();
	calNormalComponent();
	//Mxn[0] = Mx* sin(
	//Myn[0] = calMy(Mxn[0])/sin((theta[0]-delta));

};

void oblique::calM1n(){ //Menikoff's eq
	double M1n;
	double Mx2=Mx*Mx;
	//Mx2 *(k+1)*(M^2+1) = 2*(k*M^4 +2*M^2 - 1) // menikoff's eq itself
	double aaa;
	aaa = sqrt((Mx2*((Mx2*((k*(k + 2.)) + 1.)) + (8.*((k*k) - 1.)))) +
				(16.*(1. + k))) ;
	M1n =  sqrt( 0.25*Mx2 + (0.25* (Mx2 +  aaa ) - 1.)/k ) ;
	theta[0] = asin (M1n/Mx);
}

void oblique::calMinDelta(inputType variableName,
             double * variableValue ){
	double tmpDelta;
	Mx = variableValue[machV]; 
    tmpDelta =  root ( calMinDeltaEstmt, this, 0.0,  0.01,  Mx);

	delta = tmpDelta + EPS ;
	calTheta();
	calMy() ;
	delta = tmpDelta;

};

int   oblique::doAllLines (int isTex,
	inputType variableName, double * variableValue ){
	int howManyPoints ;
	if (outputInfo == infoStandard ){
		calMaxDelta(variableName, variableValue);
		howManyPoints =  (int)  maxDelta ;
		makeLines(isTex,howManyPoints, 0.0, (double) howManyPoints,
								 variableName, variableValue);
		makeLines(isTex,1, maxDelta, maxDelta,
							 variableName, variableValue);
	}
	else if (outputInfo == infoObliqueMax) {
		howManyPoints = 9 ;
		 makeLines(isTex,howManyPoints, 1.1,  2.0,
                                 variableName, variableValue);
		howManyPoints = 9 ;
		 makeLines(isTex,howManyPoints, 2.2,  4.0,
                                 variableName, variableValue);
		howManyPoints = 5 ;
		 makeLines(isTex,howManyPoints, 5.0,  10.0,
                                 variableName, variableValue);
	}
    else if (outputInfo == infoD0) {
        howManyPoints = 9 ;
         makeLines(isTex,howManyPoints, 1.1,  2.0,
                                 variableName, variableValue);
        howManyPoints = 9 ;
         makeLines(isTex,howManyPoints, 2.2,  4.0,
                                 variableName, variableValue);
        howManyPoints = 5 ;
         makeLines(isTex,howManyPoints, 5.0,  10.0,
                                 variableName, variableValue);
    }


    return yes;
}
