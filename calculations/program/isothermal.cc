/********************************************************
	FILE:isthermal.cc

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip>
#  include "stagnation.h"
#  include "externalFun.h"
#  include "isothermal.h" 
#  include "gasDynamics.h" 
using namespace std;

/*isothermal functions */
//******************************************************************
void 	isothermal::makeTexTableHead() {

    int variableNumber;

	if (outputInfo == infoStandard) {
		variableNumber = 6;
		char * variableNames[variableNumber];
		variableNames[0] = "M";
		variableNames[1] = "4fL \\over D";
		variableNames[2] = "P \\over P^{*}";
		variableNames[3] = "P_0 \\over {P_0}^{*}";
		variableNames[4] = "\\rho \\over \\rho^{*}";
		variableNames[5] = "T_0 \\over {T_0}^{*}";

		showVariableTeXHead(variableNames,variableNumber);

	}
	else if (outputInfo == infoTube) { 
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "M_1";
        variableNames[1] = "M_2";
        variableNames[2] = "4fL \\over D";
        variableNames[3] = "\\left.{ 4fL \\over D }\\right|_{1}";
        variableNames[4] = "\\left.{ 4fL \\over D }\\right|_{2}";
        variableNames[5] = "P_2 \\over P_1";

        showVariableTeXHead(variableNames,variableNumber);
	}
	else if  (outputInfo == infoTubeProfile) {
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "M_1";
        variableNames[1] = "M_2";
        variableNames[2] = "4fL \\over D";
        variableNames[3] = "\\left.{ 4fL \\over D }\\right|_{1}";
        variableNames[4] = "\\left.{ 4fL \\over D }\\right|_{2}";
        variableNames[5] = "P_2 \\over P_1";
                                                                               
                                                                               
        showVariableTeXHead(variableNames,variableNumber);
    }
    else if  (outputInfo == infoTubeShock) {
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "M_1";
        variableNames[1] = "M_2";
        variableNames[2] = "\\left.{ 4fL \\over D }\\right|_{up}";
        variableNames[3] = "\\left.{ 4fL \\over D }\\right|_{down}";
        variableNames[4] = "M_x";
        variableNames[5] = "M_y";

        showVariableTeXHead(variableNames,variableNumber);
    }
    else if (outputInfo == infoTube) {
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "M_1";
        variableNames[1] = "M_2";
        variableNames[2] = "4fL \\over D";
        variableNames[3] = "\\left.{ 4fL \\over D }\\right|_{1}";
        variableNames[4] = "\\left.{ 4fL \\over D }\\right|_{2}";
        variableNames[5] = "P_2 \\over P_1";
        showVariableTeXHead(variableNames,variableNumber);
    }

};

void    isothermal::showHead(){
    cout << endl << endl <<  "Isothermal flow" << "\t\t"  ;
    cout << "k = " << k << endl << endl ;
                                                                               
                                                                               
    if (outputInfo == infoStandard) {
        int variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "M  ";
        variableNames[1] = "fld";
        variableNames[2] = "P/P*";
        variableNames[3] = "P0/P0*";
        variableNames[4] = "R/R*";
        variableNames[5] = "U/U*";
        //variableNames[6] = "T/T*";

        showVariableHead( variableNames, variableNumber);
    }
    else if (outputInfo == infoTube) {
        int variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "M1";
        variableNames[1] = "M2";
        variableNames[2] = "fLD";
        variableNames[3] = "fLD1";
        variableNames[4] = "fLD2";
        variableNames[5] = "P2/P1";

        showVariableHead( variableNames, variableNumber);
    }
};

void    isothermal::showResults() {
    int numberVariables;
    if (outputInfo == infoStandard){
        numberVariables = 6;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M ;
        variableToBePrinted[1] = fld;
        variableToBePrinted[2] = PRstar;
        variableToBePrinted[3] = PRzeroStar;
        variableToBePrinted[4] = RRstar ;
        variableToBePrinted[5] = URstar;
        //variableToBePrinted[6] = TRstar;
                                                                               
        showVariable(variableToBePrinted, numberVariables);
                                                                             
    }
    else if (outputInfo == infoTube) {
        numberVariables = 6;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M1;
        variableToBePrinted[1] = M2;
        variableToBePrinted[2] = fld;
        variableToBePrinted[3] = fld1;
        variableToBePrinted[4] = fld2;
        variableToBePrinted[5] = p2p1;
                                                                               
        showVariable(variableToBePrinted, numberVariables);
                                                                               
    }
};

void 	isothermal::makeTexTableLine() {

    int numberVariables;

	if (outputInfo == infoStandard) {
        numberVariables = 6;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M ;
        variableToBePrinted[1] = fld;
        variableToBePrinted[2] = PRstar;
        variableToBePrinted[3] = PRzeroStar;
        variableToBePrinted[4] = RRstar ;
        variableToBePrinted[5] = TRzeroStar;
		showVariableTeXLine (variableToBePrinted, numberVariables);
	}
	else if (outputInfo == infoTube) {
		numberVariables = 6;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M1 ;
        variableToBePrinted[1] = M2;
        variableToBePrinted[2] = fld;
        variableToBePrinted[3] = fld1;
        variableToBePrinted[4] = fld2 ;
        variableToBePrinted[5] = p2p1;

		showVariableTeXLine (variableToBePrinted, numberVariables);
	}
};


void  isothermal::calTotalTemperatureRatio(){
    TRzeroStar = (2.*k / (3.*k - 1.) ) *
        (1. + (0.5 * (k -1. ) )* M*M ) ;
};

void  isothermal::calPressureRatio(){
    // calculate the pressure and density ratio
    PRstar =  1. / sqrt(k)/M ;
    Pbar = PRstar;
};

void  isothermal::calDensityRatio(){
    // calculate the pressure and density ratio
    RRstar =  1. / sqrt(k)/M ;
    Rbar = RRstar;
};

void  isothermal::calVelocityRatio() {
    // calculate the velocity ratio 
	if (M > 0.0 ){
    	URstar =  1. / sqrt(k)/M ;
	}
	else if( M < 0.0 ){
		URstar = -1.0; 
	}
	else {
		URstar = infinity; 
	}
};

double  isothermal::calPressureRatio(double MM){
    // calculate the pressure and density ratio
    return  (1. / sqrt(k)/MM) ;
};

void  isothermal::calTotalPressureRatio(){
    if (isFlowReverse == no ) {
        PRzeroStar =  (pow((2.*k/(3.*k -1.)),(k/(k -1.)))/sqrt(k))*
                pow((1. + (0.5*(k-1.))*M*M),(k/(k-1.)))/M ;
    }
    else {
        return ;
    }
};

void  isothermal::calArea(){
    Area =  pow((( 1. + (0.5*( k - 1.)  ) * M*M)/(0.5*(k+1.))),
            ((k+1.) / 2./(k-1.) ))
                / M;
};

void  isothermal::calFLD(){
    if (M < .00) {
        //flow is reversed ?
        fld = -1.0 ;
        isFLDknown= no;
        isSubSonic=no;
        isFlowReverse = yes;
        return;
    }
    else if (M > M1maxSub  ) {
        //supersonic flow
        fld = (1 -k *M *M ) / k /M/ M + log (k * M * M);
        isSubSonic=no;
        isFLDknown = no;
        isFlowReverse = no;
        return;
    }
    else if (M < 0.0000001 ) {
        // for case almost incompressible flow
		//unfinished
        fld = M ;
        isSubSonic=yes;
        isFLDknown = yes;
        isFlowReverse = no;
        return;
    }
    else {
    // the subsonic flow and ``normal'' range
        fld = (1. -k *M *M ) / k /M/ M + log (k * M * M) ;
        isSubSonic=yes;
        isFLDknown = yes ;
        isFlowReverse = no;
    }
};

double  isothermal::calFLD(double MM){
    if (MM < .00) {
        return -1.0;
    }
    else if (MM > M1maxSub  ) {
        //supersonic flow
        //return -1.;
        return (1. -k *MM *MM ) / k /MM/ MM + log (k * MM * MM) ;
    }
    else if (MM < 0.0000001 ) {
        // for case almost incompressible flow
		//unfinished
        return MM ;
    }
    else {
    // the subsonic flow and ``normal'' range
        return (1. -k *MM *MM ) / k /MM/ MM + log (k * MM * MM) ;
    }
};

int isothermal:: calTableLine (inputType variableName,
            double * variableValue){
    if (outputInfo == infoStandard ){
        return calAll( variableName,variableValue);
    }
    else  {
        return no;
    }
};

int   isothermal::calAll( inputType variableName,
						double * variableValue ){
																				
	calMach(variableName, variableValue);
	if (variableName != fldV ){
		calFLD();
	}
	calPressureRatio();
	calVelocityRatio();
	calTotalPressureRatio();
	calTotalTemperatureRatio() ;
//  if (variableName != TbarV){
//      calTemperatureRatio();
//  }
//  calTotalPressureRatio();
    return yes;
}

int   isothermal::doAllLines (int isTex, inputType variableName,
             double * variableValue ){
    int howManyPoints ;
	double sp;
	sp = singularPoint;
    if (outputInfo == infoStandard ){
        howManyPoints =  7 ;
        makeLines(isTex,howManyPoints, 0.03, 0.1,
                                 variableName, variableValue);
        howManyPoints =  12 ;
        makeLines(isTex,howManyPoints, 0.2, 0.8,
                                 variableName, variableValue);
        howManyPoints =  4 ;
        makeLines(isTex,howManyPoints, 0.81, sp,
                                 variableName, variableValue);
    }
    return yes;
}

double  isothermal::calMForFLD(double FLD, double mach){
    //what do when there is no solution ?
	if ((mach < singularPoint) && (FLD > EPS ) ) {// sub sonic flow
		return  root ( calFLD_Estmt, this, .00001, (M1maxSub-EPS), FLD);
	}
	else if ((mach > singularPoint )  && (FLD > EPS ) ){//supersonic flow
		double L0, L1;
		L0 = calFLD(5.);
		L1 = calFLD(10.);
		if (FLD < L0 )
			return root(calFLD_Estmt, this,(M1maxSub+EPS), L0, FLD );
		else if (FLD < L1) 
			return root(calFLD_Estmt, this,L0, L1, FLD );
		else // FLD > 1000. error less 0.001%
			return sqrt(exp(FLD)/k);
	}
	else if (FLD == 0.0 )  {
		return 0.0;
	}
	else { // no possible negative fld
		return -1.0;
	}
};

