/********************************************************
	FILE:pm.cc Prandlt-Meyer function

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  pm wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and pm wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip>
#  include "stagnation.h"
#  include "externalFun.h"
#  include "pm.h" 
#  include "gasDynamics.h" 
using namespace std;

/// pm class definition 
//******************************************************************

void    pm::showResults() {
    int numberVariables;
    if (outputInfo == infoStandard   ) {
        numberVariables = 6;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M ;
        variableToBePrinted[1] = nu*180./PI;
        variableToBePrinted[2] = Pbar;
        variableToBePrinted[3] = Tbar;
        variableToBePrinted[4] = Rbar;
        variableToBePrinted[5] = atan(M)*180./PI;

        showVariable(variableToBePrinted, numberVariables);
		return;
    }
};

void    pm::showHead(){
    cout << endl << endl <<  "Prandtl Meyer Function " << "\t\t"  ;
    cout << "k = " << k << endl << endl ;
                                                                               
    if (outputInfo == infoStandard ) {
        int variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "M  ";
        variableNames[1] = "Nu";
        variableNames[2] = "Pbar";
        variableNames[3] = "Tbar";
        variableNames[4] = "Rbar";
        variableNames[5] = "Mu";
                                                                               
        showVariableHead( variableNames, variableNumber);
    }
};

void  pm::calMach(inputType variableName, double * variableValue){
    if (variableName == machV) {
        M = variableValue[machV];
    }
    else if (variableName == angleV) {
        M =  root ( calNuEstmt, this, 1.0001, 100. , nu);
    }
	else if (variableName == PbarV) {
        M =  root ( calPbarEstmt, this, 1.0001, 100. , Pbar);
    }
	else if (variableName == TbarV) {
        M =  root ( calTbarEstmt, this, 1.0001, 100. , Tbar);
    }
	else if (variableName == RbarV) {
        M =  root ( calRbarEstmt, this, 1.0001, 100. , Rbar);
    }

};

int  pm::calNu(inputType variableName, double * variableValue){
	if (variableName == machV) {
		M = variableValue[machV] ;
		calNu();
		return yes; 
    }
	else if  (variableName == angleV){
		double tmpNu ;
		nu = variableValue[angleV]*PI/180. ;
		tmpNu = nu;
		calMach( variableName, variableValue);
		return yes; 
	}
	else if  (variableName == PbarV){
        Pbar = variableValue[PbarV] ;
        calMach( variableName, variableValue);
		calNu();
        return yes;
    }
	else if  (variableName == TbarV){
		Tbar = variableValue[TbarV] ;
		calMach( variableName, variableValue);
		calNu();
		return yes;
    }
	else if  (variableName == RbarV){
		Rbar = variableValue[RbarV] ;
		calMach( variableName, variableValue);
		calNu();
		return yes;
    }
	return no;
};

int   pm::calNu(void){
	nu = sqrt( (k+1.0) / (k-1.0) ) *atan (sqrt(  (k-1.0)* 
			(M*M -1.)/ (k+1.0) ))  -  atan (sqrt(M*M -1.));
	return yes;	
} ;

inputType pm::selectVariable (inputType variableName) {
    if  (variableName == machV  ) { return machV ; }  
    if  (variableName == angleV  ) { return angleV ; } //for nu  
	else { return _noV;};

} ;

void    pm::makeTexTableHead() {
                                                                               
    int variableNumber;
                                                                               
    if (outputInfo == infoStandard  ) {
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "M";
        variableNames[1] = "\\nu";
        variableNames[2] = "P \\over P_0";
        variableNames[3] = "T \\over T_0";
        variableNames[4] = "\\rho \\over \\rho_0";
        variableNames[5] = "\\mu ";

        showVariableTeXHead(variableNames,variableNumber);
		return ;
    }
	
}

void    pm::makeTexTableLine() {
    int numberVariables;


    if (outputInfo == infoStandard ) {
        numberVariables = 6;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M ;
        variableToBePrinted[1] = nu*180./PI;
        variableToBePrinted[2] = Pbar;
        variableToBePrinted[3] = Tbar;
        variableToBePrinted[4] = Rbar;
        variableToBePrinted[5] = atan(M)*180./PI ;

        showVariableTeXLine (variableToBePrinted, numberVariables);
    }

}

void pm::calNormalComponent(void){
	return;
};

int   pm::calAll(inputType variableName,
				 double * variableValue ){
	
    //calMach(variableName, variableValue); 
    calNu(variableName, variableValue); 
	calPressureRatio();
	calTemperatureRatio();
	calDensityRatio ();
//	if (variableName != TbarV){
//		calTemperatureRatio();
//	}
//	calTotalPressureRatio();
	return yes;
}

int pm:: calTableLine (inputType variableName,
            double * variableValue){
    if (outputInfo == infoStandard ){
            return calAll(variableName,variableValue);
    }
    return no;
};

void pm::calMaxDelta(inputType variableName,
             double * variableValue ){
};

int   pm::doAllLines (int isTex, inputType variableName,
             double * variableValue ){
	int howManyPoints ;
	if (outputInfo == infoStandard ){

		howManyPoints =  9 ;
		makeLines(isTex,howManyPoints, 1.0, 1.9,
								 variableName, variableValue);
		howManyPoints =  8 ;
		makeLines(isTex,howManyPoints, 2., 10.,
							 variableName, variableValue);
	}
    return yes;
}
