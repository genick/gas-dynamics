/********************************************************
	FILE:pipeFlow.cc

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip>
#  include "stagnation.h"
#  include "externalFun.h"
#  include "pipeFlow.h" 
#  include "gasDynamics.h" 
using namespace std;

// pipeFlow functions 
//****************************************************************
void  pipeFlow::calMach(inputType variableName, double * variableValue){
    if (variableName == machV) {
        M = variableValue[machV];
		isFlowReverse = no;
    }
    else if (variableName == M1fldV ) {
        fld = variableValue[fldV] ;
        M1 = variableValue[M1V];
		isFLDknown = true ;
	}
    else if (variableName == fldV) {
        fld = variableValue[fldV] ;
        M = calMForFLD(variableValue[fldV],variableValue[machV]) ;
        if ( M > 0 && M <= M1maxSub){
            isFLDknown= yes;
            isSubSonic=yes;
            isFlowReverse = no;
        }
    }
    else if (variableName == PRzeroStarV) {
        PRzeroStar = variableValue[PRzeroStarV];
        M = variableValue[machV];
		if ( M > 1. ) 
        	M = root(calPRzeroStarEstmt,this,M1maxSub,300.,PRzeroStar);
		else  if (M < 1. ) 
        	M = root(calPRzeroStarEstmt,this,.00001, M1maxSub,PRzeroStar);
		else 
			M = 1.0;
    }
    else if (variableName == PbarV) {
        Pbar = variableValue[PbarV];
		if (Pbar > 1. ) 
        	M = root  ( calPbarEstmt, this, .00001,  M1maxSub, Pbar) ;
		else  if (Pbar < 1. ) 
        	M = root  ( calPbarEstmt, this,  M1maxSub, 400., Pbar) ;
		else 
			M = 1.0;
    }
    else if (variableName == TbarV) {
        Tbar = variableValue[TbarV];
		if  ( Tbar > 0.5*(k +1))  {// no solution possible
			M = - 1.0;
			return ; 
		}
		else if ( Tbar == 1.2 )
			M = 0. ;
		else if (Tbar > 1. )
        	M = root  ( calTbarEstmt, this, .0000,  M1maxSub, Tbar) ;
        else  if (Tbar < 1. )
            M = root  ( calTbarEstmt, this,  M1maxSub, 400., Tbar) ;
        else // == 1.0 
            M = 1.0;
    }
	else if (variableName == RbarV) {
		Rbar = variableValue[RbarV];
		if  ( Rbar < sqrt((k-1.)/(k+1.)) )  {// no solution possible
			M = - 1.0;
			return ;
		}
		else if (Rbar > 1. )
			M = root  ( calRbarEstmt, this, EPS,  M1maxSub, Rbar);
		else  if (Rbar < 1. )
			M = root  ( calRbarEstmt, this,  M1maxSub, 400., Rbar) ;
		else // == 1.0
			M = 1.0;
    }
    else if (variableName == TRstarV) {
        TRstar = variableValue[TRstarV];
        M = root  ( calTRstarEstmt, this, .00001,  M1maxSub, TRstar) ;
    }
    else if (variableName == TRzeroStarV) {
        TRzeroStar = variableValue[TRzeroStarV];
        M = root  ( calTRzeroStarEstmt, this, EPS,  500., TRzeroStar) ;
    }
	else if (variableName == M1ShockV) {
		M1 = variableValue[M1V];
		fld = variableValue[fldV] ;
	}
};

void  pipeFlow::calM1(int variableName, double * variableValue){
	if (variableName == M1fldV){
		M1 =   variableValue[M1V];
		fld = variableValue[fldV];
	}
	else if (variableName == p2p1FLDV){
        fld = variableValue[fldV];
		p2p1 = variableValue[p2p1V];
 		if (calM1forFLDp2p1(variableValue[fldV],variableValue[p2p1V]) == yes){
		}
	}
	else if (variableName ==  M1ShockV) {
        M1 = variableValue[M1V];
        fld = variableValue[fldV] ;
	}
};


double  pipeFlow :: calMForFLD(double FLD, double mach){
    //what do when there is no solution ?
    if ((mach < singularPoint) && (FLD > EPS ) ) {// sub sonic flow
        return  root ( calFLD_Estmt, this, .00001, (M1maxSub-EPS), FLD);
    }
    else if ((mach > singularPoint )  && (FLD > EPS ) ){//supersonic flow
        return  root( calFLD_Estmt, this, (M1maxSub+EPS), 10000., FLD );
    }
    else if (FLD == 0.0 )  {
        return 0.0;
    }
    else { // no possible negative fld
        return -1.0;
    }
};

