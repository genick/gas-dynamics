#include <iostream>
#include <vector>
#include <functional>
#include <algorithm>

#include <fstream>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/vector.hpp>

using namespace std;

class Vertex {
private:
  // Code for serialization
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, unsigned int version)
  {
    ar & x;
    ar & y;
  }
  // End code for serialization
  double x;
  double y;
public:
  Vertex() {} // Serialization requires a default ctor!
  ~Vertex() {}
  Vertex(double newX, double newY) : x(newX), y(newY) {}
  double getX() const { return x; }
  double getY() const { return y; }
  void dump() {
    cout << x << " " << y << endl;
  }  
};
void delete_vertex(Vertex *v) { delete v; }

class Polygon {
private:
  vector<Vertex *> vertices;
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version)
  {
    ar & vertices;
  }
public:
  ~Polygon() {
    for_each(vertices.begin(), vertices.end(), delete_vertex);
  }
  void addVertex(Vertex *v) {
    vertices.push_back(v);
  }
  void dump() {
    for_each(vertices.begin(), vertices.end(), mem_fun(&Vertex::dump));
  }
};
void delete_poly(Polygon *p) { delete p; }

class Drawing {
private:
  vector<Polygon *> polygons;
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version)
  {
    ar & polygons;
  }
public:
  ~Drawing() {
    for_each(polygons.begin(), polygons.end(), delete_poly);
  }
  void addPolygon(Polygon *p) {
    polygons.push_back(p);
  }
  void dump() {
    for_each(polygons.begin(), polygons.end(), mem_fun(&Polygon::dump));
  }
};

string getFileOpen() {
  // In real life, this would
  // call a FileOpen dialog of sorts
  return "c:/myfile.grp";
}

string getFileSaveAs() {
  // In real life, this would
  // call a FileSave dialog of sorts
  return "c:/myfile.grp";
}

void saveDocument(Drawing *doc, const string &filename) {
  ofstream ofs(filename.c_str());
  boost::archive::text_oarchive oa(ofs);
  oa << *doc;
  ofs.close();
}

Drawing *openDocument(const string &filename) {
  Drawing *doc = new Drawing();
  std::ifstream ifs(filename.c_str(), std::ios::binary);
  boost::archive::text_iarchive ia(ifs);
  ia >> *doc;
  ifs.close();
  return doc;
}

int main()
{
/*  Polygon *poly1 = new Polygon();
  poly1->addVertex(new Vertex(0.1,0.2));
  poly1->addVertex(new Vertex(1.5,1.5));
  poly1->addVertex(new Vertex(0.5,2.9));
  
  Polygon *poly2 = new Polygon();
  poly2->addVertex(new Vertex(0,0));
  poly2->addVertex(new Vertex(0,1.5));
  poly2->addVertex(new Vertex(1.5,1.5));
  poly2->addVertex(new Vertex(1.5,0));
  
  Drawing *draw = new Drawing();
  draw->addPolygon(poly1);
  draw->addPolygon(poly2);
  
  // Demonstrate saving a document
  saveDocument(draw, getFileSaveAs());

  // Demonstrate opening a document
  string filename2 = getFileOpen();
  Drawing *doc2 = openDocument(getFileOpen());
  doc2->dump();

  return 0;*/
  
  Polygon *poly1 = new Polygon();
  poly1->addVertex(new Vertex(0.1,0.2));
  poly1->addVertex(new Vertex(1.5,1.5));
  poly1->addVertex(new Vertex(0.5,2.9));
  
  Polygon *poly2 = new Polygon();
  poly2->addVertex(new Vertex(0,0));
  poly2->addVertex(new Vertex(0,1.5));
  poly2->addVertex(new Vertex(1.5,1.5));
  poly2->addVertex(new Vertex(1.5,0));
  
  Drawing *draw = new Drawing();
  draw->addPolygon(poly1);
  draw->addPolygon(poly2);
  
  // Demonstrate saving a document
  saveDocument(draw, getFileSaveAs());

  // Demonstrate opening a document
  string filename2 = getFileOpen();
  Drawing *doc2 = openDocument(getFileOpen());
  doc2->dump();

  delete draw;
  return 0;  
}
