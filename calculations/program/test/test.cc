using namespace std;
                                                                                
#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip>
                                                                                

class Vertex {
private:
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version)
  {
    ar & x;
    ar & y;
  }
  double x;
  double y;
public:
  Vertex() {} // Serialization requires a default constructor
  Vertex(double newX, double newY) : x(newX), y(newY) {}
  double getX() const { return x; }
  double getY() const { return y; }
  void dump() {
    cout << x << " " << y << endl;
  }  
}; 


int main( void )
{
	Vertex v1(1.5, 2.5);
	std::ofstream ofs("myfile.vtx");
	boost::archive::text_oarchive oa(ofs);
	oa << v1;
	ofs.close();
	return 1;
}
