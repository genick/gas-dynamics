/********************************************************
	FILE:stagnation.cc

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip> 
#  include "stagnation.h" 
#  include "isothermalNozzle.h" 
#  include "externalFun.h" 

using namespace std;

// the stagnation class functions definitions
//**********************************************//

int stagnation:: calTableLine ( inputType variableName,
            double * variableValue){
    if (outputInfo == infoStandard || outputInfo == infoFn){
		return calAll( variableName,variableValue);
    }
	else if  (variableName == FnBV) {
		isFnBKnown = yes; 
		calMach ( variableName,variableValue);
		return yes;
	}
	else  {
    	return no;
    }
};
                                                                               
int   stagnation::calAll( inputType variableName,
             double * variableValue ){
        calMach(variableName, variableValue);
        calAreaRatio();
        calPressureRatio();
        if (variableName != TRstarV){
            calTemperatureRatio();
        }
        calTotalTemperatureRatio(); //towork on 
        calTotalPressureRatio(); //towork on 
        calDensityRatio ();
        calPARRatio();
		calImpulse();
		calFn() ;
		calFnB () ;
		calRhat() ;

        return yes ;
}

void  stagnation::calMach(inputType variableName, double * variableValue){
	if (variableName == machV) {
		M = variableValue[machV];
	}
	else if (variableName == PbarV) {
		Pbar = variableValue[PbarV];
		double tmpM, tmpPbar;
		tmpPbar = Pbar;
		tmpM =  variableValue[machV];
		if (tmpM < 1.0 ) {
			M = root  ( calPbarEstmt, this, .00001,  M1maxSub, Pbar) ; 
		}
		else{ 
			 M = root  ( calPbarEstmt, this, 1.0,  100., Pbar) ;
		}
	}
	else if (variableName == TbarV) {
		Tbar = variableValue[TbarV];
		if (Tbar > (2.0/(k+1.)) ) {// subsonic
			M = root  ( calTbarEstmt, this, .00001,  M1maxSub, Tbar) ; 
		}
		else if ( Tbar == (2.0/(k+1.)) ) 
			M = 1.0 ;
		else if (Tbar < 0.0001) 
			M =  sqrt( 2.0 / (k -1.) / Tbar); 
		else{ // other wise normal range supersonc 
			 M = root  ( calTbarEstmt, this, 1.0,  100., Tbar) ;
		}
	}
	else if (variableName == RbarV) {
		Rbar = variableValue[RbarV];
		if (Rbar > pow((2.0/(k+1.)), (1. /(k -1.))) ) {// subsonic
			M = root  ( calRbarEstmt, this, .00001,  M1maxSub, Rbar) ; 
		}
		else if ( Rbar == pow((2.0/(k+1.)), (1. /(k -1.))) ) 
			M = 1.0 ;
		else if (Rbar < 0.0001) 
			M =  sqrt( 2.0 / (k -1.) / Rbar); 
		else{ // other wise normal range supersonc 
			 M = root  ( calRbarEstmt, this, 1.0,  120., Rbar) ;
		}
	}
	else if (variableName == AstarV) {
        Area = variableValue[AstarV];
		if ( variableValue[machV] <1. ) {
        	M = root  ( calAstarEstmt, this, .00001,  M1maxSub, Area) ;
		}
		else {
        	M = root  ( calAstarEstmt, this, M1maxSub, 100., Area) ;
		}
    }
	else if (variableName == PARV) {
		PAR = variableValue[PARV];
        if ( PAR > 1. ) {
			M = root  ( calPAREstmt, this, .00001,  M1maxSub, PAR) ;
		}
		else {
            M = root  ( calPAREstmt, this, 1.00001,  100. , PAR) ;
		}
	}
	else if (variableName == FnBV) {
	if (isFnBKnown == no ) {
		P = variableValue[PV];
		T0 = variableValue[T0V];
		mDot = variableValue[mDotV];
		A = variableValue[AV];
	}
	else {
		FnB = variableValue[FnBV];
	}
		calFnB () ;
		calRhat() ;
		calMforRhat();
		calFn() ;
		outputInfo = infoFn;
	}
};

void  stagnation::calPressureRatio(){
	Pbar =  pow (1. / ( 1. + (( k - 1.) / 2. ) * M*M),(k / (k - 1.) ) ) ;
};

double   stagnation::calPressureRatio(double MM){
    return  pow (1. / ( 1. + (( k - 1.) / 2. ) * MM*MM),(k / (k - 1.) ) ) ;
};


void  stagnation::calImpulse(){
	FRstar = PAR * (1.0 + k * M*M)/(1.0+k) ;	
};

void  stagnation::calTemperatureRatio(){
	double tmpTbar;
	Tbar = 1. / ( 1. + (( k - 1.) / 2. ) * M*M);
	tmpTbar= Tbar;
};

void  stagnation::calDensityRatio(){ 
	 Rbar = pow ( Pbar, (1./k)) ;
};
void  stagnation::calAreaRatio(){
	    Area =  pow((( 1.0 + (( k - 1.0) / 2. ) * M*M)/((k+1.)/2.)),
            ((k+1.) / 2./(k-1.) ))
                / M;
};

void  stagnation::calPARRatio(){
	if (isPbarKnown == no) {
		calPressureRatio();
	}
	if (isAreaKnown == no ) {
		calAreaRatio();
	}
	PAR =  Area * Pbar;
	isPARknown = yes;
};

void  stagnation::calFn(){
	if (isPARknown == no ){
		calPARRatio();
	}
	Fn = k *M * pow(Tbar, - ( 0.5* (k + 1.) / (k -1.) )) ;  
};

void  stagnation::calRhat(){
	Rhat =  0.5*(1. + sqrt(1.0 + 2.*(k -1) / k* FnB)) ;
};

void  stagnation::calMforRhat(){
	Tbar =  1. /Rhat  ;
	M = sqrt (2. /(k -1.) * fabs( Rhat - 1.))  ;
};

void  stagnation::calFnB(){
	if (isFnBKnown == yes) {
		return;
	}
	else if (VN == machV){
		FnB = Fn*Fn /k / PAR /PAR  ;
	}
	else {
		double mDotA;
		mDotA = mDot / A;
		P = 100000*P ;
		FnB =  R*T0 *mDotA *mDotA / P /P  ;
	}
	isFnBKnown= yes;
	return ;
};

void stagnation::calTotalTemperatureRatio (void) {
	
} ;

void stagnation::calTotalPressureRatio (void){

} ;


void  stagnation::makeTexTableHead(){ 

    int variableNumber;

    if (outputInfo == infoStandard || outputInfo == infoStagnation ) {
        variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = "M";
        variableNames[1] = "T \\over T_0";
        variableNames[2] = "\\rho \\over \\rho_0";
        variableNames[3] = "A \\over A^{\\star}";
        variableNames[4] = "P \\over P_0";
        variableNames[5] = "A\\times P \\over A^{*} \\times P_0";
        variableNames[6] = "F \\over F^{*}";

        showVariableTeXHead(variableNames,variableNumber);
    }
    else if (outputInfo == infoFn ) {
        variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = "M";
        variableNames[1] = "Fn";
        variableNames[2] = "\\hat{\\rho} ";
        variableNames[3] = "\\left(P_0 A^{*} \\over A P \\right)^2";
        variableNames[4] =
		 "{RT_0 \\over P^2} \\left( \\dot{m}\\over A \\right)^2";
        variableNames[5] = 
		 "{1 \\over R \\rho_0 P} \\left( \\dot{m}\\over A \\right)^2";
        variableNames[6] = 
		 "{1 \\over R {\\rho_0}^2 T} \\left( \\dot{m}\\over A \\right)^2";

        showVariableTeXHead(variableNames,variableNumber);
    }
}

void  stagnation::makeTexTableLine(){ 
    int numberVariables;

    if (outputInfo == infoStandard  || outputInfo == infoStagnation ) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M ;
        variableToBePrinted[1] = Tbar;
        variableToBePrinted[2] = Rbar;
        variableToBePrinted[3] = Area;
        variableToBePrinted[4] = Pbar ;
        variableToBePrinted[5] = PAR;
        variableToBePrinted[6] = FRstar;

        showVariableTeXLine (variableToBePrinted, numberVariables);
    }
    else if (outputInfo == infoFn   ) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M ;
        variableToBePrinted[1] = Fn;
        variableToBePrinted[2] = Rhat;
        variableToBePrinted[3] = 1./PAR/PAR ;
        variableToBePrinted[4] = FnB ;
        variableToBePrinted[5] = Fn*Fn/k/Pbar;
        variableToBePrinted[6] = Fn*Fn/k/Tbar;

        showVariableTeXLine (variableToBePrinted, numberVariables);
    }
}

int stagnation::  manegerCalTableLine (int isTex, 
				inputType variableName,  double * variableValue){
	if (isTex == yes) {
		makeTexTableHead();
	}
	else{
		showHead();
	};
	if (outputInfo == infoStandard ) {
		doAllLines(isTex, variableName, variableValue);
	}
	else if ( outputInfo == infoFn ) {
		doAllLines(isTex, variableName, variableValue);
	}
	if (isTex == yes) {
		makeTexTableTail();
	};
	return yes;
};

int   stagnation::doAllLines ( int isTex,
		 inputType variableName, double * variableValue ){
    int howManyPoints ;
	if (variableName == FnBV) {
		outputInfo = infoFn ;
	}
    if (outputInfo == infoStandard ){

        howManyPoints =  2 ;
        makeLines(isTex,howManyPoints, (.0+EPS), 0.1,
                                 variableName, variableValue);
        howManyPoints =  7 ;
        makeLines(isTex,howManyPoints, 0.2, 0.9,
                                 variableName, variableValue);
        howManyPoints =  9 ;
        makeLines(isTex,howManyPoints, 1.0, 1.9,
                                 variableName, variableValue);
        howManyPoints =  16 ;
        makeLines(isTex,howManyPoints, 2., 10.,
                             variableName, variableValue);
    }
    else if ( outputInfo == infoFn){

        howManyPoints =  2 ;
        makeLines(isTex,howManyPoints, (.0+EPS), 0.1,
                                 variableName, variableValue);
        howManyPoints =  80 ;
        makeLines(isTex,howManyPoints, 0.2, 1.0,
                                 variableName, variableValue);
    }
    return yes;
}

void    stagnation::showHead(){
    cout << endl << endl <<  "adiabtic Nozzle" << "\t\t"  ;
    cout << "k = " << k << endl << endl ;

    if (outputInfo == infoStandard || outputInfo == infoStagnation ) {
        int variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = "M  ";
        variableNames[1] = "T/T0";
        variableNames[2] = "R/R0";
        variableNames[3] = "A/A*";
        variableNames[4] = "P/P0";
        variableNames[5] = "PAR";
        variableNames[6] = "F/F*";

        showVariableHead( variableNames, variableNumber);
    }
    else if (outputInfo == infoTubeProfile ) {
        int variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = "z";
        variableNames[1] = "M";
        variableNames[2] = "Uise/U^T";
        variableNames[3] = "T/T0*";
        variableNames[4] = "A/A*";
        variableNames[5] = "P/P*";
        variableNames[6] = "PAR";

        showVariableHead( variableNames, variableNumber);
    }
    else if (outputInfo == infoFn ) {
        int variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = "M";
        variableNames[1] = "Fn";
        variableNames[2] = "Rhat";
        variableNames[3] = "1/PAR^2";
        variableNames[4] = "RT0mA/P^2";
        variableNames[5] = "1/RRh0PmA";
        variableNames[6] = "1/RRh0^2TmA";

        showVariableHead( variableNames, variableNumber);
    }
}

void    stagnation::showResults() {
    int numberVariables;
    if (outputInfo == infoStandard || outputInfo == infoStagnation ) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M ;
        variableToBePrinted[1] = Tbar;
        variableToBePrinted[2] = Rbar;
        variableToBePrinted[3] = Area;
        variableToBePrinted[4] = Pbar ;
        variableToBePrinted[5] = PAR;
        variableToBePrinted[6] = FRstar;
        showVariable(variableToBePrinted, numberVariables);
    }
    else if (outputInfo == infoTubeProfile ) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        if (M < singularPoint) {
            variableToBePrinted[0] = 1.050972149 - Area* 0.052548607  ;
        }
        else {
            variableToBePrinted[0] = 0.949027851 + Area* 0.052548607 ;
        }
        variableToBePrinted[1] = M ;
		double parama = sqrt(1.+ .5*(k-1)*M*M) ;
        variableToBePrinted[2] = M/parama/URstar ;
        variableToBePrinted[3] = Tbar;
        variableToBePrinted[4] = Area;
        variableToBePrinted[5] = Pbar ;
        variableToBePrinted[6] = PAR;

        showVariable(variableToBePrinted, numberVariables);
    }
    else if (outputInfo == infoFn ) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M ;
        variableToBePrinted[1] = Fn ;
        variableToBePrinted[2] = Rhat ;
        variableToBePrinted[3] = 1./PAR/PAR;
        variableToBePrinted[4] = FnB;
        variableToBePrinted[5] = Fn*Fn/k/Pbar ;
        variableToBePrinted[6] = Fn*Fn/k/Tbar;

        showVariable(variableToBePrinted, numberVariables);
    }

}

