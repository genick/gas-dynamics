/********************************************************
	FILE: externalFun.h

	Thi cone is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/
#ifndef _EXTERNAL_FUN_STDLIB
#define _EXTERNAL_FUN_STDLIB

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip> 
#  include "definition.h" 
#  include "compressibleFlow.h"

extern double calPxpEstmt (compressibleFlow * , double , double  ) ;
extern double calP0yP0xEstmt (compressibleFlow * , double , double  ) ;
extern double calT0yT0xEstmt (compressibleFlow * , double , double  ) ;
extern double calR0yR0xEstmt (compressibleFlow * , double , double  ) ;
extern double calPAREstmt (compressibleFlow * , double , double  ) ;
extern double calAstarEstmt (compressibleFlow * c, double , double  ) ;
extern double calPbarEstmt (compressibleFlow * , double , double  ) ;
extern double calRbarEstmt (compressibleFlow * , double , double  ) ;
extern double calTbarEstmt (compressibleFlow * , double , double  ) ;
extern double calTRstarEstmt (compressibleFlow * , double , double);
extern double calTRzeroStarEstmt (compressibleFlow * , double , double);
extern double calPRzeroStarEstmt (compressibleFlow * , double , double);
extern double calShockLocationM_Estmt (compressibleFlow * , double , double);
extern double calShockLocation_Estmt (compressibleFlow * , double , double);
extern double calFLD_Estmt (compressibleFlow * ,  double , double  ) ;
extern double calP2P1_Estmt (  compressibleFlow * , double , double )  ;
extern double calMaxDeltaEstmt (  compressibleFlow * , double, double);
extern double calMinDeltaEstmt (  compressibleFlow * , double, double);
extern double calNuEstmt (  compressibleFlow * , double , double )  ;
extern double root( double f (compressibleFlow *, double , double ),
            compressibleFlow *  , double  , double , double )  ;

#endif  /* _EXTERNAL_FUN_STDLIB */
