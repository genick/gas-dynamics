/********************************************************
	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/
#ifndef _DEFINITION_STDLIB
#define _DEFINITION_STDLIB

#  include "definition.h"

int precision = 4;

//int  kN    =   0; // M1
//int  m1N    =   1; // M1
//int  fldN   =   2 ; // location of fld in the array
//int  p2p1N  =   3 ; // location of p2/p1 in the array
//int  m2N    =   4 ; // M2
//int  mN     =   5 ; // location of M in the array
//int  MxN    =   6 ; // Mx
//int  PbarN  =   7 ; // P over P_0
//int  AstarN =   8 ; // A over A^*
//int  PARN   =   9 ; // PAR
//int  P0yP0xN    =   10 ; // P0y over P0x
//int  TbarN    =   11 ; // T over T_0 
//int  TRstarN    =   12 ; // T over T^*
//int  TRzeroStarN    =   13 ; // T0 over T0^*
//int  thetaN    =   14 ; // oblique shock theta & mach 
//int  deltaMN    =   15 ; // oblique shock delta & mach
//int  angleN    =   16 ; // Prandtl-Meyer fun
//int  shockLocationN     =   17 ; // location of shock in the tube
//int  MyN     =   18 ; //My 
//int  RbarN     =   19 ; // Rbar or "similar" 
//int  T0yT0xN    =   20 ; // T0y over T0x
//int  R0yR0xN    =   21 ; // R0y over R0x
//int  PRzeroStarN    =   22 ; // P0 over P0^*
//int  UyN    =   23 ; // moving shock Uy 
//
int whatInfoN = 0;
int variableNameN=1;
int isTexN=2;
int isRangeN=3;
int classNameN=4;
int isBothBranchesN=5;



//int fannoClass = 100;
//int shockClass = 101;
//int stagnationClass = 102;
//int raylieghClass = 103;
//int isothermalClass = 104;
//int obliqueClass = 105;
//int isothermalNozzleClass = 106;
//int pmClass = 108; 

int  fldP =0 ; // location of fld in the array
int  MxP = 1 ; // location of Mx in the array
int  MyP = 2 ; // location of My in the array
int  PyPxP = 3 ; //
int  PxPsP = 4 ; //
int  PyPsP = 5 ; //
int  P1PiP = 6 ; //

//long double third= 0.33333333333333333333333333333333333333333333333333333334;
#endif  /* _DEFINITION_STDLIB  */
