/********************************************************
	FILE:rayleigh.cc

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip>
#  include "stagnation.h"
#  include "externalFun.h"
#  include "rayleigh.h" 
#  include "gasDynamics.h" 
using namespace std;


//***************************************************************************
//rayleigh class functions definitions 
//******************************
void 	rayleigh::makeTexTableLine() {
    int numberVariables;

	if (outputInfo == infoStandard) {
        numberVariables = 6;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M ;
        variableToBePrinted[1] = TRstar ;
        variableToBePrinted[2] = TRzeroStar ;
        variableToBePrinted[3] = PRstar;
        variableToBePrinted[4] = PRzeroStar ;
        variableToBePrinted[5] = RRstar;
		showVariableTeXLine (variableToBePrinted, numberVariables);
	}
	else if (outputInfo == infoTube) {
		numberVariables = 6;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M1 ;
        variableToBePrinted[1] = M2;
        variableToBePrinted[2] = fld;
        variableToBePrinted[3] = fld1;
        variableToBePrinted[4] = fld2 ;
        variableToBePrinted[5] = p2p1;

		showVariableTeXLine (variableToBePrinted, numberVariables);
	}
    else if (outputInfo == infoTubeShock) {
        numberVariables = 6;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M1;
        variableToBePrinted[1] = M2;
        variableToBePrinted[2] = fldUp;
        variableToBePrinted[3] = fldDown;
        variableToBePrinted[4] = Mx;
        variableToBePrinted[5] = My;

        showVariableTeXLine (variableToBePrinted, numberVariables);
    }

};

void 	rayleigh::makeTexTableHead() {

    int variableNumber;

	if (outputInfo == infoStandard) {
		variableNumber = 6;
		char * variableNames[variableNumber];
		variableNames[0] = "M";
		variableNames[1] = "T \\over T^{*}";
		variableNames[2] = "T_0 \\over {T_0}^{*}";
		variableNames[3] = "P \\over P^{*}";
		variableNames[4] = "P_0 \\over {P_0}^{*}";
		variableNames[5] = "\\rho^{*} \\over \\rho";

		showVariableTeXHead(variableNames,variableNumber);
	
	}
	else if (outputInfo == infoTube) { 
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "M_1";
        variableNames[1] = "M_2";
        variableNames[2] = "4fL \\over D";
        variableNames[3] = "\\left.{ 4fL \\over D }\\right|_{1}";
        variableNames[4] = "\\left.{ 4fL \\over D }\\right|_{2}";
        variableNames[5] = "P_2 \\over P_1";
                                                                                  
        showVariableTeXHead(variableNames,variableNumber);
	}
	else if  (outputInfo == infoTubeProfile) {
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "M_1";
        variableNames[1] = "M_2";
        variableNames[2] = "4fL \\over D";
        variableNames[3] = "\\left.{ 4fL \\over D }\\right|_{1}";
        variableNames[4] = "\\left.{ 4fL \\over D }\\right|_{2}";
        variableNames[5] = "P_2 \\over P_1";
                                                                               
                                                                               
        showVariableTeXHead(variableNames,variableNumber);
    }
    else if  (outputInfo == infoTubeShock) {
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "M_1";
        variableNames[1] = "M_2";
        variableNames[2] = "\\left.{ 4fL \\over D }\\right|_{up}";
        variableNames[3] = "\\left.{ 4fL \\over D }\\right|_{down}";
        variableNames[4] = "M_x";
        variableNames[5] = "M_y";

        showVariableTeXHead(variableNames,variableNumber);
    }
};

void	rayleigh::showHead(){
	
	cout << endl << endl <<  "Rayleigh flow" << "\t\t"  ;
	cout << "k = " << k << endl << endl ;

	if (outputInfo == infoStandard) {
        int variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "M  ";
        variableNames[1] = "T/T*";
        variableNames[2] = "T0/T0*";
        variableNames[3] = "P/P*";
        variableNames[4] = "P0/P0*";
        variableNames[5] = "R*/R";

        showVariableHead( variableNames, variableNumber);

	}
	else if ( outputInfo == infoTube)  {
        int variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "M1 ";
        variableNames[1] = "M2 ";
        variableNames[2] = "fld";
        variableNames[3] = "fld1";
        variableNames[4] = "fld2";
        variableNames[5] = "p2p1";
                                                                                  
        showVariableHead( variableNames, variableNumber);
	}
	else if ( outputInfo == infoShock) {
        int variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "Mx";
        variableNames[1] = "My";
        variableNames[2] = "TyTx";
        variableNames[3] = "RyRx";
        variableNames[4] = "PyPx";
        variableNames[5] = "P0yP0x";

		showVariableHead( variableNames, variableNumber);
	}
	else if (outputInfo == infoTubeShockLimits){
		int variableNumber = 3;
		char * variableNames[variableNumber];
		variableNames[0] = "M1";
		variableNames[1] = "M2upper";
		variableNames[2] = "M2lower";

		showVariableHead( variableNames, variableNumber);
	}
	else if (outputInfo == infoTubeProfile){
        int variableNumber = 7;
        char * variableNames[variableNumber];
		
        variableNames[0] = "fld";
        variableNames[1] = "Mx";
        variableNames[2] = "My";
        variableNames[3] = "Py/Px";
        variableNames[4] = "Px/P*";
        variableNames[5] = "Py/P*";
        variableNames[6] = "P1/P(i)";
		cout <<  "Fanno flow profile" << "\t"  ;
		cout << "M1 = " << M1 <<
				 "\t" << endl <<  "FLD = " <<  fld 
					<<endl << endl ;

        showVariableHead( variableNames, variableNumber);
    }
	else if (outputInfo == infoTubeShock){
        int variableNumber = 6;
        char * variableNames[variableNumber];
                                                                               
        variableNames[0] = "M1";
        variableNames[1] = "M2";
        variableNames[2] = "fldUp";
        variableNames[3] = "fldDown";
        variableNames[4] = "Mx";
        variableNames[5] = "My";
        cout <<  "Fanno flow profile" << "\t"  ;
        cout << "M1 = " << M1 <<
                 "\t" << endl <<  "FLD = " <<  fld
                    <<endl << endl ;
                                                                               
        showVariableHead( variableNames, variableNumber);
    };

};

void 	rayleigh::showResults() {
	int numberVariables;
	if (outputInfo == infoStandard){
        numberVariables = 6;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M ;
        variableToBePrinted[1] = TRstar;
        variableToBePrinted[2] =  TRzeroStar;
        variableToBePrinted[3] = PRstar;
        variableToBePrinted[4] = PRzeroStar;
        variableToBePrinted[5] = RRstar ;

        showVariable(variableToBePrinted, numberVariables);

	}
	else if (outputInfo == infoTube) {
		numberVariables = 6;
		double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M1;
        variableToBePrinted[1] = M2;
        variableToBePrinted[2] = fld;
        variableToBePrinted[3] = fld1;
        variableToBePrinted[4] = fld2;
        variableToBePrinted[5] = p2p1;

        showVariable(variableToBePrinted, numberVariables);

	}
	else if ( outputInfo == infoShock) {
		numberVariables = 6;
		double variableToBePrinted[numberVariables];
		variableToBePrinted[0] = Mx;
		variableToBePrinted[1] = My;
		variableToBePrinted[2] = TyTx;
		variableToBePrinted[3] = RyRx;
		variableToBePrinted[4] = PyPx;
		variableToBePrinted[5] = P0yP0x;
		
		showVariable(variableToBePrinted, numberVariables);
	}
	else if  ( outputInfo == infoTubeShock) {
        numberVariables = 6;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M1;
        variableToBePrinted[1] = M2;
        variableToBePrinted[2] = fldUp;
        variableToBePrinted[3] = fldDown;
        variableToBePrinted[4] = Mx;
        variableToBePrinted[5] = My;
	
		showVariable(variableToBePrinted, numberVariables);
    }
    else if  ( outputInfo == infoTubeProfile) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
		for (int j=0; j < numberNodes  ; j++){
			for (int i=0; i < numberVariables ;i++){
				variableToBePrinted[i] = Mfld[i][j];
			}

        	showVariable(variableToBePrinted, numberVariables);
		}
    }

};

int   rayleigh::doAllLines (int isTex,
			 inputType variableName, double * variableValue ){
	
	makeLines(isTex, 7,0.03,  0.1, variableName, variableValue );
	makeLines(isTex, 16,0.2,  1.0, variableName, variableValue );
	makeLines(isTex, 18, 1.1, 2.9, variableName, variableValue );
    makeLines(isTex, 14,3.,10  , variableName, variableValue );
	makeLines(isTex, 10,20.,70  , variableName, variableValue );

	return yes;
}
int rayleigh:: calTableLine (inputType variableName, 
			double * variableValue){
	if (outputInfo == infoStandard ){
            return calAll(variableName,variableValue);
	}
	return no;
};

int   rayleigh::calAll(inputType variableName,
             double * variableValue ){
        calMach(variableName, variableValue);
		if (variableName != TRstarV){
        	calTemperatureRatio();
		};
		if (variableName != TRzeroStarV){
        	calTotalTemperatureRatio();
		};
        calPressureRatio();
        calTotalPressureRatio();
        calDensityRatio ();
        return yes ;
}

void  rayleigh::calMach(int variableName, double * variableValue){
    if (variableName == machV) {
        M = variableValue[machV];
		isFlowReverse = no;
    }
    else if (variableName == fldV) {
        fld = variableValue[fldV] ;
        M = calMForFLD(variableValue[fldV],variableValue[machV]) ;
        if ( M > 0 && M <= M1maxSub){
            isFLDknown= yes;
            isSubSonic=yes;
            isFlowReverse = no;
        }
    }
    else if (variableName == PRzeroStarV) {
        PRzeroStar = variableValue[PRzeroStarV];
        M = variableValue[machV];
		if ( M > 1. ) 
        	M = root(calPRzeroStarEstmt,this,M1maxSub,300.,PRzeroStar);
		else  if (M < 1. ) 
        	M = root(calPRzeroStarEstmt,this,.00001, M1maxSub,PRzeroStar);
		else 
			M = 1.0;
    }
    else if (variableName == PbarV) {
        Pbar = variableValue[PbarV];
		if (Pbar > 1. ) 
        	M = root  ( calPbarEstmt, this, .00001,  M1maxSub, Pbar) ;
		else  if (Pbar < 1. ) 
        	M = root  ( calPbarEstmt, this,  M1maxSub, 400., Pbar) ;
		else 
			M = 1.0;
    }
    else if (variableName == TbarV) {
        Tbar = variableValue[TbarV];
        M = variableValue[machV];
        if (Tbar == 1. )
            M = 1.;
		else if ( M<  1. )
			M = root  ( calTbarEstmt, this,EPS ,  M1maxSub, Tbar);
        else if (Tbar < 0.03) // Large Mach 
			M = sqrt ( (k+1.) /k / Tbar);
		else 
            M = root  ( calTbarEstmt, this,M1maxSub, 10., Tbar); 
    }
	else if (variableName == RbarV) {
		Rbar = variableValue[RbarV];
		if  ( Rbar > (k+1.)/k )  {// no solution possible
			M = - 1.0;
			return ;
		}
		else if (Rbar == 1. )
			M = 1.0;
		else  if (Rbar < 0. )
			M = -1.0;
		else if (Rbar == 0.0 )
			M = 0.0 ;
		else  if (Rbar < 1. )
			M = root  ( calRbarEstmt, this,  EPS, 1., Rbar) ;
		else // normal supersonic situation 
			M = root  ( calRbarEstmt, this,  1., 400., Rbar) ;
    }
    else if (variableName == TRstarV) {
        TRstar = variableValue[TRstarV];
        M = root  ( calTRstarEstmt, this, .00001,  M1maxSub, TRstar) ;
    }
    else if (variableName == TRzeroStarV) {
        TRzeroStar = variableValue[TRzeroStarV];
        M = variableValue[machV];
		if (M < 1. ) 
        	M = root  ( calTRzeroStarEstmt, this, EPS, 1., TRzeroStar);
		else if (TRzeroStar < ((k+1.)*(k-1.)/k+EPS) ) 
			M = 200.;
		else if ( TRzeroStar == 1.0 )
			M = 1.0;
		else   // normal supersonic range
        	M = root (calTRzeroStarEstmt,this, M1maxSub, 20.,TRzeroStar);
    }
	else if (variableName == M1ShockV) {
		M1 = variableValue[M1V];
		fld = variableValue[fldV] ;
	}
};

void  rayleigh::calTemperatureRatio(){
	double MM;
	MM = M*M;
	TRstar = (k+1. )* (k+1. )*MM/ 
		(1. + k*MM)/(1. + k*MM)  ;
	Tbar=TRstar;
};

double  rayleigh::calTemperatureRatio(double MM){
	return 0.5* (k+1. )/ 
		(1. + (0.5*(k -1. ) *MM*MM) ) ;
};

void  rayleigh::calTotalTemperatureRatio(){
	double MM;
	MM = M*M;
	TRzeroStar = 2.*(k+1. )*MM *(1.+0.5*(k-1.)*MM)/ 
		((1. + k *M*M) *(1. + k *M*M)) ;
};

double  rayleigh::calTotalTemperatureRatio(double M){
    double MM;
    MM = M*M;
	return   2.*(k+1. )*MM *(1.+0.5*(k-1.)*MM)/
        ((1. + k *M*M) *(1. + k *M*M)) ;
};

void  rayleigh::calPressureRatio(){
	// calculate the pressure ratio
	PRstar =   (k + 1.) /( 1.+ k*M*M );
	Pbar =   PRstar;
};

double  rayleigh::calPressureRatio(double MM){
	// calculate the pressure ratio
	return (k + 1.) /( 1.+ k*M*M );
};

void  rayleigh::calVelocityRatio(){
    // calculate the velocity ratio
	URstar = M * sqrt ( 0.5 * (k+1. )/ 
		(1. + (0.5*(k -1. ) *M*M) ) ) ;
};

double  rayleigh::calVelocityRatio(double MM){
    // calculate the velocity ratio
	return MM * sqrt ( 0.5 * (k+1. )/ 
		(1. + (0.5*(k -1. ) *MM*MM) ) ) ;
};

void  rayleigh::calDensityRatio(){
    // calculate the density ratio
    RRstar =   (k+1.) *M*M / (1.+k*M*M);
};

double  rayleigh::calDensityRatio(double MM){
    // calculate the density ratio
    return  sqrt(2.*(1. + 0.5* (k -1.0) *MM*MM) /( 1.+ k))/MM ;
};

void  rayleigh::calTotalPressureRatio(){
	double MM ;
	MM = M*M;
	PRzeroStar =
		 (k + 1.) /( 1.+ k*M*M ) * pow(2. * (1. +0.5*(k-1.)*MM)/(k+1.)
				, (k /(k -1.))) ;
};
 
double  rayleigh::calTotalPressureRatio(double MM){
	return  sqrt(
			pow((2.*(1. + 0.5* (k -1.0) *MM*MM) /( 1.+ k)),
				(( k+1.0)/(k-1.0))  )
		)/MM;  
};
