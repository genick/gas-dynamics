/********************************************************
	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/
#ifndef _COMPRESSIBLEflow_STDLIB
#define _COMPRESSIBLEflow_STDLIB

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip> 
#  include "definition.h" 

class compressibleFlow{
		protected:
			//to move all the class variable to upper case
			//remove these lines when done
			int isR; //  is Range tmp untill fixed
			int isFlowReverse; // check is the direction of the flow
			int isSubSonic; // is no Shock possible  
			int isFLDknown; // check if FLD was calculated/known?  
			int isChoked;
			int isThereShock;
			int isPipeFlow;
			int isPARknown;
			int isPbarKnown ;
			int isAreaKnown ;
			int isFnBKnown;
			int isPossible;
			outputType outputInfo; // what  stuff is needed to print
			inputType VN; // variable name 
			int drivedClassName;
			double k	; // ratio of specific heat
			double fld; // the maximum length resistance 4fl/D
			double fld1; // the  fld for the tube entrance tube
			double fld2; //  the  fld for the tube exit
			double fldMAX; //  max fld for supersonic flow 
			double Area ; // relative area A/A* 
			double M1 ; // Mach number at the entrance
			double M2 ; // Mach number at the exit of the tube
			double Mfld[7][50];//tube profile 0)fld 1)Mx 2)My 3)pxpy
								//4)pxp* 5)pyp* 6) p1/p(i) 
			double steps[500][7];// for extra information on Iterations
			int numberOfIterations ;
			double M2upper ; // The tube Exit Mach number  after shock at exit
			double M2lower ;// tube Exit Mach number  after shock at entrance
			double fldUp; //the fld before the shock 
			double fldDown; //the ''left over'' fld after the shock 
			double M ;//Mach number general use i.e. isentropic relationship 
			//double TzeroRstar; // temperature ratio T0/T0*
			double PRstar ; // pressure ratio P/P* anywhere on the tube
			double P1Pstar ; // pressure ratio P1/P* entrance to the tube
			double P2Pstar ; // pressure ratio P2/P* exit of the tube
			//double PzeroRstar ; // pressure ratio P0/P0*
			double RRstar ; // relative density rho/rho*
			double URstar ; // relative velocity U/U*
			double TRstar ; // relative Temperature  T/T*
			double FRstar ; // relative Impulse  F/F*
			double TRzeroStar ; // relative Temperature  T0/T0*
			double PRzeroStar ; // relative Temperature  P0/P0*
			double p2p1; // ratio of pressure between two sides of tube p1/p2
			double Pbar ; // stagnation pressure ratio 
			double Tbar ; // stagnation temperature ratio 
			double Rbar ; // stagnation density ratio 
			double PAR ; // stagnation mass conservation "ratio" 
			double Fn ; // Fn number 
			double FnB ; // Fn Brother number 
			double Rhat ; // dimensionless density related to Fn number
			double R ; //spesific gas constant 
			double Rho ; //density 
			double Pbar1 ; // stagnation pressure ratio @ entrance p1/P1_0 
			double Pbar2 ; // stagnation pressure ratio @ exit  p2/P2_0 
			double M1maxSub; // the maximum ``subsonic''
			double singularPoint; // the similar as M1maxSub normally 1
			double superSonicFLD; // the maximum fld of supersonic branch
			double maxPossibleFLDsup;//the supersonic branch (fanno flow)
			double maxPossibleFLDsub; // the subsonic branch (fanno flow)
			double minM1exit, minM1entrance;//reverse limit constant fld
			// variables that related to the shock itself
			double alpha; // the deflection angle
			double theta[2]; // shock angle weak & strong order 
			double delta; // deflection angle oblique shock 
			double nu; // deflection angle prandtl-meyer fun 
            double TyTx; // temperature ratio Ty/Tx
            double PyPx ; // pressure ratio Py/Px
            double RyRx ; // relative density rhoY/rhox
            double P0yP0x ; // pressure ratio P0y/P0x
            double T0yT0x ; // temperature ratio T0y/T0x
            double R0yR0x ; // density ratio R0y/R0x
            double Mx ; // Mx number before the normal shock
            double My ; // My number after the normal shock
			double D ; // oblique parameter   
			double Uy ; // moving Shock y component   
			double Us ; // the moving shock velocity
			double Tx ; // upstream temperature Tx
			double Ty ; // downstream temperature Ty
			double T0 ; // stagnation temperature T0 
			double P ; // static pressure P
			double P0 ; // stagnation pressure P0
			double A ; // cross section, A
			double mDot ; // mass flow rate, \dot{m}
			double Uxp ; // upstream velocity
			double Uyp ; // upstream velocity
			double cx ; // upstream speed of sound
			double cy ; // downstream speed of sound

		public:
			// initialization of the base class
			compressibleFlow (double spesificHeatRatio, FlowName dcn){ 
                    k = spesificHeatRatio;
					R = 287.; // assume that is air constant 
 					drivedClassName = dcn ; 
					isFlowReverse = no; 
					isSubSonic = yes; 
            		isFLDknown=no;
					isPipeFlow = no;
					isPossible = yes;
			};
			// no use for destruction of the class
			virtual ~compressibleFlow (){};

			//these functions to be supplied by a specific class for
			// the specific flow model
			virtual void setMach(double Mach){M = Mach;};
			virtual void setMx(double Mach){Mx = Mach;};
			virtual void setDelta(double dd){delta = dd;};
			virtual void setAngle(double dd){nu = dd;};
			virtual void calPressureRatio(){};
			virtual double calPressureRatio(double){return 0.0;};
			virtual void calFLD (void) {};
			virtual double calFLD(double) {return 0.0;} ; // cal FLD = f( M)
			virtual void calDensityRatio (void) {};
			virtual void calTotalPressureRatio (void) {};
			virtual void calTotalDensityRatio (void) {};
			virtual double calTotalPressureRatio (double) {return 0.0;};
			virtual void calTemperatureRatio (void) {};
			virtual void calVelocityRatio (void) {};
			virtual void calTotalTemperatureRatio (void) {};
            virtual void calImpulse() {} ;
            virtual void calMy() {} ;
            virtual int  calTheta() {return no;};//shock angle oblique fun
            virtual void calDelta() {} ; // deflection oblique shock fun
            virtual void calAreaRatio () {} ;
            virtual void calPARRatio () {} ;
            virtual void calFn () {} ;
            virtual void calFnB () {} ;
            virtual void calRhat () {} ; // call diminsionless density
            virtual void calMforRhat () {} ; // call diminsionless density
            virtual void calMach (int, double *) {} ;
            virtual void calM1 (int, double *) {} ;
            virtual void calMx (int, double *) {} ;
            //virtual int calAll (int, int, double *) {return no;} ;
            virtual int calAll ( inputType, double *) {return no;} ;
            virtual int doAllLines ( int, inputType, double *)
					 {return no;} ;

            virtual inputType selectVariable (inputType) {return _noV;} ;
            virtual void  calD (int) {} ;
            virtual double calAngle (void) {return 0.0;} ;

			//calculate the new variable based on the non-standard variable 
            virtual void findMForFLD(double, double *) {};

			// obtain the values of the variables
            virtual double getD (void) {return D;} ;
            virtual double getAngle (void) {return nu;} ;
			virtual double getMach() {return M; };
			virtual double getPressure() {return Pbar; };
			virtual double getTemperature() {return Tbar; };
			virtual double getTRstar() {return TRstar; };
			virtual double getTRzeroStar() {return TRzeroStar; };
			virtual double getPRzeroStar() {return PRzeroStar; };
            virtual double getFLD (){return fld  ; };
            virtual double getArea (){return Area ; };
            virtual double getPAR (){return PAR ; };
            virtual double getM1 (){return M1 ; };
            virtual double getM2 (){return M2 ; };
			virtual double getPRstar(){return PRstar ; };
            virtual double getTzero(){return TRzeroStar; };
            virtual double getDensity (){return RRstar ; };
            virtual double getT_P_ratio (){return PRzeroStar ; };
            virtual double getP2P1 (){return p2p1 ; };
            virtual double getMy (){return My ; };
            virtual double getMx (){return Mx ; };
            virtual double getk (){return k ; };
            virtual double getmaxPossibleFLDsup (){return maxPossibleFLDsup ; };
            virtual double getP0yP0x (){return P0yP0x ; };
            virtual double getT0yT0x (){return T0yT0x ; };
            virtual double getR0yR0x (){return R0yR0x ; };

			// change the values of the variables
			void changeMach ( double Mach) {M = Mach; } ;
			
			//user interface for Tex functions
			int makeTable (outputType, int, int, inputType, double*);
					// is BasicInfo (yes, no)
					// is Range (yes, no)
					// is TeX (yes, no) 
					//variable type(M, fld,etc), 
					//their values

			//show the results with TeX
			int makeTexTableLine( inputType,double *);//variableName
								//variables themselves
			virtual void makeTexTableHead(){}; 
			virtual void makeTexTableLine(){};
			void makeTexTableTail();
			// show the results without TeX
			virtual void showHead(){} ;
			virtual void showResults(){};
			//internal functions for printing variables (base class
			//functions)
			void showVariableHead (char **, int );
			void showVariable(double *, int);
			void showVariableTeXHead(char **, int);
			void showVariableTeXLine(double *, int);


			//general functions used by all the models of flow
			int calM1forFLDp2p1(double,double);// find M1 for given fld, p2p1
			int calM1forM2(double, double, double *);// cal M1 f(fld,M2,*M1) 
			int calM2forM1(double, double, double *);// cal M2 f(fld,M1,*M2)

			//functions for managing the calculations
			virtual int calTableLine( inputType, double *);
			//virtual int calTableLine( inputType, double *) {return yes;};
			//int calTableLine( void);
			// functions to generate part table (with and without TeX)
			virtual int makeLines( int, int, double, double );
			virtual int	 setRange (inputType, int, double *, double *);
			virtual int	 makeLines(int, int,double, double, 
					inputType, double *);
			int  manegerCalTableLine(int );//manager for calculating
											//whole line of the table with
											// parameter isTex
			virtual int  manegerCalTableLine(int, inputType,  double *);
									//manager for
									// calculating whole table
									// 1. isTeX, 2. 
									//  variblelName p2p1, fld, etc,
									// 3. variableValues 
			virtual void  initianlizeTableLineCal();
			virtual void calP2P1();
			virtual double calMForFLD ( double, double );
};
#endif  /* _COMPRESSIBLEflow_STDLIB */
