// Mon Dec  4 08:57:04 CST 2006 remove whatInfo passing use as class
// veriable and add the iteration to the calculation of supersonic branch
// to find the shock location. Therefore correcting the pressure
// calculations and temperature.

/********************************************************
	FILE:fanno.cc

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip>
#  include "stagnation.h"
#  include "externalFun.h"
#  include "fanno.h" 
#  include "gasDynamics.h" 
using namespace std;


//***************************************************************************
//fanno class functions definitions 
//******************************
void 	fanno::makeTexTableLine() {
    int numberVariables;

	if (outputInfo == infoStandard) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M ;
        variableToBePrinted[1] = fld;
        variableToBePrinted[2] = PRstar;
        variableToBePrinted[3] = PRzeroStar;
        variableToBePrinted[4] = RRstar ;
        variableToBePrinted[5] = URstar;
        variableToBePrinted[6] = TRstar;
		showVariableTeXLine (variableToBePrinted, numberVariables);
	}
	else if (outputInfo == infoTube) {
		numberVariables = 6;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M1 ;
        variableToBePrinted[1] = M2;
        variableToBePrinted[2] = fld;
        variableToBePrinted[3] = fld1;
        variableToBePrinted[4] = fld2 ;
        variableToBePrinted[5] = p2p1;

		showVariableTeXLine (variableToBePrinted, numberVariables);
	}
    else if (outputInfo == infoTubeShock) {
        numberVariables = 6;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M1;
        variableToBePrinted[1] = M2;
        variableToBePrinted[2] = fldUp;
        variableToBePrinted[3] = fldDown;
        variableToBePrinted[4] = Mx;
        variableToBePrinted[5] = My;

        showVariableTeXLine (variableToBePrinted, numberVariables);
    }
    else  if (outputInfo == infoIteration ) {
        numberVariables = 5;
        double variableToBePrinted[numberVariables];
        for (int i = 0 ; i < numberOfIterations ; i++){
            variableToBePrinted[0] = steps[i][1];
            variableToBePrinted[1] = steps[i][2];
            variableToBePrinted[2] = steps[i][3];
            variableToBePrinted[3] = steps[i][4];
            variableToBePrinted[4] = fld;

            cout  << "  " << (int)  steps[i][0] << " &  ";
            showVariableTeXLine (variableToBePrinted, numberVariables);
		}
    }

};

void 	fanno::makeTexTableHead() {

    int variableNumber;

	if (outputInfo == infoStandard) {
		variableNumber = 7;
		char * variableNames[variableNumber];
		variableNames[0] = "M";
		variableNames[1] = "4fL \\over D";
		variableNames[2] = "P \\over P^{*}";
		variableNames[3] = "P_0 \\over {P_0}^{*}";
		variableNames[4] = "\\rho \\over \\rho^{*}";
		variableNames[5] = "U \\over {U}^{*}";
		variableNames[6] = "T \\over T^{*}";

		showVariableTeXHead(variableNames,variableNumber);
	
	}
	else if (outputInfo == infoTube) { 
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "M_1";
        variableNames[1] = "M_2";
        variableNames[2] = "4fL \\over D";
        variableNames[3] = "\\left.{ 4fL \\over D }\\right|_{1}";
        variableNames[4] = "\\left.{ 4fL \\over D }\\right|_{2}";
        variableNames[5] = "P_2 \\over P_1";
                                                                                  
        showVariableTeXHead(variableNames,variableNumber);
	}
	else if  (outputInfo == infoTubeProfile) {
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "M_1";
        variableNames[1] = "M_2";
        variableNames[2] = "4fL \\over D";
        variableNames[3] = "\\left.{ 4fL \\over D }\\right|_{1}";
        variableNames[4] = "\\left.{ 4fL \\over D }\\right|_{2}";
        variableNames[5] = "P_2 \\over P_1";
                                                                               
                                                                               
        showVariableTeXHead(variableNames,variableNumber);
    }
    else if  (outputInfo == infoTubeShock) {
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "M_1";
        variableNames[1] = "M_2";
        variableNames[2] = "\\left.{ 4fL \\over D }\\right|_{up}";
        variableNames[3] = "\\left.{ 4fL \\over D }\\right|_{down}";
        variableNames[4] = "M_x";
        variableNames[5] = "M_y";

        showVariableTeXHead(variableNames,variableNumber);
    }
    else if  (outputInfo == infoIteration) {
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "i";
        variableNames[1] = "\\left.{ 4fL \\over D }\\right|_{up}";
        variableNames[2] = "\\left.{ 4fL \\over D }\\right|_{down}";
        variableNames[3] = "M_x";
        variableNames[4] = "M_y";
        variableNames[5] = "{ 4fL \\over D }";

        showVariableTeXHead(variableNames,variableNumber);
    }
};

void	fanno::showHead(){
	
	cout << endl << endl <<  "Fanno flow" << "\t\t"  ;
	cout << "k = " << k << endl << endl ;

	if (outputInfo == infoStandard || outputInfo == infoMinimal) {
        int variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = "M  ";
        variableNames[1] = "fld";
        variableNames[2] = "P/P*";
        variableNames[3] = "P0/P0*";
        variableNames[4] = "R/R*";
        variableNames[5] = "U/U*";
        variableNames[6] = "T/T*";
                                                                                  
        showVariableHead( variableNames, variableNumber);

	}
	else if ( outputInfo == infoTube)  {
        int variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "M1 ";
        variableNames[1] = "M2 ";
        variableNames[2] = "fld";
        variableNames[3] = "fld1";
        variableNames[4] = "fld2";
        variableNames[5] = "p2p1";
                                                                                  
        showVariableHead( variableNames, variableNumber);
	}
	else if ( outputInfo == infoShock) {
        int variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "Mx";
        variableNames[1] = "My";
        variableNames[2] = "TyTx";
        variableNames[3] = "RyRx";
        variableNames[4] = "PyPx";
        variableNames[5] = "P0yP0x";

		showVariableHead( variableNames, variableNumber);
	}
    else  if (outputInfo == infoIteration ) {
        int variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = "i ";
        variableNames[1] = "fldU";
        variableNames[2] = "fldD";
        variableNames[3] = "Mx";
        variableNames[4] = "My";
        variableNames[5] = "fld";

        showVariableHead( variableNames, variableNumber);
    }
	else if (outputInfo == infoTubeShockLimits){
		int variableNumber = 3;
		char * variableNames[variableNumber];
		variableNames[0] = "M1";
		variableNames[1] = "M2upper";
		variableNames[2] = "M2lower";

		showVariableHead( variableNames, variableNumber);
	}
	else if (outputInfo == infoTubeProfile){
        int variableNumber = 7;
        char * variableNames[variableNumber];
		
        variableNames[0] = "fld";
        variableNames[1] = "Mx";
        variableNames[2] = "My";
        variableNames[3] = "Py/Px";
        variableNames[4] = "Px/P*";
        variableNames[5] = "Py/P*";
        variableNames[6] = "P1/P(i)";
		cout <<  "Fanno flow profile" << "\t"  ;
		cout << "M1 = " << M1 <<
				 "\t" << endl <<  "FLD = " <<  fld 
					<<endl << endl ;

        showVariableHead( variableNames, variableNumber);
    }
	else if (outputInfo == infoTubeShock){
        int variableNumber = 6;
        char * variableNames[variableNumber];
                                                                               
        variableNames[0] = "M1";
        variableNames[1] = "M2";
        variableNames[2] = "fldUp";
        variableNames[3] = "fldDown";
        variableNames[4] = "Mx";
        variableNames[5] = "My";
        cout <<  "Fanno flow profile" << "\t"  ;
        cout << "M1 = " << M1 <<
                 "\t" << endl <<  "FLD = " <<  fld
                    <<endl << endl ;
                                                                               
        showVariableHead( variableNames, variableNumber);
    };

};

void 	fanno::showResults() {
	int numberVariables;
	if (outputInfo == infoStandard || outputInfo == infoMinimal){
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M ;
        variableToBePrinted[1] = fld;
        variableToBePrinted[2] = PRstar;
        variableToBePrinted[3] = PRzeroStar;
        variableToBePrinted[4] = RRstar ;
        variableToBePrinted[5] = URstar;
        variableToBePrinted[6] = TRstar;

        showVariable(variableToBePrinted, numberVariables);

	}
	else if (outputInfo == infoTube) {
		numberVariables = 6;
		double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M1;
        variableToBePrinted[1] = M2;
        variableToBePrinted[2] = fld;
        variableToBePrinted[3] = fld1;
        variableToBePrinted[4] = fld2;
        variableToBePrinted[5] = p2p1;

        showVariable(variableToBePrinted, numberVariables);

	}
	else if ( outputInfo == infoShock) {
		numberVariables = 6;
		double variableToBePrinted[numberVariables];
		variableToBePrinted[0] = Mx;
		variableToBePrinted[1] = My;
		variableToBePrinted[2] = TyTx;
		variableToBePrinted[3] = RyRx;
		variableToBePrinted[4] = PyPx;
		variableToBePrinted[5] = P0yP0x;
		
		showVariable(variableToBePrinted, numberVariables);
	}
	else if  ( outputInfo == infoTubeShock) {
        numberVariables = 6;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M1;
        variableToBePrinted[1] = M2;
        variableToBePrinted[2] = fldUp;
        variableToBePrinted[3] = fldDown;
        variableToBePrinted[4] = Mx;
        variableToBePrinted[5] = My;
	
		showVariable(variableToBePrinted, numberVariables);
    }
    else  if (outputInfo == infoIteration ) {
        numberVariables = 5;
        double variableToBePrinted[numberVariables];
        for (int i = 0 ; i < numberOfIterations ; i++){
            variableToBePrinted[0] = steps[i][1];
            variableToBePrinted[1] = steps[i][2];
            variableToBePrinted[2] = steps[i][3];
            variableToBePrinted[3] = steps[i][4];
            variableToBePrinted[4] = fld;

            cout  << "  " << (int)  steps[i][0] << "  ";
            showVariable(variableToBePrinted, numberVariables);
        }
        return;
    }
    else if  ( outputInfo == infoTubeProfile) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
		for (int j=0; j < numberNodes  ; j++){
			for (int i=0; i < numberVariables ;i++){
				variableToBePrinted[i] = Mfld[i][j];
			}

        	showVariable(variableToBePrinted, numberVariables);
		}
    }
};

double  fanno::calFLD(double Mach){
	M=Mach;

	if (M < .00) {
		//flow is reversed ?
		if (fabs( M) >  1.0 ) {
		 isSubSonic=yes;
		}
		isFlowReverse = yes;
		return 1.0;
	}
	else if (M > 1.0 ) {
		//supersonic flow
		isSubSonic=no;
		isFLDknown = yes;
		isFlowReverse = no;
		return (1. - M *M ) / k /M/ M  + 
			0.5*(k+1.)/k *log 
					( 0.5*(k+1.) * M * M/(1.+ 0.5* (k-1.)*M*M));
	}
	else if (M < EPS ) {
		// for case almost incompressible flow
		isSubSonic=yes;
		isFLDknown = no;
		isFlowReverse = no;
		// to put the approximation formula
		// not done yet
		return M;
	}	
	else {
	// the subsonic flow and ``normal'' range
		isSubSonic=no;
		isFLDknown = no ;
		isFlowReverse = no;
        return (1. - M *M ) / k /M/ M +
            0.5*(k+1.)/k*log ( 0.5*(k+1.) * M * M/(1.+ 0.5* (k-1.)*M*M));
	}
};

void  fanno::calFLD(){

	if (M < .00) {
		//flow is reversed ?
        if (fabs( M) >  1.0 ) {
         isSubSonic=yes;
        }
		else {
			isSubSonic=no;
		}
		isFlowReverse = yes;
		return;
	}
	else if (M > 1.0 ) {
		//supersonic flow
        fld = (1. - M *M ) / k /M/ M +
            0.5*(k+1.)/k*log ( 0.5*(k+1.) * M * M/(1.+ 0.5* (k-1.)*M*M));
		isSubSonic=no;
		isFLDknown = yes;
		isFlowReverse = no;
		return;
	}
	else if (M < 0.00001 ) {
		// for case almost incompressible flow
		fld = M ;
		isSubSonic=yes;
		isFLDknown = no;
		isFlowReverse = no;
		return;
	}	
	else {
	// the subsonic flow and ``normal'' range
        fld = (1. - M *M ) / k /M/ M +
            0.5*(k+1.)/k*log ( 0.5*(k+1.) * M * M/(1.+ 0.5* (k-1.)*M*M));
		isSubSonic=no;
		isFLDknown = no ;
		isFlowReverse = no;
	}
};

void  fanno::calArea(){
	Area =  pow((( 1 + (( k - 1) / 2 ) * M*M)/((k+1)/2)), 
			((k+1) / 2/(k-1) ))
				/ M;
};
	
double  fanno::calArea(double MM){
    return  pow((( 1 + (( k - 1) / 2 ) * MM*MM)/((k+1)/2)),
            ((k+1) / 2/(k-1) ))
                / MM;
};

void  fanno::calTemperatureRatio(){
	TRstar = 0.5* (k+1. )/ 
		(1. + (0.5*(k -1. ) *M*M) ) ;
	Tbar = TRstar;
};

double  fanno::calTemperatureRatio(double MM){
	return 0.5* (k+1. )/ 
		(1. + (0.5*(k -1. ) *MM*MM) ) ;
};

void  fanno::calTotalTemperatureRatio(){
	TRzeroStar = (k+1. )/ 
		(1. + (0.5*(k -1. ) *M*M) )*0.5 ;
};

double  fanno::calTotalTemperatureRatio(double MM){
	return  (k+1. )/ 
		(1. + (0.5*(k -1. ) *MM*MM) )*0.5 ;
};

void  fanno::calPressureRatio(){
	// calculate the pressure ratio
	PRstar =   sqrt(0.5*(k + 1.) /( 1.+ 0.5*(k-1.)*M*M ))/M ;
	Pbar = PRstar; // finding the root is not problems
};

double  fanno::calPressureRatio(double MM){
	// calculate the pressure ratio
	return   sqrt(0.5*(k + 1.) /( 1.+ 0.5*(k-1.)*MM*MM ))/MM ;
};

void  fanno::calVelocityRatio(){
    // calculate the velocity ratio
	URstar = M * sqrt ( 0.5 * (k+1. )/ 
		(1. + (0.5*(k -1. ) *M*M) ) ) ;
};

double  fanno::calVelocityRatio(double MM){
    // calculate the velocity ratio
	return MM * sqrt ( 0.5 * (k+1. )/ 
		(1. + (0.5*(k -1. ) *MM*MM) ) ) ;
};

void  fanno::calDensityRatio(){
    // calculate the density ratio
    RRstar =   sqrt(2.*(1. + 0.5* (k -1.0) *M*M) /( 1.+ k))/M ;
};

double  fanno::calDensityRatio(double MM){
    // calculate the density ratio
    return  sqrt(2.*(1. + 0.5* (k -1.0) *MM*MM) /( 1.+ k))/MM ;
};

void  fanno::calTotalPressureRatio(){

	if (isFlowReverse == no ) {
		PRzeroStar =  sqrt(
			pow((2.*(1. + 0.5* (k -1.0) *M*M) /( 1.+ k)),
				(( k+1.0)/(k-1.0))  )
		)/M;
		return ;
	}
	else {
		return ;
	}
};
 
double  fanno::calTotalPressureRatio(double MM){
	return  sqrt(
			pow((2.*(1. + 0.5* (k -1.0) *MM*MM) /( 1.+ k)),
				(( k+1.0)/(k-1.0))  )
		)/MM;  
};

int fanno:: calTableLine ( inputType variableName, double * variableValue){
    if (outputInfo == infoStandard || outputInfo == infoIteration
			|| outputInfo == infoMinimal){
        return calAll( variableName,variableValue);
    }
    else  {
        return no;
    }
};

int fanno::calShockLocationForFLD (){
	double fldTMP,  fldMAXsub ;
	double fldOLD, fldDelta, fldU2;
	shock shockCla(k);
	if ( M1 < 1.0 ) { // subsonic branch no shock possible
		return no ;
	}
	fldTMP = fld ;
	M = M1;
	calFLD() ;
	fldMAX = fld;
	fld = fldTMP ; 
	fldDelta = fld;
	if ( fldMAX < fld ) {
		cout << "fld exceeded there is a shock in the tube!" << endl ;
		//check shock occurs at the entrace possible
		double Myy;
		Myy = shockCla.calMy(M);
		fldMAXsub = calFLD ( Myy );
		if (fldMAXsub <= fld){
			isPossible = no;
			cout << "this situation isn't possible" << endl ;
			cout << "The shock occures in the nozzle." << endl ;
			return no;
		}
		M2 = 1. ; //flow is chocked
		int i ;
		i = 0;
		fldDown = fld - fldMAX;
		while ( fabs ( fldDelta ) > EPS) {
			My = calMForFLD (fldDown, 0.2) ; // 0.2 -> subsonic branch
			Mx = shockCla.calMy(My);
			fldU2 = calFLD ( Mx );
			fldUp = fldMAX -  fldU2  ;
			fldDelta = fldDown - fldOLD ;
			fldOLD = fldDown ;
			fldDown =  fld - fldUp;
			if (isR == no ) {
				steps[i][0] = i;
				steps[i][1] = fldUp;
				steps[i][2] = fldDown;
				steps[i][3] = Mx;
				steps[i][4] = My;
			}
			i++;
    	}
    	numberOfIterations = i;
		M = M1;
		isThereShock = yes;
		if (outputInfo ==  infoStandard ) {
			outputInfo = infoTubeShock ;
		}
		else {
			;
		}
		return yes;
	}
	else { //no shock all supersonic flow
		fld1 = fld ;
		isSubSonic = no;
		isThereShock = no;
		return yes;
	}
};
 
int   fanno::calAll(inputType variableName,
						double * variableValue ){

	calMach(variableName, variableValue);
	if (isFLDknown == false ) {	
	}
	if (variableName == M1fldV ) {	
		//change the output if needed
		calShockLocationForFLD();
		if (isThereShock == no && isPossible == yes){// calculate the M2 and etc
			fld2 = fldMAX - fld1 ;
			M2 = calMForFLD(fld2, 2.0);
			outputInfo = infoTube ;
			//calculations for unchocked flow
			P1Pstar = calPressureRatio(fld1);
			P2Pstar = calPressureRatio(fld2);
			p2p1 = P2Pstar/ P1Pstar ;
		}
		else if ( isPossible == no) {
			return no; 
		}
	}
	else {
		if (isFLDknown == no ) {
			calFLD () ;
			//isFLDknown = yes;
		}
		calPressureRatio();
		calTemperatureRatio();
		calDensityRatio ();
		calVelocityRatio();
		calTotalPressureRatio();
	}
//  if (variableName != TbarV){
//      calTemperatureRatio();
//  }
//  calTotalPressureRatio();
    return yes;
}

int   fanno::doAllLines ( int isTex,
		 inputType variableName, double * variableValue ){
    int howManyPoints ;
    if (outputInfo == infoStandard ){
        howManyPoints =  7 ;
        makeLines(isTex,howManyPoints, 0.03, 0.1,
                                 variableName, variableValue);
        howManyPoints =  16 ;
        makeLines(isTex,howManyPoints, 0.2, 1.0,
                                 variableName, variableValue);
        howManyPoints =  8 ;
        makeLines(isTex,howManyPoints, 2.0, 10.0,
                                 variableName, variableValue);
        howManyPoints =  10 ;
        makeLines(isTex,howManyPoints, 20., 70.,
                             variableName, variableValue);
    }
    return yes;
}

