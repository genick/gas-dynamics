/********************************************************
	gasDynamics.cc

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow   
	for given 4FL/D or other info.
	The code also produces table for a specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/
using namespace std;
//int precision;
int numberNodes;

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip> 
#  include <dirent.h> 
#  include <unistd.h>
#  include "gasDynamics.h" 
#  include "definition.h" 

int main( int argc, char* argv[] )
{
	double variableValue[100];
	double k = 1.4;
	int shockLocation;
	int ioData[7];
	outputType whatInfo;
	inputType variableName; 
	int isTex = yes;
	int isRange = no;
	enum FlowName flowName;
	//resutls precision how many digits after the point.
	//int 
	;
	//precision = 4 ;

	long size;
	char *buf;
	char *ptr;
	size = pathconf(".", _PC_PATH_MAX);


	if ((buf = (char *)malloc((size_t)size)) != NULL)
		ptr = getcwd(buf, (size_t)size);

	string cwd, cwFile, argv1;
	cwd += buf;
		
	if (argc > 1 ) { 
	//if (argc == 1 ) 
		argv1 = argv[1] ;
		if ( argv1 == "-g"){
			cwFile = cwd +  "/.dammy.dammy";
			//readFile(variableValue, ioData, "./data.ini");
			cout << "now using -g option" << endl;
			cout << "working on the standard internal file" << endl;
			readFile(variableValue, ioData, cwFile);
				//"../.dammy.dammy");
				//"/mnt/aba/work/qt/gdc/.dammy.dammy");
		}
		else {
			readFile(variableValue,ioData, argv[1]);
		}
	}
	else { 
		readDefaultData(variableValue,ioData);	
	}
	// fan is the name of the actual object with k = 1.4.
	whatInfo = (outputType) ioData[whatInfoN];
	variableName = (inputType) ioData[variableNameN];
	isTex = ioData[isTexN];
	isRange = ioData[isRangeN];
	k = variableValue[kV];
	flowName = (FlowName) ioData[classNameN];
	//compressibleFlow * fanC = &fan;
	//compressibleFlow * sC = &s;
	//compressibleFlow * PG = &G;
	//compressibleFlow * Piso = &iso;
	if ( flowName == fannoFlow) {  
		//fanno fan(k) ; 
		fanno model(k) ; 
        model.makeTable(whatInfo, isRange, isTex,  variableName, variableValue);
	}
	else if ( flowName == stagnationFlow) {
		//stagnation G(k);
		stagnation model(k);
        model.makeTable(whatInfo, isRange, isTex,  variableName, variableValue);
	}
	else if ( flowName == shockFlow) {
		//shock s(k);
		shock model(k);
        model.makeTable(whatInfo, isRange, isTex,  variableName, variableValue);
	}
	else if ( flowName == shockDynamicsFlow ) {
		shockDynamics model(k);
		model.makeTable(whatInfo, isRange, isTex,  variableName, variableValue);
	}
	else if ( flowName == obliqueFlow) {
		//oblique O(k);
		oblique model(k);
		model.makeTable(whatInfo, isRange, isTex,  variableName, variableValue);
	}
	else if ( flowName == isothermalFlow) {
		//isothermal iso(k);
		isothermal model(k);
		model.makeTable(whatInfo, isRange, isTex,  variableName, variableValue);
	}
	else if ( flowName == isothermalNozzleFlow ) {
		//isothermalNozzle isoN(k);
		isothermalNozzle model(k);
		model.makeTable(whatInfo, isRange, isTex,  variableName, variableValue);
	}
	else if ( flowName == rayleighFlow ){
		//rayleigh R(k);
		rayleigh model(k);
		model.makeTable(whatInfo, isRange, isTex,  variableName, variableValue);
	}
	else if ( flowName == pmFlow ){
		//pm P(k);
		pm model(k);
		model.makeTable(whatInfo, isRange, isTex,  variableName, variableValue);
	}
	else {
		stagnation model(k);
		model.makeTable(whatInfo, isRange, isTex,  variableName, variableValue);
	}

	numberNodes = 40;
	shockLocation = 50; // number of percents

	//to build a function for setting the variables  

	
	//variableValue[shockLocationV]= ((double) shockLocation)/100.*numberNodes;
	//if  (variableValue[shockLocationV] < 1.) {
	//	variableValue[shockLocationV] = 1.;
	//}

	//the following line is an example for using the class as a pointer
	//fanC->makeTable(whatInfo,isRange, isTex, machV, M);
	//fan.makeTable(whatInfo, isRange, isTex,  variableName, variableValue);
	//s.makeTable(whatInfo, isRange, isTex,  variableName, variableValue);
	//G.makeTable(whatInfo, isRange, isTex,  variableName, variableValue);
	//iso.makeTable(whatInfo, isRange, isTex,  variableName, variableValue);
	//isoN.makeTable(whatInfo, isRange, isTex,  variableName, variableValue);
	//R.makeTable(whatInfo, isRange, isTex,  variableName, variableValue);
	//O.makeTable(whatInfo, isRange, isTex,  variableName, variableValue);
	//P.makeTable(whatInfo, isRange, isTex,  variableName, variableValue);

	return 0;
}

