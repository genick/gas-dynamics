/********************************************************

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 
	
	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2. ? 
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.
	
	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it start to proper calculation at fld> 1000. 
********************************************************/

# include <cstdio>
# include <cmath>
# include <iostream>
# include <iomanip> // what replace this file in the new system?

# 	define	PI 3.14159265358979310
# 	define	infinity   1.79769313486231570E+308 
#   define EPS  1e-6
# 	define yes  1
# 	define no  0
#   define sonic 3 // Mach = 1 
#   define machV 10 // name of variable  mach 
#   define fldV 11 // name of variable  fld 
#   define p2p1V 12 // name of variable  p2p1 
#   define M1V 13 // name of variable  p2p1 

using namespace std;

class compressibleFlow{
		protected:
			int isFlowReverse; // check is the direction of the flow
			int isSubSonic; // is no Shock possible  
			int isFLDknown; // check if FLD was calculated/known?  no?  yes
			int isBasicInfo; // check whether on basic stuff is needed
			double k	; // ratio of specific heat
			double fld; // the maximum length resistance 4fl/D
			double fld1; // the  fld for the tube entrance tube
			double fld2; //  the  fld for the tube exit
			double Area ; // relative area A/A* 
			double M1 ; // Mach number at the entrance
			double M2 ; // Mach number at the exit of the tube
			double M ;//Mach number general use such as isentropic relationship 
			double TzeroRstar; // temperature ratio T0/T0*
			double PRstar ; // pressure ratio P/P* anywhere on the tube
			double P1Pstar ; // pressure ratio P1/P* entrance to the tube
			double P2Pstar ; // pressure ratio P2/P* exit of the tube
			double PzeroRstar ; // pressure ratio P0/P0*
			double RRstar ; // relative density rho/rho*
			double URstar ; // relative velocity U/U*
			double TRstar ; // relative Temperature  T/T*
			double p2p1; // ratio of pressure between two sides of tube p1/p2
			double Pbar1 ; // stagnation pressure ratio @ entrance p1/P1_0 
			double Pbar2 ; // stagnation pressure ratio @ exit  p2/P2_0 
			double M1maxSub; // the maximum ``subsonic''
			double singularPoint; // the similar as M1maxSub normally 1
			double supperSonicFLD; // the maximum fld of suppersonic brench
			// except isothermal flow but can be used for general case
		public:
			// initialization of the class
			compressibleFlow (double spesificHeatRatio ){ 
                    k = spesificHeatRatio;
					isFlowReverse = no; 
					isSubSonic = no; 
            		isFLDknown=no;
			};
			// no use for destruction of the class
			~compressibleFlow (){};

			//these functions to supplied by specific class for
			// the specific flow model
			//virtual void calFLD() {} ;
			virtual void calPressureRatio(){};
			virtual double calFLD(double) {} ; // cal FLD = f( M)
			virtual void calDensityRatio (void) {};
			virtual void calFLD (void) {};
			virtual void calTotalPressureRatio (void) {};
			virtual void calTemperatureRatio (void) {};
			virtual void calVelocityRatio (void) {};
			virtual void calTotalTemperatureRatio (void) {};
			//calculate the new variable based on the non-standard variable 
            virtual void findMForFLD(double, double *) {};

			// obtain the values of the variables
            double getFLD (){return fld  ; };
            double getArea (){return Area ; };
            double getM1 (){return M1 ; };
            double getM2 (){return M2 ; };
			double getPRstar(){return PRstar ; };
            double getTemperatue (){return TzeroRstar ; };
            double getDensity (){return RRstar ; };
            double getT_P_ratio (){return PzeroRstar  ; };
            double getP2P1 (){return p2p1 ; };

			// change the values of the variables
			void changeMach ( double Mach) {M = Mach; } ;
			
			//user interface for Tex functions
			int makeTable (int, int, int, int, double);
					// is BasicInfo (yes, no)
					// is Range (yes, no)
					// is TeX (yes, no) 
					//variable type(M, fld,etc), 
					//its value
			int makeTable (int, int, int, double , int,  double);
					// is BasicInfo(yes, no)
					// is Range(yes, no)
					// is TeX (yes, no) 
					// fld 
					// variable kind
					// p2p1

			//show the results with TeX
			int makeTexTableLine (int , double ); // variableName, variable itself
			void makeTexTableHead(int ); //isBasicInto variable
			void makeTexTableLine(int); // 1) chocked flow and 2) two sides of tube
			void makeTexTableTail();
			// show the results without TeX
			void showHead(int );
			void showResults(int );

			//general functions used by all the models of flow
			int calM1(double,double) ; // find M1 for given fld,
									// and pressure ratio.
			int calM1forM2(double, double, double *);// find M1 f(fld,M2,*M1) 
			int calTableLine(double, int, double);
			int calTableLine(int, double);
			int calTableLine( void);
			// next function generate of part table
			int	 makeLines(int, int, int, double, double );
			int	 makeLines(int, int,int, double, double, int, double );
			int  manegerCalTableLine(int,  int );//manager for calculating
											// whole table with parameters
											// 1. isTex 2. isBasicInfo
			int  manegerCalTableLine(int, int, int, double);//manager for
									// calculating whole table
									// 1. isTeX, 2. isBasicInfo,(can be
									// used for basic table building) 
									// 3. variblelName p2p1, fld, etc,
									// 4. variableValue 
			void calP2P1();
			double calMForFLD ( double, double );
			void calM2ForFLD(double);
};


double calFLD_Estmt (compressibleFlow * c,  double estimatedMach, double  value){
	double tmpFLD;
    //c->changeMach (estimatedMach) ;
    //s.calArea();
    tmpFLD = c->calFLD(estimatedMach) ;
    //s.calPressureRatio() ;
    //s.calTotalPressureRatio() ;
    //s.calTotalTemperatureRatio() ;
    //s.calDensityRatio() ;
    return (value - tmpFLD ) ;
};

double calP2P1_Estmt (  compressibleFlow * c, double M1est, double fld) {

	double  M2est; //the value est of the mach at the exit;
	double P1PstarEst, P2PstarEst ;// the estimated p1/P*, p2/P* 
	double tmpFLD ; // estimated FLD
	double newFLD ; // the new estimated  FLD for M2
	double p2p1est;
	double p2p1Given; // the given p2/p1
	
	//calculate the P1PstarT for M1
    c->changeMach(M1est);
	c->calPressureRatio(); //calculate the pressure ratio p1/p*
    P1PstarEst  = c->getPRstar(); // assigned the ratio to a tmp variable
	
	// need to find M2
	// calculate the "leftover" of  fld to M2
	tmpFLD = c->calFLD(M1est);
	newFLD = tmpFLD - fld ;
		
	// calculate M2 
	M2est = c->calMForFLD(newFLD,M1est);
	//note that the function calculate relationship M to fld
	// not M1 or M2
	c->changeMach(M2est);
    c->calPressureRatio(); // p2/p*
    P2PstarEst= c->getPRstar() ;
    p2p1est = P2PstarEst / P1PstarEst;
	p2p1Given = c->getP2P1() ;	
	return ( p2p1est - p2p1Given ) ;	

};

double root( double f (compressibleFlow *, double , double ),
            compressibleFlow * c , double a , double b, double value) {

   double Af =  f(c,a, value) ;
  double Bf =  f(c, b, value) ;
		
                                                                                
    double m = ( a + b ) / 2.0 ;
   double Mf =  f(c, m, value ) ;
	// if root is exist
	if ( Af * Bf  > 0.0 ) {
		// no root
	//try Secant Method
		return -1.0; 
	}
                                                                                
   if ( Mf == 0.0 || (b - a ) <= EPS ) {
        return m;
    }
    else if ( Af * Mf  < 0.0 ) {
        return root (f, c, a, m, value ) ;
    }
    else {
        return root(f, c, m, b, value);
    }
}

void 	compressibleFlow::makeTexTableHead(int isBasicInfo) {

	if (isBasicInfo == yes) {
	cout << endl << endl;
	cout << "*******************************************" << endl ;
	cout << "The following stuff is the same as above/below" << endl ;
	cout << "if you use showResults with showHeads but the " << endl ;
	cout << "information is setup for the latex text processing." << endl ;
	cout << "You can just can cut and paste it in your latex file." << endl ;
	cout << "You must use longtable style file and dcolumn " << endl ;
	cout << "style files." << endl  << endl << endl;
	cout << "*******************************************" << endl ;
	cout << "\\setlongtables" << endl ;
	cout << "\\begin{longtable}" << endl ;
	cout << "{|D..{2.5}|D..{2.6}|D..{2.6}|D..{3.6}|"; // continue next line
	cout << "D..{2.6}|D..{2.6}|D..{2.6}|D..{2.6}}" << endl ;
	cout << "\\caption{ ??  \\label{?:tab:?}}\\\\"  ;
	cout << "\\hline" << endl ;
	cout << "\\multicolumn{1}{|c|} " ; // the same line
	cout << "{$\\rule[-0.1in]{0.pt}{0.3 in} \\mathbf{M} $} ";// continue  line
	cout << " & " << endl;
	cout << "\\multicolumn{1}{|c|} " ; // the same line
	cout << "{$\\mathbf{4fL \\over D} $} ";// continue  line
	cout << " & " << endl;
	cout << "\\multicolumn{1}{|c|}";
	cout << "{$  \\mathbf{P \\over P^{*} } $ } & " ;
	cout << endl ;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{P_0 \\over {P_0}^{*}} $ } & ";
	cout << endl ;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{\\rho \\over \\rho^{*} }$ } ";
	cout << endl << " &" << endl;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{U \\over {U}^{*}} $ } & ";
	cout << endl ;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{T \\over T^{*} }$ } ";
	cout << endl << "\\\\\\hline" << endl;
	cout << endl ;
	cout << "\\endfirsthead" << endl ;
	cout << "\\caption{ ?? (continue)} \\\\\\hline" << endl ;
	cout << "\\multicolumn{1}{|c|} " ; // the same line
	cout << "{$\\rule[-0.1in]{0.pt}{0.3 in} \\mathbf{M} $} ";// continue  line
	cout << " & " << endl;
	cout << "\\multicolumn{1}{|c|} " ; // the same line
	cout << "{$\\mathbf{4fL \\over D} $} ";// continue  line
	cout << " & " << endl;
	cout << "\\multicolumn{1}{|c|}";
	cout << "{$  \\mathbf{P \\over P^{*} } $ } & " ;
	cout << endl ;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{P_0 \\over {P_0}^{*}} $ } & ";
	cout << endl ;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{\\rho \\over \\rho^{*} }$ } ";
	cout << endl << " &" << endl;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{U \\over {U}^{*}} $ } & ";
	cout << endl ;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{T \\over T^{*} }$ } ";
	cout << endl << "\\\\\\hline" << endl;
	cout << "\\endhead" << endl;
	}
	else { 
	cout << endl << endl;
	cout << "*******************************************" << endl ;
	cout << "The following stuff is the same as above/below" << endl ;
	cout << "if you use showResults with showHeads but the " << endl ;
	cout << "information is setup for the latex text processing." << endl ;
	cout << "You can just can cut and paste it in your latex file." << endl ;
	cout << "You must use longtable style file and dcolumn " << endl ;
	cout << "style files." << endl  << endl << endl;
	cout << "*******************************************" << endl ;
	cout << "\\setlongtables" << endl ;
	cout << "\\begin{longtable}" << endl ;
	cout << "{|D..{2.6}|D..{2.6}|D..{2.6}|D..{3.6}|D..{2.6}|D..{2.6}|}" << endl ;
	cout << "\\caption{ ??  \\label{?:tab:?}}\\\\"  ;
	cout << "\\hline" << endl ;
	cout << "\\multicolumn{1}{|c|} " ; // the same line
	cout << "{$\\rule[-0.1in]{0.pt}{0.3 in} \\mathbf{M_1} $} ";// continue  line
	cout << " & " << endl;
	cout << "\\multicolumn{1}{|c|} " ; // the same line
	cout << "{$\\mathbf{M_2} $} ";// continue  line
	cout << " & " << endl;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{4fL \\over D} $ } & ";
	cout << endl ;
	cout << "\\multicolumn{1}{|c|}";
	cout << 
	"{$ \\mathbf{\\left.{\\left. 4fL \\over D \\right|_{max}}\\right|_{1} } $ } & ";
	cout << endl ;
	cout << "\\multicolumn{1}{|c|}";
	cout << 
	"{$ \\mathbf{\\left.{\\left. 4fL \\over D \\right|_{max}}\\right|_{2} } $ } & ";
	cout << endl ;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{P_2 \\over P_1} $ } ";
	cout << endl << "\\\\\\hline" << endl;
	cout << endl ;
	cout << "\\endfirsthead" << endl ;
	cout << "\\caption{ ?? (continue)} \\\\\\hline" << endl ;
    cout << "\\multicolumn{1}{|c|} " ; // the same line
    cout << "{$\\rule[-0.1in]{0.pt}{0.3 in} \\mathbf{M_1} $} ";// continue line 
	cout << " & " << endl;
	cout << "\\multicolumn{1}{|c|} " ; // the same line
	cout << "{$\\mathbf{M_2} $} ";// continue line 
	cout << " & " << endl;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{4fL \\over D} $ } & ";
	cout << endl ;
	cout << "\\multicolumn{1}{|c|}";
	cout << 
	"{$ \\mathbf{\\left.{\\left. 4fL \\over D \\right|_{max}}\\right|_{1} } $ } & ";
	cout << endl ;
	cout << "\\multicolumn{1}{|c|}";
	cout << 
	"{$ \\mathbf{\\left.{\\left. 4fL \\over D \\right|_{max}}\\right|_{2} } $ } & ";
	cout << endl ;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{P_{2} \\over {P_{1}}} $ }";
	cout << endl << "\\\\\\hline" << endl;
	cout << "\\endhead" << endl;
	}

};

void 	compressibleFlow::makeTexTableTail() {
	cout << "\\end{longtable}" << endl ;
};

void 	compressibleFlow::makeTexTableLine(int isBasicInfo) {

    int space;
    space = 7;
    int stdPrecision;
	stdPrecision = 4;


	if (isBasicInfo == yes) {
	    cout << setfill (' ') << setw (10);
		cout.setf(ios::fixed | ios::showpoint);
		//cout.setf ( cout::ios_base::right, cout::ios_base::showpoint, 
		//			 cout::ios_base::basefield );
        cout.precision(4);
        if (fabs (M) < 0.1) {cout.width(6); cout.precision(4); }
		else{ cout.width(4);} ;
		// if (M1 < 10) cout << "\t" ;
        cout.precision(5);
        cout.width(6);
        cout << M << "&\t" ;
        cout.width(6);
        cout << fld << "&\t" ;
        cout.width(6);
        cout << PRstar  << "&\t" ;
		//if (RRstar < 10) cout << "\t" ;
        cout.width(6);
        cout <<PzeroRstar  << "&\t"  ;
        cout.width(6);
        cout << RRstar  << "&\t"  ;
        cout.width(6);
        cout << URstar  << "&\t"  ;
        cout.width(6);
        cout << TRstar  << "\t"  ;
		//if (PRstar < 10) cout << "&\t" ;
        cout  << " \\\\ \\hline" ;
        cout << endl ;
	}
	else {
    if (M1 > 10.0 ) {cout.width(space - 1); cout.precision(stdPrecision-2); }
            else {cout.width(space);cout.precision(stdPrecision-1);};
        cout << M1 << "&\t" ;
	
	    if (M2 > 10.0 ){cout.width(space - 1); cout.precision(stdPrecision-2);}
            else {cout.width(space);cout.precision(stdPrecision-1);};
        cout << M2 << "&\t" ;
	    if (fld > 10.0 ){cout.width(space - 1); cout.precision(stdPrecision-2);}
            else {cout.width(space);cout.precision(stdPrecision-1);};
        cout << fld << "&\t" ;

	    if (fld1 > 10.0 ){cout.width(space - 1); cout.precision(stdPrecision-2);}
            else {cout.width(space);cout.precision(stdPrecision-1);};
        cout << fld1 << "&\t" ;

	    if (fld2 > 10.0 ){cout.width(space - 1); cout.precision(stdPrecision-2);}
            else {cout.width(space);cout.precision(stdPrecision-1);};
        cout << fld2 << "&\t" ;
	    if (p2p1 > 10.0 ){cout.width(space - 1); cout.precision(stdPrecision-2);}
            else {cout.width(space);cout.precision(stdPrecision-1);};
        cout << p2p1 << "\t" ;
        cout  << " \\\\ \\hline" ;
        cout << endl ;
	}


};


int 	compressibleFlow::calM1(double FLD, double P2P1){
	double fldNew ; // first guess of max fld 
	double fldPrevious ; // previous guess of max fld
	double M1max ; // the maximum value of Mach entrance to tube.
	double M1TMP ; //the TMP holder of M1
	//double M2est ; //the estimate of current M2
	// the first guess the addition of fld 	
	// find a better guess for this process
	double P2PstarEst; // the pressure ratio (1) estimate (est fld)
	double P1PstarEst; // the pressure ratio (2) estimate (est fld)
	fld = FLD;// change to the class verible 
	p2p1 = P2P1; //change to class variable
	/*  the main routine */
	/*  initialization process  and guessing */
	// first guess what is the range that M1 can be ... i.e. 0.0001 to M1max	
	/* calculate M1max */
	M1TMP = 0.1 ; // just to indicate that it is subsonic flow
	M1max = calMForFLD(FLD,M1TMP );
	// check the whether the flow is chocked.
	// check whether p2/p1 is below the critical pressure ratio.
	{
	M = M1max; // to make tmp assigned to M  
	calPressureRatio();
	if ((1./PRstar) >= p2p1 ) {//the flow is chocked
		M1 = M1max;
		M2 = 1. ;
		fld1 = FLD;
		fld2 = 0.0;
		return yes;
		}
	
	};
	
	// now the pair fld and M1max are known.
	// try to calculate/estimate the M1 for this pair 
	// including p2p1 pass trough the class variable 
	M1TMP = root ( calP2P1_Estmt, this, .00001,  M1max, FLD) ;

	//root to put something whether there is solution
	//root ( calP2P1_Estmt(0.,1.0) , .00001,  M1max, FLD, &M1TMP) ;

	// insert the information about M2 and fld1 and fld2
	fld1 = calFLD(M1TMP);
	fld2 = fld1 - FLD;
	// insert the information about M2 and fld1 and fld2
	M2 = calMForFLD(fld2,M1max ); 
//	M2 = M1 ; 
	if ( M2 <= 0.0001) {// incompressible flow
	cout << " this flow is incompressible" << endl;
	return no; // no solution 
	}
	// note that M1 is lost and has to be put again from tmp place holder  
	M1 = M1TMP;
	// and again return given value of fld
	fld = FLD;
	
	// just checking to see if working to clean later
	//cout << "p2/p2 = " << p2p1<< "\n" ;
	//cout << "calculated M1 = " << M1 << "\n" ;
	
} ;

int 	compressibleFlow::calM1forM2(double FLD, double m2, double * m1){
	double FLD2tmp ; // fld2 for the pair info (FLD, m2) 
	FLD2tmp = calFLD(m2) ;
	if (m2 <= 1.0 ) { // sub sonic flow 
		fld1 = FLD2tmp + FLD;
		*m1= calMForFLD (fld1,m2);
		return yes;
	}
	else if (m2 > 1.0 ) { // super sonic flow 
		if (FLD < supperSonicFLD) {// within possible range	
			fld1 = FLD2tmp + FLD;
			*m1= calMForFLD (fld1,m2);
			return yes;
		}
		else if (FLD == supperSonicFLD) {// the maximum possible range
			 *m1= infinity; // 1.79769313486231570E+308 ;// m1 = infinity
			return yes;
		}
		else { // the above the maximum range
			return no;
		}
	}
} ;
	

void 	compressibleFlow::showResults(int isBasicInfo) {

	int space;
	space = 7;
    int stdPrecision;
    stdPrecision = 4;
	cout << setfill (' ') << setw (10);
	cout.setf(ios::fixed | ios::showpoint);
	//cout.setf ( cout::ios_base::right, cout::ios_base::showpoint, 
	//			 cout::ios_base::basefield );

	if (isBasicInfo == yes){
    	if (M > 10.0 ) {cout.width(space); cout.precision(stdPrecision-2); }
            else {cout.width(space);cout.precision(stdPrecision-1);};
		cout.width(space -1);
		cout << M << "\t" ;
    	if (fld > 10.0 ) {cout.width(space); cout.precision(stdPrecision-2); }
            else {cout.width(space);cout.precision(stdPrecision-1);};
		cout << fld << "\t" ;
		if (PRstar > 10.0 ){cout.width(space); cout.precision(stdPrecision-2);}
			else {cout.width(space);	cout.precision(stdPrecision-1);};
		cout << PRstar  << "\t" ;
		//if (fld < 10.0) cout << "\t" ;
		if (PzeroRstar>10.){cout.width(space);cout.precision(stdPrecision-2);}
		else {cout.width(space);    cout.precision(stdPrecision-1);};
		cout <<PzeroRstar << "\t" ;
		if (RRstar > 10.0 ) {cout.width(space);cout.precision(stdPrecision-2);}
		else {cout.width(space);    cout.precision(stdPrecision-1);};
		cout << RRstar << "\t"  ;
		if (URstar > 10.0 ) {cout.width(space);cout.precision(stdPrecision-2);}
		else {cout.width(space);    cout.precision(stdPrecision-1);};
		cout << URstar << "\t" ;
        if (TRstar > 10.0 ) {cout.width(space);cout.precision(stdPrecision-2);}
        else {cout.width(space);    cout.precision(stdPrecision-1);};
		cout << TRstar  ;
		cout << endl ;
	}
	else{
		cout.precision(4);
		cout.width(space -1);
		cout << M1 << "\t" ;
		//if (M1 < 10) cout << "\t" ;
		cout.precision(4);
		cout.width(space);
		cout << M2 << "\t" ;
		//if (M2 < 10.0 ) cout  << "\t" ; 
		if (fld < 10.0 ) {cout.width(space); cout.precision(4);}
		else {cout.width(space);	cout.precision(3);};
		cout << fld << "\t" ;
		//if (fld < 10.0) cout << "\t" ;
		if (fld1 < 10.0 ) {cout.width(space); cout.precision(4);}
		else {cout.width(space);    cout.precision(3);};
		cout << fld1 << "\t" ;
		//cout << "\t";
		//if (fld1 < 14.0) cout << "\t" ;
		if (fld2 < 10.0 ) {cout.width(space); cout.precision(4);}
		else {cout.width(space);    cout.precision(3);};
		cout << fld2 << "\t"  ;
		//if (fld2 < 10.0) cout << "\t" ;
		cout.width(space-2);
		cout.precision(3);
		cout << p2p1  ;
		cout << endl ;
	}

};

void	compressibleFlow::showHead(int isBasicInfo){

	int space;
    space = 7;

    cout << setfill (' ') << setw (10);
    cout.setf ( std::ios_base::right,
                     std::ios_base::basefield );
	if (isBasicInfo == yes) {
		cout.width(space);
		cout << " with  k = "  << k <<  "\n\n" ;
 		if (M > 10.0 ) {cout.width(space); } else {cout.width(space);};
		cout << "M " ;
 		if (fld > 10.0 ) {cout.width(space); } else {cout.width(space);};
		cout << "fld " << "\t"  ;
		cout.width(space);
		cout << "P/P*" << "\t" ;
		cout.width(space);
		cout << "P0/P0*" << "\t" ;
		cout.width(space);
		cout << "R/R*"  << "\t" ;
		cout.width(space);
		cout << "U/U*"   << "\t" ;
		cout.width(space);
		cout << "T/T*"   ;
		cout << endl ;
	}
	else{
        cout.width(space-1);
        cout << "k = "  << k <<  "\n\n" ;
        cout.width(space);
        cout << "M1 " ;
        cout.width(space);
        cout << "M2 " << "\t"  ;
        cout.width(space);
        cout << "fld" << "\t" ;
        cout.width(space);
        cout << "fld1" << "\t" ;
        cout.width(space);
        cout << "fld2"  << "\t" ;
        cout.width(space);
        cout << "p2p1"   ;
        cout << endl ;
	}

};

int compressibleFlow:: calTableLine (double parameter,  int variableName,
				double variableValue){
	double m1tmp; //tmp m1
	if (variableName == fldV) {
		calM1( variableValue, parameter) ; // calculate M1 fun of (fld,p2/p1)
	}
	else if (variableName == M1V){
		double FLD = variableValue;
		double m2 = parameter;	
		if ( calM1forM2 (FLD, m2, &m1tmp) == yes)//calculate M1 fun of (fld, M2)
		{
			M1=m1tmp;
			M2 = m2;
			fld = FLD;
		}		
		else {
			return no;
		}
	}

	return yes;
};

int compressibleFlow:: calTableLine (int variblelName, double variableValue){
	// calculated  all the important parameters and see if
	// all exist.
	// to be completed later
	if (variblelName == machV){
		M = variableValue ;
		calDensityRatio();
		calFLD();
		calTotalPressureRatio();
		calPressureRatio();
		calTemperatureRatio();
		calVelocityRatio();
		return 1;
	} else if (variblelName == fldV ) {
		fld = variableValue ;	
		double Mtmp =0.1 ;// to indicate sub sonic brench
        M= calMForFLD(variableValue,Mtmp) ;
        calDensityRatio();
        calTotalPressureRatio();
        calPressureRatio();
        calTemperatureRatio();
        calVelocityRatio();
        return 1;
	}

};

int   compressibleFlow::makeTexTableLine (int variblelName, double parameter) {
	// make the necessarily calculation	
	calTableLine (variblelName, parameter);
	//make the LaTeX table
	makeTexTableHead(yes ); //isBasicInto variable
	makeTexTableLine(yes); //
	makeTexTableTail();

};

int   compressibleFlow::makeTable (int isBasicInfo, int isRange, int isTeX,
			 int variblelName, double variableValue) {
	// make the necessary calculations	
	if ( isRange == no ) {
		calTableLine (variblelName, variableValue);
		if (isTeX == yes){
		//make the LaTeX table
			makeTexTableHead(isBasicInfo ); //isBasicInto variable
			makeTexTableLine(isBasicInfo); //
			makeTexTableTail();
		}
		else {
			showHead(isBasicInfo);
			showResults(isBasicInfo);
		}
	}
	else {
		manegerCalTableLine(isTeX, isBasicInfo); 
	}
	return 1;
};

int   compressibleFlow::makeTable (int isBasicInfo, int isRange, int isTeX,
		 double parameter, int variableName, double variableValue ) {
	double FLD, P2P1;
    // make the necessary calculations
    if ( isRange == no ) {
		if (variableName == fldV){
			FLD = variableValue;
			P2P1 = parameter;
		}
		else if (variableName == p2p1V) {
			FLD = parameter;
			P2P1 = variableValue;
		}
        calTableLine (FLD, fldV, P2P1);
        if (isTeX == yes){
        //make the LaTeX table
            makeTexTableHead(isBasicInfo ); //isBasicInto variable
            makeTexTableLine(isBasicInfo); //
            makeTexTableTail();
        }
        else {
            showHead(isBasicInfo);
            showResults(isBasicInfo);
        }
    }
    else {
		if (variableName == fldV){
			FLD = variableValue;
			P2P1 = parameter;
			manegerCalTableLine(isTeX, isBasicInfo, fldV, variableValue ); //  
		}
		else if (variableName == M1V) {
			FLD = parameter; 
		    manegerCalTableLine(isTeX, isBasicInfo, M1V, FLD ); //
		}
    }
    return 1;
};


//when M is known use this function
int compressibleFlow:: calTableLine (void ){
    // calculated  all the important parameters and see if
    // all exist.
    // to be complete later

	// calculate the M2 by find fld(M1) and subtract fld [given]
		
	fld1 = fld ;
    fld = calFLD(M1) ;
	fld2 = fld1 - fld ;	
	// the fld as fun of M2 
	
    calPressureRatio() ;
    //iso.calDensityRatio() ;
    calTotalPressureRatio() ;
    calTotalTemperatureRatio() ;
                                                                                
    return 1;
};

int compressibleFlow:: makeLines (int isTex, int isBasicInfo, int numberOFsteps,
		double startingPoint, double lastPoint, int variableName,
		double variableValue ){
    int limit;
    double h, range, delta ;
    limit = numberOFsteps;
	range = fabs(startingPoint - lastPoint); //regardless the end point 	
							// if startingPoint >  lastPoint positive  
	delta = range /((double) limit); 
	h = startingPoint;
    for (int i = 0 ; (i <= limit) ; i++ ) {
        if ( calTableLine(h, variableName, variableValue) == yes ){
			if (isTex == yes) {
				//changeMach ((double) h );
				makeTexTableLine(isBasicInfo);
			}
			else {
				isBasicInfo = no;
				showResults(isBasicInfo);
			};
		}
		else{
			cout << "no results possible to large fld"<< endl; 
		};
        h = h + delta;
    };
    return 1;
};

int compressibleFlow:: makeLines (int isBasicInfo, int isTex, int numberOFsteps,
				double startingPoint, double lastPoint ){
    int limit;
    double h, range, delta ;
    limit = numberOFsteps;
	range = fabs(startingPoint - lastPoint); //regardless the end point 	
											// if startingPoint >  lastPoint 
	delta = range /((double) limit); 
	h = startingPoint;
    for (int i = 0 ; (i <= limit) ; i++ ) {
        calTableLine(machV, h);
        if (isTex == yes) {
            //changeMach ((double) h );
            makeTexTableLine(isBasicInfo);
        }
        else {
			isBasicInfo = no ;
            showResults(isBasicInfo);
        };
        h = h + delta;
    };
    return 1;
};

int compressibleFlow :: manegerCalTableLine (int isTex, int isBasicInfo,
				int variableName,  double variableValue){
    // manage the lines calculations  
    // to be complete later
		// first calculate the parameters 
		// now print either for TeX or without TeX
	//first print the header depending on the TeX or ''plain'' table
	if (isTex == yes) {
		makeTexTableHead(isBasicInfo);
	}
	else{
		showHead(isBasicInfo);
	};
	//make the line or the lines
	//sub sonic flow
	//makeLines(isTex, isBasicInfo, 20,0.05,(singularPoint -EPS ), 
	//				variableName, variableValue);
	//supper sonic flow
	makeLines(isTex, isBasicInfo, 20,(singularPoint +EPS ), 2.0, 
				variableName, variableValue);
	//makeLines(isTex, 30, 110, 1000,variableName,  variableValue);
	// the tail for TeX if needed
	if (isTex == yes) {
        makeTexTableTail();
    };

    return 1;
};

int compressibleFlow :: manegerCalTableLine (int isTex, int isBasicInfo){
    // manage the lines calculations  
    // first if all exist.
    // to be complete later
		// first calculate the parameters 
		// now print either for TeX or without TeX
	//first print the header depending on the TeX or ''graph'' table
	if (isTex == yes) {
		makeTexTableHead(isBasicInfo);
	}
	else{
		showHead(isBasicInfo);
	};
	//make the line or the lines
	makeLines(isBasicInfo, isTex, 18,	0.05, 0.95);
	makeLines(isBasicInfo, isTex, 2,0.96,(singularPoint -0.001 ));
	makeLines(isBasicInfo, isTex, 20,singularPoint+ 0.0000001,2.);
	makeLines(isBasicInfo, isTex, 10,2.5,10.);
	// the tail for TeX if needed
	if (isTex == yes) {
        makeTexTableTail();
    };

    return 1;
};


void  compressibleFlow :: calP2P1(){
	
    p2p1 = P2Pstar/P1Pstar;

};


double  compressibleFlow :: calMForFLD(double FLD, double mach){
	//what do when there is no solution ?
	double MACH;
	if ((mach < singularPoint) && (FLD > EPS ) ) { // sub sonic flow 
		return  root ( calFLD_Estmt,  this, .00001,  (M1maxSub-EPS), FLD  ) ;
	}
	else if ((mach > singularPoint )  && (FLD > EPS ) ) { // super sonic flow
		return  root ( calFLD_Estmt,  this,  (M1maxSub+EPS), 100.,  FLD  ) ;
	}
	else if (FLD == 0.0 )  {
		return 0.0;
	}
	else { // no possible negative fld
		return -1.0;
	} 

};


void  compressibleFlow :: calM2ForFLD(double FLD ){
	//what do when there is no solution
     //root ( calFLD_Estmt,  .00001,  1.0, FLD, &M2) ;
	FLD = 1.0;
};

class fanno: public compressibleFlow {
        protected:
        public:
			// to change the initialization later on
			// so that base class initialization will be considered
			//fanno(double Mach, double spesificHeatRatio ):
			fanno(double spesificHeatRatio ):
					//inilazation of base class
				//compressibleFlow(Mach,spesificHeatRatio)
				compressibleFlow(spesificHeatRatio)
					 { 
						// specific initialization process for derived class
						//the speed of the chocked flow is 1 for fanno flow
						M1maxSub = 1.0;
						singularPoint = M1maxSub ; 
						supperSonicFLD = 0.821508116;
						//this part of isothermal 1./ sqrt(spesificHeatRatio);
			  		};
			~fanno(){};
			//spesific functions of this model of flow to calculate
			//spesific parameters of this flow model.
			// 
			void calFLD();
			void calPressureRatio() ;
			void calDensityRatio();
			void calVelocityRatio();
			void calTotalPressureRatio() ;
			void calTotalTemperatureRatio();
			void calTemperatureRatio();
			void calArea();
			//functions that will not touch the local variables
			double calFLD(double) ;
			double calPressureRatio(double) ;
			double calDensityRatio(double);
			double calVelocityRatio(double);
			double calTotalPressureRatio(double) ;
			double calTotalTemperatureRatio(double);
			double calTemperatureRatio(double);
			double calArea(double);

			// specific functions to fanno flow to be add in presenting
			// the results
			void ShowHead (char *, int);

};

double fPbar (double Mach, double k){
	return  pow (1 / ( 1 + (( k - 1) / 2 ) * Mach*Mach),(k / (k - 1) ) ) ;
}; 

double calP_ratioEstmt ( compressibleFlow c, double estimatedMach, double value
) {
    c.changeMach (estimatedMach) ;
    //c.calArea();
    c.calFLD() ;
    c.calPressureRatio() ;
    c.calTotalPressureRatio() ;
    //s.calTotalTemperatureRatio() ;
    //s.calDensityRatio() ;
    return (value - c.getFLD() ) ;

};

void fanno::ShowHead(char * kindFlow, int isBasicInfo){
	cout << kindFlow << " flow" ;
	ShowHead("fanno", isBasicInfo);// do the regular header
};
double  fanno::calFLD(double Mach){
	M=Mach;

	if (M < .00) {
		//flow is reversed ?
		isFLDknown= yes;
		isSubSonic=yes;
		isFlowReverse = no;
		return 1.0;
	}
	else if (M > 1.0 ) {
		//supersonic flow
		isSubSonic=no;
		isFLDknown = yes;
		isFlowReverse = no;
		return (1. - M *M ) / k /M/ M  + 
			0.5*(k+1.)/k *log 
					( 0.5*(k+1.) * M * M/(1.+ 0.5* (k-1.)*M*M));
	}
	else if (M < EPS ) {
		// for case almost incompressible flow
		isSubSonic=yes;
		isFLDknown = no;
		isFlowReverse = no;
		// to put the approximation formula
		// not done yet
		return M;
	}	
	else {
	// the subsonic flow and ``normal'' range
		isSubSonic=no;
		isFLDknown = no ;
		isFlowReverse = yes;
        return (1. - M *M ) / k /M/ M +
            0.5*(k+1.)/k*log ( 0.5*(k+1.) * M * M/(1.+ 0.5* (k-1.)*M*M));
	}
};

void  fanno::calFLD(){

	if (M < .00) {
		//flow is reversed ?
		fld = 1.0 ;
		isFLDknown= yes;
		isSubSonic=yes;
		isFlowReverse = no;
		return;
	}
	else if (M > 1.0 ) {
		//supersonic flow
        fld = (1. - M *M ) / k /M/ M +
            0.5*(k+1.)/k*log ( 0.5*(k+1.) * M * M/(1.+ 0.5* (k-1.)*M*M));
		isSubSonic=no;
		isFLDknown = yes;
		isFlowReverse = no;
		return;
	}
	else if (M < 0.00001 ) {
		// for case almost incompressible flow
		fld = M ;
		isSubSonic=yes;
		isFLDknown = no;
		isFlowReverse = no;
		return;
	}	
	else {
	// the subsonic flow and ``normal'' range
        fld = (1. - M *M ) / k /M/ M +
            0.5*(k+1.)/k*log ( 0.5*(k+1.) * M * M/(1.+ 0.5* (k-1.)*M*M));
		isSubSonic=no;
		isFLDknown = no ;
		isFlowReverse = yes;
	}
};

void  fanno::calArea(){
	Area =  pow((( 1 + (( k - 1) / 2 ) * M*M)/((k+1)/2)), 
			((k+1) / 2/(k-1) ))
				/ M;
};
	
double  fanno::calArea(double MM){
    return  pow((( 1 + (( k - 1) / 2 ) * MM*MM)/((k+1)/2)),
            ((k+1) / 2/(k-1) ))
                / MM;
};

void  fanno::calTemperatureRatio(){
	TRstar = 0.5* (k+1. )/ 
		(1. + (0.5*(k -1. ) *M*M) ) ;
};

double  fanno::calTemperatureRatio(double MM){
	return 0.5* (k+1. )/ 
		(1. + (0.5*(k -1. ) *MM*MM) ) ;
};

void  fanno::calTotalTemperatureRatio(){
	TzeroRstar = (k+1. )/ 
		(1. + (0.5*(k -1. ) *M*M) )*0.5 ;
};

double  fanno::calTotalTemperatureRatio(double MM){
	return  (k+1. )/ 
		(1. + (0.5*(k -1. ) *MM*MM) )*0.5 ;
};

void  fanno::calPressureRatio(){
	// calculate the pressure ratio
	PRstar =   sqrt(0.5*(k + 1.) /( 1.+ 0.5*(k-1.)*M*M ))/M ;
};

double  fanno::calPressureRatio(double MM){
	// calculate the pressure ratio
	return   sqrt(0.5*(k + 1.) /( 1.+ 0.5*(k-1.)*MM*MM ))/MM ;
};

void  fanno::calVelocityRatio(){
    // calculate the velocity ratio
	URstar = M * sqrt ( 0.5 * (k+1. )/ 
		(1. + (0.5*(k -1. ) *M*M) ) ) ;
};

double  fanno::calVelocityRatio(double MM){
    // calculate the velocity ratio
	return MM * sqrt ( 0.5 * (k+1. )/ 
		(1. + (0.5*(k -1. ) *MM*MM) ) ) ;
};

void  fanno::calDensityRatio(){
    // calculate the density ratio
    RRstar =   sqrt(2.*(1. + 0.5* (k -1.0) *M*M) /( 1.+ k))/M ;
};

double  fanno::calDensityRatio(double MM){
    // calculate the density ratio
    return  sqrt(2.*(1. + 0.5* (k -1.0) *MM*MM) /( 1.+ k))/MM ;
};


void  fanno::calTotalPressureRatio(){
		PzeroRstar =  sqrt(
				pow((2.*(1. + 0.5* (k -1.0) *M*M) /( 1.+ k)),
					(( k+1.0)/(k-1.0))  )
			)/M;  

	if (isFlowReverse == no ) {
		if (isFLDknown == yes) { 
			if (isSubSonic == no ) {
				// a change in the order of the formula from the book
				PzeroRstar =  sqrt(
                	pow((2.*(1. + 0.5* (k -1.0) *M*M) /( 1.+ k)),
                    	(( k+1.0)/(k-1.0))  )
            	)/M;
			}
			else {
				PzeroRstar = 1.0;
			}
		}
		else{
			PzeroRstar = 1.0;
		}
	}
	else {
		return ;
	}
};
 
double  fanno::calTotalPressureRatio(double MM){
	return  sqrt(
			pow((2.*(1. + 0.5* (k -1.0) *MM*MM) /( 1.+ k)),
				(( k+1.0)/(k-1.0))  )
		)/MM;  
};




int main( void )
{
	double M , FLD, p2p1;
	double k = 1.4;
	// fan is the name of the actual object with k = 1.4.
	fanno fan(k) ; 
	compressibleFlow * fanC = &fan;
	int isBasicInfo = no;
	int isTex = no;
	int isRange = yes;
	M = 0.9;
	FLD = .800;
	p2p1 = 0.5 ; 
	//the following line example using as a pointer
	//fanC->makeTable(isBasicInfo,isRange, isTex, machV, M);
	// fan.makeTable(isBasicInfo, isRange, isTex, fldV, FLD);
	fan.makeTable(isBasicInfo, isRange, isTex, FLD, M1V, p2p1); //chocked flow 
	// when the manager in control  
	// the main function of calculations of p
	//for basic information 
	// fan.findMForFLD(3.21);
	// M = fan.getM1();
	//fan.calM1(.10, 0.74588059237509295); // calculate M1 fun of (fld,p2/p1)
	//fan.calP2P1 ();
	//fan.makeTexTableHead(isBasicInfo);// turn off when the manager is in control
	//fan.manegerCalTableLine (no, isBasicInfo  ); //yes with TeX no without it.
									//second parameter is isBasicInfo 
									// for basic info of the flow.
	//fan.manegerCalTableLine (no , 0.10); //yes with TeX no without it.
							// the second parameter, is pressure ratio
	//fan.calTableLine(machV, 0.00028 );	
	//fan.calTableLine(3.0);// calculated when no manager off it when manger on	
	//fan.makeTexTableLine(isBasicInfo); //turn off when the manager is on
	//fan.makeTexTableTail(); // turn off when the manager is on control

	//fan.showHead(yes); // yes show only the basic stuff 
    //fan.showResults(yes); // yes only the basic stuff 
						// just for table in (mx)grace
	return 0;
}


