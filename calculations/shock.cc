/********************************************************

	This code is copyrighted by Genick Bar_Meir 
	and has no  warrenty of any kind.
	Use this code at your own risk. 
	
	This program is open source under the same lisence as
	all the others material in potto project.
	By:
	Genick Bar-Meir

	Inputes:
	n/a

	Returens:
	prints table for Mx and My and the other ratio
	for the normal shock wave.
	It assume perfect gas with k=1.4
	
	Description:
	This code includes classes to calculate the Mx number from
	both side of the shock wave and the other parameters.
********************************************************/

# include <cstdio>
# include <cmath>
# include <iostream>
# include <iomanip> 
                                                                                
#   define  PI 3.14159265358979310
#   define EPS  1e-4
#   define yes  1
#   define no  0
#   define sonic 3 // Mach = 1

                                                                                
using namespace std;
                                                                                


class shock {
        protected:
			double TyTx; // temperature ratio Ty/Tx
			double PyPx ; // pressure ratio Py/Px
			double RyRx ; // relative density rhoY/rhox 
			double P0yP0x ; // pressure ratio P0y/P0x
			double Area ; // relative area 
			double Mx ; // Mx nubmer befor the normal shock
			double My ; // My nubmer after the normal shock
			double k	; // ratio of spesific heat
        public:
			shock(double M, double spesificHeatRatio ) { Mx = M ;
					k = spesificHeatRatio; };
			~shock(){};
			void calMy() ;
			void calPressureRatio() ;
			void calTotalPressureRatio() ;
			void calTemperatureRatio();
			void calDensityRatio();
			void calArea();
			void changeMx ( double M) {Mx = M; } ;
			void showHead();
			void showResults();
			void makeTexTableHead();
			void makeTexTable();
			void makeTexTableTail();
			double getPressure(){return PyPx ; };
            double getTemperatue (){return TyTx ; };
            double getDensity (){return RyRx ; };
            double getArea (){return Area ; };

};

void 	shock::makeTexTableHead() {

	cout << endl << endl;
	cout << "*******************************************" << endl ;
	cout << "The following stuff is the same as above" << endl ;
	cout << "information but is setup for the latex text processing" << endl ;
	cout << "you can just can cut and paste it in your latex file" << endl ;
	cout << "you must use longtable style file and dcolumn " << endl ;
	cout << "style file." << endl  << endl << endl;
	cout << "*******************************************" << endl ;
	cout << "\\setlongtables" << endl ;
	cout << "\\begin{longtable}" << endl ;
	cout << "{|D..{-1}|D..{2.7}|D..{2.7}|D..{3.7}|D..{2.7}|D..{2.7}|}" << endl ;
	cout << "\\caption{ ??  \\label{?:tab:?}}\\\\"  ;
	cout << "\\hline" << endl ;
	cout << "\\multicolumn{1}{|c|} " ; // the same line
	cout << "{$\\rule[-0.1in]{0.pt}{0.3 in} \\mathbf{M_x} $} ";// continue  line
	cout << " & " << endl;
	cout << "\\multicolumn{1}{|c|} " ; // the same line
	cout << "{$\\mathbf{M_y} $} ";// continue  line
	cout << " & " << endl;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{T_y \\over T_x} $      } & " ;
	cout << endl ;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{\\rho_y \\over \\rho_x} $ } & ";
	cout << endl ;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{P_y \\over P_x} $ } &";
	cout << endl ;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{P_{0y} \\over P_{0x}} $ }";
	cout << endl << "\\\\\\hline" << endl;
	cout << endl ;
	cout << "\\endfirsthead" << endl ;
	cout << "\\caption{ ?? (continue)} \\\\\\hline" << endl ;
    cout << "\\multicolumn{1}{|c|} " ; // the same line
    cout << "{$\\rule[-0.1in]{0.pt}{0.3 in} \\mathbf{M_x} $} ";// continue line 
	cout << " & " << endl;
	cout << "\\multicolumn{1}{|c|} " ; // the same line
	cout << "{$\\mathbf{M_y} $} ";// continue line 
	cout << " & " << endl;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{T_y \\over T_x} $      } & " ;
	cout << endl ;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{\\rho_y \\over \\rho_x} $ } & ";
	cout << endl ;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{P_y \\over P_x} $ } &";
	cout << endl ;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{P_{0y} \\over P_{0x}} $ }";
	cout << endl << "\\\\\\hline" << endl;
	cout << "\\endhead" << endl;

};

void 	shock::makeTexTableTail() {
	cout << "\\end{longtable}" << endl ;
};

void 	shock::makeTexTable() {

	    cout << setfill (' ') << setw (10);
		cout.setf(ios::fixed | ios::showpoint);
		//cout.setf ( std::ios_base::right, std::ios_base::showpoint, 
		//			 std::ios_base::basefield );
        cout.precision(2);
        cout.width(4);
        cout << Mx << "&\t" ;
		// if (Mx < 10) cout << "\t" ;
        cout.precision(5);
        cout.width(6);
        cout << My << "&\t" ;
        cout.width(6);
        cout << TyTx << "&\t" ;
		//if (TyTx < 10) cout << "\t" ;
        cout.width(6);
        cout << RyRx << "&\t" ;
		//if (RyRx < 10) cout << "\t" ;
        cout.width(6);
        cout << PyPx << "&\t"  ;
		//if (PyPx < 10) cout << "&\t" ;
        cout.width(6);
        cout << P0yP0x  << " \\\\ \\hline" ;
        cout << endl ;

};


void 	shock::showResults() {

	    cout << setfill (' ') << setw (10);
		cout.setf(ios::fixed | ios::showpoint);
		//cout.setf ( std::ios_base::right, std::ios_base::showpoint, 
		//			 std::ios_base::basefield );
        cout.precision(2);
        cout.width(4);
        cout << Mx << "\t" ;
		// if (Mx < 10) cout << "\t" ;
        cout.precision(5);
        cout.width(6);
        cout << My << "\t\t" ;
        cout.width(6);
        cout << TyTx << "\t" ;
		if (TyTx < 10) cout << "\t" ;
        cout.width(6);
        cout << RyRx << "\t" ;
		if (RyRx < 10) cout << "\t" ;
        cout.width(6);
        cout << PyPx << "\t"  ;
		if (PyPx < 10) cout << "\t" ;
        cout.width(6);
        cout << P0yP0x  ;
        cout << endl ;

};

void	shock::showHead(){

    cout << setfill (' ') << setw (10);
	cout.setf ( std::ios_base::right, 
					 std::ios_base::basefield );
    cout.width(6);
	cout << "k = "  << k <<  "\n\n" ;
    cout.width(4);
	cout << "Mx " << "\t"  ;
    cout.width(6);
	cout << "My " << "\t\t"  ;
    cout.width(6);
    cout << "TyTx" << "\t\t" ;
    cout.width(6);
    cout << "RyRx" << "\t\t" ;
    cout.width(6);
    cout << "PyPx"  << "\t\t" ;
    cout.width(6);
    cout << "P0yP0x"   ;
    cout << endl ;

};


void  shock::calMy(){
	My = sqrt((Mx*Mx + 2.0/(k -1)) / 
		((2*k / (k-1) ) *Mx*Mx -1)) ; 
};

void  shock::calArea(){
	Area =  pow((( 1 + (( k - 1) / 2 ) * Mx*Mx)/((k+1)/2)), 
			((k+1) / 2/(k-1) ))
				/ Mx;
};

void  shock::calTemperatureRatio(){
	TyTx = PyPx *PyPx * My*My/Mx/Mx;
};

void  shock::calPressureRatio(){
	PyPx =  (1 + k*Mx*Mx) / (1 + k*My*My) ;
};

void  shock::calTotalPressureRatio(){
	P0yP0x =  (PyPx)* 
	pow((1 + ((k-1)/2)*My*My),(k/(k-1))) / 
	pow((1 + ((k-1)/2)*Mx*Mx),(k/(k-1))) ;
};

void  shock:: calDensityRatio (){
	RyRx = PyPx/TyTx; 
};


int main( void )
{
	shock c(1.0, 1.4) ;
	//c.showHead();
	c.makeTexTableHead();
	int limit = 30 ;
	double h = 0.95;
	for (int i = 0 ; (i <= limit) ; i++ ) {
		h = h + 0.05;	
		c.changeMx ((double) h );
		c.calMy() ;
		c.calPressureRatio() ;
		c.calTotalPressureRatio() ;
		c.calTemperatureRatio() ;
		c.calDensityRatio() ;
		//c.showResults();
		c.makeTexTable();
	}
	//this part to calculate the more sparse of the table Mx=3-:-10
		h=2.5;
		limit= 29;
	 for (int i = 0 ; (i <= limit) ; i++ ) {
        h = h + 0.25;
        c.changeMx ((double) h );
        c.calMy() ;
        c.calPressureRatio() ;
        c.calTotalPressureRatio() ;
        c.calTemperatureRatio() ;
        c.calDensityRatio() ;
        //c.showResults();
		c.makeTexTable();
    }
	
	c.makeTexTableTail();
		c.showHead();
        c.showResults(); //just for fun
	return 0;
}
