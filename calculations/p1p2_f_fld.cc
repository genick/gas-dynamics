/********************************************************

	This code is copyrighted by Genick Bar_Meir 
	and has no  warrenty of any kind.
	Use this code at your own risk. 
	
	This program is open source under the same lisence as
	all the others material in potto project.
	By:
	Genick Bar-Meir

	Inputes:
	n/a

	Returens:
	prints table for the ratio of P1/P2 for non chocked flow 
	for given 4FL/D.
	The code also produces for specific value.	
	It assume perfect gas with k=1.4
	Oh, well, k also can be change via the class declation.
	
	Description:
	This code includes classes to calculate the various parameter
	of the isothermal flow.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it start to proper calcuation at fld> 1000. 
********************************************************/

# include <cstdio>
# include <cmath>
# include <iostream>
# include <iomanip> // what replace this file in the new system?

# 	define	PI 3.14159265358979310
#   define EPS  1e-4
# 	define yes  1
# 	define no  0
#   define sonic 3 // Mach = 1 

using namespace std;

class compressibleFlow{
		protected:
			int isFlowReverse; // check is the direction of the flow
			int isSubSonic; // is no Shock possible  
			int isFLDknown; // check if FLD was calculated/known?  no?  yes
			double k	; // ratio of spesific heat
			double fld; // the maximum length resistence 4fl/D
			double fld1; // the 4 fld for the tube entrace tube
			double fld2; //  the 4 fld for the tube exit
			double Area ; // relative area 
			double M1 ; // Mach number at the entrace
			double M2 ; // Mach number at the exit of the tube
			double M ;//Mach nubmer general use such as isentropic relationship 
		public:
			compressibleFlow (void){ 
					isFlowReverse = no; 
					isSubSonic = no; 
            		isFLDknown=no;
			};
			~ compressibleFlow (){};
			virtual void calFLD() {} ;
			virtual void calFLD(double){} ;
			virtual void calM1(double,double){} ; //to be build for every flow
											//model. find pressure ratio
											//for for given fld.  
            double getFLD (){return fld  ; };
            double getArea (){return Area ; };
            double getM1 (){return M1 ; };
};

class isothermal: public compressibleFlow {
        protected:
			double TSoverTSstarT; // temperature ratio T0/T0*T
			double PRstarT ; // pressure ratio P/P*T anywhere on the tube
			double P1Pstar ; // pressure ratio P1/P*T entrace to the tube
			double P2Pstar ; // pressure ratio P2/P*T exit of the tube
			double PzeroRstarT ; // pressure ratio P0/P0*T
			double RRstarT ; // relative density rho/rho*T
			double p2p1; // the ratio of pressure between two sides of tube
			double Pbar1 ; // stagnation pressure ratio @ entrace 
			double Pbar2 ; // stagnation pressure ratio @ exit 
			double M1maxSub; // the maximum ``subsonic'' isothermal
        public:
			// to change the initionalazation later on
			// so that base class initionalasion will be considered
			isothermal(double Mach, double spesificHeatRatio ) { 
					//initialization process
					M = Mach ;
					k = spesificHeatRatio; 
					M1maxSub = 1.0 / sqrt(spesificHeatRatio);
			  };
			~isothermal(){};
			void calFLD() ;
			void calFLD(double) ;
			void calM1(double fld,double p2p1) ; //find M1 for fld, p2/p1.
			void calPressureRatio() ;
			void calTotalPressureRatio() ;
			void calTotalTemperatureRatio();
			void calArea();
			void calP2P1();
			void findM1ForFLD(double);
			void changeMach ( double Mach) {Mach = M; } ;
			void showHead();
			void showResults();
			void makeTexTableHead();
			void makeTexTableLine();
			void makeTexTableTail();
			int	calTableLine(double ); // calculate the maneger
			int	calTableLine(double, double ); // with pressure ratio
												// calculate the maneger
			int	calTableLine(void ); // calculate the maneger when known
			// next function generate of part table
			int	 makeLines(int, int, double, double );
			int	 makeLines(int, int, double, double, double );
			int  manegerCalTableLine(int);//manager for calcaulating whole table
			int  manegerCalTableLine(int, double);//manager for calcaulating
									// whole table, well only specific P
									// ratio
			double getPRstarT(){return PRstarT ; };
            double getTemperatue (){return TSoverTSstarT ; };
            double getDensity (){return RRstarT ; };
            double getT_P_ratio (){return PzeroRstarT  ; };
            double getP2P1 (){return p2p1 ; };

};

double fPbar (double Mach, double k){
	return  pow (1 / ( 1 + (( k - 1) / 2 ) * Mach*Mach),(k / (k - 1) ) ) ;
}; 
		
double root( double f (isothermal, double , double ),
             isothermal s , double a , double b, double value) {

    double Af =  f(s,a, value) ;
    double Bf =  f(s, b, value) ;
                                                                                
    double m = ( a + b ) / 2.0 ;
    double Mf =  f(s, m, value ) ;
                                                                                
    if ( Mf == 0.0 || (b - a ) <= EPS ) {
        return m;
    }
    else if ( Af * Mf  < 0.0 ) {
        return root (f, s, a, m, value ) ;
    }
    else {
        return root(f, s, m, b, value);
    }
}

void 	isothermal::makeTexTableHead() {

	cout << endl << endl;
	cout << "*******************************************" << endl ;
	cout << "The following stuff is the same as above/below" << endl ;
	cout << "information but is setup for the latex text processing" << endl ;
	cout << "you can just can cut and paste it in your latex file" << endl ;
	cout << "you must use longtable style file and dcolumn " << endl ;
	cout << "style file." << endl  << endl << endl;
	cout << "*******************************************" << endl ;
	cout << "\\setlongtables" << endl ;
	cout << "\\begin{longtable}" << endl ;
	cout << "{|D..{-1}|D..{2.7}|D..{2.7}|D..{3.7}|D..{2.7}|D..{2.7}|}" << endl ;
	cout << "\\caption{ ??  \\label{?:tab:?}}\\\\"  ;
	cout << "\\hline" << endl ;
	cout << "\\multicolumn{1}{|c|} " ; // the same line
	cout << "{$\\rule[-0.1in]{0.pt}{0.3 in} \\mathbf{M_1} $} ";// continue  line
	cout << " & " << endl;
	cout << "\\multicolumn{1}{|c|} " ; // the same line
	cout << "{$\\mathbf{M_2} $} ";// continue  line
	cout << " & " << endl;
	cout << "\\multicolumn{1}{|c|}";
	cout << "{$  \\mathbf{\\left.{\\fldmax}\\right|_{1} } $ } & " ;
	cout << endl ;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{\\fld} $ } & ";
	cout << endl ;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{P_2 \\over P_1} $ } ";
	cout << endl << "\\\\\\hline" << endl;
	cout << endl ;
	cout << "\\endfirsthead" << endl ;
	cout << "\\caption{ ?? (continue)} \\\\\\hline" << endl ;
    cout << "\\multicolumn{1}{|c|} " ; // the same line
    cout << "{$\\rule[-0.1in]{0.pt}{0.3 in} \\mathbf{M_1} $} ";// continue line 
	cout << " & " << endl;
	cout << "\\multicolumn{1}{|c|} " ; // the same line
	cout << "{$\\mathbf{M_2} $} ";// continue line 
	cout << " & " << endl;
	cout << "\\multicolumn{1}{|c|}";
	cout << "{$  \\mathbf{\\left.{\\fldmax}\\right|_{1} } $ } & " ;
	cout << endl ;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{\\fld } $ } & ";
	cout << endl ;
	cout << "\\multicolumn{1}{|c|}{$  \\mathbf{P_{2} \\over {P_{2}}} $ }";
	cout << endl << "\\\\\\hline" << endl;
	cout << "\\endhead" << endl;

};

		

void 	isothermal::makeTexTableTail() {
	cout << "\\end{longtable}" << endl ;
};

void 	isothermal::makeTexTableLine() {

        int space;
        space = 8;

	    cout << setfill (' ') << setw (10);
		cout.setf(ios::fixed | ios::showpoint);
		//cout.setf ( cout::ios_base::right, cout::ios_base::showpoint, 
		//			 cout::ios_base::basefield );
        cout.precision(4);
        if (fabs (M) < 0.1) {cout.width(6); cout.precision(4); }
		else{ cout.width(4);} ;
        cout << M1 << "&\t" ;
		// if (M1 < 10) cout << "\t" ;
        cout.precision(5);
        cout.width(6);
        cout << M2 << "&\t" ;
        cout.width(6);
        cout << fld1 << "&\t" ;
        cout.width(6);
        cout << fld << "&\t" ;
		//if (RRstarT < 10) cout << "\t" ;
        cout.width(6);
        cout << p2p1 << "\t"  ;
		//if (PRstarT < 10) cout << "&\t" ;
        cout  << " \\\\ \\hline" ;
        cout << endl ;

};


void 	isothermal::showResults() {

		int space;
		space = 8;
	    cout << setfill (' ') << setw (10);
		cout.setf(ios::fixed | ios::showpoint);
		//cout.setf ( cout::ios_base::right, cout::ios_base::showpoint, 
		//			 cout::ios_base::basefield );
        cout.precision(4);
        cout.width(space -1);
        cout << M1 << "\t" ;
		//if (M1 < 10) cout << "\t" ;
        cout.precision(4);
        cout.width(space);
        cout << M2 << "\t" ;
		//if (M2 < 10.0 ) cout  << "\t" ; 
        if (fld < 10.0 ) {cout.width(space); cout.precision(4);}
		else {cout.width(space);	cout.precision(3);};
        cout << fld << "\t" ;
		//if (fld < 10.0) cout << "\t" ;
        if (fld1 < 10.0 ) {cout.width(space); cout.precision(4);}
        else {cout.width(space);    cout.precision(3);};
        cout << fld1 << "\t" ;
		//cout << "\t";
		//if (fld1 < 14.0) cout << "\t" ;
        if (fld2 < 10.0 ) {cout.width(space); cout.precision(4);}
        else {cout.width(space);    cout.precision(3);};
        cout << fld2 << "\t"  ;
		//if (fld2 < 10.0) cout << "\t" ;
        cout.width(space-2);
        cout.precision(3);
        cout << p2p1  ;
        cout << endl ;

};

void	isothermal::showHead(){

	int space;
    space = 8;

    cout << setfill (' ') << setw (10);
    cout.setf ( std::ios_base::right,
                     std::ios_base::basefield );
    cout.width(space-1);
	cout << "k = "  << k <<  "\n\n" ;
    cout.width(space);
	cout << "M1 " ;
	//coot << "\t"  ;
    cout.width(space);
	cout << "M2 " << "\t"  ;
    cout.width(space);
    cout << "fld" << "\t" ;
    cout.width(space);
    cout << "fld1" << "\t" ;
    cout.width(space);
    cout << "fld2"  << "\t" ;
    cout.width(space);
    cout << "p2p1"   ;
    cout << endl ;

};


double calFLD_Estmt ( isothermal s, double estimatedMach, double value) {
    s.changeMach (estimatedMach) ;
    //s.calArea();
    s.calFLD(estimatedMach) ;
    //s.calPressureRatio() ;
    //s.calTotalPressureRatio() ;
    //s.calTotalTemperatureRatio() ;
    //s.calDensityRatio() ;
    return (value - s.getFLD() ) ;
};

double calP_ratioEstmt ( isothermal s, double estimatedMach, double value
) {
    s.changeMach (estimatedMach) ;
    s.calArea();
    s.calFLD() ;
    s.calPressureRatio() ;
    s.calTotalPressureRatio() ;
    //s.calTotalTemperatureRatio() ;
    //s.calDensityRatio() ;
    return (value - s.getFLD() ) ;

};

double calP2P1_Estmt ( isothermal s, double M1est, double fld) {
	double  M2est; //the value est of the mach at the exit;
	double P1PstarEst, P2PstarEst ;// the estimated p1/P*T, p2/P*T 
	double tmpFLD ; // estimated FLD
	double newFLD ; // the new estimated  FLD for M2
	double p2p1est;
	
	//calculate the P1PstarT for M1
    s.changeMach(M1est);
	s.calPressureRatio(); //cal the ratio
    P1PstarEst  = s.getPRstarT(); // assigned the ratio to a tmp varible
	
	// need to find M2
	// calculate the leftover of  fld to M2
	s.calFLD(M1est);
	tmpFLD = s.getFLD() ; 
	newFLD = tmpFLD - fld ;
		
	// calculate M2 
    s.findM1ForFLD(newFLD);
	//note that the fuction calculete relationship M to fld
	// not M1 or M2
	M2est = s.getM1() ;
	s.changeMach(M2est);
    s.calPressureRatio();
    P2PstarEst= s.getPRstarT() ;
    p2p1est = P2PstarEst / P1PstarEst;
	return ( p2p1est - s.getP2P1() ) ;	
};

void 	isothermal::calM1(double FLD, double P2P1){
	double fldNew ; // first guess of max fld 
	double fldPrevious ; // previous guess of max fld
	double M1max ; // the maximum value of Mach entrace to tube.
	double M1TMP ; //the TMP holder of M1
	//double M2est ; //the estimate of current M2
	// the first guess the addition of fld 	
	// find a better guess for this process
	double P2PstarEst; // the pressure ratio value of the current (est fld)
	double P1PstarEst; // the pressure ratio value of the current (est fld)
	double P1Pzero; // the pressure ratio isentropic of M1 (est fld)
					// to be removed probably no usege
	fld = FLD;// change to local class verible 
	p2p1 = P2P1; //change to local class verible
	/*  the main rotine */
	/*  initialazation process  and guessing */
	// first guess what is the range that M1 can be ... i.e. 0.0001 to M1max	
	/* calculate M1max */
	findM1ForFLD(fld);
	M1max = M1 ; 
	// now the pair fld and M1max are known.
	if ( M1max <= 0.0001) {// incompressible flow
	cout << " this flow is incompressible" << endl;
	return;
	}
	M1TMP = root ( calP2P1_Estmt, *this, .00001,  M1max, FLD) ;
	

	// insrt the information about M2 and fld1 and fld2
	calFLD(M1TMP);
	fld1 = fld;
	fld2 = fld1 - FLD;
	// insrt the information about M2 and fld1 and fld2
	findM1ForFLD(fld2); 
	M2 = M1 ; 
	// note that M1 is 
	M1 = M1TMP;
	// and again return given value of fld
	fld = FLD;
	

	// just checking to see if working to clean later
	//cout << "p2/p2 = " << p2p1<< "\n" ;
	//cout << "calculated M1 = " << M1 << "\n" ;
	
	
} ;

void  isothermal::calFLD(double Mach){
	M=Mach;

	if (M < .00) {
		//flow is reversed ?
		fld = 1.0 ;
		isFLDknown= yes;
		isSubSonic=yes;
		isFlowReverse = no;
		return;
	}
	else if (M > 1.0 ) {
		//suppersonic flow
		fld = (1 -k *M *M ) / k /M/ M + log (k * M * M);
		isSubSonic=no;
		isFLDknown = yes;
		isFlowReverse = no;
		return;
	}
	else if (M < 0.0000001 ) {
		// for case alsomt incompressible flow
		fld = M ;
		isSubSonic=yes;
		isFLDknown = no;
		isFlowReverse = no;
		return;
	}	
	else {
	// the subsonic flow and ``normal'' range
		fld = (1 -k *M *M ) / k /M/ M + log (k * M * M) ;
		isSubSonic=no;
		isFLDknown = no ;
		isFlowReverse = yes;
	}
};

void  isothermal::calFLD(){

	if (M < .00) {
		//flow is reversed ?
		fld = 1.0 ;
		isFLDknown= yes;
		isSubSonic=yes;
		isFlowReverse = no;
		return;
	}
	else if (M > 1.0 ) {
		//suppersonic flow
		fld = (1 -k *M *M ) / k /M/ M + log (k * M * M);
		isSubSonic=no;
		isFLDknown = yes;
		isFlowReverse = no;
		return;
	}
	else if (M < 0.00001 ) {
		// for case alsomt incompressible flow
		fld = M ;
		isSubSonic=yes;
		isFLDknown = no;
		isFlowReverse = no;
		return;
	}	
	else {
	// the subsonic flow and ``normal'' range
		fld = (1 -k *M *M ) / k /M/ M + log (k * M * M) ;
		isSubSonic=no;
		isFLDknown = no ;
		isFlowReverse = yes;
	}
};


void  isothermal::findM1ForFLD(double FLD ){
    M1 = root ( calFLD_Estmt, *this, .00001,  (M1maxSub-EPS), FLD) ;
};


void  isothermal::calArea(){
	Area =  pow((( 1 + (( k - 1) / 2 ) * M*M)/((k+1)/2)), 
			((k+1) / 2/(k-1) ))
				/ M;
};

void  isothermal::calP2P1(){
	
    p2p1 = P2Pstar/P1Pstar;

};


void  isothermal::calTotalTemperatureRatio(){
	TSoverTSstarT = (2*k / (3*k - 1) ) *
		(1 + ((k -1 ) / 2)* M*M ) ;
};

void  isothermal::calPressureRatio(){
	// calculate the pressure and density ratio
	PRstarT =  1 / sqrt(k)/M ;
	RRstarT = PRstarT; 
};

void  isothermal::calTotalPressureRatio(){
		PzeroRstarT =  (pow((2*k/(3*k -1)),(k/(k -1)))/sqrt(k))*
                pow((1 + ((k-1)/2)*M*M),(k/(k-1)))/M ;

	if (isFlowReverse == no ) {
		if (isFLDknown == yes) { 
			if (isSubSonic == no ) {
				// a change in the order of the formula from the book
				PzeroRstarT =  (pow((2*k/(3*k -1)),(k/(k -1)))/sqrt(k))* 
				pow((1 + ((k-1)/2)*M*M),(k/(k-1)))/M ;
			}
			else {
				 PzeroRstarT = 1.0;
			}
		}
		else{
			PzeroRstarT = 1.0;
		}
	}
	else {
		return ;
	}
};
 
int isothermal:: calTableLine (double fld, double P2P1){
	calM1(fld, P2P1) ; // calculate M1 fun of (fld,p2/p1)

	return 1;
};
int isothermal:: calTableLine (double Mach){
	// calculated  all the important parameters and see if
	// all exist.
	// to be complete later
	//M = Mach ; 
	//calFLD() ;
    //calPressureRatio() ;
    //iso.calDensityRatio() ;
    calTotalPressureRatio() ;
    calTotalTemperatureRatio() ;

	return 1;
};

//when M is known use this funciton
int isothermal:: calTableLine (void ){
    // calculated  all the important parameters and see if
    // all exist.
    // to be complete later

	// calculate the M2 by find fld(M1) and subtract fld [given]
		
	fld1 = fld ;
    calFLD(M1) ;
	fld2 = fld1 - fld ;	
	// the fld as fun of M2 
	
    calPressureRatio() ;
    //iso.calDensityRatio() ;
    calTotalPressureRatio() ;
    calTotalTemperatureRatio() ;
                                                                                
    return 1;
};


int isothermal:: makeLines (int isTex, int numberOFsteps,  double
			startingPoint, double lastPoint, double P2P1 ){
    int limit;
    double h, range, delta ;
    limit = numberOFsteps;
	range = fabs(startingPoint - lastPoint); //regrdless the end point 	
											// if startingPoint >  lastPoint 
	delta = range /((double) limit); 
	h = startingPoint;
    for (int i = 0 ; (i <= limit) ; i++ ) {
        calTableLine(h, P2P1);
        if (isTex == yes) {
            //changeMach ((double) h );
            makeTexTableLine();
        }
        else {
            showResults();
        };
        h = h + delta;
    };
    return 1;
};

int isothermal:: makeLines (int isTex, int numberOFsteps,  double
			startingPoint, double lastPoint ){
    int limit;
    double h, range, delta ;
    limit = numberOFsteps;
	range = fabs(startingPoint - lastPoint); //regrdless the end point 	
											// if startingPoint >  lastPoint 
	delta = range /((double) limit); 
	h = startingPoint;
    for (int i = 0 ; (i <= limit) ; i++ ) {
        calTableLine(h);
        if (isTex == yes) {
            //changeMach ((double) h );
            makeTexTableLine();
        }
        else {
            showResults();
        };
        h = h + delta;
    };
    return 1;
};


int isothermal:: manegerCalTableLine (int isTex, double P2P1){
    // manage the lines calcuations  
    // ferst if all exist.
    // to be complete later
		// first calcalute the parameters 
		// now print either for tex or without tex
	//first print the header depending on the tex or ''graph'' table
	if (isTex == yes) {
		makeTexTableHead();
	}
	else{
		showHead();
	};
	//make the line or the lines
	double singularPoint;
	singularPoint = 1/sqrt(k) ; 
	makeLines(isTex, 20,0.05,(singularPoint -0.001 ),P2P1);
	makeLines(isTex, 30,singularPoint+ 0.0000001,10,P2P1);
	makeLines(isTex, 30, 11, 100, P2P1);
	makeLines(isTex, 30, 110, 1000, P2P1);
	// the tail for tex if needed
	if (isTex == yes) {
        makeTexTableTail();
    };


    return 1;
};

int isothermal:: manegerCalTableLine (int isTex){
    // manage the lines calcuations  
    // ferst if all exist.
    // to be complete later
		// first calcalute the parameters 
		// now print either for tex or without tex
	//first print the header depending on the tex or ''graph'' table
	if (isTex == yes) {
		makeTexTableHead();
	}
	else{
		showHead();
	};
	//make the line or the lines
	double singularPoint;
	singularPoint = 1/sqrt(k) ; 
	makeLines(isTex, 20,0.05,(singularPoint -0.001 ));
	makeLines(isTex, 30,singularPoint+ 0.0000001,10);
	// the tail for tex if needed
	if (isTex == yes) {
        makeTexTableTail();
    };

    return 1;
};



int main( void )
{
	// iso is the name of the actual object and 1.4 is k.
	isothermal iso(0.0845, 1.4) ; // the value of first varible isn't used
	// when the manager in control  
	// the main function of calcualtion of p
	//iso.findM1ForFLD(40.0);
	//iso.calM1(400.0, 0.1); // calculate M1 fun of (fld,p2/p1)
	//iso.calP2P1 ();
	//iso.makeTexTableHead(); // turn off when the manager is in control
	iso.manegerCalTableLine (yes, 0.10); //yes with tex no without it.
							// the seconf parameter, is pressure ratio
	//iso.calTableLine( 0.00028 );	
	//iso.calTableLine( );	
	//iso.makeTexTableLine(); //turn off when the manager is on
	//iso.makeTexTableTail(); // turn off when the manager is on control

	// iso.showHead();
    //iso.showResults(); //just for table in (mx)grace
	return 0;
}
