#File: extractChap.awk
#1.0v    Written Nov 3, 2006 
#    By    Genick Bar-Meir 
#
# This is an AWK program to extract the chpater papes numbers. 
#
#  Usage: extractChap filename

cat $1 | awk 'BEGIN {ChapOn = 1; firstPage=0 ; \
		fpFlag= 0; lastPage=0;} \
(ChapOn == 1 && /Chapter/) {;  \
	printf (lastPage+0) ;  \
	printf "\n" ;  \
	printf $1 ; \
	printf "   "; \
	$2 = 0 + $2 ;\
	printf $2 ; printf "  "  \
#	print "first loop" ; \
	ChapOn = 0; next  } 	\
(ChapOn == 0 && /^[^A-Za-z0-9]*[1-9]*$/){; \
		lastChar = index($1,"]") ; \
		if (lastChar != 0) { lastChar=lastChar; \
		 firstPage=substr($1,2,lastChar);}  \
		else {lastChar=length($1)  ; \
			firstPage=substr($1,2,lastChar);}  \
		printf (firstPage+0) ;  \
		printf "	" ; \
#		print "second loop" ; \
		ChapOn = 1  ; \
		next;} \
(ChapOn == 1 && /^[^A-Z]*[0-9]$/){; \
		lastChar = index($1,"]") ; \
		if (lastChar != 0) { lastChar=lastChar; \
		 lastPage=substr($NF,2,lastChar);}  \
		else {lastChar=length($1)  ; \
			lastPage=substr($1,2,lastChar);}  \
#		printf "FP="; printf (firstPage+0) ;  \
#		printf "\n" ; \
		print $0 ; \
#		printf "$1="; print $1;	\
#		printf "\n" ; \
#		ChapOn = 1  ; \
		print "\n third loop" ; \
		next;} '
