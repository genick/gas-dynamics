all: html pdf
	echo "I am here"

html: ps
	 latex2html text
	
pdf: ps
	ps2pdf text.ps text.pdf

dvi: gasDynamics.cc
	latex text.tex && latex text.tex

gasDynamics.cc :
	./calculations/makeGasDynamics

ps: 
	latex text.tex && latex text.tex && dvips -o text.ps text.dvi
clean:
	rm -f *.aux *.toc *.ps *.dvi *.log
          
