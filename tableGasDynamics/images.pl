# LaTeX2HTML 2002-2-1 (1.71)
# Associate images original text with physical files.


$key = q/mathbf{{P_0}_yover{P_0}_x};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="34" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img14.png"
 ALT="$ \mathbf{{P_0}_y \over {P_0}_x } $">|; 

$key = q/mathbf{AtimesPoverA^{*}timesP_0};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img7.png"
 ALT="$ \mathbf{A\times P \over A^{*} \times P_0} $">|; 

$key = q/mathbf{M_y};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="34" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img10.png"
 ALT="$ \mathbf{M_y} $">|; 

$key = q/mathbf{PoverP^{star}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img6.png"
 ALT="$ \mathbf{P \over P^{\star}} $">|; 

$key = q/mathbf{T_0over{T_0}^{star}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="34" HEIGHT="43" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img3.png"
 ALT="$ \mathbf{T_0 \over {T_0}^{\star}} $">|; 

$key = q/mathbf{P_yoverP_x};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img13.png"
 ALT="$ \mathbf{P_y \over P_x} $">|; 

$key = q/mathbf{FoverF^{*}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img8.png"
 ALT="$ \mathbf{F \over F^{*}} $">|; 

$key = q/1slashsqrt{k};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="50" HEIGHT="44" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img24.png"
 ALT="$ 1/\sqrt{k}$">|; 

$key = q/mathbf{T_yoverT_x};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img11.png"
 ALT="$ \mathbf{T_y \over T_x} $">|; 

$key = q/mathbf{rho_yoverrho_x};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="40" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img12.png"
 ALT="$ \mathbf{\rho_y \over \rho_x} $">|; 

$key = q/rule[-.1in]{0.pt}{0.3in}mathbf{k};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="56" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img15.png"
 ALT="$ \rule[-.1in]{0.pt}{0.3 in}\mathbf{k} $">|; 

$key = q/theta;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="20" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img25.png"
 ALT="$ \theta$">|; 

$key = q/mathbf{4fLoverD};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="30" HEIGHT="43" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img22.png"
 ALT="$ \mathbf{4fL \over D} $">|; 

$key = q/mathbf{left.{4fL_{max}overD}right|_{1}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="72" HEIGHT="43" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img21.png"
 ALT="$ \mathbf{\left.{4fL_{max} \over D }\right\vert _{1} } $">|; 

$key = q/M_y;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="31" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img1.png"
 ALT="$ M_y$">|; 

$key = q/rule[-.1in]{0.pt}{0.3in}mathbf{M};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="56" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img2.png"
 ALT="$ \rule[-.1in]{0.pt}{0.3 in}\mathbf{M} $">|; 

$key = q/mathbf{P_0over{P_0}^{star}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="33" HEIGHT="43" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img4.png"
 ALT="$ \mathbf{P_0 \over {P_0}^{\star}} $">|; 

$key = q/mathbf{{M_x}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="34" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img16.png"
 ALT="$ \mathbf{{M_x}} $">|; 

$key = q/delta;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="20" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img26.png"
 ALT="$ \delta$">|; 

$key = q/rule[-.1in]{0.pt}{0.3in}mathbf{M_1};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="34" HEIGHT="56" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img19.png"
 ALT="$ \rule[-.1in]{0.pt}{0.3 in} \mathbf{M_1} $">|; 

$key = q/mathbf{M_2};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="34" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img20.png"
 ALT="$ \mathbf{M_2} $">|; 

$key = q/mathbf{P_2overP_1};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="43" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img23.png"
 ALT="$ \mathbf{P_2 \over P_1} $">|; 

$key = q/rule[-.1in]{0.pt}{0.3in}mathbf{M_x};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="34" HEIGHT="56" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img9.png"
 ALT="$ \rule[-.1in]{0.pt}{0.3 in} \mathbf{M_x} $">|; 

$key = q/mathbf{{M_y}^{'}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="39" HEIGHT="46" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img17.png"
 ALT="$ \mathbf{{M_y}^{'} } $">|; 

$key = q/mathbf{AoverA^{star}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img5.png"
 ALT="$ \mathbf{A \over A^{\star}} $">|; 

$key = q/mathbf{{T_y}over{T_x}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img18.png"
 ALT="$ \mathbf{{T_y} \over {T_x} } $">|; 

1;

