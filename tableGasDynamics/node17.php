<? include("header.php");?>
<table style="width:100%; height:993px;" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
	<? include("left_link.php");?>
	<td style="width: 80%; vertical-align: top;">
	<div  font-family: "arial", "helvetica", "lucida", "sans";>

<!--Navigation Panel-->
<A NAME="tex2html283"
  HREF="node18.php">
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next"
 SRC="figures/next.png"></A> 
<A NAME="tex2html279"
  HREF="node13.php">
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up"
 SRC="figures/up.png"></A> 
<A NAME="tex2html275"
  HREF="node16.php">
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous"
 SRC="figures/prev.png"></A> 
<A NAME="tex2html281"
  HREF="node1.php">
<IMG WIDTH="65" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="contents"
 SRC="figures/contents.png"></A>  
<BR>
<B> Next:</B> <A NAME="tex2html284"
  HREF="node18.php">Normal Shock</A>
<B> Up:</B> <A NAME="tex2html280"
  HREF="node13.php">Isothermal Nozzle</A>
<B> Previous:</B> <A NAME="tex2html276"
  HREF="node16.php">Isothermal Nozzle for k=1.4</A>
 &nbsp; <B>  <A NAME="tex2html282"
  HREF="node1.php">Contents</A></B> 
<BR>
<BR>
<!--End of Navigation Panel-->

<H2><A NAME="SECTION00034000000000000000">
Isothermal Nozzle for k=1.67</A>
</H2>

<P>

<table border=1 width="100%" >
  <thead>
    <tr>
      <th align=left bgcolor="#fffeaa" colspan=2 >Isothermal Nozzle Flow </th>
      <th align=left  bgcolor="#00ff5a" colspan=3 >Input: M </th>
      <th align=left  bgcolor="#9ae0ee" colspan=1 >k = 1.67 </th>
    </tr>
    <tr>
      <th align=center >M   </th>
      <th align=center >T0/T0* </th>
      <th align=center >P0/P0* </th>
      <th align=center >A/A* </th>
      <th align=center >P/P* </th>
      <th align=center >PAR </th>
      <th align=center >F/F* </th>
    </tr> 
  </thead>
  <tbody>
    <tr> 
      <td align=right > 1e-6      </td>
      <td align=right > 0.749064      </td>
      <td align=right > 1.12168      </td>
      <td align=right > 433874      </td>
      <td align=right > 2.30481      </td>
      <td align=right > 1e+6      </td>
      <td align=right > 374532      </td>
    </tr>
    <tr> 
      <td align=right > 0.0500005      </td>
      <td align=right > 0.749691      </td>
      <td align=right > 1.12168      </td>
      <td align=right > 8.69554      </td>
      <td align=right > 2.30001      </td>
      <td align=right > 19.9998      </td>
      <td align=right > 7.52184      </td>
    </tr>
    <tr> 
      <td align=right > 0.1      </td>
      <td align=right > 0.751573      </td>
      <td align=right > 1.12166      </td>
      <td align=right > 4.37513      </td>
      <td align=right > 2.28565      </td>
      <td align=right > 10      </td>
      <td align=right > 3.80787      </td>
    </tr>
    <tr> 
      <td align=right > 0.2      </td>
      <td align=right > 0.759101      </td>
      <td align=right > 1.12143      </td>
      <td align=right > 2.24305      </td>
      <td align=right > 2.2291      </td>
      <td align=right > 5      </td>
      <td align=right > 1.99775      </td>
    </tr>
    <tr> 
      <td align=right > 0.25      </td>
      <td align=right > 0.764747      </td>
      <td align=right > 1.12108      </td>
      <td align=right > 1.82847      </td>
      <td align=right > 2.18762      </td>
      <td align=right > 4      </td>
      <td align=right > 1.65449      </td>
    </tr>
    <tr> 
      <td align=right > 0.3      </td>
      <td align=right > 0.771648      </td>
      <td align=right > 1.12044      </td>
      <td align=right > 1.55912      </td>
      <td align=right > 2.13796      </td>
      <td align=right > 3.33333      </td>
      <td align=right > 1.43608      </td>
    </tr>
    <tr> 
      <td align=right > 0.35      </td>
      <td align=right > 0.779803      </td>
      <td align=right > 1.11939      </td>
      <td align=right > 1.37315      </td>
      <td align=right > 2.08072      </td>
      <td align=right > 2.85714      </td>
      <td align=right > 1.289      </td>
    </tr>
    <tr> 
      <td align=right > 0.4      </td>
      <td align=right > 0.789213      </td>
      <td align=right > 1.11781      </td>
      <td align=right > 1.23973      </td>
      <td align=right > 2.01657      </td>
      <td align=right > 2.5      </td>
      <td align=right > 1.18652      </td>
    </tr>
    <tr> 
      <td align=right > 0.45      </td>
      <td align=right > 0.799878      </td>
      <td align=right > 1.11554      </td>
      <td align=right > 1.14179      </td>
      <td align=right > 1.94627      </td>
      <td align=right > 2.22222      </td>
      <td align=right > 1.11375      </td>
    </tr>
    <tr> 
      <td align=right > 0.5      </td>
      <td align=right > 0.811798      </td>
      <td align=right > 1.11243      </td>
      <td align=right > 1.06919      </td>
      <td align=right > 1.87058      </td>
      <td align=right > 2      </td>
      <td align=right > 1.0618      </td>
    </tr>
    <tr> 
      <td align=right > 0.55      </td>
      <td align=right > 0.824972      </td>
      <td align=right > 1.10831      </td>
      <td align=right > 1.01554      </td>
      <td align=right > 1.79035      </td>
      <td align=right > 1.81818      </td>
      <td align=right > 1.02497      </td>
    </tr>
    <tr> 
      <td align=right > 0.6      </td>
      <td align=right > 0.839401      </td>
      <td align=right > 1.10301      </td>
      <td align=right > 0.976701      </td>
      <td align=right > 1.70642      </td>
      <td align=right > 1.66667      </td>
      <td align=right > 0.999501      </td>
    </tr>
    <tr> 
      <td align=right > 0.65      </td>
      <td align=right > 0.855084      </td>
      <td align=right > 1.09636      </td>
      <td align=right > 0.949871      </td>
      <td align=right > 1.61965      </td>
      <td align=right > 1.53846      </td>
      <td align=right > 0.982757      </td>
    </tr>
    <tr> 
      <td align=right > 0.7      </td>
      <td align=right > 0.872022      </td>
      <td align=right > 1.0882      </td>
      <td align=right > 0.933163      </td>
      <td align=right > 1.53089      </td>
      <td align=right > 1.42857      </td>
      <td align=right > 0.972873      </td>
    </tr>
    <tr> 
      <td align=right > 0.75      </td>
      <td align=right > 0.890215      </td>
      <td align=right > 1.07837      </td>
      <td align=right > 0.925306      </td>
      <td align=right > 1.44096      </td>
      <td align=right > 1.33333      </td>
      <td align=right > 0.968477      </td>
    </tr>
    <tr> 
      <td align=right > 0.8      </td>
      <td align=right > 0.909663      </td>
      <td align=right > 1.06674      </td>
      <td align=right > 0.925467      </td>
      <td align=right > 1.35067      </td>
      <td align=right > 1.25      </td>
      <td align=right > 0.968539      </td>
    </tr>
    <tr> 
      <td align=right > 0.85      </td>
      <td align=right > 0.930365      </td>
      <td align=right > 1.05317      </td>
      <td align=right > 0.933146      </td>
      <td align=right > 1.26076      </td>
      <td align=right > 1.17647      </td>
      <td align=right > 0.972274      </td>
    </tr>
    <tr> 
      <td align=right > 0.9      </td>
      <td align=right > 0.952322      </td>
      <td align=right > 1.03757      </td>
      <td align=right > 0.948106      </td>
      <td align=right > 1.17193      </td>
      <td align=right > 1.11111      </td>
      <td align=right > 0.979068      </td>
    </tr>
    <tr> 
      <td align=right > 0.95      </td>
      <td align=right > 0.975534      </td>
      <td align=right > 1.01987      </td>
      <td align=right > 0.97033      </td>
      <td align=right > 1.08482      </td>
      <td align=right > 1.05263      </td>
      <td align=right > 0.988439      </td>
    </tr>
    <tr> 
      <td align=right > 1      </td>
      <td align=right > 1      </td>
      <td align=right > 1      </td>
      <td align=right > 1      </td>
      <td align=right > 1      </td>
      <td align=right > 1      </td>
      <td align=right > 1      </td>
    </tr>
    <tr> 
      <td align=right > 1.05      </td>
      <td align=right > 1.02572      </td>
      <td align=right > 0.977959      </td>
      <td align=right > 1.03748      </td>
      <td align=right > 0.917973      </td>
      <td align=right > 0.952381      </td>
      <td align=right > 1.01344      </td>
    </tr>
    <tr> 
      <td align=right > 1.1      </td>
      <td align=right > 1.0527      </td>
      <td align=right > 0.953758      </td>
      <td align=right > 1.08333      </td>
      <td align=right > 0.839163      </td>
      <td align=right > 0.909091      </td>
      <td align=right > 1.0285      </td>
    </tr>
    <tr> 
      <td align=right > 1.15      </td>
      <td align=right > 1.08093      </td>
      <td align=right > 0.927446      </td>
      <td align=right > 1.13829      </td>
      <td align=right > 0.763924      </td>
      <td align=right > 0.869565      </td>
      <td align=right > 1.04497      </td>
    </tr>
    <tr> 
      <td align=right > 1.2      </td>
      <td align=right > 1.11041      </td>
      <td align=right > 0.899106      </td>
      <td align=right > 1.20331      </td>
      <td align=right > 0.692533      </td>
      <td align=right > 0.833333      </td>
      <td align=right > 1.06267      </td>
    </tr>
    <tr> 
      <td align=right > 1.25      </td>
      <td align=right > 1.14115      </td>
      <td align=right > 0.868856      </td>
      <td align=right > 1.2796      </td>
      <td align=right > 0.625198      </td>
      <td align=right > 0.8      </td>
      <td align=right > 1.08146      </td>
    </tr>
    <tr> 
      <td align=right > 1.3      </td>
      <td align=right > 1.17315      </td>
      <td align=right > 0.836843      </td>
      <td align=right > 1.3686      </td>
      <td align=right > 0.562058      </td>
      <td align=right > 0.769231      </td>
      <td align=right > 1.10121      </td>
    </tr>
    <tr> 
      <td align=right > 1.35      </td>
      <td align=right > 1.2064      </td>
      <td align=right > 0.803245      </td>
      <td align=right > 1.47209      </td>
      <td align=right > 0.50319      </td>
      <td align=right > 0.740741      </td>
      <td align=right > 1.12181      </td>
    </tr>
    <tr> 
      <td align=right > 1.4      </td>
      <td align=right > 1.2409      </td>
      <td align=right > 0.768265      </td>
      <td align=right > 1.59222      </td>
      <td align=right > 0.448611      </td>
      <td align=right > 0.714286      </td>
      <td align=right > 1.14318      </td>
    </tr>
    <tr> 
      <td align=right > 1.45      </td>
      <td align=right > 1.27666      </td>
      <td align=right > 0.73213      </td>
      <td align=right > 1.73156      </td>
      <td align=right > 0.398285      </td>
      <td align=right > 0.689655      </td>
      <td align=right > 1.16523      </td>
    </tr>
    <tr> 
      <td align=right > 1.5      </td>
      <td align=right > 1.31367      </td>
      <td align=right > 0.695083      </td>
      <td align=right > 1.89323      </td>
      <td align=right > 0.352132      </td>
      <td align=right > 0.666667      </td>
      <td align=right > 1.18789      </td>
    </tr>
    <tr> 
      <td align=right > 1.55      </td>
      <td align=right > 1.35194      </td>
      <td align=right > 0.657382      </td>
      <td align=right > 2.08097      </td>
      <td align=right > 0.31003      </td>
      <td align=right > 0.645161      </td>
      <td align=right > 1.21111      </td>
    </tr>
    <tr> 
      <td align=right > 1.6      </td>
      <td align=right > 1.39146      </td>
      <td align=right > 0.619291      </td>
      <td align=right > 2.29928      </td>
      <td align=right > 0.271824      </td>
      <td align=right > 0.625      </td>
      <td align=right > 1.23483      </td>
    </tr>
    <tr> 
      <td align=right > 1.65      </td>
      <td align=right > 1.43224      </td>
      <td align=right > 0.581076      </td>
      <td align=right > 2.55362      </td>
      <td align=right > 0.237334      </td>
      <td align=right > 0.606061      </td>
      <td align=right > 1.25901      </td>
    </tr>
    <tr> 
      <td align=right > 1.7      </td>
      <td align=right > 1.47427      </td>
      <td align=right > 0.543003      </td>
      <td align=right > 2.85058      </td>
      <td align=right > 0.206357      </td>
      <td align=right > 0.588235      </td>
      <td align=right > 1.28361      </td>
    </tr>
    <tr> 
      <td align=right > 1.75      </td>
      <td align=right > 1.51756      </td>
      <td align=right > 0.505328      </td>
      <td align=right > 3.19815      </td>
      <td align=right > 0.178675      </td>
      <td align=right > 0.571429      </td>
      <td align=right > 1.30859      </td>
    </tr>
    <tr> 
      <td align=right > 1.8      </td>
      <td align=right > 1.5621      </td>
      <td align=right > 0.468296      </td>
      <td align=right > 3.60605      </td>
      <td align=right > 0.154062      </td>
      <td align=right > 0.555556      </td>
      <td align=right > 1.33392      </td>
    </tr>
    <tr> 
      <td align=right > 1.85      </td>
      <td align=right > 1.60789      </td>
      <td align=right > 0.432134      </td>
      <td align=right > 4.08614      </td>
      <td align=right > 0.132286      </td>
      <td align=right > 0.540541      </td>
      <td align=right > 1.35957      </td>
    </tr>
    <tr> 
      <td align=right > 1.9      </td>
      <td align=right > 1.65494      </td>
      <td align=right > 0.397051      </td>
      <td align=right > 4.65293      </td>
      <td align=right > 0.113115      </td>
      <td align=right > 0.526316      </td>
      <td align=right > 1.38551      </td>
    </tr>
    <tr> 
      <td align=right > 1.95      </td>
      <td align=right > 1.70325      </td>
      <td align=right > 0.363231      </td>
      <td align=right > 5.32418      </td>
      <td align=right > 0.0963192      </td>
      <td align=right > 0.512821      </td>
      <td align=right > 1.41173      </td>
    </tr>
    <tr> 
      <td align=right > 2      </td>
      <td align=right > 1.75281      </td>
      <td align=right > 0.330834      </td>
      <td align=right > 6.12178      </td>
      <td align=right > 0.0816756      </td>
      <td align=right > 0.5      </td>
      <td align=right > 1.4382      </td>
    </tr>
    <tr> 
      <td align=right > 2.25      </td>
      <td align=right > 2.01943      </td>
      <td align=right > 0.193903      </td>
      <td align=right > 13.2137      </td>
      <td align=right > 0.033635      </td>
      <td align=right > 0.444444      </td>
      <td align=right > 1.57376      </td>
    </tr>
    <tr> 
      <td align=right > 2.5      </td>
      <td align=right > 2.31742      </td>
      <td align=right > 0.101379      </td>
      <td align=right > 32.0552      </td>
      <td align=right > 0.0124785      </td>
      <td align=right > 0.4      </td>
      <td align=right > 1.71348      </td>
    </tr>
    <tr> 
      <td align=right > 2.75      </td>
      <td align=right > 2.64677      </td>
      <td align=right > 0.0471887      </td>
      <td align=right > 87.1897      </td>
      <td align=right > 0.00417063      </td>
      <td align=right > 0.363636      </td>
      <td align=right > 1.85623      </td>
    </tr>
    <tr> 
      <td align=right > 3      </td>
      <td align=right > 3.00749      </td>
      <td align=right > 0.0195369      </td>
      <td align=right > 265.44      </td>
      <td align=right > 0.00125578      </td>
      <td align=right > 0.333333      </td>
      <td align=right > 2.00125      </td>
    </tr>
    <tr> 
      <td align=right > 3.25      </td>
      <td align=right > 3.39958      </td>
      <td align=right > 0.00719269      </td>
      <td align=right > 903.28      </td>
      <td align=right > 0.000340639      </td>
      <td align=right > 0.307692      </td>
      <td align=right > 2.14801      </td>
    </tr>
    <tr> 
      <td align=right > 3.5      </td>
      <td align=right > 3.82303      </td>
      <td align=right > 0.00235516      </td>
      <td align=right > 3432.3      </td>
      <td align=right > 8.32427e-5      </td>
      <td align=right > 0.285714      </td>
      <td align=right > 2.29615      </td>
    </tr>
    <tr> 
      <td align=right > 3.75      </td>
      <td align=right > 4.27786      </td>
      <td align=right > 0.000686157      </td>
      <td align=right > 14551.2      </td>
      <td align=right > 1.8326e-5      </td>
      <td align=right > 0.266667      </td>
      <td align=right > 2.44538      </td>
    </tr>
    <tr> 
      <td align=right > 4      </td>
      <td align=right > 4.76404      </td>
      <td align=right > 0.000177968      </td>
      <td align=right > 68782.6      </td>
      <td align=right > 3.63464e-6      </td>
      <td align=right > 0.25      </td>
      <td align=right > 2.59551      </td>
    </tr>
    <tr> 
      <td align=right > 4.25      </td>
      <td align=right > 5.2816      </td>
      <td align=right > 4.11193e-5      </td>
      <td align=right > 362315      </td>
      <td align=right > 6.49419e-7      </td>
      <td align=right > 0      </td>
      <td align=right > 2.74636      </td>
    </tr>
    <tr> 
      <td align=right > 4.5      </td>
      <td align=right > 5.83052      </td>
      <td align=right > 8.46864e-6      </td>
      <td align=right > 2.12583e+6      </td>
      <td align=right > 1.04534e-7      </td>
      <td align=right > 0      </td>
      <td align=right > 2.89784      </td>
    </tr>
    <tr> 
      <td align=right > 4.75      </td>
      <td align=right > 6.41081      </td>
      <td align=right > 1.5557e-6      </td>
      <td align=right > 1.38881e+7      </td>
      <td align=right > 1.51588e-8      </td>
      <td align=right > 0      </td>
      <td align=right > 3.04982      </td>
    </tr>
    <tr> 
      <td align=right > 5      </td>
      <td align=right > 7.02247      </td>
      <td align=right > 2.55062e-7      </td>
      <td align=right > 1.00993e+8      </td>
      <td align=right > 1.98033e-9      </td>
      <td align=right > 0      </td>
      <td align=right > 3.20225      </td>
    </tr>
    <tr> 
      <td align=right > 5.25      </td>
      <td align=right > 7.6655      </td>
      <td align=right > 3.73451e-8      </td>
      <td align=right > 8.17253e+8      </td>
      <td align=right > 2.33069e-10      </td>
      <td align=right > 0      </td>
      <td align=right > 3.35505      </td>
    </tr>
    <tr> 
      <td align=right > 5.5      </td>
      <td align=right > 8.33989      </td>
      <td align=right > 4.88569e-9      </td>
      <td align=right > 7.35762e+9      </td>
      <td align=right > 2.47116e-11      </td>
      <td align=right > 0      </td>
      <td align=right > 3.50817      </td>
    </tr>
    <tr> 
      <td align=right > 5.75      </td>
      <td align=right > 9.04565      </td>
      <td align=right > 5.7141e-10      </td>
      <td align=right > 7.36793e+10      </td>
      <td align=right > 2.36041e-12      </td>
      <td align=right > 0      </td>
      <td align=right > 3.66158      </td>
    </tr>
    <tr> 
      <td align=right > 6      </td>
      <td align=right > 9.78277      </td>
      <td align=right > 5.97733e-11      </td>
      <td align=right > 8.2055e+11      </td>
      <td align=right > 2.03116e-13      </td>
      <td align=right > 0      </td>
      <td align=right > 3.81523      </td>
    </tr>
    <tr> 
      <td align=right > 6.25      </td>
      <td align=right > 10.5513      </td>
      <td align=right > 5.59495e-12      </td>
      <td align=right > 1.01613e+13      </td>
      <td align=right > 1.5746e-14      </td>
      <td align=right > 0      </td>
      <td align=right > 3.9691      </td>
    </tr>
    <tr> 
      <td align=right > 6.5      </td>
      <td align=right > 11.3511      </td>
      <td align=right > 4.68806e-13      </td>
      <td align=right > 1.399e+14      </td>
      <td align=right > 1.09969e-15      </td>
      <td align=right > 0      </td>
      <td align=right > 4.12316      </td>
    </tr>
    <tr> 
      <td align=right > 6.75      </td>
      <td align=right > 12.1824      </td>
      <td align=right > 3.51774e-14      </td>
      <td align=right > 2.1412e+15      </td>
      <td align=right > 6.91892e-17      </td>
      <td align=right > 0      </td>
      <td align=right > 4.2774      </td>
    </tr>
    <tr> 
      <td align=right > 7      </td>
      <td align=right > 13.0449      </td>
      <td align=right > 2.3646e-15      </td>
      <td align=right > 3.64271e+16      </td>
      <td align=right > 3.92173e-18      </td>
      <td align=right > 0      </td>
      <td align=right > 4.43178      </td>
    </tr>
    <tr> 
      <td align=right > 7.25      </td>
      <td align=right > 13.9389      </td>
      <td align=right > 1.42436e-16      </td>
      <td align=right > 6.88772e+17      </td>
      <td align=right > 2.00257e-19      </td>
      <td align=right > 0      </td>
      <td align=right > 4.5863      </td>
    </tr>
    <tr> 
      <td align=right > 7.5      </td>
      <td align=right > 14.8642      </td>
      <td align=right > 7.69088e-18      </td>
      <td align=right > 1.44734e+19      </td>
      <td align=right > 9.21228e-21      </td>
      <td align=right > 0      </td>
      <td align=right > 4.74095      </td>
    </tr>
    <tr> 
      <td align=right > 7.75      </td>
      <td align=right > 15.8209      </td>
      <td align=right > 3.72347e-19      </td>
      <td align=right > 3.37972e+20      </td>
      <td align=right > 3.81784e-22      </td>
      <td align=right > 0      </td>
      <td align=right > 4.8957      </td>
    </tr>
    <tr> 
      <td align=right > 8      </td>
      <td align=right > 16.809      </td>
      <td align=right > 1.61677e-20      </td>
      <td align=right > 8.76943e+21      </td>
      <td align=right > 1.42541e-23      </td>
      <td align=right > 0      </td>
      <td align=right > 5.05056      </td>
    </tr>
    <tr> 
      <td align=right > 8.25      </td>
      <td align=right > 17.8284      </td>
      <td align=right > 6.29761e-22      </td>
      <td align=right > 2.52822e+23      </td>
      <td align=right > 4.79436e-25      </td>
      <td align=right > 0      </td>
      <td align=right > 5.20551      </td>
    </tr>
    <tr> 
      <td align=right > 8.5      </td>
      <td align=right > 18.8792      </td>
      <td align=right > 2.20105e-23      </td>
      <td align=right > 8.0982e+24      </td>
      <td align=right > 1.45276e-26      </td>
      <td align=right > 0      </td>
      <td align=right > 5.36054      </td>
    </tr>
    <tr> 
      <td align=right > 8.75      </td>
      <td align=right > 19.9614      </td>
      <td align=right > 6.90397e-25      </td>
      <td align=right > 2.88182e+26      </td>
      <td align=right > 3.96575e-28      </td>
      <td align=right > 0      </td>
      <td align=right > 5.51565      </td>
    </tr>
    <tr> 
      <td align=right > 9      </td>
      <td align=right > 21.0749      </td>
      <td align=right > 1.94386e-26      </td>
      <td align=right > 1.13928e+28      </td>
      <td align=right > 9.75278e-30      </td>
      <td align=right > 0      </td>
      <td align=right > 5.67083      </td>
    </tr>
    <tr> 
      <td align=right > 9.25      </td>
      <td align=right > 22.2198      </td>
      <td align=right > 4.91363e-28      </td>
      <td align=right > 5.00329e+29      </td>
      <td align=right > 2.16074e-31      </td>
      <td align=right > 0      </td>
      <td align=right > 5.82607      </td>
    </tr>
    <tr> 
      <td align=right > 9.5      </td>
      <td align=right > 23.3961      </td>
      <td align=right > 1.11529e-29      </td>
      <td align=right > 2.44079e+31      </td>
      <td align=right > 4.31267e-33      </td>
      <td align=right > 0      </td>
      <td align=right > 5.98137      </td>
    </tr>
    <tr> 
      <td align=right > 9.75      </td>
      <td align=right > 24.6037      </td>
      <td align=right > 2.27343e-31      </td>
      <td align=right > 1.32262e+33      </td>
      <td align=right > 7.75464e-35      </td>
      <td align=right > 0      </td>
      <td align=right > 6.13673      </td>
    </tr>
    <tr> 
      <td align=right > 10      </td>
      <td align=right > 25.8427      </td>
      <td align=right > 4.16247e-33      </td>
      <td align=right > 7.96074e+34      </td>
      <td align=right > 1.25616e-36      </td>
      <td align=right > 0      </td>
      <td align=right > 6.29213      </td>
    </tr>
    <tr> 
      <td align=right > 20      </td>
      <td align=right > 101.124      </td>
      <td align=right > 2.02045e-140      </td>
      <td align=right > 2.45862e+143      </td>
      <td align=right > 2.03366e-145      </td>
      <td align=right > 0      </td>
      <td align=right > 12.5281      </td>
    </tr>
    <tr> 
      <td align=right > 30      </td>
      <td align=right > 226.592      </td>
      <td align=right > 0      </td>
      <td align=right > inf      </td>
      <td align=right > 0      </td>
      <td align=right > 0      </td>
      <td align=right > 18.7765      </td>
    </tr>
    <tr> 
      <td align=right > 40      </td>
      <td align=right > 402.247      </td>
      <td align=right > 0      </td>
      <td align=right > inf      </td>
      <td align=right > 0      </td>
      <td align=right > 0      </td>
      <td align=right > 25.0281      </td>
    </tr>
    <tr> 
      <td align=right > 50      </td>
      <td align=right > 628.09      </td>
      <td align=right > 0      </td>
      <td align=right > inf      </td>
      <td align=right > 0      </td>
      <td align=right > 0      </td>
      <td align=right > 31.2809      </td>
    </tr>
  </tbody>
</table>
<BR><HR>
<ADDRESS>
genick
2007-11-14
</ADDRESS>
        </tr>
    </tbody>
    </table>
    <? include("aboutPottoProject.php");  ?>
    </div>
    </td>
    </tr>
    </tbody>
</table>
<? include("bottom.php"); ?>

