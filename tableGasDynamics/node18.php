<? include("header.php");?>
<table style="width:100%; height:993px;" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
	<? include("left_link.php");?>
	<td style="width: 80%; vertical-align: top;">
	<div  font-family: "arial", "helvetica", "lucida", "sans";>

<!--Navigation Panel-->
<A NAME="tex2html295"
  HREF="node19.php">
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next"
 SRC="figures/next.png"></A> 
<A NAME="tex2html291"
  HREF="tableGasDynamics.php">
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up"
 SRC="figures/up.png"></A> 
<A NAME="tex2html285"
  HREF="node17.php">
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous"
 SRC="figures/prev.png"></A> 
<A NAME="tex2html293"
  HREF="node1.php">
<IMG WIDTH="65" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="contents"
 SRC="figures/contents.png"></A>  
<BR>
<B> Next:</B> <A NAME="tex2html296"
  HREF="node19.php">Normal Shock Standard tables</A>
<B> Up:</B> <A NAME="tex2html292"
  HREF="tableGasDynamics.php">tableGasDynamics</A>
<B> Previous:</B> <A NAME="tex2html286"
  HREF="node17.php">Isothermal Nozzle for k=1.67</A>
 &nbsp; <B>  <A NAME="tex2html294"
  HREF="node1.php">Contents</A></B> 
<BR>
<BR>
<!--End of Navigation Panel-->

<H1><A NAME="SECTION00040000000000000000"></A>
<A NAME="chap:shock"></A>
<BR>
Normal Shock 
</H1>

<P>
<BR><HR>
<!--Table of Child-Links-->
<A NAME="CHILD_LINKS"><STRONG>Subsections</STRONG></A>

<UL>
<LI><A NAME="tex2html297"
  HREF="node19.php">Normal Shock Standard tables</A>
<UL>
<LI><A NAME="tex2html298"
  HREF="node20.php">Normal Shock Standard Table for k=1.2</A>
<LI><A NAME="tex2html299"
  HREF="node21.php">Normal Shock Standard Table for k=1.3</A>
<LI><A NAME="tex2html300"
  HREF="node22.php">Normal Shock Standard Table for k=1.4</A>
<LI><A NAME="tex2html301"
  HREF="node23.php">Normal shock Standard Table for k=1.67</A>
</UL>
<BR>
<LI><A NAME="tex2html302"
  HREF="node24.php">Open Valve maximum values</A>
<LI><A NAME="tex2html303"
  HREF="node25.php">Shock Reflecting from a Suddenly Closed Valve</A>
<UL>
<LI><A NAME="tex2html304"
  HREF="node26.php">Reflecting Shock (closed Valve) k =1.2</A>
<LI><A NAME="tex2html305"
  HREF="node27.php">Reflecting Shock (closed Valve) k =1.3</A>
<LI><A NAME="tex2html306"
  HREF="node28.php">Reflecting Shock (closed Valve) k =1.4</A>
<LI><A NAME="tex2html307"
  HREF="node29.php">Reflecting Shock (closed Valve) k =1.67</A>
</UL>
<BR>
<LI><A NAME="tex2html308"
  HREF="node30.php">Shock propagating from suddenly open valve</A>
<UL>
<LI><A NAME="tex2html309"
  HREF="node31.php">Shock from suddenly open valve k =1.2</A>
<LI><A NAME="tex2html310"
  HREF="node32.php">Shock from suddenly open valve k =1.3</A>
<LI><A NAME="tex2html311"
  HREF="node33.php">Shock from suddenly open valve k =1.4</A>
<LI><A NAME="tex2html312"
  HREF="node34.php">Shock from suddenly open valve k =1.67</A>
</UL></UL>
<!--End of Table of Child-Links-->
<BR><HR>
<ADDRESS>
genick
2007-11-14
</ADDRESS>
        </tr>
    </tbody>
    </table>
    <? include("aboutPottoProject.php");  ?>
    </div>
    </td>
    </tr>
    </tbody>
</table>
<? include("bottom.php"); ?>

