<? include("header.php");?>
<table style="width:100%; height:993px;" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
	<? include("left_link.php");?>
	<td style="width: 80%; vertical-align: top;">
	<div  font-family: "arial", "helvetica", "lucida", "sans";>

<!--Navigation Panel-->
<A NAME="tex2html139"
  HREF="node6.php">
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next"
 SRC="figures/next.png"></A> 
<A NAME="tex2html135"
  HREF="node3.php">
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up"
 SRC="figures/up.png"></A> 
<A NAME="tex2html129"
  HREF="node4.php">
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous"
 SRC="figures/prev.png"></A> 
<A NAME="tex2html137"
  HREF="node1.php">
<IMG WIDTH="65" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="contents"
 SRC="figures/contents.png"></A>  
<BR>
<B> Next:</B> <A NAME="tex2html140"
  HREF="node6.php">Isentropic Flow for k</A>
<B> Up:</B> <A NAME="tex2html136"
  HREF="node3.php">Regular Isentropic Flow Tables</A>
<B> Previous:</B> <A NAME="tex2html130"
  HREF="node4.php">Isentropic Flow for k=1.2</A>
 &nbsp; <B>  <A NAME="tex2html138"
  HREF="node1.php">Contents</A></B> 
<BR>
<BR>
<!--End of Navigation Panel-->

<H3><A NAME="SECTION00021200000000000000">
Isentropic Flow for k=1.3</A>
</H3>


<table border=1 width="100%" >
  <thead>
    <tr>
      <th align=left bgcolor="#fffeaa" colspan=2 >Isentropic Flow </th>
      <th align=left  bgcolor="#00ff5a" colspan=3 >Input: M </th>
      <th align=left  bgcolor="#9ae0ee" colspan=1 >k = 1.3 </th>
    </tr>
    <tr>
      <th align=center >M   </th>
      <th align=center >T/T0 </th>
      <th align=center >&rho;/&rho;0 </th>
      <th align=center >A/A* </th>
      <th align=center >P/P0 </th>
      <th align=center >PAR </th>
      <th align=center >F/F* </th>
    </tr> 
  </thead>
  <tbody>
    <tr> 
      <td align=right > 1e-6      </td>
      <td align=right > 1      </td>
      <td align=right > 1      </td>
      <td align=right > 585228      </td>
      <td align=right > 1      </td>
      <td align=right > 585228      </td>
      <td align=right > 254447      </td>
    </tr>
    <tr> 
      <td align=right > 0.0500005      </td>
      <td align=right > 0.999625      </td>
      <td align=right > 0.998751      </td>
      <td align=right > 11.7213      </td>
      <td align=right > 0.998377      </td>
      <td align=right > 11.7022      </td>
      <td align=right > 5.10447      </td>
    </tr>
    <tr> 
      <td align=right > 0.1      </td>
      <td align=right > 0.998502      </td>
      <td align=right > 0.995016      </td>
      <td align=right > 5.886      </td>
      <td align=right > 0.993526      </td>
      <td align=right > 5.84789      </td>
      <td align=right > 2.57562      </td>
    </tr>
    <tr> 
      <td align=right > 0.2      </td>
      <td align=right > 0.994036      </td>
      <td align=right > 0.980257      </td>
      <td align=right > 2.99401      </td>
      <td align=right > 0.974411      </td>
      <td align=right > 2.9174      </td>
      <td align=right > 1.33439      </td>
    </tr>
    <tr> 
      <td align=right > 0.25      </td>
      <td align=right > 0.990712      </td>
      <td align=right > 0.969374      </td>
      <td align=right > 2.42616      </td>
      <td align=right > 0.960371      </td>
      <td align=right > 2.33001      </td>
      <td align=right > 1.09536      </td>
    </tr>
    <tr> 
      <td align=right > 0.3      </td>
      <td align=right > 0.98668      </td>
      <td align=right > 0.956285      </td>
      <td align=right > 2.05366      </td>
      <td align=right > 0.943547      </td>
      <td align=right > 1.93772      </td>
      <td align=right > 0.94106      </td>
    </tr>
    <tr> 
      <td align=right > 0.35      </td>
      <td align=right > 0.981957      </td>
      <td align=right > 0.941111      </td>
      <td align=right > 1.79296      </td>
      <td align=right > 0.92413      </td>
      <td align=right > 1.65693      </td>
      <td align=right > 0.835127      </td>
    </tr>
    <tr> 
      <td align=right > 0.4      </td>
      <td align=right > 0.976562      </td>
      <td align=right > 0.923989      </td>
      <td align=right > 1.60232      </td>
      <td align=right > 0.902333      </td>
      <td align=right > 1.44582      </td>
      <td align=right > 0.759371      </td>
    </tr>
    <tr> 
      <td align=right > 0.45      </td>
      <td align=right > 0.97052      </td>
      <td align=right > 0.90507      </td>
      <td align=right > 1.45857      </td>
      <td align=right > 0.878389      </td>
      <td align=right > 1.28119      </td>
      <td align=right > 0.703682      </td>
    </tr>
    <tr> 
      <td align=right > 0.5      </td>
      <td align=right > 0.963855      </td>
      <td align=right > 0.884517      </td>
      <td align=right > 1.34785      </td>
      <td align=right > 0.852547      </td>
      <td align=right > 1.14911      </td>
      <td align=right > 0.661986      </td>
    </tr>
    <tr> 
      <td align=right > 0.55      </td>
      <td align=right > 0.956595      </td>
      <td align=right > 0.862501      </td>
      <td align=right > 1.26136      </td>
      <td align=right > 0.825064      </td>
      <td align=right > 1.0407      </td>
      <td align=right > 0.630416      </td>
    </tr>
    <tr> 
      <td align=right > 0.6      </td>
      <td align=right > 0.948767      </td>
      <td align=right > 0.839198      </td>
      <td align=right > 1.19324      </td>
      <td align=right > 0.796203      </td>
      <td align=right > 0.950065      </td>
      <td align=right > 0.606389      </td>
    </tr>
    <tr> 
      <td align=right > 0.65      </td>
      <td align=right > 0.940402      </td>
      <td align=right > 0.814789      </td>
      <td align=right > 1.13949      </td>
      <td align=right > 0.766229      </td>
      <td align=right > 0.873109      </td>
      <td align=right > 0.588115      </td>
    </tr>
    <tr> 
      <td align=right > 0.7      </td>
      <td align=right > 0.931532      </td>
      <td align=right > 0.789453      </td>
      <td align=right > 1.09724      </td>
      <td align=right > 0.735401      </td>
      <td align=right > 0.806911      </td>
      <td align=right > 0.57431      </td>
    </tr>
    <tr> 
      <td align=right > 0.75      </td>
      <td align=right > 0.92219      </td>
      <td align=right > 0.76337      </td>
      <td align=right > 1.06443      </td>
      <td align=right > 0.703972      </td>
      <td align=right > 0.749331      </td>
      <td align=right > 0.564035      </td>
    </tr>
    <tr> 
      <td align=right > 0.8      </td>
      <td align=right > 0.912409      </td>
      <td align=right > 0.736713      </td>
      <td align=right > 1.03954      </td>
      <td align=right > 0.672183      </td>
      <td align=right > 0.698763      </td>
      <td align=right > 0.55658      </td>
    </tr>
    <tr> 
      <td align=right > 0.85      </td>
      <td align=right > 0.902222      </td>
      <td align=right > 0.70965      </td>
      <td align=right > 1.02142      </td>
      <td align=right > 0.640262      </td>
      <td align=right > 0.653977      </td>
      <td align=right > 0.551402      </td>
    </tr>
    <tr> 
      <td align=right > 0.9      </td>
      <td align=right > 0.891663      </td>
      <td align=right > 0.682342      </td>
      <td align=right > 1.00921      </td>
      <td align=right > 0.60842      </td>
      <td align=right > 0.61402      </td>
      <td align=right > 0.54808      </td>
    </tr>
    <tr> 
      <td align=right > 0.95      </td>
      <td align=right > 0.880766      </td>
      <td align=right > 0.654941      </td>
      <td align=right > 1.00223      </td>
      <td align=right > 0.57685      </td>
      <td align=right > 0.578138      </td>
      <td align=right > 0.546278      </td>
    </tr>
    <tr> 
      <td align=right > 1      </td>
      <td align=right > 0.869565      </td>
      <td align=right > 0.627587      </td>
      <td align=right > 1      </td>
      <td align=right > 0.545728      </td>
      <td align=right > 0.545728      </td>
      <td align=right > 0.545728      </td>
    </tr>
    <tr> 
      <td align=right > 1.05      </td>
      <td align=right > 0.858093      </td>
      <td align=right > 0.60041      </td>
      <td align=right > 1.00212      </td>
      <td align=right > 0.515207      </td>
      <td align=right > 0.516301      </td>
      <td align=right > 0.546213      </td>
    </tr>
    <tr> 
      <td align=right > 1.1      </td>
      <td align=right > 0.846382      </td>
      <td align=right > 0.573528      </td>
      <td align=right > 1.00831      </td>
      <td align=right > 0.485423      </td>
      <td align=right > 0.489458      </td>
      <td align=right > 0.547554      </td>
    </tr>
    <tr> 
      <td align=right > 1.15      </td>
      <td align=right > 0.834463      </td>
      <td align=right > 0.547046      </td>
      <td align=right > 1.01836      </td>
      <td align=right > 0.45649      </td>
      <td align=right > 0.464869      </td>
      <td align=right > 0.549607      </td>
    </tr>
    <tr> 
      <td align=right > 1.2      </td>
      <td align=right > 0.822368      </td>
      <td align=right > 0.52106      </td>
      <td align=right > 1.0321      </td>
      <td align=right > 0.428504      </td>
      <td align=right > 0.442259      </td>
      <td align=right > 0.552247      </td>
    </tr>
    <tr> 
      <td align=right > 1.25      </td>
      <td align=right > 0.810127      </td>
      <td align=right > 0.495651      </td>
      <td align=right > 1.04945      </td>
      <td align=right > 0.40154      </td>
      <td align=right > 0.421397      </td>
      <td align=right > 0.555374      </td>
    </tr>
    <tr> 
      <td align=right > 1.3      </td>
      <td align=right > 0.797766      </td>
      <td align=right > 0.470889      </td>
      <td align=right > 1.07035      </td>
      <td align=right > 0.37566      </td>
      <td align=right > 0.402086      </td>
      <td align=right > 0.5589      </td>
    </tr>
    <tr> 
      <td align=right > 1.35      </td>
      <td align=right > 0.785315      </td>
      <td align=right > 0.446833      </td>
      <td align=right > 1.09477      </td>
      <td align=right > 0.350905      </td>
      <td align=right > 0.384161      </td>
      <td align=right > 0.562754      </td>
    </tr>
    <tr> 
      <td align=right > 1.4      </td>
      <td align=right > 0.772798      </td>
      <td align=right > 0.423532      </td>
      <td align=right > 1.12274      </td>
      <td align=right > 0.327304      </td>
      <td align=right > 0.367477      </td>
      <td align=right > 0.566873      </td>
    </tr>
    <tr> 
      <td align=right > 1.45      </td>
      <td align=right > 0.760239      </td>
      <td align=right > 0.401022      </td>
      <td align=right > 1.15429      </td>
      <td align=right > 0.304873      </td>
      <td align=right > 0.35191      </td>
      <td align=right > 0.571204      </td>
    </tr>
    <tr> 
      <td align=right > 1.5      </td>
      <td align=right > 0.747664      </td>
      <td align=right > 0.379333      </td>
      <td align=right > 1.18949      </td>
      <td align=right > 0.283614      </td>
      <td align=right > 0.337355      </td>
      <td align=right > 0.575703      </td>
    </tr>
    <tr> 
      <td align=right > 1.55      </td>
      <td align=right > 0.735091      </td>
      <td align=right > 0.358485      </td>
      <td align=right > 1.22843      </td>
      <td align=right > 0.263519      </td>
      <td align=right > 0.323716      </td>
      <td align=right > 0.580331      </td>
    </tr>
    <tr> 
      <td align=right > 1.6      </td>
      <td align=right > 0.722543      </td>
      <td align=right > 0.33849      </td>
      <td align=right > 1.27124      </td>
      <td align=right > 0.244574      </td>
      <td align=right > 0.310912      </td>
      <td align=right > 0.585055      </td>
    </tr>
    <tr> 
      <td align=right > 1.65      </td>
      <td align=right > 0.710038      </td>
      <td align=right > 0.319354      </td>
      <td align=right > 1.31804      </td>
      <td align=right > 0.226754      </td>
      <td align=right > 0.29887      </td>
      <td align=right > 0.589845      </td>
    </tr>
    <tr> 
      <td align=right > 1.7      </td>
      <td align=right > 0.697593      </td>
      <td align=right > 0.301075      </td>
      <td align=right > 1.36899      </td>
      <td align=right > 0.210028      </td>
      <td align=right > 0.287526      </td>
      <td align=right > 0.594679      </td>
    </tr>
    <tr> 
      <td align=right > 1.75      </td>
      <td align=right > 0.685225      </td>
      <td align=right > 0.283646      </td>
      <td align=right > 1.42427      </td>
      <td align=right > 0.194361      </td>
      <td align=right > 0.276824      </td>
      <td align=right > 0.599534      </td>
    </tr>
    <tr> 
      <td align=right > 1.8      </td>
      <td align=right > 0.672948      </td>
      <td align=right > 0.267057      </td>
      <td align=right > 1.48408      </td>
      <td align=right > 0.179715      </td>
      <td align=right > 0.266712      </td>
      <td align=right > 0.604393      </td>
    </tr>
    <tr> 
      <td align=right > 1.85      </td>
      <td align=right > 0.660775      </td>
      <td align=right > 0.251292      </td>
      <td align=right > 1.54863      </td>
      <td align=right > 0.166047      </td>
      <td align=right > 0.257146      </td>
      <td align=right > 0.609241      </td>
    </tr>
    <tr> 
      <td align=right > 1.9      </td>
      <td align=right > 0.648719      </td>
      <td align=right > 0.236331      </td>
      <td align=right > 1.61816      </td>
      <td align=right > 0.153313      </td>
      <td align=right > 0.248084      </td>
      <td align=right > 0.614063      </td>
    </tr>
    <tr> 
      <td align=right > 1.95      </td>
      <td align=right > 0.636791      </td>
      <td align=right > 0.222155      </td>
      <td align=right > 1.69292      </td>
      <td align=right > 0.141466      </td>
      <td align=right > 0.239491      </td>
      <td align=right > 0.618849      </td>
    </tr>
    <tr> 
      <td align=right > 2      </td>
      <td align=right > 0.625      </td>
      <td align=right > 0.208737      </td>
      <td align=right > 1.77319      </td>
      <td align=right > 0.130461      </td>
      <td align=right > 0.231332      </td>
      <td align=right > 0.62359      </td>
    </tr>
    <tr> 
      <td align=right > 2.25      </td>
      <td align=right > 0.568384      </td>
      <td align=right > 0.152103      </td>
      <td align=right > 2.26821      </td>
      <td align=right > 0.086453      </td>
      <td align=right > 0.196094      </td>
      <td align=right > 0.646363      </td>
    </tr>
    <tr> 
      <td align=right > 2.5      </td>
      <td align=right > 0.516129      </td>
      <td align=right > 0.110288      </td>
      <td align=right > 2.95446      </td>
      <td align=right > 0.0569228      </td>
      <td align=right > 0.168176      </td>
      <td align=right > 0.66722      </td>
    </tr>
    <tr> 
      <td align=right > 2.75      </td>
      <td align=right > 0.468521      </td>
      <td align=right > 0.0798787      </td>
      <td align=right > 3.89221      </td>
      <td align=right > 0.0374249      </td>
      <td align=right > 0.145666      </td>
      <td align=right > 0.685974      </td>
    </tr>
    <tr> 
      <td align=right > 3      </td>
      <td align=right > 0.425532      </td>
      <td align=right > 0.0579572      </td>
      <td align=right > 5.15977      </td>
      <td align=right > 0.0246626      </td>
      <td align=right > 0.127254      </td>
      <td align=right > 0.702661      </td>
    </tr>
    <tr> 
      <td align=right > 3.25      </td>
      <td align=right > 0.386941      </td>
      <td align=right > 0.0422164      </td>
      <td align=right > 6.85706      </td>
      <td align=right > 0.0163353      </td>
      <td align=right > 0.112012      </td>
      <td align=right > 0.717423      </td>
    </tr>
    <tr> 
      <td align=right > 3.5      </td>
      <td align=right > 0.352423      </td>
      <td align=right > 0.0309182      </td>
      <td align=right > 9.10985      </td>
      <td align=right > 0.0108963      </td>
      <td align=right > 0.0992634      </td>
      <td align=right > 0.730449      </td>
    </tr>
    <tr> 
      <td align=right > 3.75      </td>
      <td align=right > 0.321608      </td>
      <td align=right > 0.0227906      </td>
      <td align=right > 12.0746      </td>
      <td align=right > 0.00732965      </td>
      <td align=right > 0.0885028      </td>
      <td align=right > 0.741933      </td>
    </tr>
    <tr> 
      <td align=right > 4      </td>
      <td align=right > 0.294118      </td>
      <td align=right > 0.0169201      </td>
      <td align=right > 15.9441      </td>
      <td align=right > 0.00497651      </td>
      <td align=right > 0.0793461      </td>
      <td align=right > 0.752063      </td>
    </tr>
    <tr> 
      <td align=right > 4.25      </td>
      <td align=right > 0.269587      </td>
      <td align=right > 0.012657      </td>
      <td align=right > 20.9534      </td>
      <td align=right > 0.00341217      </td>
      <td align=right > 0.0714966      </td>
      <td align=right > 0.761012      </td>
    </tr>
    <tr> 
      <td align=right > 4.5      </td>
      <td align=right > 0.247678      </td>
      <td align=right > 0.00954168      </td>
      <td align=right > 27.387      </td>
      <td align=right > 0.00236326      </td>
      <td align=right > 0.0647226      </td>
      <td align=right > 0.768933      </td>
    </tr>
    <tr> 
      <td align=right > 4.75      </td>
      <td align=right > 0.228083      </td>
      <td align=right > 0.00724949      </td>
      <td align=right > 35.5859      </td>
      <td align=right > 0.00165348      </td>
      <td align=right > 0.0588407      </td>
      <td align=right > 0.775961      </td>
    </tr>
    <tr> 
      <td align=right > 5      </td>
      <td align=right > 0.210526      </td>
      <td align=right > 0.00555079      </td>
      <td align=right > 45.9565      </td>
      <td align=right > 0.00116859      </td>
      <td align=right > 0.0537042      </td>
      <td align=right > 0.782213      </td>
    </tr>
    <tr> 
      <td align=right > 5.25      </td>
      <td align=right > 0.194766      </td>
      <td align=right > 0.00428261      </td>
      <td align=right > 58.9795      </td>
      <td align=right > 0.000834105      </td>
      <td align=right > 0.0491951      </td>
      <td align=right > 0.78779      </td>
    </tr>
    <tr> 
      <td align=right > 5.5      </td>
      <td align=right > 0.180587      </td>
      <td align=right > 0.0033288      </td>
      <td align=right > 75.2197      </td>
      <td align=right > 0.000601137      </td>
      <td align=right > 0.0452174      </td>
      <td align=right > 0.792779      </td>
    </tr>
    <tr> 
      <td align=right > 5.75      </td>
      <td align=right > 0.167803      </td>
      <td align=right > 0.00260614      </td>
      <td align=right > 95.3366      </td>
      <td align=right > 0.000437318      </td>
      <td align=right > 0.0416924      </td>
      <td align=right > 0.797254      </td>
    </tr>
    <tr> 
      <td align=right > 6      </td>
      <td align=right > 0.15625      </td>
      <td align=right > 0.00205463      </td>
      <td align=right > 120.096      </td>
      <td align=right > 0.000321036      </td>
      <td align=right > 0.0385553      </td>
      <td align=right > 0.801279      </td>
    </tr>
    <tr> 
      <td align=right > 6.25      </td>
      <td align=right > 0.145786      </td>
      <td align=right > 0.00163074      </td>
      <td align=right > 150.384      </td>
      <td align=right > 0.000237739      </td>
      <td align=right > 0.0357522      </td>
      <td align=right > 0.80491      </td>
    </tr>
    <tr> 
      <td align=right > 6.5      </td>
      <td align=right > 0.136286      </td>
      <td align=right > 0.00130269      </td>
      <td align=right > 187.217      </td>
      <td align=right > 0.000177538      </td>
      <td align=right > 0.0332382      </td>
      <td align=right > 0.808194      </td>
    </tr>
    <tr> 
      <td align=right > 6.75      </td>
      <td align=right > 0.127643      </td>
      <td align=right > 0.00104709      </td>
      <td align=right > 231.76      </td>
      <td align=right > 0.000133654      </td>
      <td align=right > 0.0309755      </td>
      <td align=right > 0.811172      </td>
    </tr>
    <tr> 
      <td align=right > 7      </td>
      <td align=right > 0.11976      </td>
      <td align=right > 0.000846665      </td>
      <td align=right > 285.337      </td>
      <td align=right > 0.000101397      </td>
      <td align=right > 0.0289323      </td>
      <td align=right > 0.81388      </td>
    </tr>
    <tr> 
      <td align=right > 7.25      </td>
      <td align=right > 0.112557      </td>
      <td align=right > 0.00068851      </td>
      <td align=right > 349.454      </td>
      <td align=right > 7.74968e-5      </td>
      <td align=right > 0.0270815      </td>
      <td align=right > 0.816347      </td>
    </tr>
    <tr> 
      <td align=right > 7.5      </td>
      <td align=right > 0.10596      </td>
      <td align=right > 0.000562959      </td>
      <td align=right > 425.81      </td>
      <td align=right > 5.96513e-5      </td>
      <td align=right > 0.0254001      </td>
      <td align=right > 0.818601      </td>
    </tr>
    <tr> 
      <td align=right > 7.75      </td>
      <td align=right > 0.0999063      </td>
      <td align=right > 0.000462711      </td>
      <td align=right > 516.317      </td>
      <td align=right > 4.62278e-5      </td>
      <td align=right > 0.0238682      </td>
      <td align=right > 0.820664      </td>
    </tr>
    <tr> 
      <td align=right > 8      </td>
      <td align=right > 0.0943396      </td>
      <td align=right > 0.00038222      </td>
      <td align=right > 623.123      </td>
      <td align=right > 3.60585e-5      </td>
      <td align=right > 0.0224689      </td>
      <td align=right > 0.822558      </td>
    </tr>
    <tr> 
      <td align=right > 8.25      </td>
      <td align=right > 0.089211      </td>
      <td align=right > 0.000317245      </td>
      <td align=right > 748.629      </td>
      <td align=right > 2.83018e-5      </td>
      <td align=right > 0.0211875      </td>
      <td align=right > 0.824298      </td>
    </tr>
    <tr> 
      <td align=right > 8.5      </td>
      <td align=right > 0.0844773      </td>
      <td align=right > 0.000264525      </td>
      <td align=right > 895.508      </td>
      <td align=right > 2.23464e-5      </td>
      <td align=right > 0.0200113      </td>
      <td align=right > 0.825903      </td>
    </tr>
    <tr> 
      <td align=right > 8.75      </td>
      <td align=right > 0.0801001      </td>
      <td align=right > 0.000221536      </td>
      <td align=right > 1066.73      </td>
      <td align=right > 1.7745e-5      </td>
      <td align=right > 0.0189293      </td>
      <td align=right > 0.827383      </td>
    </tr>
    <tr> 
      <td align=right > 9      </td>
      <td align=right > 0.0760456      </td>
      <td align=right > 0.000186315      </td>
      <td align=right > 1265.6      </td>
      <td align=right > 1.41684e-5      </td>
      <td align=right > 0.0179316      </td>
      <td align=right > 0.828753      </td>
    </tr>
    <tr> 
      <td align=right > 9.25      </td>
      <td align=right > 0.0722837      </td>
      <td align=right > 0.000157326      </td>
      <td align=right > 1495.76      </td>
      <td align=right > 1.13721e-5      </td>
      <td align=right > 0.01701      </td>
      <td align=right > 0.830022      </td>
    </tr>
    <tr> 
      <td align=right > 9.5      </td>
      <td align=right > 0.0687876      </td>
      <td align=right > 0.000133363      </td>
      <td align=right > 1761.21      </td>
      <td align=right > 9.1737e-6      </td>
      <td align=right > 0.0161568      </td>
      <td align=right > 0.831199      </td>
    </tr>
    <tr> 
      <td align=right > 9.75      </td>
      <td align=right > 0.0655335      </td>
      <td align=right > 0.000113469      </td>
      <td align=right > 2066.38      </td>
      <td align=right > 7.43602e-6      </td>
      <td align=right > 0.0153657      </td>
      <td align=right > 0.832294      </td>
    </tr>
    <tr> 
      <td align=right > 10      </td>
      <td align=right > 0.0625      </td>
      <td align=right > 9.68873e-5      </td>
      <td align=right > 2416.12      </td>
      <td align=right > 6.05545e-6      </td>
      <td align=right > 0.0146307      </td>
      <td align=right > 0.833313      </td>
    </tr>
    <tr> 
      <td align=right > 20      </td>
      <td align=right > 0.0163934      </td>
      <td align=right > 1.11918e-6      </td>
      <td align=right > 204202      </td>
      <td align=right > 1.83472e-8      </td>
      <td align=right > 0.00374654      </td>
      <td align=right > 0.848672      </td>
    </tr>
    <tr> 
      <td align=right > 30      </td>
      <td align=right > 0.00735294      </td>
      <td align=right > 7.73043e-8      </td>
      <td align=right > 2.94286e+6      </td>
      <td align=right > 5.68414e-10      </td>
      <td align=right > 0.00167276      </td>
      <td align=right > 0.851654      </td>
    </tr>
    <tr> 
      <td align=right > 40      </td>
      <td align=right > 0.00414938      </td>
      <td align=right > 1.148e-8      </td>
      <td align=right > 1.97847e+7      </td>
      <td align=right > 4.7635e-11      </td>
      <td align=right > 0.000942446      </td>
      <td align=right > 0.852709      </td>
    </tr>
    <tr> 
      <td align=right > 50      </td>
      <td align=right > 0.00265957      </td>
      <td align=right > 2.6064e-9      </td>
      <td align=right > 8.7078e+7      </td>
      <td align=right > 6.93191e-12      </td>
      <td align=right > 0.000603616      </td>
      <td align=right > 0.853199      </td>
    </tr>
  </tbody>
</table>
<BR><HR>
<ADDRESS>
genick
2007-11-14
</ADDRESS>
        </tr>
    </tbody>
    </table>
    <? include("aboutPottoProject.php");  ?>
    </div>
    </td>
    </tr>
    </tbody>
</table>
<? include("bottom.php"); ?>

