<?php ?>
	<br>
	<hr align="center" noshade="noshade" size="4" width="50%">
	<h3>About Potto Project<br> </h3>
	<p  align="justify" style="text-align:justify; 
		margin-bottom:0.15in;margin-top:0.1in; margin-left: 0.1in;
		;margin-right:0.5in; " >
	Potto Project has been created by Dr. Genick Bar-Meir and
	friends<a href="/opsystems.phtml"></a> to build free software and
	textbooks for college students. </p>
	<p  align="justify" style="text-align:justify; 
		margin-bottom:0.15in;margin-top:0.1in; margin-left: 0.1in;
		;margin-right:0.5in; " >
	Potto Project is under <a href="copyRight.php"><span 
	style="text-decoration: underline;">open content</span></a>
	licenses, which means that you will always have the freedom
	to use it, make copies of it, and improve it.
	You are encouraged to make use of these freedoms and
	share the textbooks and program with your family and friends!
	</p>
<?php ?>

