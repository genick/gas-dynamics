<? include("header.php");?>
<table style="width:100%; height:993px;" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
	<? include("left_link.php");?>
	<td style="width: 80%; vertical-align: top;">
	<div  font-family: "arial", "helvetica", "lucida", "sans";>

<!--Navigation Panel-->
<A NAME="tex2html249"
  HREF="node15.php">
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next"
 SRC="figures/next.png"></A> 
<A NAME="tex2html245"
  HREF="node13.php">
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up"
 SRC="figures/up.png"></A> 
<A NAME="tex2html239"
  HREF="node13.php">
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous"
 SRC="figures/prev.png"></A> 
<A NAME="tex2html247"
  HREF="node1.php">
<IMG WIDTH="65" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="contents"
 SRC="figures/contents.png"></A>  
<BR>
<B> Next:</B> <A NAME="tex2html250"
  HREF="node15.php">Isothermal Nozzle for k=1.3</A>
<B> Up:</B> <A NAME="tex2html246"
  HREF="node13.php">Isothermal Nozzle</A>
<B> Previous:</B> <A NAME="tex2html240"
  HREF="node13.php">Isothermal Nozzle</A>
 &nbsp; <B>  <A NAME="tex2html248"
  HREF="node1.php">Contents</A></B> 
<BR>
<BR>
<!--End of Navigation Panel-->

<H2><A NAME="SECTION00031000000000000000">
Isothermal Nozzle for k=1.2</A>
</H2>

<P>

<table border=1 width="100%" >
  <thead>
    <tr>
      <th align=left bgcolor="#fffeaa" colspan=2 >Isothermal Nozzle Flow </th>
      <th align=left  bgcolor="#00ff5a" colspan=3 >Input: M </th>
      <th align=left  bgcolor="#9ae0ee" colspan=1 >k = 1.2 </th>
    </tr>
    <tr>
      <th align=center >M   </th>
      <th align=center >T0/T0* </th>
      <th align=center >P0/P0* </th>
      <th align=center >A/A* </th>
      <th align=center >P/P* </th>
      <th align=center >PAR </th>
      <th align=center >F/F* </th>
    </tr> 
  </thead>
  <tbody>
    <tr> 
      <td align=right > 1e-6      </td>
      <td align=right > 0.909091      </td>
      <td align=right > 1.02854      </td>
      <td align=right > 548812      </td>
      <td align=right > 1.82212      </td>
      <td align=right > 1e+6      </td>
      <td align=right > 454545      </td>
    </tr>
    <tr> 
      <td align=right > 0.0500005      </td>
      <td align=right > 0.909318      </td>
      <td align=right > 1.02854      </td>
      <td align=right > 10.9926      </td>
      <td align=right > 1.81939      </td>
      <td align=right > 19.9998      </td>
      <td align=right > 9.11809      </td>
    </tr>
    <tr> 
      <td align=right > 0.1      </td>
      <td align=right > 0.91      </td>
      <td align=right > 1.02854      </td>
      <td align=right > 5.52114      </td>
      <td align=right > 1.81122      </td>
      <td align=right > 10      </td>
      <td align=right > 4.6      </td>
    </tr>
    <tr> 
      <td align=right > 0.2      </td>
      <td align=right > 0.912727      </td>
      <td align=right > 1.02849      </td>
      <td align=right > 2.81071      </td>
      <td align=right > 1.77891      </td>
      <td align=right > 5      </td>
      <td align=right > 2.38182      </td>
    </tr>
    <tr> 
      <td align=right > 0.25      </td>
      <td align=right > 0.914773      </td>
      <td align=right > 1.02842      </td>
      <td align=right > 2.27913      </td>
      <td align=right > 1.75505      </td>
      <td align=right > 4      </td>
      <td align=right > 1.95455      </td>
    </tr>
    <tr> 
      <td align=right > 0.3      </td>
      <td align=right > 0.917273      </td>
      <td align=right > 1.02829      </td>
      <td align=right > 1.93087      </td>
      <td align=right > 1.72633      </td>
      <td align=right > 3.33333      </td>
      <td align=right > 1.67879      </td>
    </tr>
    <tr> 
      <td align=right > 0.35      </td>
      <td align=right > 0.920227      </td>
      <td align=right > 1.02808      </td>
      <td align=right > 1.68762      </td>
      <td align=right > 1.693      </td>
      <td align=right > 2.85714      </td>
      <td align=right > 1.48961      </td>
    </tr>
    <tr> 
      <td align=right > 0.4      </td>
      <td align=right > 0.923636      </td>
      <td align=right > 1.02776      </td>
      <td align=right > 1.51027      </td>
      <td align=right > 1.65533      </td>
      <td align=right > 2.5      </td>
      <td align=right > 1.35455      </td>
    </tr>
    <tr> 
      <td align=right > 0.45      </td>
      <td align=right > 0.9275      </td>
      <td align=right > 1.02729      </td>
      <td align=right > 1.37714      </td>
      <td align=right > 1.61365      </td>
      <td align=right > 2.22222      </td>
      <td align=right > 1.25556      </td>
    </tr>
    <tr> 
      <td align=right > 0.5      </td>
      <td align=right > 0.931818      </td>
      <td align=right > 1.02664      </td>
      <td align=right > 1.27526      </td>
      <td align=right > 1.56831      </td>
      <td align=right > 2      </td>
      <td align=right > 1.18182      </td>
    </tr>
    <tr> 
      <td align=right > 0.55      </td>
      <td align=right > 0.936591      </td>
      <td align=right > 1.02577      </td>
      <td align=right > 1.19642      </td>
      <td align=right > 1.51968      </td>
      <td align=right > 1.81818      </td>
      <td align=right > 1.12645      </td>
    </tr>
    <tr> 
      <td align=right > 0.6      </td>
      <td align=right > 0.941818      </td>
      <td align=right > 1.02464      </td>
      <td align=right > 1.13522      </td>
      <td align=right > 1.46815      </td>
      <td align=right > 1.66667      </td>
      <td align=right > 1.08485      </td>
    </tr>
    <tr> 
      <td align=right > 0.65      </td>
      <td align=right > 0.9475      </td>
      <td align=right > 1.02319      </td>
      <td align=right > 1.08794      </td>
      <td align=right > 1.41411      </td>
      <td align=right > 1.53846      </td>
      <td align=right > 1.05385      </td>
    </tr>
    <tr> 
      <td align=right > 0.7      </td>
      <td align=right > 0.953636      </td>
      <td align=right > 1.02139      </td>
      <td align=right > 1.05198      </td>
      <td align=right > 1.35798      </td>
      <td align=right > 1.42857      </td>
      <td align=right > 1.03117      </td>
    </tr>
    <tr> 
      <td align=right > 0.75      </td>
      <td align=right > 0.960227      </td>
      <td align=right > 1.01917      </td>
      <td align=right > 1.0255      </td>
      <td align=right > 1.30018      </td>
      <td align=right > 1.33333      </td>
      <td align=right > 1.01515      </td>
    </tr>
    <tr> 
      <td align=right > 0.8      </td>
      <td align=right > 0.967273      </td>
      <td align=right > 1.01649      </td>
      <td align=right > 1.00717      </td>
      <td align=right > 1.2411      </td>
      <td align=right > 1.25      </td>
      <td align=right > 1.00455      </td>
    </tr>
    <tr> 
      <td align=right > 0.85      </td>
      <td align=right > 0.974773      </td>
      <td align=right > 1.01328      </td>
      <td align=right > 0.996027      </td>
      <td align=right > 1.18116      </td>
      <td align=right > 1.17647      </td>
      <td align=right > 0.998396      </td>
    </tr>
    <tr> 
      <td align=right > 0.9      </td>
      <td align=right > 0.982727      </td>
      <td align=right > 1.0095      </td>
      <td align=right > 0.991398      </td>
      <td align=right > 1.12075      </td>
      <td align=right > 1.11111      </td>
      <td align=right > 0.99596      </td>
    </tr>
    <tr> 
      <td align=right > 0.95      </td>
      <td align=right > 0.991136      </td>
      <td align=right > 1.00509      </td>
      <td align=right > 0.992819      </td>
      <td align=right > 1.06024      </td>
      <td align=right > 1.05263      </td>
      <td align=right > 0.996651      </td>
    </tr>
    <tr> 
      <td align=right > 1      </td>
      <td align=right > 1      </td>
      <td align=right > 1      </td>
      <td align=right > 1      </td>
      <td align=right > 1      </td>
      <td align=right > 1      </td>
      <td align=right > 1      </td>
    </tr>
    <tr> 
      <td align=right > 1.05      </td>
      <td align=right > 1.00932      </td>
      <td align=right > 0.994167      </td>
      <td align=right > 1.01279      </td>
      <td align=right > 0.940353      </td>
      <td align=right > 0.952381      </td>
      <td align=right > 1.00563      </td>
    </tr>
    <tr> 
      <td align=right > 1.1      </td>
      <td align=right > 1.01909      </td>
      <td align=right > 0.987544      </td>
      <td align=right > 1.03117      </td>
      <td align=right > 0.881615      </td>
      <td align=right > 0.909091      </td>
      <td align=right > 1.01322      </td>
    </tr>
    <tr> 
      <td align=right > 1.15      </td>
      <td align=right > 1.02932      </td>
      <td align=right > 0.980081      </td>
      <td align=right > 1.05521      </td>
      <td align=right > 0.82407      </td>
      <td align=right > 0.869565      </td>
      <td align=right > 1.02253      </td>
    </tr>
    <tr> 
      <td align=right > 1.2      </td>
      <td align=right > 1.04      </td>
      <td align=right > 0.971732      </td>
      <td align=right > 1.08511      </td>
      <td align=right > 0.767974      </td>
      <td align=right > 0.833333      </td>
      <td align=right > 1.03333      </td>
    </tr>
    <tr> 
      <td align=right > 1.25      </td>
      <td align=right > 1.05114      </td>
      <td align=right > 0.962454      </td>
      <td align=right > 1.12115      </td>
      <td align=right > 0.713552      </td>
      <td align=right > 0.8      </td>
      <td align=right > 1.04545      </td>
    </tr>
    <tr> 
      <td align=right > 1.3      </td>
      <td align=right > 1.06273      </td>
      <td align=right > 0.952211      </td>
      <td align=right > 1.16374      </td>
      <td align=right > 0.661001      </td>
      <td align=right > 0.769231      </td>
      <td align=right > 1.05874      </td>
    </tr>
    <tr> 
      <td align=right > 1.35      </td>
      <td align=right > 1.07477      </td>
      <td align=right > 0.940969      </td>
      <td align=right > 1.21336      </td>
      <td align=right > 0.610486      </td>
      <td align=right > 0.740741      </td>
      <td align=right > 1.07306      </td>
    </tr>
    <tr> 
      <td align=right > 1.4      </td>
      <td align=right > 1.08727      </td>
      <td align=right > 0.928704      </td>
      <td align=right > 1.27065      </td>
      <td align=right > 0.562142      </td>
      <td align=right > 0.714286      </td>
      <td align=right > 1.08831      </td>
    </tr>
    <tr> 
      <td align=right > 1.45      </td>
      <td align=right > 1.10023      </td>
      <td align=right > 0.915395      </td>
      <td align=right > 1.33634      </td>
      <td align=right > 0.516077      </td>
      <td align=right > 0.689655      </td>
      <td align=right > 1.10439      </td>
    </tr>
    <tr> 
      <td align=right > 1.5      </td>
      <td align=right > 1.11364      </td>
      <td align=right > 0.901031      </td>
      <td align=right > 1.41133      </td>
      <td align=right > 0.472367      </td>
      <td align=right > 0.666667      </td>
      <td align=right > 1.12121      </td>
    </tr>
    <tr> 
      <td align=right > 1.55      </td>
      <td align=right > 1.1275      </td>
      <td align=right > 0.885606      </td>
      <td align=right > 1.49667      </td>
      <td align=right > 0.431063      </td>
      <td align=right > 0.645161      </td>
      <td align=right > 1.13871      </td>
    </tr>
    <tr> 
      <td align=right > 1.6      </td>
      <td align=right > 1.14182      </td>
      <td align=right > 0.869125      </td>
      <td align=right > 1.5936      </td>
      <td align=right > 0.392193      </td>
      <td align=right > 0.625      </td>
      <td align=right > 1.15682      </td>
    </tr>
    <tr> 
      <td align=right > 1.65      </td>
      <td align=right > 1.15659      </td>
      <td align=right > 0.851599      </td>
      <td align=right > 1.70357      </td>
      <td align=right > 0.35576      </td>
      <td align=right > 0.606061      </td>
      <td align=right > 1.17548      </td>
    </tr>
    <tr> 
      <td align=right > 1.7      </td>
      <td align=right > 1.17182      </td>
      <td align=right > 0.833051      </td>
      <td align=right > 1.82827      </td>
      <td align=right > 0.321744      </td>
      <td align=right > 0.588235      </td>
      <td align=right > 1.19465      </td>
    </tr>
    <tr> 
      <td align=right > 1.75      </td>
      <td align=right > 1.1875      </td>
      <td align=right > 0.813509      </td>
      <td align=right > 1.96971      </td>
      <td align=right > 0.290109      </td>
      <td align=right > 0.571429      </td>
      <td align=right > 1.21429      </td>
    </tr>
    <tr> 
      <td align=right > 1.8      </td>
      <td align=right > 1.20364      </td>
      <td align=right > 0.793012      </td>
      <td align=right > 2.13019      </td>
      <td align=right > 0.2608      </td>
      <td align=right > 0.555556      </td>
      <td align=right > 1.23434      </td>
    </tr>
    <tr> 
      <td align=right > 1.85      </td>
      <td align=right > 1.22023      </td>
      <td align=right > 0.771609      </td>
      <td align=right > 2.31247      </td>
      <td align=right > 0.233751      </td>
      <td align=right > 0.540541      </td>
      <td align=right > 1.25479      </td>
    </tr>
    <tr> 
      <td align=right > 1.9      </td>
      <td align=right > 1.23727      </td>
      <td align=right > 0.749355      </td>
      <td align=right > 2.51972      </td>
      <td align=right > 0.208879      </td>
      <td align=right > 0.526316      </td>
      <td align=right > 1.2756      </td>
    </tr>
    <tr> 
      <td align=right > 1.95      </td>
      <td align=right > 1.25477      </td>
      <td align=right > 0.726314      </td>
      <td align=right > 2.7557      </td>
      <td align=right > 0.186095      </td>
      <td align=right > 0.512821      </td>
      <td align=right > 1.29674      </td>
    </tr>
    <tr> 
      <td align=right > 2      </td>
      <td align=right > 1.27273      </td>
      <td align=right > 0.702558      </td>
      <td align=right > 3.02482      </td>
      <td align=right > 0.165299      </td>
      <td align=right > 0.5      </td>
      <td align=right > 1.31818      </td>
    </tr>
    <tr> 
      <td align=right > 2.25      </td>
      <td align=right > 1.36932      </td>
      <td align=right > 0.576015      </td>
      <td align=right > 5.0864      </td>
      <td align=right > 0.087379      </td>
      <td align=right > 0.444444      </td>
      <td align=right > 1.42929      </td>
    </tr>
    <tr> 
      <td align=right > 2.5      </td>
      <td align=right > 1.47727      </td>
      <td align=right > 0.445386      </td>
      <td align=right > 9.33443      </td>
      <td align=right > 0.0428521      </td>
      <td align=right > 0.4      </td>
      <td align=right > 1.54545      </td>
    </tr>
    <tr> 
      <td align=right > 2.75      </td>
      <td align=right > 1.59659      </td>
      <td align=right > 0.322944      </td>
      <td align=right > 18.651      </td>
      <td align=right > 0.0194969      </td>
      <td align=right > 0.363636      </td>
      <td align=right > 1.66529      </td>
    </tr>
    <tr> 
      <td align=right > 3      </td>
      <td align=right > 1.72727      </td>
      <td align=right > 0.218551      </td>
      <td align=right > 40.5035      </td>
      <td align=right > 0.00822975      </td>
      <td align=right > 0.333333      </td>
      <td align=right > 1.78788      </td>
    </tr>
    <tr> 
      <td align=right > 3.25      </td>
      <td align=right > 1.86932      </td>
      <td align=right > 0.13751      </td>
      <td align=right > 95.4731      </td>
      <td align=right > 0.00322282      </td>
      <td align=right > 0.307692      </td>
      <td align=right > 1.91259      </td>
    </tr>
    <tr> 
      <td align=right > 3.5      </td>
      <td align=right > 2.02273      </td>
      <td align=right > 0.080193      </td>
      <td align=right > 244.017      </td>
      <td align=right > 0.00117088      </td>
      <td align=right > 0.285714      </td>
      <td align=right > 2.03896      </td>
    </tr>
    <tr> 
      <td align=right > 3.75      </td>
      <td align=right > 2.1875      </td>
      <td align=right > 0.043242      </td>
      <td align=right > 675.697      </td>
      <td align=right > 0.000394654      </td>
      <td align=right > 0.266667      </td>
      <td align=right > 2.16667      </td>
    </tr>
    <tr> 
      <td align=right > 4      </td>
      <td align=right > 2.36364      </td>
      <td align=right > 0.0215196      </td>
      <td align=right > 2025.77      </td>
      <td align=right > 0.00012341      </td>
      <td align=right > 0.25      </td>
      <td align=right > 2.29545      </td>
    </tr>
    <tr> 
      <td align=right > 4.25      </td>
      <td align=right > 2.55114      </td>
      <td align=right > 0.00986989      </td>
      <td align=right > 6572.05      </td>
      <td align=right > 3.58022e-5      </td>
      <td align=right > 0.235294      </td>
      <td align=right > 2.42513      </td>
    </tr>
    <tr> 
      <td align=right > 4.5      </td>
      <td align=right > 2.75      </td>
      <td align=right > 0.00416769      </td>
      <td align=right > 23061.6      </td>
      <td align=right > 9.63604e-6      </td>
      <td align=right > 0.222222      </td>
      <td align=right > 2.55556      </td>
    </tr>
    <tr> 
      <td align=right > 4.75      </td>
      <td align=right > 2.96023      </td>
      <td align=right > 0.00161907      </td>
      <td align=right > 87496.6      </td>
      <td align=right > 2.40611e-6      </td>
      <td align=right > 0.210526      </td>
      <td align=right > 2.6866      </td>
    </tr>
    <tr> 
      <td align=right > 5      </td>
      <td align=right > 3.18182      </td>
      <td align=right > 0.000578378      </td>
      <td align=right > 358815      </td>
      <td align=right > 5.5739e-7      </td>
      <td align=right > 0      </td>
      <td align=right > 2.81818      </td>
    </tr>
    <tr> 
      <td align=right > 5.25      </td>
      <td align=right > 3.41477      </td>
      <td align=right > 0.000189934      </td>
      <td align=right > 1.59004e+6      </td>
      <td align=right > 1.19793e-7      </td>
      <td align=right > 0      </td>
      <td align=right > 2.95022      </td>
    </tr>
    <tr> 
      <td align=right > 5.5      </td>
      <td align=right > 3.65909      </td>
      <td align=right > 5.73286e-5      </td>
      <td align=right > 7.61212e+6      </td>
      <td align=right > 2.38854e-8      </td>
      <td align=right > 0      </td>
      <td align=right > 3.08264      </td>
    </tr>
    <tr> 
      <td align=right > 5.75      </td>
      <td align=right > 3.91477      </td>
      <td align=right > 1.59037e-5      </td>
      <td align=right > 3.93616e+7      </td>
      <td align=right > 4.41835e-9      </td>
      <td align=right > 0      </td>
      <td align=right > 3.21542      </td>
    </tr>
    <tr> 
      <td align=right > 6      </td>
      <td align=right > 4.18182      </td>
      <td align=right > 4.05515e-6      </td>
      <td align=right > 2.19803e+8      </td>
      <td align=right > 7.58256e-10      </td>
      <td align=right > 0      </td>
      <td align=right > 3.34848      </td>
    </tr>
    <tr> 
      <td align=right > 6.25      </td>
      <td align=right > 4.46023      </td>
      <td align=right > 9.50477e-7      </td>
      <td align=right > 1.32532e+9      </td>
      <td align=right > 1.20726e-10      </td>
      <td align=right > 0      </td>
      <td align=right > 3.48182      </td>
    </tr>
    <tr> 
      <td align=right > 6.5      </td>
      <td align=right > 4.75      </td>
      <td align=right > 2.0482e-7      </td>
      <td align=right > 8.6273e+9      </td>
      <td align=right > 1.78325e-11      </td>
      <td align=right > 0      </td>
      <td align=right > 3.61538      </td>
    </tr>
    <tr> 
      <td align=right > 6.75      </td>
      <td align=right > 5.05114      </td>
      <td align=right > 4.05869e-8      </td>
      <td align=right > 6.0624e+10      </td>
      <td align=right > 2.44372e-12      </td>
      <td align=right > 0      </td>
      <td align=right > 3.74916      </td>
    </tr>
    <tr> 
      <td align=right > 7      </td>
      <td align=right > 5.36364      </td>
      <td align=right > 7.39733e-9      </td>
      <td align=right > 4.59815e+11      </td>
      <td align=right > 3.10684e-13      </td>
      <td align=right > 0      </td>
      <td align=right > 3.88312      </td>
    </tr>
    <tr> 
      <td align=right > 7.25      </td>
      <td align=right > 5.6875      </td>
      <td align=right > 1.24035e-9      </td>
      <td align=right > 3.76398e+12      </td>
      <td align=right > 3.6645e-14      </td>
      <td align=right > 0      </td>
      <td align=right > 4.01724      </td>
    </tr>
    <tr> 
      <td align=right > 7.5      </td>
      <td align=right > 6.02273      </td>
      <td align=right > 1.9138e-10      </td>
      <td align=right > 3.32507e+13      </td>
      <td align=right > 4.00994e-15      </td>
      <td align=right > 0      </td>
      <td align=right > 4.15152      </td>
    </tr>
    <tr> 
      <td align=right > 7.75      </td>
      <td align=right > 6.36932      </td>
      <td align=right > 2.71798e-11      </td>
      <td align=right > 3.16963e+14      </td>
      <td align=right > 4.07089e-16      </td>
      <td align=right > 0      </td>
      <td align=right > 4.28592      </td>
    </tr>
    <tr> 
      <td align=right > 8      </td>
      <td align=right > 6.72727      </td>
      <td align=right > 3.55388e-12      </td>
      <td align=right > 3.26018e+15      </td>
      <td align=right > 3.83415e-17      </td>
      <td align=right > 0      </td>
      <td align=right > 4.42045      </td>
    </tr>
    <tr> 
      <td align=right > 8.25      </td>
      <td align=right > 7.09659      </td>
      <td align=right > 4.27932e-13      </td>
      <td align=right > 3.61801e+16      </td>
      <td align=right > 3.35024e-18      </td>
      <td align=right > 0      </td>
      <td align=right > 4.5551      </td>
    </tr>
    <tr> 
      <td align=right > 8.5      </td>
      <td align=right > 7.47727      </td>
      <td align=right > 4.74646e-14      </td>
      <td align=right > 4.33182e+17      </td>
      <td align=right > 2.71588e-19      </td>
      <td align=right > 0      </td>
      <td align=right > 4.68984      </td>
    </tr>
    <tr> 
      <td align=right > 8.75      </td>
      <td align=right > 7.86932      </td>
      <td align=right > 4.85061e-15      </td>
      <td align=right > 5.59523e+18      </td>
      <td align=right > 2.04256e-20      </td>
      <td align=right > 0      </td>
      <td align=right > 4.82468      </td>
    </tr>
    <tr> 
      <td align=right > 9      </td>
      <td align=right > 8.27273      </td>
      <td align=right > 4.56833e-16      </td>
      <td align=right > 7.79637e+19      </td>
      <td align=right > 1.42516e-21      </td>
      <td align=right > 0      </td>
      <td align=right > 4.9596      </td>
    </tr>
    <tr> 
      <td align=right > 9.25      </td>
      <td align=right > 8.6875      </td>
      <td align=right > 3.966e-17      </td>
      <td align=right > 1.17186e+21      </td>
      <td align=right > 9.22537e-23      </td>
      <td align=right > 0      </td>
      <td align=right > 5.09459      </td>
    </tr>
    <tr> 
      <td align=right > 9.5      </td>
      <td align=right > 9.11364      </td>
      <td align=right > 3.17454e-18      </td>
      <td align=right > 1.89997e+22      </td>
      <td align=right > 5.54026e-24      </td>
      <td align=right > 0      </td>
      <td align=right > 5.22967      </td>
    </tr>
    <tr> 
      <td align=right > 9.75      </td>
      <td align=right > 9.55114      </td>
      <td align=right > 2.34334e-19      </td>
      <td align=right > 3.32269e+23      </td>
      <td align=right > 3.08678e-25      </td>
      <td align=right > 0      </td>
      <td align=right > 5.3648      </td>
    </tr>
    <tr> 
      <td align=right > 10      </td>
      <td align=right > 10      </td>
      <td align=right > 1.59554e-20      </td>
      <td align=right > 6.26747e+24      </td>
      <td align=right > 1.59554e-26      </td>
      <td align=right > 0      </td>
      <td align=right > 5.5      </td>
    </tr>
    <tr> 
      <td align=right > 20      </td>
      <td align=right > 37.2727      </td>
      <td align=right > 2.87242e-95      </td>
      <td align=right > 4.66733e+102      </td>
      <td align=right > 1.07128e-104      </td>
      <td align=right > 0      </td>
      <td align=right > 10.9318      </td>
    </tr>
    <tr> 
      <td align=right > 30      </td>
      <td align=right > 82.7273      </td>
      <td align=right > 1.76786e-223      </td>
      <td align=right > 6.04397e+232      </td>
      <td align=right > 5.51514e-235      </td>
      <td align=right > 0      </td>
      <td align=right > 16.3788      </td>
    </tr>
    <tr> 
      <td align=right > 40      </td>
      <td align=right > 146.364      </td>
      <td align=right > 0      </td>
      <td align=right > inf      </td>
      <td align=right > 0      </td>
      <td align=right > 0      </td>
      <td align=right > 21.8295      </td>
    </tr>
    <tr> 
      <td align=right > 50      </td>
      <td align=right > 228.182      </td>
      <td align=right > 0      </td>
      <td align=right > inf      </td>
      <td align=right > 0      </td>
      <td align=right > 0      </td>
      <td align=right > 27.2818      </td>
    </tr>
  </tbody>
</table>

<A NAME="296"></A>
<TABLE CELLPADDING=3 BORDER="1">
<CAPTION><STRONG>Table 10:</STRONG>
 Isothermal  Table  </CAPTION>
<TR><TD ALIGN="CENTER" COLSPAN=1><!-- MATH
 $\rule[-.1in]{0.pt}{0.3 in}\mathbf{M}$
 -->
<IMG
 WIDTH="25" HEIGHT="56" ALIGN="MIDDLE" BORDER="0"
 SRC="img3.png"
 ALT="$ \rule[-.1in]{0.pt}{0.3 in}\mathbf{M} $">
</TD>
<TD ALIGN="CENTER" COLSPAN=1><!-- MATH
 $\mathbf{T_0 \over {T_0}^{\star}}$
 -->
<IMG
 WIDTH="34" HEIGHT="43" ALIGN="MIDDLE" BORDER="0"
 SRC="img4.png"
 ALT="$ \mathbf{T_0 \over {T_0}^{\star}} $">
</TD>
<TD ALIGN="CENTER" COLSPAN=1><!-- MATH
 $\mathbf{P_0 \over {P_0}^{\star}}$
 -->
<IMG
 WIDTH="33" HEIGHT="43" ALIGN="MIDDLE" BORDER="0"
 SRC="img5.png"
 ALT="$ \mathbf{P_0 \over {P_0}^{\star}} $">
</TD>
<TD ALIGN="CENTER" COLSPAN=1><!-- MATH
 $\mathbf{A \over A^{\star}}$
 -->
<IMG
 WIDTH="27" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="img6.png"
 ALT="$ \mathbf{A \over A^{\star}} $">
</TD>
<TD ALIGN="CENTER" COLSPAN=1><!-- MATH
 $\mathbf{P \over P^{\star}}$
 -->
<IMG
 WIDTH="26" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="img7.png"
 ALT="$ \mathbf{P \over P^{\star}} $">
</TD>
<TD ALIGN="CENTER" COLSPAN=1><!-- MATH
 $\mathbf{A\times P \over A^{*} \times P_0}$
 -->
<IMG
 WIDTH="56" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="img8.png"
 ALT="$ \mathbf{A\times P \over A^{*} \times P_0} $">
</TD>
<TD ALIGN="CENTER" COLSPAN=1><!-- MATH
 $\mathbf{F \over F^{*}}$
 -->
<IMG
 WIDTH="25" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="img9.png"
 ALT="$ \mathbf{F \over F^{*}} $">
</TD>
</TR>
<TR><TD ALIGN="LEFT">0.00</TD>
<TD ALIGN="LEFT">0.52828</TD>
<TD ALIGN="LEFT">1.064</TD>
<TD ALIGN="LEFT">5.0E+5</TD>
<TD ALIGN="LEFT">2.014</TD>
<TD ALIGN="LEFT">1.0E+6</TD>
<TD ALIGN="LEFT">4.2E+5</TD>
</TR>
<TR><TD ALIGN="LEFT">0.05</TD>
<TD ALIGN="LEFT">0.52921</TD>
<TD ALIGN="LEFT">1.064</TD>
<TD ALIGN="LEFT">9.949</TD>
<TD ALIGN="LEFT">2.010</TD>
<TD ALIGN="LEFT">20.00</TD>
<TD ALIGN="LEFT">8.362</TD>
</TR>
<TR><TD ALIGN="LEFT">0.1</TD>
<TD ALIGN="LEFT">0.53199</TD>
<TD ALIGN="LEFT">1.064</TD>
<TD ALIGN="LEFT">5.001</TD>
<TD ALIGN="LEFT">2.000</TD>
<TD ALIGN="LEFT">10.00</TD>
<TD ALIGN="LEFT">4.225</TD>
</TR>
<TR><TD ALIGN="LEFT">0.2</TD>
<TD ALIGN="LEFT">0.54322</TD>
<TD ALIGN="LEFT">1.064</TD>
<TD ALIGN="LEFT">2.553</TD>
<TD ALIGN="LEFT">1.958</TD>
<TD ALIGN="LEFT">5.000</TD>
<TD ALIGN="LEFT">2.200</TD>
</TR>
<TR><TD ALIGN="LEFT">0.3</TD>
<TD ALIGN="LEFT">0.56232</TD>
<TD ALIGN="LEFT">1.063</TD>
<TD ALIGN="LEFT">1.763</TD>
<TD ALIGN="LEFT">1.891</TD>
<TD ALIGN="LEFT">3.333</TD>
<TD ALIGN="LEFT">1.564</TD>
</TR>
<TR><TD ALIGN="LEFT">0.4</TD>
<TD ALIGN="LEFT">0.58985</TD>
<TD ALIGN="LEFT">1.062</TD>
<TD ALIGN="LEFT">1.389</TD>
<TD ALIGN="LEFT">1.800</TD>
<TD ALIGN="LEFT">2.500</TD>
<TD ALIGN="LEFT">1.275</TD>
</TR>
<TR><TD ALIGN="LEFT">0.5</TD>
<TD ALIGN="LEFT">0.62665</TD>
<TD ALIGN="LEFT">1.059</TD>
<TD ALIGN="LEFT">1.183</TD>
<TD ALIGN="LEFT">1.690</TD>
<TD ALIGN="LEFT">2.000</TD>
<TD ALIGN="LEFT">1.125</TD>
</TR>
<TR><TD ALIGN="LEFT">0.6</TD>
<TD ALIGN="LEFT">0.67383</TD>
<TD ALIGN="LEFT">1.055</TD>
<TD ALIGN="LEFT">1.065</TD>
<TD ALIGN="LEFT">1.565</TD>
<TD ALIGN="LEFT">1.667</TD>
<TD ALIGN="LEFT">1.044</TD>
</TR>
<TR><TD ALIGN="LEFT">0.7</TD>
<TD ALIGN="LEFT">0.73278</TD>
<TD ALIGN="LEFT">1.047</TD>
<TD ALIGN="LEFT">0.99967</TD>
<TD ALIGN="LEFT">1.429</TD>
<TD ALIGN="LEFT">1.429</TD>
<TD ALIGN="LEFT">1.004</TD>
</TR>
<TR><TD ALIGN="LEFT">0.8</TD>
<TD ALIGN="LEFT">0.80528</TD>
<TD ALIGN="LEFT">1.036</TD>
<TD ALIGN="LEFT">0.97156</TD>
<TD ALIGN="LEFT">1.287</TD>
<TD ALIGN="LEFT">1.250</TD>
<TD ALIGN="LEFT">0.98750</TD>
</TR>
<TR><TD ALIGN="LEFT">0.9</TD>
<TD ALIGN="LEFT">0.89348</TD>
<TD ALIGN="LEFT">1.021</TD>
<TD ALIGN="LEFT">0.97274</TD>
<TD ALIGN="LEFT">1.142</TD>
<TD ALIGN="LEFT">1.111</TD>
<TD ALIGN="LEFT">0.98796</TD>
</TR>
<TR><TD ALIGN="LEFT">1.00</TD>
<TD ALIGN="LEFT">1.000</TD>
<TD ALIGN="LEFT">1.000</TD>
<TD ALIGN="LEFT">1.000</TD>
<TD ALIGN="LEFT">1.000</TD>
<TD ALIGN="LEFT">1.000</TD>
<TD ALIGN="LEFT">1.000</TD>
</TR>
<TR><TD ALIGN="LEFT">1.10</TD>
<TD ALIGN="LEFT">1.128</TD>
<TD ALIGN="LEFT">0.97376</TD>
<TD ALIGN="LEFT">1.053</TD>
<TD ALIGN="LEFT">0.86329</TD>
<TD ALIGN="LEFT">0.90909</TD>
<TD ALIGN="LEFT">1.020</TD>
</TR>
<TR><TD ALIGN="LEFT">1.20</TD>
<TD ALIGN="LEFT">1.281</TD>
<TD ALIGN="LEFT">0.94147</TD>
<TD ALIGN="LEFT">1.134</TD>
<TD ALIGN="LEFT">0.73492</TD>
<TD ALIGN="LEFT">0.83333</TD>
<TD ALIGN="LEFT">1.047</TD>
</TR>
<TR><TD ALIGN="LEFT">1.30</TD>
<TD ALIGN="LEFT">1.464</TD>
<TD ALIGN="LEFT">0.90302</TD>
<TD ALIGN="LEFT">1.247</TD>
<TD ALIGN="LEFT">0.61693</TD>
<TD ALIGN="LEFT">0.76923</TD>
<TD ALIGN="LEFT">1.079</TD>
</TR>
<TR><TD ALIGN="LEFT">1.40</TD>
<TD ALIGN="LEFT">1.681</TD>
<TD ALIGN="LEFT">0.85853</TD>
<TD ALIGN="LEFT">1.399</TD>
<TD ALIGN="LEFT">0.51069</TD>
<TD ALIGN="LEFT">0.71429</TD>
<TD ALIGN="LEFT">1.114</TD>
</TR>
<TR><TD ALIGN="LEFT">1.50</TD>
<TD ALIGN="LEFT">1.939</TD>
<TD ALIGN="LEFT">0.80844</TD>
<TD ALIGN="LEFT">1.599</TD>
<TD ALIGN="LEFT">0.41686</TD>
<TD ALIGN="LEFT">0.66667</TD>
<TD ALIGN="LEFT">1.153</TD>
</TR>
<TR><TD ALIGN="LEFT">1.60</TD>
<TD ALIGN="LEFT">2.245</TD>
<TD ALIGN="LEFT">0.75344</TD>
<TD ALIGN="LEFT">1.863</TD>
<TD ALIGN="LEFT">0.33554</TD>
<TD ALIGN="LEFT">0.62500</TD>
<TD ALIGN="LEFT">1.194</TD>
</TR>
<TR><TD ALIGN="LEFT">1.70</TD>
<TD ALIGN="LEFT">2.608</TD>
<TD ALIGN="LEFT">0.69449</TD>
<TD ALIGN="LEFT">2.209</TD>
<TD ALIGN="LEFT">0.26634</TD>
<TD ALIGN="LEFT">0.58824</TD>
<TD ALIGN="LEFT">1.237</TD>
</TR>
<TR><TD ALIGN="LEFT">1.80</TD>
<TD ALIGN="LEFT">3.035</TD>
<TD ALIGN="LEFT">0.63276</TD>
<TD ALIGN="LEFT">2.665</TD>
<TD ALIGN="LEFT">0.20846</TD>
<TD ALIGN="LEFT">0.55556</TD>
<TD ALIGN="LEFT">1.281</TD>
</TR>
<TR><TD ALIGN="LEFT">1.90</TD>
<TD ALIGN="LEFT">3.540</TD>
<TD ALIGN="LEFT">0.56954</TD>
<TD ALIGN="LEFT">3.271</TD>
<TD ALIGN="LEFT">0.16090</TD>
<TD ALIGN="LEFT">0.52632</TD>
<TD ALIGN="LEFT">1.328</TD>
</TR>
<TR><TD ALIGN="LEFT">2.00</TD>
<TD ALIGN="LEFT">4.134</TD>
<TD ALIGN="LEFT">0.50618</TD>
<TD ALIGN="LEFT">4.083</TD>
<TD ALIGN="LEFT">0.12246</TD>
<TD ALIGN="LEFT">0.50000</TD>
<TD ALIGN="LEFT">1.375</TD>
</TR>
<TR><TD ALIGN="LEFT">2.50</TD>
<TD ALIGN="LEFT">9.026</TD>
<TD ALIGN="LEFT">0.22881</TD>
<TD ALIGN="LEFT">15.78</TD>
<TD ALIGN="LEFT">0.025349</TD>
<TD ALIGN="LEFT">0.40000</TD>
<TD ALIGN="LEFT">1.625</TD>
</TR>
<TR><TD ALIGN="LEFT">3.000</TD>
<TD ALIGN="LEFT">19.41</TD>
<TD ALIGN="LEFT">0.071758</TD>
<TD ALIGN="LEFT">90.14</TD>
<TD ALIGN="LEFT">0.00370</TD>
<TD ALIGN="LEFT">0.33333</TD>
<TD ALIGN="LEFT">1.889</TD>
</TR>
<TR><TD ALIGN="LEFT">3.500</TD>
<TD ALIGN="LEFT">40.29</TD>
<TD ALIGN="LEFT">0.015317</TD>
<TD ALIGN="LEFT">7.5E+2</TD>
<TD ALIGN="LEFT">0.000380</TD>
<TD ALIGN="LEFT">0.28571</TD>
<TD ALIGN="LEFT">2.161</TD>
</TR>
<TR><TD ALIGN="LEFT">4.000</TD>
<TD ALIGN="LEFT">80.21</TD>
<TD ALIGN="LEFT">0.00221</TD>
<TD ALIGN="LEFT">9.1E+3</TD>
<TD ALIGN="LEFT">2.75E-5</TD>
<TD ALIGN="LEFT">0.25000</TD>
<TD ALIGN="LEFT">2.438</TD>
</TR>
<TR><TD ALIGN="LEFT">4.500</TD>
<TD ALIGN="LEFT">1.5E+2</TD>
<TD ALIGN="LEFT">0.000215</TD>
<TD ALIGN="LEFT">1.6E+5</TD>
<TD ALIGN="LEFT">1.41E-6</TD>
<TD ALIGN="LEFT">0.22222</TD>
<TD ALIGN="LEFT">2.718</TD>
</TR>
<TR><TD ALIGN="LEFT">5.000</TD>
<TD ALIGN="LEFT">2.8E+2</TD>
<TD ALIGN="LEFT">1.41E-5</TD>
<TD ALIGN="LEFT">4.0E+6</TD>
<TD ALIGN="LEFT">0.0</TD>
<TD ALIGN="LEFT">0.20000</TD>
<TD ALIGN="LEFT">3.000</TD>
</TR>
<TR><TD ALIGN="LEFT">5.500</TD>
<TD ALIGN="LEFT">4.9E+2</TD>
<TD ALIGN="LEFT">0.0</TD>
<TD ALIGN="LEFT">1.4E+8</TD>
<TD ALIGN="LEFT">0.0</TD>
<TD ALIGN="LEFT">0.18182</TD>
<TD ALIGN="LEFT">3.284</TD>
</TR>
<TR><TD ALIGN="LEFT">6.000</TD>
<TD ALIGN="LEFT">8.3E+2</TD>
<TD ALIGN="LEFT">0.0</TD>
<TD ALIGN="LEFT">7.3E+9</TD>
<TD ALIGN="LEFT">0.0</TD>
<TD ALIGN="LEFT">0.16667</TD>
<TD ALIGN="LEFT">3.569</TD>
</TR>
<TR><TD ALIGN="LEFT">6.500</TD>
<TD ALIGN="LEFT">1.4E+3</TD>
<TD ALIGN="LEFT">0.0</TD>
<TD ALIGN="LEFT">5.3E+11</TD>
<TD ALIGN="LEFT">0.0</TD>
<TD ALIGN="LEFT">0.15385</TD>
<TD ALIGN="LEFT">3.856</TD>
</TR>
<TR><TD ALIGN="LEFT">7.000</TD>
<TD ALIGN="LEFT">2.2E+3</TD>
<TD ALIGN="LEFT">0.0</TD>
<TD ALIGN="LEFT">5.6E+13</TD>
<TD ALIGN="LEFT">0.0</TD>
<TD ALIGN="LEFT">0.14286</TD>
<TD ALIGN="LEFT">4.143</TD>
</TR>
<TR><TD ALIGN="LEFT">7.500</TD>
<TD ALIGN="LEFT">3.4E+3</TD>
<TD ALIGN="LEFT">0.0</TD>
<TD ALIGN="LEFT">8.3E+15</TD>
<TD ALIGN="LEFT">0.0</TD>
<TD ALIGN="LEFT">0.13333</TD>
<TD ALIGN="LEFT">4.431</TD>
</TR>
<TR><TD ALIGN="LEFT">8.000</TD>
<TD ALIGN="LEFT">5.2E+3</TD>
<TD ALIGN="LEFT">0.0</TD>
<TD ALIGN="LEFT">1.8E+18</TD>
<TD ALIGN="LEFT">0.0</TD>
<TD ALIGN="LEFT">0.12500</TD>
<TD ALIGN="LEFT">4.719</TD>
</TR>
<TR><TD ALIGN="LEFT">8.500</TD>
<TD ALIGN="LEFT">7.7E+3</TD>
<TD ALIGN="LEFT">0.0</TD>
<TD ALIGN="LEFT">5.4E+20</TD>
<TD ALIGN="LEFT">0.0</TD>
<TD ALIGN="LEFT">0.11765</TD>
<TD ALIGN="LEFT">5.007</TD>
</TR>
<TR><TD ALIGN="LEFT">9.000</TD>
<TD ALIGN="LEFT">1.1E+4</TD>
<TD ALIGN="LEFT">0.0</TD>
<TD ALIGN="LEFT">2.3E+23</TD>
<TD ALIGN="LEFT">0.0</TD>
<TD ALIGN="LEFT">0.11111</TD>
<TD ALIGN="LEFT">5.296</TD>
</TR>
<TR><TD ALIGN="LEFT">9.500</TD>
<TD ALIGN="LEFT">1.6E+4</TD>
<TD ALIGN="LEFT">0.0</TD>
<TD ALIGN="LEFT">1.4E+26</TD>
<TD ALIGN="LEFT">0.0</TD>
<TD ALIGN="LEFT">0.10526</TD>
<TD ALIGN="LEFT">5.586</TD>
</TR>
<TR><TD ALIGN="LEFT">10.00</TD>
<TD ALIGN="LEFT">2.2E+4</TD>
<TD ALIGN="LEFT">0.0</TD>
<TD ALIGN="LEFT">1.2E+29</TD>
<TD ALIGN="LEFT">0.0</TD>
<TD ALIGN="LEFT">0.100000</TD>
<TD ALIGN="LEFT">5.875</TD>
</TR>
</TABLE>

<P>
<HR>
<!--Navigation Panel-->
<A NAME="tex2html249"
  HREF="node15.php">
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next"
 SRC="figures/next.png"></A> 
<A NAME="tex2html245"
  HREF="node13.php">
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up"
 SRC="figures/up.png"></A> 
<A NAME="tex2html239"
  HREF="node13.php">
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous"
 SRC="figures/prev.png"></A> 
<A NAME="tex2html247"
  HREF="node1.php">
<IMG WIDTH="65" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="contents"
 SRC="figures/contents.png"></A>  
<BR>
<B> Next:</B> <A NAME="tex2html250"
  HREF="node15.php">Isothermal Nozzle for k=1.3</A>
<B> Up:</B> <A NAME="tex2html246"
  HREF="node13.php">Isothermal Nozzle</A>
<B> Previous:</B> <A NAME="tex2html240"
  HREF="node13.php">Isothermal Nozzle</A>
 &nbsp; <B>  <A NAME="tex2html248"
  HREF="node1.php">Contents</A></B> 
<!--End of Navigation Panel-->
<ADDRESS>
genick
2007-11-14
</ADDRESS>
        </tr>
    </tbody>
    </table>
    <? include("aboutPottoProject.php");  ?>
    </div>
    </td>
    </tr>
    </tbody>
</table>
<? include("bottom.php"); ?>

