# LaTeX2HTML 2002-2-1 (1.71)
# Associate labels original text with physical files.


$key = q/chap:fanno/;
$external_labels{$key} = "$URL/" . q|node41.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:Prandtl-Meyer/;
$external_labels{$key} = "$URL/" . q|node51.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:variableArea/;
$external_labels{$key} = "$URL/" . q|node2.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:shock/;
$external_labels{$key} = "$URL/" . q|node18.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:Isothermal/;
$external_labels{$key} = "$URL/" . q|node35.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:Isothermal/;
$external_labels{$key} = "$URL/" . q|node35.html|; 
$noresave{$key} = "$nosave";

1;


# LaTeX2HTML 2002-2-1 (1.71)
# labels from external_latex_labels array.


$key = q/chap:fanno/;
$external_latex_labels{$key} = q|5|; 
$noresave{$key} = "$nosave";

$key = q/oblique:tab:k167/;
$external_latex_labels{$key} = q|8.4|; 
$noresave{$key} = "$nosave";

$key = q/shock:tab:k167/;
$external_latex_labels{$key} = q|3.4|; 
$noresave{$key} = "$nosave";

$key = q/shock:tab:open1.2/;
$external_latex_labels{$key} = q|3.9|; 
$noresave{$key} = "$nosave";

$key = q/pm:tab:1.3/;
$external_latex_labels{$key} = q|7.2|; 
$noresave{$key} = "$nosave";

$key = q/pm:tab:1.2/;
$external_latex_labels{$key} = q|7.1|; 
$noresave{$key} = "$nosave";

$key = q/shock:tab:open1.67/;
$external_latex_labels{$key} = q|3.12|; 
$noresave{$key} = "$nosave";

$key = q/ise:tab:k167/;
$external_latex_labels{$key} = q|1.4|; 
$noresave{$key} = "$nosave";

$key = q/ise:tab:nptk14/;
$external_latex_labels{$key} = q|1.7|; 
$noresave{$key} = "$nosave";

$key = q/sec:Isothermal/;
$external_latex_labels{$key} = q|4|; 
$noresave{$key} = "$nosave";

$key = q/fanno:tab:k14/;
$external_latex_labels{$key} = q|5.3|; 
$noresave{$key} = "$nosave";

$key = q/iso:tab:isoNozzlek167/;
$external_latex_labels{$key} = q|2.5|; 
$noresave{$key} = "$nosave";

$key = q/shock:tab:sClosedk13/;
$external_latex_labels{$key} = q|3.6|; 
$noresave{$key} = "$nosave";

$key = q/iso:tab:k12/;
$external_latex_labels{$key} = q|4.1|; 
$noresave{$key} = "$nosave";

$key = q/oblique:tab:k14/;
$external_latex_labels{$key} = q|8.3|; 
$noresave{$key} = "$nosave";

$key = q/oblique:tab:defk12/;
$external_latex_labels{$key} = q|8.1|; 
$noresave{$key} = "$nosave";

$key = q/ray:tab:k13/;
$external_latex_labels{$key} = q|6.2|; 
$noresave{$key} = "$nosave";

$key = q/ise:tab:k13/;
$external_latex_labels{$key} = q|1.2|; 
$noresave{$key} = "$nosave";

$key = q/oblique:tab:defk13/;
$external_latex_labels{$key} = q|8.2|; 
$noresave{$key} = "$nosave";

$key = q/fanno:tab:k13/;
$external_latex_labels{$key} = q|5.2|; 
$noresave{$key} = "$nosave";

$key = q/ise:tab:npt167/;
$external_latex_labels{$key} = q|1.8|; 
$noresave{$key} = "$nosave";

$key = q/shock:tab:open1.4/;
$external_latex_labels{$key} = q|3.11|; 
$noresave{$key} = "$nosave";

$key = q/ray:tab:k12/;
$external_latex_labels{$key} = q|6.1|; 
$noresave{$key} = "$nosave";

$key = q/iso:tab:nozzle14/;
$external_latex_labels{$key} = q|2.4|; 
$noresave{$key} = "$nosave";

$key = q/iso:tab:k13/;
$external_latex_labels{$key} = q|4.2|; 
$noresave{$key} = "$nosave";

$key = q/iso:tab:k167/;
$external_latex_labels{$key} = q|4.4|; 
$noresave{$key} = "$nosave";

$key = q/ise:tab:k14/;
$external_latex_labels{$key} = q|1.3|; 
$noresave{$key} = "$nosave";

$key = q/shock:tab:open1.3/;
$external_latex_labels{$key} = q|3.10|; 
$noresave{$key} = "$nosave";

$key = q/shock:tab:k12/;
$external_latex_labels{$key} = q|3.1|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:tab:unchoked/;
$external_latex_labels{$key} = q|4.5|; 
$noresave{$key} = "$nosave";

$key = q/ise:tab:n12/;
$external_latex_labels{$key} = q|1.5|; 
$noresave{$key} = "$nosave";

$key = q/isno:tab:k12/;
$external_latex_labels{$key} = q|2.1|; 
$noresave{$key} = "$nosave";

$key = q/fanno:tab:k167/;
$external_latex_labels{$key} = q|5.4|; 
$noresave{$key} = "$nosave";

$key = q/shock:tab:suddenlyClosed/;
$external_latex_labels{$key} = q|3.8|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:tab:basicIsothermal/;
$external_latex_labels{$key} = q|2.2|; 
$noresave{$key} = "$nosave";

$key = q/ise:tab:k12/;
$external_latex_labels{$key} = q|1.1|; 
$noresave{$key} = "$nosave";

$key = q/ray:tab:k167/;
$external_latex_labels{$key} = q|6.4|; 
$noresave{$key} = "$nosave";

$key = q/pm:tab:1.67/;
$external_latex_labels{$key} = q|7.4|; 
$noresave{$key} = "$nosave";

$key = q/shock:tab:k14/;
$external_latex_labels{$key} = q|3.3|; 
$noresave{$key} = "$nosave";

$key = q/ise:tab:nptk13/;
$external_latex_labels{$key} = q|1.6|; 
$noresave{$key} = "$nosave";

$key = q/chap:Prandtl-Meyer/;
$external_latex_labels{$key} = q|7|; 
$noresave{$key} = "$nosave";

$key = q/shock:tab:k13/;
$external_latex_labels{$key} = q|3.2|; 
$noresave{$key} = "$nosave";

$key = q/fanno:tab:basic/;
$external_latex_labels{$key} = q|5.1|; 
$noresave{$key} = "$nosave";

$key = q/pm:tab:1.4/;
$external_latex_labels{$key} = q|7.3|; 
$noresave{$key} = "$nosave";

$key = q/ise:tab:iosNoz1.3/;
$external_latex_labels{$key} = q|2.3|; 
$noresave{$key} = "$nosave";

$key = q/iso:tab:k14/;
$external_latex_labels{$key} = q|4.3|; 
$noresave{$key} = "$nosave";

$key = q/ray:tab:k14/;
$external_latex_labels{$key} = q|6.3|; 
$noresave{$key} = "$nosave";

$key = q/chap:variableArea/;
$external_latex_labels{$key} = q|1|; 
$noresave{$key} = "$nosave";

$key = q/chap:shock/;
$external_latex_labels{$key} = q|3|; 
$noresave{$key} = "$nosave";

1;

