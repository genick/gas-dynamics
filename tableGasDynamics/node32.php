<? include("header.php");?>
<table style="width:100%; height:993px;" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
	<? include("left_link.php");?>
	<td style="width: 80%; vertical-align: top;">
	<div  font-family: "arial", "helvetica", "lucida", "sans";>

<!--Navigation Panel-->
<A NAME="tex2html485"
  HREF="node33.php">
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next"
 SRC="figures/next.png"></A> 
<A NAME="tex2html481"
  HREF="node30.php">
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up"
 SRC="figures/up.png"></A> 
<A NAME="tex2html475"
  HREF="node31.php">
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous"
 SRC="figures/prev.png"></A> 
<A NAME="tex2html483"
  HREF="node1.php">
<IMG WIDTH="65" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="contents"
 SRC="figures/contents.png"></A>  
<BR>
<B> Next:</B> <A NAME="tex2html486"
  HREF="node33.php">Shock from suddenly open</A>
<B> Up:</B> <A NAME="tex2html482"
  HREF="node30.php">Shock propagating from suddenly</A>
<B> Previous:</B> <A NAME="tex2html476"
  HREF="node31.php">Shock from suddenly open</A>
 &nbsp; <B>  <A NAME="tex2html484"
  HREF="node1.php">Contents</A></B> 
<BR>
<BR>
<!--End of Navigation Panel-->

<H3><A NAME="SECTION00044200000000000000">
Shock from suddenly open valve k =1.3</A>
</H3>

<P>

<P>

<table border=1 width="100%" >
  <thead>
    <tr>
      <th align=left bgcolor="#fffeaa" colspan=3 >Shock Dynamics </th>
      <th align=left  bgcolor="#00ff5a" colspan=3  rowspan=2 >Input: My </th>
      <th align=left  bgcolor="#9ae0ee" colspan=1 rowspan=2 >k = 1.3 </th>
    </tr>
    <tr>
      <th align=left bgcolor="#fffeaa" colspan=3 > </th>
    </tr>
    <tr>
      <th align=center >Mx  </th>
      <th align=center >Mx&prime; </th>
      <th align=center >My </th>
      <th align=center >My&prime; </th>
      <th align=center >Ty/Tx </th>
      <th align=center >Py/Px </th>
      <th align=center >P0y/P0x </th>
    </tr> 
  </thead>
  <tbody>
    <tr> 
      <td align=right > 884.615      </td>
      <td align=right > 0      </td>
      <td align=right > 0.339685      </td>
      <td align=right > 2.26454      </td>
      <td align=right > 115386      </td>
      <td align=right > 884615      </td>
      <td align=right > 1.0251e-16      </td>
    </tr>
    <tr> 
      <td align=right > 13.049      </td>
      <td align=right > 0      </td>
      <td align=right > 0.346386      </td>
      <td align=right > 2.20919      </td>
      <td align=right > 26.0722      </td>
      <td align=right > 192.356      </td>
      <td align=right > 0.00014039      </td>
    </tr>
    <tr> 
      <td align=right > 9.18597      </td>
      <td align=right > 0      </td>
      <td align=right > 0.353088      </td>
      <td align=right > 2.15571      </td>
      <td align=right > 13.4066      </td>
      <td align=right > 95.2579      </td>
      <td align=right > 0.00124124      </td>
    </tr>
    <tr> 
      <td align=right > 7.46706      </td>
      <td align=right > 0      </td>
      <td align=right > 0.35979      </td>
      <td align=right > 2.10401      </td>
      <td align=right > 9.18519      </td>
      <td align=right > 62.8991      </td>
      <td align=right > 0.00421952      </td>
    </tr>
    <tr> 
      <td align=right > 6.43827      </td>
      <td align=right > 0      </td>
      <td align=right > 0.366491      </td>
      <td align=right > 2.05399      </td>
      <td align=right > 7.07514      </td>
      <td align=right > 46.7275      </td>
      <td align=right > 0.00971374      </td>
    </tr>
    <tr> 
      <td align=right > 5.73355      </td>
      <td align=right > 0      </td>
      <td align=right > 0.373193      </td>
      <td align=right > 2.00555      </td>
      <td align=right > 5.80967      </td>
      <td align=right > 37.031      </td>
      <td align=right > 0.0180819      </td>
    </tr>
    <tr> 
      <td align=right > 5.21151      </td>
      <td align=right > 0      </td>
      <td align=right > 0.379895      </td>
      <td align=right > 1.95862      </td>
      <td align=right > 4.96646      </td>
      <td align=right > 30.572      </td>
      <td align=right > 0.0294523      </td>
    </tr>
    <tr> 
      <td align=right > 4.80443      </td>
      <td align=right > 0      </td>
      <td align=right > 0.386597      </td>
      <td align=right > 1.91311      </td>
      <td align=right > 4.36454      </td>
      <td align=right > 25.9629      </td>
      <td align=right > 0.0437812      </td>
    </tr>
    <tr> 
      <td align=right > 4.47527      </td>
      <td align=right > 0      </td>
      <td align=right > 0.393298      </td>
      <td align=right > 1.86896      </td>
      <td align=right > 3.9134      </td>
      <td align=right > 22.5099      </td>
      <td align=right > 0.0609031      </td>
    </tr>
    <tr> 
      <td align=right > 4.20181      </td>
      <td align=right > 0      </td>
      <td align=right > 0.4      </td>
      <td align=right > 1.82609      </td>
      <td align=right > 3.56277      </td>
      <td align=right > 19.8276      </td>
      <td align=right > 0.080573      </td>
    </tr>
    <tr> 
      <td align=right > 3.86783      </td>
      <td align=right > 0      </td>
      <td align=right > 0.41      </td>
      <td align=right > 1.76437      </td>
      <td align=right > 3.16423      </td>
      <td align=right > 16.781      </td>
      <td align=right > 0.114022      </td>
    </tr>
    <tr> 
      <td align=right > 3.59732      </td>
      <td align=right > 0      </td>
      <td align=right > 0.42      </td>
      <td align=right > 1.70518      </td>
      <td align=right > 2.8653      </td>
      <td align=right > 14.4982      </td>
      <td align=right > 0.151442      </td>
    </tr>
    <tr> 
      <td align=right > 3.37232      </td>
      <td align=right > 0      </td>
      <td align=right > 0.43      </td>
      <td align=right > 1.64833      </td>
      <td align=right > 2.63286      </td>
      <td align=right > 12.7255      </td>
      <td align=right > 0.191788      </td>
    </tr>
    <tr> 
      <td align=right > 3.18125      </td>
      <td align=right > 0      </td>
      <td align=right > 0.44      </td>
      <td align=right > 1.59368      </td>
      <td align=right > 2.447      </td>
      <td align=right > 11.31      </td>
      <td align=right > 0.234091      </td>
    </tr>
    <tr> 
      <td align=right > 3.01633      </td>
      <td align=right > 0      </td>
      <td align=right > 0.45      </td>
      <td align=right > 1.54106      </td>
      <td align=right > 2.29502      </td>
      <td align=right > 10.1545      </td>
      <td align=right > 0.277491      </td>
    </tr>
    <tr> 
      <td align=right > 2.87204      </td>
      <td align=right > 0      </td>
      <td align=right > 0.46      </td>
      <td align=right > 1.49036      </td>
      <td align=right > 2.16847      </td>
      <td align=right > 9.19412      </td>
      <td align=right > 0.321252      </td>
    </tr>
    <tr> 
      <td align=right > 2.74441      </td>
      <td align=right > 0      </td>
      <td align=right > 0.47      </td>
      <td align=right > 1.44144      </td>
      <td align=right > 2.06146      </td>
      <td align=right > 8.38376      </td>
      <td align=right > 0.364765      </td>
    </tr>
    <tr> 
      <td align=right > 2.63044      </td>
      <td align=right > 0      </td>
      <td align=right > 0.48      </td>
      <td align=right > 1.3942      </td>
      <td align=right > 1.9698      </td>
      <td align=right > 7.69128      </td>
      <td align=right > 0.407535      </td>
    </tr>
    <tr> 
      <td align=right > 2.52785      </td>
      <td align=right > 0      </td>
      <td align=right > 0.49      </td>
      <td align=right > 1.34854      </td>
      <td align=right > 1.89042      </td>
      <td align=right > 7.09307      </td>
      <td align=right > 0.449174      </td>
    </tr>
    <tr> 
      <td align=right > 2.43487      </td>
      <td align=right > 0      </td>
      <td align=right > 0.5      </td>
      <td align=right > 1.30435      </td>
      <td align=right > 1.821      </td>
      <td align=right > 6.57143      </td>
      <td align=right > 0.489386      </td>
    </tr>
    <tr> 
      <td align=right > 2.35008      </td>
      <td align=right > 0      </td>
      <td align=right > 0.51      </td>
      <td align=right > 1.26155      </td>
      <td align=right > 1.75977      </td>
      <td align=right > 6.11279      </td>
      <td align=right > 0.527954      </td>
    </tr>
    <tr> 
      <td align=right > 2.27235      </td>
      <td align=right > 0      </td>
      <td align=right > 0.52      </td>
      <td align=right > 1.22007      </td>
      <td align=right > 1.70536      </td>
      <td align=right > 5.70663      </td>
      <td align=right > 0.564728      </td>
    </tr>
    <tr> 
      <td align=right > 2.20075      </td>
      <td align=right > 0      </td>
      <td align=right > 0.53      </td>
      <td align=right > 1.17982      </td>
      <td align=right > 1.65669      </td>
      <td align=right > 5.34461      </td>
      <td align=right > 0.59961      </td>
    </tr>
    <tr> 
      <td align=right > 2.13453      </td>
      <td align=right > 0      </td>
      <td align=right > 0.54      </td>
      <td align=right > 1.14074      </td>
      <td align=right > 1.61289      </td>
      <td align=right > 5.02008      </td>
      <td align=right > 0.63255      </td>
    </tr>
    <tr> 
      <td align=right > 2.07305      </td>
      <td align=right > 0      </td>
      <td align=right > 0.55      </td>
      <td align=right > 1.10277      </td>
      <td align=right > 1.57324      </td>
      <td align=right > 4.72765      </td>
      <td align=right > 0.663532      </td>
    </tr>
    <tr> 
      <td align=right > 2.01577      </td>
      <td align=right > 0      </td>
      <td align=right > 0.56      </td>
      <td align=right > 1.06584      </td>
      <td align=right > 1.53719      </td>
      <td align=right > 4.4629      </td>
      <td align=right > 0.69257      </td>
    </tr>
    <tr> 
      <td align=right > 1.96225      </td>
      <td align=right > 0      </td>
      <td align=right > 0.57      </td>
      <td align=right > 1.0299      </td>
      <td align=right > 1.50425      </td>
      <td align=right > 4.2222      </td>
      <td align=right > 0.719698      </td>
    </tr>
    <tr> 
      <td align=right > 1.91208      </td>
      <td align=right > 0      </td>
      <td align=right > 0.58      </td>
      <td align=right > 0.994903      </td>
      <td align=right > 1.47403      </td>
      <td align=right > 4.00251      </td>
      <td align=right > 0.744969      </td>
    </tr>
    <tr> 
      <td align=right > 1.86495      </td>
      <td align=right > 0      </td>
      <td align=right > 0.59      </td>
      <td align=right > 0.960796      </td>
      <td align=right > 1.44619      </td>
      <td align=right > 3.80128      </td>
      <td align=right > 0.768446      </td>
    </tr>
    <tr> 
      <td align=right > 1.82057      </td>
      <td align=right > 0      </td>
      <td align=right > 0.6      </td>
      <td align=right > 0.927536      </td>
      <td align=right > 1.42046      </td>
      <td align=right > 3.61635      </td>
      <td align=right > 0.790203      </td>
    </tr>
    <tr> 
      <td align=right > 1.77867      </td>
      <td align=right > 0      </td>
      <td align=right > 0.61      </td>
      <td align=right > 0.895082      </td>
      <td align=right > 1.3966      </td>
      <td align=right > 3.4459      </td>
      <td align=right > 0.810319      </td>
    </tr>
    <tr> 
      <td align=right > 1.73905      </td>
      <td align=right > 0      </td>
      <td align=right > 0.62      </td>
      <td align=right > 0.863394      </td>
      <td align=right > 1.3744      </td>
      <td align=right > 3.28834      </td>
      <td align=right > 0.828877      </td>
    </tr>
    <tr> 
      <td align=right > 1.70151      </td>
      <td align=right > 0      </td>
      <td align=right > 0.63      </td>
      <td align=right > 0.832436      </td>
      <td align=right > 1.35368      </td>
      <td align=right > 3.14233      </td>
      <td align=right > 0.84596      </td>
    </tr>
    <tr> 
      <td align=right > 1.66588      </td>
      <td align=right > 0      </td>
      <td align=right > 0.64      </td>
      <td align=right > 0.802174      </td>
      <td align=right > 1.33429      </td>
      <td align=right > 3.00669      </td>
      <td align=right > 0.861654      </td>
    </tr>
    <tr> 
      <td align=right > 1.632      </td>
      <td align=right > 0      </td>
      <td align=right > 0.65      </td>
      <td align=right > 0.772575      </td>
      <td align=right > 1.31611      </td>
      <td align=right > 2.8804      </td>
      <td align=right > 0.876042      </td>
    </tr>
    <tr> 
      <td align=right > 1.59975      </td>
      <td align=right > 0      </td>
      <td align=right > 0.66      </td>
      <td align=right > 0.74361      </td>
      <td align=right > 1.299      </td>
      <td align=right > 2.76256      </td>
      <td align=right > 0.889208      </td>
    </tr>
    <tr> 
      <td align=right > 1.56899      </td>
      <td align=right > 0      </td>
      <td align=right > 0.67      </td>
      <td align=right > 0.71525      </td>
      <td align=right > 1.28288      </td>
      <td align=right > 2.6524      </td>
      <td align=right > 0.90123      </td>
    </tr>
    <tr> 
      <td align=right > 1.53963      </td>
      <td align=right > 0      </td>
      <td align=right > 0.68      </td>
      <td align=right > 0.687468      </td>
      <td align=right > 1.26764      </td>
      <td align=right > 2.54921      </td>
      <td align=right > 0.912186      </td>
    </tr>
    <tr> 
      <td align=right > 1.51156      </td>
      <td align=right > 0      </td>
      <td align=right > 0.69      </td>
      <td align=right > 0.660239      </td>
      <td align=right > 1.25322      </td>
      <td align=right > 2.45239      </td>
      <td align=right > 0.922149      </td>
    </tr>
    <tr> 
      <td align=right > 1.48469      </td>
      <td align=right > 0      </td>
      <td align=right > 0.7      </td>
      <td align=right > 0.63354      </td>
      <td align=right > 1.23954      </td>
      <td align=right > 2.3614      </td>
      <td align=right > 0.931191      </td>
    </tr>
    <tr> 
      <td align=right > 1.45895      </td>
      <td align=right > 0      </td>
      <td align=right > 0.71      </td>
      <td align=right > 0.607348      </td>
      <td align=right > 1.22654      </td>
      <td align=right > 2.27574      </td>
      <td align=right > 0.939379      </td>
    </tr>
    <tr> 
      <td align=right > 1.43426      </td>
      <td align=right > 0      </td>
      <td align=right > 0.72      </td>
      <td align=right > 0.581643      </td>
      <td align=right > 1.21415      </td>
      <td align=right > 2.19499      </td>
      <td align=right > 0.946776      </td>
    </tr>
    <tr> 
      <td align=right > 1.41056      </td>
      <td align=right > 0      </td>
      <td align=right > 0.73      </td>
      <td align=right > 0.556403      </td>
      <td align=right > 1.20234      </td>
      <td align=right > 2.11876      </td>
      <td align=right > 0.953442      </td>
    </tr>
    <tr> 
      <td align=right > 1.38778      </td>
      <td align=right > 0      </td>
      <td align=right > 0.74      </td>
      <td align=right > 0.53161      </td>
      <td align=right > 1.19106      </td>
      <td align=right > 2.0467      </td>
      <td align=right > 0.959434      </td>
    </tr>
    <tr> 
      <td align=right > 1.36587      </td>
      <td align=right > 0      </td>
      <td align=right > 0.75      </td>
      <td align=right > 0.507246      </td>
      <td align=right > 1.18025      </td>
      <td align=right > 1.97849      </td>
      <td align=right > 0.964806      </td>
    </tr>
    <tr> 
      <td align=right > 1.34477      </td>
      <td align=right > 0      </td>
      <td align=right > 0.76      </td>
      <td align=right > 0.483295      </td>
      <td align=right > 1.1699      </td>
      <td align=right > 1.91386      </td>
      <td align=right > 0.969607      </td>
    </tr>
    <tr> 
      <td align=right > 1.32445      </td>
      <td align=right > 0      </td>
      <td align=right > 0.77      </td>
      <td align=right > 0.45974      </td>
      <td align=right > 1.15996      </td>
      <td align=right > 1.85254      </td>
      <td align=right > 0.973885      </td>
    </tr>
    <tr> 
      <td align=right > 1.30485      </td>
      <td align=right > 0      </td>
      <td align=right > 0.78      </td>
      <td align=right > 0.436566      </td>
      <td align=right > 1.15041      </td>
      <td align=right > 1.7943      </td>
      <td align=right > 0.977684      </td>
    </tr>
    <tr> 
      <td align=right > 1.28595      </td>
      <td align=right > 0      </td>
      <td align=right > 0.79      </td>
      <td align=right > 0.413759      </td>
      <td align=right > 1.14121      </td>
      <td align=right > 1.73892      </td>
      <td align=right > 0.981044      </td>
    </tr>
    <tr> 
      <td align=right > 1.26769      </td>
      <td align=right > 0      </td>
      <td align=right > 0.8      </td>
      <td align=right > 0.391304      </td>
      <td align=right > 1.13235      </td>
      <td align=right > 1.68622      </td>
      <td align=right > 0.984004      </td>
    </tr>
    <tr> 
      <td align=right > 1.25005      </td>
      <td align=right > 0      </td>
      <td align=right > 0.81      </td>
      <td align=right > 0.369189      </td>
      <td align=right > 1.1238      </td>
      <td align=right > 1.63601      </td>
      <td align=right > 0.9866      </td>
    </tr>
    <tr> 
      <td align=right > 1.23299      </td>
      <td align=right > 0      </td>
      <td align=right > 0.82      </td>
      <td align=right > 0.347402      </td>
      <td align=right > 1.11553      </td>
      <td align=right > 1.58813      </td>
      <td align=right > 0.988866      </td>
    </tr>
    <tr> 
      <td align=right > 1.21649      </td>
      <td align=right > 0      </td>
      <td align=right > 0.83      </td>
      <td align=right > 0.32593      </td>
      <td align=right > 1.10753      </td>
      <td align=right > 1.54244      </td>
      <td align=right > 0.990832      </td>
    </tr>
    <tr> 
      <td align=right > 1.20052      </td>
      <td align=right > 0      </td>
      <td align=right > 0.84      </td>
      <td align=right > 0.304762      </td>
      <td align=right > 1.09979      </td>
      <td align=right > 1.4988      </td>
      <td align=right > 0.992527      </td>
    </tr>
    <tr> 
      <td align=right > 1.18505      </td>
      <td align=right > 0      </td>
      <td align=right > 0.85      </td>
      <td align=right > 0.283887      </td>
      <td align=right > 1.09228      </td>
      <td align=right > 1.45708      </td>
      <td align=right > 0.993978      </td>
    </tr>
    <tr> 
      <td align=right > 1.17006      </td>
      <td align=right > 0      </td>
      <td align=right > 0.86      </td>
      <td align=right > 0.263296      </td>
      <td align=right > 1.08499      </td>
      <td align=right > 1.41716      </td>
      <td align=right > 0.995211      </td>
    </tr>
    <tr> 
      <td align=right > 1.15552      </td>
      <td align=right > 0      </td>
      <td align=right > 0.87      </td>
      <td align=right > 0.242979      </td>
      <td align=right > 1.0779      </td>
      <td align=right > 1.37895      </td>
      <td align=right > 0.996249      </td>
    </tr>
    <tr> 
      <td align=right > 1.14142      </td>
      <td align=right > 0      </td>
      <td align=right > 0.88      </td>
      <td align=right > 0.222925      </td>
      <td align=right > 1.07102      </td>
      <td align=right > 1.34233      </td>
      <td align=right > 0.997113      </td>
    </tr>
    <tr> 
      <td align=right > 1.12773      </td>
      <td align=right > 0      </td>
      <td align=right > 0.89      </td>
      <td align=right > 0.203127      </td>
      <td align=right > 1.06431      </td>
      <td align=right > 1.30722      </td>
      <td align=right > 0.997823      </td>
    </tr>
    <tr> 
      <td align=right > 1.11444      </td>
      <td align=right > 0      </td>
      <td align=right > 0.9      </td>
      <td align=right > 0.183575      </td>
      <td align=right > 1.05778      </td>
      <td align=right > 1.27353      </td>
      <td align=right > 0.998399      </td>
    </tr>
    <tr> 
      <td align=right > 1.10153      </td>
      <td align=right > 0      </td>
      <td align=right > 0.91      </td>
      <td align=right > 0.164262      </td>
      <td align=right > 1.0514      </td>
      <td align=right > 1.24119      </td>
      <td align=right > 0.998857      </td>
    </tr>
    <tr> 
      <td align=right > 1.08898      </td>
      <td align=right > 0      </td>
      <td align=right > 0.92      </td>
      <td align=right > 0.14518      </td>
      <td align=right > 1.04518      </td>
      <td align=right > 1.21012      </td>
      <td align=right > 0.999214      </td>
    </tr>
    <tr> 
      <td align=right > 1.07678      </td>
      <td align=right > 0      </td>
      <td align=right > 0.93      </td>
      <td align=right > 0.126321      </td>
      <td align=right > 1.03911      </td>
      <td align=right > 1.18025      </td>
      <td align=right > 0.999484      </td>
    </tr>
    <tr> 
      <td align=right > 1.06491      </td>
      <td align=right > 0      </td>
      <td align=right > 0.94      </td>
      <td align=right > 0.107678      </td>
      <td align=right > 1.03317      </td>
      <td align=right > 1.15152      </td>
      <td align=right > 0.999682      </td>
    </tr>
    <tr> 
      <td align=right > 1.05336      </td>
      <td align=right > 0      </td>
      <td align=right > 0.95      </td>
      <td align=right > 0.0892449      </td>
      <td align=right > 1.02736      </td>
      <td align=right > 1.12387      </td>
      <td align=right > 0.999819      </td>
    </tr>
    <tr> 
      <td align=right > 1.04212      </td>
      <td align=right > 0      </td>
      <td align=right > 0.96      </td>
      <td align=right > 0.0710145      </td>
      <td align=right > 1.02167      </td>
      <td align=right > 1.09724      </td>
      <td align=right > 0.999909      </td>
    </tr>
    <tr> 
      <td align=right > 1.03118      </td>
      <td align=right > 0      </td>
      <td align=right > 0.97      </td>
      <td align=right > 0.0529807      </td>
      <td align=right > 1.01609      </td>
      <td align=right > 1.07159      </td>
      <td align=right > 0.999963      </td>
    </tr>
    <tr> 
      <td align=right > 1.02052      </td>
      <td align=right > 0      </td>
      <td align=right > 0.98      </td>
      <td align=right > 0.0351375      </td>
      <td align=right > 1.01063      </td>
      <td align=right > 1.04686      </td>
      <td align=right > 0.999989      </td>
    </tr>
    <tr> 
      <td align=right > 1.01013      </td>
      <td align=right > 0      </td>
      <td align=right > 0.99      </td>
      <td align=right > 0.0174791      </td>
      <td align=right > 1.00526      </td>
      <td align=right > 1.02301      </td>
      <td align=right > 0.999999      </td>
    </tr>
    <tr> 
      <td align=right > 1      </td>
      <td align=right > 0      </td>
      <td align=right > 1      </td>
      <td align=right > 0      </td>
      <td align=right > 1      </td>
      <td align=right > 1      </td>
      <td align=right > 1      </td>
    </tr>
  </tbody>
</table>
<BR><HR>
<ADDRESS>
genick
2007-11-14
</ADDRESS>
        </tr>
    </tbody>
    </table>
    <? include("aboutPottoProject.php");  ?>
    </div>
    </td>
    </tr>
    </tbody>
</table>
<? include("bottom.php"); ?>

