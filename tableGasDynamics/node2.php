<? include("header.php");?>
<table style="width:100%; height:993px;" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
	<? include("left_link.php");?>
	<td style="width: 80%; vertical-align: top;">
	<div  font-family: "arial", "helvetica", "lucida", "sans";>

<!--Navigation Panel-->
<A NAME="tex2html89"
  HREF="node3.php">
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next"
 SRC="figures/next.png"></A> 
<A NAME="tex2html85"
  HREF="tableGasDynamics.php">
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up"
 SRC="figures/up.png"></A> 
<A NAME="tex2html79"
  HREF="node1.php">
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous"
 SRC="figures/prev.png"></A> 
<A NAME="tex2html87"
  HREF="node1.php">
<IMG WIDTH="65" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="contents"
 SRC="figures/contents.png"></A>  
<BR>
<B> Next:</B> <A NAME="tex2html90"
  HREF="node3.php">Regular Isentropic Flow Tables</A>
<B> Up:</B> <A NAME="tex2html86"
  HREF="tableGasDynamics.php">tableGasDynamics</A>
<B> Previous:</B> <A NAME="tex2html80"
  HREF="node1.php">Contents</A>
 &nbsp; <B>  <A NAME="tex2html88"
  HREF="node1.php">Contents</A></B> 
<BR>
<BR>
<!--End of Navigation Panel-->

<H1><A NAME="SECTION00020000000000000000"></A>
<A NAME="chap:variableArea"></A>
<BR>
Isentropic Flow
</H1>

<P>
<BR><HR>
<!--Table of Child-Links-->
<A NAME="CHILD_LINKS"><STRONG>Subsections</STRONG></A>

<UL>
<LI><A NAME="tex2html91"
  HREF="node3.php">Regular Isentropic Flow Tables</A>
<UL>
<LI><A NAME="tex2html92"
  HREF="node4.php">Isentropic Flow for k=1.2</A>
<LI><A NAME="tex2html93"
  HREF="node5.php">Isentropic Flow for k=1.3</A>
<LI><A NAME="tex2html94"
  HREF="node6.php">Isentropic Flow for k =1.4</A>
<LI><A NAME="tex2html95"
  HREF="node7.php">Isentropic Flow k=1.67</A>
</UL>
<BR>
<LI><A NAME="tex2html96"
  HREF="node8.php">``Naughty Professor'' Tables (NPT) for Isentropic Flow</A>
<UL>
<LI><A NAME="tex2html97"
  HREF="node9.php">NPT for Isnetropic Flow k=1.2</A>
<LI><A NAME="tex2html98"
  HREF="node10.php">NPT for Isentropic Flow k=1.3</A>
<LI><A NAME="tex2html99"
  HREF="node11.php">NPT for Isentropic Flow k=1.4</A>
<LI><A NAME="tex2html100"
  HREF="node12.php">NPT for Isentropic Flow k=1.67</A>
</UL></UL>
<!--End of Table of Child-Links-->
<BR><HR>
<ADDRESS>
genick
2007-11-14
</ADDRESS>
        </tr>
    </tbody>
    </table>
    <? include("aboutPottoProject.php");  ?>
    </div>
    </td>
    </tr>
    </tbody>
</table>
<? include("bottom.php"); ?>

