# LaTeX2HTML 2002-2-1 (1.71)
# Associate internals original text with physical files.


$key = q/chap:fanno/;
$ref_files{$key} = "$dir".q|node41.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:Prandtl-Meyer/;
$ref_files{$key} = "$dir".q|node51.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:variableArea/;
$ref_files{$key} = "$dir".q|node2.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:shock/;
$ref_files{$key} = "$dir".q|node18.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:Isothermal/;
$ref_files{$key} = "$dir".q|node35.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:Isothermal/;
$ref_files{$key} = "$dir".q|node35.html|; 
$noresave{$key} = "$nosave";

1;

