<? include("header.php");?>
<table style="width:100%; height:993px;" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
	<? include("left_link.php");?>
	<td style="width: 80%; vertical-align: top;">
	<div  font-family: "arial", "helvetica", "lucida", "sans";>

<!--Navigation Panel-->
<A NAME="tex2html199"
  HREF="node11.php">
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next"
 SRC="figures/next.png"></A> 
<A NAME="tex2html195"
  HREF="node8.php">
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up"
 SRC="figures/up.png"></A> 
<A NAME="tex2html189"
  HREF="node9.php">
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous"
 SRC="figures/prev.png"></A> 
<A NAME="tex2html197"
  HREF="node1.php">
<IMG WIDTH="65" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="contents"
 SRC="figures/contents.png"></A>  
<BR>
<B> Next:</B> <A NAME="tex2html200"
  HREF="node11.php">NPT for Isentropic Flow</A>
<B> Up:</B> <A NAME="tex2html196"
  HREF="node8.php">``Naughty Professor'' Tables (NPT)</A>
<B> Previous:</B> <A NAME="tex2html190"
  HREF="node9.php">NPT for Isnetropic Flow</A>
 &nbsp; <B>  <A NAME="tex2html198"
  HREF="node1.php">Contents</A></B> 
<BR>
<BR>
<!--End of Navigation Panel-->

<H3><A NAME="SECTION00022200000000000000">
NPT for Isentropic Flow k=1.3</A>
</H3>

<P>

<table border=1 width="100%" >
  <thead>
    <tr>
      <th align=left bgcolor="#fffeaa" colspan=2 >Isentropic Flow </th>
      <th align=left  bgcolor="#00ff5a" colspan=3 >Input: M </th>
      <th align=left  bgcolor="#9ae0ee" colspan=1 >k = 1.3 </th>
    </tr>
    <tr>
      <th align=center >M </th>
      <th align=center >Fn </th>
      <th align=center >&rho;RT<sub>0</sub>/P </th>
      <th align=center >(P<sub>0</sub>A<sup>*</sup>/PA)^2 </th>
      <th align=center >RT<sub>0</sub>/P^2 mA^2 </th>
      <th align=center >1/R&rho;<sub>0</sub>P mA^2 </th>
      <th align=center >1/R&rho;<sub>0</sub>^2T mA^2 </th>
    </tr> 
  </thead>
  <tbody>
    <tr> 
      <td align=right > 1e-6      </td>
      <td align=right > 1.3e-6      </td>
      <td align=right > 1      </td>
      <td align=right > 2.91978e-12      </td>
      <td align=right > 3.79571e-24      </td>
      <td align=right > 1.3e-12      </td>
      <td align=right > 1.3e-12      </td>
    </tr>
    <tr> 
      <td align=right > 0.0500005      </td>
      <td align=right > 0.0650941      </td>
      <td align=right > 1.00038      </td>
      <td align=right > 0.00730233      </td>
      <td align=right > 2.38014e-5      </td>
      <td align=right > 0.00326472      </td>
      <td align=right > 0.00326064      </td>
    </tr>
    <tr> 
      <td align=right > 0.1      </td>
      <td align=right > 0.130749      </td>
      <td align=right > 1.0015      </td>
      <td align=right > 0.0292416      </td>
      <td align=right > 0.000384534      </td>
      <td align=right > 0.0132359      </td>
      <td align=right > 0.01317      </td>
    </tr>
    <tr> 
      <td align=right > 0.2      </td>
      <td align=right > 0.266031      </td>
      <td align=right > 1.006      </td>
      <td align=right > 0.117492      </td>
      <td align=right > 0.00639631      </td>
      <td align=right > 0.0558701      </td>
      <td align=right > 0.054767      </td>
    </tr>
    <tr> 
      <td align=right > 0.25      </td>
      <td align=right > 0.336836      </td>
      <td align=right > 1.00937      </td>
      <td align=right > 0.184197      </td>
      <td align=right > 0.0160759      </td>
      <td align=right > 0.090877      </td>
      <td align=right > 0.0880938      </td>
    </tr>
    <tr> 
      <td align=right > 0.3      </td>
      <td align=right > 0.410572      </td>
      <td align=right > 1.0135      </td>
      <td align=right > 0.266328      </td>
      <td align=right > 0.0345343      </td>
      <td align=right > 0.137427      </td>
      <td align=right > 0.131419      </td>
    </tr>
    <tr> 
      <td align=right > 0.35      </td>
      <td align=right > 0.487893      </td>
      <td align=right > 1.01838      </td>
      <td align=right > 0.364245      </td>
      <td align=right > 0.0666959      </td>
      <td align=right > 0.19814      </td>
      <td align=right > 0.186472      </td>
    </tr>
    <tr> 
      <td align=right > 0.4      </td>
      <td align=right > 0.569491      </td>
      <td align=right > 1.024      </td>
      <td align=right > 0.478377      </td>
      <td align=right > 0.119344      </td>
      <td align=right > 0.276479      </td>
      <td align=right > 0.255464      </td>
    </tr>
    <tr> 
      <td align=right > 0.45      </td>
      <td align=right > 0.656102      </td>
      <td align=right > 1.03038      </td>
      <td align=right > 0.609215      </td>
      <td align=right > 0.20173      </td>
      <td align=right > 0.376975      </td>
      <td align=right > 0.341189      </td>
    </tr>
    <tr> 
      <td align=right > 0.5      </td>
      <td align=right > 0.748516      </td>
      <td align=right > 1.0375      </td>
      <td align=right > 0.757318      </td>
      <td align=right > 0.32639      </td>
      <td align=right > 0.505523      </td>
      <td align=right > 0.447144      </td>
    </tr>
    <tr> 
      <td align=right > 0.55      </td>
      <td align=right > 0.847583      </td>
      <td align=right > 1.04537      </td>
      <td align=right > 0.92331      </td>
      <td align=right > 0.510234      </td>
      <td align=right > 0.669783      </td>
      <td align=right > 0.577688      </td>
    </tr>
    <tr> 
      <td align=right > 0.6      </td>
      <td align=right > 0.954224      </td>
      <td align=right > 1.054      </td>
      <td align=right > 1.10788      </td>
      <td align=right > 0.77598      </td>
      <td align=right > 0.879697      </td>
      <td align=right > 0.73824      </td>
    </tr>
    <tr> 
      <td align=right > 0.65      </td>
      <td align=right > 1.06944      </td>
      <td align=right > 1.06337      </td>
      <td align=right > 1.31179      </td>
      <td align=right > 1.15406      </td>
      <td align=right > 1.14817      </td>
      <td align=right > 0.935519      </td>
    </tr>
    <tr> 
      <td align=right > 0.7      </td>
      <td align=right > 1.19431      </td>
      <td align=right > 1.0735      </td>
      <td align=right > 1.53585      </td>
      <td align=right > 1.68514      </td>
      <td align=right > 1.49198      </td>
      <td align=right > 1.17785      </td>
    </tr>
    <tr> 
      <td align=right > 0.75      </td>
      <td align=right > 1.33002      </td>
      <td align=right > 1.08438      </td>
      <td align=right > 1.78095      </td>
      <td align=right > 2.42341      </td>
      <td align=right > 1.93295      </td>
      <td align=right > 1.47555      </td>
    </tr>
    <tr> 
      <td align=right > 0.8      </td>
      <td align=right > 1.47788      </td>
      <td align=right > 1.096      </td>
      <td align=right > 2.04805      </td>
      <td align=right > 3.44095      </td>
      <td align=right > 2.49948      </td>
      <td align=right > 1.8414      </td>
    </tr>
    <tr> 
      <td align=right > 0.85      </td>
      <td align=right > 1.63931      </td>
      <td align=right > 1.10838      </td>
      <td align=right > 2.33816      </td>
      <td align=right > 4.83341      </td>
      <td align=right > 3.22866      </td>
      <td align=right > 2.29122      </td>
    </tr>
    <tr> 
      <td align=right > 0.9      </td>
      <td align=right > 1.81586      </td>
      <td align=right > 1.1215      </td>
      <td align=right > 2.65237      </td>
      <td align=right > 6.72755      </td>
      <td align=right > 4.16888      </td>
      <td align=right > 2.84461      </td>
    </tr>
    <tr> 
      <td align=right > 0.95      </td>
      <td align=right > 2.00925      </td>
      <td align=right > 1.13538      </td>
      <td align=right > 2.99183      </td>
      <td align=right > 9.29099      </td>
      <td align=right > 5.38347      </td>
      <td align=right > 3.52586      </td>
    </tr>
    <tr> 
      <td align=right > 1      </td>
      <td align=right > 2.22136      </td>
      <td align=right > 1.15      </td>
      <td align=right > 3.35775      </td>
      <td align=right > 12.745      </td>
      <td align=right > 6.95533      </td>
      <td align=right > 4.36507      </td>
    </tr>
    <tr> 
      <td align=right > 1.05      </td>
      <td align=right > 2.45424      </td>
      <td align=right > 1.16538      </td>
      <td align=right > 3.75141      </td>
      <td align=right > 17.3815      </td>
      <td align=right > 8.99312      </td>
      <td align=right > 5.39956      </td>
    </tr>
    <tr> 
      <td align=right > 1.1      </td>
      <td align=right > 2.71018      </td>
      <td align=right > 1.1815      </td>
      <td align=right > 4.17416      </td>
      <td align=right > 23.5843      </td>
      <td align=right > 11.6395      </td>
      <td align=right > 6.67556      </td>
    </tr>
    <tr> 
      <td align=right > 1.15      </td>
      <td align=right > 2.99167      </td>
      <td align=right > 1.19837      </td>
      <td align=right > 4.62742      </td>
      <td align=right > 31.8582      </td>
      <td align=right > 15.0818      </td>
      <td align=right > 8.25042      </td>
    </tr>
    <tr> 
      <td align=right > 1.2      </td>
      <td align=right > 3.30144      </td>
      <td align=right > 1.216      </td>
      <td align=right > 5.11265      </td>
      <td align=right > 42.8657      </td>
      <td align=right > 19.5663      </td>
      <td align=right > 10.1952      </td>
    </tr>
    <tr> 
      <td align=right > 1.25      </td>
      <td align=right > 3.64251      </td>
      <td align=right > 1.23438      </td>
      <td align=right > 5.63141      </td>
      <td align=right > 57.4745      </td>
      <td align=right > 25.4173      </td>
      <td align=right > 12.5981      </td>
    </tr>
    <tr> 
      <td align=right > 1.3      </td>
      <td align=right > 4.01819      </td>
      <td align=right > 1.2535      </td>
      <td align=right > 6.18531      </td>
      <td align=right > 76.8206      </td>
      <td align=right > 33.0615      </td>
      <td align=right > 15.5683      </td>
    </tr>
    <tr> 
      <td align=right > 1.35      </td>
      <td align=right > 4.4321      </td>
      <td align=right > 1.27338      </td>
      <td align=right > 6.77601      </td>
      <td align=right > 102.388      </td>
      <td align=right > 43.0613      </td>
      <td align=right > 19.2412      </td>
    </tr>
    <tr> 
      <td align=right > 1.4      </td>
      <td align=right > 4.88824      </td>
      <td align=right > 1.294      </td>
      <td align=right > 7.40526      </td>
      <td align=right > 136.114      </td>
      <td align=right > 56.1579      </td>
      <td align=right > 23.7846      </td>
    </tr>
    <tr> 
      <td align=right > 1.45      </td>
      <td align=right > 5.39098      </td>
      <td align=right > 1.31538      </td>
      <td align=right > 8.07487      </td>
      <td align=right > 180.521      </td>
      <td align=right > 73.3288      </td>
      <td align=right > 29.4064      </td>
    </tr>
    <tr> 
      <td align=right > 1.5      </td>
      <td align=right > 5.94512      </td>
      <td align=right > 1.3375      </td>
      <td align=right > 8.78671      </td>
      <td align=right > 238.894      </td>
      <td align=right > 95.8631      </td>
      <td align=right > 36.364      </td>
    </tr>
    <tr> 
      <td align=right > 1.55      </td>
      <td align=right > 6.55591      </td>
      <td align=right > 1.36038      </td>
      <td align=right > 9.54272      </td>
      <td align=right > 315.497      </td>
      <td align=right > 125.461      </td>
      <td align=right > 44.9761      </td>
    </tr>
    <tr> 
      <td align=right > 1.6      </td>
      <td align=right > 7.22911      </td>
      <td align=right > 1.384      </td>
      <td align=right > 10.3449      </td>
      <td align=right > 415.865      </td>
      <td align=right > 164.368      </td>
      <td align=right > 55.6369      </td>
    </tr>
    <tr> 
      <td align=right > 1.65      </td>
      <td align=right > 7.97102      </td>
      <td align=right > 1.40838      </td>
      <td align=right > 11.1953      </td>
      <td align=right > 547.169      </td>
      <td align=right > 215.542      </td>
      <td align=right > 68.834      </td>
    </tr>
    <tr> 
      <td align=right > 1.7      </td>
      <td align=right > 8.78854      </td>
      <td align=right > 1.4335      </td>
      <td align=right > 12.0961      </td>
      <td align=right > 718.68      </td>
      <td align=right > 282.887      </td>
      <td align=right > 85.1702      </td>
    </tr>
    <tr> 
      <td align=right > 1.75      </td>
      <td align=right > 9.6892      </td>
      <td align=right > 1.45938      </td>
      <td align=right > 13.0495      </td>
      <td align=right > 942.379      </td>
      <td align=right > 371.554      </td>
      <td align=right > 105.39      </td>
    </tr>
    <tr> 
      <td align=right > 1.8      </td>
      <td align=right > 10.6812      </td>
      <td align=right > 1.486      </td>
      <td align=right > 14.0577      </td>
      <td align=right > 1233.71      </td>
      <td align=right > 488.331      </td>
      <td align=right > 130.412      </td>
    </tr>
    <tr> 
      <td align=right > 1.85      </td>
      <td align=right > 11.7736      </td>
      <td align=right > 1.51338      </td>
      <td align=right > 15.1231      </td>
      <td align=right > 1612.56      </td>
      <td align=right > 642.163      </td>
      <td align=right > 161.37      </td>
    </tr>
    <tr> 
      <td align=right > 1.9      </td>
      <td align=right > 12.9762      </td>
      <td align=right > 1.5415      </td>
      <td align=right > 16.248      </td>
      <td align=right > 2104.51      </td>
      <td align=right > 844.837      </td>
      <td align=right > 199.662      </td>
    </tr>
    <tr> 
      <td align=right > 1.95      </td>
      <td align=right > 14.2996      </td>
      <td align=right > 1.57038      </td>
      <td align=right > 17.435      </td>
      <td align=right > 2742.38      </td>
      <td align=right > 1111.87      </td>
      <td align=right > 247.007      </td>
    </tr>
    <tr> 
      <td align=right > 2      </td>
      <td align=right > 15.7555      </td>
      <td align=right > 1.6      </td>
      <td align=right > 18.6866      </td>
      <td align=right > 3568.23      </td>
      <td align=right > 1463.67      </td>
      <td align=right > 305.522      </td>
    </tr>
    <tr> 
      <td align=right > 2.25      </td>
      <td align=right > 25.5074      </td>
      <td align=right > 1.75937      </td>
      <td align=right > 26.006      </td>
      <td align=right > 13015.6      </td>
      <td align=right > 5789.08      </td>
      <td align=right > 880.538      </td>
    </tr>
    <tr> 
      <td align=right > 2.5      </td>
      <td align=right > 41.0182      </td>
      <td align=right > 1.9375      </td>
      <td align=right > 35.3567      </td>
      <td align=right > 45759.5      </td>
      <td align=right > 22736.5      </td>
      <td align=right > 2507.56      </td>
    </tr>
    <tr> 
      <td align=right > 2.75      </td>
      <td align=right > 65.3854      </td>
      <td align=right > 2.13438      </td>
      <td align=right > 47.1288      </td>
      <td align=right > 154990      </td>
      <td align=right > 87873.4      </td>
      <td align=right > 7019.21      </td>
    </tr>
    <tr> 
      <td align=right > 3      </td>
      <td align=right > 103.155      </td>
      <td align=right > 2.35      </td>
      <td align=right > 61.7533      </td>
      <td align=right > 505475      </td>
      <td align=right > 331895      </td>
      <td align=right > 19235.7      </td>
    </tr>
    <tr> 
      <td align=right > 3.25      </td>
      <td align=right > 160.888      </td>
      <td align=right > 2.58438      </td>
      <td align=right > 79.7026      </td>
      <td align=right > 1.58699e+6      </td>
      <td align=right > 1.21892e+6      </td>
      <td align=right > 51458.6      </td>
    </tr>
    <tr> 
      <td align=right > 3.5      </td>
      <td align=right > 247.894      </td>
      <td align=right > 2.8375      </td>
      <td align=right > 101.49      </td>
      <td align=right > 4.79744e+6      </td>
      <td align=right > 4.3382e+6      </td>
      <td align=right > 134129      </td>
    </tr>
    <tr> 
      <td align=right > 3.75      </td>
      <td align=right > 377.185      </td>
      <td align=right > 3.10938      </td>
      <td align=right > 127.669      </td>
      <td align=right > 1.39718e+7      </td>
      <td align=right > 1.49308e+7      </td>
      <td align=right > 340282      </td>
    </tr>
    <tr> 
      <td align=right > 4      </td>
      <td align=right > 566.682      </td>
      <td align=right > 3.4      </td>
      <td align=right > 158.836      </td>
      <td align=right > 3.92359e+7      </td>
      <td align=right > 4.96376e+7      </td>
      <td align=right > 839874      </td>
    </tr>
    <tr> 
      <td align=right > 4.25      </td>
      <td align=right > 840.721      </td>
      <td align=right > 3.70938      </td>
      <td align=right > 195.627      </td>
      <td align=right > 1.06363e+8      </td>
      <td align=right > 1.59342e+8      </td>
      <td align=right > 2.01679e+6      </td>
    </tr>
    <tr> 
      <td align=right > 4.5      </td>
      <td align=right > 1231.93      </td>
      <td align=right > 4.0375      </td>
      <td align=right > 238.719      </td>
      <td align=right > 2.78688e+8      </td>
      <td align=right > 4.93991e+8      </td>
      <td align=right > 4.7135e+6      </td>
    </tr>
    <tr> 
      <td align=right > 4.75      </td>
      <td align=right > 1783.54      </td>
      <td align=right > 4.38438      </td>
      <td align=right > 288.832      </td>
      <td align=right > 7.06753e+8      </td>
      <td align=right > 1.47987e+9      </td>
      <td align=right > 1.07283e+7      </td>
    </tr>
    <tr> 
      <td align=right > 5      </td>
      <td align=right > 2552.15      </td>
      <td align=right > 4.75      </td>
      <td align=right > 346.724      </td>
      <td align=right > 1.73721e+9      </td>
      <td align=right > 4.28753e+9      </td>
      <td align=right > 2.37991e+7      </td>
    </tr>
    <tr> 
      <td align=right > 5.25      </td>
      <td align=right > 3611.09      </td>
      <td align=right > 5.13438      </td>
      <td align=right > 413.196      </td>
      <td align=right > 4.14467e+9      </td>
      <td align=right > 1.20258e+10      </td>
      <td align=right > 5.15016e+7      </td>
    </tr>
    <tr> 
      <td align=right > 5.5      </td>
      <td align=right > 5054.47      </td>
      <td align=right > 5.5375      </td>
      <td align=right > 489.091      </td>
      <td align=right > 9.61162e+9      </td>
      <td align=right > 3.26914e+10      </td>
      <td align=right > 1.08823e+8      </td>
    </tr>
    <tr> 
      <td align=right > 5.75      </td>
      <td align=right > 7001.87      </td>
      <td align=right > 5.95938      </td>
      <td align=right > 575.29      </td>
      <td align=right > 2.16956e+10      </td>
      <td align=right > 8.62358e+10      </td>
      <td align=right > 2.24743e+8      </td>
    </tr>
    <tr> 
      <td align=right > 6      </td>
      <td align=right > 9603.98      </td>
      <td align=right > 6.4      </td>
      <td align=right > 672.717      </td>
      <td align=right > 4.773e+10      </td>
      <td align=right > 2.21007e+11      </td>
      <td align=right > 4.54087e+8      </td>
    </tr>
    <tr> 
      <td align=right > 6.25      </td>
      <td align=right > 13049.1      </td>
      <td align=right > 6.85938      </td>
      <td align=right > 782.338      </td>
      <td align=right > 1.02474e+11      </td>
      <td align=right > 5.50957e+11      </td>
      <td align=right > 8.98468e+8      </td>
    </tr>
    <tr> 
      <td align=right > 6.5      </td>
      <td align=right > 17570.8      </td>
      <td align=right > 7.3375      </td>
      <td align=right > 905.159      </td>
      <td align=right > 2.14963e+11      </td>
      <td align=right > 1.33767e+12      </td>
      <td align=right > 1.74256e+9      </td>
    </tr>
    <tr> 
      <td align=right > 6.75      </td>
      <td align=right > 23456.5      </td>
      <td align=right > 7.83438      </td>
      <td align=right > 1042.23      </td>
      <td align=right > 4.41109e+11      </td>
      <td align=right > 3.16667e+12      </td>
      <td align=right > 3.3158e+9      </td>
    </tr>
    <tr> 
      <td align=right > 7      </td>
      <td align=right > 31058      </td>
      <td align=right > 8.35      </td>
      <td align=right > 1194.63      </td>
      <td align=right > 8.86411e+11      </td>
      <td align=right > 7.31774e+12      </td>
      <td align=right > 6.19568e+9      </td>
    </tr>
    <tr> 
      <td align=right > 7.25      </td>
      <td align=right > 40802.3      </td>
      <td align=right > 8.88438      </td>
      <td align=right > 1363.49      </td>
      <td align=right > 1.74614e+12      </td>
      <td align=right > 1.6525e+13      </td>
      <td align=right > 1.13776e+10      </td>
    </tr>
    <tr> 
      <td align=right > 7.5      </td>
      <td align=right > 53205.5      </td>
      <td align=right > 9.4375      </td>
      <td align=right > 1549.99      </td>
      <td align=right > 3.3752e+12      </td>
      <td align=right > 3.65048e+13      </td>
      <td align=right > 2.05507e+10      </td>
    </tr>
    <tr> 
      <td align=right > 7.75      </td>
      <td align=right > 68887.2      </td>
      <td align=right > 10.0094      </td>
      <td align=right > 1755.34      </td>
      <td align=right > 6.40758e+12      </td>
      <td align=right > 7.89642e+13      </td>
      <td align=right > 3.65376e+10      </td>
    </tr>
    <tr> 
      <td align=right > 8      </td>
      <td align=right > 88587.5      </td>
      <td align=right > 10.6      </td>
      <td align=right > 1980.78      </td>
      <td align=right > 1.19574e+13      </td>
      <td align=right > 1.67415e+14      </td>
      <td align=right > 6.39893e+10      </td>
    </tr>
    <tr> 
      <td align=right > 8.25      </td>
      <td align=right > 113186      </td>
      <td align=right > 11.2094      </td>
      <td align=right > 2227.61      </td>
      <td align=right > 2.19524e+13      </td>
      <td align=right > 3.48199e+14      </td>
      <td align=right > 1.10465e+11      </td>
    </tr>
    <tr> 
      <td align=right > 8.5      </td>
      <td align=right > 143723      </td>
      <td align=right > 11.8375      </td>
      <td align=right > 2497.17      </td>
      <td align=right > 3.96785e+13      </td>
      <td align=right > 7.11052e+14      </td>
      <td align=right > 1.88091e+11      </td>
    </tr>
    <tr> 
      <td align=right > 8.75      </td>
      <td align=right > 181422      </td>
      <td align=right > 12.4844      </td>
      <td align=right > 2790.83      </td>
      <td align=right > 7.06596e+13      </td>
      <td align=right > 1.42679e+15      </td>
      <td align=right > 3.16086e+11      </td>
    </tr>
    <tr> 
      <td align=right > 9      </td>
      <td align=right > 227720      </td>
      <td align=right > 13.15      </td>
      <td align=right > 3110      </td>
      <td align=right > 1.24057e+14      </td>
      <td align=right > 2.81538e+15      </td>
      <td align=right > 5.24548e+11      </td>
    </tr>
    <tr> 
      <td align=right > 9.25      </td>
      <td align=right > 284291      </td>
      <td align=right > 13.8344      </td>
      <td align=right > 3456.15      </td>
      <td align=right > 2.1487e+14      </td>
      <td align=right > 5.4669e+15      </td>
      <td align=right > 8.60088e+11      </td>
    </tr>
    <tr> 
      <td align=right > 9.5      </td>
      <td align=right > 353084      </td>
      <td align=right > 14.5375      </td>
      <td align=right > 3830.78      </td>
      <td align=right > 3.67366e+14      </td>
      <td align=right > 1.04536e+16      </td>
      <td align=right > 1.39412e+12      </td>
    </tr>
    <tr> 
      <td align=right > 9.75      </td>
      <td align=right > 436354      </td>
      <td align=right > 15.2594      </td>
      <td align=right > 4235.42      </td>
      <td align=right > 6.20341e+14      </td>
      <td align=right > 1.96967e+16      </td>
      <td align=right > 2.23497e+12      </td>
    </tr>
    <tr> 
      <td align=right > 10      </td>
      <td align=right > 536706      </td>
      <td align=right > 16      </td>
      <td align=right > 4671.65      </td>
      <td align=right > 1.03514e+15      </td>
      <td align=right > 3.65917e+16      </td>
      <td align=right > 3.54527e+12      </td>
    </tr>
    <tr> 
      <td align=right > 20      </td>
      <td align=right > 1.81442e+8      </td>
      <td align=right > 61      </td>
      <td align=right > 71242.6      </td>
      <td align=right > 1.80415e+21      </td>
      <td align=right > 1.38026e+24      </td>
      <td align=right > 1.54476e+18      </td>
    </tr>
    <tr> 
      <td align=right > 30      </td>
      <td align=right > 5.88343e+9      </td>
      <td align=right > 136      </td>
      <td align=right > 357381      </td>
      <td align=right > 9.51588e+24      </td>
      <td align=right > 4.68439e+28      </td>
      <td align=right > 3.62123e+21      </td>
    </tr>
    <tr> 
      <td align=right > 40      </td>
      <td align=right > 7.03184e+10      </td>
      <td align=right > 241      </td>
      <td align=right > 1.12587e+6      </td>
      <td align=right > 4.28234e+27      </td>
      <td align=right > 7.98487e+31      </td>
      <td align=right > 9.16666e+23      </td>
    </tr>
    <tr> 
      <td align=right > 50      </td>
      <td align=right > 4.83578e+11      </td>
      <td align=right > 376      </td>
      <td align=right > 2.74459e+6      </td>
      <td align=right > 4.93706e+29      </td>
      <td align=right > 2.595e+34      </td>
      <td align=right > 6.7636e+25      </td>
    </tr>
  </tbody>
</table>
<BR><HR>
<ADDRESS>
genick
2007-11-14
</ADDRESS>
        </tr>
    </tbody>
    </table>
    <? include("aboutPottoProject.php");  ?>
    </div>
    </td>
    </tr>
    </tbody>
</table>
<? include("bottom.php"); ?>

