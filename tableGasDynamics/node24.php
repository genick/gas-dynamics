<? include("header.php");?>
<table style="width:100%; height:993px;" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
	<? include("left_link.php");?>
	<td style="width: 80%; vertical-align: top;">
	<div  font-family: "arial", "helvetica", "lucida", "sans";>

<!--Navigation Panel-->
<A NAME="tex2html385"
  HREF="node25.php">
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next"
 SRC="figures/next.png"></A> 
<A NAME="tex2html381"
  HREF="node18.php">
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up"
 SRC="figures/up.png"></A> 
<A NAME="tex2html375"
  HREF="node23.php">
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous"
 SRC="figures/prev.png"></A> 
<A NAME="tex2html383"
  HREF="node1.php">
<IMG WIDTH="65" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="contents"
 SRC="figures/contents.png"></A>  
<BR>
<B> Next:</B> <A NAME="tex2html386"
  HREF="node25.php">Shock Reflecting from a</A>
<B> Up:</B> <A NAME="tex2html382"
  HREF="node18.php">Normal Shock</A>
<B> Previous:</B> <A NAME="tex2html376"
  HREF="node23.php">Normal shock Standard Table</A>
 &nbsp; <B>  <A NAME="tex2html384"
  HREF="node1.php">Contents</A></B> 
<BR>
<BR>
<!--End of Navigation Panel-->

<H2><A NAME="SECTION00042000000000000000">
Open Valve maximum values</A>
</H2>

<P>
<DIV ALIGN="CENTER">
<TABLE CELLPADDING=3 BORDER="1">
<TR><TD ALIGN="CENTER" COLSPAN=1><!-- MATH
 $\rule[-.1in]{0.pt}{0.3 in}\mathbf{k}$
 -->
<IMG
 WIDTH="16" HEIGHT="56" ALIGN="MIDDLE" BORDER="0"
 SRC="img16.png"
 ALT="$ \rule[-.1in]{0.pt}{0.3 in}\mathbf{k} $">
</TD>
<TD ALIGN="CENTER" COLSPAN=1><!-- MATH
 $\mathbf{{M_x}}$
 -->
<IMG
 WIDTH="34" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="img17.png"
 ALT="$ \mathbf{{M_x}} $">
</TD>
<TD ALIGN="CENTER" COLSPAN=1><!-- MATH
 $\mathbf{M_y}$
 -->
<IMG
 WIDTH="34" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="img11.png"
 ALT="$ \mathbf{M_y} $">
</TD>
<TD ALIGN="CENTER" COLSPAN=1><!-- MATH
 $\mathbf{{M_y}^{'} }$
 -->
<IMG
 WIDTH="39" HEIGHT="46" ALIGN="MIDDLE" BORDER="0"
 SRC="img18.png"
 ALT="$ \mathbf{{M_y}^{'} } $">
</TD>
<TD ALIGN="CENTER" COLSPAN=1><!-- MATH
 $\mathbf{{T_y} \over {T_x}  }$
 -->
<IMG
 WIDTH="27" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="img19.png"
 ALT="$ \mathbf{{T_y} \over {T_x} } $">
</TD>
</TR>
<TR><TD ALIGN="RIGHT">1.30</TD>
<TD ALIGN="RIGHT">1073.25</TD>
<TD ALIGN="RIGHT">0.33968</TD>
<TD ALIGN="RIGHT">2.2645</TD>
<TD ALIGN="RIGHT">169842.29</TD>
</TR>
<TR><TD ALIGN="RIGHT">1.40</TD>
<TD ALIGN="RIGHT">985.85</TD>
<TD ALIGN="RIGHT">0.37797</TD>
<TD ALIGN="RIGHT">1.8898</TD>
<TD ALIGN="RIGHT">188982.96</TD>
</TR>
<TR><TD ALIGN="RIGHT">1.50</TD>
<TD ALIGN="RIGHT">922.23</TD>
<TD ALIGN="RIGHT">0.40825</TD>
<TD ALIGN="RIGHT">1.6330</TD>
<TD ALIGN="RIGHT">204124.86</TD>
</TR>
<TR><TD ALIGN="RIGHT">1.60</TD>
<TD ALIGN="RIGHT">873.09</TD>
<TD ALIGN="RIGHT">0.43301</TD>
<TD ALIGN="RIGHT">1.4434</TD>
<TD ALIGN="RIGHT">216507.05</TD>
</TR>
<TR><TD ALIGN="RIGHT">1.70</TD>
<TD ALIGN="RIGHT">833.61</TD>
<TD ALIGN="RIGHT">0.45374</TD>
<TD ALIGN="RIGHT">1.2964</TD>
<TD ALIGN="RIGHT">226871.99</TD>
</TR>
<TR><TD ALIGN="RIGHT">1.80</TD>
<TD ALIGN="RIGHT">801.02</TD>
<TD ALIGN="RIGHT">0.47141</TD>
<TD ALIGN="RIGHT">1.1785</TD>
<TD ALIGN="RIGHT">235702.93</TD>
</TR>
<TR><TD ALIGN="RIGHT">1.90</TD>
<TD ALIGN="RIGHT">773.54</TD>
<TD ALIGN="RIGHT">0.48667</TD>
<TD ALIGN="RIGHT">1.0815</TD>
<TD ALIGN="RIGHT">243332.79</TD>
</TR>
<TR><TD ALIGN="RIGHT">2.00</TD>
<TD ALIGN="RIGHT">750.00</TD>
<TD ALIGN="RIGHT">0.50000</TD>
<TD ALIGN="RIGHT">1.00000</TD>
<TD ALIGN="RIGHT">250000.64</TD>
</TR>
<TR><TD ALIGN="RIGHT">2.10</TD>
<TD ALIGN="RIGHT">729.56</TD>
<TD ALIGN="RIGHT">0.51177</TD>
<TD ALIGN="RIGHT">0.93048</TD>
<TD ALIGN="RIGHT">255883.78</TD>
</TR>
<TR><TD ALIGN="RIGHT">2.20</TD>
<TD ALIGN="RIGHT">711.62</TD>
<TD ALIGN="RIGHT">0.52223</TD>
<TD ALIGN="RIGHT">0.87039</TD>
<TD ALIGN="RIGHT">261117.09</TD>
</TR>
<TR><TD ALIGN="RIGHT">2.30</TD>
<TD ALIGN="RIGHT">695.74</TD>
<TD ALIGN="RIGHT">0.53161</TD>
<TD ALIGN="RIGHT">0.81786</TD>
<TD ALIGN="RIGHT">265805.36</TD>
</TR>
<TR><TD ALIGN="RIGHT">2.40</TD>
<TD ALIGN="RIGHT">681.56</TD>
<TD ALIGN="RIGHT">0.54006</TD>
<TD ALIGN="RIGHT">0.77151</TD>
<TD ALIGN="RIGHT">270031.44</TD>
</TR>
<TR><TD ALIGN="RIGHT">2.50</TD>
<TD ALIGN="RIGHT">668.81</TD>
<TD ALIGN="RIGHT">0.54772</TD>
<TD ALIGN="RIGHT">0.73029</TD>
<TD ALIGN="RIGHT">273861.85</TD>
</TR>
</TABLE>
</DIV>
<P>
<DIV ALIGN="CENTER"></DIV>
<P><P>
<BR>
<DIV ALIGN="CENTER"><I>Table of maximum values of the shock-chocking phenomenon.</I>

</DIV>

<P>
<HR>
<!--Navigation Panel-->
<A NAME="tex2html385"
  HREF="node25.php">
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next"
 SRC="figures/next.png"></A> 
<A NAME="tex2html381"
  HREF="node18.php">
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up"
 SRC="figures/up.png"></A> 
<A NAME="tex2html375"
  HREF="node23.php">
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous"
 SRC="figures/prev.png"></A> 
<A NAME="tex2html383"
  HREF="node1.php">
<IMG WIDTH="65" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="contents"
 SRC="figures/contents.png"></A>  
<BR>
<B> Next:</B> <A NAME="tex2html386"
  HREF="node25.php">Shock Reflecting from a</A>
<B> Up:</B> <A NAME="tex2html382"
  HREF="node18.php">Normal Shock</A>
<B> Previous:</B> <A NAME="tex2html376"
  HREF="node23.php">Normal shock Standard Table</A>
 &nbsp; <B>  <A NAME="tex2html384"
  HREF="node1.php">Contents</A></B> 
<!--End of Navigation Panel-->
<ADDRESS>
genick
2007-11-14
</ADDRESS>
        </tr>
    </tbody>
    </table>
    <? include("aboutPottoProject.php");  ?>
    </div>
    </td>
    </tr>
    </tbody>
</table>
<? include("bottom.php"); ?>

