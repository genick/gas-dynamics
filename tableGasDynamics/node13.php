<? include("header.php");?>
<table style="width:100%; height:993px;" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
	<? include("left_link.php");?>
	<td style="width: 80%; vertical-align: top;">
	<div  font-family: "arial", "helvetica", "lucida", "sans";>

<!--Navigation Panel-->
<A NAME="tex2html233"
  HREF="node14.php">
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next"
 SRC="figures/next.png"></A> 
<A NAME="tex2html229"
  HREF="tableGasDynamics.php">
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up"
 SRC="figures/up.png"></A> 
<A NAME="tex2html223"
  HREF="node12.php">
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous"
 SRC="figures/prev.png"></A> 
<A NAME="tex2html231"
  HREF="node1.php">
<IMG WIDTH="65" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="contents"
 SRC="figures/contents.png"></A>  
<BR>
<B> Next:</B> <A NAME="tex2html234"
  HREF="node14.php">Isothermal Nozzle for k=1.2</A>
<B> Up:</B> <A NAME="tex2html230"
  HREF="tableGasDynamics.php">tableGasDynamics</A>
<B> Previous:</B> <A NAME="tex2html224"
  HREF="node12.php">NPT for Isentropic Flow</A>
 &nbsp; <B>  <A NAME="tex2html232"
  HREF="node1.php">Contents</A></B> 
<BR>
<BR>
<!--End of Navigation Panel-->

<H1><A NAME="SECTION00030000000000000000">
Isothermal Nozzle </A>
</H1>
<BR><HR>
<!--Table of Child-Links-->
<A NAME="CHILD_LINKS"><STRONG>Subsections</STRONG></A>

<UL>
<LI><A NAME="tex2html235"
  HREF="node14.php">Isothermal Nozzle for k=1.2</A>
<LI><A NAME="tex2html236"
  HREF="node15.php">Isothermal Nozzle for k=1.3</A>
<LI><A NAME="tex2html237"
  HREF="node16.php">Isothermal Nozzle for k=1.4</A>
<LI><A NAME="tex2html238"
  HREF="node17.php">Isothermal Nozzle for k=1.67</A>
</UL>
<!--End of Table of Child-Links-->
<BR><HR>
<ADDRESS>
genick
2007-11-14
</ADDRESS>
        </tr>
    </tbody>
    </table>
    <? include("aboutPottoProject.php");  ?>
    </div>
    </td>
    </tr>
    </tbody>
</table>
<? include("bottom.php"); ?>

