<? include("header.php");?>
<table style="width:100%; height:993px;" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
	<? include("left_link.php");?>
	<td style="width: 80%; vertical-align: top;">
	<div  font-family: "arial", "helvetica", "lucida", "sans";>

<!--Navigation Panel-->
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next"
 SRC="figures/next_g.png"> 
<A NAME="tex2html561"
  HREF="tableGasDynamics.php">
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up"
 SRC="figures/up.png"></A> 
<A NAME="tex2html557"
  HREF="node38.php">
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous"
 SRC="figures/prev.png"></A> 
<A NAME="tex2html563"
  HREF="node1.php">
<IMG WIDTH="65" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="contents"
 SRC="figures/contents.png"></A>  
<BR>
<B> Up:</B> <A NAME="tex2html562"
  HREF="tableGasDynamics.php">tableGasDynamics</A>
<B> Previous:</B> <A NAME="tex2html558"
  HREF="node38.php">Prandtl-Meyer Function</A>
 &nbsp; <B>  <A NAME="tex2html564"
  HREF="node1.php">Contents</A></B> 
<BR>
<BR>
<!--End of Navigation Panel-->

<H1><A NAME="SECTION00090000000000000000">
About this document ...</A>
</H1>
 <P>
This document was generated using the
<A HREF="http://www.latex2html.org/"><STRONG>LaTeX</STRONG>2<tt>HTML</tt></A> translator Version 2002-2-1 (1.71)
<P>
Copyright &#169; 1993, 1994, 1995, 1996,
<A HREF="http://cbl.leeds.ac.uk/nikos/personal.php">Nikos Drakos</A>, 
Computer Based Learning Unit, University of Leeds.
<BR>
Copyright &#169; 1997, 1998, 1999,
<A HREF="http://www.maths.mq.edu.au/~ross/">Ross Moore</A>, 
Mathematics Department, Macquarie University, Sydney.
<P>
The command line arguments were: <BR>
 <STRONG>latex2html</STRONG> <TT>tableGasDynamics</TT>
<P>
The translation was initiated by genick on 2007-11-14
<BR><HR>
<ADDRESS>
genick
2007-11-14
</ADDRESS>
        </tr>
    </tbody>
    </table>
    <? include("aboutPottoProject.php");  ?>
    </div>
    </td>
    </tr>
    </tbody>
</table>
<? include("bottom.php"); ?>

