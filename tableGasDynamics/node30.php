<? include("header.php");?>
<table style="width:100%; height:993px;" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
	<? include("left_link.php");?>
	<td style="width: 80%; vertical-align: top;">
	<div  font-family: "arial", "helvetica", "lucida", "sans";>

<!--Navigation Panel-->
<A NAME="tex2html457"
  HREF="node31.php">
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next"
 SRC="figures/next.png"></A> 
<A NAME="tex2html453"
  HREF="node18.php">
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up"
 SRC="figures/up.png"></A> 
<A NAME="tex2html449"
  HREF="node29.php">
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous"
 SRC="figures/prev.png"></A> 
<A NAME="tex2html455"
  HREF="node1.php">
<IMG WIDTH="65" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="contents"
 SRC="figures/contents.png"></A>  
<BR>
<B> Next:</B> <A NAME="tex2html458"
  HREF="node31.php">Shock from suddenly open</A>
<B> Up:</B> <A NAME="tex2html454"
  HREF="node18.php">Normal Shock</A>
<B> Previous:</B> <A NAME="tex2html450"
  HREF="node29.php">Reflecting Shock (closed Valve)</A>
 &nbsp; <B>  <A NAME="tex2html456"
  HREF="node1.php">Contents</A></B> 
<BR>
<BR>
<!--End of Navigation Panel-->

<H2><A NAME="SECTION00044000000000000000">
Shock propagating from suddenly open valve</A>
</H2>
<BR><HR>
<!--Table of Child-Links-->
<A NAME="CHILD_LINKS"><STRONG>Subsections</STRONG></A>

<UL>
<LI><A NAME="tex2html459"
  HREF="node31.php">Shock from suddenly open valve k =1.2</A>
<LI><A NAME="tex2html460"
  HREF="node32.php">Shock from suddenly open valve k =1.3</A>
<LI><A NAME="tex2html461"
  HREF="node33.php">Shock from suddenly open valve k =1.4</A>
<LI><A NAME="tex2html462"
  HREF="node34.php">Shock from suddenly open valve k =1.67</A>
</UL>
<!--End of Table of Child-Links-->
<BR><HR>
<ADDRESS>
genick
2007-11-14
</ADDRESS>
        </tr>
    </tbody>
    </table>
    <? include("aboutPottoProject.php");  ?>
    </div>
    </td>
    </tr>
    </tbody>
</table>
<? include("bottom.php"); ?>

