<? include("header.php");?>
<table style="width:100%; height:993px;" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
	<? include("left_link.php");?>
	<td style="width: 80%; vertical-align: top;">
	<div  font-family: "arial", "helvetica", "lucida", "sans";>

<!--Navigation Panel-->
<A NAME="tex2html555"
  HREF="node39.php">
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next"
 SRC="figures/next.png"></A> 
<A NAME="tex2html551"
  HREF="tableGasDynamics.php">
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up"
 SRC="figures/up.png"></A> 
<A NAME="tex2html545"
  HREF="node37.php">
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous"
 SRC="figures/prev.png"></A> 
<A NAME="tex2html553"
  HREF="node1.php">
<IMG WIDTH="65" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="contents"
 SRC="figures/contents.png"></A>  
<BR>
<B> Next:</B> <A NAME="tex2html556"
  HREF="node39.php">About this document ...</A>
<B> Up:</B> <A NAME="tex2html552"
  HREF="tableGasDynamics.php">tableGasDynamics</A>
<B> Previous:</B> <A NAME="tex2html546"
  HREF="node37.php">Rayleigh Flow</A>
 &nbsp; <B>  <A NAME="tex2html554"
  HREF="node1.php">Contents</A></B> 
<BR>
<BR>
<!--End of Navigation Panel-->

<H1><A NAME="SECTION00080000000000000000"></A>
<A NAME="chap:Prandtl-Meyer"></A>
<BR>
Prandtl-Meyer Function
</H1>

<P>

<P>

<A NAME="351"></A>
<TABLE CELLPADDING=3 BORDER="1">
<CAPTION><STRONG>Table 30:</STRONG>
 ??  </CAPTION>
<TR><TD ALIGN="CENTER" COLSPAN=1><!-- MATH
 $\rule[-.1in]{0.pt}{0.3 in}\mathbf{M}$
 -->
<IMG
 WIDTH="25" HEIGHT="56" ALIGN="MIDDLE" BORDER="0"
 SRC="img3.png"
 ALT="$ \rule[-.1in]{0.pt}{0.3 in}\mathbf{M} $">
</TD>
<TD ALIGN="CENTER" COLSPAN=1><!-- MATH
 $\mathbf{\nu}$
 -->
<IMG
 WIDTH="15" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="img32.png"
 ALT="$ \mathbf{\nu} $">
</TD>
<TD ALIGN="CENTER" COLSPAN=1><!-- MATH
 $\mathbf{P \over P_0}$
 -->
<IMG
 WIDTH="27" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="img33.png"
 ALT="$ \mathbf{P \over P_0} $">
</TD>
<TD ALIGN="CENTER" COLSPAN=1><!-- MATH
 $\mathbf{T \over T_0}$
 -->
<IMG
 WIDTH="27" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="img34.png"
 ALT="$ \mathbf{T \over T_0} $">
</TD>
<TD ALIGN="CENTER" COLSPAN=1><!-- MATH
 $\mathbf{\rho \over \rho_0}$
 -->
<IMG
 WIDTH="23" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="img35.png"
 ALT="$ \mathbf{\rho \over \rho_0} $">
</TD>
<TD ALIGN="CENTER" COLSPAN=1><!-- MATH
 $\mathbf{\mu }$
 -->
<IMG
 WIDTH="16" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="img36.png"
 ALT="$ \mathbf{\mu } $">
</TD>
</TR>
<TR><TD ALIGN="LEFT">1.0000</TD>
<TD ALIGN="LEFT">0.0</TD>
<TD ALIGN="LEFT">0.52828</TD>
<TD ALIGN="LEFT">0.83333</TD>
<TD ALIGN="LEFT">0.63394</TD>
<TD ALIGN="LEFT">45.00</TD>
</TR>
<TR><TD ALIGN="LEFT">1.100</TD>
<TD ALIGN="LEFT">1.336</TD>
<TD ALIGN="LEFT">0.46835</TD>
<TD ALIGN="LEFT">0.80515</TD>
<TD ALIGN="LEFT">0.58170</TD>
<TD ALIGN="LEFT">47.73</TD>
</TR>
<TR><TD ALIGN="LEFT">1.200</TD>
<TD ALIGN="LEFT">3.558</TD>
<TD ALIGN="LEFT">0.41238</TD>
<TD ALIGN="LEFT">0.77640</TD>
<TD ALIGN="LEFT">0.53114</TD>
<TD ALIGN="LEFT">50.19</TD>
</TR>
<TR><TD ALIGN="LEFT">1.300</TD>
<TD ALIGN="LEFT">6.170</TD>
<TD ALIGN="LEFT">0.36091</TD>
<TD ALIGN="LEFT">0.74738</TD>
<TD ALIGN="LEFT">0.48290</TD>
<TD ALIGN="LEFT">52.43</TD>
</TR>
<TR><TD ALIGN="LEFT">1.400</TD>
<TD ALIGN="LEFT">8.987</TD>
<TD ALIGN="LEFT">0.31424</TD>
<TD ALIGN="LEFT">0.71839</TD>
<TD ALIGN="LEFT">0.43742</TD>
<TD ALIGN="LEFT">54.46</TD>
</TR>
<TR><TD ALIGN="LEFT">1.500</TD>
<TD ALIGN="LEFT">11.91</TD>
<TD ALIGN="LEFT">0.27240</TD>
<TD ALIGN="LEFT">0.68966</TD>
<TD ALIGN="LEFT">0.39498</TD>
<TD ALIGN="LEFT">56.31</TD>
</TR>
<TR><TD ALIGN="LEFT">1.600</TD>
<TD ALIGN="LEFT">14.86</TD>
<TD ALIGN="LEFT">0.23527</TD>
<TD ALIGN="LEFT">0.66138</TD>
<TD ALIGN="LEFT">0.35573</TD>
<TD ALIGN="LEFT">57.99</TD>
</TR>
<TR><TD ALIGN="LEFT">1.700</TD>
<TD ALIGN="LEFT">17.81</TD>
<TD ALIGN="LEFT">0.20259</TD>
<TD ALIGN="LEFT">0.63371</TD>
<TD ALIGN="LEFT">0.31969</TD>
<TD ALIGN="LEFT">59.53</TD>
</TR>
<TR><TD ALIGN="LEFT">1.800</TD>
<TD ALIGN="LEFT">20.73</TD>
<TD ALIGN="LEFT">0.17404</TD>
<TD ALIGN="LEFT">0.60680</TD>
<TD ALIGN="LEFT">0.28682</TD>
<TD ALIGN="LEFT">60.95</TD>
</TR>
<TR><TD ALIGN="LEFT">1.900</TD>
<TD ALIGN="LEFT">23.59</TD>
<TD ALIGN="LEFT">0.14924</TD>
<TD ALIGN="LEFT">0.58072</TD>
<TD ALIGN="LEFT">0.25699</TD>
<TD ALIGN="LEFT">62.24</TD>
</TR>
<TR><TD ALIGN="LEFT">2.000</TD>
<TD ALIGN="LEFT">26.38</TD>
<TD ALIGN="LEFT">0.12780</TD>
<TD ALIGN="LEFT">0.55556</TD>
<TD ALIGN="LEFT">0.23005</TD>
<TD ALIGN="LEFT">63.43</TD>
</TR>
<TR><TD ALIGN="LEFT">3.000</TD>
<TD ALIGN="LEFT">49.76</TD>
<TD ALIGN="LEFT">0.027224</TD>
<TD ALIGN="LEFT">0.35714</TD>
<TD ALIGN="LEFT">0.076226</TD>
<TD ALIGN="LEFT">71.57</TD>
</TR>
<TR><TD ALIGN="LEFT">4.000</TD>
<TD ALIGN="LEFT">65.78</TD>
<TD ALIGN="LEFT">0.00659</TD>
<TD ALIGN="LEFT">0.23810</TD>
<TD ALIGN="LEFT">0.027662</TD>
<TD ALIGN="LEFT">75.96</TD>
</TR>
<TR><TD ALIGN="LEFT">5.000</TD>
<TD ALIGN="LEFT">76.92</TD>
<TD ALIGN="LEFT">0.00189</TD>
<TD ALIGN="LEFT">0.16667</TD>
<TD ALIGN="LEFT">0.011340</TD>
<TD ALIGN="LEFT">78.69</TD>
</TR>
<TR><TD ALIGN="LEFT">6.000</TD>
<TD ALIGN="LEFT">84.96</TD>
<TD ALIGN="LEFT">0.000633</TD>
<TD ALIGN="LEFT">0.12195</TD>
<TD ALIGN="LEFT">0.00519</TD>
<TD ALIGN="LEFT">80.54</TD>
</TR>
<TR><TD ALIGN="LEFT">7.000</TD>
<TD ALIGN="LEFT">90.97</TD>
<TD ALIGN="LEFT">0.000242</TD>
<TD ALIGN="LEFT">0.092593</TD>
<TD ALIGN="LEFT">0.00261</TD>
<TD ALIGN="LEFT">81.87</TD>
</TR>
<TR><TD ALIGN="LEFT">8.000</TD>
<TD ALIGN="LEFT">95.62</TD>
<TD ALIGN="LEFT">0.000102</TD>
<TD ALIGN="LEFT">0.072464</TD>
<TD ALIGN="LEFT">0.00141</TD>
<TD ALIGN="LEFT">82.87</TD>
</TR>
<TR><TD ALIGN="LEFT">9.000</TD>
<TD ALIGN="LEFT">99.32</TD>
<TD ALIGN="LEFT">4.74E-5</TD>
<TD ALIGN="LEFT">0.058140</TD>
<TD ALIGN="LEFT">0.000815</TD>
<TD ALIGN="LEFT">83.66</TD>
</TR>
<TR><TD ALIGN="LEFT">:10.00</TD>
<TD ALIGN="LEFT">1.0E+2</TD>
<TD ALIGN="LEFT">2.36E-5</TD>
<TD ALIGN="LEFT">0.047619</TD>
<TD ALIGN="LEFT">0.000495</TD>
<TD ALIGN="LEFT">84.29</TD>
</TR>
</TABLE>

<P>
<HR>
<!--Navigation Panel-->
<A NAME="tex2html555"
  HREF="node39.php">
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next"
 SRC="figures/next.png"></A> 
<A NAME="tex2html551"
  HREF="tableGasDynamics.php">
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up"
 SRC="figures/up.png"></A> 
<A NAME="tex2html545"
  HREF="node37.php">
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous"
 SRC="figures/prev.png"></A> 
<A NAME="tex2html553"
  HREF="node1.php">
<IMG WIDTH="65" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="contents"
 SRC="figures/contents.png"></A>  
<BR>
<B> Next:</B> <A NAME="tex2html556"
  HREF="node39.php">About this document ...</A>
<B> Up:</B> <A NAME="tex2html552"
  HREF="tableGasDynamics.php">tableGasDynamics</A>
<B> Previous:</B> <A NAME="tex2html546"
  HREF="node37.php">Rayleigh Flow</A>
 &nbsp; <B>  <A NAME="tex2html554"
  HREF="node1.php">Contents</A></B> 
<!--End of Navigation Panel-->
<ADDRESS>
genick
2007-11-14
</ADDRESS>
        </tr>
    </tbody>
    </table>
    <? include("aboutPottoProject.php");  ?>
    </div>
    </td>
    </tr>
    </tbody>
</table>
<? include("bottom.php"); ?>

