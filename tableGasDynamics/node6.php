<? include("header.php");?>
<table style="width:100%; height:993px;" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
	<? include("left_link.php");?>
	<td style="width: 80%; vertical-align: top;">
	<div  font-family: "arial", "helvetica", "lucida", "sans";>

<!--Navigation Panel-->
<A NAME="tex2html151"
  HREF="node7.php">
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next"
 SRC="figures/next.png"></A> 
<A NAME="tex2html147"
  HREF="node3.php">
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up"
 SRC="figures/up.png"></A> 
<A NAME="tex2html141"
  HREF="node5.php">
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous"
 SRC="figures/prev.png"></A> 
<A NAME="tex2html149"
  HREF="node1.php">
<IMG WIDTH="65" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="contents"
 SRC="figures/contents.png"></A>  
<BR>
<B> Next:</B> <A NAME="tex2html152"
  HREF="node7.php">Isentropic Flow k=1.67</A>
<B> Up:</B> <A NAME="tex2html148"
  HREF="node3.php">Regular Isentropic Flow Tables</A>
<B> Previous:</B> <A NAME="tex2html142"
  HREF="node5.php">Isentropic Flow for k=1.3</A>
 &nbsp; <B>  <A NAME="tex2html150"
  HREF="node1.php">Contents</A></B> 
<BR>
<BR>
<!--End of Navigation Panel-->

<H3><A NAME="SECTION00021300000000000000">
Isentropic Flow for k =1.4</A>
</H3>

<table border=1 width="90%" >
  <thead>
    <tr>
      <th align=left bgcolor="#fffeaa" colspan=2 >Isentropic Flow </th>
      <th align=left  bgcolor="#00ff5a" colspan=3 >Input: M </th>
      <th align=left  bgcolor="#9ae0ee" colspan=1 >k = 1.4 </th>
    </tr>
    <tr>
      <th align=center >M   </th>
      <th align=center >T/T0 </th>
      <th align=center >&rho;/&rho;0 </th>
      <th align=center >A/A* </th>
      <th align=center >P/P0 </th>
      <th align=center >PAR </th>
      <th align=center >F/F* </th>
    </tr> 
  </thead>
  <tbody>
    <tr> 
      <td align=right > 1e-6      </td>
      <td align=right > 1      </td>
      <td align=right > 1      </td>
      <td align=right > 578704      </td>
      <td align=right > 1      </td>
      <td align=right > 578704      </td>
      <td align=right > 241127      </td>
    </tr>
    <tr> 
      <td align=right > 0.050      </td>
      <td align=right > 0.9995      </td>
      <td align=right > 0.998751      </td>
      <td align=right > 11.5913      </td>
      <td align=right > 0.998252      </td>
      <td align=right > 11.5711      </td>
      <td align=right > 4.83815      </td>
    </tr>
    <tr> 
      <td align=right > 0.1      </td>
      <td align=right > 0.998004      </td>
      <td align=right > 0.995017      </td>
      <td align=right > 5.82183      </td>
      <td align=right > 0.993031      </td>
      <td align=right > 5.78126      </td>
      <td align=right > 2.44258      </td>
    </tr>
    <tr> 
      <td align=right > 0.2      </td>
      <td align=right > 0.992063      </td>
      <td align=right > 0.980277      </td>
      <td align=right > 2.96352      </td>
      <td align=right > 0.972497      </td>
      <td align=right > 2.88201      </td>
      <td align=right > 1.26809      </td>
    </tr>
    <tr> 
      <td align=right > 0.25      </td>
      <td align=right > 0.987654      </td>
      <td align=right > 0.969421      </td>
      <td align=right > 2.40271      </td>
      <td align=right > 0.957453      </td>
      <td align=right > 2.30048      </td>
      <td align=right > 1.04241      </td>
    </tr>
    <tr> 
      <td align=right > 0.3      </td>
      <td align=right > 0.982318      </td>
      <td align=right > 0.95638      </td>
      <td align=right > 2.03507      </td>
      <td align=right > 0.93947      </td>
      <td align=right > 1.91188      </td>
      <td align=right > 0.896991      </td>
    </tr>
    <tr> 
      <td align=right > 0.35      </td>
      <td align=right > 0.976086      </td>
      <td align=right > 0.941283      </td>
      <td align=right > 1.77797      </td>
      <td align=right > 0.918773      </td>
      <td align=right > 1.63355      </td>
      <td align=right > 0.797376      </td>
    </tr>
    <tr> 
      <td align=right > 0.4      </td>
      <td align=right > 0.968992      </td>
      <td align=right > 0.924274      </td>
      <td align=right > 1.59014      </td>
      <td align=right > 0.895614      </td>
      <td align=right > 1.42415      </td>
      <td align=right > 0.726318      </td>
    </tr>
    <tr> 
      <td align=right > 0.45      </td>
      <td align=right > 0.961076      </td>
      <td align=right > 0.905513      </td>
      <td align=right > 1.44867      </td>
      <td align=right > 0.870267      </td>
      <td align=right > 1.26073      </td>
      <td align=right > 0.674229      </td>
    </tr>
    <tr> 
      <td align=right > 0.5      </td>
      <td align=right > 0.952381      </td>
      <td align=right > 0.88517      </td>
      <td align=right > 1.33984      </td>
      <td align=right > 0.843019      </td>
      <td align=right > 1.12951      </td>
      <td align=right > 0.635352      </td>
    </tr>
    <tr> 
      <td align=right > 0.55      </td>
      <td align=right > 0.942951      </td>
      <td align=right > 0.863422      </td>
      <td align=right > 1.25495      </td>
      <td align=right > 0.814165      </td>
      <td align=right > 1.02173      </td>
      <td align=right > 0.606017      </td>
    </tr>
    <tr> 
      <td align=right > 0.6      </td>
      <td align=right > 0.932836      </td>
      <td align=right > 0.840452      </td>
      <td align=right > 1.1882      </td>
      <td align=right > 0.784004      </td>
      <td align=right > 0.931553      </td>
      <td align=right > 0.583773      </td>
    </tr>
    <tr> 
      <td align=right > 0.65      </td>
      <td align=right > 0.922084      </td>
      <td align=right > 0.816443      </td>
      <td align=right > 1.13562      </td>
      <td align=right > 0.752829      </td>
      <td align=right > 0.854925      </td>
      <td align=right > 0.566922      </td>
    </tr>
    <tr> 
      <td align=right > 0.7      </td>
      <td align=right > 0.910747      </td>
      <td align=right > 0.791579      </td>
      <td align=right > 1.09437      </td>
      <td align=right > 0.720928      </td>
      <td align=right > 0.788964      </td>
      <td align=right > 0.554247      </td>
    </tr>
    <tr> 
      <td align=right > 0.75      </td>
      <td align=right > 0.898876      </td>
      <td align=right > 0.766037      </td>
      <td align=right > 1.06242      </td>
      <td align=right > 0.688573      </td>
      <td align=right > 0.731552      </td>
      <td align=right > 0.544854      </td>
    </tr>
    <tr> 
      <td align=right > 0.8      </td>
      <td align=right > 0.886525      </td>
      <td align=right > 0.739992      </td>
      <td align=right > 1.03823      </td>
      <td align=right > 0.656022      </td>
      <td align=right > 0.681101      </td>
      <td align=right > 0.53807      </td>
    </tr>
    <tr> 
      <td align=right > 0.85      </td>
      <td align=right > 0.873744      </td>
      <td align=right > 0.713609      </td>
      <td align=right > 1.02067      </td>
      <td align=right > 0.623512      </td>
      <td align=right > 0.636399      </td>
      <td align=right > 0.533382      </td>
    </tr>
    <tr> 
      <td align=right > 0.9      </td>
      <td align=right > 0.860585      </td>
      <td align=right > 0.687044      </td>
      <td align=right > 1.00886      </td>
      <td align=right > 0.59126      </td>
      <td align=right > 0.5965      </td>
      <td align=right > 0.530388      </td>
    </tr>
    <tr> 
      <td align=right > 0.95      </td>
      <td align=right > 0.847099      </td>
      <td align=right > 0.660443      </td>
      <td align=right > 1.00215      </td>
      <td align=right > 0.55946      </td>
      <td align=right > 0.56066      </td>
      <td align=right > 0.528773      </td>
    </tr>
    <tr> 
      <td align=right > 1      </td>
      <td align=right > 0.833333      </td>
      <td align=right > 0.633938      </td>
      <td align=right > 1      </td>
      <td align=right > 0.528282      </td>
      <td align=right > 0.528282      </td>
      <td align=right > 0.528282      </td>
    </tr>
    <tr> 
      <td align=right > 1.05      </td>
      <td align=right > 0.819336      </td>
      <td align=right > 0.607653      </td>
      <td align=right > 1.00203      </td>
      <td align=right > 0.497872      </td>
      <td align=right > 0.498882      </td>
      <td align=right > 0.528711      </td>
    </tr>
    <tr> 
      <td align=right > 1.1      </td>
      <td align=right > 0.805153      </td>
      <td align=right > 0.581696      </td>
      <td align=right > 1.00793      </td>
      <td align=right > 0.468354      </td>
      <td align=right > 0.472066      </td>
      <td align=right > 0.529894      </td>
    </tr>
    <tr> 
      <td align=right > 1.15      </td>
      <td align=right > 0.790826      </td>
      <td align=right > 0.556164      </td>
      <td align=right > 1.01745      </td>
      <td align=right > 0.439829      </td>
      <td align=right > 0.447506      </td>
      <td align=right > 0.531693      </td>
    </tr>
    <tr> 
      <td align=right > 1.2      </td>
      <td align=right > 0.776398      </td>
      <td align=right > 0.531142      </td>
      <td align=right > 1.03044      </td>
      <td align=right > 0.412377      </td>
      <td align=right > 0.42493      </td>
      <td align=right > 0.533995      </td>
    </tr>
    <tr> 
      <td align=right > 1.25      </td>
      <td align=right > 0.761905      </td>
      <td align=right > 0.506701      </td>
      <td align=right > 1.04675      </td>
      <td align=right > 0.386058      </td>
      <td align=right > 0.404107      </td>
      <td align=right > 0.536705      </td>
    </tr>
    <tr> 
      <td align=right > 1.3      </td>
      <td align=right > 0.747384      </td>
      <td align=right > 0.482903      </td>
      <td align=right > 1.0663      </td>
      <td align=right > 0.360914      </td>
      <td align=right > 0.384844      </td>
      <td align=right > 0.539744      </td>
    </tr>
    <tr> 
      <td align=right > 1.35      </td>
      <td align=right > 0.732869      </td>
      <td align=right > 0.459797      </td>
      <td align=right > 1.08904      </td>
      <td align=right > 0.336971      </td>
      <td align=right > 0.366974      </td>
      <td align=right > 0.543046      </td>
    </tr>
    <tr> 
      <td align=right > 1.4      </td>
      <td align=right > 0.718391      </td>
      <td align=right > 0.437423      </td>
      <td align=right > 1.11493      </td>
      <td align=right > 0.314241      </td>
      <td align=right > 0.350355      </td>
      <td align=right > 0.546554      </td>
    </tr>
    <tr> 
      <td align=right > 1.45      </td>
      <td align=right > 0.703977      </td>
      <td align=right > 0.415812      </td>
      <td align=right > 1.14396      </td>
      <td align=right > 0.292722      </td>
      <td align=right > 0.334863      </td>
      <td align=right > 0.550222      </td>
    </tr>
    <tr> 
      <td align=right > 1.5      </td>
      <td align=right > 0.689655      </td>
      <td align=right > 0.394984      </td>
      <td align=right > 1.17617      </td>
      <td align=right > 0.272403      </td>
      <td align=right > 0.320392      </td>
      <td align=right > 0.55401      </td>
    </tr>
    <tr> 
      <td align=right > 1.55      </td>
      <td align=right > 0.675447      </td>
      <td align=right > 0.374955      </td>
      <td align=right > 1.21157      </td>
      <td align=right > 0.253262      </td>
      <td align=right > 0.306846      </td>
      <td align=right > 0.557884      </td>
    </tr>
    <tr> 
      <td align=right > 1.6      </td>
      <td align=right > 0.661376      </td>
      <td align=right > 0.35573      </td>
      <td align=right > 1.25024      </td>
      <td align=right > 0.235271      </td>
      <td align=right > 0.294144      </td>
      <td align=right > 0.561816      </td>
    </tr>
    <tr> 
      <td align=right > 1.65      </td>
      <td align=right > 0.647459      </td>
      <td align=right > 0.337311      </td>
      <td align=right > 1.29222      </td>
      <td align=right > 0.218395      </td>
      <td align=right > 0.282214      </td>
      <td align=right > 0.56578      </td>
    </tr>
    <tr> 
      <td align=right > 1.7      </td>
      <td align=right > 0.633714      </td>
      <td align=right > 0.319693      </td>
      <td align=right > 1.33761      </td>
      <td align=right > 0.202593      </td>
      <td align=right > 0.27099      </td>
      <td align=right > 0.569757      </td>
    </tr>
    <tr> 
      <td align=right > 1.75      </td>
      <td align=right > 0.620155      </td>
      <td align=right > 0.302866      </td>
      <td align=right > 1.38649      </td>
      <td align=right > 0.187824      </td>
      <td align=right > 0.260416      </td>
      <td align=right > 0.57373      </td>
    </tr>
    <tr> 
      <td align=right > 1.8      </td>
      <td align=right > 0.606796      </td>
      <td align=right > 0.286818      </td>
      <td align=right > 1.43898      </td>
      <td align=right > 0.17404      </td>
      <td align=right > 0.250441      </td>
      <td align=right > 0.577684      </td>
    </tr>
    <tr> 
      <td align=right > 1.85      </td>
      <td align=right > 0.593648      </td>
      <td align=right > 0.271533      </td>
      <td align=right > 1.49519      </td>
      <td align=right > 0.161195      </td>
      <td align=right > 0.241018      </td>
      <td align=right > 0.581606      </td>
    </tr>
    <tr> 
      <td align=right > 1.9      </td>
      <td align=right > 0.58072      </td>
      <td align=right > 0.256991      </td>
      <td align=right > 1.55526      </td>
      <td align=right > 0.14924      </td>
      <td align=right > 0.232106      </td>
      <td align=right > 0.585487      </td>
    </tr>
    <tr> 
      <td align=right > 1.95      </td>
      <td align=right > 0.56802      </td>
      <td align=right > 0.24317      </td>
      <td align=right > 1.61931      </td>
      <td align=right > 0.138126      </td>
      <td align=right > 0.223668      </td>
      <td align=right > 0.589318      </td>
    </tr>
    <tr> 
      <td align=right > 2      </td>
      <td align=right > 0.555556      </td>
      <td align=right > 0.230048      </td>
      <td align=right > 1.6875      </td>
      <td align=right > 0.127805      </td>
      <td align=right > 0.21567      </td>
      <td align=right > 0.593093      </td>
    </tr>
    <tr> 
      <td align=right > 2.25      </td>
      <td align=right > 0.496894      </td>
      <td align=right > 0.174044      </td>
      <td align=right > 2.09644      </td>
      <td align=right > 0.0864817      </td>
      <td align=right > 0.181303      </td>
      <td align=right > 0.610954      </td>
    </tr>
    <tr> 
      <td align=right > 2.5      </td>
      <td align=right > 0.444444      </td>
      <td align=right > 0.131687      </td>
      <td align=right > 2.63672      </td>
      <td align=right > 0.0585277      </td>
      <td align=right > 0.154321      </td>
      <td align=right > 0.626929      </td>
    </tr>
    <tr> 
      <td align=right > 2.75      </td>
      <td align=right > 0.39801      </td>
      <td align=right > 0.099939      </td>
      <td align=right > 3.33766      </td>
      <td align=right > 0.0397767      </td>
      <td align=right > 0.132761      </td>
      <td align=right > 0.640987      </td>
    </tr>
    <tr> 
      <td align=right > 3      </td>
      <td align=right > 0.357143      </td>
      <td align=right > 0.0762263      </td>
      <td align=right > 4.23457      </td>
      <td align=right > 0.0272237      </td>
      <td align=right > 0.115281      </td>
      <td align=right > 0.653256      </td>
    </tr>
    <tr> 
      <td align=right > 3.25      </td>
      <td align=right > 0.321285      </td>
      <td align=right > 0.0585095      </td>
      <td align=right > 5.36909      </td>
      <td align=right > 0.0187982      </td>
      <td align=right > 0.10093      </td>
      <td align=right > 0.663927      </td>
    </tr>
    <tr> 
      <td align=right > 3.5      </td>
      <td align=right > 0.289855      </td>
      <td align=right > 0.0452327      </td>
      <td align=right > 6.78962      </td>
      <td align=right > 0.0131109      </td>
      <td align=right > 0.0890182      </td>
      <td align=right > 0.6732      </td>
    </tr>
    <tr> 
      <td align=right > 3.75      </td>
      <td align=right > 0.262295      </td>
      <td align=right > 0.0352351      </td>
      <td align=right > 8.55174      </td>
      <td align=right > 0.00924199      </td>
      <td align=right > 0.0790351      </td>
      <td align=right > 0.681266      </td>
    </tr>
    <tr> 
      <td align=right > 4      </td>
      <td align=right > 0.238095      </td>
      <td align=right > 0.0276616      </td>
      <td align=right > 10.7188      </td>
      <td align=right > 0.00658609      </td>
      <td align=right > 0.0705946      </td>
      <td align=right > 0.688298      </td>
    </tr>
    <tr> 
      <td align=right > 4.25      </td>
      <td align=right > 0.216802      </td>
      <td align=right > 0.0218856      </td>
      <td align=right > 13.3622      </td>
      <td align=right > 0.00474485      </td>
      <td align=right > 0.0634014      </td>
      <td align=right > 0.694444      </td>
    </tr>
    <tr> 
      <td align=right > 4.5      </td>
      <td align=right > 0.19802      </td>
      <td align=right > 0.017449      </td>
      <td align=right > 16.5622      </td>
      <td align=right > 0.00345526      </td>
      <td align=right > 0.0572266      </td>
      <td align=right > 0.699834      </td>
    </tr>
    <tr> 
      <td align=right > 4.75      </td>
      <td align=right > 0.181406      </td>
      <td align=right > 0.0140161      </td>
      <td align=right > 20.4084      </td>
      <td align=right > 0.00254261      </td>
      <td align=right > 0.0518906      </td>
      <td align=right > 0.704576      </td>
    </tr>
    <tr> 
      <td align=right > 5      </td>
      <td align=right > 0.166667      </td>
      <td align=right > 0.0113402      </td>
      <td align=right > 25      </td>
      <td align=right > 0.00189004      </td>
      <td align=right > 0.047251      </td>
      <td align=right > 0.708764      </td>
    </tr>
    <tr> 
      <td align=right > 5.25      </td>
      <td align=right > 0.153551      </td>
      <td align=right > 0.00923912      </td>
      <td align=right > 30.4467      </td>
      <td align=right > 0.00141868      </td>
      <td align=right > 0.043194      </td>
      <td align=right > 0.712475      </td>
    </tr>
    <tr> 
      <td align=right > 5.5      </td>
      <td align=right > 0.141844      </td>
      <td align=right > 0.00757752      </td>
      <td align=right > 36.869      </td>
      <td align=right > 0.00107483      </td>
      <td align=right > 0.0396277      </td>
      <td align=right > 0.715776      </td>
    </tr>
    <tr> 
      <td align=right > 5.75      </td>
      <td align=right > 0.131363      </td>
      <td align=right > 0.00625434      </td>
      <td align=right > 44.3987      </td>
      <td align=right > 0.000821589      </td>
      <td align=right > 0.0364775      </td>
      <td align=right > 0.71872      </td>
    </tr>
    <tr> 
      <td align=right > 6      </td>
      <td align=right > 0.121951      </td>
      <td align=right > 0.00519356      </td>
      <td align=right > 53.1798      </td>
      <td align=right > 0.000633361      </td>
      <td align=right > 0.033682      </td>
      <td align=right > 0.721357      </td>
    </tr>
    <tr> 
      <td align=right > 6.25      </td>
      <td align=right > 0.113475      </td>
      <td align=right > 0.00433763      </td>
      <td align=right > 63.3685      </td>
      <td align=right > 0.000492213      </td>
      <td align=right > 0.0311908      </td>
      <td align=right > 0.723724      </td>
    </tr>
    <tr> 
      <td align=right > 6.5      </td>
      <td align=right > 0.10582      </td>
      <td align=right > 0.00364268      </td>
      <td align=right > 75.1343      </td>
      <td align=right > 0.000385468      </td>
      <td align=right > 0.0289619      </td>
      <td align=right > 0.725858      </td>
    </tr>
    <tr> 
      <td align=right > 6.75      </td>
      <td align=right > 0.0988875      </td>
      <td align=right > 0.00307506      </td>
      <td align=right > 88.6601      </td>
      <td align=right > 0.000304085      </td>
      <td align=right > 0.0269602      </td>
      <td align=right > 0.727785      </td>
    </tr>
    <tr> 
      <td align=right > 7      </td>
      <td align=right > 0.0925926      </td>
      <td align=right > 0.0026088      </td>
      <td align=right > 104.143      </td>
      <td align=right > 0.000241555      </td>
      <td align=right > 0.0251563      </td>
      <td align=right > 0.729532      </td>
    </tr>
    <tr> 
      <td align=right > 7.25      </td>
      <td align=right > 0.0868621      </td>
      <td align=right > 0.0022237      </td>
      <td align=right > 121.794      </td>
      <td align=right > 0.000193155      </td>
      <td align=right > 0.0235252      </td>
      <td align=right > 0.731119      </td>
    </tr>
    <tr> 
      <td align=right > 7.5      </td>
      <td align=right > 0.0816327      </td>
      <td align=right > 0.00190397      </td>
      <td align=right > 141.841      </td>
      <td align=right > 0.000155426      </td>
      <td align=right > 0.0220459      </td>
      <td align=right > 0.732565      </td>
    </tr>
    <tr> 
      <td align=right > 7.75      </td>
      <td align=right > 0.0768492      </td>
      <td align=right > 0.00163719      </td>
      <td align=right > 164.527      </td>
      <td align=right > 0.000125816      </td>
      <td align=right > 0.0207002      </td>
      <td align=right > 0.733886      </td>
    </tr>
    <tr> 
      <td align=right > 8      </td>
      <td align=right > 0.0724638      </td>
      <td align=right > 0.00141352      </td>
      <td align=right > 190.109      </td>
      <td align=right > 0.000102429      </td>
      <td align=right > 0.0194727      </td>
      <td align=right > 0.735095      </td>
    </tr>
    <tr> 
      <td align=right > 8.25      </td>
      <td align=right > 0.0684346      </td>
      <td align=right > 0.00122515      </td>
      <td align=right > 218.865      </td>
      <td align=right > 8.38425e-5      </td>
      <td align=right > 0.0183502      </td>
      <td align=right > 0.736205      </td>
    </tr>
    <tr> 
      <td align=right > 8.5      </td>
      <td align=right > 0.0647249      </td>
      <td align=right > 0.00106581      </td>
      <td align=right > 251.086      </td>
      <td align=right > 6.89843e-5      </td>
      <td align=right > 0.017321      </td>
      <td align=right > 0.737225      </td>
    </tr>
    <tr> 
      <td align=right > 8.75      </td>
      <td align=right > 0.0613027      </td>
      <td align=right > 0.000930462      </td>
      <td align=right > 287.085      </td>
      <td align=right > 5.70398e-5      </td>
      <td align=right > 0.0163753      </td>
      <td align=right > 0.738166      </td>
    </tr>
    <tr> 
      <td align=right > 9      </td>
      <td align=right > 0.0581395      </td>
      <td align=right > 0.00081504      </td>
      <td align=right > 327.189      </td>
      <td align=right > 4.7386e-5      </td>
      <td align=right > 0.0155042      </td>
      <td align=right > 0.739034      </td>
    </tr>
    <tr> 
      <td align=right > 9.25      </td>
      <td align=right > 0.0552105      </td>
      <td align=right > 0.000716232      </td>
      <td align=right > 371.749      </td>
      <td align=right > 3.95435e-5      </td>
      <td align=right > 0.0147003      </td>
      <td align=right > 0.739837      </td>
    </tr>
    <tr> 
      <td align=right > 9.5      </td>
      <td align=right > 0.0524934      </td>
      <td align=right > 0.000631339      </td>
      <td align=right > 421.131      </td>
      <td align=right > 3.31411e-5      </td>
      <td align=right > 0.0139568      </td>
      <td align=right > 0.740582      </td>
    </tr>
    <tr> 
      <td align=right > 9.75      </td>
      <td align=right > 0.0499688      </td>
      <td align=right > 0.000558144      </td>
      <td align=right > 475.725      </td>
      <td align=right > 2.78898e-5      </td>
      <td align=right > 0.0132679      </td>
      <td align=right > 0.741273      </td>
    </tr>
    <tr> 
      <td align=right > 10      </td>
      <td align=right > 0.047619      </td>
      <td align=right > 0.000494825      </td>
      <td align=right > 535.938      </td>
      <td align=right > 2.35631e-5      </td>
      <td align=right > 0.0126284      </td>
      <td align=right > 0.741916      </td>
    </tr>
    <tr> 
      <td align=right > 20      </td>
      <td align=right > 0.0123457      </td>
      <td align=right > 1.69351e-5      </td>
      <td align=right > 15377.3      </td>
      <td align=right > 2.09075e-7      </td>
      <td align=right > 0.00321502      </td>
      <td align=right > 0.751511      </td>
    </tr>
    <tr> 
      <td align=right > 30      </td>
      <td align=right > 0.00552486      </td>
      <td align=right > 2.26884e-6      </td>
      <td align=right > 114385      </td>
      <td align=right > 1.2535e-8      </td>
      <td align=right > 0.00143382      </td>
      <td align=right > 0.753355      </td>
    </tr>
    <tr> 
      <td align=right > 40      </td>
      <td align=right > 0.00311526      </td>
      <td align=right > 5.41673e-7      </td>
      <td align=right > 478532      </td>
      <td align=right > 1.68746e-9      </td>
      <td align=right > 0.000807502      </td>
      <td align=right > 0.754005      </td>
    </tr>
    <tr> 
      <td align=right > 50      </td>
      <td align=right > 0.00199601      </td>
      <td align=right > 1.77994e-7      </td>
      <td align=right > 1.45546e+6      </td>
      <td align=right > 3.55278e-10      </td>
      <td align=right > 0.000517091      </td>
      <td align=right > 0.754307      </td>
    </tr>
  </tbody>
</table>
<BR><HR>
<ADDRESS>
genick
2007-11-14
</ADDRESS>
        </tr>
    </tbody>
    </table>
    <? include("aboutPottoProject.php");  ?>
    </div>
    </td>
    </tr>
    </tbody>
</table>
<? include("bottom.php"); ?>

