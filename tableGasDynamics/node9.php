<? include("header.php");?>
<table style="width:100%; height:993px;" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
	<? include("left_link.php");?>
	<td style="width: 80%; vertical-align: top;">
	<div  font-family: "arial", "helvetica", "lucida", "sans";>

<!--Navigation Panel-->
<A NAME="tex2html187"
  HREF="node10.php">
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next"
 SRC="figures/next.png"></A> 
<A NAME="tex2html183"
  HREF="node8.php">
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up"
 SRC="figures/up.png"></A> 
<A NAME="tex2html177"
  HREF="node8.php">
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous"
 SRC="figures/prev.png"></A> 
<A NAME="tex2html185"
  HREF="node1.php">
<IMG WIDTH="65" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="contents"
 SRC="figures/contents.png"></A>  
<BR>
<B> Next:</B> <A NAME="tex2html188"
  HREF="node10.php">NPT for Isentropic Flow</A>
<B> Up:</B> <A NAME="tex2html184"
  HREF="node8.php">``Naughty Professor'' Tables (NPT)</A>
<B> Previous:</B> <A NAME="tex2html178"
  HREF="node8.php">``Naughty Professor'' Tables (NPT)</A>
 &nbsp; <B>  <A NAME="tex2html186"
  HREF="node1.php">Contents</A></B> 
<BR>
<BR>
<!--End of Navigation Panel-->

<H3><A NAME="SECTION00022100000000000000">
NPT for Isnetropic Flow k=1.2</A>
</H3>

<P>

<table border=1 width="100%" >
  <thead>
    <tr>
      <th align=left bgcolor="#fffeaa" colspan=2 >Isentropic Flow </th>
      <th align=left  bgcolor="#00ff5a" colspan=3 >Input: M </th>
      <th align=left  bgcolor="#9ae0ee" colspan=1 >k = 1.2 </th>
    </tr>
    <tr>
      <th align=center >M </th>
      <th align=center >Fn </th>
      <th align=center >&rho;RT<sub>0</sub>/P </th>
      <th align=center >(P<sub>0</sub>A<sup>*</sup>/PA)^2 </th>
      <th align=center >RT<sub>0</sub>/P^2 mA^2 </th>
      <th align=center >1/R&rho;<sub>0</sub>P mA^2 </th>
      <th align=center >1/R&rho;<sub>0</sub>^2T mA^2 </th>
    </tr> 
  </thead>
  <tbody>
    <tr> 
      <td align=right > 1e-6      </td>
      <td align=right > 1.2e-6      </td>
      <td align=right > 1      </td>
      <td align=right > 2.85312e-12      </td>
      <td align=right > 3.42374e-24      </td>
      <td align=right > 1.2e-12      </td>
      <td align=right > 1.2e-12      </td>
    </tr>
    <tr> 
      <td align=right > 0.0500005      </td>
      <td align=right > 0.0600831      </td>
      <td align=right > 1.00025      </td>
      <td align=right > 0.00713472      </td>
      <td align=right > 2.14635e-5      </td>
      <td align=right > 0.00301284      </td>
      <td align=right > 0.00300907      </td>
    </tr>
    <tr> 
      <td align=right > 0.1      </td>
      <td align=right > 0.120661      </td>
      <td align=right > 1.001      </td>
      <td align=right > 0.0285597      </td>
      <td align=right > 0.000346505      </td>
      <td align=right > 0.0122056      </td>
      <td align=right > 0.0121448      </td>
    </tr>
    <tr> 
      <td align=right > 0.2      </td>
      <td align=right > 0.245328      </td>
      <td align=right > 1.004      </td>
      <td align=right > 0.114581      </td>
      <td align=right > 0.00574679      </td>
      <td align=right > 0.0513706      </td>
      <td align=right > 0.0503554      </td>
    </tr>
    <tr> 
      <td align=right > 0.25      </td>
      <td align=right > 0.310459      </td>
      <td align=right > 1.00625      </td>
      <td align=right > 0.179434      </td>
      <td align=right > 0.0144122      </td>
      <td align=right > 0.0833799      </td>
      <td align=right > 0.0808224      </td>
    </tr>
    <tr> 
      <td align=right > 0.3      </td>
      <td align=right > 0.378185      </td>
      <td align=right > 1.009      </td>
      <td align=right > 0.259092      </td>
      <td align=right > 0.0308802      </td>
      <td align=right > 0.125769      </td>
      <td align=right > 0.120259      </td>
    </tr>
    <tr> 
      <td align=right > 0.35      </td>
      <td align=right > 0.449089      </td>
      <td align=right > 1.01225      </td>
      <td align=right > 0.353788      </td>
      <td align=right > 0.0594602      </td>
      <td align=right > 0.180805      </td>
      <td align=right > 0.170126      </td>
    </tr>
    <tr> 
      <td align=right > 0.4      </td>
      <td align=right > 0.523789      </td>
      <td align=right > 1.016      </td>
      <td align=right > 0.463803      </td>
      <td align=right > 0.106039      </td>
      <td align=right > 0.251475      </td>
      <td align=right > 0.232287      </td>
    </tr>
    <tr> 
      <td align=right > 0.45      </td>
      <td align=right > 0.602948      </td>
      <td align=right > 1.02025      </td>
      <td align=right > 0.589456      </td>
      <td align=right > 0.178579      </td>
      <td align=right > 0.341679      </td>
      <td align=right > 0.30909      </td>
    </tr>
    <tr> 
      <td align=right > 0.5      </td>
      <td align=right > 0.687278      </td>
      <td align=right > 1.025      </td>
      <td align=right > 0.731111      </td>
      <td align=right > 0.287784      </td>
      <td align=right > 0.456485      </td>
      <td align=right > 0.403467      </td>
    </tr>
    <tr> 
      <td align=right > 0.55      </td>
      <td align=right > 0.77755      </td>
      <td align=right > 1.03025      </td>
      <td align=right > 0.889176      </td>
      <td align=right > 0.447985      </td>
      <td align=right > 0.602464      </td>
      <td align=right > 0.519061      </td>
    </tr>
    <tr> 
      <td align=right > 0.6      </td>
      <td align=right > 0.874603      </td>
      <td align=right > 1.036      </td>
      <td align=right > 1.0641      </td>
      <td align=right > 0.678302      </td>
      <td align=right > 0.788133      </td>
      <td align=right > 0.66039      </td>
    </tr>
    <tr> 
      <td align=right > 0.65      </td>
      <td align=right > 0.979355      </td>
      <td align=right > 1.04225      </td>
      <td align=right > 1.25637      </td>
      <td align=right > 1.00419      </td>
      <td align=right > 1.02454      </td>
      <td align=right > 0.833049      </td>
    </tr>
    <tr> 
      <td align=right > 0.7      </td>
      <td align=right > 1.09281      </td>
      <td align=right > 1.049      </td>
      <td align=right > 1.46653      </td>
      <td align=right > 1.45948      </td>
      <td align=right > 1.32605      </td>
      <td align=right > 1.04396      </td>
    </tr>
    <tr> 
      <td align=right > 0.75      </td>
      <td align=right > 1.21607      </td>
      <td align=right > 1.05625      </td>
      <td align=right > 1.69515      </td>
      <td align=right > 2.08904      </td>
      <td align=right > 1.71135      </td>
      <td align=right > 1.30168      </td>
    </tr>
    <tr> 
      <td align=right > 0.8      </td>
      <td align=right > 1.35036      </td>
      <td align=right > 1.064      </td>
      <td align=right > 1.94286      </td>
      <td align=right > 2.9523      </td>
      <td align=right > 2.2048      </td>
      <td align=right > 1.61682      </td>
    </tr>
    <tr> 
      <td align=right > 0.85      </td>
      <td align=right > 1.49702      </td>
      <td align=right > 1.07225      </td>
      <td align=right > 2.21031      </td>
      <td align=right > 4.1279      </td>
      <td align=right > 2.83826      </td>
      <td align=right > 2.0025      </td>
    </tr>
    <tr> 
      <td align=right > 0.9      </td>
      <td align=right > 1.65754      </td>
      <td align=right > 1.081      </td>
      <td align=right > 2.49822      </td>
      <td align=right > 5.71978      </td>
      <td align=right > 3.65345      </td>
      <td align=right > 2.475      </td>
    </tr>
    <tr> 
      <td align=right > 0.95      </td>
      <td align=right > 1.83357      </td>
      <td align=right > 1.09025      </td>
      <td align=right > 2.80733      </td>
      <td align=right > 7.86517      </td>
      <td align=right > 4.70513      </td>
      <td align=right > 3.05451      </td>
    </tr>
    <tr> 
      <td align=right > 1      </td>
      <td align=right > 2.02694      </td>
      <td align=right > 1.1      </td>
      <td align=right > 3.13843      </td>
      <td align=right > 10.7452      </td>
      <td align=right > 6.06536      </td>
      <td align=right > 3.76611      </td>
    </tr>
    <tr> 
      <td align=right > 1.05      </td>
      <td align=right > 2.23967      </td>
      <td align=right > 1.11025      </td>
      <td align=right > 3.49236      </td>
      <td align=right > 14.5985      </td>
      <td align=right > 7.82912      </td>
      <td align=right > 4.64097      </td>
    </tr>
    <tr> 
      <td align=right > 1.1      </td>
      <td align=right > 2.47403      </td>
      <td align=right > 1.121      </td>
      <td align=right > 3.87      </td>
      <td align=right > 19.7396      </td>
      <td align=right > 10.1219      </td>
      <td align=right > 5.71787      </td>
    </tr>
    <tr> 
      <td align=right > 1.15      </td>
      <td align=right > 2.73251      </td>
      <td align=right > 1.13225      </td>
      <td align=right > 4.27226      </td>
      <td align=right > 26.5828      </td>
      <td align=right > 13.1098      </td>
      <td align=right > 7.04506      </td>
    </tr>
    <tr> 
      <td align=right > 1.2      </td>
      <td align=right > 3.01791      </td>
      <td align=right > 1.144      </td>
      <td align=right > 4.70011      </td>
      <td align=right > 35.6729      </td>
      <td align=right > 17.0132      </td>
      <td align=right > 8.68272      </td>
    </tr>
    <tr> 
      <td align=right > 1.25      </td>
      <td align=right > 3.33331      </td>
      <td align=right > 1.15625      </td>
      <td align=right > 5.15456      </td>
      <td align=right > 47.7267      </td>
      <td align=right > 22.1249      </td>
      <td align=right > 10.7059      </td>
    </tr>
    <tr> 
      <td align=right > 1.3      </td>
      <td align=right > 3.68217      </td>
      <td align=right > 1.169      </td>
      <td align=right > 5.63665      </td>
      <td align=right > 63.6866      </td>
      <td align=right > 28.8346      </td>
      <td align=right > 13.2082      </td>
    </tr>
    <tr> 
      <td align=right > 1.35      </td>
      <td align=right > 4.06833      </td>
      <td align=right > 1.18225      </td>
      <td align=right > 6.14747      </td>
      <td align=right > 84.7906      </td>
      <td align=right > 37.6623      </td>
      <td align=right > 16.3065      </td>
    </tr>
    <tr> 
      <td align=right > 1.4      </td>
      <td align=right > 4.49605      </td>
      <td align=right > 1.196      </td>
      <td align=right > 6.68816      </td>
      <td align=right > 112.664      </td>
      <td align=right > 49.3023      </td>
      <td align=right > 20.1471      </td>
    </tr>
    <tr> 
      <td align=right > 1.45      </td>
      <td align=right > 4.97007      </td>
      <td align=right > 1.21025      </td>
      <td align=right > 7.2599      </td>
      <td align=right > 149.442      </td>
      <td align=right > 64.6835      </td>
      <td align=right > 24.9126      </td>
    </tr>
    <tr> 
      <td align=right > 1.5      </td>
      <td align=right > 5.49567      </td>
      <td align=right > 1.225      </td>
      <td align=right > 7.8639      </td>
      <td align=right > 197.924      </td>
      <td align=right > 85.0506      </td>
      <td align=right > 30.8317      </td>
    </tr>
    <tr> 
      <td align=right > 1.55      </td>
      <td align=right > 6.07874      </td>
      <td align=right > 1.24025      </td>
      <td align=right > 8.50143      </td>
      <td align=right > 261.781      </td>
      <td align=right > 112.073      </td>
      <td align=right > 38.1905      </td>
    </tr>
    <tr> 
      <td align=right > 1.6      </td>
      <td align=right > 6.7258      </td>
      <td align=right > 1.256      </td>
      <td align=right > 9.1738      </td>
      <td align=right > 345.825      </td>
      <td align=right > 147.994      </td>
      <td align=right > 47.3475      </td>
    </tr>
    <tr> 
      <td align=right > 1.65      </td>
      <td align=right > 7.44413      </td>
      <td align=right > 1.27225      </td>
      <td align=right > 9.88234      </td>
      <td align=right > 456.358      </td>
      <td align=right > 195.831      </td>
      <td align=right > 58.7515      </td>
    </tr>
    <tr> 
      <td align=right > 1.7      </td>
      <td align=right > 8.24178      </td>
      <td align=right > 1.289      </td>
      <td align=right > 10.6285      </td>
      <td align=right > 601.633      </td>
      <td align=right > 259.644      </td>
      <td align=right > 72.9649      </td>
    </tr>
    <tr> 
      <td align=right > 1.75      </td>
      <td align=right > 9.12775      </td>
      <td align=right > 1.30625      </td>
      <td align=right > 11.4136      </td>
      <td align=right > 792.444      </td>
      <td align=right > 344.909      </td>
      <td align=right > 90.6928      </td>
    </tr>
    <tr> 
      <td align=right > 1.8      </td>
      <td align=right > 10.112      </td>
      <td align=right > 1.324      </td>
      <td align=right > 12.2392      </td>
      <td align=right > 1042.91      </td>
      <td align=right > 459.01      </td>
      <td align=right > 112.819      </td>
    </tr>
    <tr> 
      <td align=right > 1.85      </td>
      <td align=right > 11.2056      </td>
      <td align=right > 1.34225      </td>
      <td align=right > 13.1068      </td>
      <td align=right > 1371.48      </td>
      <td align=right > 611.917      </td>
      <td align=right > 140.451      </td>
    </tr>
    <tr> 
      <td align=right > 1.9      </td>
      <td align=right > 12.4209      </td>
      <td align=right > 1.361      </td>
      <td align=right > 14.018      </td>
      <td align=right > 1802.24      </td>
      <td align=right > 817.103      </td>
      <td align=right > 174.979      </td>
    </tr>
    <tr> 
      <td align=right > 1.95      </td>
      <td align=right > 13.7716      </td>
      <td align=right > 1.38025      </td>
      <td align=right > 14.9743      </td>
      <td align=right > 2366.64      </td>
      <td align=right > 1092.78      </td>
      <td align=right > 218.144      </td>
    </tr>
    <tr> 
      <td align=right > 2      </td>
      <td align=right > 15.2727      </td>
      <td align=right > 1.4      </td>
      <td align=right > 15.9775      </td>
      <td align=right > 3105.68      </td>
      <td align=right > 1463.58      </td>
      <td align=right > 272.131      </td>
    </tr>
    <tr> 
      <td align=right > 2.25      </td>
      <td align=right > 25.692      </td>
      <td align=right > 1.50625      </td>
      <td align=right > 21.7561      </td>
      <td align=right > 11967.3      </td>
      <td align=right > 6423.86      </td>
      <td align=right > 828.535      </td>
    </tr>
    <tr> 
      <td align=right > 2.5      </td>
      <td align=right > 43.3326      </td>
      <td align=right > 1.625      </td>
      <td align=right > 28.977      </td>
      <td align=right > 45342.1      </td>
      <td align=right > 28811.7      </td>
      <td align=right > 2542.74      </td>
    </tr>
    <tr> 
      <td align=right > 2.75      </td>
      <td align=right > 73.0699      </td>
      <td align=right > 1.75625      </td>
      <td align=right > 37.8941      </td>
      <td align=right > 168604      </td>
      <td align=right > 130561      </td>
      <td align=right > 7814.17      </td>
    </tr>
    <tr> 
      <td align=right > 3      </td>
      <td align=right > 122.87      </td>
      <td align=right > 1.9      </td>
      <td align=right > 48.7883      </td>
      <td align=right > 613803      </td>
      <td align=right > 591882      </td>
      <td align=right > 23903.8      </td>
    </tr>
    <tr> 
      <td align=right > 3.25      </td>
      <td align=right > 205.581      </td>
      <td align=right > 2.05625      </td>
      <td align=right > 61.9672      </td>
      <td align=right > 2.18245e+6      </td>
      <td align=right > 2.66219e+6      </td>
      <td align=right > 72420.1      </td>
    </tr>
    <tr> 
      <td align=right > 3.5      </td>
      <td align=right > 341.637      </td>
      <td align=right > 2.225      </td>
      <td align=right > 77.7653      </td>
      <td align=right > 7.56368e+6      </td>
      <td align=right > 1.18012e+7      </td>
      <td align=right > 216410      </td>
    </tr>
    <tr> 
      <td align=right > 3.75      </td>
      <td align=right > 563.101      </td>
      <td align=right > 2.40625      </td>
      <td align=right > 96.5435      </td>
      <td align=right > 2.55103e+7      </td>
      <td align=right > 5.12904e+7      </td>
      <td align=right > 635818      </td>
    </tr>
    <tr> 
      <td align=right > 4      </td>
      <td align=right > 919.591      </td>
      <td align=right > 2.6      </td>
      <td align=right > 118.69      </td>
      <td align=right > 8.36413e+7      </td>
      <td align=right > 2.17695e+8      </td>
      <td align=right > 1.83224e+6      </td>
    </tr>
    <tr> 
      <td align=right > 4.25      </td>
      <td align=right > 1486.84      </td>
      <td align=right > 2.80625      </td>
      <td align=right > 144.618      </td>
      <td align=right > 2.66424e+8      </td>
      <td align=right > 8.9972e+8      </td>
      <td align=right > 5.16982e+6      </td>
    </tr>
    <tr> 
      <td align=right > 4.5      </td>
      <td align=right > 2378.94      </td>
      <td align=right > 3.025      </td>
      <td align=right > 174.771      </td>
      <td align=right > 8.24243e+8      </td>
      <td align=right > 3.61358e+9      </td>
      <td align=right > 1.42663e+7      </td>
    </tr>
    <tr> 
      <td align=right > 4.75      </td>
      <td align=right > 3765.5      </td>
      <td align=right > 3.25625      </td>
      <td align=right > 209.616      </td>
      <td align=right > 2.47678e+9      </td>
      <td align=right > 1.40854e+10      </td>
      <td align=right > 3.84752e+7      </td>
    </tr>
    <tr> 
      <td align=right > 5      </td>
      <td align=right > 5895.57      </td>
      <td align=right > 3.5      </td>
      <td align=right > 249.648      </td>
      <td align=right > 7.23098e+9      </td>
      <td align=right > 5.32449e+10      </td>
      <td align=right > 1.01377e+8      </td>
    </tr>
    <tr> 
      <td align=right > 5.25      </td>
      <td align=right > 9130.41      </td>
      <td align=right > 3.75625      </td>
      <td align=right > 295.388      </td>
      <td align=right > 2.05207e+10      </td>
      <td align=right > 1.95131e+11      </td>
      <td align=right > 2.60948e+8      </td>
    </tr>
    <tr> 
      <td align=right > 5.5      </td>
      <td align=right > 13988      </td>
      <td align=right > 4.025      </td>
      <td align=right > 347.385      </td>
      <td align=right > 5.66425e+10      </td>
      <td align=right > 6.93309e+11      </td>
      <td align=right > 6.56292e+8      </td>
    </tr>
    <tr> 
      <td align=right > 5.75      </td>
      <td align=right > 21202.9      </td>
      <td align=right > 4.30625      </td>
      <td align=right > 406.214      </td>
      <td align=right > 1.52182e+11      </td>
      <td align=right > 2.38893e+12      </td>
      <td align=right > 1.61327e+9      </td>
    </tr>
    <tr> 
      <td align=right > 6      </td>
      <td align=right > 31805.4      </td>
      <td align=right > 4.6      </td>
      <td align=right > 472.476      </td>
      <td align=right > 3.9829e+11      </td>
      <td align=right > 7.98669e+12      </td>
      <td align=right > 3.87773e+9      </td>
    </tr>
    <tr> 
      <td align=right > 6.25      </td>
      <td align=right > 47226.4      </td>
      <td align=right > 4.90625      </td>
      <td align=right > 546.801      </td>
      <td align=right > 1.01629e+12      </td>
      <td align=right > 2.5923e+13      </td>
      <td align=right > 9.11879e+9      </td>
    </tr>
    <tr> 
      <td align=right > 6.5      </td>
      <td align=right > 69433.5      </td>
      <td align=right > 5.225      </td>
      <td align=right > 629.843      </td>
      <td align=right > 2.5304e+12      </td>
      <td align=right > 8.17476e+13      </td>
      <td align=right > 2.09915e+10      </td>
    </tr>
    <tr> 
      <td align=right > 6.75      </td>
      <td align=right > 101108      </td>
      <td align=right > 5.55625      </td>
      <td align=right > 722.285      </td>
      <td align=right > 6.15315e+12      </td>
      <td align=right > 2.50657e+14      </td>
      <td align=right > 4.73337e+10      </td>
    </tr>
    <tr> 
      <td align=right > 7      </td>
      <td align=right > 145870      </td>
      <td align=right > 5.9      </td>
      <td align=right > 824.836      </td>
      <td align=right > 1.46257e+13      </td>
      <td align=right > 7.47931e+14      </td>
      <td align=right > 1.04617e+11      </td>
    </tr>
    <tr> 
      <td align=right > 7.25      </td>
      <td align=right > 208568      </td>
      <td align=right > 6.25625      </td>
      <td align=right > 938.231      </td>
      <td align=right > 3.40112e+13      </td>
      <td align=right > 2.17369e+15      </td>
      <td align=right > 2.26791e+11      </td>
    </tr>
    <tr> 
      <td align=right > 7.5      </td>
      <td align=right > 295641      </td>
      <td align=right > 6.625      </td>
      <td align=right > 1063.23      </td>
      <td align=right > 7.7442e+13      </td>
      <td align=right > 6.15834e+15      </td>
      <td align=right > 4.82541e+11      </td>
    </tr>
    <tr> 
      <td align=right > 7.75      </td>
      <td align=right > 415579      </td>
      <td align=right > 7.00625      </td>
      <td align=right > 1200.63      </td>
      <td align=right > 1.72797e+14      </td>
      <td align=right > 1.70232e+16      </td>
      <td align=right > 1.00835e+12      </td>
    </tr>
    <tr> 
      <td align=right > 8      </td>
      <td align=right > 579490      </td>
      <td align=right > 7.4      </td>
      <td align=right > 1351.24      </td>
      <td align=right > 3.7813e+14      </td>
      <td align=right > 4.59516e+16      </td>
      <td align=right > 2.07082e+12      </td>
    </tr>
    <tr> 
      <td align=right > 8.25      </td>
      <td align=right > 801805      </td>
      <td align=right > 7.80625      </td>
      <td align=right > 1515.9      </td>
      <td align=right > 8.12131e+14      </td>
      <td align=right > 1.2123e+17      </td>
      <td align=right > 4.18214e+12      </td>
    </tr>
    <tr> 
      <td align=right > 8.5      </td>
      <td align=right > 1.10115e+6      </td>
      <td align=right > 8.225      </td>
      <td align=right > 1695.48      </td>
      <td align=right > 1.7132e+15      </td>
      <td align=right > 3.12845e+17      </td>
      <td align=right > 8.31094e+12      </td>
    </tr>
    <tr> 
      <td align=right > 8.75      </td>
      <td align=right > 1.50142e+6      </td>
      <td align=right > 8.65625      </td>
      <td align=right > 1890.89      </td>
      <td align=right > 3.55214e+15      </td>
      <td align=right > 7.90321e+17      </td>
      <td align=right > 1.62613e+13      </td>
    </tr>
    <tr> 
      <td align=right > 9      </td>
      <td align=right > 2.03307e+6      </td>
      <td align=right > 9.1      </td>
      <td align=right > 2103.03      </td>
      <td align=right > 7.24382e+15      </td>
      <td align=right > 1.956e+18      </td>
      <td align=right > 3.13446e+13      </td>
    </tr>
    <tr> 
      <td align=right > 9.25      </td>
      <td align=right > 2.73467e+6      </td>
      <td align=right > 9.55625      </td>
      <td align=right > 2332.87      </td>
      <td align=right > 1.45384e+16      </td>
      <td align=right > 4.74627e+18      </td>
      <td align=right > 5.95545e+13      </td>
    </tr>
    <tr> 
      <td align=right > 9.5      </td>
      <td align=right > 3.65484e+6      </td>
      <td align=right > 10.025      </td>
      <td align=right > 2581.38      </td>
      <td align=right > 2.87348e+16      </td>
      <td align=right > 1.12996e+19      </td>
      <td align=right > 1.11594e+14      </td>
    </tr>
    <tr> 
      <td align=right > 9.75      </td>
      <td align=right > 4.85454e+6      </td>
      <td align=right > 10.5062      </td>
      <td align=right > 2849.55      </td>
      <td align=right > 5.59618e+16      </td>
      <td align=right > 2.6412e+19      </td>
      <td align=right > 2.0633e+14      </td>
    </tr>
    <tr> 
      <td align=right > 10      </td>
      <td align=right > 6.40975e+6      </td>
      <td align=right > 11      </td>
      <td align=right > 3138.43      </td>
      <td align=right > 1.07452e+17      </td>
      <td align=right > 6.06536e+19      </td>
      <td align=right > 3.76611e+14      </td>
    </tr>
    <tr> 
      <td align=right > 20      </td>
      <td align=right > 1.78042e+10      </td>
      <td align=right > 41      </td>
      <td align=right > 46791.1      </td>
      <td align=right > 1.23602e+25      </td>
      <td align=right > 1.25478e+30      </td>
      <td align=right > 1.08305e+22      </td>
    </tr>
    <tr> 
      <td align=right > 30      </td>
      <td align=right > 2.14304e+12      </td>
      <td align=right > 91      </td>
      <td align=right > 233670      </td>
      <td align=right > 8.94299e+29      </td>
      <td align=right > 2.17334e+36      </td>
      <td align=right > 3.48274e+26      </td>
    </tr>
    <tr> 
      <td align=right > 40      </td>
      <td align=right > 6.58845e+13      </td>
      <td align=right > 161      </td>
      <td align=right > 734963      </td>
      <td align=right > 2.65859e+33      </td>
      <td align=right > 6.30001e+40      </td>
      <td align=right > 5.82387e+29      </td>
    </tr>
    <tr> 
      <td align=right > 50      </td>
      <td align=right > 9.47015e+14      </td>
      <td align=right > 251      </td>
      <td align=right > 1.79033e+6      </td>
      <td align=right > 1.33803e+36      </td>
      <td align=right > 1.86885e+44      </td>
      <td align=right > 1.87588e+32      </td>
    </tr>
  </tbody>
</table>
<BR><HR>
<ADDRESS>
genick
2007-11-14
</ADDRESS>
        </tr>
    </tbody>
    </table>
    <? include("aboutPottoProject.php");  ?>
    </div>
    </td>
    </tr>
    </tbody>
</table>
<? include("bottom.php"); ?>

