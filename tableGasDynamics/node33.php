<? include("header.php");?>
<table style="width:100%; height:993px;" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
	<? include("left_link.php");?>
	<td style="width: 80%; vertical-align: top;">
	<div  font-family: "arial", "helvetica", "lucida", "sans";>

<!--Navigation Panel-->
<A NAME="tex2html497"
  HREF="node34.php">
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next"
 SRC="figures/next.png"></A> 
<A NAME="tex2html493"
  HREF="node30.php">
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up"
 SRC="figures/up.png"></A> 
<A NAME="tex2html487"
  HREF="node32.php">
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous"
 SRC="figures/prev.png"></A> 
<A NAME="tex2html495"
  HREF="node1.php">
<IMG WIDTH="65" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="contents"
 SRC="figures/contents.png"></A>  
<BR>
<B> Next:</B> <A NAME="tex2html498"
  HREF="node34.php">Shock from suddenly open</A>
<B> Up:</B> <A NAME="tex2html494"
  HREF="node30.php">Shock propagating from suddenly</A>
<B> Previous:</B> <A NAME="tex2html488"
  HREF="node32.php">Shock from suddenly open</A>
 &nbsp; <B>  <A NAME="tex2html496"
  HREF="node1.php">Contents</A></B> 
<BR>
<BR>
<!--End of Navigation Panel-->

<H3><A NAME="SECTION00044300000000000000">
Shock from suddenly open valve k =1.4</A>
</H3>

<P>

<table border=1 width="100%" >
  <thead>
    <tr>
      <th align=left bgcolor="#fffeaa" colspan=3 >Shock Dynamics </th>
      <th align=left  bgcolor="#00ff5a" colspan=3  rowspan=2 >Input: My </th>
      <th align=left  bgcolor="#9ae0ee" colspan=1 rowspan=2 >k = 1.4 </th>
    </tr>
    <tr>
      <th align=left bgcolor="#fffeaa" colspan=3 > </th>
    </tr>
    <tr>
      <th align=center >Mx  </th>
      <th align=center >Mx&prime; </th>
      <th align=center >My </th>
      <th align=center >My&prime; </th>
      <th align=center >Ty/Tx </th>
      <th align=center >Py/Px </th>
      <th align=center >P0y/P0x </th>
    </tr> 
  </thead>
  <tbody>
    <tr> 
      <td align=right > 857.143      </td>
      <td align=right > 0      </td>
      <td align=right > 0.377966      </td>
      <td align=right > 1.88981      </td>
      <td align=right > 142858      </td>
      <td align=right > 857143      </td>
      <td align=right > 7.77832e-13      </td>
    </tr>
    <tr> 
      <td align=right > 19.8904      </td>
      <td align=right > 0      </td>
      <td align=right > 0.380414      </td>
      <td align=right > 1.87358      </td>
      <td align=right > 77.8719      </td>
      <td align=right > 461.4      </td>
      <td align=right > 0.000110725      </td>
    </tr>
    <tr> 
      <td align=right > 14.0464      </td>
      <td align=right > 0      </td>
      <td align=right > 0.382862      </td>
      <td align=right > 1.85754      </td>
      <td align=right > 39.3082      </td>
      <td align=right > 230.02      </td>
      <td align=right > 0.000604054      </td>
    </tr>
    <tr> 
      <td align=right > 11.4531      </td>
      <td align=right > 0      </td>
      <td align=right > 0.385311      </td>
      <td align=right > 1.84167      </td>
      <td align=right > 26.4492      </td>
      <td align=right > 152.868      </td>
      <td align=right > 0.00160647      </td>
    </tr>
    <tr> 
      <td align=right > 9.90483      </td>
      <td align=right > 0      </td>
      <td align=right > 0.387759      </td>
      <td align=right > 1.82597      </td>
      <td align=right > 20.0191      </td>
      <td align=right > 114.29      </td>
      <td align=right > 0.00318383      </td>
    </tr>
    <tr> 
      <td align=right > 8.84677      </td>
      <td align=right > 0      </td>
      <td align=right > 0.390207      </td>
      <td align=right > 1.81045      </td>
      <td align=right > 16.1609      </td>
      <td align=right > 91.143      </td>
      <td align=right > 0.00537143      </td>
    </tr>
    <tr> 
      <td align=right > 8.0647      </td>
      <td align=right > 0      </td>
      <td align=right > 0.392655      </td>
      <td align=right > 1.79509      </td>
      <td align=right > 13.5888      </td>
      <td align=right > 75.7125      </td>
      <td align=right > 0.00818519      </td>
    </tr>
    <tr> 
      <td align=right > 7.45607      </td>
      <td align=right > 0      </td>
      <td align=right > 0.395104      </td>
      <td align=right > 1.7799      </td>
      <td align=right > 11.7517      </td>
      <td align=right > 64.6918      </td>
      <td align=right > 0.0116278      </td>
    </tr>
    <tr> 
      <td align=right > 6.96484      </td>
      <td align=right > 0      </td>
      <td align=right > 0.397552      </td>
      <td align=right > 1.76487      </td>
      <td align=right > 10.3739      </td>
      <td align=right > 56.4272      </td>
      <td align=right > 0.0156925      </td>
    </tr>
    <tr> 
      <td align=right > 6.55744      </td>
      <td align=right > 0      </td>
      <td align=right > 0.4      </td>
      <td align=right > 1.75      </td>
      <td align=right > 9.30233      </td>
      <td align=right > 50      </td>
      <td align=right > 0.0203657      </td>
    </tr>
    <tr> 
      <td align=right > 5.40813      </td>
      <td align=right > 0      </td>
      <td align=right > 0.41      </td>
      <td align=right > 1.69085      </td>
      <td align=right > 6.62678      </td>
      <td align=right > 33.9559      </td>
      <td align=right > 0.0453268      </td>
    </tr>
    <tr> 
      <td align=right > 4.69532      </td>
      <td align=right > 0      </td>
      <td align=right > 0.42      </td>
      <td align=right > 1.63413      </td>
      <td align=right > 5.22487      </td>
      <td align=right > 25.5537      </td>
      <td align=right > 0.0783773      </td>
    </tr>
    <tr> 
      <td align=right > 4.19735      </td>
      <td align=right > 0      </td>
      <td align=right > 0.43      </td>
      <td align=right > 1.57965      </td>
      <td align=right > 4.36223      </td>
      <td align=right > 20.3874      </td>
      <td align=right > 0.117593      </td>
    </tr>
    <tr> 
      <td align=right > 3.82382      </td>
      <td align=right > 0      </td>
      <td align=right > 0.44      </td>
      <td align=right > 1.52727      </td>
      <td align=right > 3.77804      </td>
      <td align=right > 16.8919      </td>
      <td align=right > 0.161156      </td>
    </tr>
    <tr> 
      <td align=right > 3.53003      </td>
      <td align=right > 0      </td>
      <td align=right > 0.45      </td>
      <td align=right > 1.47685      </td>
      <td align=right > 3.35629      </td>
      <td align=right > 14.3713      </td>
      <td align=right > 0.207486      </td>
    </tr>
    <tr> 
      <td align=right > 3.29096      </td>
      <td align=right > 0      </td>
      <td align=right > 0.46      </td>
      <td align=right > 1.42826      </td>
      <td align=right > 3.03754      </td>
      <td align=right > 12.4688      </td>
      <td align=right > 0.25527      </td>
    </tr>
    <tr> 
      <td align=right > 3.09141      </td>
      <td align=right > 0      </td>
      <td align=right > 0.47      </td>
      <td align=right > 1.38138      </td>
      <td align=right > 2.78819      </td>
      <td align=right > 10.983      </td>
      <td align=right > 0.303455      </td>
    </tr>
    <tr> 
      <td align=right > 2.92151      </td>
      <td align=right > 0      </td>
      <td align=right > 0.48      </td>
      <td align=right > 1.33611      </td>
      <td align=right > 2.5878      </td>
      <td align=right > 9.79112      </td>
      <td align=right > 0.351215      </td>
    </tr>
    <tr> 
      <td align=right > 2.77455      </td>
      <td align=right > 0      </td>
      <td align=right > 0.49      </td>
      <td align=right > 1.29235      </td>
      <td align=right > 2.42326      </td>
      <td align=right > 8.81446      </td>
      <td align=right > 0.397922      </td>
    </tr>
    <tr> 
      <td align=right > 2.64575      </td>
      <td align=right > 0      </td>
      <td align=right > 0.5      </td>
      <td align=right > 1.25      </td>
      <td align=right > 2.28571      </td>
      <td align=right > 8      </td>
      <td align=right > 0.443112      </td>
    </tr>
    <tr> 
      <td align=right > 2.53166      </td>
      <td align=right > 0      </td>
      <td align=right > 0.51      </td>
      <td align=right > 1.20899      </td>
      <td align=right > 2.16902      </td>
      <td align=right > 7.31083      </td>
      <td align=right > 0.486454      </td>
    </tr>
    <tr> 
      <td align=right > 2.42966      </td>
      <td align=right > 0      </td>
      <td align=right > 0.52      </td>
      <td align=right > 1.16923      </td>
      <td align=right > 2.06877      </td>
      <td align=right > 6.72043      </td>
      <td align=right > 0.527724      </td>
    </tr>
    <tr> 
      <td align=right > 2.33775      </td>
      <td align=right > 0      </td>
      <td align=right > 0.53      </td>
      <td align=right > 1.13066      </td>
      <td align=right > 1.98168      </td>
      <td align=right > 6.20925      </td>
      <td align=right > 0.566786      </td>
    </tr>
    <tr> 
      <td align=right > 2.25438      </td>
      <td align=right > 0      </td>
      <td align=right > 0.54      </td>
      <td align=right > 1.09321      </td>
      <td align=right > 1.90532      </td>
      <td align=right > 5.76258      </td>
      <td align=right > 0.603568      </td>
    </tr>
    <tr> 
      <td align=right > 2.17829      </td>
      <td align=right > 0      </td>
      <td align=right > 0.55      </td>
      <td align=right > 1.05682      </td>
      <td align=right > 1.83781      </td>
      <td align=right > 5.36913      </td>
      <td align=right > 0.63805      </td>
    </tr>
    <tr> 
      <td align=right > 2.1085      </td>
      <td align=right > 0      </td>
      <td align=right > 0.56      </td>
      <td align=right > 1.02143      </td>
      <td align=right > 1.77766      </td>
      <td align=right > 5.02008      </td>
      <td align=right > 0.670253      </td>
    </tr>
    <tr> 
      <td align=right > 2.04418      </td>
      <td align=right > 0      </td>
      <td align=right > 0.57      </td>
      <td align=right > 0.986988      </td>
      <td align=right > 1.72373      </td>
      <td align=right > 4.70847      </td>
      <td align=right > 0.700225      </td>
    </tr>
    <tr> 
      <td align=right > 1.98466      </td>
      <td align=right > 0      </td>
      <td align=right > 0.58      </td>
      <td align=right > 0.953448      </td>
      <td align=right > 1.67508      </td>
      <td align=right > 4.4287      </td>
      <td align=right > 0.728038      </td>
    </tr>
    <tr> 
      <td align=right > 1.92938      </td>
      <td align=right > 0      </td>
      <td align=right > 0.59      </td>
      <td align=right > 0.920763      </td>
      <td align=right > 1.63095      </td>
      <td align=right > 4.17624      </td>
      <td align=right > 0.753775      </td>
    </tr>
    <tr> 
      <td align=right > 1.87785      </td>
      <td align=right > 0      </td>
      <td align=right > 0.6      </td>
      <td align=right > 0.888889      </td>
      <td align=right > 1.59073      </td>
      <td align=right > 3.94737      </td>
      <td align=right > 0.777534      </td>
    </tr>
    <tr> 
      <td align=right > 1.82968      </td>
      <td align=right > 0      </td>
      <td align=right > 0.61      </td>
      <td align=right > 0.857787      </td>
      <td align=right > 1.5539      </td>
      <td align=right > 3.73902      </td>
      <td align=right > 0.799416      </td>
    </tr>
    <tr> 
      <td align=right > 1.78452      </td>
      <td align=right > 0      </td>
      <td align=right > 0.62      </td>
      <td align=right > 0.827419      </td>
      <td align=right > 1.52004      </td>
      <td align=right > 3.54862      </td>
      <td align=right > 0.819524      </td>
    </tr>
    <tr> 
      <td align=right > 1.74209      </td>
      <td align=right > 0      </td>
      <td align=right > 0.63      </td>
      <td align=right > 0.797751      </td>
      <td align=right > 1.48879      </td>
      <td align=right > 3.37401      </td>
      <td align=right > 0.837966      </td>
    </tr>
    <tr> 
      <td align=right > 1.70211      </td>
      <td align=right > 0      </td>
      <td align=right > 0.64      </td>
      <td align=right > 0.76875      </td>
      <td align=right > 1.45984      </td>
      <td align=right > 3.21337      </td>
      <td align=right > 0.854846      </td>
    </tr>
    <tr> 
      <td align=right > 1.66437      </td>
      <td align=right > 0      </td>
      <td align=right > 0.65      </td>
      <td align=right > 0.740385      </td>
      <td align=right > 1.43294      </td>
      <td align=right > 3.06513      </td>
      <td align=right > 0.870266      </td>
    </tr>
    <tr> 
      <td align=right > 1.62866      </td>
      <td align=right > 0      </td>
      <td align=right > 0.66      </td>
      <td align=right > 0.712626      </td>
      <td align=right > 1.40786      </td>
      <td align=right > 2.92797      </td>
      <td align=right > 0.884325      </td>
    </tr>
    <tr> 
      <td align=right > 1.59483      </td>
      <td align=right > 0      </td>
      <td align=right > 0.67      </td>
      <td align=right > 0.685448      </td>
      <td align=right > 1.3844      </td>
      <td align=right > 2.80073      </td>
      <td align=right > 0.89712      </td>
    </tr>
    <tr> 
      <td align=right > 1.56271      </td>
      <td align=right > 0      </td>
      <td align=right > 0.68      </td>
      <td align=right > 0.658824      </td>
      <td align=right > 1.36242      </td>
      <td align=right > 2.6824      </td>
      <td align=right > 0.908741      </td>
    </tr>
    <tr> 
      <td align=right > 1.53217      </td>
      <td align=right > 0      </td>
      <td align=right > 0.69      </td>
      <td align=right > 0.632729      </td>
      <td align=right > 1.34175      </td>
      <td align=right > 2.57213      </td>
      <td align=right > 0.919276      </td>
    </tr>
    <tr> 
      <td align=right > 1.50308      </td>
      <td align=right > 0      </td>
      <td align=right > 0.7      </td>
      <td align=right > 0.607143      </td>
      <td align=right > 1.32227      </td>
      <td align=right > 2.46914      </td>
      <td align=right > 0.928806      </td>
    </tr>
    <tr> 
      <td align=right > 1.47535      </td>
      <td align=right > 0      </td>
      <td align=right > 0.71      </td>
      <td align=right > 0.582042      </td>
      <td align=right > 1.30387      </td>
      <td align=right > 2.37276      </td>
      <td align=right > 0.93741      </td>
    </tr>
    <tr> 
      <td align=right > 1.44886      </td>
      <td align=right > 0      </td>
      <td align=right > 0.72      </td>
      <td align=right > 0.557407      </td>
      <td align=right > 1.28646      </td>
      <td align=right > 2.28241      </td>
      <td align=right > 0.945159      </td>
    </tr>
    <tr> 
      <td align=right > 1.42355      </td>
      <td align=right > 0      </td>
      <td align=right > 0.73      </td>
      <td align=right > 0.533219      </td>
      <td align=right > 1.26995      </td>
      <td align=right > 2.19756      </td>
      <td align=right > 0.952123      </td>
    </tr>
    <tr> 
      <td align=right > 1.39931      </td>
      <td align=right > 0      </td>
      <td align=right > 0.74      </td>
      <td align=right > 0.509459      </td>
      <td align=right > 1.25425      </td>
      <td align=right > 2.11775      </td>
      <td align=right > 0.958366      </td>
    </tr>
    <tr> 
      <td align=right > 1.37609      </td>
      <td align=right > 0      </td>
      <td align=right > 0.75      </td>
      <td align=right > 0.486111      </td>
      <td align=right > 1.2393      </td>
      <td align=right > 2.04255      </td>
      <td align=right > 0.963948      </td>
    </tr>
    <tr> 
      <td align=right > 1.35381      </td>
      <td align=right > 0      </td>
      <td align=right > 0.76      </td>
      <td align=right > 0.463158      </td>
      <td align=right > 1.22504      </td>
      <td align=right > 1.97161      </td>
      <td align=right > 0.968923      </td>
    </tr>
    <tr> 
      <td align=right > 1.33242      </td>
      <td align=right > 0      </td>
      <td align=right > 0.77      </td>
      <td align=right > 0.440584      </td>
      <td align=right > 1.21142      </td>
      <td align=right > 1.90458      </td>
      <td align=right > 0.973345      </td>
    </tr>
    <tr> 
      <td align=right > 1.31187      </td>
      <td align=right > 0      </td>
      <td align=right > 0.78      </td>
      <td align=right > 0.418376      </td>
      <td align=right > 1.19838      </td>
      <td align=right > 1.84117      </td>
      <td align=right > 0.977262      </td>
    </tr>
    <tr> 
      <td align=right > 1.2921      </td>
      <td align=right > 0      </td>
      <td align=right > 0.79      </td>
      <td align=right > 0.396519      </td>
      <td align=right > 1.18588      </td>
      <td align=right > 1.7811      </td>
      <td align=right > 0.980718      </td>
    </tr>
    <tr> 
      <td align=right > 1.27306      </td>
      <td align=right > 0      </td>
      <td align=right > 0.8      </td>
      <td align=right > 0.375      </td>
      <td align=right > 1.17388      </td>
      <td align=right > 1.72414      </td>
      <td align=right > 0.983756      </td>
    </tr>
    <tr> 
      <td align=right > 1.25472      </td>
      <td align=right > 0      </td>
      <td align=right > 0.81      </td>
      <td align=right > 0.353807      </td>
      <td align=right > 1.16234      </td>
      <td align=right > 1.67005      </td>
      <td align=right > 0.986415      </td>
    </tr>
    <tr> 
      <td align=right > 1.23704      </td>
      <td align=right > 0      </td>
      <td align=right > 0.82      </td>
      <td align=right > 0.332927      </td>
      <td align=right > 1.15124      </td>
      <td align=right > 1.61865      </td>
      <td align=right > 0.988729      </td>
    </tr>
    <tr> 
      <td align=right > 1.21998      </td>
      <td align=right > 0      </td>
      <td align=right > 0.83      </td>
      <td align=right > 0.312349      </td>
      <td align=right > 1.14053      </td>
      <td align=right > 1.56974      </td>
      <td align=right > 0.990733      </td>
    </tr>
    <tr> 
      <td align=right > 1.2035      </td>
      <td align=right > 0      </td>
      <td align=right > 0.84      </td>
      <td align=right > 0.292063      </td>
      <td align=right > 1.13019      </td>
      <td align=right > 1.52315      </td>
      <td align=right > 0.992458      </td>
    </tr>
    <tr> 
      <td align=right > 1.18758      </td>
      <td align=right > 0      </td>
      <td align=right > 0.85      </td>
      <td align=right > 0.272059      </td>
      <td align=right > 1.1202      </td>
      <td align=right > 1.47874      </td>
      <td align=right > 0.993932      </td>
    </tr>
    <tr> 
      <td align=right > 1.17219      </td>
      <td align=right > 0      </td>
      <td align=right > 0.86      </td>
      <td align=right > 0.252326      </td>
      <td align=right > 1.11054      </td>
      <td align=right > 1.43637      </td>
      <td align=right > 0.995181      </td>
    </tr>
    <tr> 
      <td align=right > 1.1573      </td>
      <td align=right > 0      </td>
      <td align=right > 0.87      </td>
      <td align=right > 0.232854      </td>
      <td align=right > 1.10117      </td>
      <td align=right > 1.3959      </td>
      <td align=right > 0.996231      </td>
    </tr>
    <tr> 
      <td align=right > 1.14289      </td>
      <td align=right > 0      </td>
      <td align=right > 0.88      </td>
      <td align=right > 0.213636      </td>
      <td align=right > 1.09209      </td>
      <td align=right > 1.35722      </td>
      <td align=right > 0.997103      </td>
    </tr>
    <tr> 
      <td align=right > 1.12893      </td>
      <td align=right > 0      </td>
      <td align=right > 0.89      </td>
      <td align=right > 0.194663      </td>
      <td align=right > 1.08328      </td>
      <td align=right > 1.32022      </td>
      <td align=right > 0.997819      </td>
    </tr>
    <tr> 
      <td align=right > 1.1154      </td>
      <td align=right > 0      </td>
      <td align=right > 0.9      </td>
      <td align=right > 0.175926      </td>
      <td align=right > 1.07472      </td>
      <td align=right > 1.2848      </td>
      <td align=right > 0.998398      </td>
    </tr>
    <tr> 
      <td align=right > 1.10228      </td>
      <td align=right > 0      </td>
      <td align=right > 0.91      </td>
      <td align=right > 0.157418      </td>
      <td align=right > 1.06639      </td>
      <td align=right > 1.25086      </td>
      <td align=right > 0.998858      </td>
    </tr>
    <tr> 
      <td align=right > 1.08956      </td>
      <td align=right > 0      </td>
      <td align=right > 0.92      </td>
      <td align=right > 0.13913      </td>
      <td align=right > 1.05828      </td>
      <td align=right > 1.21832      </td>
      <td align=right > 0.999215      </td>
    </tr>
    <tr> 
      <td align=right > 1.07721      </td>
      <td align=right > 0      </td>
      <td align=right > 0.93      </td>
      <td align=right > 0.121057      </td>
      <td align=right > 1.05038      </td>
      <td align=right > 1.18711      </td>
      <td align=right > 0.999486      </td>
    </tr>
    <tr> 
      <td align=right > 1.06522      </td>
      <td align=right > 0      </td>
      <td align=right > 0.94      </td>
      <td align=right > 0.103191      </td>
      <td align=right > 1.04268      </td>
      <td align=right > 1.15714      </td>
      <td align=right > 0.999683      </td>
    </tr>
    <tr> 
      <td align=right > 1.05357      </td>
      <td align=right > 0      </td>
      <td align=right > 0.95      </td>
      <td align=right > 0.0855263      </td>
      <td align=right > 1.03516      </td>
      <td align=right > 1.12835      </td>
      <td align=right > 0.99982      </td>
    </tr>
    <tr> 
      <td align=right > 1.04225      </td>
      <td align=right > 0      </td>
      <td align=right > 0.96      </td>
      <td align=right > 0.0680556      </td>
      <td align=right > 1.02781      </td>
      <td align=right > 1.10068      </td>
      <td align=right > 0.99991      </td>
    </tr>
    <tr> 
      <td align=right > 1.03125      </td>
      <td align=right > 0      </td>
      <td align=right > 0.97      </td>
      <td align=right > 0.0507732      </td>
      <td align=right > 1.02063      </td>
      <td align=right > 1.07406      </td>
      <td align=right > 0.999963      </td>
    </tr>
    <tr> 
      <td align=right > 1.02055      </td>
      <td align=right > 0      </td>
      <td align=right > 0.98      </td>
      <td align=right > 0.0336735      </td>
      <td align=right > 1.01361      </td>
      <td align=right > 1.04844      </td>
      <td align=right > 0.999989      </td>
    </tr>
    <tr> 
      <td align=right > 1.01014      </td>
      <td align=right > 0      </td>
      <td align=right > 0.99      </td>
      <td align=right > 0.0167508      </td>
      <td align=right > 1.00673      </td>
      <td align=right > 1.02377      </td>
      <td align=right > 0.999999      </td>
    </tr>
    <tr> 
      <td align=right > 1      </td>
      <td align=right > 0      </td>
      <td align=right > 1      </td>
      <td align=right > 0      </td>
      <td align=right > 1      </td>
      <td align=right > 1      </td>
      <td align=right > 1      </td>
    </tr>
  </tbody>
</table>
<BR><HR>
<ADDRESS>
genick
2007-11-14
</ADDRESS>
        </tr>
    </tbody>
    </table>
    <? include("aboutPottoProject.php");  ?>
    </div>
    </td>
    </tr>
    </tbody>
</table>
<? include("bottom.php"); ?>

