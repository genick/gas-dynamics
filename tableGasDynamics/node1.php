<? include("header.php");?>
<table style="width:100%; height:993px;" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
	<? include("left_link.php");?>
	<td style="width: 80%; vertical-align: top;">
	<div  font-family: "arial", "helvetica", "lucida", "sans";>

<!--Navigation Panel-->
<A NAME="tex2html60"
  HREF="node2.php">
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next"
 SRC="figures/next.png"></A> 
<A NAME="tex2html58"
  HREF="tableGasDynamics.php">
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up"
 SRC="figures/up.png"></A> 
<A NAME="tex2html52"
  HREF="tableGasDynamics.php">
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous"
 SRC="figures/prev.png"></A>   
<BR>
<B> Next:</B> <A NAME="tex2html61"
  HREF="node2.php">Isentropic Flow</A>
<B> Up:</B> <A NAME="tex2html59"
  HREF="tableGasDynamics.php">tableGasDynamics</A>
<B> Previous:</B> <A NAME="tex2html53"
  HREF="tableGasDynamics.php">tableGasDynamics</A>
<BR>
<BR>
<!--End of Navigation Panel-->
<BR>

<H2><A NAME="SECTION00010000000000000000">
Contents</A>
</H2>
<!--Table of Contents-->

<UL>
<LI><A NAME="tex2html62"
  HREF="node2.php">Isentropic Flow</A>
<UL>
<LI><A NAME="tex2html63"
  HREF="node3.php">Regular Isentropic Flow Tables</A>
<LI><A NAME="tex2html64"
  HREF="node8.php">``Naughty Professor'' Tables (NPT) for Isentropic Flow</A>
</UL>
<BR>
<LI><A NAME="tex2html65"
  HREF="node13.php">Isothermal Nozzle </A>
<UL>
<LI><A NAME="tex2html66"
  HREF="node14.php">Isothermal Nozzle for k=1.2</A>
<LI><A NAME="tex2html67"
  HREF="node15.php">Isothermal Nozzle for k=1.3</A>
<LI><A NAME="tex2html68"
  HREF="node16.php">Isothermal Nozzle for k=1.4</A>
<LI><A NAME="tex2html69"
  HREF="node17.php">Isothermal Nozzle for k=1.67</A>
</UL>
<BR>
<LI><A NAME="tex2html70"
  HREF="node18.php">Normal Shock </A>
<UL>
<LI><A NAME="tex2html71"
  HREF="node19.php">Normal Shock Standard tables</A>
<LI><A NAME="tex2html72"
  HREF="node24.php">Open Valve maximum values</A>
<LI><A NAME="tex2html73"
  HREF="node25.php">Shock Reflecting from a Suddenly Closed Valve</A>
<LI><A NAME="tex2html74"
  HREF="node30.php">Shock propagating from suddenly open valve</A>
</UL>
<BR>
<LI><A NAME="tex2html75"
  HREF="node35.php">Isothermal Flow</A>
<LI><A NAME="tex2html76"
  HREF="node36.php">Fanno Flow</A>
<LI><A NAME="tex2html77"
  HREF="node37.php">Rayleigh Flow </A>
<LI><A NAME="tex2html78"
  HREF="node38.php">Prandtl-Meyer Function</A>
</UL>
<!--End of Table of Contents-->
<P>
<BR><HR>
<ADDRESS>
genick
2007-11-14
</ADDRESS>
        </tr>
    </tbody>
    </table>
    <? include("aboutPottoProject.php");  ?>
    </div>
    </td>
    </tr>
    </tbody>
</table>
<? include("bottom.php"); ?>

