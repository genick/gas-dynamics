<? include("header.php");?>
<table style="width:100%; height:993px;" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
	<? include("left_link.php");?>
	<td style="width: 80%; vertical-align: top;">
	<div  font-family: "arial", "helvetica", "lucida", "sans";>

<!--Navigation Panel-->
<A NAME="tex2html11"
  HREF="node1.php">
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next"
 SRC="figures/next.png"></A> 
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up"
 SRC="figures/up_g.png"> 
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous"
 SRC="figures/prev_g.png"> 
<A NAME="tex2html9"
  HREF="node1.php">
<IMG WIDTH="65" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="contents"
 SRC="figures/contents.png"></A>  
<BR>
<B> Next:</B> <A NAME="tex2html12"
  HREF="node1.php">Contents</A>
 &nbsp; <B>  <A NAME="tex2html10"
  HREF="node1.php">Contents</A></B> 
<BR>
<BR>
<!--End of Navigation Panel-->

<P>
<DIV ALIGN="CENTER">
<FONT SIZE="+3">Gas Dynamics Tables</FONT>
<BR>Genick Bar-Meir</FONT>
<BR>
November 14, 2007
<BR>
Version 1.2

</DIV>

<P>

<H3>Abstract:</H3>
<DIV>
This document is collections of the tables
made in the book ``Fundamentals of compressible Flow
Mechanics.'' 
There also additional tables that for various values of specific
heat, <!-- MATH
 $k =1.3, 1.67$
 -->
<IMG
 WIDTH="106" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="img2.png"
 ALT="$ k =1.3, 1.67$">
 etc.
Furthermore, the table are more detailed than those in the book.
This summary supposed to be used by professionals and students
who would like to have a handy tables summary without
going through the pages of the whole books.</DIV>
<P>

<P>

<title>Gas Dynamics Tables Compressible Flow Tables and Free
Content Textbook Book</title>
<b ="">Go to Potto <a href="../index.php">home</a>.
</b>
<center>
<font color="red">
<A NAME="pdfVersion" HREF="../text.pdf"> To download the pdf
version the book ``Fundamentals of Compressible Flow Mechanics'' 
</a>
</font>
</center>
<BR>
<BR>
These Tables were created by Potto-GDC and later where
translated by using latex2html versions 1.7.
And then they were translated from.php to php by toPHP script. 
If you intersed in this php scrip drop me a line.

<P>
<BR><HR>
<!--Table of Child-Links-->
<A NAME="CHILD_LINKS"></A>

<UL>
<LI><A NAME="tex2html13"
  HREF="node1.php">Contents</A>
<LI><A NAME="tex2html14"
  HREF="node2.php">Isentropic Flow</A>
<UL>
<LI><A NAME="tex2html15"
  HREF="node3.php">Regular Isentropic Flow Tables</A>
<UL>
<LI><A NAME="tex2html16"
  HREF="node4.php">Isentropic Flow for k=1.2</A>
<LI><A NAME="tex2html17"
  HREF="node5.php">Isentropic Flow for k=1.3</A>
<LI><A NAME="tex2html18"
  HREF="node6.php">Isentropic Flow for k =1.4</A>
<LI><A NAME="tex2html19"
  HREF="node7.php">Isentropic Flow k=1.67</A>
</UL>
<LI><A NAME="tex2html20"
  HREF="node8.php">``Naughty Professor'' Tables (NPT) for Isentropic Flow</A>
<UL>
<LI><A NAME="tex2html21"
  HREF="node9.php">NPT for Isnetropic Flow k=1.2</A>
<LI><A NAME="tex2html22"
  HREF="node10.php">NPT for Isentropic Flow k=1.3</A>
<LI><A NAME="tex2html23"
  HREF="node11.php">NPT for Isentropic Flow k=1.4</A>
<LI><A NAME="tex2html24"
  HREF="node12.php">NPT for Isentropic Flow k=1.67</A>
</UL>
</UL>
<BR>
<LI><A NAME="tex2html25"
  HREF="node13.php">Isothermal Nozzle </A>
<UL>
<LI><A NAME="tex2html26"
  HREF="node14.php">Isothermal Nozzle for k=1.2</A>
<LI><A NAME="tex2html27"
  HREF="node15.php">Isothermal Nozzle for k=1.3</A>
<LI><A NAME="tex2html28"
  HREF="node16.php">Isothermal Nozzle for k=1.4</A>
<LI><A NAME="tex2html29"
  HREF="node17.php">Isothermal Nozzle for k=1.67</A>
</UL>
<BR>
<LI><A NAME="tex2html30"
  HREF="node18.php">Normal Shock </A>
<UL>
<LI><A NAME="tex2html31"
  HREF="node19.php">Normal Shock Standard tables</A>
<UL>
<LI><A NAME="tex2html32"
  HREF="node20.php">Normal Shock Standard Table for k=1.2</A>
<LI><A NAME="tex2html33"
  HREF="node21.php">Normal Shock Standard Table for k=1.3</A>
<LI><A NAME="tex2html34"
  HREF="node22.php">Normal Shock Standard Table for k=1.4</A>
<LI><A NAME="tex2html35"
  HREF="node23.php">Normal shock Standard Table for k=1.67</A>
</UL>
<LI><A NAME="tex2html36"
  HREF="node24.php">Open Valve maximum values</A>
<LI><A NAME="tex2html37"
  HREF="node25.php">Shock Reflecting from a Suddenly Closed Valve</A>
<UL>
<LI><A NAME="tex2html38"
  HREF="node26.php">Reflecting Shock (closed Valve) k =1.2</A>
<LI><A NAME="tex2html39"
  HREF="node27.php">Reflecting Shock (closed Valve) k =1.3</A>
<LI><A NAME="tex2html40"
  HREF="node28.php">Reflecting Shock (closed Valve) k =1.4</A>
<LI><A NAME="tex2html41"
  HREF="node29.php">Reflecting Shock (closed Valve) k =1.67</A>
</UL>
<LI><A NAME="tex2html42"
  HREF="node30.php">Shock propagating from suddenly open valve</A>
<UL>
<LI><A NAME="tex2html43"
  HREF="node31.php">Shock from suddenly open valve k =1.2</A>
<LI><A NAME="tex2html44"
  HREF="node32.php">Shock from suddenly open valve k =1.3</A>
<LI><A NAME="tex2html45"
  HREF="node33.php">Shock from suddenly open valve k =1.4</A>
<LI><A NAME="tex2html46"
  HREF="node34.php">Shock from suddenly open valve k =1.67</A>
</UL>
</UL>
<BR>
<LI><A NAME="tex2html47"
  HREF="node35.php">Isothermal Flow</A>
<LI><A NAME="tex2html48"
  HREF="node36.php">Fanno Flow</A>
<LI><A NAME="tex2html49"
  HREF="node37.php">Rayleigh Flow </A>
<LI><A NAME="tex2html50"
  HREF="node38.php">Prandtl-Meyer Function</A>
<LI><A NAME="tex2html51"
  HREF="node39.php">About this document ...</A>
</UL>
<!--End of Table of Child-Links-->
<BR><HR>
<ADDRESS>
genick
2007-11-14
</ADDRESS>
        </tr>
    </tbody>
    </table>
    <? include("aboutPottoProject.php");  ?>
    </div>
    </td>
    </tr>
    </tbody>
</table>
<? include("bottom.php"); ?>

