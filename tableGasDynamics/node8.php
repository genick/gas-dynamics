<? include("./header.php");?>
<table style="width:100%; height:993px;" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
	<? include("left_link.php");?>
	<td style="width: 80%; vertical-align: top;">
	<div  font-family: "arial", "helvetica", "lucida", "sans";>

<!--Navigation Panel-->
<A NAME="tex2html171"
  HREF="node9.php">
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next"
 SRC="figures/next.png"></A> 
<A NAME="tex2html167"
  HREF="node2.php">
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up"
 SRC="figures/up.png"></A> 
<A NAME="tex2html163"
  HREF="node7.php">
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous"
 SRC="figures/prev.png"></A> 
<A NAME="tex2html169"
  HREF="node1.php">
<IMG WIDTH="65" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="contents"
 SRC="figures/contents.png"></A>  
<BR>
<B> Next:</B> <A NAME="tex2html172"
  HREF="node9.php">NPT for Isnetropic Flow</A>
<B> Up:</B> <A NAME="tex2html168"
  HREF="node2.php">Isentropic Flow</A>
<B> Previous:</B> <A NAME="tex2html164"
  HREF="node7.php">Isentropic Flow k=1.67</A>
 &nbsp; <B>  <A NAME="tex2html170"
  HREF="node1.php">Contents</A></B> 
<BR>
<BR>
<!--End of Navigation Panel-->

<H2><A NAME="SECTION00022000000000000000">
``Naughty Professor'' Tables (NPT) for Isentropic Flow</A>
</H2> 
<BR><HR>
<!--Table of Child-Links-->
<A NAME="CHILD_LINKS"><STRONG>Subsections</STRONG></A>

<UL>
<LI><A NAME="tex2html173"
  HREF="node9.php">NPT for Isnetropic Flow k=1.2</A>
<LI><A NAME="tex2html174"
  HREF="node10.php">NPT for Isentropic Flow k=1.3</A>
<LI><A NAME="tex2html175"
  HREF="node11.php">NPT for Isentropic Flow k=1.4</A>
<LI><A NAME="tex2html176"
  HREF="node12.php">NPT for Isentropic Flow k=1.67</A>
</UL>
<!--End of Table of Child-Links-->
<BR><HR>
<ADDRESS>
genick
2007-11-14
</ADDRESS>
        </tr>
    </tbody>
    </table>
    <? include("aboutPottoProject.php");  ?>
    </div>
    </td>
    </tr>
    </tbody>
</table>
<? include("bottom.php"); ?>

