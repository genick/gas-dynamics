<? include("header.php");?>
<table style="width:100%; height:993px;" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
	<? include("left_link.php");?>
	<td style="width: 80%; vertical-align: top;">
	<div  font-family: "arial", "helvetica", "lucida", "sans";>

<!--Navigation Panel-->
<A NAME="tex2html397"
  HREF="node26.php">
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next"
 SRC="figures/next.png"></A> 
<A NAME="tex2html393"
  HREF="node18.php">
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up"
 SRC="figures/up.png"></A> 
<A NAME="tex2html387"
  HREF="node24.php">
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous"
 SRC="figures/prev.png"></A> 
<A NAME="tex2html395"
  HREF="node1.php">
<IMG WIDTH="65" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="contents"
 SRC="figures/contents.png"></A>  
<BR>
<B> Next:</B> <A NAME="tex2html398"
  HREF="node26.php">Reflecting Shock (closed Valve)</A>
<B> Up:</B> <A NAME="tex2html394"
  HREF="node18.php">Normal Shock</A>
<B> Previous:</B> <A NAME="tex2html388"
  HREF="node24.php">Open Valve maximum values</A>
 &nbsp; <B>  <A NAME="tex2html396"
  HREF="node1.php">Contents</A></B> 
<BR>
<BR>
<!--End of Navigation Panel-->

<H2><A NAME="SECTION00043000000000000000">
Shock Reflecting from a Suddenly Closed Valve</A>
</H2>
<BR><HR>
<!--Table of Child-Links-->
<A NAME="CHILD_LINKS"><STRONG>Subsections</STRONG></A>

<UL>
<LI><A NAME="tex2html399"
  HREF="node26.php">Reflecting Shock (closed Valve) k =1.2</A>
<LI><A NAME="tex2html400"
  HREF="node27.php">Reflecting Shock (closed Valve) k =1.3</A>
<LI><A NAME="tex2html401"
  HREF="node28.php">Reflecting Shock (closed Valve) k =1.4</A>
<LI><A NAME="tex2html402"
  HREF="node29.php">Reflecting Shock (closed Valve) k =1.67</A>
</UL>
<!--End of Table of Child-Links-->
<BR><HR>
<ADDRESS>
genick
2007-11-14
</ADDRESS>
        </tr>
    </tbody>
    </table>
    <? include("aboutPottoProject.php");  ?>
    </div>
    </td>
    </tr>
    </tbody>
</table>
<? include("bottom.php"); ?>

