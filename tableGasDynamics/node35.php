<? include("header.php");?>
<table style="width:100%; height:993px;" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
	<? include("left_link.php");?>
	<td style="width: 80%; vertical-align: top;">
	<div  font-family: "arial", "helvetica", "lucida", "sans";>

<!--Navigation Panel-->
<A NAME="tex2html519"
  HREF="node36.php">
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next"
 SRC="figures/next.png"></A> 
<A NAME="tex2html515"
  HREF="tableGasDynamics.php">
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up"
 SRC="figures/up.png"></A> 
<A NAME="tex2html509"
  HREF="node34.php">
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous"
 SRC="figures/prev.png"></A> 
<A NAME="tex2html517"
  HREF="node1.php">
<IMG WIDTH="65" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="contents"
 SRC="figures/contents.png"></A>  
<BR>
<B> Next:</B> <A NAME="tex2html520"
  HREF="node36.php">Fanno Flow</A>
<B> Up:</B> <A NAME="tex2html516"
  HREF="tableGasDynamics.php">tableGasDynamics</A>
<B> Previous:</B> <A NAME="tex2html510"
  HREF="node34.php">Shock from suddenly open</A>
 &nbsp; <B>  <A NAME="tex2html518"
  HREF="node1.php">Contents</A></B> 
<BR>
<BR>
<!--End of Navigation Panel-->

<H1><A NAME="SECTION00050000000000000000"></A>
<A NAME="chap:Isothermal"></A>
<BR>
Isothermal Flow
</H1>

<P>

<A NAME="323"></A>
<TABLE CELLPADDING=3 BORDER="1">
<CAPTION><STRONG>Table 26:</STRONG>
 The Isothermal Flow basic parameters  </CAPTION>
<TR><TD ALIGN="CENTER" COLSPAN=1><!-- MATH
 $\rule[-.1in]{0.pt}{0.3 in}\mathbf{M}$
 -->
<IMG
 WIDTH="25" HEIGHT="56" ALIGN="MIDDLE" BORDER="0"
 SRC="img3.png"
 ALT="$ \rule[-.1in]{0.pt}{0.3 in}\mathbf{M} $">
</TD>
<TD ALIGN="CENTER" COLSPAN=1><!-- MATH
 $\mathbf{4fL \over D}$
 -->
<IMG
 WIDTH="30" HEIGHT="43" ALIGN="MIDDLE" BORDER="0"
 SRC="img20.png"
 ALT="$ \mathbf{4fL \over D} $">
</TD>
<TD ALIGN="CENTER" COLSPAN=1><!-- MATH
 $\mathbf{P \over P^{*}}$
 -->
<IMG
 WIDTH="26" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="img21.png"
 ALT="$ \mathbf{P \over P^{*}} $">
</TD>
<TD ALIGN="CENTER" COLSPAN=1><!-- MATH
 $\mathbf{P_0 \over {P_0}^{*}}$
 -->
<IMG
 WIDTH="33" HEIGHT="43" ALIGN="MIDDLE" BORDER="0"
 SRC="img22.png"
 ALT="$ \mathbf{P_0 \over {P_0}^{*}} $">
</TD>
<TD ALIGN="CENTER" COLSPAN=1><!-- MATH
 $\mathbf{\rho \over \rho^{*}}$
 -->
<IMG
 WIDTH="22" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="img23.png"
 ALT="$ \mathbf{\rho \over \rho^{*}} $">
</TD>
<TD ALIGN="CENTER" COLSPAN=1><!-- MATH
 $\mathbf{T_0 \over {T_0}^{*}}$
 -->
<IMG
 WIDTH="34" HEIGHT="43" ALIGN="MIDDLE" BORDER="0"
 SRC="img24.png"
 ALT="$ \mathbf{T_0 \over {T_0}^{*}} $">
</TD>
</TR>
<TR><TD ALIGN="LEFT">0.03000</TD>
<TD ALIGN="LEFT">785.97</TD>
<TD ALIGN="LEFT">28.1718</TD>
<TD ALIGN="LEFT">17.6651</TD>
<TD ALIGN="LEFT">28.1718</TD>
<TD ALIGN="LEFT">0.87516</TD>
</TR>
<TR><TD ALIGN="LEFT">0.04000</TD>
<TD ALIGN="LEFT">439.33</TD>
<TD ALIGN="LEFT">21.1289</TD>
<TD ALIGN="LEFT">13.2553</TD>
<TD ALIGN="LEFT">21.1289</TD>
<TD ALIGN="LEFT">0.87528</TD>
</TR>
<TR><TD ALIGN="LEFT">0.05000</TD>
<TD ALIGN="LEFT">279.06</TD>
<TD ALIGN="LEFT">16.9031</TD>
<TD ALIGN="LEFT">10.6109</TD>
<TD ALIGN="LEFT">16.9031</TD>
<TD ALIGN="LEFT">0.87544</TD>
</TR>
<TR><TD ALIGN="LEFT">0.06000</TD>
<TD ALIGN="LEFT">192.12</TD>
<TD ALIGN="LEFT">14.0859</TD>
<TD ALIGN="LEFT">8.8493</TD>
<TD ALIGN="LEFT">14.0859</TD>
<TD ALIGN="LEFT">0.87563</TD>
</TR>
<TR><TD ALIGN="LEFT">0.07000</TD>
<TD ALIGN="LEFT">139.79</TD>
<TD ALIGN="LEFT">12.0736</TD>
<TD ALIGN="LEFT">7.5920</TD>
<TD ALIGN="LEFT">12.0736</TD>
<TD ALIGN="LEFT">0.87586</TD>
</TR>
<TR><TD ALIGN="LEFT">0.08000</TD>
<TD ALIGN="LEFT">105.89</TD>
<TD ALIGN="LEFT">10.5644</TD>
<TD ALIGN="LEFT">6.6500</TD>
<TD ALIGN="LEFT">10.5644</TD>
<TD ALIGN="LEFT">0.87612</TD>
</TR>
<TR><TD ALIGN="LEFT">0.09000</TD>
<TD ALIGN="LEFT">82.7040</TD>
<TD ALIGN="LEFT">9.3906</TD>
<TD ALIGN="LEFT">5.9181</TD>
<TD ALIGN="LEFT">9.3906</TD>
<TD ALIGN="LEFT">0.87642</TD>
</TR>
<TR><TD ALIGN="LEFT">0.10000</TD>
<TD ALIGN="LEFT">66.1599</TD>
<TD ALIGN="LEFT">8.4515</TD>
<TD ALIGN="LEFT">5.3334</TD>
<TD ALIGN="LEFT">8.4515</TD>
<TD ALIGN="LEFT">0.87675</TD>
</TR>
<TR><TD ALIGN="LEFT">0.20000</TD>
<TD ALIGN="LEFT">13.9747</TD>
<TD ALIGN="LEFT">4.2258</TD>
<TD ALIGN="LEFT">2.7230</TD>
<TD ALIGN="LEFT">4.2258</TD>
<TD ALIGN="LEFT">0.88200</TD>
</TR>
<TR><TD ALIGN="LEFT">0.25000</TD>
<TD ALIGN="LEFT">7.9925</TD>
<TD ALIGN="LEFT">3.3806</TD>
<TD ALIGN="LEFT">2.2126</TD>
<TD ALIGN="LEFT">3.3806</TD>
<TD ALIGN="LEFT">0.88594</TD>
</TR>
<TR><TD ALIGN="LEFT">0.30000</TD>
<TD ALIGN="LEFT">4.8650</TD>
<TD ALIGN="LEFT">2.8172</TD>
<TD ALIGN="LEFT">1.8791</TD>
<TD ALIGN="LEFT">2.8172</TD>
<TD ALIGN="LEFT">0.89075</TD>
</TR>
<TR><TD ALIGN="LEFT">0.35000</TD>
<TD ALIGN="LEFT">3.0677</TD>
<TD ALIGN="LEFT">2.4147</TD>
<TD ALIGN="LEFT">1.6470</TD>
<TD ALIGN="LEFT">2.4147</TD>
<TD ALIGN="LEFT">0.89644</TD>
</TR>
<TR><TD ALIGN="LEFT">0.40000</TD>
<TD ALIGN="LEFT">1.9682</TD>
<TD ALIGN="LEFT">2.1129</TD>
<TD ALIGN="LEFT">1.4784</TD>
<TD ALIGN="LEFT">2.1129</TD>
<TD ALIGN="LEFT">0.90300</TD>
</TR>
<TR><TD ALIGN="LEFT">0.45000</TD>
<TD ALIGN="LEFT">1.2668</TD>
<TD ALIGN="LEFT">1.8781</TD>
<TD ALIGN="LEFT">1.3524</TD>
<TD ALIGN="LEFT">1.8781</TD>
<TD ALIGN="LEFT">0.91044</TD>
</TR>
<TR><TD ALIGN="LEFT">0.50000</TD>
<TD ALIGN="LEFT">0.80732</TD>
<TD ALIGN="LEFT">1.6903</TD>
<TD ALIGN="LEFT">1.2565</TD>
<TD ALIGN="LEFT">1.6903</TD>
<TD ALIGN="LEFT">0.91875</TD>
</TR>
<TR><TD ALIGN="LEFT">0.55000</TD>
<TD ALIGN="LEFT">0.50207</TD>
<TD ALIGN="LEFT">1.5366</TD>
<TD ALIGN="LEFT">1.1827</TD>
<TD ALIGN="LEFT">1.5366</TD>
<TD ALIGN="LEFT">0.92794</TD>
</TR>
<TR><TD ALIGN="LEFT">0.60000</TD>
<TD ALIGN="LEFT">0.29895</TD>
<TD ALIGN="LEFT">1.4086</TD>
<TD ALIGN="LEFT">1.1259</TD>
<TD ALIGN="LEFT">1.4086</TD>
<TD ALIGN="LEFT">0.93800</TD>
</TR>
<TR><TD ALIGN="LEFT">0.65000</TD>
<TD ALIGN="LEFT">0.16552</TD>
<TD ALIGN="LEFT">1.3002</TD>
<TD ALIGN="LEFT">1.0823</TD>
<TD ALIGN="LEFT">1.3002</TD>
<TD ALIGN="LEFT">0.94894</TD>
</TR>
<TR><TD ALIGN="LEFT">0.70000</TD>
<TD ALIGN="LEFT">0.08085</TD>
<TD ALIGN="LEFT">1.2074</TD>
<TD ALIGN="LEFT">1.0495</TD>
<TD ALIGN="LEFT">1.2074</TD>
<TD ALIGN="LEFT">0.96075</TD>
</TR>
<TR><TD ALIGN="LEFT">0.75000</TD>
<TD ALIGN="LEFT">0.03095</TD>
<TD ALIGN="LEFT">1.1269</TD>
<TD ALIGN="LEFT">1.0255</TD>
<TD ALIGN="LEFT">1.1269</TD>
<TD ALIGN="LEFT">0.97344</TD>
</TR>
<TR><TD ALIGN="LEFT">0.80000</TD>
<TD ALIGN="LEFT">0.00626</TD>
<TD ALIGN="LEFT">1.056</TD>
<TD ALIGN="LEFT">1.009</TD>
<TD ALIGN="LEFT">1.056</TD>
<TD ALIGN="LEFT">0.98700</TD>
</TR>
<TR><TD ALIGN="LEFT">0.81000</TD>
<TD ALIGN="LEFT">0.00371</TD>
<TD ALIGN="LEFT">1.043</TD>
<TD ALIGN="LEFT">1.007</TD>
<TD ALIGN="LEFT">1.043</TD>
<TD ALIGN="LEFT">0.98982</TD>
</TR>
<TR><TD ALIGN="LEFT">0.81879</TD>
<TD ALIGN="LEFT">0.00205</TD>
<TD ALIGN="LEFT">1.032</TD>
<TD ALIGN="LEFT">1.005</TD>
<TD ALIGN="LEFT">1.032</TD>
<TD ALIGN="LEFT">0.99232</TD>
</TR>
<TR><TD ALIGN="LEFT">0.82758</TD>
<TD ALIGN="LEFT">0.000896</TD>
<TD ALIGN="LEFT">1.021</TD>
<TD ALIGN="LEFT">1.003</TD>
<TD ALIGN="LEFT">1.021</TD>
<TD ALIGN="LEFT">0.99485</TD>
</TR>
<TR><TD ALIGN="LEFT">0.83637</TD>
<TD ALIGN="LEFT">0.000220</TD>
<TD ALIGN="LEFT">1.011</TD>
<TD ALIGN="LEFT">1.001</TD>
<TD ALIGN="LEFT">1.011</TD>
<TD ALIGN="LEFT">0.99741</TD>
</TR>
<TR><TD ALIGN="LEFT">0.84515</TD>
<TD ALIGN="LEFT">0.0</TD>
<TD ALIGN="LEFT">1.000</TD>
<TD ALIGN="LEFT">1.000</TD>
<TD ALIGN="LEFT">1.000</TD>
<TD ALIGN="LEFT">1.000</TD>
</TR>
</TABLE>
<PRE>
To produce this table rum program with:\\
variableName =  machV;
int isRange = yes;
whatInfo = infoStandard ;
</PRE>

<P>
<A NAME="330"></A>
<TABLE CELLPADDING=3 BORDER="1">
<CAPTION><STRONG>Table 27:</STRONG>
 The flow parameters for unchoked flow  
</CAPTION>
<TR><TD ALIGN="CENTER" COLSPAN=1><!-- MATH
 $\rule[-.1in]{0.pt}{0.3 in} \mathbf{M_1}$
 -->
<IMG
 WIDTH="34" HEIGHT="56" ALIGN="MIDDLE" BORDER="0"
 SRC="img25.png"
 ALT="$ \rule[-.1in]{0.pt}{0.3 in} \mathbf{M_1} $">
</TD>
<TD ALIGN="CENTER" COLSPAN=1><!-- MATH
 $\mathbf{M_2}$
 -->
<IMG
 WIDTH="34" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="img26.png"
 ALT="$ \mathbf{M_2} $">
</TD>
<TD ALIGN="CENTER" COLSPAN=1><!-- MATH
 $\mathbf{\left.{4fL_{max} \over D }\right|_{1} }$
 -->
<IMG
 WIDTH="72" HEIGHT="43" ALIGN="MIDDLE" BORDER="0"
 SRC="img27.png"
 ALT="$ \mathbf{\left.{4fL_{max} \over D }\right\vert _{1} } $">
</TD>
<TD ALIGN="CENTER" COLSPAN=1><!-- MATH
 $\mathbf{4fL \over D }$
 -->
<IMG
 WIDTH="30" HEIGHT="43" ALIGN="MIDDLE" BORDER="0"
 SRC="img20.png"
 ALT="$ \mathbf{4fL \over D} $">
</TD>
<TD ALIGN="CENTER" COLSPAN=1><!-- MATH
 $\mathbf{P_2 \over P_1}$
 -->
<IMG
 WIDTH="27" HEIGHT="43" ALIGN="MIDDLE" BORDER="0"
 SRC="img28.png"
 ALT="$ \mathbf{P_2 \over P_1} $">
</TD>
</TR>
<TR><TD ALIGN="LEFT">0.7272</TD>
<TD ALIGN="LEFT">0.84095</TD>
<TD ALIGN="LEFT">0.05005</TD>
<TD ALIGN="LEFT">0.05000</TD>
<TD ALIGN="LEFT">0.10000</TD>
</TR>
<TR><TD ALIGN="LEFT">0.6934</TD>
<TD ALIGN="LEFT">0.83997</TD>
<TD ALIGN="LEFT">0.08978</TD>
<TD ALIGN="LEFT">0.08971</TD>
<TD ALIGN="LEFT">0.10000</TD>
</TR>
<TR><TD ALIGN="LEFT">0.6684</TD>
<TD ALIGN="LEFT">0.84018</TD>
<TD ALIGN="LEFT">0.12949</TD>
<TD ALIGN="LEFT">0.12942</TD>
<TD ALIGN="LEFT">0.10000</TD>
</TR>
<TR><TD ALIGN="LEFT">0.6483</TD>
<TD ALIGN="LEFT">0.83920</TD>
<TD ALIGN="LEFT">0.16922</TD>
<TD ALIGN="LEFT">0.16912</TD>
<TD ALIGN="LEFT">0.10000</TD>
</TR>
<TR><TD ALIGN="LEFT">0.5914</TD>
<TD ALIGN="LEFT">0.83889</TD>
<TD ALIGN="LEFT">0.32807</TD>
<TD ALIGN="LEFT">0.32795</TD>
<TD ALIGN="LEFT">0.10000</TD>
</TR>
<TR><TD ALIGN="LEFT">0.5807</TD>
<TD ALIGN="LEFT">0.83827</TD>
<TD ALIGN="LEFT">0.36780</TD>
<TD ALIGN="LEFT">0.36766</TD>
<TD ALIGN="LEFT">0.10000</TD>
</TR>
<TR><TD ALIGN="LEFT">0.5708</TD>
<TD ALIGN="LEFT">0.83740</TD>
<TD ALIGN="LEFT">0.40754</TD>
<TD ALIGN="LEFT">0.40737</TD>
<TD ALIGN="LEFT">0.10000</TD>
</TR>
</TABLE>

<P>
<HR>
<!--Navigation Panel-->
<A NAME="tex2html519"
  HREF="node36.php">
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next"
 SRC="figures/next.png"></A> 
<A NAME="tex2html515"
  HREF="tableGasDynamics.php">
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up"
 SRC="figures/up.png"></A> 
<A NAME="tex2html509"
  HREF="node34.php">
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous"
 SRC="figures/prev.png"></A> 
<A NAME="tex2html517"
  HREF="node1.php">
<IMG WIDTH="65" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="contents"
 SRC="figures/contents.png"></A>  
<BR>
<B> Next:</B> <A NAME="tex2html520"
  HREF="node36.php">Fanno Flow</A>
<B> Up:</B> <A NAME="tex2html516"
  HREF="tableGasDynamics.php">tableGasDynamics</A>
<B> Previous:</B> <A NAME="tex2html510"
  HREF="node34.php">Shock from suddenly open</A>
 &nbsp; <B>  <A NAME="tex2html518"
  HREF="node1.php">Contents</A></B> 
<!--End of Navigation Panel-->
<ADDRESS>
genick
2007-11-14
</ADDRESS>
        </tr>
    </tbody>
    </table>
    <? include("aboutPottoProject.php");  ?>
    </div>
    </td>
    </tr>
    </tbody>
</table>
<? include("bottom.php"); ?>

