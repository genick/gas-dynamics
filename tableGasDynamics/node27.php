<? include("header.php");?>
<table style="width:100%; height:993px;" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
	<? include("left_link.php");?>
	<td style="width: 80%; vertical-align: top;">
	<div  font-family: "arial", "helvetica", "lucida", "sans";>

<!--Navigation Panel-->
<A NAME="tex2html425"
  HREF="node28.php">
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next"
 SRC="figures/next.png"></A> 
<A NAME="tex2html421"
  HREF="node25.php">
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up"
 SRC="figures/up.png"></A> 
<A NAME="tex2html415"
  HREF="node26.php">
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous"
 SRC="figures/prev.png"></A> 
<A NAME="tex2html423"
  HREF="node1.php">
<IMG WIDTH="65" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="contents"
 SRC="figures/contents.png"></A>  
<BR>
<B> Next:</B> <A NAME="tex2html426"
  HREF="node28.php">Reflecting Shock (closed Valve)</A>
<B> Up:</B> <A NAME="tex2html422"
  HREF="node25.php">Shock Reflecting from a</A>
<B> Previous:</B> <A NAME="tex2html416"
  HREF="node26.php">Reflecting Shock (closed Valve)</A>
 &nbsp; <B>  <A NAME="tex2html424"
  HREF="node1.php">Contents</A></B> 
<BR>
<BR>
<!--End of Navigation Panel-->

<H3><A NAME="SECTION00043200000000000000">
Reflecting Shock (closed Valve) k =1.3</A>
</H3>

<P>

<table border=1 width="100%" >
  <thead>
    <tr>
      <th align=left bgcolor="#fffeaa" colspan=3 >Shock Dynamics </th>
      <th align=left  bgcolor="#00ff5a" colspan=3  rowspan=2 >Input: Mx&prime; </th>
      <th align=left  bgcolor="#9ae0ee" colspan=1 rowspan=2 >k = 1.3 </th>
    </tr>
    <tr>
      <th align=left bgcolor="#fffeaa" colspan=3 > </th>
    </tr>
    <tr>
      <th align=center >Mx  </th>
      <th align=center >Mx&prime; </th>
      <th align=center >My </th>
      <th align=center >My&prime; </th>
      <th align=center >Ty/Tx </th>
      <th align=center >Py/Px </th>
      <th align=center >P0y/P0x </th>
    </tr> 
  </thead>
  <tbody>
    <tr> 
      <td align=right > 1.00577      </td>
      <td align=right > 0.01      </td>
      <td align=right > 0.994276      </td>
      <td align=right > 0      </td>
      <td align=right > 1.003      </td>
      <td align=right > 1.01307      </td>
      <td align=right > 1      </td>
    </tr>
    <tr> 
      <td align=right > 1.01157      </td>
      <td align=right > 0.02      </td>
      <td align=right > 0.9886      </td>
      <td align=right > 0      </td>
      <td align=right > 1.00601      </td>
      <td align=right > 1.0263      </td>
      <td align=right > 0.999998      </td>
    </tr>
    <tr> 
      <td align=right > 1.0174      </td>
      <td align=right > 0.03      </td>
      <td align=right > 0.982974      </td>
      <td align=right > 0      </td>
      <td align=right > 1.00902      </td>
      <td align=right > 1.03968      </td>
      <td align=right > 0.999993      </td>
    </tr>
    <tr> 
      <td align=right > 1.02326      </td>
      <td align=right > 0.04      </td>
      <td align=right > 0.977399      </td>
      <td align=right > 0      </td>
      <td align=right > 1.01204      </td>
      <td align=right > 1.05321      </td>
      <td align=right > 0.999984      </td>
    </tr>
    <tr> 
      <td align=right > 1.02916      </td>
      <td align=right > 0.05      </td>
      <td align=right > 0.971872      </td>
      <td align=right > 0      </td>
      <td align=right > 1.01506      </td>
      <td align=right > 1.0669      </td>
      <td align=right > 0.999969      </td>
    </tr>
    <tr> 
      <td align=right > 1.03509      </td>
      <td align=right > 0.06      </td>
      <td align=right > 0.966392      </td>
      <td align=right > 0      </td>
      <td align=right > 1.01809      </td>
      <td align=right > 1.08074      </td>
      <td align=right > 0.999947      </td>
    </tr>
    <tr> 
      <td align=right > 1.04106      </td>
      <td align=right > 0.07      </td>
      <td align=right > 0.960961      </td>
      <td align=right > 0      </td>
      <td align=right > 1.02113      </td>
      <td align=right > 1.09474      </td>
      <td align=right > 0.999916      </td>
    </tr>
    <tr> 
      <td align=right > 1.04706      </td>
      <td align=right > 0.08      </td>
      <td align=right > 0.955579      </td>
      <td align=right > 0      </td>
      <td align=right > 1.02417      </td>
      <td align=right > 1.10889      </td>
      <td align=right > 0.999875      </td>
    </tr>
    <tr> 
      <td align=right > 1.05309      </td>
      <td align=right > 0.09      </td>
      <td align=right > 0.950242      </td>
      <td align=right > 0      </td>
      <td align=right > 1.02722      </td>
      <td align=right > 1.12321      </td>
      <td align=right > 0.999822      </td>
    </tr>
    <tr> 
      <td align=right > 1.05915      </td>
      <td align=right > 0.1      </td>
      <td align=right > 0.944954      </td>
      <td align=right > 0      </td>
      <td align=right > 1.03027      </td>
      <td align=right > 1.13769      </td>
      <td align=right > 0.999757      </td>
    </tr>
    <tr> 
      <td align=right > 1.12159      </td>
      <td align=right > 0.2      </td>
      <td align=right > 0.894582      </td>
      <td align=right > 0      </td>
      <td align=right > 1.0613      </td>
      <td align=right > 1.29161      </td>
      <td align=right > 0.998103      </td>
    </tr>
    <tr> 
      <td align=right > 1.1957      </td>
      <td align=right > 0.3125      </td>
      <td align=right > 0.84308      </td>
      <td align=right > 0      </td>
      <td align=right > 1.09745      </td>
      <td align=right > 1.48575      </td>
      <td align=right > 0.992999      </td>
    </tr>
    <tr> 
      <td align=right > 1.2738      </td>
      <td align=right > 0.425      </td>
      <td align=right > 0.796614      </td>
      <td align=right > 0      </td>
      <td align=right > 1.13532      </td>
      <td align=right > 1.70378      </td>
      <td align=right > 0.983044      </td>
    </tr>
    <tr> 
      <td align=right > 1.35573      </td>
      <td align=right > 0.5375      </td>
      <td align=right > 0.754757      </td>
      <td align=right > 0      </td>
      <td align=right > 1.17528      </td>
      <td align=right > 1.94732      </td>
      <td align=right > 0.967158      </td>
    </tr>
    <tr> 
      <td align=right > 1.44131      </td>
      <td align=right > 0.65      </td>
      <td align=right > 0.717102      </td>
      <td align=right > 0      </td>
      <td align=right > 1.21768      </td>
      <td align=right > 2.21791      </td>
      <td align=right > 0.94471      </td>
    </tr>
    <tr> 
      <td align=right > 1.53033      </td>
      <td align=right > 0.7625      </td>
      <td align=right > 0.683264      </td>
      <td align=right > 0      </td>
      <td align=right > 1.26285      </td>
      <td align=right > 2.51694      </td>
      <td align=right > 0.915543      </td>
    </tr>
    <tr> 
      <td align=right > 1.62256      </td>
      <td align=right > 0.875      </td>
      <td align=right > 0.652877      </td>
      <td align=right > 0      </td>
      <td align=right > 1.31108      </td>
      <td align=right > 2.84566      </td>
      <td align=right > 0.879953      </td>
    </tr>
    <tr> 
      <td align=right > 1.71777      </td>
      <td align=right > 0.9875      </td>
      <td align=right > 0.625603      </td>
      <td align=right > 0      </td>
      <td align=right > 1.36262      </td>
      <td align=right > 3.20519      </td>
      <td align=right > 0.838624      </td>
    </tr>
    <tr> 
      <td align=right > 1.81574      </td>
      <td align=right > 1.1      </td>
      <td align=right > 0.601124      </td>
      <td align=right > 0      </td>
      <td align=right > 1.41769      </td>
      <td align=right > 3.59651      </td>
      <td align=right > 0.792544      </td>
    </tr>
    <tr> 
      <td align=right > 1.90495      </td>
      <td align=right > 1.2      </td>
      <td align=right > 0.581475      </td>
      <td align=right > 0      </td>
      <td align=right > 1.46978      </td>
      <td align=right > 3.97172      </td>
      <td align=right > 0.748543      </td>
    </tr>
    <tr> 
      <td align=right > 1.996      </td>
      <td align=right > 1.3      </td>
      <td align=right > 0.563617      </td>
      <td align=right > 0      </td>
      <td align=right > 1.52494      </td>
      <td align=right > 4.37324      </td>
      <td align=right > 0.702599      </td>
    </tr>
    <tr> 
      <td align=right > 2.08875      </td>
      <td align=right > 1.4      </td>
      <td align=right > 0.547376      </td>
      <td align=right > 0      </td>
      <td align=right > 1.58328      </td>
      <td align=right > 4.80153      </td>
      <td align=right > 0.655592      </td>
    </tr>
    <tr> 
      <td align=right > 2.18307      </td>
      <td align=right > 1.5      </td>
      <td align=right > 0.532596      </td>
      <td align=right > 0      </td>
      <td align=right > 1.64488      </td>
      <td align=right > 5.25699      </td>
      <td align=right > 0.60835      </td>
    </tr>
    <tr> 
      <td align=right > 2.27882      </td>
      <td align=right > 1.6      </td>
      <td align=right > 0.519134      </td>
      <td align=right > 0      </td>
      <td align=right > 1.70983      </td>
      <td align=right > 5.73995      </td>
      <td align=right > 0.561617      </td>
    </tr>
    <tr> 
      <td align=right > 2.37589      </td>
      <td align=right > 1.7      </td>
      <td align=right > 0.50686      </td>
      <td align=right > 0      </td>
      <td align=right > 1.77821      </td>
      <td align=right > 6.25073      </td>
      <td align=right > 0.51603      </td>
    </tr>
    <tr> 
      <td align=right > 2.47418      </td>
      <td align=right > 1.8      </td>
      <td align=right > 0.495656      </td>
      <td align=right > 0      </td>
      <td align=right > 1.85005      </td>
      <td align=right > 6.78957      </td>
      <td align=right > 0.472109      </td>
    </tr>
    <tr> 
      <td align=right > 2.57357      </td>
      <td align=right > 1.9      </td>
      <td align=right > 0.485418      </td>
      <td align=right > 0      </td>
      <td align=right > 1.92543      </td>
      <td align=right > 7.35671      </td>
      <td align=right > 0.430257      </td>
    </tr>
    <tr> 
      <td align=right > 2.67398      </td>
      <td align=right > 2      </td>
      <td align=right > 0.476051      </td>
      <td align=right > 0      </td>
      <td align=right > 2.00438      </td>
      <td align=right > 7.95234      </td>
      <td align=right > 0.39076      </td>
    </tr>
    <tr> 
      <td align=right > 2.77532      </td>
      <td align=right > 2.1      </td>
      <td align=right > 0.467469      </td>
      <td align=right > 0      </td>
      <td align=right > 2.08695      </td>
      <td align=right > 8.57662      </td>
      <td align=right > 0.353804      </td>
    </tr>
    <tr> 
      <td align=right > 2.87752      </td>
      <td align=right > 2.2      </td>
      <td align=right > 0.459596      </td>
      <td align=right > 0      </td>
      <td align=right > 2.17316      </td>
      <td align=right > 9.22971      </td>
      <td align=right > 0.319487      </td>
    </tr>
    <tr> 
      <td align=right > 2.98051      </td>
      <td align=right > 2.3      </td>
      <td align=right > 0.452365      </td>
      <td align=right > 0      </td>
      <td align=right > 2.26305      </td>
      <td align=right > 9.91173      </td>
      <td align=right > 0.287832      </td>
    </tr>
    <tr> 
      <td align=right > 3.08423      </td>
      <td align=right > 2.4      </td>
      <td align=right > 0.445713      </td>
      <td align=right > 0      </td>
      <td align=right > 2.35665      </td>
      <td align=right > 10.6228      </td>
      <td align=right > 0.2588      </td>
    </tr>
    <tr> 
      <td align=right > 3.18862      </td>
      <td align=right > 2.5      </td>
      <td align=right > 0.439585      </td>
      <td align=right > 0      </td>
      <td align=right > 2.45396      </td>
      <td align=right > 11.363      </td>
      <td align=right > 0.232309      </td>
    </tr>
    <tr> 
      <td align=right > 3.29362      </td>
      <td align=right > 2.6      </td>
      <td align=right > 0.433933      </td>
      <td align=right > 0      </td>
      <td align=right > 2.55502      </td>
      <td align=right > 12.1324      </td>
      <td align=right > 0.208243      </td>
    </tr>
    <tr> 
      <td align=right > 3.39919      </td>
      <td align=right > 2.7      </td>
      <td align=right > 0.428712      </td>
      <td align=right > 0      </td>
      <td align=right > 2.65984      </td>
      <td align=right > 12.9311      </td>
      <td align=right > 0.186464      </td>
    </tr>
    <tr> 
      <td align=right > 3.50528      </td>
      <td align=right > 2.8      </td>
      <td align=right > 0.423883      </td>
      <td align=right > 0      </td>
      <td align=right > 2.76844      </td>
      <td align=right > 13.7592      </td>
      <td align=right > 0.166818      </td>
    </tr>
    <tr> 
      <td align=right > 3.61187      </td>
      <td align=right > 2.9      </td>
      <td align=right > 0.419411      </td>
      <td align=right > 0      </td>
      <td align=right > 2.88082      </td>
      <td align=right > 14.6167      </td>
      <td align=right > 0.149146      </td>
    </tr>
  </tbody>
</table>
<BR><HR>
<ADDRESS>
genick
2007-11-14
</ADDRESS>
        </tr>
    </tbody>
    </table>
    <? include("aboutPottoProject.php");  ?>
    </div>
    </td>
    </tr>
    </tbody>
</table>
<? include("bottom.php"); ?>

