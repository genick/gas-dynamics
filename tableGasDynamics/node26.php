<? include("header.php");?>
<table style="width:100%; height:993px;" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
	<? include("left_link.php");?>
	<td style="width: 80%; vertical-align: top;">
	<div  font-family: "arial", "helvetica", "lucida", "sans";>

<!--Navigation Panel-->
<A NAME="tex2html413"
  HREF="node27.php">
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next"
 SRC="figures/next.png"></A> 
<A NAME="tex2html409"
  HREF="node25.php">
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up"
 SRC="figures/up.png"></A> 
<A NAME="tex2html403"
  HREF="node25.php">
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous"
 SRC="figures/prev.png"></A> 
<A NAME="tex2html411"
  HREF="node1.php">
<IMG WIDTH="65" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="contents"
 SRC="figures/contents.png"></A>  
<BR>
<B> Next:</B> <A NAME="tex2html414"
  HREF="node27.php">Reflecting Shock (closed Valve)</A>
<B> Up:</B> <A NAME="tex2html410"
  HREF="node25.php">Shock Reflecting from a</A>
<B> Previous:</B> <A NAME="tex2html404"
  HREF="node25.php">Shock Reflecting from a</A>
 &nbsp; <B>  <A NAME="tex2html412"
  HREF="node1.php">Contents</A></B> 
<BR>
<BR>
<!--End of Navigation Panel-->

<H3><A NAME="SECTION00043100000000000000">
Reflecting Shock (closed Valve) k =1.2</A>
</H3>

<P>

<table border=1 width="100%" >
  <thead>
    <tr>
      <th align=left bgcolor="#fffeaa" colspan=3 >Shock Dynamics </th>
      <th align=left  bgcolor="#00ff5a" colspan=3  rowspan=2 >Input: Mx&prime; </th>
      <th align=left  bgcolor="#9ae0ee" colspan=1 rowspan=2 >k = 1.2 </th>
    </tr>
    <tr>
      <th align=left bgcolor="#fffeaa" colspan=3 > </th>
    </tr>
    <tr>
      <th align=center >Mx  </th>
      <th align=center >Mx&prime; </th>
      <th align=center >My </th>
      <th align=center >My&prime; </th>
      <th align=center >Ty/Tx </th>
      <th align=center >Py/Px </th>
      <th align=center >P0y/P0x </th>
    </tr> 
  </thead>
  <tbody>
    <tr> 
      <td align=right > 1.00552      </td>
      <td align=right > 0.01      </td>
      <td align=right > 0.994521      </td>
      <td align=right > 0      </td>
      <td align=right > 1.002      </td>
      <td align=right > 1.01207      </td>
      <td align=right > 1      </td>
    </tr>
    <tr> 
      <td align=right > 1.01106      </td>
      <td align=right > 0.02      </td>
      <td align=right > 0.989083      </td>
      <td align=right > 0      </td>
      <td align=right > 1.004      </td>
      <td align=right > 1.02427      </td>
      <td align=right > 0.999998      </td>
    </tr>
    <tr> 
      <td align=right > 1.01664      </td>
      <td align=right > 0.03      </td>
      <td align=right > 0.983685      </td>
      <td align=right > 0      </td>
      <td align=right > 1.00601      </td>
      <td align=right > 1.0366      </td>
      <td align=right > 0.999994      </td>
    </tr>
    <tr> 
      <td align=right > 1.02224      </td>
      <td align=right > 0.04      </td>
      <td align=right > 0.978327      </td>
      <td align=right > 0      </td>
      <td align=right > 1.00802      </td>
      <td align=right > 1.04907      </td>
      <td align=right > 0.999986      </td>
    </tr>
    <tr> 
      <td align=right > 1.02788      </td>
      <td align=right > 0.05      </td>
      <td align=right > 0.973012      </td>
      <td align=right > 0      </td>
      <td align=right > 1.01003      </td>
      <td align=right > 1.06167      </td>
      <td align=right > 0.999973      </td>
    </tr>
    <tr> 
      <td align=right > 1.03354      </td>
      <td align=right > 0.06      </td>
      <td align=right > 0.967734      </td>
      <td align=right > 0      </td>
      <td align=right > 1.01204      </td>
      <td align=right > 1.07442      </td>
      <td align=right > 0.999953      </td>
    </tr>
    <tr> 
      <td align=right > 1.03924      </td>
      <td align=right > 0.07      </td>
      <td align=right > 0.962498      </td>
      <td align=right > 0      </td>
      <td align=right > 1.01406      </td>
      <td align=right > 1.0873      </td>
      <td align=right > 0.999925      </td>
    </tr>
    <tr> 
      <td align=right > 1.04497      </td>
      <td align=right > 0.08      </td>
      <td align=right > 0.957302      </td>
      <td align=right > 0      </td>
      <td align=right > 1.01608      </td>
      <td align=right > 1.10032      </td>
      <td align=right > 0.999889      </td>
    </tr>
    <tr> 
      <td align=right > 1.05072      </td>
      <td align=right > 0.09      </td>
      <td align=right > 0.952144      </td>
      <td align=right > 0      </td>
      <td align=right > 1.0181      </td>
      <td align=right > 1.11348      </td>
      <td align=right > 0.999842      </td>
    </tr>
    <tr> 
      <td align=right > 1.05651      </td>
      <td align=right > 0.1      </td>
      <td align=right > 0.947027      </td>
      <td align=right > 0      </td>
      <td align=right > 1.02013      </td>
      <td align=right > 1.12678      </td>
      <td align=right > 0.999783      </td>
    </tr>
    <tr> 
      <td align=right > 1.11603      </td>
      <td align=right > 0.2      </td>
      <td align=right > 0.897967      </td>
      <td align=right > 0      </td>
      <td align=right > 1.04064      </td>
      <td align=right > 1.26785      </td>
      <td align=right > 0.998299      </td>
    </tr>
    <tr> 
      <td align=right > 1.18654      </td>
      <td align=right > 0.3125      </td>
      <td align=right > 0.847187      </td>
      <td align=right > 0      </td>
      <td align=right > 1.06439      </td>
      <td align=right > 1.44495      </td>
      <td align=right > 0.993668      </td>
    </tr>
    <tr> 
      <td align=right > 1.26071      </td>
      <td align=right > 0.425      </td>
      <td align=right > 0.800793      </td>
      <td align=right > 0      </td>
      <td align=right > 1.0891      </td>
      <td align=right > 1.64296      </td>
      <td align=right > 0.984524      </td>
    </tr>
    <tr> 
      <td align=right > 1.33841      </td>
      <td align=right > 0.5375      </td>
      <td align=right > 0.758485      </td>
      <td align=right > 0      </td>
      <td align=right > 1.11499      </td>
      <td align=right > 1.86327      </td>
      <td align=right > 0.969739      </td>
    </tr>
    <tr> 
      <td align=right > 1.41948      </td>
      <td align=right > 0.65      </td>
      <td align=right > 0.719966      </td>
      <td align=right > 0      </td>
      <td align=right > 1.14228      </td>
      <td align=right > 2.1072      </td>
      <td align=right > 0.948557      </td>
    </tr>
    <tr> 
      <td align=right > 1.50375      </td>
      <td align=right > 0.7625      </td>
      <td align=right > 0.684943      </td>
      <td align=right > 0      </td>
      <td align=right > 1.17118      </td>
      <td align=right > 2.37593      </td>
      <td align=right > 0.920637      </td>
    </tr>
    <tr> 
      <td align=right > 1.59103      </td>
      <td align=right > 0.875      </td>
      <td align=right > 0.653131      </td>
      <td align=right > 0      </td>
      <td align=right > 1.20187      </td>
      <td align=right > 2.67058      </td>
      <td align=right > 0.886067      </td>
    </tr>
    <tr> 
      <td align=right > 1.6811      </td>
      <td align=right > 0.9875      </td>
      <td align=right > 0.624256      </td>
      <td align=right > 0      </td>
      <td align=right > 1.2345      </td>
      <td align=right > 2.9921      </td>
      <td align=right > 0.845331      </td>
    </tr>
    <tr> 
      <td align=right > 1.77377      </td>
      <td align=right > 1.1      </td>
      <td align=right > 0.598056      </td>
      <td align=right > 0      </td>
      <td align=right > 1.26923      </td>
      <td align=right > 3.34138      </td>
      <td align=right > 0.799253      </td>
    </tr>
    <tr> 
      <td align=right > 1.85817      </td>
      <td align=right > 1.2      </td>
      <td align=right > 0.576814      </td>
      <td align=right > 0      </td>
      <td align=right > 1.30196      </td>
      <td align=right > 3.67576      </td>
      <td align=right > 0.754678      </td>
    </tr>
    <tr> 
      <td align=right > 1.94432      </td>
      <td align=right > 1.3      </td>
      <td align=right > 0.55733      </td>
      <td align=right > 0      </td>
      <td align=right > 1.33652      </td>
      <td align=right > 4.03314      </td>
      <td align=right > 0.707595      </td>
    </tr>
    <tr> 
      <td align=right > 2.0321      </td>
      <td align=right > 1.4      </td>
      <td align=right > 0.539453      </td>
      <td align=right > 0      </td>
      <td align=right > 1.37299      </td>
      <td align=right > 4.41393      </td>
      <td align=right > 0.658907      </td>
    </tr>
    <tr> 
      <td align=right > 2.12139      </td>
      <td align=right > 1.5      </td>
      <td align=right > 0.523041      </td>
      <td align=right > 0      </td>
      <td align=right > 1.41142      </td>
      <td align=right > 4.8185      </td>
      <td align=right > 0.609509      </td>
    </tr>
    <tr> 
      <td align=right > 2.21207      </td>
      <td align=right > 1.6      </td>
      <td align=right > 0.507967      </td>
      <td align=right > 0      </td>
      <td align=right > 1.45186      </td>
      <td align=right > 5.24717      </td>
      <td align=right > 0.56024      </td>
    </tr>
    <tr> 
      <td align=right > 2.30402      </td>
      <td align=right > 1.7      </td>
      <td align=right > 0.494112      </td>
      <td align=right > 0      </td>
      <td align=right > 1.49437      </td>
      <td align=right > 5.70021      </td>
      <td align=right > 0.511853      </td>
    </tr>
    <tr> 
      <td align=right > 2.39716      </td>
      <td align=right > 1.8      </td>
      <td align=right > 0.481365      </td>
      <td align=right > 0      </td>
      <td align=right > 1.53898      </td>
      <td align=right > 6.17787      </td>
      <td align=right > 0.464991      </td>
    </tr>
    <tr> 
      <td align=right > 2.49138      </td>
      <td align=right > 1.9      </td>
      <td align=right > 0.469629      </td>
      <td align=right > 0      </td>
      <td align=right > 1.58573      </td>
      <td align=right > 6.68035      </td>
      <td align=right > 0.420176      </td>
    </tr>
    <tr> 
      <td align=right > 2.58661      </td>
      <td align=right > 2      </td>
      <td align=right > 0.458813      </td>
      <td align=right > 0      </td>
      <td align=right > 1.63464      </td>
      <td align=right > 7.20786      </td>
      <td align=right > 0.377806      </td>
    </tr>
    <tr> 
      <td align=right > 2.68275      </td>
      <td align=right > 2.1      </td>
      <td align=right > 0.448834      </td>
      <td align=right > 0      </td>
      <td align=right > 1.68576      </td>
      <td align=right > 7.76053      </td>
      <td align=right > 0.338161      </td>
    </tr>
    <tr> 
      <td align=right > 2.77975      </td>
      <td align=right > 2.2      </td>
      <td align=right > 0.439619      </td>
      <td align=right > 0      </td>
      <td align=right > 1.73909      </td>
      <td align=right > 8.33853      </td>
      <td align=right > 0.301412      </td>
    </tr>
    <tr> 
      <td align=right > 2.87752      </td>
      <td align=right > 2.3      </td>
      <td align=right > 0.431099      </td>
      <td align=right > 0      </td>
      <td align=right > 1.79466      </td>
      <td align=right > 8.94196      </td>
      <td align=right > 0.267633      </td>
    </tr>
    <tr> 
      <td align=right > 2.97602      </td>
      <td align=right > 2.4      </td>
      <td align=right > 0.423213      </td>
      <td align=right > 0      </td>
      <td align=right > 1.85249      </td>
      <td align=right > 9.57094      </td>
      <td align=right > 0.236821      </td>
    </tr>
    <tr> 
      <td align=right > 3.07518      </td>
      <td align=right > 2.5      </td>
      <td align=right > 0.415906      </td>
      <td align=right > 0      </td>
      <td align=right > 1.91259      </td>
      <td align=right > 10.2256      </td>
      <td align=right > 0.208907      </td>
    </tr>
    <tr> 
      <td align=right > 3.17496      </td>
      <td align=right > 2.6      </td>
      <td align=right > 0.409128      </td>
      <td align=right > 0      </td>
      <td align=right > 1.97498      </td>
      <td align=right > 10.9059      </td>
      <td align=right > 0.183774      </td>
    </tr>
    <tr> 
      <td align=right > 3.27531      </td>
      <td align=right > 2.7      </td>
      <td align=right > 0.402833      </td>
      <td align=right > 0      </td>
      <td align=right > 2.03967      </td>
      <td align=right > 11.612      </td>
      <td align=right > 0.161268      </td>
    </tr>
    <tr> 
      <td align=right > 3.37619      </td>
      <td align=right > 2.8      </td>
      <td align=right > 0.39698      </td>
      <td align=right > 0      </td>
      <td align=right > 2.10667      </td>
      <td align=right > 12.344      </td>
      <td align=right > 0.141215      </td>
    </tr>
    <tr> 
      <td align=right > 3.47756      </td>
      <td align=right > 2.9      </td>
      <td align=right > 0.391533      </td>
      <td align=right > 0      </td>
      <td align=right > 2.17598      </td>
      <td align=right > 13.1019      </td>
      <td align=right > 0.123424      </td>
    </tr>
  </tbody>
</table>
<BR><HR>
<ADDRESS>
genick
2007-11-14
</ADDRESS>
        </tr>
    </tbody>
    </table>
    <? include("aboutPottoProject.php");  ?>
    </div>
    </td>
    </tr>
    </tbody>
</table>
<? include("bottom.php"); ?>

