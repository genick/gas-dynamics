<? include("header.php");?>
<table style="width:100%; height:993px;" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
	<? include("left_link.php");?>
	<td style="width: 80%; vertical-align: top;">
	<div  font-family: "arial", "helvetica", "lucida", "sans";>

<!--Navigation Panel-->
<A NAME="tex2html363"
  HREF="node23.php">
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next"
 SRC="figures/next.png"></A> 
<A NAME="tex2html359"
  HREF="node19.php">
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up"
 SRC="figures/up.png"></A> 
<A NAME="tex2html353"
  HREF="node21.php">
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous"
 SRC="figures/prev.png"></A> 
<A NAME="tex2html361"
  HREF="node1.php">
<IMG WIDTH="65" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="contents"
 SRC="figures/contents.png"></A>  
<BR>
<B> Next:</B> <A NAME="tex2html364"
  HREF="node23.php">Normal shock Standard Table</A>
<B> Up:</B> <A NAME="tex2html360"
  HREF="node19.php">Normal Shock Standard tables</A>
<B> Previous:</B> <A NAME="tex2html354"
  HREF="node21.php">Normal Shock Standard Table</A>
 &nbsp; <B>  <A NAME="tex2html362"
  HREF="node1.php">Contents</A></B> 
<BR>
<BR>
<!--End of Navigation Panel-->

<H3><A NAME="SECTION00041300000000000000">
Normal Shock Standard Table for k=1.4</A>
</H3>

<P>

<table border=1 width="100%" >
  <thead>
    <tr>
      <th align=left bgcolor="#fffeaa" colspan=2 >Normal Shock </th>
      <th align=left  bgcolor="#00ff5a" colspan=2 >Input: Mx </th>
      <th align=left  bgcolor="#9ae0ee" colspan=1 >k = 1.4 </th>
    </tr>
    <tr>
      <th align=center >Mx </th>
      <th align=center >My </th>
      <th align=center >Ty/Tx </th>
      <th align=center >&rho;y/&rho;x </th>
      <th align=center >Py/Px </th>
      <th align=center >P0y/P0x </th>
    </tr> 
  </thead>
  <tbody>
    <tr> 
      <td align=right > 1      </td>
      <td align=right > 1      </td>
      <td align=right > 1      </td>
      <td align=right > 1      </td>
      <td align=right > 1      </td>
      <td align=right > 1      </td>
    </tr>
    <tr> 
      <td align=right > 1.01      </td>
      <td align=right > 0.990132      </td>
      <td align=right > 1.00664      </td>
      <td align=right > 1.01669      </td>
      <td align=right > 1.02345      </td>
      <td align=right > 0.999999      </td>
    </tr>
    <tr> 
      <td align=right > 1.02      </td>
      <td align=right > 0.980519      </td>
      <td align=right > 1.01325      </td>
      <td align=right > 1.03344      </td>
      <td align=right > 1.04713      </td>
      <td align=right > 0.99999      </td>
    </tr>
    <tr> 
      <td align=right > 1.03      </td>
      <td align=right > 0.971154      </td>
      <td align=right > 1.01981      </td>
      <td align=right > 1.05024      </td>
      <td align=right > 1.07105      </td>
      <td align=right > 0.999967      </td>
    </tr>
    <tr> 
      <td align=right > 1.04      </td>
      <td align=right > 0.962025      </td>
      <td align=right > 1.02634      </td>
      <td align=right > 1.06709      </td>
      <td align=right > 1.0952      </td>
      <td align=right > 0.999923      </td>
    </tr>
    <tr> 
      <td align=right > 1.05      </td>
      <td align=right > 0.953125      </td>
      <td align=right > 1.03284      </td>
      <td align=right > 1.08398      </td>
      <td align=right > 1.11958      </td>
      <td align=right > 0.999853      </td>
    </tr>
    <tr> 
      <td align=right > 1.06      </td>
      <td align=right > 0.944445      </td>
      <td align=right > 1.03931      </td>
      <td align=right > 1.10092      </td>
      <td align=right > 1.1442      </td>
      <td align=right > 0.999751      </td>
    </tr>
    <tr> 
      <td align=right > 1.07      </td>
      <td align=right > 0.935977      </td>
      <td align=right > 1.04575      </td>
      <td align=right > 1.1179      </td>
      <td align=right > 1.16905      </td>
      <td align=right > 0.999611      </td>
    </tr>
    <tr> 
      <td align=right > 1.08      </td>
      <td align=right > 0.927713      </td>
      <td align=right > 1.05217      </td>
      <td align=right > 1.13492      </td>
      <td align=right > 1.19413      </td>
      <td align=right > 0.999431      </td>
    </tr>
    <tr> 
      <td align=right > 1.09      </td>
      <td align=right > 0.919647      </td>
      <td align=right > 1.05856      </td>
      <td align=right > 1.15199      </td>
      <td align=right > 1.21945      </td>
      <td align=right > 0.999204      </td>
    </tr>
    <tr> 
      <td align=right > 1.1      </td>
      <td align=right > 0.91177      </td>
      <td align=right > 1.06494      </td>
      <td align=right > 1.16908      </td>
      <td align=right > 1.245      </td>
      <td align=right > 0.998928      </td>
    </tr>
    <tr> 
      <td align=right > 1.2      </td>
      <td align=right > 0.84217      </td>
      <td align=right > 1.12799      </td>
      <td align=right > 1.34161      </td>
      <td align=right > 1.51333      </td>
      <td align=right > 0.992798      </td>
    </tr>
    <tr> 
      <td align=right > 1.25      </td>
      <td align=right > 0.812636      </td>
      <td align=right > 1.15938      </td>
      <td align=right > 1.42857      </td>
      <td align=right > 1.65625      </td>
      <td align=right > 0.987057      </td>
    </tr>
    <tr> 
      <td align=right > 1.3      </td>
      <td align=right > 0.785957      </td>
      <td align=right > 1.19087      </td>
      <td align=right > 1.5157      </td>
      <td align=right > 1.805      </td>
      <td align=right > 0.979374      </td>
    </tr>
    <tr> 
      <td align=right > 1.35      </td>
      <td align=right > 0.761753      </td>
      <td align=right > 1.22261      </td>
      <td align=right > 1.60278      </td>
      <td align=right > 1.95958      </td>
      <td align=right > 0.969737      </td>
    </tr>
    <tr> 
      <td align=right > 1.4      </td>
      <td align=right > 0.739709      </td>
      <td align=right > 1.25469      </td>
      <td align=right > 1.68966      </td>
      <td align=right > 2.12      </td>
      <td align=right > 0.958194      </td>
    </tr>
    <tr> 
      <td align=right > 1.45      </td>
      <td align=right > 0.719562      </td>
      <td align=right > 1.2872      </td>
      <td align=right > 1.77614      </td>
      <td align=right > 2.28625      </td>
      <td align=right > 0.944837      </td>
    </tr>
    <tr> 
      <td align=right > 1.5      </td>
      <td align=right > 0.701089      </td>
      <td align=right > 1.32022      </td>
      <td align=right > 1.86207      </td>
      <td align=right > 2.45833      </td>
      <td align=right > 0.929787      </td>
    </tr>
    <tr> 
      <td align=right > 1.55      </td>
      <td align=right > 0.684101      </td>
      <td align=right > 1.35379      </td>
      <td align=right > 1.94732      </td>
      <td align=right > 2.63625      </td>
      <td align=right > 0.913188      </td>
    </tr>
    <tr> 
      <td align=right > 1.6      </td>
      <td align=right > 0.668437      </td>
      <td align=right > 1.38797      </td>
      <td align=right > 2.03175      </td>
      <td align=right > 2.82      </td>
      <td align=right > 0.8952      </td>
    </tr>
    <tr> 
      <td align=right > 1.65      </td>
      <td align=right > 0.653958      </td>
      <td align=right > 1.4228      </td>
      <td align=right > 2.11525      </td>
      <td align=right > 3.00958      </td>
      <td align=right > 0.875988      </td>
    </tr>
    <tr> 
      <td align=right > 1.7      </td>
      <td align=right > 0.640544      </td>
      <td align=right > 1.45833      </td>
      <td align=right > 2.19772      </td>
      <td align=right > 3.205      </td>
      <td align=right > 0.855721      </td>
    </tr>
    <tr> 
      <td align=right > 1.75      </td>
      <td align=right > 0.628089      </td>
      <td align=right > 1.49458      </td>
      <td align=right > 2.27907      </td>
      <td align=right > 3.40625      </td>
      <td align=right > 0.834565      </td>
    </tr>
    <tr> 
      <td align=right > 1.8      </td>
      <td align=right > 0.616501      </td>
      <td align=right > 1.53158      </td>
      <td align=right > 2.35922      </td>
      <td align=right > 3.61333      </td>
      <td align=right > 0.812684      </td>
    </tr>
    <tr> 
      <td align=right > 1.85      </td>
      <td align=right > 0.605701      </td>
      <td align=right > 1.56935      </td>
      <td align=right > 2.43811      </td>
      <td align=right > 3.82625      </td>
      <td align=right > 0.790232      </td>
    </tr>
    <tr> 
      <td align=right > 1.9      </td>
      <td align=right > 0.595616      </td>
      <td align=right > 1.60792      </td>
      <td align=right > 2.51568      </td>
      <td align=right > 4.045      </td>
      <td align=right > 0.767357      </td>
    </tr>
    <tr> 
      <td align=right > 1.95      </td>
      <td align=right > 0.586185      </td>
      <td align=right > 1.64729      </td>
      <td align=right > 2.59188      </td>
      <td align=right > 4.26958      </td>
      <td align=right > 0.744195      </td>
    </tr>
    <tr> 
      <td align=right > 2      </td>
      <td align=right > 0.57735      </td>
      <td align=right > 1.6875      </td>
      <td align=right > 2.66667      </td>
      <td align=right > 4.5      </td>
      <td align=right > 0.720874      </td>
    </tr>
    <tr> 
      <td align=right > 2.1      </td>
      <td align=right > 0.561277      </td>
      <td align=right > 1.77045      </td>
      <td align=right > 2.8119      </td>
      <td align=right > 4.97833      </td>
      <td align=right > 0.674203      </td>
    </tr>
    <tr> 
      <td align=right > 2.2      </td>
      <td align=right > 0.547056      </td>
      <td align=right > 1.85686      </td>
      <td align=right > 2.95122      </td>
      <td align=right > 5.48      </td>
      <td align=right > 0.628136      </td>
    </tr>
    <tr> 
      <td align=right > 2.3      </td>
      <td align=right > 0.534411      </td>
      <td align=right > 1.9468      </td>
      <td align=right > 3.08455      </td>
      <td align=right > 6.005      </td>
      <td align=right > 0.583295      </td>
    </tr>
    <tr> 
      <td align=right > 2.4      </td>
      <td align=right > 0.523118      </td>
      <td align=right > 2.04033      </td>
      <td align=right > 3.2119      </td>
      <td align=right > 6.55333      </td>
      <td align=right > 0.540144      </td>
    </tr>
    <tr> 
      <td align=right > 2.5      </td>
      <td align=right > 0.512989      </td>
      <td align=right > 2.1375      </td>
      <td align=right > 3.33333      </td>
      <td align=right > 7.125      </td>
      <td align=right > 0.499015      </td>
    </tr>
    <tr> 
      <td align=right > 2.6      </td>
      <td align=right > 0.503871      </td>
      <td align=right > 2.23834      </td>
      <td align=right > 3.44898      </td>
      <td align=right > 7.72      </td>
      <td align=right > 0.460123      </td>
    </tr>
    <tr> 
      <td align=right > 2.7      </td>
      <td align=right > 0.495634      </td>
      <td align=right > 2.34289      </td>
      <td align=right > 3.55899      </td>
      <td align=right > 8.33833      </td>
      <td align=right > 0.42359      </td>
    </tr>
    <tr> 
      <td align=right > 2.8      </td>
      <td align=right > 0.488167      </td>
      <td align=right > 2.45117      </td>
      <td align=right > 3.66355      </td>
      <td align=right > 8.98      </td>
      <td align=right > 0.389464      </td>
    </tr>
    <tr> 
      <td align=right > 2.9      </td>
      <td align=right > 0.48138      </td>
      <td align=right > 2.56321      </td>
      <td align=right > 3.76286      </td>
      <td align=right > 9.645      </td>
      <td align=right > 0.357733      </td>
    </tr>
    <tr> 
      <td align=right > 3      </td>
      <td align=right > 0.475191      </td>
      <td align=right > 2.67901      </td>
      <td align=right > 3.85714      </td>
      <td align=right > 10.3333      </td>
      <td align=right > 0.328344      </td>
    </tr>
    <tr> 
      <td align=right > 3.5      </td>
      <td align=right > 0.451154      </td>
      <td align=right > 3.31505      </td>
      <td align=right > 4.26087      </td>
      <td align=right > 14.125      </td>
      <td align=right > 0.212948      </td>
    </tr>
    <tr> 
      <td align=right > 4      </td>
      <td align=right > 0.434959      </td>
      <td align=right > 4.04688      </td>
      <td align=right > 4.57143      </td>
      <td align=right > 18.5      </td>
      <td align=right > 0.138756      </td>
    </tr>
    <tr> 
      <td align=right > 4.5      </td>
      <td align=right > 0.423552      </td>
      <td align=right > 4.87509      </td>
      <td align=right > 4.81188      </td>
      <td align=right > 23.4583      </td>
      <td align=right > 0.0916979      </td>
    </tr>
    <tr> 
      <td align=right > 5      </td>
      <td align=right > 0.415227      </td>
      <td align=right > 5.8      </td>
      <td align=right > 5      </td>
      <td align=right > 29      </td>
      <td align=right > 0.0617163      </td>
    </tr>
    <tr> 
      <td align=right > 5.5      </td>
      <td align=right > 0.408974      </td>
      <td align=right > 6.8218      </td>
      <td align=right > 5.14894      </td>
      <td align=right > 35.125      </td>
      <td align=right > 0.0423614      </td>
    </tr>
    <tr> 
      <td align=right > 6      </td>
      <td align=right > 0.404162      </td>
      <td align=right > 7.94059      </td>
      <td align=right > 5.26829      </td>
      <td align=right > 41.8333      </td>
      <td align=right > 0.0296509      </td>
    </tr>
    <tr> 
      <td align=right > 6.5      </td>
      <td align=right > 0.400381      </td>
      <td align=right > 9.15643      </td>
      <td align=right > 5.36508      </td>
      <td align=right > 49.125      </td>
      <td align=right > 0.0211476      </td>
    </tr>
    <tr> 
      <td align=right > 7      </td>
      <td align=right > 0.39736      </td>
      <td align=right > 10.4694      </td>
      <td align=right > 5.44444      </td>
      <td align=right > 57      </td>
      <td align=right > 0.0153515      </td>
    </tr>
    <tr> 
      <td align=right > 7.5      </td>
      <td align=right > 0.394907      </td>
      <td align=right > 11.8795      </td>
      <td align=right > 5.5102      </td>
      <td align=right > 65.4583      </td>
      <td align=right > 0.0113286      </td>
    </tr>
    <tr> 
      <td align=right > 8      </td>
      <td align=right > 0.39289      </td>
      <td align=right > 13.3867      </td>
      <td align=right > 5.56522      </td>
      <td align=right > 74.5      </td>
      <td align=right > 0.00848783      </td>
    </tr>
    <tr> 
      <td align=right > 8.5      </td>
      <td align=right > 0.391211      </td>
      <td align=right > 14.9911      </td>
      <td align=right > 5.61165      </td>
      <td align=right > 84.125      </td>
      <td align=right > 0.00644918      </td>
    </tr>
    <tr> 
      <td align=right > 9      </td>
      <td align=right > 0.389799      </td>
      <td align=right > 16.6927      </td>
      <td align=right > 5.65116      </td>
      <td align=right > 94.3333      </td>
      <td align=right > 0.00496386      </td>
    </tr>
    <tr> 
      <td align=right > 9.5      </td>
      <td align=right > 0.388601      </td>
      <td align=right > 18.4915      </td>
      <td align=right > 5.68504      </td>
      <td align=right > 105.125      </td>
      <td align=right > 0.00386636      </td>
    </tr>
    <tr> 
      <td align=right > 10      </td>
      <td align=right > 0.387575      </td>
      <td align=right > 20.3875      </td>
      <td align=right > 5.71429      </td>
      <td align=right > 116.5      </td>
      <td align=right > 0.00304475      </td>
    </tr>
    <tr> 
      <td align=right > 20      </td>
      <td align=right > 0.380387      </td>
      <td align=right > 78.7219      </td>
      <td align=right > 5.92593      </td>
      <td align=right > 466.5      </td>
      <td align=right > 0.000107775      </td>
    </tr>
    <tr> 
      <td align=right > 25      </td>
      <td align=right > 0.379517      </td>
      <td align=right > 122.472      </td>
      <td align=right > 5.95238      </td>
      <td align=right > 729      </td>
      <td align=right > 3.5859e-5      </td>
    </tr>
    <tr> 
      <td align=right > 30      </td>
      <td align=right > 0.379043      </td>
      <td align=right > 175.944      </td>
      <td align=right > 5.96685      </td>
      <td align=right > 1049.83      </td>
      <td align=right > 1.45314e-5      </td>
    </tr>
    <tr> 
      <td align=right > 35      </td>
      <td align=right > 0.378757      </td>
      <td align=right > 239.139      </td>
      <td align=right > 5.97561      </td>
      <td align=right > 1429      </td>
      <td align=right > 6.75705e-6      </td>
    </tr>
    <tr> 
      <td align=right > 40      </td>
      <td align=right > 0.378571      </td>
      <td align=right > 312.055      </td>
      <td align=right > 5.98131      </td>
      <td align=right > 1866.5      </td>
      <td align=right > 3.4771e-6      </td>
    </tr>
    <tr> 
      <td align=right > 45      </td>
      <td align=right > 0.378444      </td>
      <td align=right > 394.694      </td>
      <td align=right > 5.98522      </td>
      <td align=right > 2362.33      </td>
      <td align=right > 1.93387e-6      </td>
    </tr>
    <tr> 
      <td align=right > 50      </td>
      <td align=right > 0.378353      </td>
      <td align=right > 487.055      </td>
      <td align=right > 5.98802      </td>
      <td align=right > 2916.5      </td>
      <td align=right > 1.14377e-6      </td>
    </tr>
    <tr> 
      <td align=right > 55      </td>
      <td align=right > 0.378286      </td>
      <td align=right > 589.139      </td>
      <td align=right > 5.9901      </td>
      <td align=right > 3529      </td>
      <td align=right > 7.11033e-7      </td>
    </tr>
    <tr> 
      <td align=right > 60      </td>
      <td align=right > 0.378234      </td>
      <td align=right > 700.944      </td>
      <td align=right > 5.99168      </td>
      <td align=right > 4199.83      </td>
      <td align=right > 4.60617e-7      </td>
    </tr>
    <tr> 
      <td align=right > 65      </td>
      <td align=right > 0.378194      </td>
      <td align=right > 822.472      </td>
      <td align=right > 5.99291      </td>
      <td align=right > 4929      </td>
      <td align=right > 3.08912e-7      </td>
    </tr>
    <tr> 
      <td align=right > 70      </td>
      <td align=right > 0.378163      </td>
      <td align=right > 953.722      </td>
      <td align=right > 5.99388      </td>
      <td align=right > 5716.5      </td>
      <td align=right > 2.1338e-7      </td>
    </tr>
  </tbody>
</table>
<BR><HR>
<ADDRESS>
genick
2007-11-14
</ADDRESS>
        </tr>
    </tbody>
    </table>
    <? include("aboutPottoProject.php");  ?>
    </div>
    </td>
    </tr>
    </tbody>
</table>
<? include("bottom.php"); ?>

