<? include("header.php");?>
<table style="width:100%; height:993px;" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
	<? include("left_link.php");?>
	<td style="width: 80%; vertical-align: top;">
	<div  font-family: "arial", "helvetica", "lucida", "sans";>

<!--Navigation Panel-->
<A NAME="tex2html323"
  HREF="node20.php">
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next"
 SRC="figures/next.png"></A> 
<A NAME="tex2html319"
  HREF="node18.php">
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up"
 SRC="figures/up.png"></A> 
<A NAME="tex2html313"
  HREF="node18.php">
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous"
 SRC="figures/prev.png"></A> 
<A NAME="tex2html321"
  HREF="node1.php">
<IMG WIDTH="65" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="contents"
 SRC="figures/contents.png"></A>  
<BR>
<B> Next:</B> <A NAME="tex2html324"
  HREF="node20.php">Normal Shock Standard Table</A>
<B> Up:</B> <A NAME="tex2html320"
  HREF="node18.php">Normal Shock</A>
<B> Previous:</B> <A NAME="tex2html314"
  HREF="node18.php">Normal Shock</A>
 &nbsp; <B>  <A NAME="tex2html322"
  HREF="node1.php">Contents</A></B> 
<BR>
<BR>
<!--End of Navigation Panel-->

<H2><A NAME="SECTION00041000000000000000">
Normal Shock Standard tables</A>
</H2>

<P>
<BR><HR>
<!--Table of Child-Links-->
<A NAME="CHILD_LINKS"><STRONG>Subsections</STRONG></A>

<UL>
<LI><A NAME="tex2html325"
  HREF="node20.php">Normal Shock Standard Table for k=1.2</A>
<LI><A NAME="tex2html326"
  HREF="node21.php">Normal Shock Standard Table for k=1.3</A>
<LI><A NAME="tex2html327"
  HREF="node22.php">Normal Shock Standard Table for k=1.4</A>
<LI><A NAME="tex2html328"
  HREF="node23.php">Normal shock Standard Table for k=1.67</A>
</UL>
<!--End of Table of Child-Links-->
<BR><HR>
<ADDRESS>
genick
2007-11-14
</ADDRESS>
        </tr>
    </tbody>
    </table>
    <? include("aboutPottoProject.php");  ?>
    </div>
    </td>
    </tr>
    </tbody>
</table>
<? include("bottom.php"); ?>

