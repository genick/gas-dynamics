<!--Navigation Panel-->
<A NAME="tex2html225" HREF="node1.php">
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0"
ALT="next"
 SRC="figures/next.png"></A> 
<A NAME="potto home" HREF="../index.php">
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up"
 SRC="figures/up.png"> 
<A NAME="potto home" HREF="../index.php">
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0"
ALT="previous"
 SRC="figures/prev.png">   
</a>
<BR>
<B> Next:</B> <A NAME="tex2html226"
  HREF="node1.php">Epilogue for the POTTO</A>

<BR>
<B> Up:</B> <A NAME="tex2html226"
  HREF="../index.php">POTTO PROJECT HOME</A>
<BR>
<!--End of Navigation Panel-->

<h1>Fundamentals of Compressible Fluid Mechanics</h1> 
<DIV ALIGN="CENTER">
<FONT SIZE="+1">Please note that the conversion of the source
files has
several errors. For the exact typesetting use the pdf or dvi
files. </FONT></DIV>
<P>
<br>
<A NAME="pdfVersion"
  HREF="../text.pdf">

To download the pdf version 
</a>



<DIV ALIGN="CENTER">
</DIV>

<FONT SIZE="+2">
<B>
`We are like dwarfs sitting on the shoulders of giants''
</B>
</FONT>
<BR>
<BR>
<P>
<FONT SIZE="+2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;from The Metalogicon by John in 1159
</FONT>

<BR><HR>
<!--Table of Child-Links-->
<A NAME="CHILD_LINKS"></A>

<UL>
<LI><A NAME="tex2html258"
  HREF="node1.php">Contents</A>
<LI><A NAME="tex2html259"
  HREF="node2.php">Notice of Copyright For This Document:</A>
<UL>
<LI><A NAME="tex2html260"
  HREF="node3.php">0.1 GNU Free Documentation License</A>
<LI><A NAME="tex2html261"
  HREF="node4.php">0.2 Potto Project License</A>
</UL>
<BR>
<LI><A NAME="tex2html262"
  HREF="node5.php">About This Author</A>
<LI><A NAME="tex2html263"
  HREF="node6.php">Epilogue for the POTTO Project</A>
<LI><A NAME="tex2html264"
  HREF="node7.php">Epilogue For This Book</A>
<UL>
<LI><A NAME="tex2html265"
  HREF="node8.php">0.3 Version 0.4.2</A>
<LI><A NAME="tex2html266"
  HREF="node9.php">0.4 Version 0.4</A>
<LI><A NAME="tex2html267"
  HREF="node10.php">0.5 Version 0.3</A>
</UL>
<BR>
<LI><A NAME="tex2html268"
  HREF="node11.php">How This Book Was Written</A>
<LI><A NAME="tex2html269"
  HREF="node12.php">About Gas Dyanmics Calculator</A>
<LI><A NAME="tex2html270"
  HREF="node13.php">Preface</A>
<LI><A NAME="tex2html271"
  HREF="node14.php">To Do List and Road Map</A>
<UL>
<LI><A NAME="tex2html272"
  HREF="node15.php">0.5.1 Speed of Sound</A>
<LI><A NAME="tex2html273"
  HREF="node16.php">0.5.2 Stagnation effects </A>
<LI><A NAME="tex2html274"
  HREF="node17.php">0.5.3 Nozzle </A>
<LI><A NAME="tex2html275"
  HREF="node18.php">0.5.4 Isothermal Nozzle</A>
<LI><A NAME="tex2html276"
  HREF="node19.php">0.5.5 Isothermal Flow</A>
<LI><A NAME="tex2html277"
  HREF="node20.php">0.5.6 Fanno Flow</A>
<LI><A NAME="tex2html278"
  HREF="node21.php">0.5.7 Rayleigh Flow</A>
<LI><A NAME="tex2html279"
  HREF="node22.php">0.5.8 Evacuation and filling semi rigid Chambers</A>
<LI><A NAME="tex2html280"
  HREF="node23.php">0.5.9 Evacuating and filling chambers under external forces</A>
<LI><A NAME="tex2html281"
  HREF="node24.php">0.5.10 Oblique shock</A>
<LI><A NAME="tex2html282"
  HREF="node25.php">0.5.11 Prandtl-Meyer</A>
</UL>
<BR>
<LI><A NAME="tex2html283"
  HREF="node26.php">1. Introduction</A>
<UL>
<LI><A NAME="tex2html284"
  HREF="node27.php">1.1 What is Compressible Flow ?</A>
<LI><A NAME="tex2html285"
  HREF="node28.php">1.2 Why Compressible Flow is Important? </A>
<LI><A NAME="tex2html286"
  HREF="node29.php">1.3 Historical Background</A>
<UL>
<LI><A NAME="tex2html287"
  HREF="node30.php">1.3.1 Early Developments</A>
<UL>
<LI><A NAME="tex2html288"
  HREF="node31.php">1.3.1.1 Speed of Sound</A>
</UL>
<LI><A NAME="tex2html289"
  HREF="node32.php">1.3.2 The shock wave puzzle</A>
<LI><A NAME="tex2html290"
  HREF="node33.php">1.3.3 Choking Flow</A>
<UL>
<LI><A NAME="tex2html291"
  HREF="node34.php">1.3.3.1 Nozzle flow</A>
<LI><A NAME="tex2html292"
  HREF="node35.php">1.3.3.2 Rayleigh Flow</A>
<LI><A NAME="tex2html293"
  HREF="node36.php">1.3.3.3 Fanno Flow</A>
<LI><A NAME="tex2html294"
  HREF="node37.php">1.3.3.4 Isothermal Flow</A>
</UL>
<LI><A NAME="tex2html295"
  HREF="node38.php">1.3.4 External flow </A>
<UL>
<LI><A NAME="tex2html296"
  HREF="node39.php">1.3.4.1 Filling and evacuating gaseous chambers </A>
</UL>
<LI><A NAME="tex2html297"
  HREF="node40.php">1.3.5 Biographies of Major Figures</A>
<UL>
<LI><A NAME="tex2html298"
  HREF="node41.php">1.3.5.1 Galileo Galilei</A>
<LI><A NAME="tex2html299"
  HREF="node42.php">1.3.5.2 Ernest Mach (1838-1916)</A>
<LI><A NAME="tex2html300"
  HREF="node43.php">1.3.5.3 John William Strutt (Lord Rayleigh)</A>
<LI><A NAME="tex2html301"
  HREF="node44.php">1.3.5.4 William John Macquorn Rankine</A>
<LI><A NAME="tex2html302"
  HREF="node45.php">1.3.5.5 Gino Girolamo Fanno</A>
<LI><A NAME="tex2html303"
  HREF="node46.php">1.3.5.6  Ludwig Prandtl</A>
<LI><A NAME="tex2html304"
  HREF="node47.php">1.3.5.7 E.R.G.&nbsp;Eckert</A>
<LI><A NAME="tex2html305"
  HREF="node48.php">1.3.5.8 Ascher Shapiro</A>
</UL>
</UL>
</UL>
<BR>
<LI><A NAME="tex2html306"
  HREF="node49.php">2. Speed of Sound</A>
<UL>
<LI><A NAME="tex2html307"
  HREF="node50.php">2.0.1 Speed of sound in ideal and perfect gases</A>
<LI><A NAME="tex2html308"
  HREF="node51.php">2.0.2 Speed of Sound in Almost Incompressible Liquid</A>
<LI><A NAME="tex2html309"
  HREF="node52.php">2.0.3 Speed of Sound in Solids</A>
<LI><A NAME="tex2html310"
  HREF="node53.php">2.0.4 Sound Speed in Two Phase Medium</A>
</UL>
<BR>
<LI><A NAME="tex2html311"
  HREF="node54.php">3. Isentropic Variable Area Flow</A>
<UL>
<LI><A NAME="tex2html312"
  HREF="node55.php">3.1 Stagnation State</A>
<UL>
<LI><A NAME="tex2html313"
  HREF="node56.php">3.1.1 Relationships for small Mach number</A>
<LI><A NAME="tex2html314"
  HREF="node57.php">3.1.2 Deviations for the Ideal Gas Model</A>
</UL>
<LI><A NAME="tex2html315"
  HREF="node58.php">3.2 Isentropic Converging-Diverging Flow in Cross Section</A>
<UL>
<LI><A NAME="tex2html316"
  HREF="node59.php">3.2.1 The Properties in The Adiabatic Nozzle</A>
<UL>
<LI><A NAME="tex2html317"
  HREF="node60.php">3.2.1.1 Relationship Between the Mach Number and Cross Section Area</A>
</UL>
<LI><A NAME="tex2html318"
  HREF="node61.php">3.2.2 Examples</A>
<LI><A NAME="tex2html319"
  HREF="node62.php">3.2.3 Mass Flow Rate </A>
<UL>
<LI><A NAME="tex2html320"
  HREF="node63.php">3.2.3.1 Flow with pressure losses</A>
</UL>
</UL>
<LI><A NAME="tex2html321"
  HREF="node64.php">3.3  Isentropic Isothermal Flow Nozzle</A>
</UL>
<BR>
<LI><A NAME="tex2html322"
  HREF="node65.php">4. Normal Shock </A>
<UL>
<LI><A NAME="tex2html323"
  HREF="node66.php">4.1 Informal model</A>
<LI><A NAME="tex2html324"
  HREF="node67.php">4.2 Formal Model</A>
<UL>
<LI><A NAME="tex2html325"
  HREF="node68.php">4.2.1 Speed of sound definition</A>
<UL>
<LI><A NAME="tex2html326"
  HREF="node69.php">4.2.1.1 The maximum speed of sound velocity</A>
</UL>
<LI><A NAME="tex2html327"
  HREF="node70.php">4.2.2 Prandtl's condition</A>
<LI><A NAME="tex2html328"
  HREF="node71.php">4.2.3 Solution</A>
<LI><A NAME="tex2html329"
  HREF="node72.php">4.2.4 Small Perturbation Solution</A>
<LI><A NAME="tex2html330"
  HREF="node73.php">4.2.5 Shock Thickness</A>
<LI><A NAME="tex2html331"
  HREF="node74.php">4.2.6 Moving Shock</A>
<LI><A NAME="tex2html332"
  HREF="node75.php">4.2.7 Shock in sudden complete stop</A>
<LI><A NAME="tex2html333"
  HREF="node76.php">4.2.8 Normal Shock in Ducts</A>
</UL>
</UL>
<BR>
<LI><A NAME="tex2html334"
  HREF="node77.php">5. Normal Shock in Variable Duct Areas  </A>
<UL>
<LI><A NAME="tex2html335"
  HREF="node78.php">5.1 Nozzle efficiency</A>
</UL>
<BR>
<LI><A NAME="tex2html336"
  HREF="node79.php">6. Nozzle Flow With External Forces</A>
<UL>
<LI><A NAME="tex2html337"
  HREF="node80.php">6.1 Isentropic Nozzle</A>
<LI><A NAME="tex2html338"
  HREF="node81.php">6.2 Isothermal Nozzle</A>
</UL>
<BR>
<LI><A NAME="tex2html339"
  HREF="node82.php">7. Isothermal Flow</A>
<UL>
<LI><A NAME="tex2html340"
  HREF="node83.php">7.1 The control Volume Analysis/Governing equations</A>
<LI><A NAME="tex2html341"
  HREF="node84.php">7.2 Dimensionless Representation </A>
<LI><A NAME="tex2html342"
  HREF="node85.php">7.3 The Entrance Limitation Of Isothermal Flow Model</A>
<LI><A NAME="tex2html343"
  HREF="node86.php">7.4 Comparison with Incompressible Flow</A>
<LI><A NAME="tex2html344"
  HREF="node87.php">7.5 Supersonic Branch</A>
<LI><A NAME="tex2html345"
  HREF="node88.php">7.6 Figures and Tables</A>
<LI><A NAME="tex2html346"
  HREF="node89.php">7.7 Examples</A>
<LI><A NAME="tex2html347"
  HREF="node90.php">7.8 Unchoked situation</A>
</UL>
<BR>
<LI><A NAME="tex2html348"
  HREF="node91.php">8.  Fanno Flow</A>
<UL>
<LI><A NAME="tex2html349"
  HREF="node92.php">8.1 Introduction</A>
<LI><A NAME="tex2html350"
  HREF="node93.php">8.2 Model</A>
<UL>
<LI><A NAME="tex2html351"
  HREF="node94.php">8.2.1 Dimensionalization of the equations</A>
</UL>
<LI><A NAME="tex2html352"
  HREF="node95.php">8.3 The Mechanics and Why The Flow is Chock?</A>
<UL>
<LI><A NAME="tex2html353"
  HREF="node96.php">8.3.0.1 Why the flow is chock?</A>
<LI><A NAME="tex2html354"
  HREF="node97.php">8.3.0.2 The Trends</A>
</UL>
<LI><A NAME="tex2html355"
  HREF="node98.php">8.4 The working equations</A>
<UL>
<LI><A NAME="tex2html356"
  HREF="node99.php">8.4.1 Example</A>
</UL>
<LI><A NAME="tex2html357"
  HREF="node100.php">8.5 Supersonic Flow</A>
<LI><A NAME="tex2html358"
  HREF="node101.php">8.6 Maximum length for the supersonic flow</A>
<LI><A NAME="tex2html359"
  HREF="node102.php">8.7 Working Conditions</A>
<UL>
<LI><A NAME="tex2html360"
  HREF="node103.php">8.7.1 Variations of the tube length (<IMG
 WIDTH="30" HEIGHT="43" ALIGN="MIDDLE" BORDER="0"
 SRC="img17.png"
 ALT="$ \fld$">) effects</A>
<UL>
<LI><A NAME="tex2html361"
  HREF="node104.php">8.7.1.1 Subsonic branch</A>
<LI><A NAME="tex2html362"
  HREF="node105.php">8.7.1.2 Supersonic Branch</A>
</UL>
<LI><A NAME="tex2html363"
  HREF="node106.php">8.7.2 The Pressure Ratio, <!-- MATH
 $P_2 \over P_1$
 -->
<IMG
 WIDTH="22" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="img6.png"
 ALT="$ P_2 \over P_1$">, effects</A>
<UL>
<LI><A NAME="tex2html364"
  HREF="node107.php">8.7.2.1 Choking explanation for pressure variation/reduction</A>
<LI><A NAME="tex2html365"
  HREF="node108.php">8.7.2.2 Short <IMG
 WIDTH="30" HEIGHT="43" ALIGN="MIDDLE" BORDER="0"
 SRC="img17.png"
 ALT="$ \fld$"></A>
<LI><A NAME="tex2html366"
  HREF="node109.php">8.7.2.3 Long <IMG
 WIDTH="30" HEIGHT="43" ALIGN="MIDDLE" BORDER="0"
 SRC="img17.png"
 ALT="$ \fld$"></A>
</UL>
<LI><A NAME="tex2html367"
  HREF="node110.php">8.7.3 Entrance Mach number, <IMG
 WIDTH="28" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img4.png"
 ALT="$ M_1$">, effects</A>
</UL>
<LI><A NAME="tex2html368"
  HREF="node112.php">8.8 The Approximation of the Fanno flow by Isothermal Flow</A>
<LI><A NAME="tex2html369"
  HREF="node113.php">8.9 More Examples</A>
</UL>
<BR>
<LI><A NAME="tex2html370"
  HREF="node114.php">9. RAYLEIGH FLOW</A>
<UL>
<LI><A NAME="tex2html371"
  HREF="node115.php">9.1 Introduction</A>
<LI><A NAME="tex2html372"
  HREF="node116.php">9.2 Governing Equation</A>
</UL>
<BR>
<LI><A NAME="tex2html373"
  HREF="node117.php">10. REAL GAS EFFECTS</A>
<LI><A NAME="tex2html374"
  HREF="node118.php">11. Evacuating and Filling a Semi Rigid Chambers</A>
<UL>
<LI><A NAME="tex2html375"
  HREF="node119.php">11.1 Governing equations and Assumptions</A>
<LI><A NAME="tex2html376"
  HREF="node120.php">11.2 General model and Non-dimensionalization </A>
<UL>
<LI><A NAME="tex2html377"
  HREF="node121.php">11.2.1 Isentropic process</A>
<LI><A NAME="tex2html378"
  HREF="node122.php">11.2.2 A Note on the entrance Mach number</A>
</UL>
<LI><A NAME="tex2html379"
  HREF="node123.php">11.3 Rapid evacuating of a rigid tank </A>
<UL>
<LI><A NAME="tex2html380"
  HREF="node124.php">11.3.1 Filling process</A>
<LI><A NAME="tex2html381"
  HREF="node125.php">11.3.2 The isothermal process</A>
<LI><A NAME="tex2html382"
  HREF="node126.php">11.3.3 Simple Semi Rigid Chamber</A>
<LI><A NAME="tex2html383"
  HREF="node127.php">11.3.4 The ``simple'' General Case</A>
</UL>
<LI><A NAME="tex2html384"
  HREF="node128.php">11.4 Evacuation and Filling of Chambers thorough a nozzle</A>
</UL>
<BR>
<LI><A NAME="tex2html385"
  HREF="node129.php">12. Evacuating/Filing Chambers under External Volume Control </A>
<UL>
<LI><A NAME="tex2html386"
  HREF="node130.php">12.1 Model</A>
<UL>
<LI><A NAME="tex2html387"
  HREF="node131.php">12.1.1 Rapid Process</A>
<LI><A NAME="tex2html388"
  HREF="node132.php">12.1.2 Examples</A>
<LI><A NAME="tex2html389"
  HREF="node133.php">12.1.3 Direct Connection</A>
<LI><A NAME="tex2html390"
  HREF="node134.php">12.1.4 Summary</A>
</UL>
</UL>
<BR>
<LI><A NAME="tex2html391"
  HREF="node135.php">13. Topics in Unsteady one Dimensional gas dynamics</A>
<LI><A NAME="tex2html392"
  HREF="node136.php">14. Oblique-Shock</A>
<UL>
<LI><A NAME="tex2html393"
  HREF="node137.php">14.1 Preface to Oblique Shock</A>
<LI><A NAME="tex2html394"
  HREF="node138.php">14.2 Introduction </A>
<UL>
<LI><A NAME="tex2html395"
  HREF="node139.php">14.2.1 Introduction to Oblique Shock</A>
<LI><A NAME="tex2html396"
  HREF="node140.php">14.2.2 Introduction to Prandtl-Meyer Function</A>
<LI><A NAME="tex2html397"
  HREF="node141.php">14.2.3 Introduction to zero inclination</A>
</UL>
<LI><A NAME="tex2html398"
  HREF="node142.php">14.3 Oblique Shock</A>
<LI><A NAME="tex2html399"
  HREF="node143.php">14.4 Solution</A>
<UL>
<LI><A NAME="tex2html400"
  HREF="node144.php">14.4.1 Upstream Mach number, <IMG
 WIDTH="28" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img4.png"
 ALT="$ M_1$">, and deflection
angle, <IMG
 WIDTH="12" HEIGHT="16" ALIGN="BOTTOM" BORDER="0"
 SRC="img8.png"
 ALT="$ \delta $"></A>
<LI><A NAME="tex2html401"
  HREF="node145.php">14.4.2 In What Situations No Oblique Shock Exist or When <IMG
 WIDTH="53" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img9.png"
 ALT="$ D&gt;0$"></A>
<UL>
<LI><A NAME="tex2html402"
  HREF="node146.php">14.4.2.1 Large deflection angle for given, <IMG
 WIDTH="28" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img4.png"
 ALT="$ M_1$"></A>
<LI><A NAME="tex2html403"
  HREF="node147.php">14.4.2.2 The special case of <IMG
 WIDTH="54" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img11.png"
 ALT="$ D\geq 0$"> of  <!-- MATH
 $\delta \geq 0$
 -->
<IMG
 WIDTH="47" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="img12.png"
 ALT="$ \delta \geq 0$"></A>
</UL>
<LI><A NAME="tex2html404"
  HREF="node148.php">14.4.3 Upstream Mach Number, <IMG
 WIDTH="28" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img4.png"
 ALT="$ M_1$">,  and Shock Angle,
<IMG
 WIDTH="12" HEIGHT="16" ALIGN="BOTTOM" BORDER="0"
 SRC="img13.png"
 ALT="$ \theta $"></A>
<LI><A NAME="tex2html405"
  HREF="node149.php">14.4.4 For Given Two Angles, <IMG
 WIDTH="12" HEIGHT="16" ALIGN="BOTTOM" BORDER="0"
 SRC="img8.png"
 ALT="$ \delta $"> and <IMG
 WIDTH="12" HEIGHT="16" ALIGN="BOTTOM" BORDER="0"
 SRC="img13.png"
 ALT="$ \theta $"> </A>
<LI><A NAME="tex2html406"
  HREF="node150.php">14.4.5 Flow in semi-2D shape</A>
<LI><A NAME="tex2html407"
  HREF="node151.php">14.4.6 Small <IMG
 WIDTH="12" HEIGHT="16" ALIGN="BOTTOM" BORDER="0"
 SRC="img8.png"
 ALT="$ \delta $"> ``Weak Oblique shock''</A>
<LI><A NAME="tex2html408"
  HREF="node152.php">14.4.7 Close and Far Views of The Oblique Shock</A>
<LI><A NAME="tex2html409"
  HREF="node153.php">14.4.8 Maximum value of of Oblique shock</A>
<LI><A NAME="tex2html410"
  HREF="node154.php">14.4.9 Examples</A>
<LI><A NAME="tex2html411"
  HREF="node155.php">14.4.10 Detached shock</A>
<LI><A NAME="tex2html412"
  HREF="node156.php">14.4.11 Application of oblique shock</A>
<LI><A NAME="tex2html413"
  HREF="node157.php">14.4.12 Optimization of design</A>
<LI><A NAME="tex2html414"
  HREF="node158.php">14.4.13 Breakage of the design</A>
</UL>
<LI><A NAME="tex2html415"
  HREF="node159.php">14.5 Summary</A>
<LI><A NAME="tex2html416"
  HREF="node160.php">14.6 Appendix: Oblique Shock Stability Analysis</A>
</UL>
<BR>
<LI><A NAME="tex2html417"
  HREF="node161.php">15. Prandtl-Meyer function</A>
<LI><A NAME="tex2html418"
  HREF="node162.php">16. Topics in Steady state Two Dimensional flow</A>
<LI><A NAME="tex2html419"
  HREF="node163.php">A. Computer Program</A>
<UL>
<LI><A NAME="tex2html420"
  HREF="node164.php">A.1 About the Program</A>
<LI><A NAME="tex2html421"
  HREF="node165.php">A.2 Usage</A>
<LI><A NAME="tex2html422"
  HREF="node166.php">A.3 Program listings</A>
</UL></UL>
<!--End of Table of Child-Links-->
<HR>
<!--Navigation Panel-->
<A NAME="tex2html256" HREF="node1.php">
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next"
 SRC="figures/next.png"></A> 
<A NAME="Potto Project" HREF="../index.php">
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up"
 SRC="figures/up.png"></A> 
<A NAME="Potto Project" HREF="../index.php">
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous"
 SRC="figures/prev.png"></A> 
<A NAME="tex2html254"
  HREF="node1.php">
<IMG WIDTH="65" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="contents"
 SRC="figures/contents.png"></A>  
<BR>
<B> Next:</B> <A NAME="tex2html257"
  HREF="node1.php">Contents</A>
 &nbsp; <B>  <A NAME="tex2html255"
  HREF="node1.php">Contents</A></B> 
<!--End of Navigation Panel-->
<ADDRESS>
<I>Genick Bar-Meir <BR>
 copyright Dec 30, 2005 </I>
</ADDRESS>
