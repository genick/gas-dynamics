// $Id: WidDoubleEdit.cxx,v 1.5 2009/03/19 15:53:33 khomich Exp $
//
// WidDoubleEdit - Widget for floating point input with or without slider
//
#undef QT3_SUPPORT
#include <iostream>
#include <math.h>
#include <limits.h>
#include <QtGui/qlayout.h>

#include "doubleEdit.h"

const int WidDoubleEdit::m_SLMIN= (10*SHRT_MIN);
const int WidDoubleEdit::m_SLMAX= (10*SHRT_MAX);
const int WidDoubleEdit::m_SLPAGE= ((int)USHRT_MAX);
/*  If tick drawing is to be used, the value range for QSlider must
    not be the whole int range since the calculation of the tick positions
    (in int variables) will then lead to an overflow.  */

//********************************************************************
//              Constructor
//********************************************************************
WidDoubleEdit::WidDoubleEdit( double bottom, double top, int decimals,
   SliderType sltype, const char *label, QWidget *parent, const char *name)
 : QWidget( parent)
{
  setObjectName(name);
  if( top<=bottom )
    top= bottom+1.0;     // garbage in, garbage out...
  m_bottom= bottom;
  m_range= top-bottom;
  if( decimals<0 )
    decimals= 0;
  m_decimals= decimals;
  if( sltype==LogSlider && bottom<0.0 )
      sltype= LinSlider;
  m_sltype= sltype;
  if( sltype==LogSlider ) {
    m_logbot= log(bottom);
    m_logrange= log(top)-m_logbot;
  }
  else
    m_logbot= m_logrange= 0;
  m_last_sigval= m_bottom-1.0;
  
  QVBoxLayout *toplayout= new QVBoxLayout( this );
  QHBoxLayout *linelayout= new QHBoxLayout( );
  toplayout->addItem(linelayout);
  
  if( label )
  {
    m_label= new QLabel( QString(label), this );
    m_label->setMinimumSize(m_label->sizeHint());
    linelayout->addWidget( m_label );
    linelayout->addSpacing( 10 );
  }
  else
    m_label= 0;
  
  m_edit= new QLineEdit( this );
  m_validator= new QDoubleValidator( bottom, top, decimals, this );
  m_edit->setValidator( m_validator );
  linelayout->addWidget( m_edit );
  connect( m_edit, SIGNAL(textChanged(const QString&)),
                   SLOT(handle_edchange(const QString&)) );
  connect( m_edit, SIGNAL(returnPressed()), SLOT(handle_edreturn()) );
  
  if( sltype!=NoSlider )
  {
    toplayout->addSpacing( 5 );
    m_slider= new QSlider( Qt::Horizontal, this );
    m_slider->setMinimum(m_SLMIN);
    m_slider->setMaximum(m_SLMAX);
    m_slider->setPageStep(m_SLPAGE);
    m_slider->setValue(m_SLMIN);
    m_slider->setSingleStep(m_SLPAGE/10);
    m_slider->setPageStep(m_SLPAGE);
    m_slider->setTickInterval( m_SLPAGE );
    m_slider->setTickPosition( QSlider::TicksBelow );
    toplayout->addWidget( m_slider );
    connect( m_slider, SIGNAL(valueChanged(int)), SLOT(handle_slchange(int)));
  }
  else
    m_slider= 0;
  
  setTracking( false, false );
  setUpdateTracking( false, false );
  m_lock= false;
  setValue( m_bottom );
  
}  // Constructor


//********************************************************************
//              value
//********************************************************************
double WidDoubleEdit::value()
{
  return get_txtvalue( m_edit->text() );
}


//********************************************************************
//              setRange
//********************************************************************
void WidDoubleEdit::setRange( double bottom, double top )
{
  if( bottom==m_bottom && top==m_bottom+m_range )
    return;
  
  double currval;
  
  if( top<=bottom )
    top= bottom+1.0;     // garbage in, garbage out...
  m_bottom= bottom;
  m_range= top-bottom;
  if( m_sltype==LogSlider ) {
    if( bottom<0.0 )
      m_sltype= LinSlider;
    else {
      m_logbot= log(bottom);
      m_logrange= log(top)-m_logbot;
    }
  }
  
  currval= get_txtvalue( m_edit->text() );
  m_validator->setRange( bottom, top, m_decimals );
  
  if( currval<bottom ) {
    currval= bottom;
    update_edit( currval );
  }
  else if( currval>top ) {
    currval= top;
    update_edit( currval );
  }
  if( m_slider )
    update_slider( currval );
  
}


//********************************************************************
//              setPrecision
//********************************************************************
void WidDoubleEdit::setPrecision( int decimals )
{
  if( decimals<0 || m_decimals==decimals )
    return;
  
  double currval, newval;
  
  m_decimals= decimals;
  currval= get_txtvalue( m_edit->text() );
  update_edit( currval );
  if( m_slider && (newval= get_txtvalue(m_edit->text()))!=currval )
    update_slider( newval );
  
}


//********************************************************************
//********************************************************************
//              setTracking
//********************************************************************
void WidDoubleEdit::setTracking( bool track_sl, bool track_ed )
{
  m_track_sl= track_sl;
  m_track_ed= track_ed;
  if( m_slider )
    m_slider->setTracking( m_track_sl || m_trup_sl );
}


//********************************************************************
//              setUpdateTracking
//********************************************************************
void WidDoubleEdit::setUpdateTracking( bool track_sl, bool track_ed )
{
  m_trup_sl= track_sl;
  m_trup_ed= track_ed;
  if( m_slider )
    m_slider->setTracking( m_track_sl || m_trup_sl );
}


//********************************************************************
//              setLabel
//********************************************************************

void WidDoubleEdit::setLabel( const QString label )
{
  if( m_label )
    m_label->setText( label );
}


//********************************************************************
//              setValue
//********************************************************************
void WidDoubleEdit::setValue( double val )
{
  if( val<m_bottom )
    val= m_bottom;
  else if( val>m_bottom+m_range )
    val= m_bottom+m_range;
  
  if( m_slider )
    update_slider( val );
  update_edit( val );
  m_last_sigval= val;
    // so no signals are generated if this value stays the same
}


//********************************************************************
//          private slots:
//********************************************************************

//********************************************************************
//              handle_slchange
//********************************************************************

void WidDoubleEdit::handle_slchange( int slval )
{
  if( m_lock )
    return;
  
  double val= get_slidval( slval );
  
  if( m_trup_sl || !m_track_sl )
    update_edit( val );
  if( (m_track_sl || !m_trup_sl) && val!=m_last_sigval ) {
    m_last_sigval= val;
    emit valueChanged( val );
  }
}


//********************************************************************
//              handle_edchange
//********************************************************************

void WidDoubleEdit::handle_edchange( const QString &text )
{
  if( m_lock )
    return;
  
  double val= get_txtvalue( text );
  
  if( m_trup_ed )
    update_slider( val );
  if( m_track_ed && val!=m_last_sigval ) {
    m_last_sigval= val;
    m_last_sigval= val;
    emit valueChanged( val );
  }
}


//********************************************************************
//              handle_edreturn
//********************************************************************

void WidDoubleEdit::handle_edreturn()
{
  double val= get_txtvalue( m_edit->text() );
  
  update_slider( val );
  if( !m_track_ed && val!=m_last_sigval ) {
    m_last_sigval= val;
    emit valueChanged( val );
  }
  emit returnPressed();
}

//********************************************************************
//          private methods:
//********************************************************************

//********************************************************************
//              update_edit
//********************************************************************

void WidDoubleEdit::update_edit( double val )
{
  QString newtext;
  bool lock;
  
  newtext.setNum( val, 'f', m_decimals );
  lock= m_lock;
  m_lock= true;   // ignore textChanged signals resulting from update
  m_edit->setText( newtext );
  m_lock= lock;
}

//********************************************************************
//              update_slider
//********************************************************************

void WidDoubleEdit::update_slider( double val )
{
  if( isnan(val) )
    return;
  
  bool lock= m_lock;
  m_lock= true;   // ignore valueChanged signals resulting from update
  if( val<m_bottom )
    m_slider->setValue( m_SLMIN );
  else if( val>m_bottom+m_range )
    m_slider->setValue( m_SLMAX );
  else {
    if( m_sltype==LogSlider )
      val= (log(val)-m_logbot)/m_logrange;
    else
      val= (val-m_bottom)/m_range;
    m_slider->setValue( (int)((double)m_SLMIN +
                         ((double)m_SLMAX-(double)m_SLMIN)*val) );
  }
  m_lock= lock;
  
}

//********************************************************************
//              get_txtvalue
//********************************************************************

double WidDoubleEdit::get_txtvalue( QString text )
{
  double val;
  bool success;
  
  val= text.toDouble( &success );
  if( !success )
    return sqrt( (double)-1.0 );   // return NaN
  else
    return val;
}


//********************************************************************
//              get_slidval
//********************************************************************

double WidDoubleEdit::get_slidval( int slval )
{
  double val;
  
  val= ((double)slval-(double)m_SLMIN)/((double)m_SLMAX-(double)m_SLMIN);
  if( m_sltype==LogSlider )
    return exp(m_logbot + m_logrange*val);
  else
    return m_bottom + m_range*val;
}

