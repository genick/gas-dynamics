#ifndef STGUI_H
#define STGUI_H 

#	 include <QWidget>
#	 include <QMainWindow>
#	 include <QPushButton>
#	 include <QLabel>
#	 include <QString>
#	 include <QVector>
#   include "showResults.h"
#   include "doubleEdit.h"


//class stGui : public QWidget
class stGui : public QWidget
{
  Q_OBJECT

	public:
		stGui(QWidget *parent = 0);
		QString fileName ; 
		void saveData();

	public slots:
		

	private slots:
		void OnPlus();
		void OnMinus();
		void onCalculate () ;

  private:
		QLabel *label;
		ShowResults *other ;
		bool isShowResults ;
		QVector<WidDoubleEdit> vect;
		WidDoubleEdit *kInput;
		WidDoubleEdit *T1Input;
		WidDoubleEdit *LeInput;
		//WidDoubleEdit *RInput;
		WidDoubleEdit *P2Input;
		WidDoubleEdit *UpInput;
		WidDoubleEdit *fldInput;
		WidDoubleEdit *VdeltaInput;
		WidDoubleEdit *cyclesInput;
		WidDoubleEdit *frequencyInput;
		WidDoubleEdit *VratioInput;
		//ParametersList paraList;


/*********************************
	//Q_OBJECT
	public:
		stGui(QWidget *parent = 0);

	private slots:
		void OnApplyPressed () ;
		void OnPlus() ;

	private:
		QPushButton *calculate;
		QPushButton *quit;
		QLabel *label;
*******************************************/

};

#endif // end of STGUI_H

