#ifndef SHOWRESULTS_H
#define SHOWRESULTS_H 

#	include <QWidget>
#	include <QMainWindow>
#	include <QPushButton>
#	include <QTextEdit>
#	include <QStringList>
#	include <QString>
#	include <QTextBrowser>
#  include <QProcess>
#	include <QTextStream>
#	include <QByteArray>

#	include "message.h"

class ShowResults : public QWidget
{
	Q_OBJECT
	public:
		ShowResults(QWidget *parent = 0);
		void launch();

	signals:
		void messageSent(const Message &message);

	public slots:
		void setMessage(const Message &message);
		void readFromStdout();

	private slots:
		void OnClearPressed();
		void onClose();
		void onCalculate();
		void sendMessage();

	private:
		QPushButton *clear;
		QPushButton *close;
		QTextBrowser *output;
		Message thisMessage;
		QProcess  process;
		QString executablePath;
		QStringList arguments;
		int curPosX;
};

#endif // end of SHOWRESULTS_H

