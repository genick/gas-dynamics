#	include <QGridLayout>
#	include <QLabel>
#	include <QLineEdit>
#	include <QMainWindow>
#	include <QPushButton>
#	include <QVBoxLayout>
#	include <QApplication>
#	include <QListWidget>
#	include <QProcess>
#	include <QMessageBox>
#	include <QLibraryInfo>
#	include <QDir>


#	include "showResults.h"

ShowResults::ShowResults(QWidget *parent) : QWidget(parent)
{
	QVBoxLayout *vbox = new QVBoxLayout(this);
	QHBoxLayout *hbox = new QHBoxLayout();

	output = new QTextBrowser();
	
	vbox->setSpacing(2);
	vbox->addWidget(output);
	// buttons 
	clear = new QPushButton("Clear", this);
	close = new QPushButton("Close", this);

	hbox->addWidget(close);
	hbox->addWidget(clear);
	vbox->addLayout(hbox);
	connect(close, SIGNAL(clicked()), this, SLOT(close()));
	connect(clear, SIGNAL(clicked()), this, SLOT(OnClearPressed()));
	//connect( process, SIGNAL(readyReadStdout()), this, SLOT(readFromStdout()) );
	// toput the result out
	QTextStream qout(stdout);
	QProcess cmd;

	setLayout(vbox);
	launch() ;
};

void ShowResults::onCalculate()
{
	//return ;
  //statusBar()->showMessage("OK button pressed", 2000);
};

void ShowResults::onClose()
{
	//return ;
  //statusBar()->showMessage("OK button pressed", 2000);
};

void ShowResults::OnClearPressed()
{
	output->setPlainText("");
	return ;
 //statusBar()->showMessage("Apply button pressed", 2000);
}

void ShowResults::sendMessage()
{
    thisMessage = Message(output->toPlainText(), thisMessage.headers());
    emit messageSent(thisMessage);
}

void ShowResults::setMessage(const Message &message)
{
    thisMessage = message;
    output->setPlainText(thisMessage.body());
}

void ShowResults::launch()
{
//	executablePath = "/home/genick/books/compressibleFlow/qdc/gd/gd" ;
//	arguments << " -g" << " " ;
//	process.start(executablePath, arguments);
	//process.start("pwd");
	QTextStream out(stdout);
	// if (!(process))

    QString executablePath;
    QStringList arguments;
	executablePath =  QDir::cleanPath(QDir::currentPath() + QLatin1Char('/') + "gd");
	arguments << "-g";


	// process = new QProcess();
	//process.start("ls -l");
	process.setProcessChannelMode(QProcess::ForwardedChannels);
	process.start(executablePath, arguments);
	
	if (!process.waitForStarted())  {
		return ;
	}
	process.waitForReadyRead();
	//output->setPlainText(process.readAllStandardOutput() );
	QByteArray result = process.readAll();
	//result = process.readAllStandardError ();
	//QTextStream out(stdout);
	out << result;
	output->setPlainText(result.data());
}

void ShowResults::readFromStdout()
{
//	// Read and process the data.
//	//curPosX = output-\begin{comment}
//	curPosX = output->contentsHeight() - 30 ;
//	output->append( process.readStdout() );
}


