/****************************************************************************
** Meta object code from reading C++ file 'showResults.h'
**
** Created: Tue Nov 27 13:51:20 2012
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "showResults.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'showResults.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ShowResults[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      21,   13,   12,   12, 0x05,

 // slots: signature, parameters, type, tag, flags
      42,   13,   12,   12, 0x0a,
      62,   12,   12,   12, 0x0a,
      79,   12,   12,   12, 0x08,
      96,   12,   12,   12, 0x08,
     106,   12,   12,   12, 0x08,
     120,   12,   12,   12, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ShowResults[] = {
    "ShowResults\0\0message\0messageSent(Message)\0"
    "setMessage(Message)\0readFromStdout()\0"
    "OnClearPressed()\0onClose()\0onCalculate()\0"
    "sendMessage()\0"
};

void ShowResults::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ShowResults *_t = static_cast<ShowResults *>(_o);
        switch (_id) {
        case 0: _t->messageSent((*reinterpret_cast< const Message(*)>(_a[1]))); break;
        case 1: _t->setMessage((*reinterpret_cast< const Message(*)>(_a[1]))); break;
        case 2: _t->readFromStdout(); break;
        case 3: _t->OnClearPressed(); break;
        case 4: _t->onClose(); break;
        case 5: _t->onCalculate(); break;
        case 6: _t->sendMessage(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ShowResults::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ShowResults::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_ShowResults,
      qt_meta_data_ShowResults, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ShowResults::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ShowResults::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ShowResults::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ShowResults))
        return static_cast<void*>(const_cast< ShowResults*>(this));
    return QWidget::qt_metacast(_clname);
}

int ShowResults::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void ShowResults::messageSent(const Message & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
