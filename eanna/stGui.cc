#	include <QGridLayout>
#	include <QLabel>
#	include <QLineEdit>
#	include <QMainWindow>
#	include <QPushButton>
#	include <QVBoxLayout>
#	include <QApplication>
#	include <QListWidget>
#	include <QTextStream>
//#	include <QtGui>

#	include "stGui.h"
#	include "showResults.h"
#	include "iniFile.h"

stGui::stGui(QWidget *parent) : QWidget(parent)
{
	//QString values[16] = { "A:", "P", "T", "/", "4", "5", "6", "*",
	//		 "1", "2", "3", "-", "0", ".", "=", "+" };
	kInput = new WidDoubleEdit (1.2, 2.0, 5, WidDoubleEdit::LinSlider, "Cp/Cv=k", this, "kInput") ;
	kInput->setValue(1.4);
	//kInput->setEnabled(true); //kInput->setShown(true);
	T1Input = new WidDoubleEdit (250., 500., 5, WidDoubleEdit::LinSlider, "T1", this, "T1Input") ;
	T1Input->setValue(300.);
	LeInput = new WidDoubleEdit (0.1, 20.0, 5, WidDoubleEdit::LinSlider, "V1/Apipe", this, "LeInput") ;
	LeInput->setValue(.5);
	//RInput = new WidDoubleEdit (200., 500., 5, WidDoubleEdit::LinSlider, "R", this, "P1Input") ;
	//RInput->setValue(287.);
	P2Input = new WidDoubleEdit (.10, 2.0, 5, WidDoubleEdit::LinSlider, "P2", this, "P2Input") ;
	P2Input->setValue(1.);
	UpInput = new WidDoubleEdit (0.2, 5.0, 5, WidDoubleEdit::LogSlider , "Up", this, "UpInput") ;
	UpInput->setValue(1.);
	fldInput = new WidDoubleEdit (.50, 50., 5, WidDoubleEdit::LogSlider, "fld", this, "fldInput") ;
	fldInput->setValue(10.);
	VdeltaInput = new WidDoubleEdit (0.0, 0.8, 5, WidDoubleEdit::LinSlider, "Vdelta", this, "VdeltaInput") ;
	VdeltaInput->setValue(.4);
	cyclesInput = new WidDoubleEdit (1.2, 20.0, 5, WidDoubleEdit::LinSlider, "cycles", this, "cyclesInput") ;
	cyclesInput->setValue(10.);
	frequencyInput = new WidDoubleEdit (1.2, 200.0, 5, WidDoubleEdit::LogSlider, "frequency", this, "frequencyInput") ;
	frequencyInput->setValue(5.);
	VratioInput = new WidDoubleEdit (.2, 20.0, 5, WidDoubleEdit::LinSlider, "Vratio", this, "VratioInput") ;
	VratioInput->setValue(2.0);

	//vect.append(kInput);
	// material for saving the data   
	fileName = ".dammy.dammy"; 
	//end material saving           
	QGridLayout *box = new QGridLayout();
	box->setSpacing(3);
	//// different layout
	QVBoxLayout *vbox = new QVBoxLayout(this);
	QHBoxLayout *hbox = new QHBoxLayout();
	vbox->setSpacing(3);
	vbox->addStretch(1);
	isShowResults = false ;
	//int lastLabel = 4;
	//for (int i=0; i< lastLabel ; i++) {
		//QLabel *label = new QLabel(values[i], this);
		//QLineEdit *inputBox = new QLineEdit(this);
		//box->addWidget(label, i, 0);
		//box->addWidget(inputBox, i, 1);
	//}

	//QPushButton *plus = new QPushButton("+", this);
	//QPushButton *minus = new QPushButton("-", this);
	QPushButton *calculate = new QPushButton("calculate", this);
	QPushButton *quit = new QPushButton("Quit", this);
  //plus->setGeometry(50, 40, 75, 30);
  /************** total grid
	box->addWidget(plus, lastLabel, 0);
	box->addWidget(minus, lastLabel, 1);
	box->addWidget(calculate, lastLabel, 2);
	box->addWidget(quit, lastLabel, 3);
	*************end total grid*/

	//minus->setGeometry(50, 80, 75, 30);
	//calculate->setGeometry(50, 120, 75, 30);
	//quit->setGeometry(50, 160, 75, 30);

  label = new QLabel("0", this);
  label->setGeometry(190, 80, 20, 30);

	//connect(plus, SIGNAL(clicked()), this, SLOT(OnPlus()));
	//connect(minus, SIGNAL(clicked()), this, SLOT(OnMinus()));
	connect(calculate, SIGNAL(clicked()), this, SLOT(onCalculate()));
	connect(quit, SIGNAL(clicked()), qApp, SLOT(quit()));
	//setLayout(box);
	vbox->addWidget(kInput);
	vbox->addWidget(T1Input);
	vbox->addWidget(LeInput);
	vbox->addWidget(P2Input);
	vbox->addWidget(UpInput);
	vbox->addWidget(fldInput);
	vbox->addWidget(VdeltaInput);
	vbox->addWidget(cyclesInput);
	vbox->addWidget(frequencyInput);
	vbox->addWidget(fldInput);
	vbox->addWidget(VratioInput);
	//vbox->addLayout(box);
	//vbox->addWidget(RInput);
	hbox->addWidget(calculate);
	hbox->addSpacing(30);
	hbox->addWidget(quit);
	vbox->addLayout(hbox);

	setLayout(vbox);
}

void stGui::OnPlus()
{
  int val = label->text().toInt();
  val++;
  label->setText(QString::number(val));
}

void stGui::OnMinus()
{
  int val = label->text().toInt();
  val--;
  label->setText(QString::number(val));
}

void stGui::onCalculate()
{
	//QTextStream out(stdout);
	//out << "we are out here" ; 
	saveData() ;	

	if (!isShowResults){
		//out << "we are in here" ; 
		
		other = new ShowResults;
		other->show();
		//other->launch();
		isShowResults = true ;
	}
	
};

void	stGui::saveData() {
	QTextStream out(stdout);
	//out << "we are save here" ; 
	//double tmpValue = 0.;
	string fileName;
	fileName = ".dammy.dammy";
	//double k ;
	//k =  kInput->value();
	//out << "k= " << k ;

	CIniFile iniFile(fileName);
	iniFile.HeaderComment("This file was created by Eanna Calculator.");
	iniFile.HeaderComment ("This file can be edited if you know what you doing." );
	iniFile.HeaderComment ("You also can contact Dr. Bar-Meir for more consaltation." );

	iniFile.SetValueF( "data", "k", kInput->value() );
	iniFile.SetValueF( "data", "T1", T1Input->value() );
	iniFile.SetValueF( "data", "L0", 1. );
	iniFile.SetValueF( "data", "Le", LeInput->value() );
	//iniFile.SetValueF( "data", "R", RInput->value() );
	iniFile.SetValueF( "data", "P2", P2Input->value() );
	iniFile.SetValueF( "data", "Up", UpInput->value() );
	iniFile.SetValueF( "data", "fld", 10. );
	iniFile.SetValueF( "data", "Vdelta", 0.4 );
	iniFile.SetValueF( "data", "cycles", 4.0 );
	iniFile.SetValueF( "data", "frequency", 1.0 );
	iniFile.SetValueF( "data", "Vratio", 2.0 );


	iniFile.SetValue( "integer", "isTex", "no" );
	iniFile.SetValue( "integer", "isHTML", "no" );
	iniFile.SetValue( "integer", "isRange", "no" );

	iniFile.SetValue( "InputOutput", "whatInfo", "infoStandard" );
	iniFile.SetValue( "InputOutput", "variableName", "P1V" );
	iniFile.SetValue( "InputOutput", "className", "twoChambersFlow" );

	iniFile.WriteFile();

	return ;
};

