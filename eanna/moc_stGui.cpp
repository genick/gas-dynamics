/****************************************************************************
** Meta object code from reading C++ file 'stGui.h'
**
** Created: Tue Nov 27 13:51:21 2012
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "stGui.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'stGui.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_stGui[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
       7,    6,    6,    6, 0x08,
      16,    6,    6,    6, 0x08,
      26,    6,    6,    6, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_stGui[] = {
    "stGui\0\0OnPlus()\0OnMinus()\0onCalculate()\0"
};

void stGui::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        stGui *_t = static_cast<stGui *>(_o);
        switch (_id) {
        case 0: _t->OnPlus(); break;
        case 1: _t->OnMinus(); break;
        case 2: _t->onCalculate(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData stGui::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject stGui::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_stGui,
      qt_meta_data_stGui, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &stGui::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *stGui::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *stGui::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_stGui))
        return static_cast<void*>(const_cast< stGui*>(this));
    return QWidget::qt_metacast(_clname);
}

int stGui::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
