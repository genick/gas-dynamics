                                                          
                                             
Pages: 135-150 (15 pages)
Edited by: Irene dated 27 April 2007

Chapter 8
Isothermal Flow

The gas [+that] flows through a long tube [-has a applicability +is
applicable] in situations [-which occur in +over] a relatively long distance
and where heat transfer is relatively rapid so that the temperature can be
treated, for engineering purposes, as a constant. 

To put discussion [-when the +for what] “relatively [-fast +rapid]” means. 

This model is applicable when a natural gas (or other gases) [-flows for a
large +flow over a long] distance. 

It is more [-predominate +predominant] (more applicable) in situations where
the gas is pumped [-for +over] a length of kilometers.

For instance, in a perfect gas, the density is inverse of the pressure (it has
to be kept in mind that the gas undergoes [-a +an] isothermal process[-.]).  

At critical point the velocity reaches the speed of sound at the exit and
hence the flow will be [-chocked +choked]1.

1This explanation is not correct as it will [+be] shown later on. Close to the
critical point (about, 1=pk[+)], the heat transfer, is relatively high and the
isothermal flow model is not valid anymore. Therefore, the study of the
isothermal flow above this point is [+only an] academic discussion but also
provides the upper limit for [-the] Fanno Flow.

Figure [-(8.1) +8.1] describes the flow of gas from the left to the right. The
heat transfer up stream (or down stream) is assumed to be negligible. Hence,
the energy equation
can be written as [+the] following:

The momentum equation [+is] written as [+the] following
where A is the cross section area (it doesn’t have to be a perfect circle [+;]
a close enough shape is sufficient[-.]).
Perhaps more quantitative [-discussion +discussions] about how “circular”
the shape should be  

The shear stress is the force per area that acts on the fluid by the tube wall
[-on the fluid ?redundant]. 

Again it is assumed that the gas is a perfect gas and therefore, equation of
state [+is] expressed as [+the] following:

In this section the equations are transformed into the dimensionless form and
presented
[-in a dimensionless form +as such]. First it must be recalled that the
temperature is
 constant and [-therefor +therefore], equation of state reads

Now, the Fanning friction factor2 is introduced, this factor is a
dimensionless friction
factor sometimes referred to as the friction coefficient [-as following
?redundant]:

Rearranging equation (8.9) and [-utilizing the identify +using the identity]
for perfect gas M2 =  U2=kP [-yield +yields]:

Now the pressure, P as a function of the Mach number [-have to substitute +has
to be substituted] along with velocity, U.

The [-dimensionalization +dimension] of the mass conservation equation yields


where [-is +are] the stagnation equations? put them in a table

2It should be noted that [+the] Fanning factor [+is] based on hydraulic
radius, instead of Diameter friction equation, thus [-”+“]Fanning f” values
are only 1/4th of [-”+“]Darcy f” values.


Notice that dT0 6= 0 [-in +is an] isothermal flow. 

[-Utilizing +By utilizing] the momentum equation [-also requires +it is
possible] to obtain a relation between the pressure and density[+.] [-and
recalling +Recall] that [-in +an] isothermal flow (T = 0) yields

One can [-noticed +notice] that there are two possible solutions (because
[+of] the square power). These different solutions are [-super sonic
+supersonic] and subsonic solution.

Thus, the equations need to be obtained in the [-form] variable [+form] as a
function of 4fL D . The density is eliminated from equation (8.15) when
combined with [-the] equation (8.20) to [-became +become]

The variables in equation (8.24) can be separated to obtain [+the] integrable
form as
follows

It can be noticed that at the entrance (x = 0) for which M = M (the initial
velocity
to tube isn’t zero[-.]). The term 4fL D is positive for any x, thus, the term
on [+the] other side has to be positive as well. To obtain this restriction 1
= kM2. Thus, the
value M = 1pk is the limiting case [-where] from a mathematical point of view. 
[+When] Mach number [-larger from] M > 1pk [+, it] makes the right hand side
[-integrate] negative. 

The [-Physical +physical] meaning of this value [+is] similar toM = 1
[-chocked +choked] flow which [-were +was] discussed in a variable area flow
[+in] Chapter 4.
Further it can be noticed from equation (8.26) that when M ! 1
pk the value of right hand side [-approached +approaches] infinity (). Since
the stagnation temperature (T0) has a finite value which means that dT0 ! 1.
Heat transfer [-have +has] a limited value therefore [+the flow] model [-of
the flow] must be changed. A more appropriate model is an adiabatic flow model
yet it can serve as [+a] bounding boundary.


The definition for perfect gas [-yield +yields] M2 = U2=kRT and noticing that
T =
constant is used to describe the relation of the properties at M = 1=pk. 

[-Denote +By denoting] the [-supper script +superscript] [-of] symbol   for
the [-chocking +choking] condition [-and +,] one can obtain that


[-Utilizing +By utilizing] the continuity equation[+, it] provides

These equations (8.30)-(8.35) are presented [-on] in Figure [-(8.2) +8.2]

8.3 The Entrance Limitation [-Of +of] Supersonic Branch
Situations where the conditions at the tube exit have not arrived [-to +at]
the critical conditions are discussed here. 

Denote 1 and 2 [+as] the conditions at the inlet and exit respectably. From
equation (8.24)

For the case that M1 >> M2 and M1 ! 1 equation (8.36) [+is] reduced into the
following approximation


This relationship shows [-that for] the maximum limit that Mach number can
approach
when the heat transfer is [-extraordinary +extraordinarily] fast. In reality,
even small 4fL
D > 2
results in [+a] Mach number which is larger than 4.5.

With this conflicting mechanism obviously the flow is closer to [+the] Fanno
flow model. Yet this model [-provide +provides] the direction [-that +for]
heat transfer effecting the flow.

In these cases, one should expect that the isothermal flow [-to +should] have
similar [-characters  +characteristics] as [-the flow of] incompressible flow. 

To obtain [+a] similar expression for [-the] isothermal flow, a relation
between M2 and M1 and pressures [-has +have] to be derived. 

Substituting this expression into (8.40) [-yield +yields]

Because f is always positive there is only one solution to [+the] above
equation [-even
though +for] M2. [??In case the no solution and in case only one solution to
M2.]
[-Expending +Expanding] the solution for small pressure ratio drop, P1 ??
P2=P1, by some mathematics[-.
denote]

now we have to [-expend +expand] into [+a] series around   = 0 and remember
that

rearranging equation (8.48) [-yield +yields]

and further rearrangement [-yield +yields]

The larger value of the solution is not physically possible because
[??something missing here]

8.5 Supersonic Branch
Apparently, this analysis/model is over simplified for the supersonic branch
and
does not [-produced +produce] reasonable results since it neglects to take
into account the heat transfer effects. A dimensionless analysis4
[-demonstrate +demonstrates] that all the common materials that [-this +the]
author is familiar [-which create +with creates] a large error that [-the
fundamental assumption of the model breaks +breaks the model]. 
Nevertheless, this model can provide a better understanding [-so +to] the
trends and deviations [-from +of the] Fanno flow model [-can be understood
?redundant].

In the supersonic flow, the hydraulic entry length is very large as shown
below [??which diagram]. 

The thermal entry length is in the order of the hydrodynamic entry length
([-Look +look] at the Prandtl number, (0.7-1.0), value for the common
gases[-.]). 

For example, a [+gas] flow [-gas] in a tube with for 4fL
D = 10 the required Mach number is over 200. Almost all the perfect gas model
substances dealt [+with] in this book[+,] the speed of sound is a function of
temperature. For this illustration, [+for] most gas cases[+,] the speed of
sound is about 300[m=sec]. 
So, [-for] even for relatively small tubes with 4fL D = 10 the inlet speed is
over 56 [km/sec]. This requires that the entrance length to be larger than the
actual length of the [+air] [+tube] [-tub for air]. 

On the other hand [-with +a] typical value of friction coefficient f = 0:005
results in

The fact that the actual tube length is only less [+than] 1% [-than +of] the
entry length means that the assumption [-of +is that] the isothermal also
breaks (as in [-control +controlling a] large time [-respond +response]).

T0  = 18:37 which means that [+the] maximum [-of the mount +amount] of energy
is insufficient.
Now with limitation, this topic will be covered in the next version because it
provide some insight and boundary to [+the]  Fanno Flow model.


4This dimensional analysis is a bit tricky, and is based on estimates.
Currently and ashamedly [-this +the] author is looking for a more simplified
explanation. The current explanation is correct but based on hands waving and
definitely [-dose +does] not [-satisfied this  +satisfy the] author.

There can be several [-kind +kinds] of questions aside [+from] the proof
questions6 
6The proof questions are questions that ask for proof or for finding a
mathematical [-intently +intent] (normally good for mathematicians and study
of perturbation methods). 
7Those who are mathematically [+inclined can] include these [-kind +kinds] of
questions [-can exit] but there [-is +are] no real world situations with
isothermal model with shock.

What should be the pump pressure so that a flow rate of 2 [kg=sec] will be
achieved[-.+?]

Hint[+:] calculate the maximum flow rate and then check if this request is
reasonable.

Utilizing [-the table +Table] 8.1 or the program provides


It is more appropriate to assume [+an] isothermal model hence our model is
appropriate.

From [-the table +Table] 8.1 or [+by] utilizing the program [-provides]

The entrance Mach number [+is] obtained by

At first, the minimum diameter will be obtained when the flow is [-chocked
+choked]. 
Now, with the value of M1 either [+by] utilizing Table [-(8.1) +8.1] or using
the provided program yields

8It is unfortunate, but [-is seem +it seems] that this standard will be around
in USA for some time.

With this pipe size the calculations are to be repeated in [-reversed to
+reverse and] produces: (Clearly the maximum mass is determined with)

The usage of the above equation [-clearly +can be] applied to the whole pipe. 

To check whether the flow rate [-is satisfied +satisfies] the requirement

Since 50:3   0:2 the mass flow rate [-requirements +requirement] is satisfied.

Nevertheless, for [+the] sake of the exercise the other parameters will be
calculated. This
situation is [+a] reversed question. 

It should be noted that the flow isn’t [-chocked +choked].

A [+gas flows] [-flow of gas] from a station (a) with pressure of 20[bar]
through a pipe with 0.4[m] diameter and 4000 [m] length to a different station
(b). The pressure at the exit (station (b)) is 2[bar]. 

Calculate the Mach number at the entrance to [+the] pipe and the flow rate.

First, the information whether the flow is choked [-needed +needs] to be
found. 
The pressure at point (b) [+by] utilizing the isentropic relationship (M = 1)
pressure
ratio is 0.52828.

As the pressure at point (b) is smaller [+than] the actual pressure P   <
P2[+,] [-than the actual pressure] one must conclude that the flow is not
choked. The solution is [+an] iterative process.
1. guess [+the reasonable -reasonably the] value of M1 and calculate 4fLD


3. Obtain M2 from the Table ? or [+by] using [-the] Potto–GDC.
4. Calculate the pressure, P2 [+bear] in mind that this isn’t the real
pressure but based on the assumption
5. Compare the results of guessed pressure P2 with the actual pressure[-.] and
[-chose +choose] new [??number] accordingly.
Now the process has been done for you and is provided in [-the] Figure (??) or
in
[+the] table [-resulted] from the provided program.

