\contentsline {chapter}{\numberline {1}Isentropic Flow}{7}{chapter.1}
\contentsline {section}{\numberline {1.1}Regular Isentropic Flow Tables}{7}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Isentropic Flow for k=1.2}{7}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Isentropic Flow for k=1.3}{9}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Isentropic Flow for k =1.4}{12}{subsection.1.1.3}
\contentsline {subsection}{\numberline {1.1.4}Isentropic Flow k=1.67}{14}{subsection.1.1.4}
\contentsline {section}{\numberline {1.2}``Naughty Professor'' Tables (NPT) for Isentropic Flow}{17}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}NPT for Isentropic Flow k=1.2}{17}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}NPT for Isentropic Flow k=1.3}{19}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}NPT for Isentropic Flow k=1.4}{22}{subsection.1.2.3}
\contentsline {subsection}{\numberline {1.2.4}NPT for Isentropic Flow k=1.67}{24}{subsection.1.2.4}
\contentsline {chapter}{\numberline {2}Isothermal Nozzle }{29}{chapter.2}
\contentsline {section}{\numberline {2.1}Isothermal Nozzle for k=1.2}{29}{section.2.1}
\contentsline {section}{\numberline {2.2}Isothermal Nozzle for k=1.3}{33}{section.2.2}
\contentsline {section}{\numberline {2.3}Isothermal Nozzle for k=1.4}{35}{section.2.3}
\contentsline {section}{\numberline {2.4}Isothermal Nozzle for k=1.67}{38}{section.2.4}
\contentsline {chapter}{\numberline {3}Normal Shock }{41}{chapter.3}
\contentsline {section}{\numberline {3.1}Normal Shock Standard tables}{41}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Normal Shock Standard Table for k=1.2}{41}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Normal Shock Standard Table for k=1.3}{43}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Normal Shock Standard Table for k=1.4}{45}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}Normal shock Standard Table for k=1.67}{47}{subsection.3.1.4}
\contentsline {section}{\numberline {3.2}Open Valve maximum values}{50}{section.3.2}
\contentsline {section}{\numberline {3.3}Shock Reflecting from a Suddenly Closed Valve}{50}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Reflecting Shock (closed Valve) k =1.2}{50}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Reflecting Shock (closed Valve) k =1.3}{53}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Reflecting Shock (closed Valve) k =1.4}{55}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}Reflecting Shock (closed Valve) k =1.67}{56}{subsection.3.3.4}
\contentsline {section}{\numberline {3.4}Shock propagating from suddenly open valve}{58}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Shock from suddenly open valve k =1.2}{58}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Shock from suddenly open valve k =1.3}{60}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}Shock from suddenly open valve k =1.4}{63}{subsection.3.4.3}
\contentsline {subsection}{\numberline {3.4.4}Shock from suddenly open valve k =1.67}{65}{subsection.3.4.4}
\contentsline {chapter}{\numberline {4}Isothermal Flow}{69}{chapter.4}
\contentsline {section}{\numberline {4.1}Isothermal Flow k=1.2}{69}{section.4.1}
\contentsline {section}{\numberline {4.2}Isothermal Flow k=1.3}{72}{section.4.2}
\contentsline {section}{\numberline {4.3}Isothermal Flow k=1.4}{75}{section.4.3}
\contentsline {section}{\numberline {4.4}Isothermal Flow k=1.67}{78}{section.4.4}
\contentsline {section}{\numberline {4.5}Unchoked Isothermal Flow k=1.4}{81}{section.4.5}
\contentsline {chapter}{\numberline {5} Fanno Flow}{83}{chapter.5}
\contentsline {section}{\numberline {5.1} Standard Fanno Flow Table for k=1.2 }{83}{section.5.1}
\contentsline {section}{\numberline {5.2} Standard Fanno Flow Table for k=1.3 }{119}{section.5.2}
\contentsline {section}{\numberline {5.3} Standard Fanno Flow Table for k=1.4 }{155}{section.5.3}
\contentsline {section}{\numberline {5.4} Standard Fanno Flow Table for k=1.67 }{191}{section.5.4}
\contentsline {chapter}{\numberline {6}Rayleigh Flow }{229}{chapter.6}
\contentsline {section}{\numberline {6.1}Rayleigh Flow Table for k=1.2}{229}{section.6.1}
\contentsline {section}{\numberline {6.2}Rayleigh Flow Table for k=1.3}{265}{section.6.2}
\contentsline {section}{\numberline {6.3}Rayleigh Flow Table for k=1.4}{300}{section.6.3}
\contentsline {section}{\numberline {6.4}Rayleigh Flow Table for k=1.67}{336}{section.6.4}
\contentsline {chapter}{\numberline {7}Prandtl-Meyer Function}{373}{chapter.7}
\contentsline {section}{\numberline {7.1}Prandtl-Meyer Function for k=1.2}{373}{section.7.1}
\contentsline {section}{\numberline {7.2}Prandtl-Meyer Function for k=1.3}{403}{section.7.2}
\contentsline {section}{\numberline {7.3}Prandtl-Meyer Function for k=1.4}{432}{section.7.3}
\contentsline {section}{\numberline {7.4}Prandtl--Meyer Function for k=1.67}{462}{section.7.4}
\contentsline {chapter}{\numberline {8}Oblique Shock}{493}{chapter.8}
\contentsline {section}{\numberline {8.1}Values at the Deflection Point}{493}{section.8.1}
\contentsline {subsection}{\numberline {8.1.1}Deflection Point for k=1.2}{493}{subsection.8.1.1}
\contentsline {subsection}{\numberline {8.1.2}Deflection Point for k=1.3}{523}{subsection.8.1.2}
\contentsline {subsection}{\numberline {8.1.3}Deflection Point for k=1.4}{553}{subsection.8.1.3}
\contentsline {subsection}{\numberline {8.1.4}Deflection Point for k=1.67}{582}{subsection.8.1.4}
\contentsline {section}{\numberline {8.2}Regular Oblique Shock Figures}{613}{section.8.2}
\contentsline {subsection}{\numberline {8.2.1}Oblique Shock k=1.2}{613}{subsection.8.2.1}
\contentsline {subsection}{\numberline {8.2.2}Oblique Shock k=1.3}{614}{subsection.8.2.2}
\contentsline {subsection}{\numberline {8.2.3}Oblique Shock k=1.4}{615}{subsection.8.2.3}
\contentsline {subsection}{\numberline {8.2.4}Oblique Shock k=1.67}{616}{subsection.8.2.4}
