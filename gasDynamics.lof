\addvspace {10\p@ }
\contentsline {figure}{\numberline {1.1}{\ignorespaces The shock as a connection of Fanno and Rayleigh lines}}{7}{figure.1.1}
\contentsline {figure}{\numberline {1.2}{\ignorespaces The schematic of deLavel's turbine}}{9}{figure.1.2}
\contentsline {figure}{\numberline {1.3}{\ignorespaces The measured pressure in a nozzle}}{10}{figure.1.3}
\contentsline {figure}{\numberline {1.4}{\ignorespaces Flow rate as a function of the back pressure}}{11}{figure.1.4}
\contentsline {figure}{\numberline {1.5}{\ignorespaces Portrait of Galileo Galilei}}{14}{figure.1.5}
\contentsline {figure}{\numberline {1.6}{\ignorespaces Photo of Ernest Mach}}{15}{figure.1.6}
\contentsline {figure}{\numberline {1.7}{\ignorespaces The photo of a bullet in a supersonic flow taken by Mach}}{15}{figure.1.7}
\contentsline {figure}{\numberline {1.8}{\ignorespaces Lord Rayleigh portrait}}{16}{figure.1.8}
\contentsline {figure}{\numberline {1.9}{\ignorespaces Portrait of Rankine}}{17}{figure.1.9}
\contentsline {figure}{\numberline {1.10}{\ignorespaces The photo of Gino Fanno approximately in 1950}}{17}{figure.1.10}
\contentsline {figure}{\numberline {1.11}{\ignorespaces Photo of Prandtl}}{18}{figure.1.11}
\contentsline {figure}{\numberline {1.12}{\ignorespaces The photo and famous diagram of Theodor Meyer}}{20}{figure.1.12}
\contentsline {figure}{\numberline {1.13}{\ignorespaces The photo of Ernst Rudolf George Eckert with Bar-Meir's family}}{21}{figure.1.13}
\addvspace {10\p@ }
\contentsline {figure}{\numberline {2.1}{\ignorespaces Cylinder and piston configuration of maximum work}}{33}{figure.2.1}
\contentsline {figure}{\numberline {2.2}{\ignorespaces Dimensionless work available in a cylinder piston configuration}}{35}{figure.2.2}
\contentsline {figure}{\numberline {2.3}{\ignorespaces Temperature Velocity Pressure}}{36}{figure.2.3}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces The pressure lines}}{36}{figure.2.3}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces The energy lines}}{36}{figure.2.3}
\contentsline {figure}{\numberline {2.4}{\ignorespaces The velocity temperature diagram}}{38}{figure.2.4}
\addvspace {10\p@ }
\contentsline {figure}{\numberline {3.1}{\ignorespaces Schematics of flow in a pipe with varying density}}{44}{figure.3.1}
\contentsline {figure}{\numberline {3.2}{\ignorespaces The explanation for the direction relative to surface}}{55}{figure.3.2}
\contentsline {figure}{\numberline {3.3}{\ignorespaces The work on the control volume}}{56}{figure.3.3}
\contentsline {figure}{\numberline {3.4}{\ignorespaces A long pipe exposed to a sudden pressure difference}}{60}{figure.3.4}
\contentsline {figure}{\numberline {3.5}{\ignorespaces The mass balance on the infinitesimal control volume}}{63}{figure.3.5}
\addvspace {10\p@ }
\contentsline {figure}{\numberline {4.1}{\ignorespaces A very slow moving piston in a still gas}}{68}{figure.4.1}
\contentsline {figure}{\numberline {4.2}{\ignorespaces Stationary sound wave and gas moves relative to the pulse}}{68}{figure.4.2}
\contentsline {figure}{\numberline {4.3}{\ignorespaces The compressibility chart}}{73}{figure.4.3}
\contentsline {figure}{\numberline {4.4}{\ignorespaces The Correction Factor for Time of Sound Wave}}{80}{figure.4.4}
\contentsline {figure}{\numberline {4.5}{\ignorespaces Doppler effect schematic}}{84}{figure.4.5}
\contentsline {figure}{\numberline {4.6}{\ignorespaces Moving object at three relative velocities }}{85}{figure.4.6}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {Object travels at 0.005 of the speed of sound}}}{85}{figure.4.6}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {Object travels at 0.05 of the speed of sound}}}{85}{figure.4.6}
\contentsline {subfigure}{\numberline {(c)}{\ignorespaces {Object travels at 0.15 of the speed of sound}}}{85}{figure.4.6}
\contentsline {figure}{\numberline {4.7}{\ignorespaces The hearing as a function of the frequency}}{92}{figure.4.7}
\contentsline {figure}{\numberline {4.8}{\ignorespaces Schematic of the spherical waves}}{93}{figure.4.8}
\addvspace {10\p@ }
\contentsline {figure}{\numberline {5.1}{\ignorespaces Flow through a converging diverging nozzle}}{99}{figure.5.1}
\contentsline {figure}{\numberline {5.2}{\ignorespaces Perfect gas flows through a tube}}{101}{figure.5.2}
\contentsline {figure}{\numberline {5.3}{\ignorespaces The stagnation properties as a function of the Mach number, $k=1.4$}}{102}{figure.5.3}
\contentsline {figure}{\numberline {5.4}{\ignorespaces Control volume inside a converging-diverging nozzle.}}{104}{figure.5.4}
\contentsline {figure}{\numberline {5.5}{\ignorespaces The relationship between the cross section and the Mach number}}{108}{figure.5.5}
\contentsline {figure}{\numberline {5.6}{\ignorespaces Various ratios as a function of Mach number for isothermal Nozzle}}{129}{figure.5.6}
\contentsline {figure}{\numberline {5.7}{\ignorespaces The comparison of nozzle flow }}{130}{figure.5.7}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {Comparison between the isothermal nozzle and adiabatic nozzle in various variables}}}{130}{figure.5.7}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {The comparison of the adiabatic model and isothermal model}}}{130}{figure.5.7}
\contentsline {figure}{\numberline {5.8}{\ignorespaces Comparison of the pressure and temperature drop (two scales)}}{131}{figure.5.8}
\contentsline {figure}{\numberline {5.9}{\ignorespaces Schematic to explain the significances of the Impulse function}}{133}{figure.5.9}
\contentsline {figure}{\numberline {5.10}{\ignorespaces Schematic of a flow through a nozzle example \textup {\hbox {\mathsurround \z@ \normalfont (\ignorespaces \ref {variableArea:ex:impulse}\unskip \@@italiccorr )}}}}{135}{figure.5.10}
\addvspace {10\p@ }
\contentsline {figure}{\numberline {6.1}{\ignorespaces A shock wave inside a tube}}{145}{figure.6.1}
\contentsline {figure}{\numberline {6.2}{\ignorespaces The intersection of Fanno flow and Rayleigh flow}}{147}{figure.6.2}
\contentsline {figure}{\numberline {6.3}{\ignorespaces The $M_{exit}$ and $P_0$ as a function $M_{upstream}$}}{151}{figure.6.3}
\contentsline {figure}{\numberline {6.4}{\ignorespaces The ratios of the static properties of the two sides of the shock.}}{153}{figure.6.4}
\contentsline {figure}{\numberline {6.5}{\ignorespaces The shock drag diagram }}{155}{figure.6.5}
\contentsline {figure}{\numberline {6.6}{\ignorespaces Stationary and moving coordinates for the moving shock}}{157}{figure.6.6}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces Stationary coordinates}}{157}{figure.6.6}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces Moving coordinates}}{157}{figure.6.6}
\contentsline {figure}{\numberline {6.7}{\ignorespaces The shock drag diagram for moving shock}}{159}{figure.6.7}
\contentsline {figure}{\numberline {6.8}{\ignorespaces The diagram for the common explanation for shock drag}}{160}{figure.6.8}
\contentsline {figure}{\numberline {6.9}{\ignorespaces A shock from close Valve in two coordinates}}{161}{figure.6.9}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces Stationary coordinates}}{161}{figure.6.9}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces Moving coordinates}}{161}{figure.6.9}
\contentsline {figure}{\numberline {6.10}{\ignorespaces The moving shock a result of a sudden stop}}{162}{figure.6.10}
\contentsline {figure}{\numberline {6.11}{\ignorespaces A shock move into still medium: open valve case}}{164}{figure.6.11}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces Stationary coordinates}}{164}{figure.6.11}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces Moving coordinates}}{164}{figure.6.11}
\contentsline {figure}{\numberline {6.12}{\ignorespaces The number of iterations to achieve convergence.}}{164}{figure.6.12}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {${M_y}^{'} = 0.3$}}}{164}{figure.6.12}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {${M_y}^{'}=1.3$}}}{164}{figure.6.12}
\contentsline {figure}{\numberline {6.13}{\ignorespaces Schematic of showing the piston pushing air}}{166}{figure.6.13}
\contentsline {figure}{\numberline {6.14}{\ignorespaces Time the pressure at the nozzle for the French problem}}{168}{figure.6.14}
\contentsline {figure}{\numberline {6.15}{\ignorespaces Max Mach number as a function of $k$.}}{169}{figure.6.15}
\contentsline {figure}{\numberline {6.16}{\ignorespaces Time the pressure at the nozzle for the French problem}}{172}{figure.6.16}
\contentsline {figure}{\numberline {6.17}{\ignorespaces Moving shock as a result of valve opening}}{174}{figure.6.17}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {Stationary coordinates}}}{174}{figure.6.17}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {Moving coordinates}}}{174}{figure.6.17}
\contentsline {figure}{\numberline {6.18}{\ignorespaces The results of the partial opening of the valve}}{175}{figure.6.18}
\contentsline {figure}{\numberline {6.19}{\ignorespaces A shock as a result of partially a valve closing}}{176}{figure.6.19}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {Stationary coordinates}}}{176}{figure.6.19}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {Moving coordinates}}}{176}{figure.6.19}
\contentsline {figure}{\numberline {6.20}{\ignorespaces Schematic of a piston pushing air in a tube}}{182}{figure.6.20}
\contentsline {figure}{\numberline {6.21}{\ignorespaces Figure for Example \ref {shock:ex:twoPistons} }}{184}{figure.6.21}
\contentsline {figure}{\numberline {6.22}{\ignorespaces The shock tube schematic with a pressure ``diagram''}}{185}{figure.6.22}
\contentsline {figure}{\numberline {6.23}{\ignorespaces Maximum Mach number that can be obtained for given specific heats }}{189}{figure.6.23}
\contentsline {figure}{\numberline {6.24}{\ignorespaces The Mach number obtained with various parameters }}{191}{figure.6.24}
\contentsline {figure}{\numberline {6.25}{\ignorespaces Differential element to describe the isentropic pressure}}{193}{figure.6.25}
\contentsline {figure}{\numberline {6.26}{\ignorespaces Porous piston pushing gas}}{194}{figure.6.26}
\contentsline {figure}{\numberline {6.27}{\ignorespaces Initial Shock tube schematic for thermodynamics consideration}}{197}{figure.6.27}
\contentsline {figure}{\numberline {6.28}{\ignorespaces The final or equilibrium stage in the shock tube}}{200}{figure.6.28}
\contentsline {figure}{\numberline {6.29}{\ignorespaces Dimensionless work of shock tube}}{201}{figure.6.29}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {Dimensionless work as a function for various $\xi _e$, $k=1.4$}}}{201}{figure.6.29}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {Dimensionless work as a function for various $k$, $\xi _e=0.4$}}}{201}{figure.6.29}
\contentsline {figure}{\numberline {6.30}{\ignorespaces The equilibrium length as a function of the initial dimensionless length }}{204}{figure.6.30}
\contentsline {figure}{\numberline {6.32}{\ignorespaces Explanation why the ruptured diaphragm cannot reach maximum temperature}}{204}{figure.6.32}
\contentsline {figure}{\numberline {6.31}{\ignorespaces The equilibrium pressure as a function of the initial dimensionless length }}{205}{figure.6.31}
\contentsline {figure}{\numberline {6.33}{\ignorespaces Ames Research Center Shock Tube with Thomas N. Canning}}{207}{figure.6.33}
\contentsline {figure}{\numberline {6.34}{\ignorespaces Figure for Example \textup {\hbox {\mathsurround \z@ \normalfont (\ignorespaces \ref {shock:ex:openValveExample}\unskip \@@italiccorr )}}}}{208}{figure.6.34}
\contentsline {figure}{\numberline {6.35}{\ignorespaces The results for Example \textup {\hbox {\mathsurround \z@ \normalfont (\ignorespaces \ref {shock:ex:openValveExample}\unskip \@@italiccorr )}}}}{209}{figure.6.35}
\addvspace {10\p@ }
\contentsline {figure}{\numberline {7.1}{\ignorespaces $\tmspace +\thinmuskip {.1667em}$ The flow in the nozzle with different back pressures.}}{219}{figure.7.1}
\contentsline {figure}{\numberline {7.2}{\ignorespaces A nozzle with a normal shock}}{220}{figure.7.2}
\contentsline {figure}{\numberline {7.3}{\ignorespaces Clarify the definitions of diffuser efficiency}}{227}{figure.7.3}
\contentsline {figure}{\numberline {7.4}{\ignorespaces Schematic of a supersonic tunnel for Example \textup {\hbox {\mathsurround \z@ \normalfont (\ignorespaces \ref {varibleArea:fig:superSonicTunnel}\unskip \@@italiccorr )}}}}{227}{figure.7.4}
\contentsline {figure}{\numberline {7.5}{\ignorespaces Exit Mach number for non--ideal nozzle general solution}}{234}{figure.7.5}
\contentsline {figure}{\numberline {7.6}{\ignorespaces The ratio of the exit temperature and stagnation temperature}}{236}{figure.7.6}
\addvspace {10\p@ }
\addvspace {10\p@ }
\contentsline {figure}{\numberline {9.1}{\ignorespaces Control volume for isothermal flow}}{241}{figure.9.1}
\contentsline {figure}{\numberline {9.2}{\ignorespaces Working relationships for isothermal flow}}{247}{figure.9.2}
\contentsline {figure}{\numberline {9.3}{\ignorespaces The entrance Mach for isothermal flow for $\genfrac {}{}{}1{4\tmspace +\thinmuskip {.1667em}f\tmspace +\thinmuskip {.1667em}L}{D}$}}{258}{figure.9.3}
\addvspace {10\p@ }
\contentsline {figure}{\numberline {10.1}{\ignorespaces Control volume of the gas flow in a constant cross section }}{261}{figure.10.1}
\contentsline {figure}{\numberline {10.2}{\ignorespaces Various parameters in fanno flow}}{271}{figure.10.2}
\contentsline {figure}{\numberline {10.3}{\ignorespaces Schematic of Example \ref {fanno:ex:reservoir}}}{271}{figure.10.3}
\contentsline {figure}{\numberline {10.4}{\ignorespaces The schematic of Example \textup {\hbox {\mathsurround \z@ \normalfont (\ignorespaces \ref {fanno:ex:ex2}\unskip \@@italiccorr )}}}}{273}{figure.10.4}
\contentsline {figure}{\numberline {10.5}{\ignorespaces The maximum length as a function of specific heat, $k$}}{278}{figure.10.5}
\contentsline {figure}{\numberline {10.6}{\ignorespaces The effects of increase of $\frac {4\tmspace +\thinmuskip {.1667em}f\tmspace +\thinmuskip {.1667em}L}{D}$ on the Fanno line}}{278}{figure.10.6}
\contentsline {figure}{\numberline {10.7}{\ignorespaces The effects of the increase of $\genfrac {}{}{}1{4\tmspace +\thinmuskip {.1667em}f\tmspace +\thinmuskip {.1667em}L}{D}$ on the Fanno Line}}{279}{figure.10.7}
\contentsline {figure}{\numberline {10.8}{\ignorespaces $M_{in}$ and $\mathaccentV {dot}05F{m}$ as a function of the $\unhbox \voidb@x \hbox {$\frac {4fL}{D}$}$}}{279}{figure.10.8}
\contentsline {figure}{\numberline {10.9}{\ignorespaces $M_1$ as a function $M_2$ for various $\unhbox \voidb@x \hbox {$\frac {4fL}{D}$}$}}{281}{figure.10.9}
\contentsline {figure}{\numberline {10.10}{\ignorespaces $\tmspace +\thinmuskip {.1667em}$ $M_1$ as a function $M_2$}}{282}{figure.10.10}
\contentsline {figure}{\numberline {10.11}{\ignorespaces $\tmspace +\thinmuskip {.1667em}$ The pressure distribution as a function of $\frac {4\tmspace +\thinmuskip {.1667em}f\tmspace +\thinmuskip {.1667em}L}{D}$}}{283}{figure.10.11}
\contentsline {figure}{\numberline {10.12}{\ignorespaces Pressure as a function of long $\frac {4\tmspace +\thinmuskip {.1667em}f\tmspace +\thinmuskip {.1667em}L}{D}$}}{284}{figure.10.12}
\contentsline {figure}{\numberline {10.13}{\ignorespaces $\tmspace +\thinmuskip {.1667em}$ The effects of pressure variations on Mach number profile}}{285}{figure.10.13}
\contentsline {figure}{\numberline {10.14}{\ignorespaces $\tmspace +\thinmuskip {.1667em}$ Pressure ratios as a function of $\genfrac {}{}{}1{4\tmspace +\thinmuskip {.1667em}f\tmspace +\thinmuskip {.1667em}L}{D}$ when the total $\genfrac {}{}{}1{4\tmspace +\thinmuskip {.1667em}f\tmspace +\thinmuskip {.1667em}L}{D} = 0.3$}}{286}{figure.10.14}
\contentsline {figure}{\numberline {10.15}{\ignorespaces $\tmspace +\thinmuskip {.1667em}$ Schematic of a ``long'' tube in supersonic branch }}{286}{figure.10.15}
\contentsline {figure}{\numberline {10.16}{\ignorespaces $\tmspace +\thinmuskip {.1667em}$ The extra tube length as a function of the shock location }}{288}{figure.10.16}
\contentsline {figure}{\numberline {10.17}{\ignorespaces $\tmspace +\thinmuskip {.1667em}$ The maximum entrance Mach number as a function of $\unhbox \voidb@x \hbox {$\frac {4fL}{D}$}$ }}{289}{figure.10.17}
\contentsline {figure}{\numberline {10.18}{\ignorespaces $\tmspace +\thinmuskip {.1667em}$ Unchoked flow showing the hypothetical ``full'' tube}}{292}{figure.10.18}
\contentsline {figure}{\numberline {10.19}{\ignorespaces Pressure ratio obtained for fix $\frac {4\tmspace +\thinmuskip {.1667em}f\tmspace +\thinmuskip {.1667em}L}{D}$ for k=1.4 }}{292}{figure.10.19}
\contentsline {figure}{\numberline {10.20}{\ignorespaces Conversion of solution for given $\genfrac {}{}{}0{4\tmspace +\thinmuskip {.1667em}f\tmspace +\thinmuskip {.1667em}L}{D}=0.5$ and pressure ratio}}{294}{figure.10.20}
\contentsline {figure}{\numberline {10.21}{\ignorespaces $\tmspace +\thinmuskip {.1667em}$ The results of the algorithm showing the conversion rate}}{295}{figure.10.21}
\contentsline {figure}{\numberline {10.22}{\ignorespaces $\tmspace +\thinmuskip {.1667em}$ Solution to a missing diameter }}{298}{figure.10.22}
\contentsline {figure}{\numberline {10.23}{\ignorespaces $\tmspace +\thinmuskip {.1667em}$ $M_{1}$ as a function of $\frac {4\tmspace +\thinmuskip {.1667em}f\tmspace +\thinmuskip {.1667em}L}{D}$ comparison with Isothermal Flow}}{299}{figure.10.23}
\contentsline {figure}{\numberline {10.24}{\ignorespaces $\tmspace +\thinmuskip {.1667em}$ ``Moody'' diagram}}{303}{figure.10.24}
\addvspace {10\p@ }
\contentsline {figure}{\numberline {11.1}{\ignorespaces The control volume of Rayleigh Flow}}{305}{figure.11.1}
\contentsline {figure}{\numberline {11.2}{\ignorespaces The temperature entropy diagram for Rayleigh line}}{307}{figure.11.2}
\contentsline {figure}{\numberline {11.3}{\ignorespaces The basic functions of Rayleigh Flow (k=1.4)}}{312}{figure.11.3}
\contentsline {figure}{\numberline {11.4}{\ignorespaces Schematic of the combustion chamber}}{317}{figure.11.4}
\addvspace {10\p@ }
\contentsline {figure}{\numberline {12.1}{\ignorespaces The two different classifications of models}}{319}{figure.12.1}
\contentsline {figure}{\numberline {12.2}{\ignorespaces Comparison direct and reduced connection}}{320}{figure.12.2}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces Reduced connection}}{320}{figure.12.2}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces Direct connections}}{320}{figure.12.2}
\contentsline {figure}{\numberline {12.3}{\ignorespaces Comparison direct and reduced connection}}{321}{figure.12.3}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces Reduced connection}}{321}{figure.12.3}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces Direct connections}}{321}{figure.12.3}
\contentsline {figure}{\numberline {12.4}{\ignorespaces The pressure assumptions in the chamber}}{321}{figure.12.4}
\contentsline {figure}{\numberline {12.5}{\ignorespaces The reduced time as a function of the modified reduced pressure}}{329}{figure.12.5}
\contentsline {figure}{\numberline {12.6}{\ignorespaces The reduced time as a function of the modified reduced pressure}}{331}{figure.12.6}
\addvspace {10\p@ }
\contentsline {figure}{\numberline {13.1}{\ignorespaces The control volume of the ``Cylinder''}}{336}{figure.13.1}
\contentsline {figure}{\numberline {13.2}{\ignorespaces The pressure ratio as a function of the dimensionless time}}{342}{figure.13.2}
\contentsline {figure}{\numberline {13.3}{\ignorespaces $\overline {P}$ as a function of $\overline {t}$ for choked condition }}{343}{figure.13.3}
\contentsline {figure}{\numberline {13.4}{\ignorespaces The pressure ratio as a function of the dimensionless time}}{344}{figure.13.4}
\contentsline {figure}{\numberline {13.5}{\ignorespaces Energy transfer in cycle piston cylinder Assembly}}{344}{figure.13.5}
\addvspace {10\p@ }
\contentsline {figure}{\numberline {14.1}{\ignorespaces A view of a normal shock as a limited case for oblique shock}}{345}{figure.14.1}
\contentsline {figure}{\numberline {14.2}{\ignorespaces The oblique shock or Prandtl--Meyer function regions}}{346}{figure.14.2}
\contentsline {figure}{\numberline {14.3}{\ignorespaces A typical oblique shock schematic}}{347}{figure.14.3}
\contentsline {figure}{\numberline {14.4}{\ignorespaces Flow around spherically blunted $30^\circ $ cone-cylinder}}{352}{figure.14.4}
\contentsline {figure}{\numberline {14.5}{\ignorespaces The different views of a large inclination angle}}{354}{figure.14.5}
\contentsline {figure}{\numberline {14.6}{\ignorespaces The three different Mach numbers}}{355}{figure.14.6}
\contentsline {figure}{\numberline {14.7}{\ignorespaces The various coefficients of three different Mach numbers}}{359}{figure.14.7}
\contentsline {figure}{\numberline {14.8}{\ignorespaces The ``imaginary'' Mach waves at zero inclination}}{360}{figure.14.8}
\contentsline {figure}{\numberline {14.9}{\ignorespaces The D, shock angle, and $M_y$ for $M_1=3$}}{361}{figure.14.9}
\contentsline {figure}{\numberline {14.10}{\ignorespaces $\tmspace +\thinmuskip {.1667em}$ The possible range of solutions}}{363}{figure.14.10}
\contentsline {figure}{\numberline {14.11}{\ignorespaces $\tmspace +\thinmuskip {.1667em}$Two dimensional wedge}}{364}{figure.14.11}
\contentsline {figure}{\numberline {14.12}{\ignorespaces $\tmspace +\thinmuskip {.1667em}$ Schematic of finite wedge with zero angle of attack.}}{366}{figure.14.12}
\contentsline {figure}{\numberline {14.13}{\ignorespaces $\tmspace +\thinmuskip {.1667em}$ A local and a far view of the oblique shock.}}{367}{figure.14.13}
\contentsline {figure}{\numberline {14.14}{\ignorespaces $\tmspace +\thinmuskip {.1667em}$ The schematic for a round--tip bullet in a supersonic flow.}}{369}{figure.14.14}
\contentsline {figure}{\numberline {14.15}{\ignorespaces $\tmspace +\thinmuskip {.1667em}$ The schematic for a symmetrical suction section with reflection.}}{370}{figure.14.15}
\contentsline {figure}{\numberline {14.16}{\ignorespaces The ``detached'' shock in a complicated configuration}}{370}{figure.14.16}
\contentsline {figure}{\numberline {14.17}{\ignorespaces $\tmspace +\thickmuskip {.2777em}$ Oblique shock around a cone}}{372}{figure.14.17}
\contentsline {figure}{\numberline {14.18}{\ignorespaces $\tmspace +\thickmuskip {.2777em}$ Maximum values of the properties in an oblique shock}}{373}{figure.14.18}
\contentsline {figure}{\numberline {14.20}{\ignorespaces Schematic for Example \textup {\hbox {\mathsurround \z@ \normalfont (\ignorespaces \ref {oblique:ex:inletEx}\unskip \@@italiccorr )}}}}{373}{figure.14.20}
\contentsline {figure}{\numberline {14.19}{\ignorespaces $\tmspace +\thickmuskip {.2777em}$ Two variations of inlet suction for supersonic flow}}{374}{figure.14.19}
\contentsline {figure}{\numberline {14.21}{\ignorespaces Schematic for Example \textup {\hbox {\mathsurround \z@ \normalfont (\ignorespaces \ref {oblique:ex:throat}\unskip \@@italiccorr )}}}}{375}{figure.14.21}
\contentsline {figure}{\numberline {14.22}{\ignorespaces $\tmspace +\thickmuskip {.2777em}$ Schematic of two angles turn with two weak shocks}}{376}{figure.14.22}
\contentsline {figure}{\numberline {14.23}{\ignorespaces Schematic for Example \textup {\hbox {\mathsurround \z@ \normalfont (\ignorespaces \ref {oblique:ex:reflectiveWall}\unskip \@@italiccorr )}}}}{379}{figure.14.23}
\contentsline {figure}{\numberline {14.24}{\ignorespaces Illustration for Example \textup {\hbox {\mathsurround \z@ \normalfont (\ignorespaces \ref {oblique:ex:tunnel2deflection}\unskip \@@italiccorr )}}}}{383}{figure.14.24}
\contentsline {figure}{\numberline {14.25}{\ignorespaces $\tmspace +\thinmuskip {.1667em}$ Revisiting of shock drag diagram for the oblique shock.}}{385}{figure.14.25}
\contentsline {figure}{\numberline {14.27}{\ignorespaces $\tmspace +\thickmuskip {.2777em}$ Typical examples of unstable and stable situations.}}{386}{figure.14.27}
\contentsline {figure}{\numberline {14.28}{\ignorespaces $\tmspace +\thinmuskip {.1667em}$ The schematic of stability analysis for oblique shock.}}{387}{figure.14.28}
\contentsline {figure}{\numberline {14.26}{\ignorespaces Oblique $\delta -\theta -M$ relationship figure}}{388}{figure.14.26}
\addvspace {10\p@ }
\contentsline {figure}{\numberline {15.1}{\ignorespaces A view of a normal shock as a limited case for oblique shock}}{389}{figure.15.1}
\contentsline {figure}{\numberline {15.2}{\ignorespaces The oblique shock or Prandtl--Meyer function regions}}{390}{figure.15.2}
\contentsline {figure}{\numberline {15.3}{\ignorespaces A typical oblique shock schematic}}{391}{figure.15.3}
\contentsline {figure}{\numberline {15.4}{\ignorespaces Flow around spherically blunted $30^\circ $ cone-cylinder}}{396}{figure.15.4}
\contentsline {figure}{\numberline {15.5}{\ignorespaces The different views of a large inclination angle}}{398}{figure.15.5}
\contentsline {figure}{\numberline {15.6}{\ignorespaces The three different Mach numbers}}{399}{figure.15.6}
\contentsline {figure}{\numberline {15.7}{\ignorespaces The various coefficients of three different Mach numbers}}{403}{figure.15.7}
\contentsline {figure}{\numberline {15.8}{\ignorespaces The ``imaginary'' Mach waves at zero inclination}}{404}{figure.15.8}
\contentsline {figure}{\numberline {15.9}{\ignorespaces The D, shock angle, and $M_y$ for $M_1=3$}}{405}{figure.15.9}
\contentsline {figure}{\numberline {15.10}{\ignorespaces $\tmspace +\thinmuskip {.1667em}$ The possible range of solutions}}{407}{figure.15.10}
\contentsline {figure}{\numberline {15.11}{\ignorespaces $\tmspace +\thinmuskip {.1667em}$Two dimensional wedge}}{408}{figure.15.11}
\contentsline {figure}{\numberline {15.12}{\ignorespaces $\tmspace +\thinmuskip {.1667em}$ Schematic of finite wedge with zero angle of attack.}}{410}{figure.15.12}
\contentsline {figure}{\numberline {15.13}{\ignorespaces $\tmspace +\thinmuskip {.1667em}$ A local and a far view of the oblique shock.}}{411}{figure.15.13}
\contentsline {figure}{\numberline {15.14}{\ignorespaces $\tmspace +\thinmuskip {.1667em}$ The schematic for a round--tip bullet in a supersonic flow.}}{413}{figure.15.14}
\contentsline {figure}{\numberline {15.15}{\ignorespaces $\tmspace +\thinmuskip {.1667em}$ The schematic for a symmetrical suction section with reflection.}}{414}{figure.15.15}
\contentsline {figure}{\numberline {15.16}{\ignorespaces The ``detached'' shock in a complicated configuration}}{414}{figure.15.16}
\contentsline {figure}{\numberline {15.17}{\ignorespaces $\tmspace +\thickmuskip {.2777em}$ Oblique shock around a cone}}{416}{figure.15.17}
\contentsline {figure}{\numberline {15.18}{\ignorespaces $\tmspace +\thickmuskip {.2777em}$ Maximum values of the properties in an oblique shock}}{417}{figure.15.18}
\contentsline {figure}{\numberline {15.20}{\ignorespaces Schematic for Example \textup {\hbox {\mathsurround \z@ \normalfont (\ignorespaces \ref {oblique:ex:inletEx}\unskip \@@italiccorr )}}}}{417}{figure.15.20}
\contentsline {figure}{\numberline {15.19}{\ignorespaces $\tmspace +\thickmuskip {.2777em}$ Two variations of inlet suction for supersonic flow}}{418}{figure.15.19}
\contentsline {figure}{\numberline {15.21}{\ignorespaces Schematic for Example \textup {\hbox {\mathsurround \z@ \normalfont (\ignorespaces \ref {oblique:ex:throat}\unskip \@@italiccorr )}}}}{419}{figure.15.21}
\contentsline {figure}{\numberline {15.22}{\ignorespaces $\tmspace +\thickmuskip {.2777em}$ Schematic of two angles turn with two weak shocks}}{420}{figure.15.22}
\contentsline {figure}{\numberline {15.23}{\ignorespaces Schematic for Example \textup {\hbox {\mathsurround \z@ \normalfont (\ignorespaces \ref {oblique:ex:reflectiveWall}\unskip \@@italiccorr )}}}}{424}{figure.15.23}
\contentsline {figure}{\numberline {15.24}{\ignorespaces Illustration for Example \textup {\hbox {\mathsurround \z@ \normalfont (\ignorespaces \ref {oblique:ex:tunnel2deflection}\unskip \@@italiccorr )}}}}{427}{figure.15.24}
\contentsline {figure}{\numberline {15.25}{\ignorespaces $\tmspace +\thinmuskip {.1667em}$ Revisiting of shock drag diagram for the oblique shock.}}{429}{figure.15.25}
\contentsline {figure}{\numberline {15.27}{\ignorespaces $\tmspace +\thickmuskip {.2777em}$ Typical examples of unstable and stable situations.}}{430}{figure.15.27}
\contentsline {figure}{\numberline {15.28}{\ignorespaces $\tmspace +\thinmuskip {.1667em}$ The schematic of stability analysis for oblique shock.}}{431}{figure.15.28}
\contentsline {figure}{\numberline {15.26}{\ignorespaces Oblique $\delta -\theta -M$ relationship figure}}{432}{figure.15.26}
\addvspace {10\p@ }
\contentsline {figure}{\numberline {16.1}{\ignorespaces Definition of the angle for the Prandtl--Meyer function}}{433}{figure.16.1}
\contentsline {figure}{\numberline {16.2}{\ignorespaces The angles of the Mach line triangle}}{433}{figure.16.2}
\contentsline {figure}{\numberline {16.3}{\ignorespaces The schematic of the turning flow}}{434}{figure.16.3}
\contentsline {figure}{\numberline {16.4}{\ignorespaces The mathematical coordinate description}}{435}{figure.16.4}
\contentsline {figure}{\numberline {16.5}{\ignorespaces Prandtl-Meyer function after the maximum angle }}{439}{figure.16.5}
\contentsline {figure}{\numberline {16.6}{\ignorespaces The angle as a function of the Mach number}}{440}{figure.16.6}
\contentsline {figure}{\numberline {16.7}{\ignorespaces Diamond shape for supersonic d'Alembert's Paradox}}{440}{figure.16.7}
\contentsline {figure}{\numberline {16.8}{\ignorespaces The definition of attack angle for the Prandtl--Meyer function}}{441}{figure.16.8}
\contentsline {figure}{\numberline {16.9}{\ignorespaces Schematic for Example \textup {\hbox {\mathsurround \z@ \normalfont (\ignorespaces \ref {oblique:ex:inletEx}\unskip \@@italiccorr )}}}}{441}{figure.16.9}
\contentsline {figure}{\numberline {16.10}{\ignorespaces $\tmspace +\thinmuskip {.1667em}$ Schematic for the reversed question of Example \ref {pm:ex:reversed}}}{443}{figure.16.10}
\contentsline {figure}{\numberline {16.11}{\ignorespaces $\tmspace +\thinmuskip {.1667em}$ Schematic of the nozzle and Prandtl--Meyer expansion.}}{445}{figure.16.11}
\addvspace {10\p@ }
\contentsline {figure}{\numberline {A.1}{\ignorespaces Schematic diagram that explains the structure of the program}}{450}{figure.A.1}
\addvspace {10\p@ }
