#include <qapplication.h>
#include <qfiledialog.h>
#include <qmenubar.h>
#include <qmessagebox.h>
#include <qpainter.h>
#include <qpixmap.h>
#include <qpopupmenu.h>
#include <qscrollview.h>
#include <qwidget.h>
#include <qcursor.h>
#include "gdForm.h"


int main(int argc, char *argv[]) {
	//*argv =  "-style=CDE";
	QApplication app(argc, argv);
	gdForm *dialog = new gdForm();
	app.setMainWidget(dialog);
//    QMessageBox::about(0, "About Gas Dymanics Calculator",
//            "<h2>Gas Dymanics Calculator 0.4.3.2</h2>"
//               "<p>Copyright &copy; 2003 Dr. Genick Bar-Meir"
//               "<p>Gas Dymanics Calculator is a small application that "
//               "calculates several quantities in different"
//               "flow models.");
	dialog->show();
	return app.exec();
}
