/****************************************************************************
** Form interface generated from reading ui file 'gdForm.ui'
**
** Created by: The User Interface Compiler ($Id: qt/main.cpp   3.3.6   edited Aug 31 2005 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef GDFORM_H
#define GDFORM_H

#include <qvariant.h>
#include <qpixmap.h>
#include <qmainwindow.h>
#include "iniFile.h"
#include "definition.h"
#include "doubleEdit.h"

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QAction;
class QActionGroup;
class QToolBar;
class QPopupMenu;
class QCheckBox;
class QButtonGroup;
class QRadioButton;
class QPushButton;
class QLabel;

class gdForm : public QMainWindow
{
    Q_OBJECT

public:
    gdForm( QWidget* parent = 0, const char* name = 0, WFlags fl = WType_TopLevel );
    ~gdForm();

    QCheckBox* isRangeBox;
    QCheckBox* isTexBox;
    QButtonGroup* outputGroup;
    QRadioButton* radioStandard;
    QRadioButton* radioMinimum;
    QRadioButton* radioMaximum;
    QButtonGroup* modelGroup;
    QRadioButton* radioOblique;
    QRadioButton* radioShock;
    QRadioButton* radioShockDynamics;
    QRadioButton* radioPM;
    QRadioButton* radioIsoNozzle;
    QRadioButton* radioIsentropic;
    QRadioButton* radioIsothermal;
    QRadioButton* radioRayleigh;
    QRadioButton* radioFanno;
    QButtonGroup* subModelGroup;
    QRadioButton* suddenOpenRadio;
    QRadioButton* suddenCloseRadio;
    QRadioButton* partiallyCloseRadio;
    QRadioButton* partiallyOpenRadio;
    QPushButton* pushQuit;
    QButtonGroup* whatInputGroup;
    QPushButton* calculateButton;
    QLabel* infoNotice;
    QButtonGroup* inputDataGroup;
    QButtonGroup* pixGroup;
    QLabel* pixExLabel0;
    QLabel* pixExLabel1;
    QLabel* pixExLabel2;
    QLabel* pixExLabel3;
    QMenuBar *menubar;
    QPopupMenu *fileMenu;
    QPopupMenu *helpMenu;
    QAction* fileNewAction;
    QAction* fileOpenAction;
    QAction* fileSaveAction;
    QAction* fileSaveAsAction;
    QAction* fileExitAction;
    QAction* helpContentsAction;
    QAction* helpIndexAction;
    QAction* helpAboutAction;

public slots:
    virtual void init();
    virtual void helpAbout();
    virtual void helpIndex();
    virtual void helpContents();
    virtual void clearParameters();
    virtual void setParameters();
    virtual void clearInputBoxs();
    virtual void setInput();
    virtual void calculate();
    virtual int LabelChanged();
    virtual void fileSave();
    virtual void fileSaveAs();
    virtual void fileOpen();
    virtual void save( QString FileName );
    virtual int canSave();
    virtual void valueChange( double k );
    virtual void Load();
    virtual void newValues();
    virtual void selectBranch( branchType a );
    virtual void valueChange();
    virtual void setNotice( inputType whatInput );
    virtual void setNoticeIfNeeded();
    virtual void branchSelected();
    virtual void fix_mD();
    virtual void onSubModelGroup();
    virtual void clearSubGroup();
    virtual void setSubParameters();
    virtual void notImplementedYet(QString);
    virtual void clearInfoBox();
    virtual void setInfoBoxs();
    virtual void setInfoNotice(QString);
    virtual void newData();
    virtual void fixP(int, QString );
    virtual void fixT(int, QString );
    virtual void fixRho(int, QString );
    virtual void fix_s(int, QString);
    virtual void fix_x(int );
    virtual void fix_h(int, QString);
    virtual void fix_u(int, QString);
    virtual void fix_U(int, QString);
    virtual void fix_mD(int, QString );
    virtual void UnitsChanged0(int );
    virtual void UnitsChanged1(int );
    virtual void UnitsChanged2(int );
    virtual void saveSelectedBranch( CIniFile & );
    virtual void setSubInputShockDynamics();
    virtual void setSubInputFanno();
    virtual void setSubInput();
    virtual void setInputBoxValue(int);
    virtual void valueCorrectionDueUnitsChange(int);
    virtual void temperatureConversion(int, double , int, double & );

signals:
    void newData(QString);

protected:

protected slots:
    virtual void languageChange();

private:
    QPixmap image0;
    QPixmap image1;
    QPixmap image2;
    QPixmap image3;
    QPixmap image4;
    QPixmap image5;
    QPixmap image6;

};

#endif // GDFORM_H
