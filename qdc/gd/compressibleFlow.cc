// last change Mon Dec 19 17:31:35 CST 2011 minor correction 
// of the new compler

/********************************************************
	FILE:compressibleFlow.cc

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#	include <stdio.h>
#	include <stdlib.h>
#  include <iostream>
#  include <iomanip>
#  include "stagnation.h"
#  include "externalFun.h"
#  include "compressibleFlow.h" 
#  include "gasDynamics.h" 
using namespace std;

// compressible Flow function definitions
//*******************************************

void 	compressibleFlow::makeTexTableTail(int * ioData) {
    if (isR == yes){
		cout << "\\end{longtable}" << endl ;
	}
	else {
		cout << "\\end{tabular}" << endl ;
		cout << "\\end{center}" << endl ;
		cout << "\\medskip" << endl ;
	}
};

int 	compressibleFlow::calM1forFLDp2p1(double FLD, double P2P1){
	double M1max, M1TMP ; // the maximum value of Mach entrance to tube. the TMP holder of M1
	// the first guess the addition of fld find a better guess for this process
	fld = FLD;   p2p1 = P2P1; // change to the class variable and change to class variable
	/*  the main routine and initialization process  and guessing */
	// first guess what is the range that M1 can be ... i.e. 0.0001 to M1maxam	calculate M1max */
	M1TMP = 0.1 ; // just to indicate that it is subsonic flow
	M1max = calMForFLD(FLD,M1TMP );
	// check the whether the flow is chocked.  check whether p2/p1 is below the critical pressure ratio.
	M1TMP = root ( calP2P1_Estmt, this, .00001,  M1maxSub, FLD) ;
	M = M1max; // to make tmp assigned to M  
	calPressureRatio();
	if ((1./PRstar) >= p2p1 ||  M1TMP < 0.0 ) { //the flow is chocked
		M1 = M1max;
		M2 = M1maxSub;
		fld1 = FLD;
		fld2 = 0.0;
		isChoked = yes;
		p2p1 = 1./PRstar;
		return yes;
	}
    isChoked = no;
	// now the pair fld and M1max are known.  Try to calculate/estimate the M1 for this pair 
	// including p2p1 pass trough the class variable 
	// insert the information about M2 and fld1 and fld2
	fld1 = calFLD(M1TMP);
	if ( fld1 < FLD) { // numerical error 
		fld1 = FLD;
		fld2 =0.0;
		M1 = M1max;
		M2 = M1maxSub;
		return yes;
	}
	fld2 = fld1 - FLD;
	// insert the information about M2 and fld1 and fld2
	M2 = calMForFLD(fld2,M1max ); 
	if ( M2 <= 0.0001) {// incompressible flow
	cout << " this flow is incompressible" << endl;
	return no; // no solution 
	}
	// note that M1 is lost and has to be put again from tmp place holder  
	M1 = M1TMP;
	// and again return given value of fld
	fld = FLD;
	// just checking to see if working to clean later //cout << "p2/p2 = " << p2p1<< "\n" ;
	return yes;
} ;

int 	compressibleFlow::calM1forM2(double FLD, double m2, double * m1){
	double FLD2tmp ; // fld2 for the pair info (FLD, m2) 
	FLD2tmp = calFLD(m2) ;
	if (m2 <= 1.0 ) { // sub sonic flow 
		fld1 = FLD2tmp + FLD;
		*m1= calMForFLD (fld1,m2);
		if ( *m1 > 0.0 ) { 
			return yes;
		}
		else {
			return no;
		}
	}
	else if (m2 > 1.0 ) { // super sonic flow 
		if (FLD < superSonicFLD) {// within possible range	
			fld1 = FLD2tmp + FLD;
			*m1= calMForFLD (fld1,m2);
			return yes;
		}
		else if (FLD == superSonicFLD) {// the maximum possible range
			 *m1= infinity; // 1.79769313486231570E+308 ;// m1 = infinity
			return yes;
		}
		else { // the above the maximum range
			return no;
		}
	}
	return yes;
} ;

int 	compressibleFlow::calM2forM1(double FLD, double m1, double * m2){
	if (isChoked == yes) {
		*m2 = M1maxSub ; 
		return yes;
	}
    double FLD1tmp ; // fld2 for the pair info (FLD, m2)
    FLD1tmp = calFLD(m1) ;
    if (m1 <= M1maxSub ) { // subsonic flow
        fld2 = FLD1tmp - FLD;
        *m2= calMForFLD (fld2,m1);
		if ( *m2 > 0.0 ) {
        	return yes;
		}
		else {
        	return no;
		}
    }
    else if (m1 > M1maxSub ) { // super sonic flow
        if (FLD < superSonicFLD) {// within max possible range sup flow
			if ( FLD1tmp > FLD ) { //shockless and chokless
				fld2 = FLD1tmp - FLD;
				*m2= calMForFLD (fld2,m1);
				return yes;
			}
			else { // choked and must be a shock somewhere in tube
				*m2 = 1;
				return yes;
			}
        }
        else if (FLD == superSonicFLD) {// the maximum possible range
             *m2= infinity; // 1.79769313486231570E+308 ;// m1 = infinity
            return yes;
        }
        else { // the above the maximum range
            return no;
        }
    }
	return yes;
};

void 	compressibleFlow::showVariable(double * variableToBePrinted,
			int numberVariables){
	int space;
	space = 10;
	int stdPrecision;
	stdPrecision = 4;
    cout.setf (  std::ios_base::left, std::ios_base::basefield );
    for (int i=0; i < numberVariables ; i++){
    	if ( variableToBePrinted[i] < 0.0 ) { // the varible negative
			space = 12 ;
		}
		else {
			space = 10 ;
		} 
    	if ( fabs( variableToBePrinted[i]) < 10.0 )
       		{cout.width(space); cout.precision(4);}
        else {cout.width(space);    cout.precision(3);};
            cout << showpoint << variableToBePrinted[i] ; // << "\t" ;
        }
        cout << endl ;
};

void 	compressibleFlow::showVariableTeXLine (double * variableToBePrinted,
			int numberVariables){
	int space;
	space = 9;
    int stdPrecision;
	//move from the global variable to local 
	//future more smart manipulation for formating 
    stdPrecision = precision;
    cout.setf ( std::ios_base::right,
                     std::ios_base::basefield );
	cout << setiosflags(ios::uppercase);
	//cout << setfill('0');

    for (int i=0; i < numberVariables ; i++){
    	if ( fabs( variableToBePrinted[i]) >= 100.0 ) { // three digits	
            //cout << setiosflags(ios::fixed | ios::showpoint);
            cout << setiosflags(ios::fixed );
			cout.width(space);    
			cout.precision(stdPrecision- 2);
		}
        else if ( fabs( variableToBePrinted[i]) >= 10.0 ) {
			//cout << setiosflags(ios::fixed | ios::showpoint);
			cout << setiosflags(ios::fixed );
			cout.width(space);
			cout.precision(stdPrecision);
		}
		else if ( fabs( variableToBePrinted[i]) >= 1.0 ){
			//cout << setiosflags(ios::fixed | ios::showpoint);
			cout << setiosflags(ios::fixed );
			cout.width(space);
            cout.precision(stdPrecision );
		}
		else if ( fabs( variableToBePrinted[i]) >= 0.01 ){
			//cout << setiosflags(ios::fixed | ios::showpoint);
			cout << setiosflags(ios::fixed );
			cout.width(space +1 );
            cout.precision(stdPrecision+1);
		}
		else if ( fabs( variableToBePrinted[i]) >= 1.25E-6 ){
			cout.setf(ios::scientific);
			cout.width(space +1 );
            cout.precision(stdPrecision -1 );
		}
		else if ( fabs( variableToBePrinted[i]) < 1.25E-6 ){
            cout.setf(ios::scientific);
            cout.width(space  );
            cout.precision(stdPrecision -2 );
			//make variable zero
			variableToBePrinted[i] = 0.0;
        };
		//cout <<  showpoint << variableToBePrinted[i] ;
		cout <<  variableToBePrinted[i] ;
		if (i +1 < numberVariables) 
			{cout   << "&" ; }
	}
        cout  << " \\\\ \\hline" ;
        cout << endl ;
};

void compressibleFlow::showVariableHead (char ** variableNames,
					 int numberVariables ) {
	int space = 10;
    cout.setf ( std::ios_base::right,
                     std::ios_base::basefield );
    for (int i=0; i < numberVariables ; i++){
		cout.width(space);
		cout << variableNames[i] ;// << "\t" ;
     }
        cout << endl ;
};

void compressibleFlow :: showVariableTeXHead (char ** variableNames,
					 int numberVariables ) {
    cout << endl << endl;
    cout << "*******************************************" << endl ;
    cout << "The following stuff is the same as above/below" << endl ;
    cout << "if you use showResults with showHeads but the " << endl ;
    cout << "information is setup for the latex text processing." << endl ;
    cout << "You can just can cut and paste it in your latex file." << endl
;
	if (isR == yes){
    cout << "You must use longtable style file and dcolumn " << endl ;
    cout << "style files." << endl  << endl << endl;
    cout << "*******************************************" << endl ;
    cout << "\\setlongtables" << endl ;
	cout << "\\begin{htmlonly} "   << endl ;
    cout << "\\begin{longtable}"  ;
	cout << "{" ; // before the line
	for (int i=0; i <  numberVariables; i++) {
    	cout << "|l";
	};
	cout << "|}" << endl ; //end of the line
	cout << "\\end{htmlonly} " << endl ;
	cout << "%begin{latexonly} " << endl ;
    cout << "\\begin{longtable}" << endl ;
	//calculated the width of the table
	}
	else {
		cout << "\\begin{center}" << endl ;
		cout << "%begin{latexonly} " << endl ;
		cout << "\\begin{tabular}" << endl ;
	}
	cout << "{" ; // before the line
	int tableWidthSuppliment;
    tableWidthSuppliment = 0;
	if ( numberVariables - 7 ) {
		tableWidthSuppliment = 7 - numberVariables; 
	}
	for (int i=0; i <  numberVariables; i++) {
    	cout << "|D..{" << precision - 3 + tableWidthSuppliment
				  << "." << precision + tableWidthSuppliment << "}";
	};
	cout << "|}" << endl ; //end of the line
	cout << "%end{latexonly} " << endl ;
	if (isR == yes){
    	cout << "\\caption{ ??  \\label{?:tab:?}}\\\\"  << endl;
	}
    cout << "\\hline" << endl ;

    for (int i=0; i < numberVariables ; i++){
		cout << "\\multicolumn{1}{|c"  ; // the same line
		if (i+1 == numberVariables ) { cout << "|" ;}
		cout << "} {$" ; 
		if (i == 0 ) { cout << "\\rule[-0.1in]{0.pt}{0.3 in}" ;}
		cout <<  "\\mathbf{"  ; // the same line
		cout << variableNames[i];
		cout  << "} $} ";// continue line
		if (i+1 < numberVariables ) { cout << " & ";}
		cout  << endl;
	}
    cout << endl << "\\\\\\hline" << endl;
    cout << endl ;
    if (isR == yes){
		cout << "\\endfirsthead" << endl ;
		cout << "\\caption{ ?? (continue)} \\\\\\hline" << endl ;

		for (int i=0; i < numberVariables ; i++){
			cout << "\\multicolumn{1}{|c"  ; // the same line
			if (i+1 == numberVariables ) { cout << "|" ;}
			cout << "} {$" ;
			if (i == 0 ) { cout << "\\rule[-0.1in]{0.pt}{0.3 in}" ;}
			cout <<  "\\mathbf{"  ; // the same line
			cout << variableNames[i];
			cout  << "} $} ";// continue line
			if (i+1 < numberVariables ) { cout << " & ";}
			cout  << endl;
		}
		cout << endl << "\\\\\\hline" << endl;
		cout << "\\endhead" << endl;
	}
};

void compressibleFlow :: showVariableHTMLHead (char ** variableNames,
					 int numberVariables ) {
	cout << endl << endl;
	//cout << "this html code for table" << endl ;
	cout << "<tr class=\"text\"><td align=\"center\">" << endl ;
	cout << "<table border=1 width=\"100%\" >" << endl ;
	cout << "  <thead>" << endl;
	cout << "    <tr>" << endl ;
	cout << "      <th align=left bgcolor=\"#fffeaa\" colspan="; 
	cout << 2 << " >";  
	cout <<  flowModelName ;
	cout << " </th>" << endl;
	cout << "      <th align=left  bgcolor=\"#00ff5a\" colspan="; 
	if (numberVariables > 5) {
		cout << numberVariables-4 << " >";  
	}
	else {
		cout << numberVariables-3 << " >";  
	}
	cout << "Input: ";
	cout << inputVariableName ;
  	cout << " </th>" << endl;
  	cout << "      <th align=left  bgcolor=\"#9ae0ee\" colspan="; 
	if (numberVariables < 8) {
		cout << 1 << " >";  
	}
	else {
		cout << 2 << " >";  
	}
	cout << "k = ";
	cout << k ;
  	cout << " </th>" << endl;
	cout << "    </tr>" << endl ;
	cout << "    <tr>" << endl ;
	for (int i=0; i < numberVariables ; i++){
		cout << "      <th align=center >" ;
		cout <<  variableNames[i] << " </th>" << endl;
	}
	cout << "    </tr> " << endl;
	cout << "  </thead>" << endl;
	cout << "  <tbody>" << endl;
};

void 	compressibleFlow::showVariableHTMLLine (double * variableToBePrinted,
			int numberVariables){
	cout << "    <tr> " << endl;
    for (int i=0; i < numberVariables ; i++){
    	cout << "      <td align=right > " ;
		cout <<  variableToBePrinted[i] ;
    	cout << "      </td>" << endl ;
	}
	cout << "    </tr>" << endl;
};

void 	compressibleFlow::makeHTMLTableTail(int *ioData) {
	cout << "  </tbody>" << endl;
	cout << "</table>" << endl ;
	cout << "</td>" << endl ;
	cout << "</tr>" << endl ;
	if (ioData[variableNameN] == fldP2P1V ){
    cout << "</html>" << endl ;
	}
};


void    compressibleFlow::makeHTMLTableHead(int * ioData){
    int variableNumber;
                                                                               
    if (outputInfo == infoStandard || outputInfo == infoMinimal) {
        variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = (char *  ) "M  ";
        variableNames[1] = (char *  ) "T/T0";
        variableNames[2] = (char *  ) "&rho;/&rho;0";
        variableNames[3] = (char *  ) "A/A*";
        variableNames[4] = (char *  ) "P/P0";
        variableNames[5] = (char *  ) "PAR";
        variableNames[6] = (char *  ) "F/F*";
                                                                               
        showVariableHTMLHead(variableNames,variableNumber);
    }
};

void    compressibleFlow::makeHTMLTableLine(int * ioData) {
    int numberVariables;

    if (outputInfo == infoStandard || outputInfo == infoMinimal) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M ;
        variableToBePrinted[1] = Tbar;
        variableToBePrinted[2] = Rbar;
        variableToBePrinted[3] = Area;
        variableToBePrinted[4] = Pbar ;
        variableToBePrinted[5] = PAR;
        variableToBePrinted[6] = FRstar;

        showVariableHTMLLine (variableToBePrinted, numberVariables);
    }
};

int compressibleFlow:: calTableLine (int* ioData, double * variableValue){
	if ((outputType) ioData[whatInfoN] == infoStandard || 
		(outputType) outputInfo == infoMinimal ||
		(outputType) outputInfo == infoIteration ){
		if (ioData[isBothBranchesN] == yes) {
			variableValue[machV] = 0.5;
			int calResults[2] ;
			calResults[0] = calAll( variableValue);
			//to save the calculations and do second branch
			int i ;
			i =0 ;
			saveResults (i);
			// now the supersonic
			variableValue[machV] = 2.0;
			calResults[1] = calAll( variableValue);
			i =1 ;
			saveResults (i);
			if (calResults[0] == yes || calResults[1] == yes ){ 
				return yes;
			}
			else {
				return no;
			}
		}
		else {// no only single branch
            return calAll( variableValue);
		}
    }
	else {
    	return no;
	}
};

//int compressibleFlow::calTableLine ( int * ioData, double * variableValue){
//	inputType variableName = (inputType) ioData[variableNameN];
//	double m1tmp; //tmp m1 place holder
//	double m2tmp; //tmp m2 place holder
//	double m2, m1 ; //  local m2, m1
//	double FLD ; // local fld
//	//perhaps to put isRange variable check
//	//isR = isRange;
//	if (outputInfo == infoStagnation) {
//		//cal the stagnation regular  variables
//		return calAll( variableValue);
//	}
//	else if (outputInfo == infoStandard ){
//		calMach(ioData, variableValue);
//		if (variableName == MxV){
//			return calAll( variableValue);
//		}
//		else if (variableName != fldV){
//			calFLD();
//		}
//		if (drivedClassName == fannoFlow){
//        	calDensityRatio();
//			calTemperatureRatio();
//		}
//		else{
//        	calTotalTemperatureRatio();
//		}
//        calTotalPressureRatio();
//        calPressureRatio();
//        calVelocityRatio();
//        return yes;
//	}
//	else if (outputInfo == infoTube ){
//
//		calM1(variableName, variableValue);
//		if (calM2forM1(variableValue[fldN], M1, &m2tmp) == yes) {
//			M2 = m2tmp;
//			if(variableName == p2p1FLDV) {
//            	fld = variableValue[fldN];
//			}
//			if (isChoked == yes){
//				p2p1 = calPressureRatio (M2)/calPressureRatio (M1);
//				fld1 = variableValue[fldN] ;
//				fld2 = 0.0;
//			}
//			else {
//				p2p1 = variableValue[p2p1N];  
//            	fld1 = calFLD(M1);
//            	fld2 = calFLD(M2);
//			}
//			return yes;	
//		}
//		else {
//			return no;
//		}
//	}
//	else if (outputInfo == infoShock ){
//		return calAll( variableValue);
//    }
//	else if (outputInfo == infoTubeShock ){
//		//double leftFLD, rightFLD, extraFLD, shockLocation;
//		double  extraFLD, shockLocation;
//		calM1(variableName, variableValue);
//		//  the shock at the entrance: check if supersonic flow possible
//		if (M1 > 1.0 ) { //supper sonic flow
//			maxPossibleFLDsup = calFLD (M1);			
//			shock s(k);
//			m1tmp = s.calMy(M1);// shock at the entrance (M1y)
//            maxPossibleFLDsub = calFLD (m1tmp);
//			//calM2forM1(fld,m1tmp, &M2upper) ;
//			if (fld<=maxPossibleFLDsub){//is subSonic possible entire tube 
//				extraFLD = fld - maxPossibleFLDsup;
//				shockLocation = root (calShockLocationM_Estmt, this, 0.,
//							 maxPossibleFLDsup - EPS, extraFLD ) ;
//				M2 = 1.0;
//				fldUp =  shockLocation;
//				fldDown = fld - fldUp;
//				My =  calMForFLD (fldDown , 0.1 ); //subsonic branch
//				Mx =  s.calMy(My) ; //symmetrical function
//				return yes;
//			}
//			else {// not possible not enough fld
//				 return no;
//			}
//        }
//        else { //the flow is subsonic
//            return no;
//        }
//	}
//    else if (outputInfo == infoTubeProfile){
//		// for profiles in nozzles
// 		if ( drivedClassName ==  stagnationFlow
//			||drivedClassName == isothermalNozzleFlow){
//			return calAll( variableValue);
//		}
//		//double leftFLD, rightFLD, extraFLD; 
//		double  extraFLD; 
//		double shockLocation;
//		shock s(k);
//		//cal left side
//		fld = FLD = variableValue[fldN];
//		M1 = variableValue[m1N]; 
//		maxPossibleFLDsup = calFLD (M1);
//		extraFLD = fld - maxPossibleFLDsup;
//		shockLocation = root 
//			(calShockLocationM_Estmt,this, 0.,maxPossibleFLDsup-EPS,extraFLD);
//		M2 = 1.0;
//		fldUp =  shockLocation;
//		fldDown = fld - fldUp;
//        My =  calMForFLD (fldDown , 0.1 ); //subsonic branch
//        Mx =  s.calMy(My) ; //symmetrical function
//		return yes;
//	}
//    else if (variableName == M1fldV){ // 
//		//cal the Mach and pressure profiles for M1 (supSonic) 
//		double deltaFLD ;// tube variable (xi)
//		int i, shockLocation;
//        fld = FLD = variableValue[fldN];
//        M1 =  m1 = variableValue[m1N];
//		p2p1 =  variableValue[p2p1N];
//		shockLocation =  (int) variableValue[shockLocationN];
//		//calculate the max fld (m1)
//        maxPossibleFLDsup = calFLD (m1);
//        shock s(k);
//        m1tmp = s.calMy(m1);//(M1y) to use lower limit entrance shock
//
//		//to be remove
//        //maxPossibleFLDsub = calFLD (m1tmp);
//        //calM2forM1(FLD,m1tmp, &M2upper);
//
//		deltaFLD = FLD / fabs((double) (numberNodes -1 )) ; 
//		double p1pStar; // , pxpStar, pxpy, pypStar, p2pStar; 
//		
//		// cal the entrance pressure ratio
//		p1pStar = calPressureRatio(m1) ;
//		//use the calculations of supSonic branch to shock
//		//assign new fldx;  cal shock relationship fldy, My etc   
//		//shockLocation = 1;	
//		Mfld[fldP][0]= 0. ;
//		Mfld[MxP][0] = M1;
//		Mfld[MyP][0] = 0.0;
//		Mfld[PyPxP][0] = 0.0;
//		Mfld[PxPsP][0] = p1pStar;
//		Mfld[PyPsP][0] = 0.0;
//		Mfld[P1PiP][0] = 1.0 ;
//		Mfld[P1PiP][1 ] = 1.0;
//		double Lfld [numberNodes]; //local variable for leftover fld
//		//The section before the shockLocation
//        for (i = 1; i <shockLocation    ; i++){
//            //upstream "before" shock location
//            Mfld[fldP][i] = Mfld[fldP][i-1] + deltaFLD;
//            // M = f(fld, branch)
//            Lfld [i] =  maxPossibleFLDsup -  Mfld[fldP][i]; 
//            Mfld[MxP][i] = calMForFLD(  Lfld [i], 10.0 );
//            Mfld[MyP][i] = 0.0 ;
//            Mfld[PyPxP][i] = 0.0 ;
//            Mfld[PxPsP][i] = calPressureRatio( Mfld[MxP][i]);//p(i+1)/p*
//            Mfld[PyPsP][i] = 0.0;//p(i+1)/p*
//            Mfld[P1PiP][i] =  p1pStar/Mfld[PxPsP][i];//[p1/p(i)]*[p(i-1)/p(i)]
//			//tmp reverse
//			Mfld[P1PiP][i] = 1./ Mfld[P1PiP][i] ;
//        }
//
//		//@ shock location calculation
//		//fld
//		Mfld[fldP][shockLocation] = ((double) shockLocation) * deltaFLD;
//		//Mx
//		Mfld[MxP][shockLocation] =
//			 calMForFLD(maxPossibleFLDsup - Mfld[fldP][shockLocation],
//							 10.0 ); //sup branch
//		//cal My
//		Mfld[MyP][shockLocation] = s.calMy( Mfld[MxP][shockLocation] ); 
//		//cal pypx
//		Mfld[PyPxP][shockLocation] = s.calPressureRatio(
//				 Mfld[MxP][shockLocation], Mfld[MyP][shockLocation] ); 
//		//cal pxpStar
//		Mfld[PxPsP][shockLocation] = calPressureRatio(Mfld[MxP][shockLocation]);
//		//cal pypStar 
//		Mfld[PyPsP][shockLocation] = calPressureRatio(Mfld[MyP][shockLocation]);
//		//cal p1/p(i)
//		Mfld[P1PiP][shockLocation] =  p1pStar/Mfld[PxPsP][shockLocation]
//				/ Mfld[PyPxP][shockLocation];
// 
//
//		//After the shock
//		Lfld [shockLocation] = calFLD(Mfld[MyP][shockLocation] );
//		for (i= (shockLocation+1); i   < numberNodes   ; i++){
//			//downstream "after" shock location
//	        Mfld[fldP][i ] = Mfld[fldP][i-1] + deltaFLD;
//			// M = f(fld, branch) 
//			Lfld [i] =  Lfld [shockLocation] -
//					 (double) (i - shockLocation) * deltaFLD;	
//			Mfld[MxP][i ] = calMForFLD(  Lfld [i], 0.1 );
//			Mfld[MyP][i ] = 0.0 ;
//			Mfld[PyPxP][i] = 0.0 ;
//			Mfld[PxPsP][i] = calPressureRatio( Mfld[MxP][i]);//p(i+1)/p*
//			Mfld[PyPsP][i] = 0.0;//p(i+1)/p*
//			Mfld[P1PiP][i] =  Mfld[P1PiP][shockLocation]*
//					Mfld[PyPsP][shockLocation] /Mfld[PxPsP][i] ; 
//          //tmp reverse pressure ratio 
//			Mfld[P1PiP][i] = 1.0 /   Mfld[P1PiP][i]; 
//		}
//          //tmp reverse pressure ratio 
//		 Mfld[P1PiP][shockLocation] = 1./ Mfld[P1PiP][shockLocation];
//		return yes;
//	}
//    if (variableName == machV){
//        M = variableValue[mN] ;
//        calPressureRatio();
//        calDensityRatio();
//		calTemperatureRatio();
//		if (drivedClassName == stagnationFlow){
//			calAreaRatio();
//			calPARRatio();
//		}
//		else {
//			calFLD();
//			calTotalPressureRatio();
//			calVelocityRatio();
//		}
//        return yes;
//    }
//	else if (variableName == fldV ) {
//		fld = variableValue[fldN] ;			
//        M = calMForFLD(variableValue[fldN],variableValue[mN]) ;
//        calDensityRatio();
//        calTotalPressureRatio();
//        calPressureRatio();
//        calTemperatureRatio();
//        calVelocityRatio();
//        return yes;
//    }
//	// to fix this problem the same ******************fldV
//	else if (variableName == fldV) {
//		calM1forFLDp2p1( variableValue [fldN], variableValue [p2p1N] );
//	}
//	else if (variableName == machV){
//		if ( calM2forM1(variableValue[fldN], variableValue[m1N],
//				 &m2tmp) == yes)//calculate M1 f(fld, M2)
//		{
//			M1= variableValue[m1N];
//			M2 = m2tmp;
//			fld = variableValue[fldN];
//			fld1 = calFLD(M1);
//			fld2 = calFLD(M2);
//    		p2p1 = calPressureRatio (M2)/calPressureRatio (M1);
//
//			return yes ;
//		}		
//		else {
//			return no;
//		}
//	}
//    else if (variableName == M1ShockV){
//        FLD = variableValue[fldN] ;
//		m1 = variableValue[m1N];
//		// just after the shock at the entrance
//		// check if supper sonic flow
//		if (m1 > 1.0 ) { //supper sonic flow
//			maxPossibleFLDsup = calFLD (m1);			
//			shock s(k);
//			m1tmp = s.calMy(m1);// shock at the entrance (M1y)
//            //maxPossibleFLDsub = calFLD (m1tmp);
//
//			M1 = m1;// place into the class variable
//			calM2forM1(FLD,m1tmp, &M2upper) ;
//		
//			if (FLD<=maxPossibleFLDsup){//is supSonic possible for whole tube
//									//shock can occur at exit
//				calM2forM1(FLD,m1, &m2);  // what to do if not?   
//    			M2lower = s.calMy(m2);
//				return yes;
//			}
//			else {// no shock at the entrance possible
//				// to check if the shock is before entrance
//			//M1 = m1;// place into the class variable
//				M2lower = -1.0;
//				if (M2upper> 0.0) {return yes;} 
//				else { return no;}
//			}
//        }
//        else {
//            return no;
//        }
//    }
//	else if (variableName == FLDShockV){
//        FLD = variableValue[fldN];
//		m1 =  variableValue[m1N];
//		//tobecontinue
//		//find mim possible m1  sub and sup 
//		//min
//    	maxPossibleFLDsup = calFLD (m1);
//    	shock s(k);
//    	m1tmp = s.calMy(m1);// shock at the entrance (M1y)
//    	//maxPossibleFLDsub = calFLD (m1tmp);
//    	                                                                      
//    	M1 = m1;// place into the class variable
//    	calM2forM1(FLD,m1tmp, &M2upper) ;
//    	                                                                      
//    	if (FLD<=maxPossibleFLDsup){//is supSonic possible for whole tube
//    	                        //shock can occur at exit
//    	    calM2forM1(FLD,m1, &m2);  // what to do if not?
//    	    M2lower = s.calMy(m2);
//    	    return yes;
//    	}
//    	else {// no shock at the entrance possible
//    	    // to check if the shock is before entrance
//    	//M1 = m1;// place into the class variable
//    	    M2lower = -1.0;
//    	    if (M2upper> 0.0) {return yes;}
//    	    else { return no;}
//    	}
//    }
//    else if (variableName == MFLDshockLocationV ){ // 
//		//double leftFLD, rightFLD, extraFLD; 
//		double  extraFLD; 
//		double shockLocation;
//		shock s(k);
//		//cal left side
//		fld = FLD = variableValue[fldN];
//		M1 = variableValue[m1N]; 
//		maxPossibleFLDsup = calFLD (M1);
//		extraFLD = fld - maxPossibleFLDsup;
//		shockLocation = root 
//			(calShockLocationM_Estmt,this,0.,maxPossibleFLDsup-EPS,extraFLD );
//		M2 = 1.0;
//		fldUp =  shockLocation;
//		fldDown = fld - fldUp;
//        My =  calMForFLD (fldDown , 0.1 ); //subsonic branch
//        Mx =  s.calMy(My) ; //symmetrical function
//		return yes;
//	}
//    else if (variableName == M1fldV){ // 
//		//cal the Mach and pressure profiles for M1 (supSonic) 
//		double deltaFLD ;// tube variable (xi)
//		int i, shockLocation;
//        fld = FLD = variableValue[fldN];
//        M1 =  m1 = variableValue[m1N];
//		p2p1 =  variableValue[p2p1N];
//		shockLocation =  (int) variableValue[shockLocationN];
//		//calculate the max fld (m1)
//        maxPossibleFLDsup = calFLD (m1);
//        shock s(k);
//        m1tmp = s.calMy(m1);//(M1y) to use lower limit entrance shock
//
//		//to be remove
//
//		deltaFLD = FLD / fabs((double) (numberNodes -1 )) ; 
//		double p1pStar; //, pxpStar, pxpy, pypStar, p2pStar; 
//		
//		// cal the entrance pressure ratio
//		p1pStar = calPressureRatio(m1) ;
//		//use the calculations of supSonic branch to shock
//		//assign new fldx;  cal shock relationship fldy, My etc   
//		//shockLocation = 1;	
//		Mfld[fldP][0]= 0. ;
//		Mfld[MxP][0] = M1;
//		Mfld[MyP][0] = 0.0;
//		Mfld[PyPxP][0] = 0.0;
//		Mfld[PxPsP][0] = p1pStar;
//		Mfld[PyPsP][0] = 0.0;
//		Mfld[P1PiP][0] = 1.0 ;
//		Mfld[P1PiP][1 ] = 1.0;
//		double Lfld [numberNodes]; //local variable for leftover fld
//		//The section before the shockLocation
//        for (i = 1; i <shockLocation    ; i++){
//            //upstream "before" shock location
//            Mfld[fldP][i] = Mfld[fldP][i-1] + deltaFLD;
//            // M = f(fld, branch)
//            Lfld [i] =  maxPossibleFLDsup -  Mfld[fldP][i]; 
//            Mfld[MxP][i] = calMForFLD(  Lfld [i], 10.0 );
//            Mfld[MyP][i] = 0.0 ;
//            Mfld[PyPxP][i] = 0.0 ;
//            Mfld[PxPsP][i] = calPressureRatio( Mfld[MxP][i]);//p(i+1)/p*
//            Mfld[PyPsP][i] = 0.0;//p(i+1)/p*
//            Mfld[P1PiP][i] =  p1pStar/Mfld[PxPsP][i];//[p1/p(i)]*[p(i-1)/p(i)]
//			//tmp reverse
//			Mfld[P1PiP][i] = 1./ Mfld[P1PiP][i] ;
//        }
//
//		//@ shock location calculation
//		//fld
//		Mfld[fldP][shockLocation] = ((double) shockLocation) * deltaFLD;
//		//Mx
//		Mfld[MxP][shockLocation] =
//			 calMForFLD(maxPossibleFLDsup - Mfld[fldP][shockLocation],
//							 10.0 ); //sup branch
//		//cal My
//		Mfld[MyP][shockLocation] = s.calMy( Mfld[MxP][shockLocation] ); 
//		//cal pypx
//		Mfld[PyPxP][shockLocation] = s.calPressureRatio(
//				 Mfld[MxP][shockLocation], Mfld[MyP][shockLocation] ); 
//		//cal pxpStar
//		Mfld[PxPsP][shockLocation] = calPressureRatio(Mfld[MxP][shockLocation]);
//		//cal pypStar 
//		Mfld[PyPsP][shockLocation] = calPressureRatio(Mfld[MyP][shockLocation]);
//		//cal p1/p(i)
//		Mfld[P1PiP][shockLocation] =  p1pStar/Mfld[PxPsP][shockLocation]
//				/ Mfld[PyPxP][shockLocation];
// 
//
//		//After the shock
//		Lfld [shockLocation] = calFLD(Mfld[MyP][shockLocation] );
//		for (i= (shockLocation+1); i   < numberNodes   ; i++){
//			//downstream "after" shock location
//	        Mfld[fldP][i ] = Mfld[fldP][i-1] + deltaFLD;
//			// M = f(fld, branch) 
//			Lfld [i] =  Lfld [shockLocation] -
//					 (double) (i - shockLocation) * deltaFLD;	
//			Mfld[MxP][i ] = calMForFLD(  Lfld [i], 0.1 );
//			Mfld[MyP][i ] = 0.0 ;
//			Mfld[PyPxP][i] = 0.0 ;
//			Mfld[PxPsP][i] = calPressureRatio( Mfld[MxP][i]);//p(i+1)/p*
//			Mfld[PyPsP][i] = 0.0;//p(i+1)/p*
//			Mfld[P1PiP][i] =  Mfld[P1PiP][shockLocation]*
//					Mfld[PyPsP][shockLocation] /Mfld[PxPsP][i] ; 
//          //tmp reverse pressure ratio 
//			Mfld[P1PiP][i] = 1.0 /   Mfld[P1PiP][i]; 
//		}
//          //tmp reverse pressure ratio 
//		 Mfld[P1PiP][shockLocation] = 1./ Mfld[P1PiP][shockLocation];
//		return yes;
//    }
//	else if (variableName == M1fldP2P1V) { //find shock location
//		double P2Pstar1; // P2/Pstar1 = (P2/P1)/( P^*1/P1)  
//		double Pstar1P1; // P^*1/P1  
//		double shockLocation;
//		stagnation st(k);
//		shock s(k);
//		fld = FLD = variableValue[fldN];
//		M1 =  m1 = variableValue[m1N];
//		p2p1 =  variableValue[p2p1N];
//		Pstar1P1 = st.calPressureRatio(M1); 
//		P2Pstar1 = p2p1/Pstar1P1;
//		shockLocation = 
//			root ( calShockLocation_Estmt,  this,  0., fld ,  P2Pstar1  ) ;
//		M2 = 1.0;
//		fldUp = fld * shockLocation;
//		fldDown = fld - fldUp;
//		My =  calMForFLD (fldDown , 0.1 ); //subsonic branch
//		Mx =  s.calMy(My) ; //symmetrical function 
//		return yes;	
//	}
//	return no;
//};

int   compressibleFlow::makeTable ( int * ioData, double * variableValue ) {
	//IOD = ioData ;
	outputInfo = (outputType) ioData[whatInfoN];
	inputInfo = (inputType) ioData[variableNameN];
	isBothBranches = (inputType) ioData[isBothBranchesN];
	isChoked = (inputType) ioData[isChokedN];
	isR = (inputType) ioData[isRangeN];
	// inputVariableName = ioData[variableNameN];
	convertInputName( inputInfo );//set the input name
	if ( ioData[isRangeN] == no ) {
		if (calTableLine ( ioData,  variableValue) == no) {
			return no;
		}
		if (ioData[isTexN] == yes){
			//make the LaTeX table
			makeTexTableHead(ioData); 
			makeTexTableLine(ioData); 
			makeTexTableTail(ioData);
		}
		else if (ioData[isHtmlN] == yes){
			//make the HTML table
			makeHTMLTableHead( ioData); 
			makeHTMLTableLine( ioData); 
			makeHTMLTableTail(ioData);
		}
		else {
			showHead(ioData);
			showResults(ioData);
		}
	}
	else { // isRange==yes
		manegerCalTableLine( ioData,  variableValue);
	}
	return yes;
};

int  compressibleFlow::setRange(int * ioData ,
		  double * range, double * variableValue)
{
	int rangeNumber;
	rangeNumber = (int) range[0]; 
	for (int i = 1 ; (i <=  abs( rangeNumber) ) ; i++ ) {
        makeLines( ioData, (int)  range[i*3-2],
			range[i*3-1],  range[3*i],  variableValue);

	}
	return yes;
};


int compressibleFlow:: setVariableInRange ( int * ioData , double *variableValue,  double h){ 
	if ( isChoked == no ) {
		if ( ( rangeParameter) ioData[whatRangeParameterN] == M1range ){
			variableValue[M1V ] = h;
		} 
		else {
			variableValue[(inputType) (ioData[whatRangeParameterN] ) ] = h;
		}
	}
	else {
		variableValue[ (inputType) (ioData[whatRangeParameterN] ) ] = h;
		 if ( (inputType) (ioData[whatRangeParameterN] ) == Mrange){
			//cout << "test is okay" << endl;
			variableValue[machV ] = h;
		} 
	}
	return yes;
};


int compressibleFlow:: makeLines ( int * ioData , int numberOFsteps,
		double startingPoint, double lastPoint, double * variableValue ){
	int isItWorking = yes;
	//inputType variableName = (inputType) ioData[variableNameN];
	int  isTex = ioData[isTexN];
	double h, range, delta ;
	range = lastPoint - startingPoint ; 
	delta = range /((double) numberOFsteps); 
	h = startingPoint;
	for (int i = 0 ; (i <= abs(numberOFsteps) ) ; i++ ) {
		setVariableInRange(ioData, variableValue, h);
		//variableValue[inputType(ioData[variableNameN]) ] = h ;
		//isothermalNozzle G(k);
		//G.calMach(ioData, variableValue);
		//G.calAreaRatio();
		//URstar = G.getMach();
		if ( calTableLine( ioData, variableValue) == yes ){
			if (isTex == yes) {
				//changeMach ((double) h );
				makeTexTableLine(ioData);
			}
			else if (ioData[isHtmlN] == yes ) {
				makeHTMLTableLine(ioData);
			}
			else {
				showResults(ioData);
			};
		}
		else{
			cout << "no results are possible too large 4fL/D"<< endl; 
			isItWorking = no;
			//return no;
		};
        h = h + delta;
    };
	if (isItWorking == no) {
		cout << "some runs didn't work" << endl;
    	return no;
	}
	else {
    	return yes;
	}
};

int compressibleFlow:: makeLines (int *ioData, int numberOFsteps,
		double startingPoint, double lastPoint ){
	double h, range, delta;
	double variableValue[7];
	int  isTex = ioData[isTexN];
	range = fabs(startingPoint - lastPoint); //make the range positive
	delta = range / double (numberOFsteps); 
	h = startingPoint;
	for (int i = 0 ; (i <= abs(numberOFsteps)) ; i++ ) {
		variableValue[mN] = h; // much number
		ioData[variableNameN] = int( machV) ;
		calTableLine( ioData, variableValue);
		if (isTex == yes) {
			makeTexTableLine(ioData);
		}
		else {
			showResults(ioData);
		};
		h = h + delta;
	};
	return yes;
};

int compressibleFlow::manegerCalTableLine (int *ioData, double *variableValue){
	inputType variableName = (inputType) ioData[variableNameN];
	int  isTex = ioData[isTexN];
	rangeParameterName = (rangeParameter) ioData[whatRangeParameterN] ;
	isFLDknown = ioData[isFLDknownN] ;
	//make the head of the table
	if (isTex == yes) {
		makeTexTableHead ( ioData ) ;
	}
	else if ( ioData[isHtmlN] == yes){ 
		makeHTMLTableHead (ioData);
	}
	else {
		showHead(ioData);
	};
	if (outputInfo == infoStandard || outputInfo == infoObliqueMax
			|| outputInfo == infoD0 || outputInfo == infoFn ) {
		doAllLines(ioData,  variableValue);
		goto outForNow;
	}
	else if (outputInfo == infoTube ) {
		if (drivedClassName == isothermalFlow ){
		  makeLines(ioData,98,2.0,100.0, variableValue);
		}
		else if  (drivedClassName == fannoFlow ){
          makeLines(ioData,98,2.0,100.0,  variableValue);
        }
		goto outForNow;
	}
	else if (outputInfo == infoStagnation ) {
        if (drivedClassName == isothermalNozzleFlow ){
			makeLines(ioData, 7,0.03,  0.1,  variableValue );
			makeLines(ioData, 12,0.2,  0.8, variableValue );
			makeLines(ioData, 4,0.81,  singularPoint,  variableValue );
			makeLines(ioData, 4, singularPoint, 1.0, variableValue );
			makeLines(ioData, 40, 1.1, 4.0,  variableValue );
        }
        goto outForNow;
    }
	else if (outputInfo == infoTubeProfile ) {
		double ll;
        if (drivedClassName == isothermalNozzleFlow ){
			ll = 0.97 ;// Astar lower limit  
		}
		else if ( drivedClassName == stagnationFlow){
			ll = 1.0 ; // Astar lower limit for stagnation class
		}
		else {
			goto outForNow;
		}
		variableValue[mN] = 0.5  ;
		makeLines(ioData, 40,20.,  ll ,  variableValue);
		variableValue[mN] = 5.  ;
		makeLines(ioData, 40, ll , 20.,  variableValue);
		goto outForNow;
	}

	//makeLines(isTex 20,0.05,(singularPoint -EPS ), 
	//				variableName, variableValue);
	//makeLines( isTex, 20,(singularPoint +EPS ), 2.0, 
	//				variableName, variableValue);
	//check the special cases are need to the limiting borders
	else if ( variableName == M1ShockV){
		//calculated the minimum entrance and exit Mach number 
		shock s(k);
		minM1entrance = calMForFLD(variableValue[fldN], 0.1); // tmp storage
		minM1entrance = s.calMy(minM1entrance) + 5*EPS;
		minM1exit = calMForFLD(variableValue[fldN], 10.0); //supper sonic flow no
									//shock at 1.0
    	makeLines( ioData, 2, minM1exit, minM1entrance, variableValue);

	}
	else if ( variableName == FLDShockV) {
		//the boundaries of aren't relevant since the output m=(fld
		//or x)
		makeLines(ioData, 2, 0., 0.,  variableValue);
	} 
	else if ( variableName == M1fldV) {
        makeLines( ioData, 2, 0., 0.,  variableValue);
    }

	//makeLines(ioData, 30, 110, 1000,variableName,  variableValue);
	if (isTex == yes) {// the tail for TeX if needed
        makeTexTableTail(ioData);
    };
    return yes;
	// now make tail for the right format
	outForNow:  
		makeTail(ioData);
	return yes;
};

void compressibleFlow:: makeTail (int * ioData ){
	if (ioData[isTexN] == yes) {
		makeTexTableTail(ioData);
	}
	else if (ioData[isHtmlN] == yes){
		makeHTMLTableTail(ioData);
	};
}

int compressibleFlow:: manegerCalTableLine (int * ioData ){
    // manage the lines calculations.  first if all exist.
    // to be complete later
		// first calculate the parameters 
		// now print either for TeX or HTML, or without TeX
	//first print the header depending on the TeX or ''graph'' table
	int isTex, isHTML = 0 ;
	isTex = ioData[isTexN];
	isHTML = ioData[isHTML];
	if (isTex == yes) {
		makeTexTableHead ( ioData ) ;
	}
	else{
		showHead(ioData);
	};
	//make the line or the lines
	makeLines( ioData, 18,	0.05, 0.95);
	makeLines( ioData, 2,0.96,(singularPoint -0.001 ));
	makeLines(ioData, 20,singularPoint+ 0.0000001,2.);
	makeLines(ioData, 10,2.5,10.);
	// print the tail for TeX if needed
	if (isTex == yes) {
        makeTexTableTail(ioData);
    }
	else if (isHTML == yes) {
        makeHTMLTableTail(ioData);
	} 
    return yes;
};


void  compressibleFlow :: calP2P1(){
	// redo when P2star isn't known
	//P2Pstar = calPressureRatio (M2) ;  
	//P1Pstar = calPressureRatio (M1) ; 		
    p2p1 = P2Pstar/P1Pstar;

};


double  compressibleFlow :: calMForFLD(double FLD, double mach){
	//what do when there is no solution ?
	if ((mach < singularPoint) && (FLD > EPS ) ) { // sub sonic flow 
		return  root ( calFLD_Estmt,  this, .00001,  (M1maxSub-EPS), FLD  ) ;
	}
	else if ((mach > singularPoint )  && (FLD > EPS ) ) { // super sonic flow
		return  root ( calFLD_Estmt,  this,  (M1maxSub+EPS), 10000.,  FLD  ) ;
	}
	else if (FLD == 0.0 )  {
		return 0.0;
	}
	else { // no possible negative fld
		return -1.0;
	} 
};

void compressibleFlow :: convertInputName(inputType vName ){
	if (vName == machV)
		inputVariableName = "M";
	else if (vName == PbarV)
		inputVariableName = "Pbar";
	else if (vName == RbarV)
		inputVariableName = "&rho;bar";
	else if (vName == fldV)
		inputVariableName = "fld";
	else if (vName == p2p1FLDV)
		inputVariableName = "p2/p1 fld";
	else if (vName == angleV)
		inputVariableName = "angle";
	else if (vName == nuV)
		inputVariableName = "&nu;" ;
	else if (vName == M1ShockV)
		inputVariableName = "M1";
	else if (vName == FLDShockV)
		inputVariableName = "fld";
	else if (vName == M1fldV)
		inputVariableName =  "M1 fld";
	else if (vName == M1fldP2P1V)
		inputVariableName = "M1 fld P2P1";
	else if (vName == MxV)
		inputVariableName = "Mx" ;
	else if (vName == MyV)
		inputVariableName = "My" ;
	else if (vName == mDotP0TV)
		inputVariableName =  "mDot P0 T" ;
	else if (vName == mDotPT0V)
		inputVariableName = "mDot P T0" ;
	else if (vName == MFLDshockLocationV)
		inputVariableName =  "FLD shock Location" ;
	else if (vName == AstarV)
		inputVariableName = "A/A*" ;
	else if (vName == PARV)
		inputVariableName =  "PAR" ;
	else if (vName == P0yP0xV)
		inputVariableName = "P0y/P0x" ;
	else if (vName == T0yT0xV)
		inputVariableName = "T0y/T0x" ;
	else if (vName == R0yR0xV)
		inputVariableName = "&rho;0y/&rho;0x" ;
	else if (vName == TbarV)
		inputVariableName = "Tbar" ;
	else if (vName == RbarV)
		inputVariableName = "Rbar" ;
	else if (vName == TRstarV)
		inputVariableName = "T/T*" ;
	else if (vName == TRzeroStarV)
		inputVariableName = "T0/T0*" ;
	else if (vName == deltaMV)
		inputVariableName =   "&delta; Mx" ;
	else if (vName == thetaMV)
		inputVariableName = "&theta; Mx" ;
	else if (vName == deltaThetaV)
		inputVariableName = "&delta;&theta;" ;
	else if (vName == M2fldV )
		inputVariableName = "M2 fld" ;
	else if (vName == omegaV)
		inputVariableName = "&omega" ;
	else if (vName == PRzeroStarV)
		inputVariableName = "P0/P0*" ;
	else if (vName == MxpV)
		inputVariableName = "Mx&prime;" ;
	else if (vName == MypOpenV)
		inputVariableName = "My&prime;" ;
	else if (vName == MxOpenV)
		inputVariableName =  "Mx" ;
	else if (vName == MyOpenV)
		inputVariableName = "My" ;
	else if (vName == MxGV)
		inputVariableName = "Mx" ;
	else if (vName == MxMypPCV)
		inputVariableName = "Mx  My&prime;" ;
	else if (vName == MxpMypPOV)
		inputVariableName = "Mx&prime; My&prime;" ;
	else if (vName == MxpMyPOV)
		inputVariableName = "Mx&prime; My" ;
	else if (vName == MxpMypPCV)
		inputVariableName = "Mx&prime; My&prime;" ;
	else if (vName == UxpV)
		inputVariableName = "Ux&prime;" ;
	else if (vName == UyOpenV)
		inputVariableName = "Uy" ;
	else if (vName == UypTxOV)
		inputVariableName = "Uy&prime; Tx0" ;
	else if (vName == UsOpenV)
		inputVariableName = "Us" ;
	else if (vName == kV)
		inputVariableName = "k" ;
	else if (vName == P1V)
		inputVariableName = "P1" ;
	else if (vName == PyPxV)
		inputVariableName = "Py/Px" ;
	else if (vName == TyTxV)
		inputVariableName = "Ty/Tx" ;
	else if (vName == RyRxV )
		inputVariableName = "&rho;y/&rho;x" ;
	else if (vName == PyPxOpenV)
		inputVariableName = "Py/Px" ;
	else if (vName == fldP2P1V)
		inputVariableName = "fld  P2/P1" ;
	else if (vName == M1P2P1V)
		inputVariableName = "M1  P2/P1" ;
	else if (vName == M2P2P1V)
		inputVariableName = "M2  P2/P1" ;
	else  // default
		inputVariableName = "M";
};

double calP_ratioEstmt ( compressibleFlow c, double estimatedMach, double value) {
    c.changeMach (estimatedMach) ;
    //c.calArea();
    c.calFLD() ;
    c.calPressureRatio() ;
    c.calTotalPressureRatio() ;
    //s.calTotalTemperatureRatio() ;
    //s.calDensityRatio() ;
    return (value - c.getFLD() ) ;

};

