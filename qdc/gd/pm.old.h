/********************************************************
	file: pm.h prandtl-Meyer function

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/
#ifndef _PM_STDLIB
#define _PM_STDLIB

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip> 
#  include "definition.h"
#  include "compressibleFlow.h"

class pm: public stagnation {
        protected:
			//local functions and veriables to be access via other
			//functions 
			double  a1, a2, a3 ,Mx2, Mx4, sinDelta, cosDelta, R, Q, D, S ;
    		double x1, x2, x3, chi;
    		int Rsign;
			int  calNu(void) ;
			void calNormalComponent(void);
        public:
			double Myn[2]; //My normal components weak and strong accordently
			double Mxn[2]; //Mx normal components weak and strong accordently 
			double maxDelta; //max Delta for given Mx 

			// to change the initialization later on
			// so that base class initialization will be considered
			pm(double spesificHeatRatio ):
				//initialization of base class
				stagnation (spesificHeatRatio )
					 { 
						//specific initialization process for derived class
						//the critical M is ? for pm flow
						M1maxSub = 1.0;
						singularPoint = M1maxSub ; 
			  		};
			~pm(){};
			//spesific functions of this model of flow to calculate
			//spesific parameters of this flow model.
			// 
            void setMach(double Mach) {M = Mach ;};
			void setAngle(double dd) {nu = dd;};
			void calMach( int, double * ) ;
			int  calNu(int, double * ) ;
            void makeTexTableHead(int );
            void makeTexTableLine (int );
			int calAll(int, int, double *);
            int  doAllLines(int, int, int, double *);
            int  calTableLine (int,  int, double * );
			void showResults(int );
			void showHead(int ) ;
            int selectVariable (int)  ;
			void calMaxDelta(int, double *) ;
            double getAngle(){return nu ; };
            double calAngle(){calNu(); return nu ; };


			//functions that do not touch the class variables
			//double calFLD(double) ;
			//double calPressureRatio(double) ;
			//double calDensityRatio(double);
			//double calVelocityRatio(double);
			//double calTotalPressureRatio(double) ;
			//double calTotalTemperatureRatio(double);
			//double calTemperatureRatio(double);
			//double calArea(double);
            //double getTemperatue (){return TyTx ; };
            //double getDensity (){return RyRx ; };
            //double getArea (){return Area ; };

};
#endif  /* _PM_STDLIB */
