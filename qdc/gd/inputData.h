// last change Wed Jul 11 15:26:30 CDT 2007 change public stagnation to
// compressibleFlow 
/********************************************************
	file: readFile.h  to read data functions

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	to write ?

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/
#ifndef _READFILE_STDLIB
#define _READFILE_STDLIB

#  include "iniFile.h"
#  include <cmath>
#  include <iostream>
#  include <string>
#  include <iomanip> 
//#  include "definition.h"
#  include "compressibleFlow.h"

class readFile: public compressibleFlow {
        protected:
			//local functions and veriables to be access via other
			//functions 
			//double  a1, a2, a3 ,Mx2, Mx4, sinDelta, cosDelta, R, Q, D, S ;
        public:
			//double Mxn[2]; //Mx normal components weak and strong accordently 

			readFile( )  ;
			~readFile(){};
			//spesific functions of this model of flow to calculate
			//spesific parameters of this flow model.
			// 

};
	int readFile (double *, int * , string )  ;
	int writeFile (double *, int * , string )  ;
	int readDefaultData (double *, int * )  ;

#endif  /* _READFILE_STDLIB */
