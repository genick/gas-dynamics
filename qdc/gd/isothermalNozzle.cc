/********************************************************
 * last change Mon Dec 19 10:01:51 CST 2011 cast conversion from string
 * constant to ‘char*’ 
**********************************************************************
	FILE:isothermalNozzle.cc

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, isothermalNozzle and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip> 
#  include "isothermalNozzle.h" 
#  include "externalFun.h" 

using namespace std;

// the isothermalNozzle class functions definitions
//**********************************************//

void  isothermalNozzle::calMach(int * ioData,  double *variableValue){
	inputType variableName =  (inputType) ioData[variableNameN];
	if (variableName == machV) {
		M = variableValue[machV];
	}
	else if (variableName == TRzeroStarV) {
		TRzeroStar = variableValue[TRzeroStarV];
		M = root  ( calTRzeroStarEstmt, this, EPS,  500., TRzeroStar) ;
    }
	else if (variableName == PRzeroStarV) {
		PRzeroStar = variableValue[PRzeroStarV];
		M = variableValue[machV];
		if ( PRzeroStar > exp(k*0.5) * 2./(1.+k) ) {//not possible range
			outputErrorMessage = "P0/P0* is out range! too large";	
			M = -1.;
		}
		else if ( PRzeroStar == 1. ) //sonic
			M = 1.;
		else if ( PRzeroStar > 1. ) //subsonic
			M = root(calPRzeroStarEstmt,this,EPS, 1.0,PRzeroStar);
		else  if (PRzeroStar < 1. ) //supersonic
			M = root(calPRzeroStarEstmt,this,1.0,300.,PRzeroStar);
		else
			M = 1.0;
    }
	else if (variableName == RRzeroStarV) {
		RRzeroStar = variableValue[RRzeroStarV]; 
		PRzeroStar = 1./RRzeroStar;
		M = variableValue[machV];
		if (  PRzeroStar > exp(k*0.5)* pow((2.0/(k+1.)), (k /(k -1.))) ) {
			M = -1. ;
			return ;
		} 
		else if (  PRzeroStar == exp(k*0.5)* pow((2.0/(k+1.)), (k /(k -1.))) ) 
			M = 0.;
		else if ( PRzeroStar < 1. )
			M = root(calPRzeroStarEstmt,this,1.0,300.,PRzeroStar);
		else  if (PRzeroStar > 1. )
			M = root(calPRzeroStarEstmt,this,.00001, 1.0,PRzeroStar);
		else
			M = 1.0;
    }
	else if (variableName == RbarV) {
		Rbar = variableValue[RbarV];
		Pbar = Rbar;
		M = root  ( calPbarEstmt, this, .00001,  100., Pbar) ; 
	}
	else if (variableName == PbarV) {
		Pbar = variableValue[PbarV];
		M = root  ( calPbarEstmt, this, .00001,  100., Pbar) ; 
	}
	else if (variableName == TbarV) {
		//Tbar = variableValue[TbarV];
		TRzeroStar = variableValue[TbarV];
		M = variableValue[machV];
		M = root  ( calTRzeroStarEstmt, this, .00001,  100., TRzeroStar) ; 
	}
	else if (variableName == AstarV) {
		Area = variableValue[AstarV];
		if ( variableValue[machV] < singularPoint ) {
			M = root  ( calAstarEstmt, this, .00001,  M1maxSub, Area) ;
		}
		else {
			M = root  ( calAstarEstmt, this, M1maxSub, 20., Area) ;
		}
    }
	else if (variableName == PARV) {
		PAR = variableValue[PARV];
		if (PAR == 1.)
			M = 1;
		else if (PAR > 1.) // subsonic 
			M = root  ( calPAREstmt, this, .00001,  M1maxSub, PAR) ;
		else // supersonic 
			M = root  ( calPAREstmt, this, M1maxSub, 100.,  PAR) ;
	}
};

void  isothermalNozzle::calPressureRatio(){
	Pbar = exp((1-M*M  )*k*0.5) ;
};

double   isothermalNozzle::calPressureRatio(double MM){
    return  pow (1. / ( 1. + (( k - 1.) / 2. ) * MM*MM),(k / (k - 1.) ) ) ;
};

void  isothermalNozzle::calTemperatureRatio(){
	Tbar = 1. / ( 1. + (( k - 1.) / 2. ) * M*M);
};

void  isothermalNozzle::calDensityRatio(){ 
	 Rbar = pow ( Pbar, (1./k)) ;
};
void  isothermalNozzle::calAreaRatio(){
	Area = exp((M*M -1 )*k*0.5) / M;
};

void  isothermalNozzle::calPARRatio(){
	if (Pbar < EPS)  
		PAR = 0.;
	else 
		PAR =  Area * Pbar;
};

void isothermalNozzle::calTotalTemperatureRatio(){
	TRzeroStar = ((2. * (1.0 + 0.5 * (k -1) *M*M ))/(k+1.0));
	//double tmpTzR = TRzeroStar; 
	// (2. * (1.0 + 0.5 * (k -1) *M*M ))/(k+1.0) ;
} 

void  isothermalNozzle::calImpulse(){
    FRstar = (1. + k*M*M)/(1.+k)/M ;
}

void isothermalNozzle::calTotalPressureRatio(){
	PRzeroStar = exp((1 - M*M  )*k*0.5) *  
		pow((2.*(1.0 + 0.5 * (k -1) *M*M )/(1.+k)), (k / (k -1.0))) ;
} 

void    isothermalNozzle::showHead(int * ioData ){

    cout << endl << endl <<  "Isothermal Nozzle" << "\t\t"  ;
    cout << "k = " << k << endl << endl ;

    if (outputInfo == infoStandard || outputInfo == infoStagnation ) {
        int variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "M  ";
        variableNames[1] = (char *) "T0/T0*";
        variableNames[2] = (char *) "P0/P0*";
        variableNames[3] = (char *) "A/A*";
        variableNames[4] = (char *) "P/P*";
        variableNames[5] = (char *) "PAR";
        variableNames[6] = (char *) "F/F*";

        showVariableHead( variableNames, variableNumber);
    }
	if (outputInfo == infoTubeProfile ) {
        int variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "z";
        variableNames[1] = (char *) "M";
        variableNames[2] = (char *) "T0/T0*";
        variableNames[3] = (char *) "P0/P0*";
        variableNames[4] = (char *) "A/A*";
        variableNames[5] = (char *) "P/P*";
        variableNames[6] = (char *) "PAR";
                                                                               
        showVariableHead( variableNames, variableNumber);
    }

}

void    isothermalNozzle::showResults(int * ioData ) {
	int numberVariables;
	if (outputInfo == infoStandard || outputInfo == infoStagnation ) {
		numberVariables = 7;
		double variableToBePrinted[numberVariables];
		if (isBothBranches == no) {
			variableToBePrinted[0] = M ;
			variableToBePrinted[1] = TRzeroStar;
			variableToBePrinted[2] = PRzeroStar;
			variableToBePrinted[3] = Area;
			variableToBePrinted[4] = Pbar ;
			variableToBePrinted[5] = PAR;
			variableToBePrinted[6] = FRstar;
			showVariable(variableToBePrinted, numberVariables);
		}
		else if (isBothBranches == yes) {
			for (int i = 0 ; i < 2 ; i++){
				for (int j = 0 ; j < numberVariables ; j++){
					variableToBePrinted[j] =  steps[i][j];
				}
				showVariable(variableToBePrinted, numberVariables);
			}
		}
	}
	else if (outputInfo == infoTubeProfile ) {
		numberVariables = 7;
		double variableToBePrinted[numberVariables];
		if (M < singularPoint) {
        	variableToBePrinted[0] = 1.0509734 - Area* 0.052548607  ;
		}
		else {
			variableToBePrinted[0] = 0.949027851 + Area* 0.052548607 ;
		}
		variableToBePrinted[1] = M ;
		variableToBePrinted[2] = TRzeroStar;
		variableToBePrinted[3] = PRzeroStar;
		variableToBePrinted[4] = Area;
		variableToBePrinted[5] = Pbar ;
		double tmpA, tmpB;
		tmpA = pow((1.0+0.5*(k-1)*M*M),(k / (k - 1.) ));
		tmpB = exp(0.5*k) * pow((2.0/(k+1.)),(k/(k-1.0)));
		variableToBePrinted[6] =  PRzeroStar/ tmpA /tmpB; //PAR; 
		showVariable(variableToBePrinted, numberVariables);
	}
}

void  isothermalNozzle::makeTexTableHead(int * ioData){ 
	int variableNumber;
	if (outputInfo == infoStandard || outputInfo == infoStagnation ) {
		variableNumber = 7;
		char * variableNames[variableNumber];
		variableNames[0] = (char *) "M";
		variableNames[1] = (char *) "T_0 \\over {T_0}^{\\star}";
		variableNames[2] = (char *) "P_0 \\over {P_0}^{\\star}";
		variableNames[3] = (char *) "A \\over A^{\\star}";
		variableNames[4] = (char *) "P \\over P^{\\star}";
		variableNames[5] = (char *) "A\\times P \\over A^{*} \\times P_0";
		variableNames[6] = (char *) "F \\over F^{*}";
		showVariableTeXHead(variableNames,variableNumber);
	}
}

void  isothermalNozzle::makeTexTableLine(int * ioData){
	int numberVariables;
	if (outputInfo == infoStandard  || outputInfo == infoStagnation ) {
		numberVariables = 7;
		double variableToBePrinted[numberVariables];
		if (isBothBranches == no) {
			variableToBePrinted[0] = M ;
			variableToBePrinted[1] = TRzeroStar;
			variableToBePrinted[2] = PRzeroStar;
			variableToBePrinted[3] = Area;
			variableToBePrinted[4] = Pbar ;
			variableToBePrinted[5] = PAR;
			variableToBePrinted[6] = FRstar;
			showVariableTeXLine (variableToBePrinted, numberVariables);
		}
		else if (isBothBranches == yes) {
			for (int i = 0 ; i < 2 ; i++){
				for (int j = 0 ; j < numberVariables  ; j++){
					variableToBePrinted[j] =  steps[i][j];
				}
				showVariableTeXLine (variableToBePrinted, numberVariables);
			}
		}
	}
}

void   isothermalNozzle::saveResults (int i ) {
   steps[i][0] = M ;
   steps[i][1] = TRzeroStar;
   steps[i][2] = PRzeroStar;
   steps[i][3] = Area;
   steps[i][4] = Pbar ;
   steps[i][5] = PAR;
   steps[i][6] = FRstar;
}

int   isothermalNozzle::calAll(int * ioData, double * variableValue ){ 
        calMach(ioData, variableValue);
        calAreaRatio();
        calPressureRatio();
        calTotalTemperatureRatio();
        calTotalPressureRatio();
        calDensityRatio ();
        calPARRatio();
        calImpulse();
        return yes ;
}

int isothermalNozzle:: calTableLine ( int* ioData, double * variableValue){
    if (outputInfo == infoStandard || outputInfo == infoStagnation){
        return calAll(ioData, variableValue);
    }
    else  {
        return no;
    }
};

int   isothermalNozzle::doAllLines (int * ioData, double * variableValue ){
	int howManyPoints ;
	if (outputInfo == infoStandard ){
		howManyPoints =  2 ;
		makeLines(ioData,howManyPoints, (.0+EPS), 0.1, variableValue);
		howManyPoints =  15 ;
		makeLines(ioData,howManyPoints, 0.2, 0.95, variableValue);
		howManyPoints =  19 ;
		makeLines(ioData,howManyPoints, 1.0, 1.95, variableValue);
		howManyPoints =  32 ;
		makeLines(ioData,howManyPoints, 2., 10., variableValue);
		howManyPoints =  3 ;
		makeLines(ioData,howManyPoints, 20., 50., variableValue);
	}
	return yes;
}

void    isothermalNozzle::makeHTMLTableHead(int * ioData){
    int variableNumber;
                                                                               
    if (outputInfo == infoStandard || outputInfo == infoMinimal) {
        variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "M  ";
        variableNames[1] = (char *) "T0/T0*";
        variableNames[2] = (char *) "P0/P0*";
        variableNames[3] = (char *) "A/A*";
        variableNames[4] = (char *) "P/P*";
        variableNames[5] = (char *) "PAR";
        variableNames[6] = (char *) "F/F*";

        showVariableHTMLHead(variableNames,variableNumber);
    }
};

void    isothermalNozzle::makeHTMLTableLine(int * ioData) {
	int numberVariables;

	if (outputInfo == infoStandard || outputInfo == infoMinimal) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
		if (isBothBranches == no) {
			variableToBePrinted[0] = M ;
			variableToBePrinted[1] = TRzeroStar;
			variableToBePrinted[2] = PRzeroStar;
			variableToBePrinted[3] = Area;
			variableToBePrinted[4] = Pbar ;
			variableToBePrinted[5] = PAR;
			variableToBePrinted[6] = FRstar;
			showVariableHTMLLine (variableToBePrinted, numberVariables);
		}
		else if (isBothBranches == yes) {
			for (int i = 0 ; i < 2 ; i++){
				for (int j = 0 ; j < numberVariables ; j++){
					variableToBePrinted[j] =  steps[i][j];
				}
				showVariableHTMLLine (variableToBePrinted, numberVariables);
			}
		}
	}
};

