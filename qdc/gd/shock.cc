/********************************************************
 * last change Mon Dec 19 12:11:35 CST 2011 cat conversion from string
 * constant to ‘char*’ 
**********************************************************************
	FILE:shock.cc


	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip>
#  include "stagnation.h"
#  include "externalFun.h"
#  include "shock.h" 
#  include "shock.h" 
#  include "gasDynamics.h" 
using namespace std;

/// shock class implimantion  
//******************************************************************

void  shock::calMach(int variableName, double * variableValue){
    if (variableName == MxV) {
        Mx = variableValue[MxV];
    }
};

void  shock::calMx(int variableName, double * variableValue){
	double tmpMx;
    if (variableName == P0yP0xV) {
		double tmpP0yP0x;
        P0yP0x = variableValue[P0yP0xV];
		tmpP0yP0x = P0yP0x;
        Mx = root  ( calP0yP0xEstmt, this, M1maxSub, 100.0, P0yP0x) ;
		return;
    }
    else if (variableName == R0yR0xV) {
		double tmpR0yR0x;
        R0yR0x = variableValue[R0yR0xV];
		tmpR0yR0x = R0yR0x;
        Mx = root  ( calR0yR0xEstmt, this, M1maxSub, 100.0, R0yR0x) ;
		return;
    }
	else if ((variableName == MxV) || (variableName == machV)){ 
		Mx = variableValue[MxV] ;
		calMy();
		if ( Mx < 1.0) {
			tmpMx = Mx ;
			Mx = My;
			My = tmpMx;
		}
	}
    else if ( variableName == MyV ){
		double tmpMx ;
		double tmpMy = Mx = variableValue[MyV] ;
        calMy();
		tmpMx = Mx;
		tmpMy = My;
        if ( Mx < 1.0) {
            tmpMx = Mx ;
            Mx = My;
            My = tmpMx;
        }
    }
	else if ( variableName == PyPxV ){
        double tmpPbar,tmpMx;
		PyPx = variableValue[PyPxV] ;
		tmpPbar = PyPx;
		Mx = root  ( calPbarEstmt, this,  M1maxSub, 50., PyPx) ;
		tmpMx= Mx;
		tmpMx += 0.0;
    }
    else if ( variableName == TyTxV ){
        double tmpTbar,tmpMx;
        TyTx = variableValue[TyTxV] ;
        tmpTbar = TyTx;
        Mx = root  ( calTbarEstmt, this,  M1maxSub, 50., TyTx) ;
        tmpMx= Mx;
        tmpMx += 0.0;
    }
    else if ( variableName == RyRxV ){
        double tmpRbar,tmpMx;
        RyRx = variableValue[RyRxV] ;
        tmpRbar = RyRx;
        Mx = root  ( calRbarEstmt, this,  M1maxSub, 50., RyRx) ;
        tmpMx= Mx;
        tmpMx += 0.0;
    }
    else if ( variableName == RbarV ){
        double tmpRbar,tmpMx;
        RyRx = variableValue[RbarV] ;
        tmpRbar = RyRx;
        Mx = root  ( calRbarEstmt, this,  M1maxSub, 50., RyRx) ;
        tmpMx= Mx;
        tmpMx += 0.0;
    }

}
	
void  shock::calMx(){
    Mx = sqrt((My*My + 2.0/(k -1.0)) /
        ((2.0*k / (k-1.0) ) *My*My -1.0)) ;
		double tmpMy;
		tmpMy=Mx;
};


void  shock::calMy(){
	double tmpCondition, tmpMx;
	tmpCondition = sqrt(2.0*k / (k-1.0) );
	tmpMx = Mx;
	if ( Mx * Mx* (2.0*k / (k-1.0) ) > 1.0 ) { 
    	My = sqrt((Mx*Mx + 2.0/(k -1.0)) /
        	((2.0*k / (k-1.0) ) *Mx*Mx -1.0)) ;
	}
	else {
		My = -1.0;
	}
		double tmpMy, kTmp;
		kTmp = k;
		tmpMy=My;
};

double  shock::calMy(double m){
	if (  Mx < sqrt(2.0*k / (k-1.0) ) )
    	return  sqrt((m*m + 2.0/(k -1.0)) / ((2.0*k / (k-1.0) ) *m*m -1.)) ;
	else 
		return -1.0;
};

void  shock::calPressureRatio(void){
	double tmpMx, tmpMy, tmpk,tmpPyPx;
	tmpMx = Mx;
	tmpMy = My;
	tmpk =k;
	PyPx =  (1. + k*Mx*Mx) / (1. + k*My*My) ;
	tmpPyPx =PyPx;
	tmpPyPx += 0.;
};

double   shock::calPressureRatio(double MMx, double MMy){
    return  (1. + k*MMx*MMx) / (1. + k*MMy*MMy) ;
};

void  shock:: calDensityRatio (){
	if (TyTx > 0.)
		UxUy = RyRx = PyPx/TyTx;
	else 
		UxUy = RyRx = 0.;
};
                                                                                
void  shock::calTotalDensityRatio(){
    R0yR0x =  RyRx *
    pow((1. + ((k-1.)/2.)*My*My),(1./(k-1.))) /
    pow((1. + ((k-1.)/2.)*Mx*Mx),(1./(k-1.))) ;
};

void  shock::calTotalTemperatureRatio(){
    T0yT0x =  (PyPx)*
    pow((1 + ((k-1)/2)*My*My),(k/(k-1))) /
    pow((1 + ((k-1)/2)*Mx*Mx),(k/(k-1))) ;
};

void  shock::calTotalPressureRatio(){
    P0yP0x =  (PyPx)*
    pow((1 + ((k-1)/2)*My*My),(k/(k-1))) /
    pow((1 + ((k-1)/2)*Mx*Mx),(k/(k-1))) ;
};

double  shock::calTotalPressureRatio(double MMx, double MMy){
	PyPx = calPressureRatio(MMx, MMy);
    return   PyPx*
    pow((1. + (0.5*(k-1.))*MMy*MMy),(k/(k-1.))) /
    pow((1. + (0.5*(k-1.))*MMx*MMx),(k/(k-1.))) ;
};
                                                                                
void  shock::calTemperatureRatio(){
    TyTx = PyPx * PyPx * My*My/Mx/Mx;
};
                                                                                
void  shock::calArea(){
    Area =  pow((( 1 + (( k - 1) / 2 ) * Mx*Mx)/((k+1)/2)),
            ((k+1) / 2/(k-1) ))
                / Mx;
};

void    shock::makeTexTableHead(int * ioData) {
                                                                               
    int variableNumber;
                                                                               
    if (outputInfo == infoStandard || outputInfo == infoMinimal) {
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "M_x";
        variableNames[1] = (char *) "M_y";
        variableNames[2] = (char *) "T_y \\over T_x";
        variableNames[3] = (char *) "\\rho_y \\over \\rho_x";
        variableNames[4] = (char *) "P_y \\over P_x";
        variableNames[5] = (char *) "{P_0}_y \\over {P_0}_x";
                                                                               
        showVariableTeXHead(variableNames,variableNumber);
    }
}

void    shock::makeTexTableLine(int * ioData) {
    int numberVariables;

    if (outputInfo == infoStandard || outputInfo == infoMinimal) {
        numberVariables = 6;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx ;
        variableToBePrinted[1] = My;
        variableToBePrinted[2] = TyTx;
        variableToBePrinted[3] = RyRx;
        variableToBePrinted[4] = PyPx ;
        variableToBePrinted[5] = P0yP0x;

        showVariableTeXLine (variableToBePrinted, numberVariables);
    }
}

int   shock::calAll(//inputType variableName,
				 double * variableValue ){
    calMx(inputInfo, variableValue);
	calAreaRatio();
	if (inputInfo != PbarV){ 
		calPressureRatio();
	}
	if (inputInfo != TbarV){
		calTemperatureRatio();
	}
    //if (variableName != TbarV){
		calTotalPressureRatio();
	//}
    //if (variableName != TrV){
		calDensityRatio ();
	//}
	return yes;
};

int shock:: calTableLine (int* ioData, double * variableValue){
    if ((outputType) ioData[whatInfoN] == infoStandard || 
		(outputType) outputInfo == infoMinimal ){
            return calAll( variableValue);
    }
	else {
    	return no;
	}
};


int shock:: calTableLine (inputType variableName,
            double * variableValue){
    if (outputInfo == infoStandard || outputInfo == infoMinimal ){
            return calAll( variableValue);
    }
    return no;
};


int   shock::doAllLines (int * ioData, double * variableValue ){
	makeLines(ioData, 10,1.00,  1.1,  variableValue);
	makeLines(ioData, 16,1.2,  2.0,  variableValue);
	makeLines(ioData, 8, 2.1, 2.9,  variableValue);
	makeLines(ioData, 14,3.,10  ,  variableValue );
	makeLines(ioData, 10,20.,70  ,  variableValue );
	return yes;
}

void  shock::showResults(int * ioData) {
    int numberVariables;
    if (outputInfo == infoStandard   ) {
        numberVariables = 6;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx ;
        variableToBePrinted[1] = My;
        variableToBePrinted[2] = TyTx;
        variableToBePrinted[3] = RyRx;
        variableToBePrinted[4] = PyPx ;
        variableToBePrinted[5] = P0yP0x;

        showVariable(variableToBePrinted, numberVariables);
        return;
    }
};

void    shock::showHead(int * ioData){
    cout << endl << endl <<  "Shock Function " << "\t\t"  ;
    cout << "k = " << k << endl << endl ;
                                                                                
    if (outputInfo == infoStandard ) {
        int variableNumber = 6;
        char * variableNames[variableNumber];

        variableNames[0] = (char *) "Mx  ";
        variableNames[1] = (char *) "My";
        variableNames[2] = (char *) "TyTx";
        variableNames[3] = (char *) "RyRx";
        variableNames[4] = (char *) "PyPx";
        variableNames[5] = (char *) "P0yP0x";
                                                                                
        showVariableHead( variableNames, variableNumber);
    }
};

void    shock::makeHTMLTableHead(int * ioData){
    int variableNumber;
                                                                               
    if (outputInfo == infoStandard || outputInfo == infoMinimal) {
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "Mx";
        variableNames[1] = (char *) "My";
        variableNames[2] = (char *) "Ty/Tx";
        variableNames[3] = (char *) "&rho;y/&rho;x";
        variableNames[4] = (char *) "Py/Px";
        variableNames[5] = (char *) "P0y/P0x";
                                                                               
        showVariableHTMLHead(variableNames,variableNumber);
    }
};

void    shock::makeHTMLTableLine(int * ioData) {
    int numberVariables;

    if (outputInfo == infoStandard || outputInfo == infoMinimal) {
        numberVariables = 6;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx ;
        variableToBePrinted[1] = My;
        variableToBePrinted[2] = TyTx;
        variableToBePrinted[3] = RyRx;
        variableToBePrinted[4] = PyPx ;
        variableToBePrinted[5] = P0yP0x;

        showVariableHTMLLine (variableToBePrinted, numberVariables);
    }
};

void    shock::makeHTMLTableTail(int * ioData) {
	compressibleFlow::makeHTMLTableTail(ioData);		
};
