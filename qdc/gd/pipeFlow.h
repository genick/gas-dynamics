// last change Mon Feb  4 09:46:51 CST 2013 unchock new algarithem for given P2/P1 and fld
//////////////////////////////////////////////////////////////////////////////////////////
/********************************************************
	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/
#ifndef _PIPEflow_STDLIB
#define _PIPEflow_STDLIB

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip> 
#  include <limits>
#  include "definition.h" 
#  include "compressibleFlow.h" 
#  include "shock.h"

class pipeFlow:public compressibleFlow {
	protected:
	public:
		double Mmax, Mout,  Min, Mbar, Msmall ;
		// to change the initialization later on
		// so that base class initialization will be considered
		//pipeFlow(double Mach, double spesificHeatRatio ):
		pipeFlow(double spesificHeatRatio , FlowName dcn ):
		//inilazation of base class
		//compressibleFlow(Mach,spesificHeatRatio)
		compressibleFlow(spesificHeatRatio, dcn){ 
			isPipeFlow = yes;
			// specific initialization process for derived class
			//isChoked = no; 
		};
		~pipeFlow(){};
		//spesific functions of this model of flow to calculate
		//spesific parameters of this flow model.
		int  doAllLines ( int *,  double *); // making lines for range option
		virtual void calMach( inputType, double * ) ;
		void calM1( int, double * ) ;
		double getTemperature() {return Tbar; };
		virtual double calMForFLD ( double, double );
		virtual int  calShockLocationForFLD ( ) {return yes;};
		virtual void setChoked(int isChked )  {isChoked = isChked;};
		virtual double calP2P1(double , double );
		int calM1forFLDp2p1(double,double);// find M1 for given fld, p2p1 using the new techanique
		int calM1forFLDp2p1(double,double,double);// find M1 for given fld, p2, and p1 using the new techanique
		//functions that will not touch the class variables
		// specific functions to fanno flow to be add in presenting
		// the results
		//void showHead(int );
		//void showResults(int);
		//void makeTexTableHead(int ); 
		//void makeTexTableLine (int );
};

#endif  /* _PIPEflow_STDLIB */
