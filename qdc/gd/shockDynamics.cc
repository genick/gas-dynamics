/********************************************************
 * last change Mon Dec 19 12:01:50 CST 2011 cast conversion from string
 * constant to ‘char*’
**********************************************************************
	FILE:shockDynamics.cc

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shockDynamics wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shockDynamics wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip>
#  include <limits>
#  include "stagnation.h"
#  include "externalFun.h"
#  include "shockDynamics.h" 
#  define _USE_MATH_DEFINES
using namespace std;

/// shockDynamics class definition 
//******************************************************************

void    shockDynamics::showResults(int * ioData) {
    int numberVariables;
    if (outputInfo == infoStandard ) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx ;
        variableToBePrinted[1] = Mxp;
        variableToBePrinted[2] = My;
        variableToBePrinted[3] = Myp;
        variableToBePrinted[4] = TyTx;
        variableToBePrinted[5] = PyPx ;
        variableToBePrinted[6] = P0yP0x;

        showVariable(variableToBePrinted, numberVariables);
		return;
    }
    else if (outputInfo == infoMax ) {
        numberVariables = 9;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx ;
        variableToBePrinted[1] = Mxp;
        variableToBePrinted[2] = My;
        variableToBePrinted[3] = Myp;
        variableToBePrinted[4] = TyTx;
        variableToBePrinted[5] = PyPx ;
        variableToBePrinted[6] = P0yP0x;
        variableToBePrinted[7] = Uyp;
        variableToBePrinted[8] = cx;

		showVariable(variableToBePrinted, numberVariables);
		return;
    }
    else  if (outputInfo == infoMinimal ) {
		        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx ;
        variableToBePrinted[1] = 0.;
        variableToBePrinted[2] = theta[0]*180./PI ;
        variableToBePrinted[3] = delta*180./PI;
        variableToBePrinted[4] = PyPx;
        variableToBePrinted[5] = TyTx;
        variableToBePrinted[6] = P0yP0x;
                                                                                
        showVariable(variableToBePrinted, numberVariables);
        return;
	}
	else  if (outputInfo == infoIteration ) {
        numberVariables = 5;
        double variableToBePrinted[numberVariables];
		for (int i = 0 ; i < numberOfIterations ; i++){
			variableToBePrinted[0] = steps[i][1];
			variableToBePrinted[1] = steps[i][2];
			variableToBePrinted[2] = steps[i][3];
			variableToBePrinted[3] = steps[i][4];
			variableToBePrinted[4] = Myp;

			cout  << "	" << (int)  steps[i][0] << "  ";
			showVariable(variableToBePrinted, numberVariables);
		}
        return;
    }
	else  {
		return ;
	}
};

void    shockDynamics::showHead(int* ioData){
    cout << endl << endl <<  " Shock Dynamics" << "\t\t"  ;
    cout << "k = " << k << endl << endl ;
                                                                               
    if (outputInfo == infoStandard ) {
        int variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "Mx ";
        variableNames[1] = (char *) "Mxp";
        variableNames[2] = (char *) "My";
        variableNames[3] = (char *) "Myp";
        variableNames[4] = (char *) "TyTx";
        variableNames[5] = (char *) "PyPx";
        variableNames[6] = (char *) "P0yP0x";

        showVariableHead( variableNames, variableNumber);
    }
    else if (outputInfo == infoMax ) {
        int variableNumber = 9;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "Mx ";
        variableNames[1] = (char *) "Mxp";
        variableNames[2] = (char *) "My";
        variableNames[3] = (char *) "Myp";
        variableNames[4] = (char *) "TyTx";
        variableNames[5] = (char *) "PyPx";
        variableNames[6] = (char *) "P0yP0x";
        variableNames[7] = (char *) "Uyp";
        variableNames[8] = (char *) "c_x";

        showVariableHead( variableNames, variableNumber);
    }
    else if (outputInfo == infoMinimal ) {
        int variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "Mx  ";
        variableNames[1] = (char *) "My_w";
        variableNames[2] = (char *) "Theta_w";
        variableNames[3] = (char *) "delta";
        variableNames[4] = (char *) "Pbar" ; 
        variableNames[5] = (char *)  "Tbar" ; 
        variableNames[6] = (char *) "P0yP0x"; 
                                                                               
        showVariableHead( variableNames, variableNumber);
    }
    else  if (outputInfo == infoIteration ) {
        int variableNumber = 6;
        char * variableNames[variableNumber];
        cout << "The values at zero delta are:" << endl;
        variableNames[0] = (char *) "i    ";
        variableNames[1] = (char *) "Mx";
        variableNames[2] = (char *) "My";
        variableNames[3] = (char *) "TyTx";
        variableNames[4] = (char *) "PyPx";
        variableNames[5] = (char *) "Myp";

        showVariableHead( variableNames, variableNumber);
    }

};

void  shockDynamics::calMach(inputType variableName, double * variableValue){
    if (variableName == MxV) {
        Mx = variableValue[MxV];
    }
	else if (variableName == deltaThetaV) {
		M = 2. *(1.0/tan(theta[0]) + tan(delta))/
			(sin (2.* theta[0]) - tan(delta)*
					(k + cos (2.*theta[0]))) ;
		if (M > 0.) {
			M = sqrt(M);
			Mx = M *sin(theta[0]);
		}
		else {
		//no solution 
			
		}
	}
};

void  shockDynamics::calMx(inputType variableName, double * variableValue){
	double tmpMx;
    if (variableName == P0yP0xV) {
        P0yP0x = variableValue[P0yP0xV];
			//change due the new root finding brent methods May 25, 2012
        //M = root  ( calP0yP0xEstmt, this, M1maxSub, 100.0, P0yP0x) ;
		M = brent  ( calP0yP0xEstmt, this, M1maxSub, 100.0, P0yP0x) ;
		Mx = M * sin (theta[0]);
		return;
    }
	else if (variableName == MxV){ 
		Mx = variableValue[MxV] ;
		calMy();
		if ( Mx < 1.0) {
			tmpMx = Mx ;
			Mx = My;
			My = tmpMx;
		}
	}
    else if (variableName == machV){
        Mx = variableValue[machV] ;
        calMy();
        if ( Mx < 1.0) {
            tmpMx = Mx ;
            Mx = My;
            My = tmpMx;
        }
    }
}

void  shockDynamics::calMy(){
    My = sqrt((Mx*Mx + 2.0/(k -1.0)) /
        ((2.0*k / (k-1.0) ) *Mx*Mx -1.0)) ;
};

double  shockDynamics::calMy(double m){
    return  sqrt((m*m + 2.0/(k -1.0)) / ((2.0*k / (k-1.0) ) *m*m -1.)) ;
};

inputType shockDynamics::selectVariable (inputType variableName) {
    if  (variableName == deltaMV  ) { return deltaMV ; }  
    else if  (variableName == MxV  ) { return MxV ; }  
    else if  (variableName == MxpV  ) { return MxpV ; }  
    else if  (variableName == MxOpenV  ) { return MxOpenV ; }  
    else if  (variableName == MyOpenV  ) { return MyOpenV ; }  
    else if  (variableName == MypOpenV  ) { return MypOpenV ; }  
    else if  (variableName == MxpMypPOV  ) { return MypV ; }  
	else { return _noV;};

} ;

void    shockDynamics::makeTexTableHead(int * ioData) {
                                                                               
    int variableNumber;
                                                                               
    if (outputInfo == infoStandard ) {
        variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "M_x";
        variableNames[1] = (char *) "M_y";
        variableNames[2] = (char *) "{M_x}^{'}";
        variableNames[3] = (char *) "{M_y}^{'}";
        variableNames[4] = (char *) "{T_y} \\over {T_x}";
        variableNames[5] = (char *) "{P_y} \\over {P_x}";
        variableNames[6] = (char *) "{P_0}_y \\over {P_0}_x ";
        //variableNames[6] = "{\\rho}_y \\over {\\rho}_x ";
        showVariableTeXHead(variableNames,variableNumber);
		return ;
    }
    else if (outputInfo == infoMax  ) {
        variableNumber = 9;
		char * variableNames[variableNumber];
        variableNames[0] = (char *) "M_x";
        variableNames[1] = (char *) "M_y";
        variableNames[2] = (char *) "{M_x}^{'}";
        variableNames[3] = (char *) "{M_y}^{'}";
        variableNames[4] = (char *) "{T_y} \\over {T_x}";
        variableNames[5] = (char *) "{P_y} \\over {P_x}";
        variableNames[6] = (char *) "{P_0}_y \\over {P_0}_x ";
        variableNames[7] = (char *) "{U_y}^{'} ";
        variableNames[8] = (char *) "{c_x}";

		showVariableTeXHead(variableNames,variableNumber);
		return;
    }
    else if (outputInfo == infoMinimal ) {
        variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "M_x";
        variableNames[1] = (char *) "{M_y}_w";
        variableNames[2] = (char *) "\\theta_w";
        variableNames[3] = (char *) "\\delta ";
        variableNames[4] = (char *) "{P_y} \\over {P_x}  ";
        variableNames[5] = (char *) "{T_y} \\over {T_x}  ";
		  variableNames[6] =   (char *) "{P_0}_y \\over {P_0}_x  ";

        showVariableTeXHead(variableNames,variableNumber);
		return ;
    }
    else  if (outputInfo == infoObliqueMax ) {
        int variableNumber = 4;
        char * variableNames[variableNumber];
        cout << "The maximum values are:" << endl;
        variableNames[0] = (char *) "M_x";
        variableNames[1] = (char *) "M_y";
        variableNames[2] = (char *) "\\delta_{max}";
        variableNames[3] = (char *) "\\theta_{max}";

        showVariableTeXHead ( variableNames, variableNumber);
    }
    else  if (outputInfo == infoIteration ) {
        int variableNumber = 6;
        char * variableNames[variableNumber];
        cout << "The maximum values are:" << endl;
        variableNames[0] = (char *) "i";
        variableNames[1] = (char *) "M_x";
        variableNames[2] = (char *) "M_y";
        variableNames[3] = (char *) "{T_y} \\over {T_x}  ";
        variableNames[4] = (char *) "{P_y} \\over {P_x}  ";
        variableNames[5] = (char *) "{M_y}^{'}  ";

        showVariableTeXHead ( variableNames, variableNumber);
    }
}

void    shockDynamics::makeTexTableLine(int * ioData) {
    int numberVariables;

    if (outputInfo == infoStandard ) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx ;
        variableToBePrinted[1] = My;
        variableToBePrinted[2] = Mxp;
        variableToBePrinted[3] = Myp;
        variableToBePrinted[4] = TyTx;
        variableToBePrinted[5] = PyPx ;
        variableToBePrinted[6] = P0yP0x;

        showVariableTeXLine (variableToBePrinted, numberVariables);
    }
    else if (outputInfo == infoMax   ) {
        numberVariables = 9;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx ;
        variableToBePrinted[1] = My;
        variableToBePrinted[2] = Mxp;
        variableToBePrinted[3] = Myp;
        variableToBePrinted[4] = TyTx;
        variableToBePrinted[5] = PyPx ;
        variableToBePrinted[6] = P0yP0x;
        variableToBePrinted[7] = Uyp;
        variableToBePrinted[8] = cx;

       	showVariableTeXLine ( variableToBePrinted, numberVariables);
		return;
    }
    else if (outputInfo == infoMinimal ) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx ;
        variableToBePrinted[1] = 0.;
        variableToBePrinted[2] = theta[0]*180./PI ;
        variableToBePrinted[3] = delta*180./PI;
        variableToBePrinted[4] = PyPx;
        variableToBePrinted[5] = TyTx;
        variableToBePrinted[6] = P0yP0x;
                                                                                
        showVariableTeXLine (variableToBePrinted, numberVariables);
    }
	else  if (outputInfo == infoIteration ) {
        numberVariables = 5;
        double variableToBePrinted[numberVariables];
		for (int i = 0 ; i < numberOfIterations ; i++){
			variableToBePrinted[0] = steps[i][1];
			variableToBePrinted[1] = steps[i][2];
			variableToBePrinted[2] = steps[i][3];
			variableToBePrinted[3] = steps[i][4];
			variableToBePrinted[4] = Myp;

			cout << (int) steps[i][0] << "&";

        	showVariableTeXLine ( variableToBePrinted, numberVariables);
		}
    }

}

void shockDynamics::calMx(void) { // for given Mxp
	int i ;
	double  MxOld, deltaMx  ;
	//cout << "calMx" << endl ;
	//successive  repitition
	//first iteretion
	Mx = MxOld = 1.0 +Mxp;
	deltaMx = 1.0;
	i = 0;
	while (deltaMx > EPS) { 
		calMy();
		calPressureRatio();
		calTemperatureRatio();
		if (isR == no ) {
			steps[i][0] = i; 
			steps[i][1] = Mx; 
			steps[i][2] = My; 
			steps[i][3] = TyTx; 
			steps[i][4] = PyPx; 
			steps[i][5] = Myp; 
		}
		Mx =  sqrt(TyTx)*( My) + Mxp  ;
		deltaMx = abs (Mx - MxOld );
		MxOld = Mx;	
//cout<<"number of iteration =" << i << " Mx" << steps[i][0] << endl ;
		i++;
	}
	numberOfIterations = i;
}

int	shockDynamics::calAll( double * variableValue ){
	double   Mxtmp;
	if  (inputInfo == MxpV) {
		shockDynamicsName = "Close valve";
		Msx = 0.0;
		Mxp =  variableValue[MxpV] ;
		calMx();
		Myp = 0.;
		// the second iteration method not used any more
        //lowerLimit = 1.0;
        //Mx = root ( calPxpEstmt, this, 1.-Mxp +EPS, 100., Mxp); //actualy Mxp
        //Mx += Mxp ;
		//Mx1 = Mx;
        //calMy();
		
		//successive  repitition
		//first iteretion
	}
	else if (inputInfo == MxV) {
		shockDynamicsName = "Close valve";
		Mxtmp = variableValue[MxV] ;
		Mxp = root ( calPxpEstmt, this, 1.-Mxtmp +EPS, 100., Mxtmp);
		Mx = Mxtmp ;
		calMy();
		Myp = 0.;
	}
	else if (inputInfo == UxpV) {
		shockDynamicsName = "Close valve";
		Uxp = variableValue[UxpV] ;
		Tx = variableValue[TxV] ;
		Mxp = Uxp / sqrt(k * 287.* Tx);
		Myp = 0.;
		//Mx = Mxp + 1.0;
		calMx();
		//TyTx = variableValue[TyTxV ] ;
        //lowerLimit = 1.0;
 //Mx = root ( calPxpEstmt, this, 1.-Mxp +EPS, 100., Mxp); //actualy Mxp
        //Mx += Mxp ;
        //calMy();
	}
	else if (inputInfo == MyV) {
		shockDynamicsName = "Close valve";
		Mx =  variableValue[MyV] ;
		calMy();
		double tmpMy;
		tmpMy = My;
		My = Mx;
		Mx = tmpMy;
		calPressureRatio();
		calTemperatureRatio();
		Mxp = Mx - sqrt(TyTx) * My ;
		Myp = 0.;
	}
	else if (inputInfo == MxOpenV ) {
		shockDynamicsName = "Open valve";
		Mx =  variableValue[MxV] ;
		calMy();
		calPressureRatio();
		calTemperatureRatio();
		Myp = My * (sqrt(TyTx) - 1.);  	
		Mxp = 0.0; 
	}
	else if (inputInfo == MyOpenV ) {
		shockDynamicsName = "Open valve";
		My = variableValue[MyOpenV] ;
		discontinuity::calMx();
		calPressureRatio();
		calTemperatureRatio();
		Myp = sqrt(1./TyTx) *Mx - My ;
		Mxp = 0.0; 
	}
	else if (inputInfo == PyPxOpenV ) {
		double tmpPbar;
		shockDynamicsName = "Open valve";
		tmpPbar = PyPx = variableValue[PyPxV] ;
		Mx = root  ( calPbarEstmt, this,  M1maxSub, 50., PyPx) ;
		discontinuity::calMy();
		calTemperatureRatio();
		Myp = sqrt(1./TyTx) *Mx - My ;
		Mxp = 0.0; 
	}
	else if (inputInfo == UsOpenV ) {
		shockDynamicsName = "Open valve";
		Us =  variableValue[UsV] ;
		Tx = variableValue[TxV] ;
		Mx = Us / sqrt(k * 287.* Tx);
		if ( Mx < (1.0 + EPS )) { //no shock possible
			cout << "Mx = " << Mx << " and it is too small."
				<< endl ;
			return no;
		}
		discontinuity::calMy();
		calPressureRatio();
		calTemperatureRatio();
		Msy = Us /  sqrt(k * 287.* Tx * TyTx);
		Myp = Msy - My;
		Mxp = 0.0; 
	}
	else if (inputInfo == UypTxOV ) {
		shockDynamicsName = "Open valve";
		Uyp =  variableValue[UypV] ;
		Tx = variableValue[TxV] ;;
		cx = sqrt( k*287*Tx);
		Msx =  ((Uyp*(k + 1.)) + 
			sqrt(((Uyp*Uyp)*((k*(k + 2.)) + 1.)) + (16.*(cx*cx)))) 
			/ (4.*cx) ;
		Us = Msx * cx ;
		Mx = Msx;
		Mxp = 0.;
		calMy () ;
		calPressureRatio();
		calTemperatureRatio();
		cy = sqrt( k*287*Tx*TyTx);
		Myp = Msx - Uyp/cy;
	}
	else if (inputInfo == MypOpenV ) {
		shockDynamicsName = "Open valve";
		Myp =  variableValue[MypV] ;
		if ( Myp >= sqrt ( 2. /k / (k -1.)) ) {
			cout << "no solution is possible for this velocity " 
			<< Myp << "." << endl <<  "The maximum velocity is "
			<< sqrt ( 2. /k / (k -1.)) << endl ; 
			return no;
		} 
		double MxOld, deltaMx;
		//My = sqrt(TyTx)*Myp;
		//first iteretion
		Mx = MxOld = 1.0;
		deltaMx = 1.0;
		int i ;
		i = 0;
		while (deltaMx > EPS) { 
			calMy();
			calPressureRatio();
			calTemperatureRatio();
			if (isR == no ) {
				steps[i][0] = i; 
				steps[i][1] = Mx; 
				steps[i][2] = My; 
				steps[i][3] = TyTx; 
				steps[i][4] = PyPx; 
			}
			Mx =  sqrt(TyTx)*( Myp  + My)  ;
			deltaMx = abs (Mx - MxOld );
			MxOld = Mx;	
//cout<<"number of iteration =" << i << " Mx" << steps[i][0] << endl ;
			i++;
		}
		Mxp = 0.;
		numberOfIterations = i;
	}
	else if (inputInfo == MxGV ) {
		shockDynamicsName = "Open valve"; // is it? to check
		Mx = variableValue[MxV] ;
		Myp = variableValue[MypV] ;
		calMy();
		if (My < Myp) {
			cout << "no solution possible" << endl ; 
			cout << "too fast Myp " << endl ; 
			return no;	
		}
		calPressureRatio();
		calTemperatureRatio();
		Msy = My - Myp;
		Msx = sqrt(TyTx)* Msy;
		Mxp = Mx - Msx;
	}
	else if (inputInfo == MxpMypPOV ) {
		shockDynamicsName = "Partially Open valve"; 
		Myp = variableValue[MypV] ;
		Mxp = variableValue[MxpV] ;
		double MxOld, deltaMx;
		//first iteretion
		Mx = MxOld = deltaMx = Mxp + 1.0;
		int i ;
		i = 0;
		while (deltaMx > EPS) { 
			calMy();
			calPressureRatio();
			calTemperatureRatio();
			Msx = Mx + Mxp;
			Msy = My + Myp;
			if (isR == no ) {
				steps[i][0] = i; 
				steps[i][1] = Mx; 
				steps[i][2] = My; 
				steps[i][3] = TyTx; 
				steps[i][4] = PyPx; 
				steps[i][5] = Msx; 
				steps[i][6] = Msy; 
			}
			Mx =  sqrt(TyTx)* Msy - Mxp   ;
			deltaMx = abs (Mx - MxOld );
			MxOld = Mx;	
//cout<<"number of iteration =" << i << " Mx" << steps[i][0] << endl ;
			i++;
		}
		numberOfIterations = i;
	}
	else if (inputInfo == MxpMyPOV ) {
		shockDynamicsName = "Partially Open valve"; 
		My = variableValue[MyV] ;
		Mxp = variableValue[MxpV] ;
		discontinuity::calMx(); //Mx
		Msx = Mx + Mxp;
		calPressureRatio();
		calTemperatureRatio();
		Msy = Msx / sqrt(TyTx) ;  
		Myp = Msy - My;
	}
	else if (inputInfo == MxMypPCV ) {
		double Ms;
		shockDynamicsName = "Partially Close valve"; 
		Myp = variableValue[MypV] ;
		Mx = variableValue[MxV] ;
		calMy();
		Ms = My - Myp;
		Mxp = Mx - Ms ;
	}
	else if (inputInfo == MxpMypPCV ) {
		shockDynamicsName = "Partially Close valve"; 
		Myp = variableValue[MypV] ;
		Mxp = variableValue[MxpV] ;
		double MxOld, deltaMx;
		//first iteretion
		Mx = MxOld = deltaMx = Mxp + 1.0;
		int i ;
		i = 0;
		while (deltaMx > EPS) { 
			calMy();
			calPressureRatio();
			calTemperatureRatio();
			Msx = Mx - Mxp;
			Msy = My - Myp;
			if (isR == no ) {
				steps[i][0] = i; 
				steps[i][1] = Mx; 
				steps[i][2] = My; 
				steps[i][3] = TyTx; 
				steps[i][4] = PyPx; 
				steps[i][5] = Msx; 
				steps[i][6] = Msy; 
			}
			Mx =  sqrt(TyTx)* Msy + Mxp   ;
			deltaMx = abs (Mx - MxOld );
			MxOld = Mx;	
			i++ ;
		}
		numberOfIterations = i;
	}
	else if (inputInfo == UpTyV ) {
		double a, b ;
//		double Usx, Usy;
		Uyp =  variableValue[UpV] ;
		setUyp(variableValue[UpV]);
		Tx =  variableValue[TxV] ;
		Uxp = variableValue[UxpV];
//		cx = sqrt ( k * R.value*Tx) + EPS ;
//		Ux = cx;
//		cout << "ux" << "	" << "Usx" << "	" << "Usy" << endl ;
//		for (int jj = 0 ; jj < 100 ; jj++){
//			Ux =  cx + (double) jj*2.  ; 
//			Mx = Ux/cx;
//			calMy () ;
//			calPressureRatio();
//			calTemperatureRatio();
//			calDensityRatio() ;
//			Usx = Uxp + Ux ; // x side
//			Usy = Ux/getUxUy() + Uyp ; // y side
//			cout << Ux << "	" <<  Usx << "	" << Usy << endl ;
//		}	
		a = sqrt ( k * R.value*Tx) + EPS ;
		b = a + Uyp ;
		Ux = brent ( calFU, this,  a, b, 0.0) ;
		Mx = Ux/cx;
		Mxp = Uxp /cx ;
		cy = cx * TyTx;
		Myp = Uyp /cy ;
	}
	else if (inputInfo == kV ) {
		k = 1.2;
		int i;
		i = 0;
		while (k < 2.5) {
			k +=  0.1 ;
			My = sqrt(0.5* (k -1.) / k)+EPS ;
			discontinuity::calMx();
			calPressureRatio();
			calTemperatureRatio();
			Myp = sqrt(1./TyTx) *Mx - My ;
			steps[i][0] = k; 
			steps[i][1] = Mx; 
			steps[i][2] = My; 
			steps[i][3] = Myp; 
			steps[i][4] = TyTx; 
			steps[i][5] = PyPx; 
			i++;
		}
		numberOfIterations = i;
	}
	//calAreaRatio();
	if (inputInfo != PyPxV){ 
		calPressureRatio();
	}
	if (inputInfo != TbarV){
		calTemperatureRatio();
	}
	calDensityRatio ();
	calTotalPressureRatio();
	return yes;
}

int shockDynamics::fU (double Uxdummy){
	double Ux, f; 
	Us = Uxp + Uxdummy ;
	Mx = Uxdummy/sqrt (k*R.value*Tx) ;
	calMy () ; 
	calDensityRatio() ;
	Ux = 1.0;
	f = Uxp + Ux - UxUy*Ux - Uyp ; 	
	return yes;
}

double shockDynamics::calUs (double Uxdummy) {
	return Uxp + Uxdummy;
} ;

int shockDynamics:: calTableLine (  inputType variableName,
            double * variableValue){
	if (outputInfo == infoStandard || outputInfo == infoMinimal ){
		return calAll( variableValue);
	}
	else if (outputInfo == infoMax ) {
		return calAll( variableValue);
	}
	else if (outputInfo == infoObliqueMax ) {
		return yes;
	}
    else if (outputInfo == infoObliqueMin ) {
        //calMinDelta(variableName, variableValue );
        return yes;
    }
    else if (outputInfo == infoIteration ) {
		return calAll( variableValue);
    }
    return no;
};

int   shockDynamics::doAllLines ( int * ioData, double * variableValue ){
	inputType variableName = (inputType) ioData[variableNameN];
	int howManyPoints,  howManySets ; 
	double startP, endP, rangeD;
	double range [61];
	if (outputInfo == infoStandard ){
		if (variableName == MxOpenV){
			startP = 1.01;
			endP = 1.1;
		}
		else if (variableName == MyOpenV){
			startP =  sqrt(0.5*(k-1.)/k+EPS);
			endP  = double(int (startP *10.) + 1 )/10.;
		}
		else{ 
			startP = 0.5*(k-1.)/k+EPS;
			//upper boundry 
			endP  = double(int (startP *10.) + 1 )/10.;
		}
		if ( variableName == MyOpenV ){  
			range[1] =  9.0; // howManyPoints;
			range[2] = startP ; 
			range[3] = endP ; 
			int ii= 0;
			for (int jj = int (10*endP) ; jj < 10 ; jj++){
				range[3*(1+ii)+1] = 9. ; 
				range[3*(1+ii)+2] = double(jj/10.) + 0.01 ; 
				range[3*(1+ii)+3] = double(jj/10.) + 0.1 ; 
				ii++;
			}
			range[0] = double (ii+1);
		}
		else if (variableName == MypOpenV ){
			howManySets = 6;
			range[0] = (double) howManySets;
			range[1] =  9.0; // howManyPoints;
			range[2] = 0.01 ; 
			range[3] = .10 ; 
			range[4] = 8. ; 
			range[5] = range[3] + 0.1 ; 
			range[6] = 1.0 ; 
			range[7] = 7. ; 
			range[8] = range[6] + .1 ; 
			range[9] = 1.8 ; 
			range[10] = 7. ; 
			range[11] = 1.81; // sqrt(2.*k/(k-1.)) - EPS ; 
			range[12] = 1.88 ;// sqrt(2.*k/(k-1.)) - EPS ; 
			range[13] = 9. ; 
			range[14] = 1.88; // sqrt(2.*k/(k-1.)) - EPS ; 
			range[15] = 1.89 ;// sqrt(2.*k/(k-1.)) - EPS ; 
			range[13] = 9. ; 
			range[14] = 1.8881; // sqrt(2.*k/(k-1.)) - EPS ; 
			range[15] = 1.8889 ;// sqrt(2.*k/(k-1.)) - EPS ; 
		} 
		else if (variableName == MxpV ){
			howManySets = 4;
			range[0] = (double) howManySets;
			range[1] =  9.0; // howManyPoints;
			range[2] = 0.01 ; 
			range[3] = .10 ; 
			range[4] = 8. ; 
			range[5] = range[3] + 0.1 ; 
			range[6] = range[3] + 1.0 ; 
			range[7] = 8. ; 
			range[8] = range[6] + .1 ; 
			range[9] = range [6] + 0.9 ; 
			range[10] = 8. ; 
			range[11] = range[9]+0.1; // sqrt(2.*k/(k-1.)) - EPS ; 
			range[12] = range [9] + 0.9 ;// sqrt(2.*k/(k-1.)) - EPS ; 
			range[13] = 9. ; 
			range[14] = 1.88; // sqrt(2.*k/(k-1.)) - EPS ; 
			range[15] = 1.89 ;// sqrt(2.*k/(k-1.)) - EPS ; 
			range[13] = 8. ; 
			range[14] = 2.0; // sqrt(2.*k/(k-1.)) - EPS ; 
			range[15] = 10. ;// sqrt(2.*k/(k-1.)) - EPS ; 
		} 
		else if (variableName == MxpMyPOV ){
		}
		else if (variableName == MxpMypPOV ){
			if  ( variableValue[MxpV] < 0.9 ) {
				howManySets = 4;
				range[0] = (double) howManySets;
				range[1] =  9.0; // howManyPoints;
				range[2] = variableValue[MxpV]  ; 
				range[3] = 1.0 ; 
				range[4] = 7. ; 
				range[5] = range[3] + 0.1 ; 
				range[6] = range[3]+ 0.8 ; 
			}
			else { //for the supersonic branch
				howManySets = 4;
				range[0] = (double) howManySets;
				range[1] =  9.0; // howManyPoints;
				range[2] = variableValue[MxpV]  ; 
				range[3] =  variableValue[MxpV] + 0.1 ; 
				range[4] = 9. ; 
				range[5] = range[3] + 0.1 ; 
				range[6] = 1.9 ; 
			}
			range[7] = 20. ; 
			range[8] = range[6] + .01 ; 
			range[9] = range[6]+ .1 ; 
			range[10] = 9. ; 
			range[11] = range[9]+0.001; // sqrt(2.*k/(k-1.)) - EPS ; 
			range[12] = range[9]+0.01 ;// sqrt(2.*k/(k-1.)) - EPS ; 
			range[13] = 9. ; 
			range[14] = range[12]+ 0.1; // sqrt(2.*k/(k-1.)) - EPS ; 
			range[15] = range[12]+ 0.9 ;// sqrt(2.*k/(k-1.)) - EPS ; 
			range[13] = 10. ; 
			range[14] = range[15]+ 0.1; // sqrt(2.*k/(k-1.)) - EPS ; 
			range[15] = range[15]+ 0.2 ;// sqrt(2.*k/(k-1.)) - EPS ; 
		} 
		//else {
		//	return no;
		//}

		setRange( ioData, range, variableValue);

		howManyPoints = 9 ;
		rangeD = 0.1;
		startP = endP + rangeD;
		endP = 2.0;

		range[0] = (double) howManyPoints;
		range[2] = startP ; 
		range[3] = endP ; 
		//setRange( variableName, isTex, range, variableValue);
		//makeLines(isTex,howManyPoints, startP,  endP,
								 //variableName, variableValue);
		howManyPoints = 7 ;
		rangeD = 1.0 ;
		startP = endP + rangeD;
		endP = 10.;
		//makeLines(isTex,howManyPoints, startP,  endP,
		//						 variableName, variableValue);
	}
    else if (outputInfo == infoD0) {
        howManyPoints = 9 ;
         makeLines(ioData,howManyPoints, 1.1,  2.0, variableValue);
        howManyPoints = 9 ;
         makeLines(ioData,howManyPoints, 2.2,  4.0,  variableValue);
        howManyPoints = 5 ;
         makeLines(ioData,howManyPoints, 5.0,  10.0,  variableValue);
    }

    return yes;
}


void    shockDynamics::makeHTMLTableHead(int * ioData){
    int variableNumber;
                                                                               
    if (outputInfo == infoStandard || outputInfo == infoMinimal) {
        variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "Mx ";
        variableNames[1] = (char *) "Mx&prime;";
        variableNames[2] = (char *) "My";
        variableNames[3] = (char *) "My&prime;";
        variableNames[4] = (char *) "Ty/Tx";
        variableNames[5] = (char *) "Py/Px";
        variableNames[6] = (char *) "P0y/P0x";
                                                                               
        showVariableHTMLHead(variableNames,variableNumber);
    }
    else  if (outputInfo == infoIteration ) {
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "i    ";
        variableNames[1] = (char *) "Mx";
        variableNames[2] = (char *) "My";
        variableNames[3] = (char *) "TyTx";
        variableNames[4] = (char *) "PyPx";
        variableNames[5] = (char *) "Myp";
        showVariableHTMLHead( variableNames, variableNumber);
    }
};

void    shockDynamics::makeHTMLTableLine(int * ioData) {
    int numberVariables;

    if (outputInfo == infoStandard || outputInfo == infoMinimal) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx ;
        variableToBePrinted[1] = Mxp;
        variableToBePrinted[2] = My;
        variableToBePrinted[3] = Myp;
        variableToBePrinted[4] = TyTx;
        variableToBePrinted[5] = PyPx ;
        variableToBePrinted[6] = P0yP0x;

        showVariableHTMLLine (variableToBePrinted, numberVariables);
    }
	else  if (outputInfo == infoIteration ) {
		numberVariables = 6;
		double variableToBePrinted[numberVariables];
		for (int i = 0 ; i < numberOfIterations ; i++){
			variableToBePrinted[0] = i;
			variableToBePrinted[1] = steps[i][1];
			variableToBePrinted[2] = steps[i][2];
			variableToBePrinted[3] = steps[i][3];
			variableToBePrinted[4] = steps[i][4];
			variableToBePrinted[5] = Myp;

			showVariableHTMLLine(variableToBePrinted, numberVariables);
		}
        return;
    }
};

void shockDynamics :: showVariableHTMLHead (char ** variableNames,
					 int numberVariables ) {
	cout << endl << endl;
	//cout << "this html code for table" << endl ;
	cout << "<tr class=\"text\"><td align=\"center\">" << endl ;
	cout << "<table border=1 width=\"100%\" >" << endl ;
	cout << "  <thead>" << endl;
	cout << "    <tr>" << endl ;
	cout << "      <th align=left bgcolor=\"#fffeaa\" colspan=";
	cout << 3 << " >";
	cout <<  flowModelName ;
	cout << " </th>" << endl;
	cout << "      <th align=left  bgcolor=\"#00ff5a\" colspan=";
	cout << numberVariables-4 << "  rowspan=2 >"; 
	cout << "Input: ";
	cout << inputVariableName ;
	cout << " </th>" << endl;
	cout << "      <th align=left  bgcolor=\"#9ae0ee\" colspan=";
	cout << 1 << " rowspan=2 >";  
	cout << "k = ";
	cout << k ;
	cout << " </th>" << endl;
	cout << "    </tr>" << endl ;
	cout << "    <tr>" << endl ;
	cout << "      <th align=left bgcolor=\"#fffeaa\" colspan="; 
	cout << 3 << " >";  
	cout <<  shockDynamicsName ;
	cout << " </th>" << endl;
	cout << "    </tr>" << endl ;
	cout << "    <tr>" << endl ;
	for (int i=0; i < numberVariables ; i++){
		cout << "      <th align=center >" ;
		cout <<  variableNames[i] << " </th>" << endl;
	}
	cout << "    </tr> " << endl;
	cout << "  </thead>" << endl;
	cout << "  <tbody>" << endl;
};
