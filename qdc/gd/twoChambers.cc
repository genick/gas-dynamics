//initial release Mon Mar 26 10:35:49 CDT 2012
//**********************************************************************
/********************************************************
	FILE:twoChambers.cc

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  twoChambers wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and twoChambers.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip>
#	include <limits>
#  include "stagnation.h"
#  include "externalFun.h"
#  include "twoChambers.h" 
#  include "gasDynamics.h" 
#	define _USE_MATH_DEFINES
using namespace std;

/// twoChambers class definition 
//******************************************************************

void    twoChambers::showResults(int * ioData) {
    int numberVariables;
    if (outputInfo == infoStandard ) {
		numberVariables = 9;
		double variableToBePrinted[numberVariables];

		for (int i = 0 ; i < iCyclesLimit ; i++){ 
			variableToBePrinted[0] 	= P[timeP][i] ;
			variableToBePrinted[1] 	= P[ch1P][i] ;
			variableToBePrinted[2] 	= P[ch2P][i] ;
			variableToBePrinted[3] 	= P[MbarP][i] ;
			variableToBePrinted[4] 	= P[UpistonP][i] ;
			variableToBePrinted[5] 	= P[V1P][i] ;
			variableToBePrinted[6] 	= P[mBar1P][i] ;
			variableToBePrinted[7] 	= P[mBar2P][i] ;
			variableToBePrinted[8] 	= P[mTotalP][i] ;

			showVariable(variableToBePrinted, numberVariables);
		}
		return;
    }
    else if (outputInfo == infoMax ) {
        numberVariables = 9;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx ;
        variableToBePrinted[1] = Mbar;
        variableToBePrinted[2] = My;
        variableToBePrinted[3] = Mbar;
        variableToBePrinted[4] = TyTx;
        variableToBePrinted[5] = PyPx ;
        variableToBePrinted[6] = P0yP0x;
        variableToBePrinted[7] = Uyp;
        variableToBePrinted[8] = cx;

		showVariable(variableToBePrinted, numberVariables);
		return;
	}
	else  if (outputInfo == infoTubeProfile ) {
		numberVariables = 6;
		double variableToBePrinted[numberVariables];
		for (int i = 0 ; i < 70 ; i++){ 
			variableToBePrinted[0] = Mfld[7][i] ;
			variableToBePrinted[1] = Mfld[2][i] ;
			variableToBePrinted[2] = Mfld[3][i]  ;
			variableToBePrinted[3] = Mfld[4][i] ;
			variableToBePrinted[4] = Mfld[5][i] ;
			variableToBePrinted[5] = Mfld[6][i] ;
			showVariable(variableToBePrinted, numberVariables);
		}
		return;
	}
	else  if (outputInfo == infoIteration ) {
        numberVariables = 5;
        double variableToBePrinted[numberVariables];
		for (int i = 0 ; i < numberOfIterations ; i++){
			variableToBePrinted[0] = steps[i][1];
			variableToBePrinted[1] = steps[i][2];
			variableToBePrinted[2] = steps[i][3];
			variableToBePrinted[3] = steps[i][4];
			variableToBePrinted[4] = Mbar;

			cout  << "	" << (int)  steps[i][0] << "  ";
			showVariable(variableToBePrinted, numberVariables);
		}
        return;
    }
	else  {
		return ;
	}
};

void	twoChambers::showHead(int* ioData){
    cout << endl << endl <<  " Shock Tube" << "\t\t"  ;
    cout << "Vr = ? " << k << "\tk5 = " << k5 << endl << endl ;
                                                                               
    if (outputInfo == infoStandard ) {
        int variableNumber = 9;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "time ";
        variableNames[1] = (char *) "P1";
        variableNames[2] = (char *) "P2";
        variableNames[3] = (char *) "Mbar";
        variableNames[4] = (char *) "Upiston";
        variableNames[5] = (char *) "V1";
        variableNames[6] = (char *) "m1Bar";
        variableNames[7] = (char *) "m2Bar";
        variableNames[8] = (char *) "mTotal";

        showVariableHead( variableNames, variableNumber);
    }
    else if (outputInfo == infoMax ) {
        int variableNumber = 9;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "Mx ";
        variableNames[1] = (char *) "Mxp";
        variableNames[2] = (char *) "My";
        variableNames[3] = (char *) "Myp";
        variableNames[4] = (char *) "TyTx";
        variableNames[5] = (char *) "PyPx";
        variableNames[6] = (char *) "P0yP0x";
        variableNames[7] = (char *) "Uyp";
        variableNames[8] = (char *) "c_x";

        showVariableHead( variableNames, variableNumber);
    }
    else  if (outputInfo == infoIteration ) {
        int variableNumber = 6;
        char * variableNames[variableNumber];
        cout << "The values at zero delta are:" << endl;
        variableNames[0] = (char *) "i    ";
        variableNames[1] = (char *) "Mx";
        variableNames[2] = (char *) "My";
        variableNames[3] = (char *) "TyTx";
        variableNames[4] = (char *) "PyPx";
        variableNames[5] = (char *) "Myp";

        showVariableHead( variableNames, variableNumber);
    }
};

double  twoChambers :: calMForFLD(double FLD, double mach){
    //what do when there is no solution ?
    if ((mach < singularPoint) && (FLD > EPS ) ) {// sub sonic flow
        //return  root ( calFLD_Estmt, this, .00001, (M1maxSub-EPS), FLD);
        double a = root ( calFLD_Estmt, this, .00001, (M1maxSub-EPS), FLD);
        return a;  
    }
    else if ((mach > singularPoint )  && (FLD > EPS ) ){//supersonic flow
        return  root( calFLD_Estmt, this, (M1maxSub+EPS), 10000., FLD );
    }
    else if (FLD == 0.0 )  {
        return 0.0;
    }
    else { // no possible negative fld
        return -1.0;
    }
};

double  twoChambers :: calP2P1(double M1, double M2){
	if (M2 > singularPoint){
		return -1.;
	}
	else if ( M1 >= M2) {
		return -1.;
	}
	else{
		//P1Pstar = calPfanno(M1);
		//P2Pstar = calPfanno(M2);
		P1Pstar = fanno::calPressureRatio(M1);
		P2Pstar = fanno::calPressureRatio(M2);
		return  P2Pstar/P1Pstar;
	}
}

void  twoChambers::calM(double P1, double P2){
		bool reverseFlow = false ;
		double fld_a, fld_b, Psmalla, Psmallb, Mb, Ma, Pa, Pb, Pright, Pleft; 
		if (P2 > P1){
			reverseFlow = true;
			double tmp ;
			tmp = P1; P1 = P2; P2 = tmp ;
		}
		fanno::calMForFLD(fld, 0.5) ;
		Mmax = M ; 
		P2P1 = P2/P1;
		if  (  P2 == P1  ) {
			M = Mout = Min = Mbar = 0. ; 
			goto outNow ;
		}
		else if  ( ( P1 - P2 ) < 0.02 ) {
			Msmall = sqrt((1.0 - P2/P1) /k / fld) ; 
			Mout = Min = Mbar = Msmall /Mmax ;
			fld_a = calFLD(Mbar);
			goto outNowA;
		}  
		else {
		// old stuff first guess deltaFLD f(P2P1)  choked flow
		// new stuff first calculate the M for a given fld
		double oldP1 = 1.0, P1n, newMi, oldPrCal, newPrCal,  oldP2=1.0;
		//double fld_a, fld_b, Psmalla, Psmallb, Mb, Ma, Pa, Pb, Pright, Pleft; 
		double Pn = 1., Pll = 1., Prr = 1., Mrr = 1., Mll = 1. ;
		//this the first on the right for the M, Msmall is on the left.
		Ma = newMi = calMForFLD(fld,0.5) ;
		Pright = P1n = fanno::calPressureRatio( newMi ); // also limit pressure above which flow is chocked
		oldP2 = 1.0 ;
		Pleft = 1.0 ; // to change when P2 changes
		if ( 1.0 / P2P1 > Pright ) { // the flow is chocked
			Mbar = 1. ;
			goto outNow ;
		}
		oldPrCal = P1n / oldP1 ; // the pressure ratio between the
		// check the lefthand side. what is the pressure for that M according the full equations.
		fld_a = fanno::calFLD(Msmall);
		fld_b = fld_a + fld ;
		Ma = Msmall ;
		Mb = calMForFLD (fld_b, 0.2) ;
		Pa = Psmalla = fanno::calPressureRatio( Ma );
		Pb = Psmallb =  fanno::calPressureRatio (Mb) ;
		Pll = newPrCal = Pb / Pa - 1./P2P1 ;
		Pn = 1.0/P2P1;
		Prr = Pright - 1./P2P1;
		Mll = Msmall ;
		Mrr = Mmax ;
		// guess new value of M			
		double Mc = 0.;
		double Md = numeric_limits<double>::max( ) ;

    //double fa = function(a);
    //double fb = function(b);

		double Pc = 0.;
		double Ms = 0.;
		double Ps = 0.;
			// if |f(a)| < |f(b)| then swap (a,b) end if
	if (abs(Pll) < abs(Prr))
		{ double tmp = Mll; Mll = Mrr; Mrr = tmp; tmp = Pll; Pll = Prr; Prr = tmp; }
		Mc = Mll;
		Pc = Pll;
		bool mflag = true;
		int i = 0;
		while (!(Prr==0) && (abs(Mll-Mrr) > EPS)){
			if ((Pll != Pc) && (Prr != Pc)) // Inverse quadratic interpolation
				Ms = Mll * Prr * Pc / (Pll - Prr) / (Pll - Pc) + 
					Mrr * Pll * Pc / (Prr - Pll) / (Prr - Pc) + 
					Mc * Pll * Prr / (Pc - Pll) / (Pc - Prr);
			else // Secant Rule
				Ms = Mrr - Prr * (Mrr - Mll) / (Prr - Pll);

			double tmp2 = (3. * Mll + Mrr) / 4.;
			if(
				(!(((Ms > tmp2) && (Ms < Mrr)) || ((Ms < tmp2) && (Ms > Mrr)))) ||
				(mflag && (abs(Ms - Mrr) >= ( 0.5*abs(Mrr - Mc) ))) ||
				(!mflag && (abs(Ms - Mrr) >= 0.5*(abs(Mc - Md) )))
			)
			{
				Ms = 0.5*(Mll + Mrr) ;
				mflag = true;
			}
			else
			{
				if ((mflag && (abs(Mrr - Mc) < EPS)) || (!mflag && (abs(Mc - Md) < EPS)))
				{
					Ms = 0.5*(Mll + Mrr) ;
					mflag = true;
			}
				else
					mflag = false;
			}
			fld_a = fanno::calFLD(Ms); // function calculations
			fld_b = fld_a + fld ;
			Mb = calMForFLD (fld_b, 0.2) ;
			Pa = fanno::calPressureRatio( Ms );
			Pb = fanno::calPressureRatio (Mb) ;
			Ps = Pb / Pa - 1./P2P1 ;
			Md = Mc;
			Mc = Mrr;

			Pc = Prr;
			if (Pll * Ps < 0) { Mrr = Ms; Prr = Ps ; }
			else { Mll = Ms; Pll = Ps; }
        // if |f(a)| < |f(b)| then swap (a,b) end if
			if (abs(Pll) < abs(Prr))
			{ double tmp = Mll; Mll = Mrr; Mrr = tmp; tmp = Pll; Pll = Prr; Prr = tmp; }
			i++;
			if (i > 1000) {
				cout <<  Mrr << endl ;
				return ;
			}
		}
		Mbar = Mrr/Mmax ;
	goto outNowA;
	}
	outNowA:
	// calculate Min as well	
	Min = calMForFLD((fld_a + fld),0.5); 
	outNow:
	if ( reverseFlow == true ){
		double tmp ;
		tmp = P1; P1 = P2; P2 = tmp ;
		Mout = Mbar = - Mbar ; reverseFlow = false ;	
		return ;
	}
	else {
		Mout = Mbar;
		return; 
	}
//			while (Error > EPS){
				// new guess
//				P2P1p = calP2P1(M1, M2) ;
//				deltaFLD = deltaFLD *  P2P1/P2P1p ; 
//				M2 = calMForFLD(deltaFLD, 0.5);
//				fld = givenFLD + deltaFLD;
//				M1 =  calMForFLD(fld, 0.5);
//				Error = abs (P2P1 - P2P1p) ;	
//				steps[i][0] = M1 ;
//				steps[i][1] = M2 ;
//				steps[i][2] = fld ;
//				steps[i][3] = calP2P1(M1, M2) ;
//				steps[i][4] = deltaFLD ;
//				i++;
//			}
//			numberOfIterations = i ;
//			return;
};

inputType twoChambers::selectVariable (inputType variableName) {
    if  (variableName == deltaMV  ) { return deltaMV ; }  
    else if  (variableName == MxV  ) { return MxV ; }  
    else if  (variableName == MxpV  ) { return MxpV ; }  
    else if  (variableName == MxpMypPOV  ) { return MypV ; }  
	else { return _noV;};

} ;

void    twoChambers::makeTexTableHead(int * ioData) {
                                                                               
    int variableNumber;
                                                                               
    if (outputInfo == infoStandard ) {
        variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "\\dfrac{P_5}{P_1} ";
        variableNames[1] = (char *) "M_{s1}";
        variableNames[2] = (char *) "\\dfrac{P_2}{P_1}";
        variableNames[3] = (char *) "\\dfrac{T_2}{T_1}";
        variableNames[4] = (char *) "\\dfrac{P_5}{P_3}";
        variableNames[5] = (char *) "\\dfrac{T_5}{T_3}";
        variableNames[6] = (char *) "U_{s5}";
        //variableNames[6] = "{\\rho}_y \\over {\\rho}_x ";
        showVariableTeXHead(variableNames,variableNumber);
		return ;
    }
    else if (outputInfo == infoMax  ) {
        variableNumber = 9;
		char * variableNames[variableNumber];
        variableNames[0] = (char *) "M_x";
        variableNames[1] = (char *) "M_y";
        variableNames[2] = (char *) "{M_x}^{'}";
        variableNames[3] = (char *) "{M_y}^{'}";
        variableNames[4] = (char *) "{T_y} \\over {T_x}";
        variableNames[5] = (char *) "{P_y} \\over {P_x}";
        variableNames[6] = (char *) "{P_0}_y \\over {P_0}_x ";
        variableNames[7] = (char *) "{U_y}^{'} ";
        variableNames[8] = (char *) "{c_x}";

		showVariableTeXHead(variableNames,variableNumber);
		return;
    }
    else if (outputInfo == infoMinimal ) {
        variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "M_x";
        variableNames[1] = (char *) "{M_y}_w";
        variableNames[2] = (char *) "\\theta_w";
        variableNames[3] = (char *) "\\delta ";
        variableNames[4] = (char *) "{P_y} \\over {P_x}  ";
        variableNames[5] = (char *) "{T_y} \\over {T_x}  ";
		  variableNames[6] =   (char *) "{P_0}_y \\over {P_0}_x  ";

        showVariableTeXHead(variableNames,variableNumber);
		return ;
    }
    else  if (outputInfo == infoObliqueMax ) {
        int variableNumber = 4;
        char * variableNames[variableNumber];
        cout << "The maximum values are:" << endl;
        variableNames[0] = (char *) "M_x";
        variableNames[1] = (char *) "M_y";
        variableNames[2] = (char *) "\\delta_{max}";
        variableNames[3] = (char *) "\\theta_{max}";

        showVariableTeXHead ( variableNames, variableNumber);
    }
    else  if (outputInfo == infoIteration ) {
        int variableNumber = 6;
        char * variableNames[variableNumber];
        cout << "The maximum values are:" << endl;
        variableNames[0] = (char *) "i";
        variableNames[1] = (char *) "M_x";
        variableNames[2] = (char *) "M_y";
        variableNames[3] = (char *) "{T_y} \\over {T_x}  ";
        variableNames[4] = (char *) "{P_y} \\over {P_x}  ";
        variableNames[5] = (char *) "{M_y}^{'}  ";

        showVariableTeXHead ( variableNames, variableNumber);
    }
}

void    twoChambers::makeTexTableLine(int * ioData) {
    int numberVariables;

    if (outputInfo == infoStandard ) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = P1 ;
        variableToBePrinted[1] = Mbar;
        variableToBePrinted[2] = PyPx;
        variableToBePrinted[3] = TyTx;
        variableToBePrinted[4] = P2;
        variableToBePrinted[5] = P2;
        variableToBePrinted[6] = TyTx; // sqrt(k5 *287 * T5/T5T3 ) ; 

        showVariableTeXLine (variableToBePrinted, numberVariables);
    }
    else if (outputInfo == infoMax   ) {
        numberVariables = 9;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx ;
        variableToBePrinted[1] = My;
        variableToBePrinted[2] = Mbar;
        variableToBePrinted[3] = Mbar;
        variableToBePrinted[4] = TyTx;
        variableToBePrinted[5] = PyPx ;
        variableToBePrinted[6] = P0yP0x;
        variableToBePrinted[7] = Uyp;
        variableToBePrinted[8] = cx;

       	showVariableTeXLine ( variableToBePrinted, numberVariables);
		return;
    }
    else if (outputInfo == infoMinimal ) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx ;
        variableToBePrinted[1] = 0.;
        variableToBePrinted[2] = theta[0]*180./PI ;
        variableToBePrinted[3] = delta*180./PI;
        variableToBePrinted[4] = PyPx;
        variableToBePrinted[5] = TyTx;
        variableToBePrinted[6] = P0yP0x;
                                                                                
        showVariableTeXLine (variableToBePrinted, numberVariables);
    }
	else  if (outputInfo == infoIteration ) {
        numberVariables = 5;
        double variableToBePrinted[numberVariables];
		for (int i = 0 ; i < numberOfIterations ; i++){
			variableToBePrinted[0] = steps[i][1];
			variableToBePrinted[1] = steps[i][2];
			variableToBePrinted[2] = steps[i][3];
			variableToBePrinted[3] = steps[i][4];
			variableToBePrinted[4] = Mbar;

			cout << (int) steps[i][0] << "&";

        	showVariableTeXLine ( variableToBePrinted, numberVariables);
		}
    }

}

int	twoChambers::calAll( double * variableValue ){
	//double   Mxtmp;
	if  (inputInfo == P1V) {
		shockDynamicsName = "pressure input ";
		P[1][0] =  1.0 ; // variableValue[P1V];
		P[2][0] =  variableValue[P2V];
		T10.value = T1.value = variableValue[T1V] ;
		//T10.value = variableValue[T10V] ;
		T10.isKnown = yes ; T1.isKnown = yes ;
		Le = variableValue[LeV] ;
		cycles = variableValue [cyclesV] ;
		frequency = variableValue [frequencyV] ;
		Vratio = variableValue [VratioV] ;
		fld = variableValue [fldV] ;
		Vdelta = variableValue[VdeltaV] ;
		V10 = 1.0 ; // the initial volume of the piston
		Vstroke = V10 - Vdelta; 
		deltaT = 0.005 ;
		Ubar = frequency*variableValue[UpV] ;
		accumolatedV = 0.0;
		iCyclesLimit = 2100 ; //  (int) frequency * cycles;
		calP();
	}
	return yes;
}

double twoChambers::calUpperLimitsMs1(){
	//return ((sqrt(5*k1*k1-6*k1+5)+k1+1)/(2*k1-2) );
	return ((sqrt(((k1*k1+2*k1+1)*k5+4*k1*k1*k1-8*k1*k1+4*k1)/k5)+k1+1)/((2*k1-2)*sqrt(k1/k5)));
}

double fm (double M, double k){
	double fm;
	fm = pow((1.+ 0.5* (k-1.)*M), (-0.5*(k+1.)/(k -1.)) ) ;
	return fm;
}; 

double f1(double t, double P, double V1 , double Mbar, double Upiston, double k ) {
	double fM, f;
	fM = fm (Mbar, k) ;
	f = k/V1*( Upiston*P - Mbar*fM*pow (P,0.5*(5.*k - 3.)/k ) );
	return f;
}

double f2( double P2, double P1, double Vr , double Mbar, double k ) {
	double f = 1.0, fM;
	fM = fm (Mbar, k) ;
	f = k*pow(P2,((k-1.)/k) ) *Mbar *fM * Vr * pow (P1,0.5*(3*k - 1.)/k ) ; 	
	return f;
}

double twoChambers::calUpiston(){
	accumolatedV = Vdelta + 0.5*Vstroke*( 1. - cos(2.*PI*t + PI) ) ;
	return Vstroke*PI*sin(2.*PI*t);
}

double calMass(double P , double deltaP , double V, double k  ){
	double T =  pow((P + deltaP)/P, (k-1.0/k))  ;
	return (P + deltaP ) * V/T ;// with the R
};

double twoChambers::calP1(int i ){
	double j;
	//calDM (i, P1s);
	j = 1.0/P[V1P][i]* (dm + pow(P[ch1P][i-1], (1./k)) * P[V1P][i-1] );
	return   pow(j,k);
};

double twoChambers::calP2(int i ){
	double f;
	f = P[ch2P][i] = (pow(P[ch2P][i-1],1./k) + dm/Vratio) ; 
	f = pow (f, k) ;
	return f ;
};

void twoChambers::calTi (int i, double P1){
	double junk; 
	//junk = P[T1P][i] =  dm /(dm + P[mBar1P][i-1]) * P1 / P[ch1P][i-1]*P[V1P][i] / P[V1P][i-1] ;
	junk = P[T1P][i] =  P[T1P][i-1] * P1 / P[ch1P][i-1]*P[V1P][i] / P[V1P][i-1] ;
	junk = P[T1P][i] =  P[T1P][i-1] * pow( P1 / P[ch1P][i-1], (k-1.)/k) ;
}
void twoChambers::calDM (int i, double P1){
	double junk ; // , mI, mIminus;
	junk = dm = P[ch1P][i-1]/  P[T1P][i-1] * M * fm(M,k) * sqrt(k*R.value*T1.value)*deltaT/Le ;
	//dm = M * fm(M,k) * pow( P1, (0.5 * (3*k-1.) / k) )  ; // * sqrt(k*R.value)     ;
	//Upiston * P[ch1P][i-1] / P[T1P][i-1] ; 
	//mI = P1 * P[V1P][i] / P[T1P][i] ;
	//mIminus = P[ch1P][i-1] * P[V1P][i-1] / P[T1P][i-1] ;
	//dm = P1 * P[V1P][i] / P[T1P][i] - P[ch1P][i-1] * P[V1P][i-1] / P[T1P][i-1]; junk = dm ; 
}

void twoChambers::calP2Step (int i, double P1, double P2){
	calDM( i, P1 ) ; 	
	//calTi (i, P1 ) ;
	calP2( i ) ;
	calM(P1 , P2 ) ;
}

int twoChambers::calP (){ // main routine to cal pressure
	//double K1, K2, K3, K4;
	double  m2010;
	//double Kk1, Kk2, Kk3, Kk4 ;
	double T1s, P1s ;
	double mBarToShow , dP1, dP2, dammyV, ttt=(double ) 0.0, mBar[3], deltaMbar[3] ;
	enum {cylinderC = 0 , chamberC = 1, totalC = 2};
	double junk ;
	dP1 = P1 = P1s = P[ch1P][0] ; 
	dP2 = P2 = P[ch2P][0];
	m2010 = P[ch2P][0] / P[ch1P][0] * Vratio; // m20/m10
	t = deltaMbar[0] = deltaMbar[1] = deltaMbar[2] =  (double ) 0 ;
	P[UpistonP][0] = calUpiston (); 
	P[V1P][0] = V1 = accumolatedV;
	P[mBar1P][0] = 1.0;
	P[mBar2P][0] = Vratio; // to be modified according to initial condition
	P[T1P][0] = P[T2P][0] = T1.value; //to be modified according to initial condition
	P[mTotalP][0] = P[mBar1P][0] + m2010* P[mBar2P][0] ;
	for (int i = 1 ; i < iCyclesLimit ; i++){ 
		//P1 = dP1 ; P2 = dP2 ;
		ttt = t = t + deltaT ;
		Upiston  = calUpiston ();
		P[V1P][i] = dammyV = V1 = accumolatedV ;
		calM(P[ch1P][i-1], P[ch2P][i-1]); 
		calDM (i, P1s ) ;
		P1s = dP1 =  calP1(i);
		P[ch2P][i] = P2 = dP2 = calP2(i) ;
		//K1 = (deltaT * f2( P2, P1s, V1, Mbar,  k));
			//calP2Step (i, (P1s + 0.5 * K1), P2 ); P2 = P[ch2P][i] ;
		//calM((P1s + 0.5 * K1), P2 );
		mBarToShow = Mbar ;
		//K2 = (deltaT * f1((t + 0.5 * deltaT), (P1s + 0.5 * K1), V1, Mbar, Upiston, k));
		//calP2Step (i, (P1s + 0.5 * K1), P[ch2P][i] );
		//P2 = P[ch2P][i] ; 
		//calM((P1s + 0.5 * K2), P2 );
		//mBarToShow = Mbar ;
		//K3 = (deltaT * f1((t + 0.5 * deltaT), (P1s + 0.5 * K2), V1, Mbar, Upiston, k));
		//calP2Step (i, (P1s + 0.5 * K2), P[ch2P][i] );
		//P2 = P[ch2P][i] ; 
		//calM((P1s + 0.5 * K3), P2 );
		//mBarToShow = Mbar ;
		//K4 = (deltaT * f1((t + deltaT), (P1 + K3), V1, Mbar, Upiston, k));
		//P1 = (P1 + (1. / 6.) * (K1 + 2. * K2 + 2. * K3 + K4));
		//calP2Step (i, P1 , P2 );
		//P2 = P[ch2P][i] ;
		//P2 = (P2 + (1. / 6.) * (Kk1 + 2. * Kk2 + 2. * Kk3 + Kk4));
		// double junk = pow ( (P1 /P[ch1P][i-1]),((k-1.)/k)) ; junk = junk * accumolatedV /P[V1P][i-1] ;
		dP1 = P1 = P1s ; dP2 = P2 ;
		T1s =  P[T1P][i-1]*pow(P1s/P[ch1P][i-1],(k-1.)/k) ;  
		P[mBar2P][i] =  P[mBar2P][i-1] + dm ;
			//junk = mBar[chamberC] = P[mBar2P][i] - dm  ;
		//P[V1P][i] = dammyV ;
		if ( P[UpistonP][i] <  Upiston ){
			Uyp = Upiston ;
			Uxp = P[UpistonP][i] ;
			//deltaMbar[0] = mBar[cylinderC] - P[mBar1P][i] ;
			calTemperatureAfterShock (i, T1s) ; // calculate the pressure after the shock 
			dP1 = P1 ;
			T1s = P[T1P][i-1] * pow ( (P1/P[ch1P][i-1] ),(k-1.)/k) ; 
			P[T1P][i] = junk = T1s * TyTx ;
			P[ch1P][i] = P1 ; //  P1 already done. P[T1P][i]/P[T1P][i-1]*P1 ;; // increase P in the piston due to add energy
		}
		else {
			P[T1P][i] = P[T1P][i-1] * pow ( (P1/P[ch1P][i-1] ),(k-1.)/k) ;
			//deltaMbar[0] = P[mBar1P][i] - mBar[cylinderC] ;
		}
		junk = mBar[cylinderC] = P[mBar1P][i-1] - dm ;// pow(P1/P[ch1P][i-1],(k-1.)/k ) *V1/P[V1P][i] ;
		junk = mBar[chamberC] =  P[mBar2P][i-1] + dm ; // pow((P2/P[ch1P][0]), (1./k) ) * m2010   ;
		//deltaMbar[chamberC] = mBar[chamberC] - P[mBar2P][i] ;
		calChamberTemperature (i) ;
		P[timeP]		[i] = t ;
		P[ch1P]		[i] = P1s = P1 ;
		P[ch2P]		[i] = P2 ;
		P[MbarP]		[i] = Mbar ;
		P[UpistonP]	[i] = Upiston ;
		P[V1P]		[i] = V1 ;
		junk = P[mBar1P]	[i] = mBar[cylinderC]  ;// P[mBar1P][i-1] * mBar[cylinderC] ;
		junk = P[mBar2P]	[i] = mBar[chamberC] ;
		junk = P[mTotalP]	[i] = mBar[cylinderC] + mBar[chamberC] ;
		//P[T1P]	[i] = T1;
		P[T2P]	[i] = T2;
	}
	return yes;
};

int twoChambers::calChamberTemperature ( int i){
	double junk;
	double Tin = P[T1P][i-1];
	T2 = junk = P[T2P][i -1] + dm*k*(Tin-P[T2P][i-1]+ 0.5*M*M*Tin*(k-1.0))/(P[mBar2P][i-1] + dm)  ;	
	return yes;
}
int twoChambers::calTemperatureAfterShock ( int i, double T1s){
	double a, b ;
	//Uyp =  Upiston ;
	Tx =  T1s;
	cal_cx () ;
	//to be continue
	//  Uxp = variableValue[UxpV];
	a = cx;
	b = a + Uyp; 
	Ux = brent ( calFUtwoChamber, this, a, b, 0.0) ; 
	Mx = Ux/cx; 
	calMy () ;
	calUxUy();
	P1 = P1 * PyPx ;
	return yes;
};

void  twoChambers::calUxUy () {
	PyPx =  (1. + k*Mx*Mx) / (1. + k*My*My) ;
	TyTx = PyPx * PyPx * My*My/Mx/Mx;
	UxUy = RyRx = PyPx/TyTx;
}; 

void  twoChambers::calMy(){
	if (  Mx < sqrt(2.0*k / (k-1.0) ) )
		My = sqrt((Mx*Mx + 2.0/(k -1.0)) / ((2.0*k / (k-1.0) ) *Mx*Mx -1.0)) ;
	else
		My = -1.0;
};

int twoChambers:: calTableLine (int* ioData, double * variableValue){
	if ((outputType) ioData[whatInfoN] == infoStandard ||
		(outputType) outputInfo == infoMinimal ||
		(outputType) outputInfo == infoIteration ){
		return compressibleFlow::calTableLine (ioData, variableValue);
   }
	else if ((outputType) ioData[whatInfoN] == infoTubeProfile )  { 
		if (calAll (variableValue) == yes) {
			//return calProfile (ioData, variableValue) ;
			return yes;
		}
		else {
			return no;
		}
	}
	else {
		return calAll (variableValue) ;
	}
};


int twoChambers:: calTableLine (  inputType variableName,
            double * variableValue){
	if (outputInfo == infoStandard || outputInfo == infoMinimal ){
		return calAll( variableValue);
	}
	else if (outputInfo == infoTubeProfile ) {
		return calAll( variableValue);
	}
    return no;
};

int twoChambers::doAllLines ( int * ioData, double * variableValue ){
	inputType variableName = (inputType) ioData[variableNameN];
	int howManyPoints,  howManySets ; 
	double startP, endP, rangeD;
	double range [61];
	if (outputInfo == infoStandard ){
		if (variableName == P5P1V){
			startP = 1.01;
			endP = 500.;
		}
		else{ //the default is not determined yet 
			startP = 0.5*(k-1.)/k+EPS;
			//upper boundary 
			endP  = double(int (startP *10.) + 1 )/10.;
		}
		if (variableName == P5P1V ){
			howManySets = 4;
			range[0] = (double) howManySets;
			range[1] =  9.0; // howManyPoints;
			range[2] = 1.01 ; // starting point  
			range[3] = 10.10 ;  // last point
			range[4] = 10. ; 
			range[5] = range[3] + 0.9 ; // starting point  from the last range 
			range[6] = 50. ;  // last point   
			range[7] = 8. ; 
			range[8] = range[6] + 10. ; // starting point  from the last range  
			range[9] = 90. ; 
			range[10] = 8. ; 
			range[11] = range[9] + 1.; // 
			range[12] = 500. ;//
		}
		//else {
		//	return no;
		//}

		setRange( ioData, range, variableValue);

		howManyPoints = 9 ;
		rangeD = 0.1;
		startP = endP + rangeD;
		endP = 2.0;

		range[0] = (double) howManyPoints;
		range[2] = startP ; 
		range[3] = endP ; 
		//setRange( variableName, isTex, range, variableValue);
		//makeLines(isTex,howManyPoints, startP,  endP,
								 //variableName, variableValue);
		howManyPoints = 7 ;
		rangeD = 1.0 ;
		startP = endP + rangeD;
		endP = 10.;
		//makeLines(isTex,howManyPoints, startP,  endP,
		//						 variableName, variableValue);
	}
   else if (outputInfo == infoD0) {
        howManyPoints = 9 ;
         makeLines(ioData,howManyPoints, 1.1,  2.0, variableValue);
        howManyPoints = 9 ;
         makeLines(ioData,howManyPoints, 2.2,  4.0,  variableValue);
        howManyPoints = 5 ;
         makeLines(ioData,howManyPoints, 5.0,  10.0,  variableValue);
    }

    return yes;
}

void	twoChambers::calPressureRatio(){
	//discontinuity::calMy();
	fanno::calPressureRatio();
};

int twoChambers::makeTable ( int * ioData, double * variableValue )  {
	return compressibleFlow::makeTable (  ioData,  variableValue ) ; 
}

void 	twoChambers::makeHTMLTableHead(int * ioData){
    int variableNumber;
                                                                               
    if (outputInfo == infoStandard || outputInfo == infoMinimal) {
        variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "P5/P1 ";
        variableNames[1] = (char *) "Ms1";
        variableNames[2] = (char *) "T2/T1";
        variableNames[3] = (char *) "UU"; // "My&prime;";
        variableNames[4] = (char *) "Ty/Tx";
        variableNames[5] = (char *) "Py/Px";
        variableNames[6] = (char *) "P0y/P0x";
                                                                               
        showVariableHTMLHead(variableNames,variableNumber);
    }
    else  if (outputInfo == infoIteration ) {
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "i    ";
        variableNames[1] = (char *) "Mx";
        variableNames[2] = (char *) "My";
        variableNames[3] = (char *) "TyTx";
        variableNames[4] = (char *) "PyPx";
        variableNames[5] = (char *) "Myp";
        showVariableHTMLHead( variableNames, variableNumber);
    }
};

void    twoChambers::makeHTMLTableLine(int * ioData) {
    int numberVariables;

    if (outputInfo == infoStandard || outputInfo == infoMinimal) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Pbar ;
        //variableToBePrinted[1] = Ms1;
        variableToBePrinted[2] = Tbar;
        variableToBePrinted[3] = Tbar;
        variableToBePrinted[4] = TyTx;
        variableToBePrinted[5] = PyPx ;
        variableToBePrinted[6] = P0yP0x;

        showVariableHTMLLine (variableToBePrinted, numberVariables);
    }
	else  if (outputInfo == infoIteration ) {
		numberVariables = 6;
		double variableToBePrinted[numberVariables];
		for (int i = 0 ; i < numberOfIterations ; i++){
			variableToBePrinted[0] = i;
			variableToBePrinted[1] = steps[i][1];
			variableToBePrinted[2] = steps[i][2];
			variableToBePrinted[3] = steps[i][3];
			variableToBePrinted[4] = steps[i][4];
			//variableToBePrinted[5] = Myp;

			showVariableHTMLLine(variableToBePrinted, numberVariables);
		}
        return;
    }
};

void twoChambers :: showVariableHTMLHead (char ** variableNames,
					 int numberVariables ) {
	cout << endl << endl;
	//cout << "this html code for table" << endl ;
	cout << "<tr class=\"text\"><td align=\"center\">" << endl ;
	cout << "<table border=1 width=\"100%\" >" << endl ;
	cout << "  <thead>" << endl;
	cout << "    <tr>" << endl ;
	cout << "      <th align=left bgcolor=\"#fffeaa\" colspan=";
	cout << 3 << " >";
	cout <<  flowModelName ;
	cout << " </th>" << endl;
	cout << "      <th align=left  bgcolor=\"#00ff5a\" colspan=";
	cout << numberVariables-4 << "  rowspan=2 >"; 
	cout << "Input: ";
	cout << inputVariableName ;
	cout << " </th>" << endl;
	cout << "      <th align=left  bgcolor=\"#9ae0ee\" colspan=";
	cout << 1 << " rowspan=2 >";  
	cout << "k = ";
	cout << k ;
	cout << " </th>" << endl;
	cout << "    </tr>" << endl ;
	cout << "    <tr>" << endl ;
	cout << "      <th align=left bgcolor=\"#fffeaa\" colspan="; 
	cout << 3 << " >";  
	cout <<  flowModelName ;
	cout << " </th>" << endl;
	cout << "    </tr>" << endl ;
	cout << "    <tr>" << endl ;
	for (int i=0; i < numberVariables ; i++){
		cout << "      <th align=center >" ;
		cout <<  variableNames[i] << " </th>" << endl;
	}
	cout << "    </tr> " << endl;
	cout << "  </thead>" << endl;
	cout << "  <tbody>" << endl;
};
