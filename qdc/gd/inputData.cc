/* last change Wed Jul 11 15:33:43 CDT 2007 isHTML issues 
********************************************************
	file:	inputData.cc

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow   
	for given 4FL/D or other info.
	The code also produces table for a specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/
#	 include <iostream>
#	 include <map>
#	 include <string>
#	 include "iniFile.h"
#	 include "definition.h"

using namespace std;

int writeFile(double * variableValue, int *  inputOutput ,  string fileName)
{
	CIniFile iniFile( fileName);
	return yes;
}

int readFile(double * variableValue, int *  inputOutput ,  string fileName)
{
	string whatInfo, variableName, isRange, isTex, isHTML, rangeParameter, 
		isBothBranches, isChoked, className;
		
	CIniFile iniFile( fileName);
	if (iniFile.ReadFile() == false ) {
		return false;
	};
	//cout << "NKeys = " << iniFile.NumKeys() << endl;
	//cout << "number of data" << iniFile.NumValues("data") << endl;
	whatInfo =     iniFile.GetValue("InputOutput", "whatInfo") ;
	variableName = iniFile.GetValue("InputOutput", "variableName") ;
	isRange = iniFile.GetValue("integer", "isRange") ;
	rangeParameter = iniFile.GetValue("integer", "rangeNameName") ;
	isTex = iniFile.GetValue("integer", "isTex") ;
	isHTML = iniFile.GetValue("integer", "isHTML") ;
	isBothBranches = iniFile.GetValue("integer", "isBothBranches") ;
	isChoked = iniFile.GetValue("integer", "isChoked") ;
	className = iniFile.GetValue("InputOutput", "className") ;
	if (isHTML == "no") 
		cout << className << " and name of variable is " 
				<<  variableName << endl;

	if (isTex == "yes") 
		inputOutput[isTexN] = yes ; 
	else 
		inputOutput[isTexN] = no ; 

	if (isHTML == "yes") 
		inputOutput[isHtmlN] = yes ; 
	else 
		inputOutput[isHtmlN] = no ; 
		
	if (isRange == "yes"){ 
		inputOutput[isRangeN] = yes ; 
		if (rangeParameter == "Mrange") {
			inputOutput[whatRangeParameterN] =  (int) Mrange  ;
		}
		else if (rangeParameter == "M1range") {
			inputOutput[whatRangeParameterN] =  (int) M1range  ;
		}
		else{ //default
			inputOutput[whatRangeParameterN] =  (int) Mrange  ;
		}
	}
	else 
		inputOutput[isRangeN] = no ; 

    if (isBothBranches == "yes")
        inputOutput[isBothBranchesN] = yes ;
    else
        inputOutput[isBothBranchesN] = no ;

    if (isChoked == "no")
        inputOutput[isChokedN] = no ;
    else
        inputOutput[isChokedN] = yes ;

	string flowModel;
	if (className == "stagnationFlow") { 
		inputOutput[classNameN] = (int) stagnationFlow;
		flowModel = "stagnation";
	}
	else if (className == "fannoFlow") { 
		inputOutput[classNameN] = (int) fannoFlow;
		flowModel = "Fanno";
	}
	else if (className == "shockFlow")  {
		inputOutput[classNameN] = (int) shockFlow;
		flowModel = "Shock";
	}
	else if (className == "rayleighFlow")  {
		inputOutput[classNameN] = (int) rayleighFlow;
		flowModel = "Rayleigh";
	}
	else if (className == "obliqueFlow") { 
		inputOutput[classNameN] = (int) obliqueFlow;
		flowModel = "Oblique Shock";
	}
	else if (className == "shockDynamicsFlow") { 
		inputOutput[classNameN] = (int) shockDynamicsFlow;
		flowModel = "Shock Dynamics";
	}
	else if (className == "shockTubeFlow") { 
		inputOutput[classNameN] = (int) shockTubeFlow;
		flowModel = "Shock Tube";
	}
	else if (className == "pmFlow") { 
		inputOutput[classNameN] = (int) pmFlow;
		flowModel = "P-M Flow";
	}
	else if (className == "isothermalFlow") { 
		inputOutput[classNameN] = (int) isothermalFlow;
		flowModel = "Isothermal Pipe Flow";
	}
	else if (className == "isothermalNozzleFlow") { 
		inputOutput[classNameN] = (int) isothermalNozzleFlow;
		flowModel = "Isothermal Nozzle Flow";
	}
	else if (className == "isothermalnozzleFlow") { 
		inputOutput[classNameN] = (int) isothermalNozzleFlow;
		flowModel = "Isothermal Nozzle Flow";
	}
	else if (className == "twoChambersFlow") { 
		inputOutput[classNameN] = (int) twoChambersFlow;
		flowModel = "two Chambers Flow";
	}
	else {//default 
		inputOutput[classNameN] = (int) stagnationFlow;
		flowModel = "stagnation (default)";
	}
		
    //inputOutput[isTexN] = no ;
    //if (isRange == "yes")
    //inputOutput[isRangeN] = yes ;

	if (isHTML == "no" ) {
		if ( isTex == "yes" ) 
			cout << "isTex = yes ::::" ;  
		else 
			cout << "isTex = no ::::" ;
		
		if (isRange == "yes") { 
			cout << "isRange = yes " ;
		}
		else {
			cout << "isRange = no :::::::: " ;
		}
		cout << "Flow Model = " <<  flowModel<< endl ;
	};

	if (whatInfo == "infoMinimal" ) 
		inputOutput[whatInfoN] = infoMinimal ; 
	else if (whatInfo == "infoStandard" )
		 inputOutput[whatInfoN] = (int) infoStandard; 
	else if ( whatInfo == "infoTube") 
		 inputOutput[whatInfoN] =  infoTube; 
	else if ( whatInfo == "infoMax") 
		 inputOutput[whatInfoN] =  infoMax; 
	else if ( whatInfo == "infoObliqueMax") 
		 inputOutput[whatInfoN] = infoObliqueMax; 
	else if ( whatInfo == "infoIteration") 
		 inputOutput[whatInfoN] =  infoIteration; 
	else if ( whatInfo == "infomDot") 
		 inputOutput[whatInfoN] =  infomDot; 
	else if ( whatInfo == "infoFn") 
		 inputOutput[whatInfoN] =  infoFn; 
	else if ( whatInfo == "infoRange") 
		 inputOutput[whatInfoN] =  infoRange; 
	else if ( whatInfo == "infoTubeProfile") 
		 inputOutput[whatInfoN] =  infoTubeProfile; 
	else  
         inputOutput[whatInfoN] = infoStandard;

	if (variableName == "machV")
		inputOutput[variableNameN] = machV;
	else if (variableName == "PbarV")
		inputOutput[variableNameN] = PbarV;
	else if (variableName == "RbarV")
		inputOutput[variableNameN] = RbarV;
	else if (variableName == "fldV") {
		inputOutput[variableNameN] = fldV;
		inputOutput[isFLDknownN] = yes;
	}
	else if (variableName == "p2p1FLDV")
		inputOutput[variableNameN] = p2p1FLDV;
	else if (variableName == "angleV")
		inputOutput[variableNameN] = angleV;
	else if (variableName == "nuV")
		inputOutput[variableNameN] = nuV;
	else if (variableName == "M1ShockV")
		inputOutput[variableNameN] = M1ShockV;
	else if (variableName == "FLDShockV")
		inputOutput[variableNameN] = FLDShockV;
	else if (variableName == "P1V") 
		inputOutput[variableNameN] = (int) P1V;
	else if (variableName == "M1fldV")
		inputOutput[variableNameN] = (int) M1fldV;
	else if (variableName == "M2fldV")
		inputOutput[variableNameN] = (int) M2fldV;
	else if (variableName == "M1P2P1V")
		inputOutput[variableNameN] = (int) M1P2P1V;
	else if (variableName == "M2P2P1V")
		inputOutput[variableNameN] = (int) M2P2P1V;
	else if (variableName == "deltaFldV")
		inputOutput[variableNameN] = (int) deltaFldV;
	else if (variableName == "M1fldP2P1V")
		inputOutput[variableNameN] = (int) M1fldP2P1V;
	else if (variableName == "fldP2P1V")
		inputOutput[variableNameN] = (int) fldP2P1V;
	else if (variableName == "MxV")
		inputOutput[variableNameN] = (int) MxV ;
	else if (variableName == "MyV")
		inputOutput[variableNameN] = (int) MyV ;
	else if (variableName == "mDotP0TV")
		inputOutput[variableNameN] = (int) mDotP0TV ;
	else if (variableName == "mDotPT0V")
		inputOutput[variableNameN] = (int) mDotPT0V ;
	else if (variableName == "MFLDshockLocationV")
		inputOutput[variableNameN] =  MFLDshockLocationV ;
	else if (variableName == "AstarV")
		inputOutput[variableNameN] = AstarV ;
	else if (variableName == "PARV")
		inputOutput[variableNameN] =  PARV ;
	else if (variableName == "P0yP0xV")
		inputOutput[variableNameN] = P0yP0xV ;
	else if (variableName == "T0yT0xV")
		inputOutput[variableNameN] = T0yT0xV ;
	else if (variableName == "R0yR0xV")
		inputOutput[variableNameN] = R0yR0xV ;
	else if (variableName == "TbarV")
		inputOutput[variableNameN] = TbarV ;
	else if (variableName == "RbarV")
		inputOutput[variableNameN] = RbarV ;
	else if (variableName == "TRstarV")
		inputOutput[variableNameN] = TRstarV ;
	else if (variableName == "TRzeroStarV")
		inputOutput[variableNameN] = (int)  TRzeroStarV ;
	else if (variableName == "RRzeroStarV")
		inputOutput[variableNameN] = (int)  RRzeroStarV ;
	else if (variableName == "deltaMV")
		inputOutput[variableNameN] =   deltaMV ;
	else if (variableName == "thetaMV")
		inputOutput[variableNameN] = thetaMV ;
	else if (variableName == "deltaThetaV")
		inputOutput[variableNameN] = deltaThetaV ;
	else if (variableName == "omegaV")
		inputOutput[variableNameN] = omegaV ;
	else if (variableName == "PRzeroStarV")
		inputOutput[variableNameN] = PRzeroStarV ;
	else if (variableName == "MxpV")
		inputOutput[variableNameN] = (int) MxpV ;
	else if (variableName == "MypOpenV")
		inputOutput[variableNameN] = (int) MypOpenV ;
	else if (variableName == "MxOpenV")
		inputOutput[variableNameN] = (int) MxOpenV ;
	else if (variableName == "MyOpenV")
		inputOutput[variableNameN] = (int) MyOpenV ;
	else if (variableName == "MxGV")
		inputOutput[variableNameN] = (int) MxGV ;
	else if (variableName == "MxpMypPOV")
		inputOutput[variableNameN] = (int) MxpMypPOV ;
	else if (variableName == "MxpMyPOV")
		inputOutput[variableNameN] = (int) MxpMyPOV ;
	else if (variableName == "MxMypPCV")
		inputOutput[variableNameN] = (int) MxMypPCV ;
	else if (variableName == "MxpMypPCV")
		inputOutput[variableNameN] = (int) MxpMypPCV ;
	else if (variableName == "UxpV")
		inputOutput[variableNameN] = (int) UxpV ;
	else if (variableName == "UyOpenV")
		inputOutput[variableNameN] = (int) UyOpenV ;
	else if (variableName == "UypTxOV")
		inputOutput[variableNameN] = (int) UypTxOV ;
	else if (variableName == "UsOpenV")
		inputOutput[variableNameN] = (int) UsOpenV ;
	else if (variableName == "kV")
		inputOutput[variableNameN] = (int) kV ;
	else if (variableName == "PyPxV")
		inputOutput[variableNameN] = (int) PyPxV ;
	else if (variableName == "TyTxV")
		inputOutput[variableNameN] = (int) TyTxV ;
	else if (variableName == "RyRxV")
		inputOutput[variableNameN] = (int) RyRxV ;
	else if (variableName == "PyPxOpenV")
		inputOutput[variableNameN] = (int) PyPxOpenV ;
	else if (variableName == "P5P1V")
		inputOutput[variableNameN] = (int) P5P1V ;
	else if (variableName == "UpTyV")
		inputOutput[variableNameN] = (int) UpTyV ;
	else  // default
		inputOutput[variableNameN] =(int)  machV;

//	iniFile.Path( "./iniTest.sav");
//	iniFile.WriteFile();

	variableValue[kV] = iniFile.GetValueF("data", "k") ;  
	variableValue[k1V] = iniFile.GetValueF("data", "k1") ;  
	variableValue[k5V] = iniFile.GetValueF("data", "k5") ;  
	variableValue[M1V] = iniFile.GetValueF("data", "M1") ;  
	variableValue[fldV] = iniFile.GetValueF("data", "fld") ;
	variableValue[P2P1V] = iniFile.GetValueF("data", "P2P1") ;
	variableValue[P5P1V] = iniFile.GetValueF("data", "P5P1") ;
	variableValue[M2V] = iniFile.GetValueF("data", "M2") ; 
	variableValue[machV] = iniFile.GetValueF("data", "M") ; 
	variableValue[MxV] = iniFile.GetValueF("data", "Mx") ; 
	variableValue[MyV] = iniFile.GetValueF("data", "My") ; 
	variableValue[PbarV] = iniFile.GetValueF("data", "Pbar") ; 
	variableValue[P1V] = iniFile.GetValueF("data", "P1") ; 
	variableValue[T10V] = iniFile.GetValueF("data", "T10") ; 
	variableValue[P2V] = iniFile.GetValueF("data", "P2") ; 
	variableValue[UpV] = iniFile.GetValueF("data", "Up") ; 
	variableValue[RbarV] = iniFile.GetValueF("data", "Rbar") ; 
	variableValue[TbarV] = iniFile.GetValueF("data", "Tbar") ;
	variableValue[TRstarV] = iniFile.GetValueF("data", "TRstar") ; 
	variableValue[TRzeroStarV] = iniFile.GetValueF("data", "TRzeroStar");
	variableValue[PRzeroStarV] = iniFile.GetValueF("data", "PRzeroStar");
	variableValue[RRzeroStarV] = iniFile.GetValueF("data", "RRzeroStar");
	variableValue[AstarV] = iniFile.GetValueF("data", "Astar") ; 
	variableValue[PARV] = iniFile.GetValueF("data", "PAR") ; 
	variableValue[P0yP0xV] = iniFile.GetValueF("data", "P0yP0x") ;  
	variableValue[T0yT0xV] = iniFile.GetValueF("data", "T0yT0x") ;  
	variableValue[R0yR0xV] = iniFile.GetValueF("data", "R0yR0x") ;  
	variableValue[thetaV] = iniFile.GetValueF("data", "theta") ;
	variableValue[deltaMV] = iniFile.GetValueF("data", "delta") ;
	variableValue[deltaV] = iniFile.GetValueF("data", "delta") ;
	variableValue[angleV] = iniFile.GetValueF("data", "angle") ;
	variableValue[shockLocationV]=iniFile.GetValueF("data", "shockLocation");
	variableValue[MxpV] = iniFile.GetValueF("data", "Mxp");
	variableValue[UxpV] = iniFile.GetValueF("data", "Uxp");
	variableValue[UypV] = iniFile.GetValueF("data", "Uyp");
	variableValue[UsV] = iniFile.GetValueF("data", "Us");
	variableValue[TxV] = iniFile.GetValueF("data", "Tx");
	variableValue[T1V] = iniFile.GetValueF("data", "T1");
	variableValue[T5V] = iniFile.GetValueF("data", "T5");
	variableValue[L0V] = iniFile.GetValueF("data", "L0");
	variableValue[L1V] = iniFile.GetValueF("data", "L1");
	variableValue[L2V] = iniFile.GetValueF("data", "L2");
	variableValue[MypOpenV] = iniFile.GetValueF("data", "Myp");
	variableValue[TyV] = iniFile.GetValueF("data", "Ty");
	variableValue[MxOpenV] = iniFile.GetValueF("data", "Mx");
	variableValue[MyOpenV] = iniFile.GetValueF("data", "My");
	variableValue[TyTxV] = iniFile.GetValueF("data", "TyTx");
	variableValue[PyPxV] = iniFile.GetValueF("data", "PyPx");
	variableValue[RyRxV] = iniFile.GetValueF("data", "RyRx");
	variableValue[MypV] = iniFile.GetValueF("data", "Myp");
	variableValue[MypV] = iniFile.GetValueF("data", "Myp");
	variableValue[PV] = iniFile.GetValueF("data", "P");
	variableValue[P0V] = iniFile.GetValueF("data", "P0");
	variableValue[TV] = iniFile.GetValueF("data", "T");
	variableValue[T0V] = iniFile.GetValueF("data", "T0");
	variableValue[mDotV] = iniFile.GetValueF("data", "mDot");
	variableValue[nuV] = iniFile.GetValueF("data", "nu");
	variableValue [cyclesV] = iniFile.GetValueF("data", "cycles") ; 
	variableValue [p2p1V] = iniFile.GetValueF("data", "P2P1") ; 
	variableValue [VdeltaV] = iniFile.GetValueF("data", "Vdelta") ; 
	variableValue [frequencyV] = iniFile.GetValueF("data", "frequency") ; 
	variableValue [LeV] = iniFile.GetValueF("data", "Le") ; 
	variableValue [VratioV] = iniFile.GetValueF("data", "Vratio");


//  Lookup what we have now.
//	for ( unsigned keyID = 0; keyID < iniFile.NumKeys(); ++keyID) {
//		cout << "Key = " << iniFile.KeyName( keyID) << endl;
//		for ( unsigned valueID = 0;
//				 valueID < iniFile.NumValues( keyID); ++valueID) {
//			cout << "   ValueName = " << iniFile.ValueName( keyID, valueID) <<
//				"  Value = " << iniFile.GetValue( keyID, valueID) << endl;
//		}
 // }
  return yes;
}

int readDefaultData(double* variableValue, int*  inputOutput ){
	double k, M, M1, M2, FLD, p2p1, Mx, My, Pbar, Tbar, TRstar,
		TRzeroStar,  Astar, PAR, P0yP0x, theta, delta, omega, nu, Uxp, Uyp,
		Us, Mxp, Myp, Msx, Msy, Tx ;
	outputType whatInfo;
	//int isTex = yes;
	//int isRange = no;
	inputType variableName; 
/* the kind of information needed */
 	//whatInfo = infoStagnation ; // total properties (stagnation)
 	whatInfo = (outputType) infoStandard;//standard tube flow and shock(point)
 	//whatInfo = infoMinimal ; //minimal weak shock 
 	//whatInfo = infoTube ; //print two side of tube flow 
 	//whatInfo = infoShock ; // two side of shock
 	//whatInfo = infoTubeProfile ; // the tube profile
 	//whatInfo = infoTubeShock ; // two sides and shock area
 	//whatInfo = infoObliqueMax ; //oblique max of parameters
 	//whatInfo = infoObliqueMin ; //oblique min of parameters
 	//whatInfo = infoD0 ; //oblique min of parameters
/* the kind of information provided */
	//variableName =  M1fldV; // M1 & fld given
	//variableName =  p2p1FLDV; // p2/p1, M1 & fld given
	//variableName =  MxV; // upstream Mach number
	//variableName =  fldV; // fld given
	//variableName =  angleV; // the angle of depends on the class
	//variableName =  deltaThetaV; // the two angles
	//variableName =  UsV; // moving shock velocity 
	//variableName =  UypV; // moving shock velocity 
	variableName =  UxpV; // moving shock velocity 
	//variableName =  machV; // Mach number
	//variableName =  AstarV; // the star area ratio
	//variableName =  M1ShockV; //    
	//variableName =  MFLDshockLocationV; // 
	//variableName =  PbarV; // the pressure ratio 
	//variableName =  TbarV; // the temperature ratio 
	//variableName =  TRstarV; // T/T*
	//variableName =  TRzeroStarV; // 
	//variableName =  PARV; // 
	//variableName =  P0yP0xV; // 
	//variableName =  deltaMV; // 

	M1 = 1.5;
	M2 = 0.5;
	FLD = 40.0 ;
	p2p1 = 0.5 ; 
	//M =  3.0 ;
	M =   3.3 ;
	Mx =  1.2346  ;
	My =  .2346  ;
	Pbar = 0.2619;
	Tbar = 0.4053 ;
	TRstar = 0.4053 ;
	TRzeroStar = 0.5016 ;
	Astar = 4.0;
	PAR = 1.5 ;
	P0yP0x = 0.5525;
	delta = 14.43; 
	theta = 30.099 ;
	omega = 20.0;
	nu = 26.4449 ;
	k = 1.4;
	Uyp = 100.;
	Mxp = 1.5;
	Uxp = 400.;
	Tx = 273.;
	Us = 320.;
	Myp = 1.3;
	Msx = 1.3;
	Msy = 0.5;
		
	variableValue[kV]=k;
	variableValue[PbarV]=Pbar;
	variableValue[M1V]=M1;
	variableValue[M2V]=M2;
	variableValue[fldV]=FLD;
	variableValue[p2p1V]=p2p1;
	variableValue[machV]=M;
	variableValue[MxV]=Mx;
	variableValue[MyV]=My;
	variableValue[AstarV]=Astar;
	variableValue[PARV]=PAR;
	variableValue[P0yP0xV]=P0yP0x;
	variableValue[TbarV]=Tbar;
	variableValue[TRstarV]=TRstar;
	variableValue[TRzeroStarV]=TRzeroStar;
	variableValue[thetaV]=theta; 
	variableValue[deltaMV]=delta;
	variableValue[angleV]=nu;
	variableValue[UypV]=Uyp;
	inputOutput[variableNameN] = (int) variableName ;
	inputOutput[whatInfoN] = (int)  whatInfo ; 
	inputOutput[isTexN] =  no ; 
	inputOutput[isRangeN] =  no ; 
	inputOutput[classNameN] =  shockFlow ; 
	inputOutput[isBothBranchesN] = no ;
	return yes;
}

