#include <iostream>

using namespace std;

#include "iniFile.h"
#include "definition.h"
#include <map>
#include <string>


enum StringValue { 	evNotDefined,

					evStringValue1,
					evStringValue2,
					evStringValue3,
					evEnd };

static std::map<std::string, StringValue> s_mapStringValues;

// Intialization
static void Initialize();

int readFile(double * variableVaule)
{
	double Pbar;
	string whatInfo, variableName;
	CIniFile iniFile( "data.ini");
	iniFile.ReadFile();
	//cout << "NKeys = " << iniFile.NumKeys() << endl;
	//cout << "number of data" << iniFile.NumValues("data") << endl;
	whatInfo =     iniFile.GetValue("InputOutput", "whatInfo") ;
	variableName = iniFile.GetValue("InputOutput", "variableName") ;
	cout << whatInfo << " and name of variable is " <<  variableName << endl;
	Pbar = iniFile.GetValueF("data", "Pbar") ;
	variableVaule[PbarN]=Pbar;
	cout << "Pbar" << Pbar << endl;
	cout << "vriableVaule" <<   variableVaule[PbarN] << endl;

//	iniFile.Path( "./iniTest.sav");
//	iniFile.WriteFile();

	Initialize();

	for ( unsigned valueID = 0; valueID < iniFile.NumValues( 0); ++valueID) {
		cout << "   ValueName = " << iniFile.ValueName( 0, valueID) <<
			"  Value = " << iniFile.GetValue( 0, valueID) << endl;
		switch (s_mapStringValues[iniFile.ValueName( 0, valueID)] ){
		
		
      case evStringValue1:
        cout << "Detected the first valid string." << endl;
        break;
      case evStringValue2:
        cout << "Detected the second valid string." << endl;
        break;
      case evStringValue3:
        cout << "Detected the third valid string." << endl;
        break;
      case evEnd:
        cout << "Detected program end command. "
             << "Programm will be stopped." << endl;
        return(0);
      default:
        cout << "'" << "test" << "' is an invalid string.s_mapStringValues now contains "
             << s_mapStringValues.size()
             << " entries." << endl;
        break;
		}
	}

//  Lookup what we have now.
	for ( unsigned keyID = 0; keyID < iniFile.NumKeys(); ++keyID) {
		cout << "Key = " << iniFile.KeyName( keyID) << endl;

		for ( unsigned valueID = 0; valueID < iniFile.NumValues( keyID); ++valueID) {
			cout << "   ValueName = " << iniFile.ValueName( keyID, valueID) <<
				"  Value = " << iniFile.GetValue( keyID, valueID) << endl;
		}
  }
  return 0;
}

void Initialize()
{
  s_mapStringValues["Pbar"] = evStringValue1;
  s_mapStringValues["Second Value"] = evStringValue2;
  s_mapStringValues["Third Value"] = evStringValue3;
  s_mapStringValues["end"] = evEnd;

  cout << "s_mapStringValues contains "
       << s_mapStringValues.size()
       << " entries." << endl;
}
