/********************************************************
	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/
#ifndef _DEFINITION_STDLIB
#define _DEFINITION_STDLIB

#  include "constant.h" 

struct property {
    PhysicalUnits   units;
	double value;
	int isKnown;
    property()   {
        units = __none ;
		value = 1.0; 
		isKnown = no;
    }
	~property(){}
	void setValue (double a) { value = a ; isKnown = yes;};
} ;

enum inputType{ _noV, machV, fldV, p2p1FLDV, angleV, nuV, M1ShockV, FLDShockV,
	M1fldV, M2fldV, M1fldP2P1V, deltaFldV, MxV, MyV, UpV,  cyclesV, T10V,
	MFLDshockLocationV, PbarV, AstarV, M1P2P1V, M2P2P1V, frequencyV,
	PARV, P0yP0xV, PyPxV, RyRxV, TbarV, TRstarV, TRzeroStarV, RRzeroStarV,
	deltaMV, deltaV, thetaMV, thetaV, P2P1V, fldP2P1V, T1V, T5V,L0V, L1V, L2V, 
	deltaThetaV, omegaV, RbarV, T0yT0xV, R0yR0xV, PRzeroStarV, 
	UxpV, UyOpenV, UypV, UsV, MxsV, MypV, MxpV, MypOpenV, AV, VdeltaV, 
	P0barV, T0barV, R0barV, P0V, TV, mDotV, mDotPT0V, P5P1V, VratioV,
	MxOpenV, MyOpenV, PbarOpenV, PyPxOpenV, kV, k1V, k5V, p2p1V, M1V, mDotP0TV,
	shockLocationV, UsOpenV, M1M2V, UypTxOV, FnBV, T0V, PV, P1V, P2V, LeV,
	M2V, TxV, TyV, TyTxV, MxGV, MxpMypPOV, MxpMyPOV, MxpMypPCV, UpTyV,
	MxMypPCV };

//what information is sought. Note different derived class yields different
//results 
enum outputType { infoStagnation, infoStandard, infoRange,  infoTube, 
	infoShock, infoTubeShock,  infoTubeProfile, infoFn, infomDot,
	infoTubeShockLimits, infoObliqueMax, infoObliqueMin,
	infoMinimal, infoD0 , infoMax, infoIteration };

enum outputFormat { plainT, TexT, LatexT, HtmlT }; 

//#   infoStagnation 920 // print standard (stagnation) info
//#   infoStandard 921 // standard info for (fanno, etc) 
//#   infoTube 922 // print tube sides for (fanno, etc) including shock
//#   infoShock 923 // print shock sides info 
//#   infoTubeShock 924 // print tube info shock main info
//#	  infoTubeProfile 925// the Mach number and pressure ratio profiles 
//#   infoTubeShockLimits 926 // print tube with shock extreme cases info 
//#   infoObliqueMax 927 // prameters associated max Oblique 
//#   infoObliqueMin 928 // prameters associated mininum Oblique 
//#   infoMinimal 929 // prameters associated mininal information 
//#   infoD0 930 // just to check D at delta 0  
//#   infoMax 931 // additional information i.e. prameters ratios    

// variables number 


enum PropertyType{Pp, Tp, mDAp, RHOp, UP, HP};

enum PressureUnitsTypes{ Bar, Pa, MPa, Psi};

const double Pconversion[4] = {100000., 1., 1000000., 6894.75729};
    
enum TemperatureUnitsTypes{ CTU, KTU, RTU, FTU};

    //double Pconversion[Bar] =1.;
    //double Pconversion[Pa] = 100000.;
    //double Pconversion[MPa] =  1000000.;
    //double Pconversion[Psi] =  14.5 ;

enum  InputType{kN, m1N, fldN, p2p1N, m2N, mN, shockLocationN,  MxN,
		MxpN,  MyN, MypN, PbarN, TbarN, TRstarN, TRzeroStarN, PRzeroStarN,
		AstarN, PARN,  P0yP0xN, R0yR0xN, thetaN, deltaMN, angleN,
		RbarN, UyN, UxN, UsN, TyTxN, TxN, T0yT0xN  };

//data info output in array
enum DataInfo { whatInfoN,	 variableNameN, isTexN,  isRangeN,  isHtmlN, whatRangeParameterN,
	classNameN, isBothBranchesN, isChokedN, isFLDknownN}	 ; // 

enum FlowName{ fannoFlow , stagnationFlow, rayleighFlow, isothermalFlow,
	  obliqueFlow, shockFlow, pmFlow, isothermalNozzleFlow,
		shockDynamicsFlow , shockTubeFlow, twoChambersFlow}; 

enum rangeParameter{Mrange, M1range, fldRange, Pbarrange};

//precition of the results 
extern int precision;

// variables number for the tube profile
extern int numberNodes ; // number of nodes in tube
extern int  fldP ;  // location of fld in the array 
extern int  MxP ;  ; // location of Mx in the array 
extern int  MyP ;  // location of My in the array 
extern int  PyPxP  ; // 
extern int  PxPsP  ; // 
extern int  PyPsP  ; // 
extern int  P1PiP  ; // 

#endif  /* _DEFINITION_STDLIB */
