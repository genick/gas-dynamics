/********************************************************
	file: shock.h

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/
#ifndef _SHOCK_STDLIB
#define _SHOCK_STDLIB

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip> 
#  include "definition.h"
#  include "compressibleFlow.h"
#  include "discontinuity.h"

class shock : public discontinuity {
        protected:
            void calMx () ;
        public:
			// to change the initialization later on
			// so that base class initialization will be considered
			shock(double spesificHeatRatio ):
				//initialization of base class
				discontinuity(spesificHeatRatio, shockFlow )
					 { 
						// specific initialization process for derived class
						//the critical M is ? for shock flow
						M1maxSub = 1.0;
						flowModelName = "Normal Shock";
						singularPoint = M1maxSub ; 
			  		};
			~shock(){};
			//spesific functions of this model of flow to calculate
			//spesific parameters of this flow model.
			// 
            void setMach(double Mach) {Mx = Mach ;};
			void calMach( int, double * ) ;
			void calMy() ;
			double calMy(double);
			void calPressureRatio() ;
			double calPressureRatio(double, double ) ; //PyPx f (Mx,My)
			void calDensityRatio();
			//void calVelocityRatio();
			void calTotalPressureRatio() ;
			void calTotalDensityRatio() ;
			double calTotalPressureRatio(double,double) ;
			void calTotalTemperatureRatio();
			void calTemperatureRatio();
			void calArea();
			void changeMx ( double mach) {Mx = mach; } ;
			void makeTexTableHead(int * ) ;
			void makeTexTableLine (int * ) ;
			void calMx ( int, double *) ;
			virtual int calAll( double *);
            int  doAllLines( int *, double *);
            int  calTableLine (inputType, double * );
            int  calTableLine (int*, double * );
            void showResults(int* );
            void showHead(int*) ;
			//html functions
			void makeHTMLTableHead(int * );
			void makeHTMLTableLine(int * );
			void makeHTMLTableTail(int *);

			//functions that do not touch the class variables
			//double calFLD(double) ;
			//double calPressureRatio(double) ;
			//double calDensityRatio(double);
			//double calVelocityRatio(double);
			//double calTotalPressureRatio(double) ;
			//double calTotalTemperatureRatio(double);
			//double calTemperatureRatio(double);
			//double calArea(double);
            //double getPressure(){return PyPx ; };
            //double getTemperatue (){return TyTx ; };
            //double getDensity (){return RyRx ; };
            //double getArea (){return Area ; };

};
#endif  /* _SHOCK_STDLIB */
