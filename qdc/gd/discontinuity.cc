/********************************************************
 * last change Mon Dec 19 09:59:04 CST 2011 cast conversion from string
 * constant to ‘char*’ 
**********************************************************************
	FILE:discontinuity.cc

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip>
#  include "stagnation.h"
#  include "externalFun.h"
#  include "discontinuity.h" 
#  include "gasDynamics.h" 
using namespace std;

/// discontinuity class definition 
//******************************************************************

double discontinuity::getDensity(){
	double tmpRyRx;
	tmpRyRx= RyRx ;
	return RyRx ;
}

double discontinuity::getPressure(){
	double tmpPyPx;
	tmpPyPx= PyPx ;
	return PyPx ;
}

void  discontinuity::calMach(inputType variableName, double * variableValue){
    if (variableName == MxV) {
        Mx = variableValue[MxV];
    }
};

void  discontinuity::calMx(inputType variableName, double * variableValue){
	double tmpMx;
    if (variableName == P0yP0xV) {
        P0yP0x = variableValue[P0yP0xV];
        M = root  ( calP0yP0xEstmt, this, M1maxSub, 100.0, P0yP0x) ;
		return;
    }
	else if (variableName == MxV){ 
		Mx = variableValue[MxV] ;
		calMy();
		if ( Mx < 1.0) {
			tmpMx = Mx ;
			Mx = My;
			My = tmpMx;
		}
	}
    else if (variableName == machV){
        Mx = variableValue[machV] ;
        calMy();
        if ( Mx < 1.0) {
            tmpMx = Mx ;
            Mx = My;
            My = tmpMx;
        }
    }
}

void  discontinuity::calMy(){
    My = sqrt((Mx*Mx + 2.0/(k -1.0)) /
        ((2.0*k / (k-1.0) ) *Mx*Mx -1.0)) ;
};

void discontinuity::calMx( ){
	double  tmpMy;
	tmpMy = My  ;
	Mx = My;
	calMy();
	Mx = My ;
	My = tmpMy; 
}

double  discontinuity::calMy(double m){
    return  sqrt((m*m + 2.0/(k -1.0)) / ((2.0*k / (k-1.0) ) *m*m -1.)) ;
};
                                                                                  

void  discontinuity::calPressureRatio(void){
    PyPx =  (1. + k*Mx*Mx) / (1. + k*My*My) ;
};

double   discontinuity::calPressureRatio(double MMx, double MMy){
    return  (1. + k*MMx*MMx) / (1. + k*MMy*MMy) ;
};

void  discontinuity:: calDensityRatio (){
    UxUy = RyRx = PyPx/TyTx;
};
                                                                                
void  discontinuity::calTotalPressureRatio(){
    P0yP0x =  (PyPx)*
    pow((1 + ((k-1)/2)*My*My),(k/(k-1))) /
    pow((1 + ((k-1)/2)*Mx*Mx),(k/(k-1))) ;
};

double  discontinuity::calTotalPressureRatio(double MMx ){
	double MMx2, ppp;
	MMx2 = MMx*MMx;
	ppp = pow( ((1. + k) *MMx2/ ((k - 1.)* MMx2+2.0)) ,(k/(k-1.)))* 
    pow(((1. + k)/(2.*k*MMx2-(k-1) ) ),(1./(k-1.))) ;
	return ppp;
};


void  discontinuity::calTemperatureRatio(){
    TyTx = PyPx *PyPx * My*My/Mx/Mx;
};
                                                                                
void  discontinuity::calArea(){
    Area =  pow((( 1 + (( k - 1) / 2 ) * Mx*Mx)/((k+1)/2)),
            ((k+1) / 2/(k-1) ))
                / Mx;
};

void    discontinuity::makeTexTableHead(int * ioData) {
                                                                               
    int variableNumber;
                                                                               
    if (outputInfo == infoStandard || outputInfo == infoShock) {
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "M_x";
        variableNames[1] = (char *) "M_y";
        variableNames[2] = (char *) "T_y \\over T_x";
        variableNames[3] = (char *) "\\rho_y \\over \\rho_x";
        variableNames[4] = (char *) "P_y \\over P_x";
        variableNames[5] = (char *) "{P_0}_y \\over {P_0}_x";
                                                                               
        showVariableTeXHead(variableNames,variableNumber);
    }
}

void    discontinuity::makeTexTableLine(int * ioData) {
    int numberVariables;

    if (outputInfo == infoStandard || outputInfo == infoShock) {
        numberVariables = 6;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx ;
        variableToBePrinted[1] = My;
        variableToBePrinted[2] = TyTx;
        variableToBePrinted[3] = RyRx;
        variableToBePrinted[4] = PyPx ;
        variableToBePrinted[5] = P0yP0x;

        showVariableTeXLine (variableToBePrinted, numberVariables);
    }
}

int   discontinuity::calAll(inputType variableName,
				 double * variableValue ){
    calMx(variableName, variableValue);
	calAreaRatio();
	if (variableName != PbarV){ 
		calPressureRatio();
	}
	if (variableName != TbarV){
		calTemperatureRatio();
	}
    //if (variableName != TbarV){
		calTotalPressureRatio();
	//}
    //if (variableName != TrV){
		calDensityRatio ();
	//}
	return yes;
}

int discontinuity:: calTableLine (
		inputType variableName, double * variableValue){
    if (outputInfo == infoStandard ){
            return calAll(variableName,variableValue);
    }
    return no;
};


int   discontinuity::doAllLines ( int * ioData, double * variableValue ){
    makeLines(ioData, 7,0.03,  0.1,  variableValue);
    makeLines(ioData, 16,0.2,  1.0,  variableValue);
    makeLines(ioData, 18, 1.1, 2.9,  variableValue);
    makeLines(ioData, 14,3.,10  ,  variableValue );
    makeLines(ioData, 10,20.,70  ,  variableValue );
    return yes;
}

