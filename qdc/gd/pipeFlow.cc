// last change Mon Feb  4 09:46:51 CST 2013 unchock new algarithem for given P2/P1 and fld
//////////////////////////////////////////////////////////////////////////////////////////
/********************************************************
	FILE:pipeFlow.cc

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip>
#  include "stagnation.h"
#  include "externalFun.h"
#  include "pipeFlow.h" 
#  include "gasDynamics.h" 
using namespace std;

// pipeFlow functions 
//****************************************************************
void  pipeFlow::calMach(inputType variableName, double * variableValue){
	if (variableName == machV) {
		M = variableValue[machV];
		isFlowReverse = no;
		isFLDknown = no;
	}
	else if (variableName == M1fldV ) {
		fld = variableValue[fldV] ;
		M1 = variableValue[M1V];
		isFLDknown = true ;
	}
	else if (variableName == M2fldV ) {
		fld = variableValue[fldV] ;
		M2 = variableValue[M2V];
		isFLDknown = true ;
	}
	else if (variableName == deltaFldV ) {
		//for choked flow the pressure ratio change 
		//initial fld = 0.1
		double deltaFLD;
		outputInfo =infoIteration;
		deltaFLD = fld  = variableValue[fldV];
		for ( int  i = 0; i < 400 ; i++ ) {
			fld += 0.01;	
			M1 = calMForFLD(fld, 0.5);
			calM2forM1(deltaFLD, M1, &M2);
			steps[i][0] = fld ;
			steps[i][1] = M1 ;
			steps[i][2] = M2 ;
			steps[i][3] = calP2P1(M1, M2) ;
			steps[i][4] = deltaFLD ;
			numberOfIterations = i ;
		} 
	}
	else if (variableName == M1P2P1V ) {
		p2p1 = variableValue[P2P1V];
		M1 = variableValue[M1V];
		P1Pstar = calPressureRatio( M1);
		P2Pstar = p2p1 * P1Pstar ;
		M2 = brent ( calPbarEstmt, this, EPS, M1maxSub, P2Pstar) ;
		double fld1, fld2;
		fld1 = calFLD(M1);	
		fld2 = calFLD(M2);	
		fld = fld1 -fld2;
	}
	else if (variableName == M2P2P1V ) {
		p2p1 = variableValue[P2P1V];
		M2 = variableValue[M2V];
		P2Pstar = calPressureRatio( M2);
		P1Pstar = P2Pstar / p2p1  ;
		if (P1Pstar <= 1.0 ) {
			M1 = -1.0;
			M = -1.;
			fld = -1.0;
		}
		M1 = brent ( calPbarEstmt, this, EPS, M1maxSub, P1Pstar) ;
		double fld1, fld2;
		fld1 = calFLD(M1);	
		fld2 = calFLD(M2);	
		fld = fld1 -fld2;
	}
	else if (variableName == fldP2P1V ) {
		double  P2P1p, P2Pstarp, P2P1min, deltaFLD, givenFLD ;
		isFLDknown = true;
		int i = 0 ; //number of iterations 
		givenFLD = variableValue[fldV];
		P2P1 = variableValue[P2P1V];
		isFLDknown = true;
		// first guess deltaFLD f(P2P1)  choked flow
		P2Pstarp = P2Pstar = 1.0/P2P1;
		M2 = brent ( calPbarEstmt, this, EPS, M1maxSub, P2Pstar) ;
		//deltaFLD is also currently total fld for p2p1 
		deltaFLD = calFLD(M2);
		fld = givenFLD + deltaFLD ;
		M1 = calMForFLD(fld, 0.5);
		P2P1min = 1.0/calPressureRatio( M1);
		if  (false ){// oldstuff  ( givenFLD > deltaFLD) { //not choked
			M2 = brent ( calPbarEstmt, this, EPS, M1maxSub, P2Pstarp) ;
			deltaFLD = calFLD(M2);
			double Error = 1.;
			while (Error > EPS){
				// new guess
				P2P1p = calP2P1(M1, M2) ;
				deltaFLD = deltaFLD *  P2P1/P2P1p ; 
				M2 = calMForFLD(deltaFLD, 0.5);
				fld = givenFLD + deltaFLD;
				M1 =  calMForFLD(fld, 0.5);
				Error = abs (P2P1 - P2P1p) ;
				steps[i][0] = M1 ;
				steps[i][1] = M2 ;
				steps[i][2] = fld ;
				steps[i][3] = calP2P1(M1, M2) ;
				steps[i][4] = deltaFLD ;
				i++;
			}
			p2p1 = P2P1 ; //calP2P1(M1, M2) ;
			numberOfIterations = i ;
			return;
		}
		else if ( true ){ //  givenFLD < deltaFLD ){
			if ( calM1forFLDp2p1 (variableValue[fldV],variableValue[p2p1V]) == false){
			//impossible ratio below the minimum
				//M1 = -1. ; M2 = -1. ;
				//outputErrorMessage = "The pressure ratio causes choked flow";
				return;
			}
			else {
				return ;
			}
		}
		else if (  givenFLD == deltaFLD ) {
			//the flow is choked and possible
			M2 = 1.; 
			isFLDknown = false ;
			outputErrorMessage = 
					"This pressure ratio causes exactly the flow to choke";
			return;
		}
	}
	else if (variableName == fldV) {
		fld = variableValue[fldV] ;
		if (isChoked == no) {
			M1 = variableValue[M1V] ; // supply from the range 
			fld1 = calFLD(M1) ;  
			fld2 = fld1-fld ;  
			M2 = calMForFLD(fld2, 0.2) ;
			P1P2 = calP2P1(M1,M2); 
			return ;
		}	
		M = calMForFLD(variableValue[fldV],variableValue[machV]) ;
		if ( M > 0 && M <= M1maxSub){
			isFLDknown= yes;
			isSubSonic=yes;
			isFlowReverse = no;
		}
	}
	else if (variableName == PRzeroStarV) {
		PRzeroStar = variableValue[PRzeroStarV];
		M = variableValue[machV];
		if ( M > 1. ) 
        	M = brent(calPRzeroStarEstmt,this,M1maxSub,300.,PRzeroStar);
		else  if (M < 1. ) 
        	M = brent(calPRzeroStarEstmt,this,.00001, M1maxSub,PRzeroStar);
		else 
			M = 1.0;
    }
    else if (variableName == PbarV) {
		Pbar = variableValue[PbarV];
		if (Pbar > 1. ) 
        	M = brent  ( calPbarEstmt, this, .00001,  M1maxSub, Pbar) ;
		else  if (Pbar < 1. ) 
        	M = brent  ( calPbarEstmt, this,  M1maxSub, 400., Pbar) ;
		else 
			M = 1.0;
    }
    else if (variableName == TbarV) {
        Tbar = variableValue[TbarV];
		if  ( Tbar > 0.5*(k +1))  {// no solution possible
			M = - 1.0;
			return ; 
		}
		else if ( Tbar == 1.2 )
			M = 0. ;
		else if (Tbar > 1. )
        	M = brent  ( calTbarEstmt, this, .0000,  M1maxSub, Tbar) ;
        else  if (Tbar < 1. )
            M = brent  ( calTbarEstmt, this,  M1maxSub, 400., Tbar) ;
        else // == 1.0 
            M = 1.0;
	}
	else if (variableName == RbarV) {
		Rbar = variableValue[RbarV];
		if  ( Rbar < sqrt((k-1.)/(k+1.)) )  {// no solution possible
			M = - 1.0;
			return ;
		}
		else if (Rbar > 1. )
			M = brent  ( calRbarEstmt, this, EPS,  M1maxSub, Rbar);
		else  if (Rbar < 1. )
			M = brent  ( calRbarEstmt, this,  M1maxSub, 400., Rbar) ;
		else // == 1.0
			M = 1.0;
	}
	else if (variableName == TRstarV) {
		TRstar = variableValue[TRstarV];
		M = brent ( calTRstarEstmt, this, .00001,  M1maxSub, TRstar) ;
	}
	else if (variableName == TRzeroStarV) {
		TRzeroStar = variableValue[TRzeroStarV];
		M = brent ( calTRzeroStarEstmt, this, EPS,  500., TRzeroStar) ;
   }
	else if (variableName == M1ShockV) {
		M1 = variableValue[M1V];
		fld = variableValue[fldV] ;
	}
};

void  pipeFlow::calM1(int variableName, double * variableValue){
	if (variableName == M1fldV){
		M1 =   variableValue[M1V];
		fld = variableValue[fldV];
	}
	else if (variableName == p2p1FLDV){
        fld = variableValue[fldV];
		p2p1 = variableValue[p2p1V];
 		if (calM1forFLDp2p1(variableValue[fldV],variableValue[p2p1V]) == yes){
		}
	}
	else if (variableName ==  M1ShockV) {
        M1 = variableValue[M1V];
        fld = variableValue[fldV] ;
	}
};


int pipeFlow::calM1forFLDp2p1(double fld, double P2, double P1){
		bool reverseFlow = false ;
		if (P2 > P1){
			reverseFlow = true;
			double tmp ;
			tmp = P1; P1 = P2; P2 = tmp ;
		}
		P2P1 = P2/P1;
		if  (  P2 == P1  ) {
			M = Mout = Min = Mbar = 0. ; 
			return true;
		}
		else if  ( ( P1 - P2 ) < 0.02 ) {
			Msmall = sqrt((1.0 - P2/P1) /k / fld) ; 
			Mout = Min = Mbar = Msmall /Mmax ;
			fld = calFLD(Mbar);
			return true ;
		}
		else{
			return calM1forFLDp2p1(fld,P2P1);
		}
};

int pipeFlow::calM1forFLDp2p1(double fld, double P2P1){
		bool reverseFlow = false ;
		double fld_a, fld_b, Psmalla, Psmallb, Mb, Ma, Pa, Pb, Pright, Pleft; 
		calMForFLD(fld, 0.5) ;
		int i = 0 ;
		Mmax = M ; 
		Msmall = 0.01; // to overlap with the 0.02 (see) down 
		if  (  P2P1 == 1 ) {
			M = Mout = Min = Mbar = 0. ; 
			numberOfIterations = i ;
			goto outNow ;
		}
		else if  ( P2P1  < 0.02 ) {
			Msmall = sqrt((1.0 - P2P1) /k / fld) ; 
			Mout = Min = Mbar = Msmall /Mmax ;
			fld_a = calFLD(Mbar);
			numberOfIterations = i ;
			goto outNowA;
		}  
		else {
		double  P1n, newMi, oldP2=1.0;
		//double oldPrCal;
		double Pll = 1.0, Mll = 1.0, Prr = 1., Mrr = 1. ;
		//the iteration the M is on the right, Msmall is on the left.
		Ma = newMi = Mmax ;
		Pright = P1n = 1./calPressureRatio( newMi ); // also limit pressure above which flow is chocked
		oldP2 = 1.0 ;
		Pleft = 1.0 ; // to change when P2 changes
		if ( P2P1 < Pright ) { // the flow is chocked
			isChoked = yes ;
			Mbar = Mmax ;
			goto outNow ;
		}
		//oldPrCal = P1n / oldP1 ; // the pressure ratio between the
		// check the lefthand side. what is the pressure for that M according the full equations.
		fld_a = calFLD(Msmall);
		fld_b = fld_a - fld ;
		Ma = Msmall ;
		Mb = calMForFLD (fld_b, 0.2) ;
		Pa = Psmalla = calPressureRatio( Ma );
		Pb = Psmallb =  calPressureRatio (Mb) ;
		Pll = Pb / Pa - P2P1 ;
		Prr = Pright - P2P1;
		Mll = Msmall ;
		Mrr = Mmax ;
		// guess new value of M			
		double Mc = 0.;
		double Md = numeric_limits<double>::max( ) ;

    //double fa = function(a);
    //double fb = function(b);

		double Pc = 0.;
		double Ms = 0.;
		double Ps = 0.;
			// if |f(a)| < |f(b)| then swap (a,b) end if
	if (abs(Pll) < abs(Prr))
		{ double tmp = Mll; Mll = Mrr; Mrr = tmp; tmp = Pll; Pll = Prr; Prr = tmp; }
		Mc = Mll;
		Pc = Pll;
		bool mflag = true;
		int i = 0;
		while (!(Prr==0) && (abs(Mll-Mrr) > EPS)){
			if ((Pll != Pc) && (Prr != Pc)) // Inverse quadratic interpolation
				Ms = Mll * Prr * Pc / (Pll - Prr) / (Pll - Pc) + 
					Mrr * Pll * Pc / (Prr - Pll) / (Prr - Pc) + 
					Mc * Pll * Prr / (Pc - Pll) / (Pc - Prr);
			else // Secant Rule
				Ms = Mrr - Prr * (Mrr - Mll) / (Prr - Pll);

			double tmp2 = (3. * Mll + Mrr) / 4.;
			if(
				(!(((Ms > tmp2) && (Ms < Mrr)) || ((Ms < tmp2) && (Ms > Mrr)))) ||
				(mflag && (abs(Ms - Mrr) >= ( 0.5*abs(Mrr - Mc) ))) ||
				(!mflag && (abs(Ms - Mrr) >= 0.5*(abs(Mc - Md) )))
			)
			{
				Ms = 0.5*(Mll + Mrr) ;
				mflag = true;
			}
			else
			{
				if ((mflag && (abs(Mrr - Mc) < EPS)) || (!mflag && (abs(Mc - Md) < EPS)))
				{
					Ms = 0.5*(Mll + Mrr) ;
					mflag = true;
			}
				else
					mflag = false;
			}
			fld_a = calFLD(Ms); // function calculations
			fld_b = fld_a - fld ;
			Mb = calMForFLD (fld_b, 0.2) ;
			Pa = calPressureRatio( Ms );
			Pb = calPressureRatio (Mb) ;
			Ps = Pb / Pa - P2P1 ;
			Md = Mc;
			Mc = Mrr;
			Pc = Prr;

			if (Pll * Ps < 0) { Mrr = Ms; Prr = Ps ; }
			else { Mll = Ms; Pll = Ps; }
        // if |f(a)| < |f(b)| then swap (a,b) end if
			if (abs(Pll) < abs(Prr))
			{ double tmp = Mll; Mll = Mrr; Mrr = tmp; tmp = Pll; Pll = Prr; Prr = tmp; }
			steps[i][0] =  Ms ; // M1
			steps[i][1] = Mb ; //  M2
			steps[i][2] = Pb/Pa ; // pressure ratio 
			steps[i][3] = fld_a ; // fld
			steps[i][4] = fld_b ; // delta fld
			steps[i][5] = Mrr-Mll ; // Mach number range 
			steps[i][6] = Ps ; // Mach number range 
			i++;
			numberOfIterations = i ;
			if (i > 1000) {
				cout <<  Mrr << endl ;
				return true ;
			}
		}
		Mbar = Mrr/Mmax ;
	goto outNowA;
	}
	outNowA:
	// calculate Min as well	
	Min = calMForFLD((fld_a + fld),0.5); 
	outNow:
	if ( reverseFlow == true ){
		//double tmp ;
		//tmp = P1; P1 = P2; P2 = tmp ;
		Mout = Mbar = - Mbar ; reverseFlow = false ;	
		return false ;
	}
	else if ( isChoked  == true ) {
		M1 = Mmax ;
		M2 = singularPoint ; 
		return true ; 
	}
	else {
		M1 = Ma ; // the flow is chocked
		Mout = Mbar;
		return true; 
	}
};

double  pipeFlow :: calP2P1(double M1, double M2){
	if (M2 > singularPoint){
		return -1.;
	}
	else if ( M1 >= M2) {
		return -1.;
	}
	else{
		M = M1;
		calPressureRatio();
		P1Pstar = Pbar; 
		M = M2;
		calPressureRatio();
		P2Pstar = Pbar;
		return  P2Pstar/P1Pstar;
	}
}

double  pipeFlow :: calMForFLD(double FLD, double mach){
    //what to do when there is no solution ?
    if ((mach < singularPoint) && (FLD > EPS ) ) {// sub sonic flow
        return brent ( calFLD_Estmt, this, .00001, (M1maxSub-EPS), FLD);
    }
    else if ((mach > singularPoint )  && (FLD > EPS ) ){//supersonic flow
        return  brent ( calFLD_Estmt, this, (M1maxSub+EPS), 10000., FLD );
    }
    else if (FLD == 0.0 )  {
        return 0.0;
    }
    else { // no possible negative fld
        return -1.0;
    }
};

int   pipeFlow :: doAllLines ( int * ioData, double * variableValue ){

	double startP, endP,  sp;
	sp = singularPoint;
	//int SingP = 1;
	double range [500];

	if (isChoked == no){
		range[1] = 99. ;
		range[2] =  0.01 ;
		if (isFLDknown == yes){
			range[3] = calMForFLD (variableValue[fldV], 0.2 ) ;
		}
		else {
			range[3] = Mmax ;
		}
		range[0] = double (1);
	}
	else {
		startP =  0.01;
		endP  = 0.1;
		//this is the subsonic branch
		range[1] = 99. ;
		range[2] =  0.01 ;
		range[3] = sp ;
		//this is the supersonic branch between 1 and 5
		range[4] = 399. ;
		range[5] = sp + 0.01 ;
		range[6] = sp + 4. ;
		range[7] = 249. ;
		range[8] = 5.02 ;
		range[9] = 10.0 ;
		range[10] = 399. ;
		range[11] = 10.1 ;
		range[12] = 50.0 ;
		
		range[0] = double (4);
		isFLDknown=no;
	}	
	setRange( ioData, range, variableValue);
	return yes;

}
