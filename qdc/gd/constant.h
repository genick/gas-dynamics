/********************************************************
    This code is copyrighted by Genick Bar_Meir
    and has no warranty of any kind. If fact it was know to
	get computer turn to Mochelm and it will try to kill you.
    Use this code at your own risk.

    This program is open source under the same license as
    all the others material in Potto project.
	Either gpl, or Potto License.
    By: the horrible Genick Bar-Meir

    Description:
	definitions for  some constants

********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip>
#	ifndef _DEFINITION_CONSTANTS_LIB
#	define _DEFINITION_CONSTANTS_LIB

	
const	double	PI= 3.1415926535897932384626433832795028 ;
const	double	infinity=1.79769313486231570E+308;
const  	double	EPS =1e-6;
const	double	THIRD=0.33333333333333333333333333333333333333333;
const	int		yes=1;
const	int		no=0;
const	int		sonic=3; // Mach = 1

//enum answers{no=0, yes=1, sonic}  ;

enum PhysicalUnits{// unitless
			__none, 
			// standanrd (almost unitless)
			standU,
			// pressure units
			barP, PaP,
			// temperature units
			KT,CT, RT, FT,
			// density units
			kgm3R, lbmft3R,
			// Cp units
			kJkgK }; 
//const 	int 	precision =6;


#endif  /* _DEFINITION_CONSTANTS_LIB */

