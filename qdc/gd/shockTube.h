//last change
//initial Tue Dec 20 10:21:51 CST 2011
/********************************************************
	file: shockTube.h

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/
#ifndef _SHOCKTUBE_STDLIB
#define _SHOCKTUBE_STDLIB

#  include <cstdio>
#	include <cstdlib>
#  include <cmath>
#  include <iostream>
#  include <iomanip> 
#  include "definition.h"
#  include "compressibleFlow.h"

class shockTube: public  discontinuity {
        protected:
			//local functions and veriables to be access via other
			//functions 
			int  isDknown;
			//double Mxp, Myp, Msy, Msx, Ms1  ;
        public:
			double P5P1, Us1, Us3, Us4, Us5, T2T1, P5P3, T5T3, Pc_time ;
			//property T3;

			// to change the initialization later on
			// so that base class initialization will be considered
			shockTube(double spesificHeatRatio ):
				//initialization of base class
				discontinuity (spesificHeatRatio, shockTubeFlow )
					 { 
						//specific initialization process for derived class
						M1maxSub = 1.0;
						flowModelName = "Shock Tube";
						singularPoint = M1maxSub ; 
						isDknown = no;
			  		};
			shockTube(double k1 , double k5):
				//initialization of base class
				discontinuity (k1, k5, shockTubeFlow )
					 { 
						//specific initialization process for derived class
						M1maxSub = 1.0;
						flowModelName = "Shock Tube";
						singularPoint = M1maxSub ; 
						isDknown = no;
			  		};
			~shockTube(){};
			//spesific functions of this model of flow to calculate
			//spesific parameters of this flow model.
			// 
			void setMach(double Mach) {Mx = Mach ;};
			void setMs1(double Mach) {Ms1 = Mach ;};
			void calMach( inputType, double * ) ;
			void calMy() ;
			double calMy(double);
			void makeTexTableHead(int * ) ;
			void makeTexTableLine (int * ) ;
			void calMx (inputType, double *) ;
			void calT2T1();
			void calMx () ; //for given Mxp
			int calMs1(); //calculate the Mach moving to the left 
			int calAll( double *);
			int doAllLines( int *, double *);
			int calTableLine ( inputType, double * );
			int calProfile ( int *, double * );
			int calTableLine ( int *, double * );
			void showResults(int *);
			void showHead(int *) ;
			inputType selectVariable (inputType)  ;

			//html functions
			void showVariableHTMLHead(char **, int);
			void makeHTMLTableHead(int *);
			void makeHTMLTableLine(int *);
			//functions that do not touch the class variables
			//double calFLD(double) ;
			//virtual double calPressureRatio() ;
			virtual void calP5P1() ;
			virtual void calP5P3() ;
			virtual void calUs(double *) ;
			virtual void calUs1(double *) ;
			virtual void calPressureRatio() ;
			double getP5P1(){return P5P1;};
			double getPressure(){return Pbar;};
			double calUpperLimitsMs1 ();
};
#endif  /* _SHOCKTUBE_STDLIB */
