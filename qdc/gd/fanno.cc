// last change Mon Feb  4 09:46:51 CST 2013 unchock new algarithem for given P2/P1 and fld
//////////////////////////////////////////////////////////////////////////////////////////
// Mon Dec 19 17:38:22 CST 2011 cast conversion from string
// constant to ‘char*’
// Mon Dec  4 08:57:04 CST 2006 remove whatInfo passing use as class
// veriable and add the iteration to the calculation of supersonic branch
// to find the shock location. Therefore correcting the pressure
// calculations and temperature.

/********************************************************
	FILE:fanno.cc

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/
#  include <cstdlib>
#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip>
#  include "stagnation.h"
#  include "externalFun.h"
#  include "fanno.h" 
#  include "definition.h"
#  include "compressibleFlow.h"
#  include "gasDynamics.h" 

using namespace std;


//***************************************************************************
//fanno class functions definitions 
//******************************
void 	fanno::makeTexTableLine(int * ioData) {
    int numberVariables;

	if (outputInfo == infoStandard) {
		numberVariables = 7;
		double variableToBePrinted[numberVariables];
		if ( (ioData[variableNameN] == fldP2P1V ) ||
			(ioData[variableNameN] ==  M1P2P1V )  ||
			(ioData[variableNameN] ==  M2P2P1V ) ) {
			numberVariables = 4;
			double variableToBePrinted[numberVariables];
			variableToBePrinted[0] = M1;
			variableToBePrinted[1] = M2;
			variableToBePrinted[2] = fld;
			variableToBePrinted[3] = p2p1;
			showVariableTeXLine (variableToBePrinted, numberVariables);
		} 
		else if (isBothBranches == no) {
			variableToBePrinted[0] = M ;
			variableToBePrinted[1] = fld;
			variableToBePrinted[2] = PRstar;
			variableToBePrinted[3] = PRzeroStar;
			variableToBePrinted[4] = RRstar ;
			variableToBePrinted[5] = URstar;
			variableToBePrinted[6] = TRstar;
			showVariableTeXLine (variableToBePrinted, numberVariables);
		}
		else if (isBothBranches == yes) {
			for (int i = 0 ; i < 2 ; i++){
				for (int j = 0 ; j < 6 ; j++){
					variableToBePrinted[j] =  steps[i][j];
				}
				showVariableTeXLine (variableToBePrinted, numberVariables);
			}
		}
	}
	else if (outputInfo == infoTube) {
		if (isSubSonic == no){
			numberVariables = 6;
			double variableToBePrinted[numberVariables];
			variableToBePrinted[0] = M1;
			variableToBePrinted[1] = M2;
			variableToBePrinted[2] = fld;
			variableToBePrinted[3] = fld1;
			variableToBePrinted[4] = fld2;
			variableToBePrinted[5] = p2p1;
			showVariableTeXLine (variableToBePrinted, numberVariables);
		}
		else {
			numberVariables = 4;
			double variableToBePrinted[numberVariables];
			variableToBePrinted[0] = M1;
			variableToBePrinted[1] = M2;
			variableToBePrinted[2] = fld;
			variableToBePrinted[3] = p2p1;
			showVariableTeXLine (variableToBePrinted, numberVariables);
		}
	}
	else if (outputInfo == infoTubeShock) {
		numberVariables = 6;
		double variableToBePrinted[numberVariables];
		variableToBePrinted[0] = M1;
		variableToBePrinted[1] = M2;
		variableToBePrinted[2] = fldUp;
		variableToBePrinted[3] = fldDown;
		variableToBePrinted[4] = Mx;
		variableToBePrinted[5] = My;
		showVariableTeXLine (variableToBePrinted, numberVariables);
	}
	else  if (outputInfo == infoIteration ) {
		numberVariables = 5;
		double variableToBePrinted[numberVariables];
		if (ioData[variableNameN] == fldP2P1V ) {
			for (int i = 0 ; i < numberOfIterations ; i++){
				for (int j = 0 ; j < 4 ; j++){
					variableToBePrinted[j] = steps[i][j];
				}
				variableToBePrinted[4] = fld;
				cout  << "  " << (int)  steps[i][0] << " &  ";
				showVariableTeXLine (variableToBePrinted, numberVariables);
			}
		}
		else {
			for (int i = 0 ; i < numberOfIterations ; i++){
				variableToBePrinted[0] = steps[i][1];
				variableToBePrinted[1] = steps[i][2];
				variableToBePrinted[2] = steps[i][3];
				variableToBePrinted[3] = steps[i][4];
				variableToBePrinted[4] = fld;
				cout  << "  " << (int)  steps[i][0] << " &  ";
				showVariableTeXLine (variableToBePrinted, numberVariables);
			}
		}
	}
};

void 	fanno::makeTexTableHead(int * ioData) {

    int variableNumber;

	if (outputInfo == infoStandard) {
		if ( (ioData[variableNameN] == fldP2P1V ) ||
			(ioData[variableNameN] ==  M1P2P1V )   ||
			(ioData[variableNameN] ==  M2P2P1V ) ) {
			int variableNumber = 4;
			char * variableNames[variableNumber];
			variableNames[0] = (char *) "M_1  ";
			variableNames[1] = (char *) "M_2  ";
			variableNames[2] = (char *) "4fL \\over D";
			variableNames[3] = (char *) "P_2 \\over P_1";
			cout << "<html>" << endl;
			showVariableTeXHead(variableNames,variableNumber);
		} 
		else {
			variableNumber = 7;
			char * variableNames[variableNumber];
			variableNames[0] = (char *) "M";
			variableNames[1] = (char *) "4fL \\over D";
			variableNames[2] = (char *) "P \\over P^{*}";
			variableNames[3] = (char *) "P_0 \\over {P_0}^{*}";
			variableNames[4] = (char *) "\\rho \\over \\rho^{*}";
			variableNames[5] = (char *) "U \\over {U}^{*}";
			variableNames[6] = (char *) "T \\over T^{*}";

			showVariableTeXHead(variableNames,variableNumber);
		}
	}
	else if (outputInfo == infoTube) { 
		if (isSubSonic == no){
			int variableNumber = 6;
			char * variableNames[variableNumber];
			variableNames[0] = (char *) "M_1";
			variableNames[1] = (char *) "M_2";
			variableNames[2] = (char *) "4fL \\over D";
			variableNames[3] = (char *) "\\left.{ 4fL \\over D }\\right|_{1}";
			variableNames[4] = (char *) "\\left.{ 4fL \\over D }\\right|_{2}";
			variableNames[5] = (char *) "P_2 \\over P_1";
			showVariableTeXHead(variableNames,variableNumber);
		}
		else {
			int variableNumber = 4;
			char * variableNames[variableNumber];
			variableNames[0] = (char *) "M_1";
			variableNames[1] = (char *) "M_2";
			variableNames[2] = (char *) "4fL \\over D";
			variableNames[3] = (char *) "P_2 \\over P_1";
			showVariableTeXHead(variableNames,variableNumber);
		}
	}
	else if  (outputInfo == infoTubeProfile) {
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "M_1";
        variableNames[1] = (char *) "M_2";
        variableNames[2] = (char *) "4fL \\over D";
        variableNames[3] = (char *) "\\left.{ 4fL \\over D }\\right|_{1}";
        variableNames[4] = (char *) "\\left.{ 4fL \\over D }\\right|_{2}";
        variableNames[5] = (char *) "P_2 \\over P_1";
                                                                               
                                                                               
        showVariableTeXHead(variableNames,variableNumber);
    }
    else if  (outputInfo == infoTubeShock) {
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "M_1";
        variableNames[1] = (char *) "M_2";
        variableNames[2] = (char *) "\\left.{ 4fL \\over D }\\right|_{up}";
        variableNames[3] = (char *) "\\left.{ 4fL \\over D }\\right|_{down}";
        variableNames[4] = (char *) "M_x";
        variableNames[5] = (char *) "M_y";

        showVariableTeXHead(variableNames,variableNumber);
    }
    else if  (outputInfo == infoIteration) {
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "i";
        variableNames[1] = (char *) "\\left.{ 4fL \\over D }\\right|_{up}";
        variableNames[2] = (char *) "\\left.{ 4fL \\over D }\\right|_{down}";
        variableNames[3] = (char *) "M_x";
        variableNames[4] = (char *) "M_y";
        variableNames[5] = (char *) "{ 4fL \\over D }";

        showVariableTeXHead(variableNames,variableNumber);
    }
};

void	fanno::showHead(int *ioData){
	
	cout << endl << endl <<  "Fanno flow" << "\t\t"  ;
	cout << "k = " << k << endl << endl ;

	if (outputInfo == infoStandard || outputInfo == infoMinimal) {
		if ( (ioData[variableNameN] == fldP2P1V ) ||
			(ioData[variableNameN] ==  M1P2P1V ) ||
			(ioData[variableNameN] ==  M2P2P1V ) ) {
			int variableNumber = 4;
			char * variableNames[variableNumber];
			variableNames[0] = (char *) "M1  ";
			variableNames[1] = (char *) "M2  ";
			variableNames[2] = (char *) "fld ";
			variableNames[3] = (char *) "P2P1";
			showVariableHead( variableNames, variableNumber);
		} 
		else if ( ioData[variableNameN] == fldV ) {
			int variableNumber = 5;
			char * variableNames[variableNumber];
			variableNames[0] = (char *) "M1  ";
			variableNames[1] = (char *) "M2  ";
			variableNames[2] = (char *) "fld ";
			variableNames[3] = (char *) "P2P1";
			variableNames[4] = (char *) "d(fld)";
			showVariableHead( variableNames, variableNumber);
		}
		else {
			int variableNumber = 7;
			char * variableNames[variableNumber];
			variableNames[0] = (char *) "M  ";
			variableNames[1] = (char *) "fld";
			variableNames[2] = (char *) "P/P*";
			variableNames[3] = (char *) "P0/P0*";
			variableNames[4] = (char *) "R/R*";
			variableNames[5] = (char *) "U/U*";
			variableNames[6] = (char *) "T/T*";
			showVariableHead( variableNames, variableNumber);
		}
	}
	else if ( outputInfo == infoTube)  {
		if (isSubSonic == no){
			int variableNumber = 6;
			char * variableNames[variableNumber];
			variableNames[0] = (char *) "M1 ";
			variableNames[1] = (char *) "M2 ";
			variableNames[2] = (char *) "fld";
			variableNames[3] = (char *) "fld1";
			variableNames[4] = (char *) "fld2";
			variableNames[5] = (char *) "p2p1";
			showVariableHead( variableNames, variableNumber);
		}
		else {
			int variableNumber = 4;
			char * variableNames[variableNumber];
			variableNames[0] = (char *) "M1 ";
			variableNames[1] = (char *) "M2 ";
			variableNames[2] = (char *) "fld";
			variableNames[3] = (char *) "p2p1";
			showVariableHead( variableNames, variableNumber);
		}
	}
	else if ( outputInfo == infoShock) {
		int variableNumber = 6;
		char * variableNames[variableNumber];
		variableNames[0] = (char *) "Mx";
		variableNames[1] = (char *) "My";
		variableNames[2] = (char *) "TyTx";
		variableNames[3] = (char *) "RyRx";
		variableNames[4] = (char *) "PyPx";
		variableNames[5] = (char *) "P0yP0x";
		showVariableHead( variableNames, variableNumber);
	}
	else  if (outputInfo == infoIteration ) {
		if (ioData[variableNameN] == fldP2P1V ){
			int variableNumber = 8;
			char * variableNames[variableNumber];
			variableNames[0] = (char *) "i ";
			variableNames[1] = (char *) "M1 ";
			variableNames[2] = (char *) "M2";
			variableNames[3] = (char *) "P2P1";
			variableNames[4] = (char *) "fld";
			variableNames[5] = (char *) "deltaFLD";
			variableNames[6] = (char *) "M2-M1";
			variableNames[7] = (char *) "Delta P2/P1";
			showVariableHead( variableNames, variableNumber);
		}
		else if ( ioData[variableNameN] != deltaFldV){ 
			int variableNumber = 5;
			char * variableNames[variableNumber];
			variableNames[0] = (char *) "fld ";
			variableNames[1] = (char *) "M1";
			variableNames[2] = (char *) "M2";
			variableNames[3] = (char *) "P2P1";
			variableNames[4] = (char *) "deltaFLD";
			showVariableHead( variableNames, variableNumber);
		}
		else {	
			int variableNumber = 6;
			char * variableNames[variableNumber];
			variableNames[0] = (char *) "i ";
			variableNames[1] = (char *) "fldU";
			variableNames[2] = (char *) "fldD";
			variableNames[3] = (char *) "Mx";
			variableNames[4] = (char *) "My";
			variableNames[5] = (char *) "fld";
			showVariableHead( variableNames, variableNumber);
		}
    }
	else if (outputInfo == infoTubeShockLimits){
		int variableNumber = 3;
		char * variableNames[variableNumber];
		variableNames[0] = (char *) "M1";
		variableNames[1] = (char *) "M2upper";
		variableNames[2] = (char *) "M2lower";

		showVariableHead( variableNames, variableNumber);
	}
	else if (outputInfo == infoTubeProfile){
        int variableNumber = 7;
        char * variableNames[variableNumber];
		
        variableNames[0] = (char *) "fld";
        variableNames[1] = (char *) "Mx";
        variableNames[2] = (char *) "My";
        variableNames[3] = (char *) "Py/Px";
        variableNames[4] = (char *) "Px/P*";
        variableNames[5] = (char *) "Py/P*";
        variableNames[6] = (char *) "P1/P(i)";
		cout <<  "Fanno flow profile" << "\t"  ;
		cout << "M1 = " << M1 <<
				 "\t" << endl <<  "FLD = " <<  fld 
					<<endl << endl ;

        showVariableHead( variableNames, variableNumber);
    }
	else if (outputInfo == infoTubeShock){
        int variableNumber = 6;
        char * variableNames[variableNumber];

        variableNames[0] = (char *) "M1";
        variableNames[1] = (char *) "M2";
        variableNames[2] = (char *) "fldUp";
        variableNames[3] = (char *) "fldDown";
        variableNames[4] = (char *) "Mx";
        variableNames[5] = (char *) "My";
        cout <<  "Fanno flow profile" << "\t"  ;
        cout << "M1 = " << M1 <<
                 "\t" << endl <<  "FLD = " <<  fld
                    <<endl << endl ;

        showVariableHead( variableNames, variableNumber);
    }
};

void 	fanno::showResults(int *ioData) {
	int numberVariables;
	if (outputInfo == infoStandard || outputInfo == infoMinimal){
		numberVariables = 7;
		double variableToBePrinted[numberVariables];
		if ( (ioData[variableNameN] == fldP2P1V ) ||
			(ioData[variableNameN] ==  M1P2P1V ) ||
			(ioData[variableNameN] ==  M2P2P1V ) ) {
			numberVariables = 4;
			double variableToBePrinted[numberVariables];
			variableToBePrinted[0] = M1;
			variableToBePrinted[1] = M2;
			variableToBePrinted[2] = fld;
			variableToBePrinted[3] = p2p1;
			showVariable(variableToBePrinted, numberVariables);
		} 
		else if (ioData[variableNameN] == fldV) {
			numberVariables = 5;
			double variableToBePrinted[numberVariables];
			variableToBePrinted[0] = M1;
			variableToBePrinted[1] = M2;
			variableToBePrinted[2] = fld1;
			variableToBePrinted[3] = P1P2;
			variableToBePrinted[4] = fld2;
			showVariable(variableToBePrinted, numberVariables);
		}
		else if (isBothBranches == no) {
			variableToBePrinted[0] = M ;
			variableToBePrinted[1] = fld;
			variableToBePrinted[2] = PRstar;
			variableToBePrinted[3] = PRzeroStar;
			variableToBePrinted[4] = RRstar ;
			variableToBePrinted[5] = URstar;
			variableToBePrinted[6] = TRstar;
			showVariable(variableToBePrinted, numberVariables);
		}
		else if (isBothBranches == yes) {
			for (int i = 0 ; i < 2 ; i++){
				for (int j = 0 ; j < 7 ; j++){
					variableToBePrinted[j] =  steps[i][j];
				}
				showVariable(variableToBePrinted, numberVariables);
			}
		}
	}
	else if (outputInfo == infoTube) {
		if (isSubSonic == no){
			numberVariables = 6;
			double variableToBePrinted[numberVariables];
			variableToBePrinted[0] = M1;
			variableToBePrinted[1] = M2;
			variableToBePrinted[2] = fld;
			variableToBePrinted[3] = fld1;
			variableToBePrinted[4] = fld2;
			variableToBePrinted[5] = p2p1;
			showVariable(variableToBePrinted, numberVariables);
		}
		else {
			numberVariables = 4;
			double variableToBePrinted[numberVariables];
			variableToBePrinted[0] = M1;
			variableToBePrinted[1] = M2;
			variableToBePrinted[2] = fld;
			variableToBePrinted[3] = p2p1;
			showVariable(variableToBePrinted, numberVariables);
		}
	}
	else if ( outputInfo == infoShock) {
		numberVariables = 6;
		double variableToBePrinted[numberVariables];
		variableToBePrinted[0] = Mx;
		variableToBePrinted[1] = My;
		variableToBePrinted[2] = TyTx;
		variableToBePrinted[3] = RyRx;
		variableToBePrinted[4] = PyPx;
		variableToBePrinted[5] = P0yP0x;
		showVariable(variableToBePrinted, numberVariables);
	}
	else if  ( outputInfo == infoTubeShock) {
		numberVariables = 6;
		double variableToBePrinted[numberVariables];
		variableToBePrinted[0] = M1;
		variableToBePrinted[1] = M2;
		variableToBePrinted[2] = fldUp;
		variableToBePrinted[3] = fldDown;
		variableToBePrinted[4] = Mx;
		variableToBePrinted[5] = My;
		showVariable(variableToBePrinted, numberVariables);
	}
	else  if (outputInfo == infoIteration ) {
		if (ioData[variableNameN] != fldP2P1V ){
			numberVariables = 1;
			double variableToBePrinted[numberVariables];
			for (int i = 0 ; i < numberOfIterations ; i++){
				cout << i << "	" ;
				for (int j = 1 ; j < 5 ; j++){
					variableToBePrinted[j] = steps[i][j];
				}
				cout << i ;
				showVariable(variableToBePrinted, numberVariables);
			}
			return;
		}
		else if (ioData[variableNameN] == fldP2P1V ){
			numberVariables = 7;
			double variableToBePrinted[numberVariables];
			for (int i = 0 ; i < numberOfIterations ; i++){
				cout << (int) i << "	" ;
				for (int j = 0 ; j < 7 ; j++){
					variableToBePrinted[j] = steps[i][j];
				}
				showVariable(variableToBePrinted, numberVariables);
			}
			return;
		}
		else if (ioData[variableNameN] != deltaFldV) { 
			numberVariables = 6;
			double variableToBePrinted[numberVariables];
			for (int i = 0 ; i < numberOfIterations ; i++){
				variableToBePrinted[0] = i;
				for (int j = 0 ; j < 6 ; j++){
					variableToBePrinted[j+1] = steps[i][j];
				}
				showVariable(variableToBePrinted, numberVariables);
			}
		}
		else{ 
			numberVariables = 5;
			double variableToBePrinted[numberVariables];
			for (int i = 0 ; i < numberOfIterations ; i++){
				for (int j = 0 ; j < 5 ; j++){
					variableToBePrinted[j] = steps[i][j];
				}
				cout << i ;
				showVariable(variableToBePrinted, numberVariables);
			}
			return;
		}
    }
    else if  ( outputInfo == infoTubeProfile) {
		numberVariables = 7;
		double variableToBePrinted[numberVariables];
		for (int j=0; j < numberNodes  ; j++){
			for (int i=0; i < numberVariables ;i++){
				variableToBePrinted[i] = Mfld[i][j];
			}
        	showVariable(variableToBePrinted, numberVariables);
		}
	}
	else if ( outputInfo == infoIteration)  {
		numberVariables = 5;
		double variableToBePrinted[numberVariables];
		for (int i = 0 ; i < numberOfIterations ; i++){
			variableToBePrinted[0] = (double) i ;
			for (int j = 0 ; j < 5 ; j++){
				variableToBePrinted[j+1] =  steps[i][j]; 
			}
        	showVariable(variableToBePrinted, numberVariables);
		}
	}
};

double  fanno::calFLD(double Mach){
	M=Mach;
	if (M < .00) {
		//flow is reversed ?
		if (fabs( M) >  1.0 ) {
		 isSubSonic=yes;
		}
		isFlowReverse = yes;
		return 1.0;
	}
	else if (M > 1.0 ) {
		//supersonic flow
		isSubSonic=no;
		isFLDknown = yes;
		isFlowReverse = no;
		return (1. - M *M ) / k /M/ M  + 
			0.5*(k+1.)/k *log 
					( 0.5*(k+1.) * M * M/(1.+ 0.5* (k-1.)*M*M));
	}
	else if (M == 1. ) {
		return 0.;
		isFlowReverse = no;
		isFLDknown = yes ;
		isSubSonic=sonic;
	}
	else if (0.00001 > M  && M > EPS ) {
		// for case almost incompressible flow
		isSubSonic=yes;
		isFLDknown = no;
		isFlowReverse = no;
		// to put the approximation formula
		double eta, oneOverK , MM;
		oneOverK = 1./k ;
		MM = M*M;
		eta = (MM - 1. ) / (MM + 1.)  ;
		return oneOverK * ( 1/MM + (k+1.)*eta *(1. + eta*eta/3. + 0.2*pow(eta,4) )  
			+ 1./7.*pow(eta,6) +   1./9.*pow(eta,8))  ;
	}	
	else {
	// the subsonic flow and ``normal'' range
		isSubSonic=yes;
		isFLDknown = yes ;
		isFlowReverse = no;
        return (1. - M *M ) / k /M/ M +
            0.5*(k+1.)/k*log ( 0.5*(k+1.) * M * M/(1.+ 0.5* (k-1.)*M*M));
	}
};

void  fanno::calFLD(){

	if (M < .00) {
		//flow is reversed ?
        if (fabs( M) >  1.0 ) {
         isSubSonic=yes;
        }
		else {
			isSubSonic=no;
		}
		isFlowReverse = yes;
		return;
	}
	else if (M < (1.00000 + EPS) && M > (1.00000 - EPS) ) {
		// for case almost incompressible flow
		fld = 0. ;
		isSubSonic=no;
		isFLDknown = yes;
		isFlowReverse = no;
		return;
	}	
	else if (M > 1.0 ) {
		//supersonic flow
        fld = (1. - M *M ) / k /M/ M +
            0.5*(k+1.)/k*log ( 0.5*(k+1.) * M * M/(1.+ 0.5* (k-1.)*M*M));
		isSubSonic=no;
		isFLDknown = yes;
		isFlowReverse = no;
		return;
	}
	else if (M < 0.00001 ) {
		// for case almost incompressible flow
		fld = M ;
		isSubSonic=yes;
		isFLDknown = no;
		isFlowReverse = no;
		return;
	}	
	else {
	// the subsonic flow and ``normal'' range
        fld = (1. - M *M ) / k /M/ M +
            0.5*(k+1.)/k*log ( 0.5*(k+1.) * M * M/(1.+ 0.5* (k-1.)*M*M));
		isSubSonic=no;
		isFLDknown = no ;
		isFlowReverse = no;
	}
};

void  fanno::calArea(){
	Area =  pow((( 1. + ( 0.5* ( k - 1.) ) * M*M)/(0.5*(k+1))), 
			((k+1) / 2/(k-1) ))
				/ M;
};
	
double  fanno::calArea(double MM){
    return  pow((( 1 + (( k - 1) / 2 ) * MM*MM)/((k+1)/2)),
            ((k+1) / 2/(k-1) ))
                / MM;
};

void  fanno::calTemperatureRatio(){
	TRstar = 0.5* (k+1. )/ 
		(1. + (0.5*(k -1. ) *M*M) ) ;
	Tbar = TRstar;
};

double  fanno::calTemperatureRatio(double MM){
	return 0.5* (k+1. )/ 
		(1. + (0.5*(k -1. ) *MM*MM) ) ;
};

void  fanno::calTotalTemperatureRatio(){
	TRzeroStar = (k+1. )/ 
		(1. + (0.5*(k -1. ) *M*M) )*0.5 ;
};

double  fanno::calTotalTemperatureRatio(double MM){
	return  (k+1. )/ 
		(1. + (0.5*(k -1. ) *MM*MM) )*0.5 ;
};

void  fanno::calPressureRatio(){
	// calculate the pressure ratio
	PRstar =   sqrt(0.5*(k + 1.) /( 1.+ 0.5*(k-1.)*M*M ))/M ;
	Pbar = PRstar; // finding the root is not problems
};

double  fanno::calPressureRatio(double MM){
	// calculate the pressure ratio
	return   sqrt(0.5*(k + 1.) /( 1.+ 0.5*(k-1.)*MM*MM ))/MM ;
};

void  fanno::calVelocityRatio(){
    // calculate the velocity ratio
	URstar = M * sqrt ( 0.5 * (k+1. )/ 
		(1. + (0.5*(k -1. ) *M*M) ) ) ;
};

double  fanno::calVelocityRatio(double MM){
    // calculate the velocity ratio
	return MM * sqrt ( 0.5 * (k+1. )/ 
		(1. + (0.5*(k -1. ) *MM*MM) ) ) ;
};

void  fanno::calDensityRatio(){
    // calculate the density ratio
    RRstar =   sqrt(2.*(1. + 0.5* (k -1.0) *M*M) /( 1.+ k))/M ;
};

double  fanno::calDensityRatio(double MM){
    // calculate the density ratio
    return  sqrt(2.*(1. + 0.5* (k -1.0) *MM*MM) /( 1.+ k))/MM ;
};

void  fanno::calTotalPressureRatio(){

	if (isFlowReverse == no ) {
		PRzeroStar =  sqrt(
			pow((2.*(1. + 0.5* (k -1.0) *M*M) /( 1.+ k)),
				(( k+1.0)/(k-1.0))  )
		)/M;
		return ;
	}
	else {
		return ;
	}
};
 
double  fanno::calTotalPressureRatio(double MM){
	return  sqrt(
			pow((2.*(1. + 0.5* (k -1.0) *MM*MM) /( 1.+ k)),
				(( k+1.0)/(k-1.0))  )
		)/MM;  
};

int fanno:: calTableLine (int* ioData, double * variableValue){
	if ((outputType) ioData[whatInfoN] == infoStandard || 
		(outputType) outputInfo == infoMinimal ){
		if (ioData[isBothBranchesN] == yes) {
			variableValue[machV] = 0.5;
			int calResults[2] ;
			calResults[0] = calAll( ioData, variableValue);
			//to save the calculations and do second branch
			int i ;
			i =0 ;
			saveResults (i);
			// now the supersonic
			variableValue[machV] = 2.0;
			calResults[1] = calAll( ioData, variableValue);
			i =1 ;
			saveResults (i);
			if (calResults[0] == yes || calResults[1] == yes ){ 
				return yes;
			}
			else {
				return no;
			}
		}
		else {// only single branch
            return calAll( ioData, variableValue);
		}
	}
	else if ( outputInfo == infoIteration) {// only single branch
		return calAll( ioData, variableValue);
	}
	else {
    	return no;
	}
};

int fanno::calShockLocationForFLD (){
	double fldTMP,  fldMAXsub ;
	double fldOLD, fldDelta, fldU2;
	shock shockCla(k);
	if ( M1 < 1.0 ) { // subsonic branch no shock possible
		return no ;
	}
	fldTMP = fld ;
	M = M1;
	calFLD() ;
	fldMAX = fld;
	fld = fldTMP ; 
	fldDelta = fld;
	if ( fldMAX < fld ) {
		cout << "fld exceeded there is a shock in the tube!" << endl ;
		//check shock occurs at the entrace possible
		double Myy;
		Myy = shockCla.calMy(M);
		fldMAXsub = calFLD ( Myy );
		if (fldMAXsub <= fld){
			isPossible = no;
			cout << "this situation isn't possible" << endl ;
			cout << "The shock occures in the nozzle." << endl ;
			return no;
		}
		M2 = 1. ; //flow is chocked
		int i ;
		i = 0;
		fldDown = fld - fldMAX;
		while ( fabs ( fldDelta ) > EPS) {
			My = calMForFLD (fldDown, 0.2) ; // 0.2 -> subsonic branch
			Mx = shockCla.calMy(My);
			fldU2 = calFLD ( Mx );
			fldUp = fldMAX -  fldU2  ;
			fldDelta = fldDown - fldOLD ;
			fldOLD = fldDown ;
			fldDown =  fld - fldUp;
			if (isR == no ) {
				steps[i][0] = i;
				steps[i][1] = fldUp;
				steps[i][2] = fldDown;
				steps[i][3] = Mx;
				steps[i][4] = My;
			}
			i++;
    	}
    	numberOfIterations = i;
		M = M1;
		isThereShock = yes;
		if (outputInfo ==  infoStandard ) {
			outputInfo = infoTubeShock ;
		}
		else {
			;
		}
		return yes;
	}
	else { //no shock all supersonic flow
		fld1 = fld ;
		isSubSonic = no;
		isThereShock = no;
		return yes;
	}
};
 
void fanno:: saveResults (int i ) {
	steps[i][0] = M;
	steps[i][1] = fld;
	steps[i][2] = PRstar;
	steps[i][3] = PRzeroStar;
	steps[i][4] = RRstar;
	steps[i][5] = URstar;
	steps[i][6] = TRstar;
}

int   fanno::calAll(int * ioData, double * variableValue ){
	calMach(inputInfo, variableValue);
	if (inputInfo == M1fldV ) {
		if (isChoked == no ) 
			outputInfo = infoTube ;
		if ( M1 < singularPoint )  { //subsonic branch
			double givenFld, givenM, deltaFLD;
			isSubSonic = yes;
			givenFld = fld ;
			givenM = M1;
			deltaFLD = calFLD (givenM) - givenFld;
			if ( delta <= 0. ){
				M2 = 1.;
				isChoked = yes;
				M1 = calMForFLD (givenFld, 0.5);
			}
			else {
				M2 = calMForFLD(deltaFLD, 0.5);
				M = M1;
				calPressureRatio();
 				P1Pstar = Pbar;
				M = M2;
				calPressureRatio();
 				P2Pstar = Pbar;
				compressibleFlow::calP2P1();
			}
		}
		//change the output if needed
		else if ( M1 == singularPoint) { //already choked
			return no;
		}
		else if ( calShockLocationForFLD() == yes ){
			if (isThereShock == no && isPossible == yes){// calculate M2 and etc
				fld2 = fldMAX - fld1 ;
				M2 = calMForFLD(fld2, 2.0);
				outputInfo = infoTube ;
				//calculations for unchocked flow
				P1Pstar = calPressureRatio(fld1);
				P2Pstar = calPressureRatio(fld2);
				p2p1 = P2Pstar/ P1Pstar ;
			}
			else if ( isPossible == no) {
				return no; 
			}
		}
	}
	else if (inputInfo == deltaFldV ) {
		outputInfo = infoIteration ;
	}
	else if (inputInfo == M2fldV ) {
		double totalFLD;
		if (isChoked == no ) 
			outputInfo = infoTube ;
		totalFLD = fld + calFLD(M2) ;
		M1 = calMForFLD(totalFLD, M2);
		p2p1 = calP2P1(M1, M2);
		//calPressureRatio();
		//P1Pstar = Pbar;
		//M = M2;
		//calPressureRatio();
		//P2Pstar = Pbar;
		//compressibleFlow::calP2P1();
	}
	else if (( inputInfo == fldP2P1V) ||
         ( inputInfo  ==  M1P2P1V ) ||
         ( inputInfo  ==  M2P2P1V ) ) {
		//no Mach(es) yet 
	}
	else if ( (isChoked == no ) && ( ioData [isRangeN]  == yes ) ) {
		return yes;
	}
	else {
		if (isFLDknown == no ) {
			calFLD () ;
			//isFLDknown = yes;
		}
		calPressureRatio();
		calTemperatureRatio();
		calDensityRatio ();
		calVelocityRatio();
		calTotalPressureRatio();
	}
//  if (variableName != TbarV){
//      calTemperatureRatio();
//  }
//  calTotalPressureRatio();
    return yes;
}

int   fanno::doAllLines ( int * ioData, double * variableValue ){

	double startP, endP,  sp;
	sp = singularPoint;
	//int SingP = 1;
	double range [500];

	if (isChoked == no){
		range[1] = 99. ;
		range[2] =  0.01 ;
		if (isFLDknown == yes){
			range[3] = calMForFLD (variableValue[fldV], 0.2 ) ;
		}
		else {
			range[3] = Mmax ;
		}
		range[0] = double (1);
	}
	else {
		startP =  0.01;
		endP  = 0.1;
		//this is the subsonic branch
		range[1] = 99. ;
		range[2] =  0.01 ;
		range[3] = sp ;
		//this is the supersonic branch between 1 and 5
		range[4] = 399. ;
		range[5] = sp + 0.01 ;
		range[6] = sp + 4. ;
		range[7] = 249. ;
		range[8] = 5.02 ;
		range[9] = 10.0 ;
		range[10] = 399. ;
		range[11] = 10.1 ;
		range[12] = 50.0 ;
		
		range[0] = double (4);
		isFLDknown=no;
	}	
	setRange( ioData, range, variableValue);
	return yes;

	//int howManyPoints ;
	//if (outputInfo == infoStandard ){
//		howManyPoints =  7 ;
//		makeLines(ioData ,howManyPoints, 0.03, 0.1, variableValue);
//		howManyPoints =  16 ;
//		makeLines(ioData,howManyPoints, 0.2, 1.0,  variableValue);
//		howManyPoints =  8 ;
//		makeLines(ioData,howManyPoints, 2.0, 10.0, variableValue);
//		howManyPoints =  10 ;
//		makeLines(ioData,howManyPoints, 20., 70.,  variableValue);
//	}
}

void    fanno::makeHTMLTableHead(int * ioData){
	int variableNumber;
	if (outputInfo == infoStandard || outputInfo == infoMinimal) {
		if ((ioData[variableNameN] == fldP2P1V ) ||
			(ioData[variableNameN] ==  M1P2P1V ) ||
			(ioData[variableNameN] ==  M2P2P1V ) ) {
			int variableNumber = 4;
			char * variableNames[variableNumber];
			variableNames[0] = (char *) "M1  ";
			variableNames[1] = (char *) "M2  ";
			variableNames[2] = (char *) "fld ";
			variableNames[3] = (char *) "P2/P1";

			showVariableHTMLHead(variableNames,variableNumber);
		} 
		else {
			variableNumber = 7;
			char * variableNames[variableNumber];
			variableNames[0] = (char *) "M";
			variableNames[1] = (char *) "fld";
			variableNames[2] = (char *) "P/P*";
			variableNames[3] = (char *) "P0/P0*";
			variableNames[4] = (char *) "&rho;/&rho;*";
			variableNames[5] = (char *) "U/U*";
			variableNames[6] = (char *) "T/T*";
			showVariableHTMLHead(variableNames,variableNumber);
		}
	}
	else if ( outputInfo == infoTube)  {
		if (isSubSonic == no){
			int variableNumber = 6;
			char * variableNames[variableNumber];
			variableNames[0] = (char *) "M1 ";
			variableNames[1] = (char *) "M2 ";
			variableNames[2] = (char *) "fld";
			variableNames[3] = (char *) "fld1";
			variableNames[4] = (char *) "fld2";
			variableNames[5] = (char *) "P2/P1";
			showVariableHTMLHead(variableNames,variableNumber);
		}
		else {
			int variableNumber = 4;
			char * variableNames[variableNumber];
			variableNames[0] = (char *) "M1 ";
			variableNames[1] = (char *) "M2 ";
			variableNames[2] = (char *) "fld";
			variableNames[3] = (char *) "P2/P1";
			showVariableHTMLHead(variableNames,variableNumber);
		}
	}
	else if ( outputInfo == infoIteration)  {
		int variableNumber = 5;
		char * variableNames[variableNumber];
		variableNames[0] = (char *) "i ";
		variableNames[1] = (char *) "M1 ";
		variableNames[2] = (char *) "M2 ";
		variableNames[3] = (char *) "fld";
		variableNames[4] = (char *) "P2/P1";
		cout << "<html> " << endl;
		showVariableHTMLHead(variableNames,variableNumber);
	}
};

void    fanno::makeHTMLTableLine(int * ioData) {
	int numberVariables;
	if (outputInfo == infoStandard || outputInfo == infoMinimal) {
		numberVariables = 7;
		double variableToBePrinted[numberVariables];
		if ( (ioData[variableNameN] == fldP2P1V ) ||
			(ioData[variableNameN] ==  M1P2P1V ) ||
			(ioData[variableNameN] ==  M2P2P1V ) )
		 {
			numberVariables = 4;
			double variableToBePrinted[numberVariables];
			variableToBePrinted[0] = M1;
			variableToBePrinted[1] = M2;
			variableToBePrinted[2] = fld;
			variableToBePrinted[3] = p2p1;
			showVariableHTMLLine (variableToBePrinted, numberVariables);
		} 
		else if (isBothBranches == no) {
			variableToBePrinted[0] = M ;
			variableToBePrinted[1] = fld;
			variableToBePrinted[2] = PRstar;
			variableToBePrinted[3] = PRzeroStar;
			variableToBePrinted[4] = RRstar ;
			variableToBePrinted[5] = URstar;
			variableToBePrinted[6] = TRstar;
			showVariableHTMLLine (variableToBePrinted, numberVariables);
		}
		else if (isBothBranches == yes) {
			for (int i = 0 ; i < 2 ; i++){
				for (int j = 0 ; j < 7 ; j++){
					variableToBePrinted[j] =  steps[i][j]; 
				}
				showVariableHTMLLine (variableToBePrinted, numberVariables);
			}
		}
	}
	else if (outputInfo == infoTube) {
		if (isSubSonic == no){
			numberVariables = 6;
			double variableToBePrinted[numberVariables];
			variableToBePrinted[0] = M1;
			variableToBePrinted[1] = M2;
			variableToBePrinted[2] = fld;
			variableToBePrinted[3] = fld1;
			variableToBePrinted[4] = fld2;
			variableToBePrinted[5] = p2p1;
			showVariableHTMLLine (variableToBePrinted, numberVariables);
		}
		else {
			numberVariables = 4;
			double variableToBePrinted[numberVariables];
			variableToBePrinted[0] = M1;
			variableToBePrinted[1] = M2;
			variableToBePrinted[2] = fld;
			variableToBePrinted[3] = p2p1;
			showVariableHTMLLine (variableToBePrinted, numberVariables);
		}
	}
	else if ( outputInfo == infoIteration)  {
		numberVariables = 5;
		double variableToBePrinted[numberVariables];
		for (int i = 0 ; i < numberOfIterations ; i++){
			variableToBePrinted[0] = (double) i ;
			for (int j = 0 ; j < 5 ; j++){
				variableToBePrinted[j+1] =  steps[i][j]; 
			}
			showVariableHTMLLine (variableToBePrinted, numberVariables);
		}
	}
};

void  fanno::makeHTMLTableTail(int * ioData) {
	compressibleFlow::makeHTMLTableTail(ioData);		
};
