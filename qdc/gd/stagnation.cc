// last change Mon Dec 19 17:48:39 CST 2011 cast conversion from string
// constant to ‘char*’
/********************************************************
	FILE:stagnation.cc

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip> 
#  include "stagnation.h" 
#  include "isothermalNozzle.h" 
#  include "externalFun.h" 

using namespace std;

// the stagnation class functions definitions
//**********************************************//

int stagnation:: calTableLine (int * ioData,  double * variableValue){
    if (outputInfo == infoStandard ||
		 outputInfo == infoStagnation ||
		outputInfo == infomDot ||
		outputInfo == infoFn ){
		return calAll(ioData,  variableValue);
    }
    else  {
    	return no;
    }
};
                                                                               
int   stagnation::calAll(int * ioData,  double * variableValue ){

	calMach(ioData, variableValue);
	calAreaRatio();
	calPressureRatio();
	if (inputInfo != TRstarV){
		calTemperatureRatio();
	}
	calTotalTemperatureRatio(); //towork on 
	calTotalPressureRatio(); //towork on 
	calDensityRatio ();
	calPARRatio();
	calImpulse();
	if (ioData[whatInfoN] == infoFn ) {
		calFn();
	}
	return yes ;
}

void  stagnation::calMach(int * ioData, double * variableValue){
	inputType variableName = (inputType) ioData[variableNameN];
	if (variableName == machV) {
		M = variableValue[machV];
	}
	else if (variableName == PbarV) {
		Pbar = variableValue[PbarV];
		double tmpM, tmpPbar;
		tmpPbar = Pbar;
		tmpM =  variableValue[machV];
		if (tmpM < 1.0 ) {
			M = root  ( calPbarEstmt, this, .00001,  M1maxSub, Pbar) ; 
		}
		else{ 
			 M = root  ( calPbarEstmt, this, 1.0,  100., Pbar) ;
		}
	}
	else if (variableName == TbarV) {
		Tbar = variableValue[TbarV];
		if (Tbar > (2.0/(k+1.)) ) {// subsonic
			M = root  ( calTbarEstmt, this, .00001,  M1maxSub, Tbar) ; 
		}
		else if ( Tbar == (2.0/(k+1.)) ) 
			M = 1.0 ;
		else if (Tbar < 0.0001) 
			M =  sqrt( 2.0 / (k -1.) / Tbar); 
		else{ // other wise normal range supersonc 
			 M = root  ( calTbarEstmt, this, 1.0,  100., Tbar) ;
		}
	}
	else if (variableName == RbarV) {
		Rbar = variableValue[RbarV];
		if (Rbar > pow((2.0/(k+1.)), (1. /(k -1.))) ) {// subsonic
			M = root  ( calRbarEstmt, this, .00001,  M1maxSub, Rbar) ; 
		}
		else if ( Rbar == pow((2.0/(k+1.)), (1. /(k -1.))) ) 
			M = 1.0 ;
		else if (Rbar < 0.0001) 
			M =  sqrt( 2.0 / (k -1.) / Rbar); 
		else{ // other wise normal range supersonc 
			 M = root  ( calRbarEstmt, this, 1.0,  120., Rbar) ;
		}
	}
	else if (variableName == AstarV) {
		Area = variableValue[AstarV];
		if ( variableValue[machV] <1. ) {
        	M = root  ( calAstarEstmt, this, .00001,  M1maxSub, Area) ;
		}
		else {
        	M = root  ( calAstarEstmt, this, M1maxSub, 100., Area) ;
		}
	}
	else if (variableName == PARV) {
		PAR = variableValue[PARV];
        if ( PAR > 1. ) {
			M = root  ( calPAREstmt, this, .00001,  M1maxSub, PAR) ;
		}
		else {
            M = root  ( calPAREstmt, this, 1.00001,  100. , PAR) ;
		}
	}
	else if (variableName == mDotP0TV) {
		double mDot2, correctionFacor; // local veriable
		P0.setValue (variableValue[P0V]);
		T.setValue (variableValue[TV]);
		mDot.setValue (variableValue[mDotV]);
		mDot2 = mDot.value*mDot.value;
		PAR = sqrt(P0.value*P0.value/R.value /T.value/mDot2);
		if ( PAR > 1. ) {
			M = root  ( calPAREstmt, this, .00001,  M1maxSub, PAR) ;
		}
		else {
			M = root  ( calPAREstmt, this, 1.00001,  100. , PAR) ;
		}
		//correctionFacor = 2.*(k -1) * mDot.value 
         //       *mDot.value/k /R.value/T0.value;
		correctionFacor = 2.*(k -1) * mDot.value *mDot.value/k
			 *R.value*T0.value /P.value /P.value;
		double Rhat;
		Rhat = 0.5*(1.+sqrt(1.+correctionFacor)); 
		//rho.value = 0.5 * ( 1. + PRT0
		//	+ sqrt(PRT0 * PRT0+ correctionFacor ));
		rho.value = Rhat * P.value /R.value /T0.value; 
		rho.isKnown = yes;
		T.value = P.value / R.value /rho.value ;
		T.isKnown = yes;
		Tbar = T.value/T0.value ;
		calFn();
	}
	else if (variableName == mDotPT0V) {
		double PRT0, correctionFacor; // local veriable
		P.setValue (variableValue[PV]);
		P0.setValue (variableValue[P0V]);
		T.setValue (variableValue[TV]);
		T0.setValue (variableValue[T0V]);
		mDot.setValue (variableValue[mDotV]);
		PRT0 = P.value/R.value /T0.value;
		//correctionFacor = 2.*(k -1) * mDot.value 
         //       *mDot.value/k /R.value/T0.value;
		correctionFacor = 2.*(k -1) * mDot.value *mDot.value/k
			 *R.value*T0.value /P.value /P.value;
		double Rhat;
		Rhat = 0.5*(1.+sqrt(1.+correctionFacor)); 
		//rho.value = 0.5 * ( 1. + PRT0
		//	+ sqrt(PRT0 * PRT0+ correctionFacor ));
		rho.value = Rhat * P.value /R.value /T0.value; 
		rho.isKnown = yes;
		T.value = P.value / R.value /rho.value ;
		T.isKnown = yes;
		Tbar = 1. / Rhat ;
		M = sqrt ( 2. * (Rhat - 1.) / ( k-1.)); 
		calFn();
	}
};

void  stagnation::calPressureRatio(){
	Pbar =  pow (1. / ( 1. + (( k - 1.) / 2. ) * M*M),(k / (k - 1.) ) ) ;
};

double   stagnation::calPressureRatio(double MM){
    return  pow (1. / ( 1. + (( k - 1.) / 2. ) * MM*MM),(k / (k - 1.) ) ) ;
};


void  stagnation::calImpulse(){
	FRstar = PAR * (1.0 + k * M*M)/(1.0+k) ;	
};

void  stagnation::calFn(){
	//if ( M.value == yes) {
	// if M known and Tbar isnot calculate it
		Fn.setValue(k * M * pow (Tbar,0.5*(k+1.)/(1.-k))) ;	
		Fn.isKnown = yes;
		Fnk = Fn.value*Fn.value/k;
		oPAR2 = 1. / PAR /PAR ; 
 		RT0.setValue(Fnk*oPAR2);
 		RT0.isKnown = yes;
		rho0P.setValue(Fnk/Pbar);
		rho0P.isKnown = yes;
		rho0T.setValue(Fnk/Tbar);
		rho0T.isKnown = yes;
	//}
};


void  stagnation::calTemperatureRatio(){
	double tmpTbar;
	Tbar = 1. / ( 1. + (( k - 1.) / 2. ) * M*M);
	tmpTbar= Tbar;
};

void  stagnation::calDensityRatio(){ 
	 Rbar = pow ( Pbar, (1./k)) ;
};

void  stagnation::calAreaRatio(){
	    Area =  pow((( 1.0 + (( k - 1.0) / 2. ) * M*M)/((k+1.)/2.)),
            ((k+1.) / 2./(k-1.) ))
                / M;
};
void  stagnation::calPARRatio(){
	PAR =  Area * Pbar;
};

void stagnation::calTotalTemperatureRatio (void) {
	
} ;

void stagnation::calTotalPressureRatio (void){

} ;


void  stagnation::makeTexTableHead(int * ioData){ 

    int variableNumber;

    if (outputInfo == infoStandard || outputInfo == infoStagnation ) {
        variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "M";
        variableNames[1] = (char *) "T \\over T_0";
        variableNames[2] = (char *) "\\rho \\over \\rho_0";
        variableNames[3] = (char *) "A \\over A^{\\star}";
        variableNames[4] = (char *) "P \\over P_0";
        variableNames[5] = (char *) "A\\times P \\over A^{*} \\times P_0";
        variableNames[6] = (char *) "F \\over F^{*}";

        showVariableTeXHead(variableNames,variableNumber);
    }
	else if (outputInfo == infomDot ) {
        variableNumber = 5;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "M";
        variableNames[1] = (char *) "T[K] ";
        variableNames[2] = (char *) "P[Bar]";
        variableNames[3] = (char *) "\\rho[kg/m^3]";
        variableNames[4] = (char *) "Fn ";
        showVariableTeXHead(variableNames,variableNumber);
    }
    else if (outputInfo == infoFn ) {
        int variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "M";
        variableNames[1] = (char *) "Fn";
        variableNames[2] = (char *) "\\rho RT_0 \\over P"; //rho hat
        variableNames[3] = (char *) "\\left( P_0A^{*} \\over PA\\right )^2";
        variableNames[4] = (char *) 
				"{RT_0\\over P^(char *) 2} \\left(\\dot{m} \\over A\\right)^2";
        variableNames[5] = (char *) 
				"{ 1 \\over R\\rho_0 P} \\left(\\dot{m} \\over A \\right)^2";
        variableNames[6] = (char *) 
				"{1\\over R{\\rho_0}^2T}\\left(\\dot{m} \\over A\\right)^2";
        showVariableTeXHead(variableNames,variableNumber);
    }
}

void  stagnation::makeTexTableLine(int * ioData){ 
    int numberVariables;

    if (outputInfo == infoStandard  || outputInfo == infoStagnation ) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M ;
        variableToBePrinted[1] = Tbar;
        variableToBePrinted[2] = Rbar;
        variableToBePrinted[3] = Area;
        variableToBePrinted[4] = Pbar ;
        variableToBePrinted[5] = PAR;
        variableToBePrinted[6] = FRstar;

        showVariableTeXLine (variableToBePrinted, numberVariables);
    }
	else if (outputInfo == infomDot ) {
        numberVariables = 5;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M ;
        variableToBePrinted[1] = T.value ;
        variableToBePrinted[2] = P.value/100000.;
        variableToBePrinted[3] = rho.value;
        variableToBePrinted[4] = Fn.value ;
        showVariableTeXLine (variableToBePrinted, numberVariables);
    }
	else if (outputInfo == infoFn ) {
		numberVariables = 7;
		double variableToBePrinted[numberVariables];
		variableToBePrinted[0] = M ;
		variableToBePrinted[1] = Fn.value ;
		variableToBePrinted[2] = 1./Tbar; //rhohat
		variableToBePrinted[3] = 1./PAR/PAR;
		variableToBePrinted[4] = RT0.value ;
		variableToBePrinted[5] = rho0P.value ;
		variableToBePrinted[6] = rho0T.value ;
		showVariableTeXLine (variableToBePrinted, numberVariables);
	}
}

int   stagnation::doAllLines ( int * ioData, double * variableValue ){
	int howManyPoints;
	if (outputInfo == infoStandard || outputInfo == infoFn){
		howManyPoints =  2 ;
		makeLines(ioData,howManyPoints, (.0+EPS), 0.1, variableValue);
		howManyPoints =  15 ;
		makeLines(ioData,howManyPoints, 0.2, 0.95, variableValue);
		howManyPoints =  19 ;
		makeLines(ioData,howManyPoints, 1.0, 1.95, variableValue);
		howManyPoints =  32 ;
		makeLines(ioData,howManyPoints, 2., 10., variableValue);
		howManyPoints =  3 ;
		makeLines(ioData,howManyPoints, 20., 50., variableValue);
    }
    return yes;
}

void    stagnation::showHead(int * ioData){
    cout << endl << endl <<  "adiabtic Nozzle" << "\t\t"  ;
    cout << "k = " << k << endl << endl ;

    if (outputInfo == infoStandard || outputInfo == infoStagnation ) {
        int variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "M  ";
        variableNames[1] = (char *) "T/T0";
        variableNames[2] = (char *) "R/R0";
        variableNames[3] = (char *) "A/A*";
        variableNames[4] = (char *) "P/P0";
        variableNames[5] = (char *) "PAR";
        variableNames[6] = (char *) "F/F*";

        showVariableHead( variableNames, variableNumber);
    }
    else if (outputInfo == infoTubeProfile ) {
        int variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "z";
        variableNames[1] = (char *) "M";
        variableNames[2] = (char *) "Uise/U^T";
        variableNames[3] = (char *) "T/T0*";
        variableNames[4] = (char *) "A/A*";
        variableNames[5] = (char *) "P/P*";
        variableNames[6] = (char *) "PAR";
        showVariableHead( variableNames, variableNumber);
    }
    else if (outputInfo == infomDot ) {
        int variableNumber = 5;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "M";
        variableNames[1] = (char *) "T[K]";
        variableNames[2] = (char *) "P[Bar]";
        variableNames[3] = (char *) "rho[kg/m^3]";
        variableNames[4] = (char *) "Fn";
        showVariableHead( variableNames, variableNumber);
    }
    else if (outputInfo == infoFn ) {
        int variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "M";
        variableNames[1] = (char *) "Fn";
        variableNames[2] = (char *) "rhoRT0/P"; //rho hat
        variableNames[3] = (char *) "(P0A*/PA)^2";
        variableNames[4] = (char *) "RT0/P^2 (mA)^2";
        variableNames[5] = (char *) "1/Rrho0P (mA)^2";
        variableNames[6] = (char *) "1/Rrho0^2T (mA)^2";
        showVariableHead( variableNames, variableNumber);
    }
}

void    stagnation::showResults(int *ioData) {
    int numberVariables;
    if (outputInfo == infoStandard || outputInfo == infoStagnation ) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M ;
        variableToBePrinted[1] = Tbar;
        variableToBePrinted[2] = Rbar;
        variableToBePrinted[3] = Area;
        variableToBePrinted[4] = Pbar ;
        variableToBePrinted[5] = PAR;
        variableToBePrinted[6] = FRstar;
        showVariable(variableToBePrinted, numberVariables);
	}
	else if (outputInfo == infoTubeProfile ) {
		numberVariables = 7;
		double variableToBePrinted[numberVariables];
		if (M < singularPoint) {
            variableToBePrinted[0] = 1.050972149 - Area* 0.052548607  ;
        }
        else {
            variableToBePrinted[0] = 0.949027851 + Area* 0.052548607 ;
        }
        variableToBePrinted[1] = M ;
		double parama = sqrt(1.+ .5*(k-1)*M*M) ;
        variableToBePrinted[2] = M/parama/URstar ;
        variableToBePrinted[3] = Tbar;
        variableToBePrinted[4] = Area;
        variableToBePrinted[5] = Pbar ;
        variableToBePrinted[6] = PAR;
        showVariable(variableToBePrinted, numberVariables);
    }
	else if (outputInfo == infomDot ) {
		numberVariables = 5;
		double variableToBePrinted[numberVariables];
		variableToBePrinted[0] = M ;
		variableToBePrinted[1] = T.value ;
		variableToBePrinted[2] = P.value/100000.;
		variableToBePrinted[3] = rho.value;
		variableToBePrinted[4] = Fn.value ;
		showVariable(variableToBePrinted, numberVariables);
	}
	else if (outputInfo == infoFn ) {
		numberVariables = 7;
		double variableToBePrinted[numberVariables];
		variableToBePrinted[0] = M ;
		variableToBePrinted[1] = Fn.value ;
		variableToBePrinted[2] = 1./Tbar; //rhohat
		variableToBePrinted[3] = 1./PAR/PAR;
		variableToBePrinted[4] = RT0.value ;
		variableToBePrinted[5] = rho0P.value ;
		variableToBePrinted[6] = rho0T.value ;
		showVariable(variableToBePrinted, numberVariables);
	}
}

void    stagnation::makeHTMLTableHead(int * ioData){
    int variableNumber;
                                                                               
    if (outputInfo == infoStandard || outputInfo == infoMinimal) {
        variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "M  ";
        variableNames[1] = (char *) "T/T0";
        variableNames[2] = (char *) "&rho;/&rho;0";
        variableNames[3] = (char *) "A/A*";
        variableNames[4] = (char *) "P/P0";
        variableNames[5] = (char *) "PAR";
        variableNames[6] = (char *) "F/F*";
        showVariableHTMLHead(variableNames,variableNumber);
    }
    else if (outputInfo == infoFn ) {
        int variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "M";
        variableNames[1] = (char *) "Fn";
        variableNames[2] = (char *) "&rho;RT<sub>0</sub>/P"; //rho hat
        variableNames[3] = (char *) "(P<sub>0</sub>A<sup>*</sup>/PA)^2";
        variableNames[4] = (char *) "RT<sub>0</sub>/P^2 mA^2";
        variableNames[5] = (char *) "1/R&rho;<sub>0</sub>P mA^2";
        variableNames[6] = (char *) "1/R&rho;<sub>0</sub>^2T mA^2";
        showVariableHTMLHead(variableNames,variableNumber);
    }
};

void    stagnation::makeHTMLTableLine(int *ioData) {
	
	int numberVariables;
	numberVariables = 7;
	double variableToBePrinted[numberVariables];
	if (outputInfo == infoStandard || outputInfo == infoMinimal) {
		if (isBothBranches == no) {
			variableToBePrinted[0] = M ;
			variableToBePrinted[1] = Tbar;
			variableToBePrinted[2] = Rbar;
			variableToBePrinted[3] = Area;
			variableToBePrinted[4] = Pbar ;
			variableToBePrinted[5] = PAR;
			variableToBePrinted[6] = FRstar;

			showVariableHTMLLine (variableToBePrinted, numberVariables);
		}
		else if (isBothBranches == yes) {
			for (int i = 0 ; i < 2 ; i++){
				for (int j = 0 ; j < numberVariables ; j++){
					variableToBePrinted[j] =  steps[i][j];
				}
				showVariableHTMLLine (variableToBePrinted, numberVariables);
			}
		}
	}
	else if (outputInfo == infoFn ) {
		numberVariables = 7;
		double variableToBePrinted[numberVariables];
		variableToBePrinted[0] = M ;
		variableToBePrinted[1] = Fn.value ;
		variableToBePrinted[2] = 1./Tbar; //rhohat
		variableToBePrinted[3] = 1./PAR/PAR;
		variableToBePrinted[4] = RT0.value ;
		variableToBePrinted[5] = rho0P.value ;
		variableToBePrinted[6] = rho0T.value ;
		showVariableHTMLLine (variableToBePrinted, numberVariables);
	}
};

void    stagnation::makeHTMLTableTail(int * ioData) {
	compressibleFlow::makeHTMLTableTail(ioData);		
};

void stagnation:: saveResults (int i ) {
	steps[i][0] = M;
	steps[i][1] = Tbar;
	steps[i][2] = Rbar;
	steps[i][3] = Area;
	steps[i][4] = Pbar;
	steps[i][5] = PAR;
	steps[i][6] = FRstar;
}
