/********************************************************
	FILE:oblique.cc

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  oblique wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and oblique wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip>
#  include "stagnation.h"
#  include "externalFun.h"
#  include "oblique.h" 
#  include "gasDynamics.h" 
using namespace std;

/// oblique class definition 
//******************************************************************
//

int oblique::manegerCalTableLine (int *ioData, double *variableValue){
	compressibleFlow::manegerCalTableLine (ioData, variableValue);	
	if ( ioData[whatInfoN] == infoRange) {
		doAllLines(ioData,  variableValue);
	}	
	makeTail(ioData);	
	return yes;
};

void    oblique::showResults(int * ioData) {
	int numberVariables;
	if (outputInfo == infoStandard && isObliquePossible == yes  ) {
		numberVariables = 8;
		double variableToBePrinted[numberVariables];
		variableToBePrinted[0] = Mx ;
		variableToBePrinted[1] = Myn[1];
		variableToBePrinted[2] = Myn[0];
		variableToBePrinted[3] = theta[1]*180./PI;
		variableToBePrinted[4] = theta[0]*180./PI ;
		variableToBePrinted[5] = delta*180./PI;
		variableToBePrinted[6] = D;
		variableToBePrinted[7] = P0yP0x;
		showVariable(variableToBePrinted, numberVariables);
		return;
	}
	else if (outputInfo == infoRange && isObliquePossible == yes  ) {
		if (inputInfo == thetaMV ){
			numberVariables = 3;
			double variableToBePrinted[numberVariables];
			variableToBePrinted[0] = theta[0]*180./PI ;
			variableToBePrinted[1] = delta*180./PI ;
			variableToBePrinted[2] = Mx ;
			showVariable(variableToBePrinted, numberVariables);
		}
		else if (inputInfo == deltaMV ){
			numberVariables = 8;
			// only weak shock for now
			double variableToBePrinted[numberVariables];
			variableToBePrinted[0] = Mx ;
			variableToBePrinted[1] = Myn[0];
			variableToBePrinted[2] = Myn[1];
			variableToBePrinted[3] = Myn[2];
			variableToBePrinted[4] = theta[0]*180./PI ;
			variableToBePrinted[5] = theta[1]*180./PI ;
			variableToBePrinted[6] = theta[2]*180./PI ;
			variableToBePrinted[7] = delta*180./PI;
			showVariable(variableToBePrinted, numberVariables);
			return;
		}
	}
	else if (outputInfo == infoMax && isObliquePossible == yes  ) {
		numberVariables = 7;
		// only weak shock for now
		double variableToBePrinted[numberVariables];
		variableToBePrinted[0] = Mx ;
		variableToBePrinted[1] = Myn[0];
		variableToBePrinted[2] = theta[0]*180./PI ;
		variableToBePrinted[3] = delta*180./PI;
		variableToBePrinted[4] = PyPx;
		variableToBePrinted[5] = TyTx;
		variableToBePrinted[6] = P0yP0x;
		showVariable(variableToBePrinted, numberVariables);
		return;
	}
	else  if (outputInfo == infoMinimal ) {
		numberVariables = 7;
		double variableToBePrinted[numberVariables];
		variableToBePrinted[0] = Mx ;
		variableToBePrinted[1] = Myn[0];
		variableToBePrinted[2] = theta[0]*180./PI ;
		variableToBePrinted[3] = delta*180./PI;
		variableToBePrinted[4] = PyPx;
		variableToBePrinted[5] = TyTx;
		variableToBePrinted[6] = P0yP0x;
		showVariable(variableToBePrinted, numberVariables);
		return;
	}
    else  if (outputInfo == infoObliqueMax ) {
        numberVariables = 4;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx  ;
        variableToBePrinted[1] = Myn[0];
        variableToBePrinted[2] = maxDelta* 180./PI ;
        variableToBePrinted[3] = theta[1]* 180./PI;

        showVariable(variableToBePrinted, numberVariables);
		return;
	}
    else  if (outputInfo == infoObliqueMin ) {
        numberVariables = 4;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx  ;
        variableToBePrinted[1] = My ;
        variableToBePrinted[2] = delta* 180./PI ;
        variableToBePrinted[3] = theta[0]* 180./PI;

        showVariable(variableToBePrinted, numberVariables);
        return;
    }
    else  if (outputInfo == infoD0 ) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx  ;
        variableToBePrinted[1] = D ;
        variableToBePrinted[2] = theta[0]* 180./PI;

        showVariable(variableToBePrinted, numberVariables);
        return;
    }
	else if (outputInfo == infoStandard && isObliquePossible == no  ) {
		cout << "Mx=" << Mx 
			<< " delta=" << delta*180./PI ;  
		cout << " :: No oblique shock possible. " << endl ;
		if (delta ==0.0) {
			cout << " For normal shock My =" << Myn[1];
		}
		cout  << endl ;
		return ;
	}
};

void    oblique::showHead(int * ioData){
    cout << endl << endl <<  "Oblique Shock" << "\t\t"  ;
    cout << "k = " << k << endl << endl ;

	int variableNumber ;
    if (outputInfo == infoStandard ) {
		variableNumber = 8;
		char * variableNames[variableNumber];
		variableNames[0] = (char *) "Mx  ";
		variableNames[1] = (char *) "My_s";
		variableNames[2] = (char *) "My_w";
		variableNames[3] = (char *) "Theta_s";
		variableNames[4] = (char *) "Theta_w";
		variableNames[5] = (char *) "delta";
		variableNames[6] = (char *) "D";
		variableNames[7] = (char *) "P0yP0x";
		showVariableHead( variableNames, variableNumber);
    }
	else if (outputInfo == infoRange  ) {
		if (inputInfo == thetaMV ){
			variableNumber = 3;
			char * variableNames[variableNumber];
			variableNames[0] = (char *) "theta";
			variableNames[1] = (char *) "delta";
			variableNames[2] = (char *) "Mx   ";
			showVariableHead( variableNames, variableNumber);
		}
		else if (inputInfo == deltaMV ){
			variableNumber = 8;
			char * variableNames[variableNumber];
			variableNames[0] = (char *) "Mx  ";
			variableNames[1] = (char *) "My_s";
			variableNames[2] = (char *) "My_w";
			variableNames[3] = (char *) "My_i";
			variableNames[4] = (char *) "Theta_s";
			variableNames[5] = (char *) "Theta_w";
			variableNames[6] = (char *) "Theta_i";
			variableNames[7] = (char *) "delta";
			showVariableHead( variableNames, variableNumber);
		}
	}
    else if (outputInfo == infoMax ) {
        variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "Mx  ";
        variableNames[1] = (char *) "My_w";
        variableNames[2] = (char *) "Theta_w";
        variableNames[3] = (char *) "delta"; 
        variableNames[4] = (char *) "Pbar" ; 
        variableNames[5] =  (char *) "Tbar" ; 
        variableNames[6] = (char *) "P0yP0x"; 
        //variableNames[7] = "P0yP0x";
        showVariableHead( variableNames, variableNumber);
    }
    else if (outputInfo == infoMinimal ) {
        variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "Mx  ";
        variableNames[1] = (char *) "My_w";
        variableNames[2] = (char *) "Theta_w";
        variableNames[3] = (char *) "delta";
        variableNames[4] = (char *) "Pbar" ; 
        variableNames[5] =  (char *) "Tbar" ; 
        variableNames[6] = (char *) "P0yP0x"; 
        showVariableHead( variableNames, variableNumber);
    }
	else  if (outputInfo == infoObliqueMax ) {
        variableNumber = 4;
        char * variableNames[variableNumber];
		cout << "The maximum values are:" << endl;
        variableNames[0] = (char *) "Mx  ";
        variableNames[1] = (char *) "My";
        variableNames[2] = (char *) "delta";
        variableNames[3] = (char *) "theta";
        showVariableHead( variableNames, variableNumber);
    }
    else  if (outputInfo == infoObliqueMin ) {
        variableNumber = 4;
        char * variableNames[variableNumber];
        cout << "The values at minimum delta are:" << endl;
        variableNames[0] = (char *) "Mx  ";
        variableNames[1] = (char *) "My";
        variableNames[2] = (char *) "delta";
        variableNames[3] = (char *) "theta";
        showVariableHead( variableNames, variableNumber);
    }
    else  if (outputInfo == infoD0 ) {
        variableNumber = 3;
        char * variableNames[variableNumber];
        cout << "The values at zero delta are:" << endl;
        variableNames[0] = (char *) "Mx  ";
        variableNames[1] = (char *) "D";
        variableNames[2] = (char *) "theta";
        showVariableHead( variableNames, variableNumber);
    }

};

void  oblique::calMach(inputType variableName, double * variableValue){
    if (variableName == MxV) {
        Mx = variableValue[MxV];
    }
	else if (variableName == deltaThetaV) {
		M = 2. *(1.0/tan(theta[0]) + tan(delta))/
			(sin (2.* theta[0]) - tan(delta)*
					(k + cos (2.*theta[0]))) ;
		if (M > 0.) {
			M = sqrt(M);
			Mx = M *cos(theta[0]);
		}
		else {
		//no solution 
			
		}
	}
};

void  oblique::calMx(inputType variableName, double * variableValue){
	double tmpMx;
    if (variableName == P0yP0xV) {
        P0yP0x = variableValue[P0yP0xV];
        M = root  ( calP0yP0xEstmt, this, M1maxSub, 100.0, P0yP0x) ;
		Mx = M * sin (theta[0]);
		return;
    }
	else if (variableName == MxV){ 
		Mx = variableValue[MxV] ;
		calMy();
		if ( Mx < 1.0) {
			tmpMx = Mx ;
			Mx = My;
			My = tmpMx;
		}
	}
    else if (variableName == machV){
        Mx = variableValue[machV] ;
        calMy();
        if ( Mx < 1.0) {
            tmpMx = Mx ;
            Mx = My;
            My = tmpMx;
        }
    }
}

void  oblique::calMy(){
    My = sqrt((Mx*Mx + 2.0/(k -1.0)) /
        ((2.0*k / (k-1.0) ) *Mx*Mx -1.0)) ;
};

double  oblique::calMy(double m){
    return  sqrt((m*m + 2.0/(k -1.0)) / ((2.0*k / (k-1.0) ) *m*m -1.)) ;
};

void  oblique::calDelta(void){
	double a, b, sinTheta, sinTheta2;
	// double c;
	double Mx2;
	//theta[0] = 50.0*PI  /180. ;
	sinTheta= sin(theta[0]);
	sinTheta2 = sinTheta*sinTheta;
    //Mx = 2.0;
	Mx2 = Mx*Mx;
	a = tan (theta[0]) *(0.5*(k+1.)*Mx2/(Mx2*sinTheta2 -1.) -1.0 ) ;
	if ( a <= 0.0 ){
		b = -1.;
		cout << "there is no possible solution for this theta "
			<< "or Mx" << endl;
	}
	else {
		b = atan(1./a) ;
	}
	//c = b* 180. /PI ;
	//delta = c;
	delta = b;
	return;
};

int  oblique::calTheta(inputType variableName, double * variableValue){
	if (variableName == deltaMV) {
		delta  = PI * variableValue[deltaMV] /180. ;
		Mx = variableValue[MxV] ;
		calTheta();
		return yes; 
    }
	else if (variableName == thetaMV) {
		theta[0]  = PI * variableValue[thetaV] /180. ;
		theta[1] = 0.;
		calMinMx();
		Mx = variableValue[MxV] ;
		if (minMx > Mx ){
			cout <<"no possible solution!! too low Mx or change Theta"<< endl;
			cout << "The minimum Mx is "<< minMx << endl;
			return no;
		}
		calDelta ();
		return yes; 
    }
    else if (variableName == deltaThetaV) {
		theta[0]  = PI * variableValue[thetaV] /180. ;
		theta[1]  = 0. ;
		delta =  PI * variableValue[deltaMV] /180. ;
		return yes;
	}
	else if ( variableName == MxV){
		Mx = variableValue[MxV];
		calM1n();
		calDelta();
		return yes;
	}
	else { // default if not regular variable try delta and Mx
		variableName = deltaMV;
        delta  = PI * variableValue[deltaMV] /180. ;
        Mx = variableValue[MxV] ;
        calTheta();
        return yes;
	}
};

void  oblique::calD(){
	Mx2 = Mx*Mx;
	Mx4 = Mx2*Mx2;
	double tmpDelta;
	tmpDelta = delta;
	sinDelta= sin(delta);
	cosDelta= cos(delta);
	a1 = - (Mx2 + 2.0 )/Mx2 - k * sinDelta*sinDelta;
	a2 = (2.0*Mx2 +1.) /Mx4 + (0.25*(k+1.)*(k+1.) + (k -1.)/Mx2 )* 
				sinDelta*sinDelta  ;
	a3  = - cosDelta*cosDelta / Mx4;   
	R = (9.0 * a1*a2 - 27*a3 - 2.0 *a1*a1*a1)/ 54.0; //change 54 to multiply
	Q = ( 3.*a2- a1*a1) / 9.0 ;
	D = Q*Q*Q + R*R ;
	if (isDknown == no) {
		D = Q*Q*Q + R*R ;
	}
	if (outputInfo == infoObliqueMin || outputInfo == infoD0 ){
		return ;
	}
	else if (delta == 0.) {
		D = 0.0;
		return;
	}
};

int   oblique::calTheta(void){
	//int sign;
	if (delta == 0.0){
		theta[0]= theta[1] = 0.0;
		isObliquePossible = no;
		return no;
	}
	calD();
	if (D <= 0){
		chi = acos ( R / sqrt(-Q*Q*Q));
		x1 = asin(sqrt(2.*sqrt(-Q)*cos(THIRD*chi) - THIRD*a1)) ;
		x2 = asin(sqrt(2.* sqrt(-Q)* cos (THIRD*(chi + 2.*PI) ) 
					- THIRD*a1)) ;
		x3 = asin(sqrt(2.* sqrt(-Q)* cos (THIRD*(chi + 4.0*PI) ) 
					- THIRD*a1)) ;
		theta[0] = x1 ; //strong solution
		theta[2] = x2 ; //not possible solution
		theta[1] = x3 ; // weak solution
		isObliquePossible = yes;
		return yes;
	}
	else {
		isObliquePossible = no;
		return no;
	}	
} ;

int oblique::setVariableInRange (int * ioData, double * variableValue, 
		double h) {
	if ( ioData[variableNameN] == deltaMV ){
		Mx = variableValue[MxV] = h;
		delta = variableValue[deltaV] ;
		return yes;
	}
	else if ( ioData[variableNameN] == thetaMV ){
		theta[0] = variableValue[thetaV] = h;
		Mx = variableValue[MxV] ;
		return yes;
	}
	else {
		return no;
	}
};

inputType oblique::selectVariable (int * ioData) {
	inputType variableName = inputType(ioData[variableNameN]);
    if  (variableName == deltaMV  ) { return deltaMV ; }  
    else if  (variableName == MxV  ) { return MxV ; }  
	else { return _noV;};

} ;

void    oblique::makeTexTableHead(int * ioData) {
                                                                               
    int variableNumber;
	if (isObliquePossible == no) {
		cout << " no oblique shock is possible for this case" << endl ;
		return ;
	}
                                                                               
    if (outputInfo == infoStandard || outputInfo == infoShock) {
        variableNumber = 8;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "M_1";
        variableNames[1] = (char *) "M_x";
        variableNames[2] = (char *) "{M_y}_s";
        variableNames[3] = (char *) "{M_y}_w";
        variableNames[4] = (char *) "\\theta_s";
        variableNames[5] = (char *) "\\theta_w";
        variableNames[6] = (char *) "\\delta ";
        variableNames[7] = (char *) "{P_0}_y \\over {P_0}_x  ";

        showVariableTeXHead(variableNames,variableNumber);
		return ;
    }
    else if (outputInfo == infoMax && isObliquePossible == yes  ) {
		variableNumber = 7;
		// only weak shock for now
		char * variableNames[variableNumber];
		variableNames[0] = (char *) "M_x";
		variableNames[1] = (char *) "{M_y}_{max}";
		variableNames[2] = (char *) "\\theta_{max}";
		variableNames[3] = (char *) "\\delta ";
		variableNames[4] = (char *) "{P_y} \\over {P_x}  ";
		variableNames[5] = (char *) "{T_y} \\over {T_x}  ";
		variableNames[6] = (char *) "{P_0}_y \\over {P_0}_x  ";

		showVariableTeXHead(variableNames,variableNumber);
		return;
    }
	else if (outputInfo == infoMinimal ) {
		variableNumber = 7;
		char * variableNames[variableNumber];
		variableNames[0] = (char *) "M_x";
		variableNames[1] = (char *) "{M_y}_w";
		variableNames[2] = (char *) "\\theta_w";
		variableNames[3] = (char *) "\\delta ";
		variableNames[4] = (char *) "{P_y} \\over {P_x}  ";
		variableNames[5] = (char *) "{T_y} \\over {T_x}  ";
		variableNames[6] = (char *) "{P_0}_y \\over {P_0}_x  ";
		showVariableTeXHead(variableNames,variableNumber);
		return ;
    }
	else  if (outputInfo == infoObliqueMax ) {
		int variableNumber = 4;
		char * variableNames[variableNumber];
		cout << "The maximum values are:" << endl;
		variableNames[0] = (char *) "M_x";
		variableNames[1] = (char *) "M_y";
		variableNames[2] = (char *) "\\delta_{max}";
		variableNames[3] = (char *) "\\theta_{max}";
		showVariableTeXHead ( variableNames, variableNumber);
	}
}

void    oblique::makeTexTableLine(int * ioData) {
    int numberVariables;

    if (isObliquePossible == no) {
        cout << "no oblique shock is possible for this case" << endl ;
        return ;
    }

    if (outputInfo == infoStandard || outputInfo == infoShock) {
        numberVariables = 8;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M ;
        variableToBePrinted[1] = Mx ;
        variableToBePrinted[2] = Myn[1];
        variableToBePrinted[3] = Myn[0];
        variableToBePrinted[4] = theta[1]*180./PI;
        variableToBePrinted[5] = theta[0]*180./PI ;
        variableToBePrinted[6] = delta*180./PI;
        variableToBePrinted[7] = P0yP0x;

        showVariableTeXLine (variableToBePrinted, numberVariables);
    }
    else if (outputInfo == infoMax && isObliquePossible == yes  ) {
        numberVariables = 7;
		// only weak shock for now
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx ;
        variableToBePrinted[1] = Myn[0];
        variableToBePrinted[2] = theta[0]*180./PI ;
        variableToBePrinted[3] = delta*180./PI;
        variableToBePrinted[4] = PyPx;
        variableToBePrinted[5] = TyTx;
        variableToBePrinted[6] = P0yP0x;

        showVariableTeXLine(variableToBePrinted, numberVariables);
		return;
    }
    else if (outputInfo == infoMinimal ) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx ;
        variableToBePrinted[1] = Myn[0];
        variableToBePrinted[2] = theta[0]*180./PI ;
        variableToBePrinted[3] = delta*180./PI;
        variableToBePrinted[4] = PyPx;
        variableToBePrinted[5] = TyTx;
        variableToBePrinted[6] = P0yP0x;
                                                                                
        showVariableTeXLine (variableToBePrinted, numberVariables);
    }

	else  if (outputInfo == infoObliqueMax ) {
        numberVariables = 4;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx;
        variableToBePrinted[1] = Myn[0];
        variableToBePrinted[2] = delta*180./PI;
        variableToBePrinted[3] = theta[0]*180./PI;
        showVariableTeXLine ( variableToBePrinted, numberVariables);
    }
}

void oblique::calNormalComponent(void){
	for (int i = 0 ; i < 2 ; i++) {
		//Mxn[i] = Mx*sin(theta[i]);
		Mxn[i] = Mx*sin(theta[i]);
		if (theta[i] == 0.0 ){
				Myn[i] = 0.;
		}
		else {
			Myn[i] = calMy(Mxn[i])/sin((theta[i]-delta));
			//Myn[i] = calMy(Mxn[i]);
		}
	}
	P0yP0x = calTotalPressureRatio( Mxn[0] );
	return;
};

int   oblique::calAll( int * ioData, double * variableValue ){
	if ( calTheta(inputInfo, variableValue) == no ){
		return no;
	} 
	if  (inputInfo == deltaThetaV) {
		calMach(inputInfo, variableValue) ;
	}
	else if (inputInfo == MxV) {
		//all is done	
	}
	else if ( inputInfo == deltaMV ){
		delta  = PI * variableValue[deltaMV] /180. ;
		Mx = variableValue[MxV] ;
		calTheta();
	} 
	else if (inputInfo != deltaMV)  {
		calDelta() ;
	}
	calNormalComponent();
	//calAreaRatio();
	if ( (inputInfo != PbarV) || (inputInfo != TbarV) ) { 
		My = Myn[0];
		calPressureRatio();
	}
//	calTotalPressureRatio();
	calDensityRatio ();
	return yes;
}

int oblique:: calTableLine (int *ioData , double * variableValue){
	if (outputInfo == infoStandard || outputInfo == infoMinimal ){
		return calAll( ioData,  variableValue);
	}
	else if (outputInfo == infoRange ) {
		return calAll(ioData, variableValue);
	}
	else if (outputInfo == infoMax ) {
		return calAll(ioData, variableValue);
	}
	else if (outputInfo == infoObliqueMax ) {
		//change variable name and value
		//ioData[variableNameN] = MxV;
		calMaxDelta(ioData, variableValue );
		return yes;
	}
    else if (outputInfo == infoObliqueMin ) {
        calMinDelta(inputInfo, variableValue );
        return yes;
    }
    else if (outputInfo == infoD0 ) {
		Mx = variableValue[machV];
		delta =0.0 ;
		calD ();
        //calMinDelta(variableName, variableValue );
        return yes;
    }
    return no;
};

int oblique::calMxForDeltaTheta(){
	double overMx2, sinTheta, sinTheta2, tanTheta, cotDelta;
	if (theta[0] < delta) {
		return no;
	}
	else if (inputInfo == thetaMV){
		minMx = Mx ;
		return yes;
	}
	else{
		sinTheta = sin(theta[0]) ;
		sinTheta2 = sinTheta * sinTheta;
		tanTheta = tan (theta[0]) ;
		cotDelta = 1./tan(delta) ;
		overMx2 = sinTheta2 - 0.5*(k+1)* (tanTheta/(tanTheta+cotDelta));
		double tmpMx;
		Mx = sqrt(1./overMx2);
		tmpMx = Mx;
		minMx = Mx ; //for min calculation of Mx
		tmpMx += 0.;
		return yes;
	}
}

int oblique::calMinMx()
{
	//select small angle 
	delta = 0.1*PI/180. ;
	//theta[0] =  19.*PI /180.;
	calMxForDeltaTheta(); 
	return yes;
}

void oblique::calMaxDelta(int* ioData, double * variableValue ){
	double Mx2, Mx4, Mn1;
	Mx = variableValue[MxV]; 
	Mx2 = Mx*Mx;
	Mx4 = Mx2*Mx2;
	Mn1 = sqrt(sqrt((k+1)*(k+1)*Mx4 +8*(k*k-1)*Mx2+16*(k+1))+(1+k)*Mx2 -4)
			/(2*sqrt(k));
	theta[0] = theta[1] = theta[2] = asin( Mn1 /Mx);
	calDelta();
	//double tmpDelta;
	//delta =  root ( calMaxDeltaEstmt, this, 0.001, PI*0.25 , Mx);
	//do { // grantee negative D
	//	calD();
	//	delta = delta - 0.05 *EPS ;
	//} while ( D >= 0.0 ) ;
	
	maxDelta = delta ;
	double maxDeltaTMP;
	maxDeltaTMP =  delta *180./PI;
	isDknown = yes;
	//calD();
	//D = 0 ;
	//calTheta();
	calNormalComponent();
};

void oblique::calM1n(){ //Menikoff's eq
	double M1n;
	double Mx2=Mx*Mx;
	//Mx2 *(k+1)*(M^2+1) = 2*(k*M^4 +2*M^2 - 1) // menikoff's eq itself
	double aaa;
	aaa = sqrt((Mx2*((Mx2*((k*(k + 2.)) + 1.)) + (8.*((k*k) - 1.)))) +
				(16.*(1. + k))) ;
	M1n =  sqrt( 0.25*Mx2 + (0.25* (Mx2 +  aaa ) - 1.)/k ) ;
	theta[0] = asin (M1n/Mx);
}

void oblique::calMinTheta(int * ioData ,
             double * variableValue ){
	Mx = variableValue[MxV];
	minTheta = 180* asin(1./ Mx)/PI + 0.1;
};

void oblique::calMinDelta(inputType variableName,
             double * variableValue ){
	double tmpDelta;
	Mx = variableValue[machV]; 
    tmpDelta =  root ( calMinDeltaEstmt, this, 0.0,  0.01,  Mx);

	delta = tmpDelta + EPS ;
	calTheta();
	calMy() ;
	delta = tmpDelta;

};

int   oblique::doAllLines (int * ioData, double * variableValue ){
	int howManyPoints ;
	double range [500];
	int oneBeoreEnd = 0 ;
	if (outputInfo == infoRange ){
		if (ioData[variableNameN] == thetaMV ){
			calMinTheta(ioData, variableValue) ;
			range[3*oneBeoreEnd+1] = 30. ; 
			range[3*oneBeoreEnd+2] = minTheta ; 
			range[3*oneBeoreEnd+3] = 10*(int(minTheta/10) + 1) ; 
			oneBeoreEnd++;
			range[3*oneBeoreEnd+1] = 20. ; 
			range[3*oneBeoreEnd+2] = range[3*oneBeoreEnd] +1. ; 
			range[3*oneBeoreEnd+3] = 75.0 ; 
			oneBeoreEnd++;
			range[3*oneBeoreEnd+1] = 20. ; 
			range[3*oneBeoreEnd+2] = 75. ; 
			range[3*oneBeoreEnd+3] = 90.-EPS  ; 
			range[0] = ++oneBeoreEnd;
			setRange( ioData, range, variableValue);
			return yes;
		}
		else if (ioData[variableNameN] == deltaMV ){
			range[3*oneBeoreEnd+1] = 400. ; 
			range[3*oneBeoreEnd+2] = 1.0 ; 
			range[3*oneBeoreEnd+3] = 5.0 ; 
			oneBeoreEnd++;
			range[3*oneBeoreEnd+1] = 250. ; 
			range[3*oneBeoreEnd+2] = 5.0 ; 
			range[3*oneBeoreEnd+3] = 10.0 ; 
			oneBeoreEnd++;
			range[3*oneBeoreEnd+1] = 200. ; 
			range[3*oneBeoreEnd+2] = 10.0 ; 
			range[3*oneBeoreEnd+3] = 50.0 ; 
			range[0] = ++oneBeoreEnd;
			setRange( ioData, range, variableValue);
			return yes;
		}
	}
	else if (outputInfo == infoObliqueMax) {
		range[3*oneBeoreEnd+1] = 100. ; 
		range[3*oneBeoreEnd+2] = 1.0 ; 
		range[3*oneBeoreEnd+3] = 2.0 ; 
		oneBeoreEnd++;
		//this is the supersonic branch between 1 and 5
		range[oneBeoreEnd*3+1] = 149. ;
		range[oneBeoreEnd*3+2] = 2.02 ;
		range[oneBeoreEnd*3+3] = 5. ;
		oneBeoreEnd++;
		range[oneBeoreEnd*3+1] = 124. ;
		range[oneBeoreEnd*3+2] = 5.04 ;
		range[oneBeoreEnd*3+3] = 10.0 ;
		oneBeoreEnd++;
		range[oneBeoreEnd*3+1] = 599. ;
		range[oneBeoreEnd*3+2] = 10.1 ;
		range[oneBeoreEnd*3+3] = 70.0 ;
		
		range[0] = double (oneBeoreEnd+1);
		
		//set the variable name 
		ioData[variableNameN] =  MxV; 
		setRange( ioData, range, variableValue);
		return yes;
		//howManyPoints = 10 ;
		//makeLines(ioData,howManyPoints, 1.0,  2.0,  variableValue);
		//howManyPoints = 9 ;
		//makeLines(ioData,howManyPoints, 2.2,  4.0,  variableValue);
		//howManyPoints = 5 ;
		//makeLines(ioData,howManyPoints, 5.0,  10.0,  variableValue);
	}
    else if (outputInfo == infoD0) {
        howManyPoints = 9 ;
         makeLines(ioData,howManyPoints, 1.1,  2.0,  variableValue);
        howManyPoints = 9 ;
         makeLines(ioData,howManyPoints, 2.2,  4.0,  variableValue);
        howManyPoints = 5 ;
         makeLines(ioData,howManyPoints, 5.0,  10.0,  variableValue);
    }

    return yes;
}

void    oblique::makeHTMLTableHead(int * ioData){
    int variableNumber;

    if (outputInfo == infoStandard || outputInfo == infoMinimal) {
        variableNumber = 8;
        char * variableNames[variableNumber];
        variableNames[0] = (char*) "Mx  ";
        variableNames[1] = (char*) "My_s";
        variableNames[2] = (char*) "My_w";
        variableNames[3] = (char*) "&theta;_s";
        variableNames[4] = (char*) "&theta;_w";
        variableNames[5] = (char*) "&delta;";
        variableNames[6] = (char*) "D";
        variableNames[7] = (char*) "P0y/P0x";
        showVariableHTMLHead(variableNames,variableNumber);
    }
    else if (outputInfo == infoObliqueMax ) {
        variableNumber = 4;
        char * variableNames[variableNumber];
        variableNames[0] = (char*) "Mx  ";
        variableNames[1] = (char*) "My";
        variableNames[2] = (char*) "&theta;";
        variableNames[3] = (char*) "&delta;";
        showVariableHTMLHead(variableNames,variableNumber);
    }
};

void    oblique::makeHTMLTableLine(int * ioData) {
    int numberVariables;

    if (outputInfo == infoStandard || outputInfo == infoMinimal) {
        numberVariables = 8;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx ;
        variableToBePrinted[1] = Myn[1];
        variableToBePrinted[2] = Myn[0];
        variableToBePrinted[3] = theta[1]*180./PI;
        variableToBePrinted[4] = theta[0]*180./PI ;
        variableToBePrinted[5] = delta*180./PI;
        variableToBePrinted[6] = D;
        variableToBePrinted[7] = P0yP0x;
        showVariableHTMLLine (variableToBePrinted, numberVariables);
    }
    else if (outputInfo == infoObliqueMax ) {
        numberVariables = 4;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx ;
        variableToBePrinted[1] = Myn[0];
        variableToBePrinted[2] = theta[0]*180./PI;
        variableToBePrinted[3] = delta*180./PI;
        showVariableHTMLLine (variableToBePrinted, numberVariables);
    }
};
