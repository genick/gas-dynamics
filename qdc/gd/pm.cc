/********************************************************
 * last change Mon Dec 19 09:33:33 CST 2011 cast conversion from string constant to ‘char*’
 * for the new compiler
**********************************************************************
	FILE:pm.cc Prandlt-Meyer function

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  pm wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and pm wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip>
#  include "stagnation.h"
#  include "externalFun.h"
#  include "pm.h" 
#  include "gasDynamics.h" 
using namespace std;

/// pm class definition 
//******************************************************************

void    pm::showResults(int * ioData) {
	int numberVariables;
	if (outputInfo == infoStandard   ) {
		numberVariables = 6;
		double variableToBePrinted[numberVariables];
		variableToBePrinted[0] = M ;
		variableToBePrinted[1] = nu*180./PI;
		variableToBePrinted[2] = Pbar;
		variableToBePrinted[3] = Tbar;
		variableToBePrinted[4] = Rbar;
		variableToBePrinted[5] = atan(M)*180./PI;
		showVariable(variableToBePrinted, numberVariables);
		return;
	}
};

void    pm::showHead(int * ioData){
	cout << endl << endl <<  "Prandtl Meyer Function " << "\t\t"  ;
	cout << "k = " << k << endl << endl ;
	if (outputInfo == infoStandard ) {
		int variableNumber = 6;
		char * variableNames[variableNumber];
		variableNames[0] = (char *) "M  ";
		variableNames[1] = (char *) "Nu";
		variableNames[2] = (char *) "Pbar";
		variableNames[3] = (char *) "Tbar";
		variableNames[4] = (char *) "Rbar";
		variableNames[5] = (char *) "Mu";
		showVariableHead( variableNames, variableNumber);
	}
};

void  pm::calMach( int * ioData, double * variableValue){
	inputType variableName =  (inputType) ioData[variableNameN];
	if (variableName == machV) {
		M = variableValue[machV];
	}
	else if (variableName == angleV || variableName == nuV) {
		M =  root ( calNuEstmt, this, 1.0001, 100. , nu);
	}
	else if (variableName == PbarV) {
        M =  root ( calPbarEstmt, this, 1.0001, 100. , Pbar);
    }
	else if (variableName == TbarV) {
        M =  root ( calTbarEstmt, this, 1.0001, 100. , Tbar);
    }
	else if (variableName == RbarV) {
        M =  root ( calRbarEstmt, this, 1.0001, 100. , Rbar);
    }

};

int  pm::calNu(int* ioData, double * variableValue){
	inputType variableName =  (inputType) ioData[variableNameN];
	if (variableName == machV) {
		M = variableValue[machV] ;
		calNu();
		return yes; 
    }
	else if  (variableName == angleV ){
		nu = variableValue[angleV]*PI/180. ;
		calMach(ioData, variableValue);
		return yes; 
	}
	else if  ( variableName == nuV){
		nu = variableValue[nuV]*PI/180. ;
		calMach(ioData,  variableValue);
		return yes; 
	}
	else if  (variableName == PbarV){
        Pbar = variableValue[PbarV] ;
        calMach(ioData, variableValue);
		calNu();
        return yes;
    }
	else if  (variableName == TbarV){
		Tbar = variableValue[TbarV] ;
		calMach(ioData,  variableValue);
		calNu();
		return yes;
    }
	else if  (variableName == RbarV){
		Rbar = variableValue[RbarV] ;
		calMach(ioData, variableValue);
		calNu();
		return yes;
    }
	else if  (variableName == PARV){
		stagnation::calMach(ioData, variableValue);
		calNu();
		return yes;
    }
	else if  (variableName == AstarV){
		stagnation::calMach(ioData, variableValue);
		calNu();
		return yes;
    }
	return no;
};

int   pm::calNu(void){
	nu = sqrt( (k+1.0) / (k-1.0) ) *atan (sqrt(  (k-1.0)* 
			(M*M -1.)/ (k+1.0) ))  -  atan (sqrt(M*M -1.));
	return yes;	
} ;

inputType pm::selectVariable (inputType variableName) {
    if  (variableName == machV  ) { return machV ; }  
    if  (variableName == angleV  ) { return angleV ; } //for nu  
	else { return _noV;};

} ;

void	pm::makeTexTableHead(int * ioData) {

	int variableNumber;
	if (outputInfo == infoStandard  ) {
		variableNumber = 6;
		char * variableNames[variableNumber];
		variableNames[0] = (char *) "M";
		variableNames[1] = (char *) "\\nu";
		variableNames[2] = (char *) "P \\over P_0";
		variableNames[3] = (char *) "T \\over T_0";
		variableNames[4] = (char *) "\\rho \\over \\rho_0";
		variableNames[5] = (char *) "\\mu ";

		showVariableTeXHead(variableNames,variableNumber);
		return ;
	}
}

void    pm::makeTexTableLine(int * ioData ) {
	int numberVariables;
	if (outputInfo == infoStandard ) {
		numberVariables = 6;
		double variableToBePrinted[numberVariables];
		variableToBePrinted[0] = M ;
		variableToBePrinted[1] = nu*180./PI;
		variableToBePrinted[2] = Pbar;
		variableToBePrinted[3] = Tbar;
		variableToBePrinted[4] = Rbar;
		variableToBePrinted[5] = atan(M)*180./PI ;

		showVariableTeXLine (variableToBePrinted, numberVariables);
	}
}

void pm::calNormalComponent(void){
	return;
};

int   pm::calAll(int * ioData, double * variableValue ){
	
	calNu( ioData, variableValue); 
	calPressureRatio();
	calTemperatureRatio();
	calDensityRatio ();
//	if (variableName != TbarV){
//		calTemperatureRatio();
//	}
//	calTotalPressureRatio();
	return yes;
}

int pm:: calTableLine (int * ioData, double * variableValue){
    if (outputInfo == infoStandard ){
            return calAll(ioData, variableValue);
    }
    return no;
};

void pm::calMaxDelta(inputType variableName,
             double * variableValue ){
};

int   pm::doAllLines (int * ioData, double * variableValue ){

	double range [500];
	int oneBeoreEnd = 0 ;
	range[3*oneBeoreEnd+1] = 100. ; 
	range[3*oneBeoreEnd+2] = 1.0 ; 
	range[3*oneBeoreEnd+3] = 2.0 ; 
	oneBeoreEnd++;
	//this is the supersonic branch between 1 and 5
	range[oneBeoreEnd*3+1] = 149. ;
	range[oneBeoreEnd*3+2] = 2.02 ;
	range[oneBeoreEnd*3+3] = 5. ;
	oneBeoreEnd++;
	range[oneBeoreEnd*3+1] = 124. ;
	range[oneBeoreEnd*3+2] = 5.04 ;
	range[oneBeoreEnd*3+3] = 10.0 ;
	oneBeoreEnd++;
	range[oneBeoreEnd*3+1] = 599. ;
	range[oneBeoreEnd*3+2] = 10.1 ;
	range[oneBeoreEnd*3+3] = 70.0 ;
	
	range[0] = double (oneBeoreEnd+1);
	
	setRange( ioData, range, variableValue);
	return yes;

//	int howManyPoints ;
//	if (outputInfo == infoStandard ){
//		howManyPoints =  9 ;
//		makeLines(ioData,howManyPoints, 1.0, 1.9, variableValue);
//		howManyPoints =  8 ;
//		makeLines(ioData,howManyPoints, 2., 10.,  variableValue);
//	}
//   return yes;
}


void    pm::makeHTMLTableHead( int * ioData ){
    int variableNumber;
                                                                               
    if (outputInfo == infoStandard || outputInfo == infoMinimal) {
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "M  ";
        variableNames[1] = (char *) "&nu;";
        variableNames[2] = (char *) "Pbar";
        variableNames[3] = (char *) "Tbar";
        variableNames[4] = (char *) "&rho;bar";
        variableNames[5] = (char *) "&mu;";

        showVariableHTMLHead(variableNames,variableNumber);
    }
};

void    pm::makeHTMLTableLine(int * ioData ) {
    int numberVariables;

    if (outputInfo == infoStandard || outputInfo == infoMinimal) {
        numberVariables = 6;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = M ;
        variableToBePrinted[1] = nu*180./PI;
        variableToBePrinted[2] = Pbar;
        variableToBePrinted[3] = Tbar;
        variableToBePrinted[4] = Rbar;
        variableToBePrinted[5] = atan(M)*180./PI;

        showVariableHTMLLine (variableToBePrinted, numberVariables);
    }
};

void    pm::makeHTMLTableTail(int * ioData) {
	compressibleFlow::makeHTMLTableTail(ioData);		
};
