/********************************************************
	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
//#	include <stdio.h>
//#	include <stdlib.h>
#  include <cmath>
#  include <iostream>
#  include <iomanip> 
#  include "compressibleFlow.h"
#  include "shock.h"
#  include "pipeFlow.h"
#  include "fanno.h"
#  include "definition.h"
#  include "isothermal.h"
#  include "rayleigh.h"
#  include "stagnation.h"
#  include "isothermalNozzle.h"
#  include "externalFun.h"
#  include "oblique.h"
#  include "shockDynamics.h"
#  include "shockTube.h"
#  include "twoChambers.h"
#  include "pm.h"
#  include "inputData.h"


