/********************************************************
	FILE:externalFun.cc

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip>
#	include  <limits>
#  include "stagnation.h"
#  include "externalFun.h"
#  include "fanno.h" 
#  include "gasDynamics.h" 
using namespace std;

//general functions utilizing the class definitions
//**************************************************
double calPxpEstmt
		(compressibleFlow * c, double estimatedMM, double  value){
	c->setMach (estimatedMM+value) ;
	c->calMy () ;
	c->calPressureRatio () ;
	c->calTemperatureRatio () ;
	return(  c->getMx() - value - c->getMy()* sqrt(c->getTemperature()) );
}

double calP0yP0xEstmt
		(compressibleFlow * c, double estimatedMM, double  value){
	c->setMach (estimatedMM) ;
	c->calMy () ;
	c->calPressureRatio () ;
	c->calTotalPressureRatio();
	return(value - c->getP0yP0x() );
}

double calR0yR0xEstmt
		(compressibleFlow * c, double estimatedMM, double  value){
	c->setMach (estimatedMM) ;
	c->calMy () ;
	c->calPressureRatio () ;
	c->calTemperatureRatio ();
	c->calDensityRatio();
	c->calTotalDensityRatio();
	return(value - c->getR0yR0x() );
}

double calT0yT0xEstmt
		(compressibleFlow * c, double estimatedMM, double  value){
	c->setMach (estimatedMM) ;
	c->calMy () ;
	c->calPressureRatio () ;
	c->calTemperatureRatio () ;
	c->calTotalTemperatureRatio();
	return(value - c->getT0yT0x() );
}

double calPAREstmt 
		(compressibleFlow * c, double estimatedMM, double  value){
    c->setMach (estimatedMM) ;
	c->calAreaRatio();
	c->calPressureRatio();
    c->calPARRatio();
	return(value - c->getPAR() );	
};
double calAstarEstmt 
		(compressibleFlow * c, double estimatedMM, double  value){
    c->setMach (estimatedMM) ;
    c->calAreaRatio();
	return(value - c->getArea() );	
};

double calP1P5Estmt 
		(compressibleFlow * c, double estimatedMM, double  value){
    c->setMx (estimatedMM) ;
    c->calP5P1();
    return (value - c->getP5P1() ) ;
};

double calPbarEstmt 
		(compressibleFlow * c, double estimatedMM, double  value){
    c->setMach (estimatedMM) ;
    c->calMy();// for shock case
    c->calPressureRatio();
    return (value - c->getPressure() ) ;
};

double calRbarEstmt 
		(compressibleFlow * c, double estimatedMM, double  value){
    c->setMach (estimatedMM) ;
    c->calMy();// for shock case
    c->calPressureRatio();
    c->calTemperatureRatio();
    c->calDensityRatio();
    return (value - c->getDensity() ) ;
};

double calTbarEstmt 
		(compressibleFlow * c, double estimatedMM, double  value){
    c->setMach (estimatedMM) ;
    c->calMy();// for shock case
    c->calPressureRatio();
    c->calTemperatureRatio();
    return (value - c->getTemperature() ) ;
};

double calTRstarEstmt 
		(compressibleFlow * c, double estimatedMM, double  value){
    c->setMach (estimatedMM) ;
    c->calTemperatureRatio();
    return (value - c->getTRstar() ) ;
};

double calPRzeroStarEstmt 
		(compressibleFlow * c, double estimatedMM, double  value){
    c->setMach (estimatedMM) ;
    c->calTotalPressureRatio();
    return (value - c->getPRzeroStar() ) ;
};

double calTRzeroStarEstmt 
		(compressibleFlow * c, double estimatedMM, double  value){
    c->setMach (estimatedMM) ;
    c->calTotalTemperatureRatio();
    return (value - c->getTRzeroStar() ) ;
};

double calShockLocationM_Estmt 
		(compressibleFlow * c, double estimatedShockLocation, double  value){
	//calculated a correcting to estimated Shock Location 
	double FLD; //the total FLD given
	double upStreamFLD;//,downStreamFLD;// estimated fld where shock occurs
	double  downStreamFLDcal;// cal extra fld
	double k, Mx, My;
	//double M1cal, M1original;
	double maxPossibleFLDsup;
	FLD = c->getFLD();
	k =c->getk();		
	shock s(k);
	stagnation st(k);
	//cal left side
	maxPossibleFLDsup = c->getmaxPossibleFLDsup();
	upStreamFLD = maxPossibleFLDsup - estimatedShockLocation ;
	Mx = c->calMForFLD(upStreamFLD, 10. ); //in sup branch 
	My = s.calMy(Mx);
	downStreamFLDcal = c->calFLD( My); //after the shock
	return (downStreamFLDcal + estimatedShockLocation-maxPossibleFLDsup
		- value); 
	
	//downStreamFLD = FLD - upStreamFLD;
	//start with downstream branch 
	//My = c->calMForFLD(downStreamFLD, 0.1 ); //in sub branch 
	//Mx = s.calMy(My);
	//superSonic branch 
	//calculated the FLD if this Mx is correct
	//downStreamFLDcal = c->calFLD( Mx);
	//FLDcal = upStreamFLD + downStreamFLDcal;
	//M1cal = c->calMForFLD(FLDcal, 10.0 ); //in sup branch
	//M1original = c->getM1();

	//return (M1original - M1cal);
};

double calShockLocation_Estmt 
		(compressibleFlow * c, double estimatedShockLocation, double  value){
	//calculated a corrding to estimated Shock Location 
	double FLD; //the total FLD given
	double upStreamFLD, downStreamFLD;// estimated fld(s) where shock occurs
	double k, Mx, My;
	double downStreamFLDcal; //calculated the FLD for this shockLocation 
	double M1cal, M1original;
	double FLDcal; 		
	FLD = c->getFLD();
	upStreamFLD = estimatedShockLocation * FLD;
	downStreamFLD = FLD - upStreamFLD;
	k =c->getk();		
	shock s(k);
	stagnation st(k);
	//start with downstream branch 
	My = c->calMForFLD(downStreamFLD, 0.1 ); //in sub branch 
	Mx = s.calMy(My);
	//superSonic branch 
	//calculated the FLD if this Mx is correct
	downStreamFLDcal = c->calFLD( Mx);
	FLDcal = upStreamFLD + downStreamFLDcal;
	M1cal = c->calMForFLD(FLDcal, 10.0 ); //in sup branch
	M1original = c->getM1();

	return (M1original - M1cal);
};

double calFLD_Estmt (compressibleFlow * c,  double estimatedMach, double  value){
	double tmpFLD;
    //c->changeMach (estimatedMach) ;
    //s.calArea();
    tmpFLD = c->calFLD(estimatedMach) ;
    //s.calPressureRatio() ;
    //s.calTotalPressureRatio() ;
    //s.calTotalTemperatureRatio() ;
    //s.calDensityRatio() ;
    return (value - tmpFLD ) ;
};

double calP2P1_Estmt (  compressibleFlow * c, double M1est, double fld) {

	double  M2est; //the value est of the mach at the exit;
	double P1PstarEst, P2PstarEst ;// the estimated p1/P*, p2/P* 
	double tmpFLD ; // estimated FLD
	double newFLD ; // the new estimated  FLD for M2
	double p2p1est;
	double p2p1Given; // the given p2/p1
	
	//calculate the P1PstarT for M1
    c->changeMach(M1est);
	c->calPressureRatio(); //calculate the pressure ratio p1/p*
    P1PstarEst  = c->getPRstar(); // assigned the ratio to a tmp variable
	
	// need to find M2
	// calculate the "leftover" of  fld to M2
	tmpFLD = c->calFLD(M1est);
	newFLD = tmpFLD - fld ;
		
	// calculate M2 
	M2est = c->calMForFLD(newFLD,M1est);
	//note that the function calculate relationship M to fld
	// not M1 or M2
	c->changeMach(M2est);
    c->calPressureRatio(); // p2/p*
    P2PstarEst= c->getPRstar() ;
    p2p1est = P2PstarEst / P1PstarEst;
	p2p1Given = c->getP2P1() ;	
	return ( p2p1est - p2p1Given ) ;	

};

double calMinDeltaEstmt 
		(compressibleFlow * c, double estimatedDelta, double  value){
    c->setMach (value) ;
	c->setDelta(estimatedDelta);
    c->calD(infoObliqueMin);
    //return (value - c->getDelta() ) ;
	return (c->getD());
};

double calMaxDeltaEstmt 
		(compressibleFlow * c, double estimatedDelta, double  value){
    c->setMach (value) ;
	c->setDelta(estimatedDelta);
    c->calD(infoStandard); //any kind of info but infoObliqueMin
    //return (value - c->getDelta() ) ;
	return (c->getD());
};

double calNuEstmt 
		(compressibleFlow * c, double estimatedMach, double  value){
    c->setMach (estimatedMach) ;
	c->setAngle(value);
    c->calAngle();
    return (value - c->getAngle() ) ;
};

double calFU (compressibleFlow * c, double Uxdummy, double  value){
	double us1, us2, f ;
	c->setUx( Uxdummy);
	c->cal_cx () ;
	c->setMxcx(Uxdummy) ;
	c->calMy () ;
	c->calPressureRatio();
	c->calTemperatureRatio();
	c->calDensityRatio() ;
	us1 = c->getUx()/c->getUxUy() + c->getUyp() ; // y side
	us2 = c->getUxp() + c->getUx() ; // x side
	f = us1 - us2 ;
	return f ;
};

double calFUtwoChamber (compressibleFlow * c, double Uxdummy, double  value){
	double us1, us2, f ;
	c->setUx( Uxdummy);
	c->cal_cx () ;
	c->setMxcx(Uxdummy) ;
	c->calMy () ;
	c->calUxUy();
	us1 = c->getUx()/c->getUxUy() + c->getUyp() ; // y side
	us2 = c->getUxp() + c->getUx() ; // x side
	f = us1 - us2 ;
	return f ;
};

double root( double f (compressibleFlow *, double , double ),
            compressibleFlow * c , double a , double b, double value) {

	double Af =  f(c,a, value) ;
	double Bf =  f(c, b, value) ;

	double m = 0.5*( a + b )  ;
	double Mf =  f(c, m, value ) ;
	// if root is exist
	if ( Af * Bf  > 0.0 ) {
		// no root
	//try Secant Method
		return -1.0; 
	}

   if ( Mf == 0.0 || (b - a ) <= EPS ) {
        return m;
    }
    else if ( Af * Mf  < 0.0 ) {
        return root (f, c, a, m, value ) ;
    }
    else {
        return root(f, c, m, b, value);
    }
}

//public static double BrentsMethodSolve(Func<double, double> function, double lowerLimit, double upperLimit, double errorTol)
//double brent(double f(double , double , double , double ), double a , double b, double xiA, double k , double Pab) {
double brent( double f (compressibleFlow *, double , double ), compressibleFlow * CC , double a , double b, double value) {
	double c = 0.;
	double d = numeric_limits<double>::max( );
	double EPS = numeric_limits<double>::epsilon( ) *10000.0 ;

	double fa =  f(CC, a, value); 
	double fb =  f(CC, b, value);

	double fc = 0.;
	double s = 0.;
	double fs = 0.;
    // if f(a) f(b) >= 0 then error-exit
	if (fa * fb >= 0.) {
	if (fa < fb)
		return a;
	else
		return b;
    }
    // if |f(a)| < |f(b)| then swap (a,b) end if
	if (abs(fa) < abs(fb))
		{ double tmp = a; a = b; b = tmp; tmp = fa; fa = fb; fb = tmp; }
	c = a;
	fc = fa;
	bool mflag = true;
	int i = 0;
	while (!(fb==0) && (abs(a-b) > EPS)){
		if ((fa != fc) && (fb != fc))
			// Inverse quadratic interpolation
			s = a * fb * fc / (fa - fb) / (fa - fc) + b * fa * fc / (fb - fa) / (fb - fc) + c * fa * fb / (fc - fa) / (fc - fb);
		else
			// Secant Rule
			s = b - fb * (b - a) / (fb - fa);

		double tmp2 = (3 * a + b) / 4;
		if (
			(!(((s > tmp2) && (s < b)) || ((s < tmp2) && (s > b)))) || 
			(mflag && (abs(s - b) >= (abs(b - c) / 2))) || 
			(!mflag && (abs(s - b) >= (abs(c - d) / 2)))
			)
		{
			s = (a + b) / 2;
			mflag = true;
		}
		else
		{
			if ((mflag && (abs(b - c) < EPS)) || (!mflag && (abs(c - d) < EPS))) {
				s = (a + b) / 2;
				mflag = true;
			}
			else
				mflag = false;
		}
		fs = f(CC, s, value);
		d = c;
		c = b;
		fc = fb;
		if (fa * fs < 0) { b = s; fb = fs; }
		else { a = s; fa = fs; }

		// if |f(a)| < |f(b)| then swap (a,b) end if
		if (abs(fa) < abs(fb))
			{ double tmp = a; a = b; b = tmp; tmp = fa; fa = fb; fb = tmp; }
			i++;
        //if (i > 1000)
         //   throw new Exception(String.Format("Error is {0}", fb));
	}
	return b;
};
