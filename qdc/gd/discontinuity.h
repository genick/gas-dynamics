/********************************************************
	file: discontinuity.h

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/
#ifndef _DISCONTINUITY_STDLIB
#define _DISCONTINUITY_STDLIB

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip> 
#  include "definition.h"
#  include "compressibleFlow.h"

class discontinuity: public compressibleFlow {
        protected:
        public:
			// to change the initialization later on
			// so that base class initialization will be considered
			discontinuity(double spesificHeatRatio , FlowName dcn):
				//initialization of base class
				compressibleFlow(spesificHeatRatio, dcn)
					 { 
						// specific initialization process for the derived class
						//the critical M is 1 for shock flow
						M1maxSub = 1.0;
						singularPoint = M1maxSub ; 
			  		};
			discontinuity(double k1 , double k5, FlowName dcn):
				//initialization of base class
				compressibleFlow(k1, k5, dcn)
					 { 
						// specific initialization process for the derived class
						//the critical M is 1 for shock flow
						M1maxSub = 1.0;
						singularPoint = M1maxSub ; 
			  		};
			~discontinuity(){};
			//spesific functions of this model of flow to calculate
			//spesific parameters of this flow model.
			// 
            void setMach(double Mach) {Mx = Mach ;};
			void calMach( inputType, double * ) ;
			void calMy() ;
            void calMx () ;
			double calMy(double);
			virtual void calPressureRatio() ;
			virtual double calPressureRatio(double, double ) ; //PyPx f (Mx,My)
			void calDensityRatio();
			//void calVelocityRatio();
			void calTotalPressureRatio() ;
			double calTotalPressureRatio(double ) ;
			//void calTotalTemperatureRatio();
			virtual void calTemperatureRatio();
			virtual void calArea();
			virtual void makeTexTableHead(int * ) ;
			virtual void makeTexTableLine (int * ) ;
			virtual void calMx (inputType, double * ) ;
			virtual int calAll(inputType, double *);
			virtual int  doAllLines(int *, double *);
			virtual int  calTableLine (inputType, double * );


			//functions that do not touch the class variables
			//double calFLD(double) ;
			//double calPressureRatio(double) ;
			//double calDensityRatio(double);
			//double calVelocityRatio(double);
			//double calTotalPressureRatio(double) ;
			//double calTotalTemperatureRatio(double);
			//double calTemperatureRatio(double);
			//double calArea(double);
            virtual double getPressure();
            virtual double getTemperature (){return TyTx ; };
            virtual double getDensity (); /// {return RyRx ; };
            //double getArea (){return Area ; };

};
#endif  /* _DISCONTINUITY_STDLIB */
