/********************************************************
	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip> 
#  include "compressibleFlow.h"
#  include "pipeFlow.h"
#  include "shock.h"
#  include "externalFun.h" 
#  include "definition.h" 

#ifndef _FANNO_STDLIB
#define _FANNO_STDLIB

class fanno: public pipeFlow {
        protected:
        public:
			// to change the initialization later on
			// so that base class initialization will be considered
			//fanno(double Mach, double spesificHeatRatio ):
			fanno(double spesificHeatRatio ):
					//inilazation of base class
				//compressibleFlow(Mach,spesificHeatRatio)
				pipeFlow (spesificHeatRatio, fannoClass)
					 { 
						// specific initialization process for derived class
						//the speed of the chocked flow is 1 for fanno flow
						M1maxSub = 1.0;
						singularPoint = M1maxSub ; 
						//to change with k
						superSonicFLD = 0.821508116;
			  		};
			~fanno(){};
			//spesific functions of this model of flow to calculate
			//spesific parameters of this flow model.
			// 
			void calFLD();
			void calPressureRatio() ;
			void calDensityRatio();
			void calVelocityRatio();
			void calTotalPressureRatio() ;
			void calTotalTemperatureRatio();
			void calTemperatureRatio();
			void calArea();
			//functions that will not touch the class variables
			double calFLD(double) ;
			double calPressureRatio(double) ;
			double calDensityRatio(double);
			double calVelocityRatio(double);
			double calTotalPressureRatio(double) ;
			double calTotalTemperatureRatio(double);
			double calTemperatureRatio(double);
			double calArea(double);

			// specific functions to fanno flow to be add in presenting
			// the results

			void showHead(int );
			void showResults(int);
			void makeTexTableHead(int ); 
			void makeTexTableLine (int );
            int  calTableLine (int ,  int , double * );
			int calAll(int, int, double *);
			int  doAllLines (int, int, int, double *);
};

#endif  /* _FANNO_STDLIB */
