//last change
//initial Mon Mar 26 10:23:46 CDT 2012
/********************************************************
	file: twoChambers.h

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/
#ifndef _TWOCHAMBERS_STDLIB
#define _TWOCHAMBERS_STDLIB 

#  include <cstdio>
#	include <cstdlib>
#  include <cmath>
#  include <iostream>
#  include <iomanip> 
#  include "definition.h"
#  include "compressibleFlow.h"
#  include "fanno.h"

//class twoChambers: public discontinuity {
//

//class		twoChambers : public fanno , public shock {
class		twoChambers : public fanno {
		protected:
			//local functions and veriables to be access via other
			//functions 
			double Mbar, Mmax, Up, V1, V2, Upiston, t, deltaT , dm, T2;
		public:
			double P[15][10000]; // pressure index  time 0 chamber 1 chamber 2
			enum pArrayType {timeP, ch1P, ch2P, MbarP, UpistonP, V1P, m2m1, mBar1P, mBar2P, mTotalP, dmP, T1P, T2P};
			double P1, P2, Ubar, cycles, frequency, Vratio, Msmall , Vdelta, Vstroke, V10, accumolatedV;
			double Min, Mout;
			int iCyclesLimit ;
			// to change the initialization later on
			// so that base class initialization will be considered
			twoChambers(double spesificHeatRatio ):
				//initialization of base class
				//discontinuity (spesificHeatRatio, twoChambersFlow )
				//fanno (spesificHeatRatio ), shock (spesificHeatRatio)
				//t = 0.0; // time start is zero
				fanno (spesificHeatRatio )
					 {
						//specific initialization process for derived class
						M1maxSub = 1.0;
						V1 = 1.;
						compressibleFlow::flowModelName = "two Chambers";
						singularPoint = M1maxSub ; 
						t = 0.0;
			};
				
			~twoChambers(){};
			//spesific functions of this model of flow to calculate
			//spesific parameters of this flow model.
			// 
			void calM( double, double) ;
			void makeTexTableHead(int * ) ;
			void makeTexTableLine (int * ) ;
			int calMs1(); //calculate the Mach moving to the left 
			int calP();
			double calUpiston();
			int calAll( double *);
			int doAllLines( int *, double *);
			int calTableLine ( inputType, double * );
			//int calProfile ( int *, double * );
			int calTableLine ( int *, double * );
			void showResults(int *);
			void showHead(int *) ;
			inputType selectVariable (inputType)  ;

			//html functions
			void showVariableHTMLHead(char **, int);
			void makeHTMLTableHead(int *);
			void makeHTMLTableLine(int *);
			//functions that do not touch the class variables
			//double calFLD(double) ;
			//virtual double calPressureRatio() ;
			virtual void calPressureRatio() ;
			virtual double calMForFLD (double, double);
			//virtual double calFLD(double);
			virtual double calP2P1(double, double);
			virtual int calTemperatureAfterShock(int , double);
			virtual int calChamberTemperature( int );
			virtual void calMy() ;
			virtual void calDM(int, double );
			virtual void calTi(int, double );
			virtual void calUxUy() ;
			virtual double calP1(int ) ;
			virtual double calP2(int ) ;
			virtual void calP2Step(int, double, double ) ;
			int makeTable ( int * , double * ) ; 
			double calUpperLimitsMs1 ();
};
#endif  /* _TWOCHAMBERS_STDLIB */
