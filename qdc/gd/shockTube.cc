//initial release Tue Dec 20 10:54:15 CST 2011
//**********************************************************************
/********************************************************
	FILE:shockTube.cc

	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shockTube wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shockTube.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/

#  include <cstdio>
#  include <cmath>
#  include <iostream>
#  include <iomanip>
#  include "stagnation.h"
#  include "externalFun.h"
#  include "shockTube.h" 
#  include "gasDynamics.h" 
using namespace std;

/// shockTube class definition 
//******************************************************************

void    shockTube::showResults(int * ioData) {
    int numberVariables;
    if (outputInfo == infoStandard ) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = P5P1 ;
        variableToBePrinted[1] = Ms1;
        variableToBePrinted[2] = PyPx;
        variableToBePrinted[3] = TyTx;
        variableToBePrinted[4] = P5P3;
        variableToBePrinted[5] = T5T3;
        variableToBePrinted[6] = Us3; // sqrt(k5 *287 * T5/T5T3 ) ; // Us3;

        showVariable(variableToBePrinted, numberVariables);
		return;
    }
    else if (outputInfo == infoMax ) {
        numberVariables = 9;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx ;
        variableToBePrinted[1] = Mxp;
        variableToBePrinted[2] = My;
        variableToBePrinted[3] = Myp;
        variableToBePrinted[4] = TyTx;
        variableToBePrinted[5] = PyPx ;
        variableToBePrinted[6] = P0yP0x;
        variableToBePrinted[7] = Uyp;
        variableToBePrinted[8] = cx;

		showVariable(variableToBePrinted, numberVariables);
		return;
	}
	else  if (outputInfo == infoTubeProfile ) {
		numberVariables = 6;
		double variableToBePrinted[numberVariables];
		for (int i = 0 ; i < 70 ; i++){ 
			variableToBePrinted[0] = Mfld[7][i] ;
			variableToBePrinted[1] = Mfld[2][i] ;
			variableToBePrinted[2] = Mfld[3][i]  ;
			variableToBePrinted[3] = Mfld[4][i] ;
			variableToBePrinted[4] = Mfld[5][i] ;
			variableToBePrinted[5] = Mfld[6][i] ;
			showVariable(variableToBePrinted, numberVariables);
		}
		return;
	}
	else  if (outputInfo == infoIteration ) {
        numberVariables = 5;
        double variableToBePrinted[numberVariables];
		for (int i = 0 ; i < numberOfIterations ; i++){
			variableToBePrinted[0] = steps[i][1];
			variableToBePrinted[1] = steps[i][2];
			variableToBePrinted[2] = steps[i][3];
			variableToBePrinted[3] = steps[i][4];
			variableToBePrinted[4] = Myp;

			cout  << "	" << (int)  steps[i][0] << "  ";
			showVariable(variableToBePrinted, numberVariables);
		}
        return;
    }
	else  {
		return ;
	}
};

void    shockTube::showHead(int* ioData){
    cout << endl << endl <<  " Shock Tube" << "\t\t"  ;
    cout << "k1 = " << k1 << "\tk5 = " << k5 << endl << endl ;
                                                                               
    if (outputInfo == infoStandard ) {
        int variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "P5/P1 ";
        variableNames[1] = (char *) "Ms1";
        variableNames[2] = (char *) "P2/P1";
        variableNames[3] = (char *) "T2/T1";
        variableNames[4] = (char *) "P5/P3";
        variableNames[5] = (char *) "T5/T3";
        variableNames[6] = (char *) "Us5";

        showVariableHead( variableNames, variableNumber);
    }
    else if (outputInfo == infoMax ) {
        int variableNumber = 9;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "Mx ";
        variableNames[1] = (char *) "Mxp";
        variableNames[2] = (char *) "My";
        variableNames[3] = (char *) "Myp";
        variableNames[4] = (char *) "TyTx";
        variableNames[5] = (char *) "PyPx";
        variableNames[6] = (char *) "P0yP0x";
        variableNames[7] = (char *) "Uyp";
        variableNames[8] = (char *) "c_x";

        showVariableHead( variableNames, variableNumber);
    }
    else if (outputInfo == infoTubeProfile ) {
    	cout << " Tube Profile " << endl ; // "\t\t"  ;
    	//cout << "Us1 = " << Us1 << endl  ;
    	cout << "time = " << Mfld[0][1]  << endl  ;
		//cout << "L1 = " << Mfld[1][1] << endl   ; // location of the border zone 1 to 2
		//cout << "L2 = " << Mfld[1][2] << endl ; // location of the border zone 2 to 3
		//cout << "L3 = " << Mfld[1][3] << endl ; // location of the border zone 3 to 4
		//cout << "L4 = " << Mfld[1][4] << endl ; 
    	//cout << endl << endl <<  " Tube Profile " << endl ; // "\t\t"  ;
        int variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "x";
        variableNames[1] = (char *) "P/P1";
        variableNames[2] = (char *) "T/T1";
        variableNames[3] = (char *) "c";
        variableNames[4] = (char *) "U" ; 
        variableNames[5] = (char *)  "M" ; 
                                                                               
        showVariableHead( variableNames, variableNumber);
    }
    else  if (outputInfo == infoIteration ) {
        int variableNumber = 6;
        char * variableNames[variableNumber];
        cout << "The values at zero delta are:" << endl;
        variableNames[0] = (char *) "i    ";
        variableNames[1] = (char *) "Mx";
        variableNames[2] = (char *) "My";
        variableNames[3] = (char *) "TyTx";
        variableNames[4] = (char *) "PyPx";
        variableNames[5] = (char *) "Myp";

        showVariableHead( variableNames, variableNumber);
    }

};

void  shockTube::calMach(inputType variableName, double * variableValue){
    if (variableName == MxV) {
        Mx = variableValue[MxV];
    }
	else if (variableName == deltaThetaV) {
		M = 2. *(1.0/tan(theta[0]) + tan(delta))/
			(sin (2.* theta[0]) - tan(delta)*
					(k + cos (2.*theta[0]))) ;
		if (M > 0.) {
			M = sqrt(M);
			Mx = M *sin(theta[0]);
		}
		else {
		//no solution 
			
		}
	}
};

void  shockTube::calMx(inputType variableName, double * variableValue){
	double tmpMx;
    if (variableName == P0yP0xV) {
        P0yP0x = variableValue[P0yP0xV];
        M = root  ( calP0yP0xEstmt, this, M1maxSub, 100.0, P0yP0x) ;
		Mx = M * sin (theta[0]);
		return;
    }
	else if (variableName == MxV){ 
		Mx = variableValue[MxV] ;
		calMy();
		if ( Mx < 1.0) {
			tmpMx = Mx ;
			Mx = My;
			My = tmpMx;
		}
	}
    else if (variableName == machV){
        Mx = variableValue[machV] ;
        calMy();
        if ( Mx < 1.0) {
            tmpMx = Mx ;
            Mx = My;
            My = tmpMx;
        }
    }
}

void  shockTube::calMy(){
    My = sqrt((Mx*Mx + 2.0/(k -1.0)) /
        ((2.0*k / (k-1.0) ) *Mx*Mx -1.0)) ;
};

double  shockTube::calMy(double m){
    return  sqrt((m*m + 2.0/(k -1.0)) / ((2.0*k / (k-1.0) ) *m*m -1.)) ;
};

inputType shockTube::selectVariable (inputType variableName) {
    if  (variableName == deltaMV  ) { return deltaMV ; }  
    else if  (variableName == MxV  ) { return MxV ; }  
    else if  (variableName == MxpV  ) { return MxpV ; }  
    else if  (variableName == MxOpenV  ) { return MxOpenV ; }  
    else if  (variableName == MyOpenV  ) { return MyOpenV ; }  
    else if  (variableName == MypOpenV  ) { return MypOpenV ; }  
    else if  (variableName == MxpMypPOV  ) { return MypV ; }  
	else { return _noV;};

} ;

void    shockTube::makeTexTableHead(int * ioData) {
                                                                               
    int variableNumber;
                                                                               
    if (outputInfo == infoStandard ) {
        variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "\\dfrac{P_5}{P_1} ";
        variableNames[1] = (char *) "M_{s1}";
        variableNames[2] = (char *) "\\dfrac{P_2}{P_1}";
        variableNames[3] = (char *) "\\dfrac{T_2}{T_1}";
        variableNames[4] = (char *) "\\dfrac{P_5}{P_3}";
        variableNames[5] = (char *) "\\dfrac{T_5}{T_3}";
        variableNames[6] = (char *) "U_{s5}";
        //variableNames[6] = "{\\rho}_y \\over {\\rho}_x ";
        showVariableTeXHead(variableNames,variableNumber);
		return ;
    }
    else if (outputInfo == infoMax  ) {
        variableNumber = 9;
		char * variableNames[variableNumber];
        variableNames[0] = (char *) "M_x";
        variableNames[1] = (char *) "M_y";
        variableNames[2] = (char *) "{M_x}^{'}";
        variableNames[3] = (char *) "{M_y}^{'}";
        variableNames[4] = (char *) "{T_y} \\over {T_x}";
        variableNames[5] = (char *) "{P_y} \\over {P_x}";
        variableNames[6] = (char *) "{P_0}_y \\over {P_0}_x ";
        variableNames[7] = (char *) "{U_y}^{'} ";
        variableNames[8] = (char *) "{c_x}";

		showVariableTeXHead(variableNames,variableNumber);
		return;
    }
    else if (outputInfo == infoMinimal ) {
        variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "M_x";
        variableNames[1] = (char *) "{M_y}_w";
        variableNames[2] = (char *) "\\theta_w";
        variableNames[3] = (char *) "\\delta ";
        variableNames[4] = (char *) "{P_y} \\over {P_x}  ";
        variableNames[5] = (char *) "{T_y} \\over {T_x}  ";
		  variableNames[6] =   (char *) "{P_0}_y \\over {P_0}_x  ";

        showVariableTeXHead(variableNames,variableNumber);
		return ;
    }
    else  if (outputInfo == infoObliqueMax ) {
        int variableNumber = 4;
        char * variableNames[variableNumber];
        cout << "The maximum values are:" << endl;
        variableNames[0] = (char *) "M_x";
        variableNames[1] = (char *) "M_y";
        variableNames[2] = (char *) "\\delta_{max}";
        variableNames[3] = (char *) "\\theta_{max}";

        showVariableTeXHead ( variableNames, variableNumber);
    }
    else  if (outputInfo == infoIteration ) {
        int variableNumber = 6;
        char * variableNames[variableNumber];
        cout << "The maximum values are:" << endl;
        variableNames[0] = (char *) "i";
        variableNames[1] = (char *) "M_x";
        variableNames[2] = (char *) "M_y";
        variableNames[3] = (char *) "{T_y} \\over {T_x}  ";
        variableNames[4] = (char *) "{P_y} \\over {P_x}  ";
        variableNames[5] = (char *) "{M_y}^{'}  ";

        showVariableTeXHead ( variableNames, variableNumber);
    }
}

void    shockTube::makeTexTableLine(int * ioData) {
    int numberVariables;

    if (outputInfo == infoStandard ) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = P5P1 ;
        variableToBePrinted[1] = Ms1;
        variableToBePrinted[2] = PyPx;
        variableToBePrinted[3] = TyTx;
        variableToBePrinted[4] = P5P3;
        variableToBePrinted[5] = T5T3;
        variableToBePrinted[6] = Us3; // sqrt(k5 *287 * T5/T5T3 ) ; // Us3;

        showVariableTeXLine (variableToBePrinted, numberVariables);
    }
    else if (outputInfo == infoMax   ) {
        numberVariables = 9;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx ;
        variableToBePrinted[1] = My;
        variableToBePrinted[2] = Mxp;
        variableToBePrinted[3] = Myp;
        variableToBePrinted[4] = TyTx;
        variableToBePrinted[5] = PyPx ;
        variableToBePrinted[6] = P0yP0x;
        variableToBePrinted[7] = Uyp;
        variableToBePrinted[8] = cx;

       	showVariableTeXLine ( variableToBePrinted, numberVariables);
		return;
    }
    else if (outputInfo == infoMinimal ) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Mx ;
        variableToBePrinted[1] = 0.;
        variableToBePrinted[2] = theta[0]*180./PI ;
        variableToBePrinted[3] = delta*180./PI;
        variableToBePrinted[4] = PyPx;
        variableToBePrinted[5] = TyTx;
        variableToBePrinted[6] = P0yP0x;
                                                                                
        showVariableTeXLine (variableToBePrinted, numberVariables);
    }
	else  if (outputInfo == infoIteration ) {
        numberVariables = 5;
        double variableToBePrinted[numberVariables];
		for (int i = 0 ; i < numberOfIterations ; i++){
			variableToBePrinted[0] = steps[i][1];
			variableToBePrinted[1] = steps[i][2];
			variableToBePrinted[2] = steps[i][3];
			variableToBePrinted[3] = steps[i][4];
			variableToBePrinted[4] = Myp;

			cout << (int) steps[i][0] << "&";

        	showVariableTeXLine ( variableToBePrinted, numberVariables);
		}
    }

}

void shockTube::calMx(void) { // for given Mxp
	int i ;
	double  MxOld, deltaMx  ;
	//cout << "calMx" << endl ;
	//successive  repitition
	//first iteretion
	Mx = MxOld = 1.0 +Mxp;
	deltaMx = 1.0;
	i = 0;
	while (deltaMx > EPS) { 
		calMy();
		calPressureRatio();
		calTemperatureRatio();
		if (isR == no ) {
			steps[i][0] = i; 
			steps[i][1] = Mx; 
			steps[i][2] = My; 
			steps[i][3] = TyTx; 
			steps[i][4] = PyPx; 
			steps[i][5] = Myp; 
		}
		Mx =  sqrt(TyTx)*( My) + Mxp  ;
		deltaMx = abs (Mx - MxOld );
		MxOld = Mx;	
//cout<<"number of iteration =" << i << " Mx" << steps[i][0] << endl ;
		i++;
	}
	numberOfIterations = i;
}

int	shockTube::calAll( double * variableValue ){
	double   Mxtmp;
	if  (inputInfo == P5P1V) {
		shockDynamicsName = "pressure input ";
		P5P1 =  variableValue[P5P1V];
		//calMs1();
		//Mxtmp = variableValue[MxV] ;
		Mxtmp = P5P1 ;
		//k1 = k;
		double upLimitsMs1 = calUpperLimitsMs1() - EPS ; 
		Ms1 = root ( calP1P5Estmt, this, 1.0+EPS, upLimitsMs1, Mxtmp);
		P5P1 =  variableValue[P5P1V];
	}
	else if (inputInfo == MyOpenV ) {
		shockDynamicsName = "Open valve";
		My = variableValue[MyOpenV] ;
		discontinuity::calMx();
		calPressureRatio();
		calTemperatureRatio();
		Myp = sqrt(1./TyTx) *Mx - My ;
		Mxp = 0.0; 
	}
	else if (inputInfo == PyPxOpenV ) {
		double tmpPbar;
		shockDynamicsName = "Open valve";
		tmpPbar = PyPx = variableValue[PyPxV] ;
		Mx = root  ( calPbarEstmt, this,  M1maxSub, 50., PyPx) ;
		discontinuity::calMy();
		calTemperatureRatio();
		Myp = sqrt(1./TyTx) *Mx - My ;
		Mxp = 0.0; 
	}
	else if (inputInfo == UypTxOV ) {
		shockDynamicsName = "Open valve";
		Uyp =  variableValue[UypV] ;
		Tx = variableValue[TxV] ;;
		cx = sqrt( k*287*Tx);
		Msx =  ((Uyp*(k + 1.)) + 
			sqrt(((Uyp*Uyp)*((k*(k + 2.)) + 1.)) + (16.*(cx*cx)))) 
			/ (4.*cx) ;
		Us = Msx * cx ;
		Mx = Msx;
		Mxp = 0.;
		calMy () ;
		calPressureRatio();
		calTemperatureRatio();
		cy = sqrt( k*287*Tx*TyTx);
		Myp = Msx - Uyp/cy;
	}
	else if (inputInfo == MxpMypPCV ) {
		shockDynamicsName = "Partially Close valve"; 
		Myp = variableValue[MypV] ;
		Mxp = variableValue[MxpV] ;
		double MxOld, deltaMx;
		//first iteretion
		Mx = MxOld = deltaMx = Mxp + 1.0;
		int i ;
		i = 0;
		while (deltaMx > EPS) { 
			calMy();
			calPressureRatio();
			calTemperatureRatio();
			Msx = Mx - Mxp;
			Msy = My - Myp;
			if (isR == no ) {
				steps[i][0] = i; 
				steps[i][1] = Mx; 
				steps[i][2] = My; 
				steps[i][3] = TyTx; 
				steps[i][4] = PyPx; 
				steps[i][5] = Msx; 
				steps[i][6] = Msy; 
			}
			Mx =  sqrt(TyTx)* Msy + Mxp   ;
			deltaMx = abs (Mx - MxOld );
			MxOld = Mx;	
			i++ ;
		}
		numberOfIterations = i;
	}
	if (inputInfo != PyPxV){ 
		calPressureRatio();
	}
	if (inputInfo != TbarV){
		calT2T1();
	}
	calDensityRatio ();
	calP5P3() ;
	calUs ( variableValue) ;
	//calTotalPressureRatio();
	return yes;
}

double shockTube::calUpperLimitsMs1(){
	//return ((sqrt(5*k1*k1-6*k1+5)+k1+1)/(2*k1-2) );
	return ((sqrt(((k1*k1+2*k1+1)*k5+4*k1*k1*k1-8*k1*k1+4*k1)/k5)+k1+1)/((2*k1-2)*sqrt(k1/k5)));
}

void shockTube::calP5P3(){
		P5P3 = P5P1/PyPx; // PyPx is P2/P1 and P2 = P3
		T5T3 = pow(P5P3,((k5-1)/k5)) ;
};

void shockTube::calT2T1(){
	//Mx = Ms1;
	calMy();
	//double kkk = (k1-1.)/(k1+1.) ;
	discontinuity::calTemperatureRatio();
	//T2T1 =  (kkk + P2P1 )/(1. +kkk* ) ;	
};

// calcuation of the material behind the shock.
void shockTube::calUs(double * variableValue){
      T5.setValue (variableValue[T5V]);
      T1.setValue (variableValue[T1V]);
		T2.setValue(T1.value*TyTx);
		Us = sqrt(k1 *287 * T2.value*My ); 
};

void shockTube::calUs1(double * variableValue){
	T1.setValue (variableValue[T1V]);
	Us1 = sqrt(k1 * 287 * T1.value )*Ms1; 
	Us4 = sqrt(k5 * 287 * T5.value ); 
};

int shockTube:: calProfile (int* ioData, double * variableValue){
	// Mfld [7][50] 0) info 1) general location 2) Pi/P5 3) Ti/T5 4) ci  5) U  6) Mach = 1  7)zone 4 location    
	// calculate locations.
	L1.setValue (variableValue[L1V]);
	calUs1(variableValue) ;
	Mfld[0][1] = Pc_time = L1.value/Us1 ; //the time takes to shock to reach to pscriped point
	Mfld[1][1] = L1.value ; // location of the border zone 1 to 2
	Mfld[1][7] = L1.value ; // location of the border zone 1 to 2
	Mfld[1][2] = Pc_time * Us3 ; // location of the border zone 2 to 3
	T3.setValue (T5.value/T5T3) ;  
	Mfld[1][3] = - Pc_time*sqrt(k5*287*T3.value) ; // location of the border zone 3 to 4
	Mfld[1][4] = - Pc_time * Us4  ; // location of the border zone 3 to 4
	// calculations of the zone 4 location for given pressure	
	// point 1 to fix as input etc 
	Mfld [7][0] = L0.value ; // the value at point 
	Mfld [2][0] = 1.0 ; // P/P1 
	Mfld [3][0] = 1. ; // T/T1 
	Mfld [4][0] = sqrt(k1*287.*T1.value)  ; //c  
	Mfld [5][0] = 0.  ; // U  
	Mfld [6][0] = Mfld [5][0] / Mfld [4][0]   ; // M  
	// point 2 before the shock to fix as input etc 
	Mfld [7][1] = L1.value ; // the value at point 
	Mfld [2][1] = 1.0 ; // P/P1 
	Mfld [3][1] = 1.0 ; // T/T1 
	Mfld [4][1] = sqrt(k1*287.*T1.value)  ; //c  
	Mfld [5][1] = 0.  ; // U  
	Mfld [6][1] = Mfld [5][0] / Mfld [4][0]   ; // M  
	// point 2 after the shock to fix as input etc 
	Mfld [7][2] = L1.value ; // the value at point 
	Mfld [2][2] = PyPx ; // P 
	Mfld [3][2] = TyTx ; // T 
	Mfld [4][2] = sqrt(k1*287.*T1.value*TyTx)  ; //c  
	Mfld [5][2] = Mfld [4][2] *My  ; // U  
	Mfld [6][2] = My   ; // M  
	// point 3 before the contact surface 
	Mfld [7][3] = Pc_time*Us ; // the value at point 
	Mfld [2][3] = PyPx ; // P 
	Mfld [3][3] = TyTx ; // T 
	Mfld [4][3] = sqrt(k1*287.*T1.value*TyTx)  ; //c  
	Mfld [5][3] = Us  ; // U
	Mfld [6][3] = My   ; // M  
	// point 3 after the contact surface 
	Mfld [7][4] = Pc_time*Us ; // the value at point 
	Mfld [2][4] = PyPx ; // P 
	Mfld [3][4] = T3.value ; // T 
	Mfld [4][4] = sqrt(k1*287.*T3.value)  ; //c  
	Mfld [5][4] = Us  ; // U  
	Mfld [6][4] = Us/ Mfld [4][4] ; // M  

	double Px ; // pressure ratio Px/P5
	double Tx ; // pressure ratio Tx/T5
	double power ; // raising the power P--> T
	power = (k5-1.)/k5 ;
	int j = 10; // insert the point from point 10
	for (int i = 0 ; i < 50 ; i++){
		Px = 1. - (1. - 1./P5P3)*0.02 *(49.-i); //0.02 for 51 points  49 for the two end points
		Mfld[2][i+j] = 1./Px ; 
		Mfld[3][i+j] = Tx = T5.value * pow ( Px,power) ; // T
		Mfld[4][i+j] = sqrt(k5*287*Tx); // ci=velocity
		Mfld[5][i+j] = Mfld[4][i+j] ; // c = M
		Mfld[6][i+j] = 1.0 ; // c = M
		Mfld[7][i+j] = sqrt(k5*287*Tx) * Pc_time; //location
	}	

	//cout << " cal profile " << endl;
	return yes ;
};

int shockTube:: calTableLine (int* ioData, double * variableValue){
	if ((outputType) ioData[whatInfoN] == infoStandard ||
		(outputType) outputInfo == infoMinimal ||
		(outputType) outputInfo == infoIteration ){
		return compressibleFlow::calTableLine (ioData, variableValue);
   }
	else if ((outputType) ioData[whatInfoN] == infoTubeProfile )  { 
		if (calAll (variableValue) == yes) {
			return calProfile (ioData, variableValue) ;
		}
		else {
			return no;
		}
	}
	else {
		return calAll (variableValue) ;
	}
};


int shockTube:: calTableLine (  inputType variableName,
            double * variableValue){
	if (outputInfo == infoStandard || outputInfo == infoMinimal ){
		return calAll( variableValue);
	}
	else if (outputInfo == infoTubeProfile ) {
		return calAll( variableValue);
	}
    return no;
};

int shockTube::doAllLines ( int * ioData, double * variableValue ){
	inputType variableName = (inputType) ioData[variableNameN];
	int howManyPoints,  howManySets ; 
	double startP, endP, rangeD;
	double range [61];
	if (outputInfo == infoStandard ){
		if (variableName == P5P1V){
			startP = 1.01;
			endP = 500.;
		}
		else{ //the default is not determined yet 
			startP = 0.5*(k-1.)/k+EPS;
			//upper boundry 
			endP  = double(int (startP *10.) + 1 )/10.;
		}
		if (variableName == P5P1V ){
			howManySets = 4;
			range[0] = (double) howManySets;
			range[1] =  9.0; // howManyPoints;
			range[2] = 1.01 ; // starting point  
			range[3] = 10.10 ;  // last point
			range[4] = 10. ; 
			range[5] = range[3] + 0.9 ; // starting point  from the last range 
			range[6] = 50. ;  // last point   
			range[7] = 8. ; 
			range[8] = range[6] + 10. ; // starting point  from the last range  
			range[9] = 90. ; 
			range[10] = 8. ; 
			range[11] = range[9] + 1.; // 
			range[12] = 500. ;//
		}
		//else {
		//	return no;
		//}

		setRange( ioData, range, variableValue);

		howManyPoints = 9 ;
		rangeD = 0.1;
		startP = endP + rangeD;
		endP = 2.0;

		range[0] = (double) howManyPoints;
		range[2] = startP ; 
		range[3] = endP ; 
		//setRange( variableName, isTex, range, variableValue);
		//makeLines(isTex,howManyPoints, startP,  endP,
								 //variableName, variableValue);
		howManyPoints = 7 ;
		rangeD = 1.0 ;
		startP = endP + rangeD;
		endP = 10.;
		//makeLines(isTex,howManyPoints, startP,  endP,
		//						 variableName, variableValue);
	}
   else if (outputInfo == infoD0) {
        howManyPoints = 9 ;
         makeLines(ioData,howManyPoints, 1.1,  2.0, variableValue);
        howManyPoints = 9 ;
         makeLines(ioData,howManyPoints, 2.2,  4.0,  variableValue);
        howManyPoints = 5 ;
         makeLines(ioData,howManyPoints, 5.0,  10.0,  variableValue);
    }

    return yes;
}

void	shockTube::calPressureRatio(){
	discontinuity::calMy();
	discontinuity::calPressureRatio();
};

void	shockTube::calP5P1(){
	Ms1 = Mx ;
	double pAAA = (1- (k5-1.)/(k1+1.0) * sqrt(k5/k1)*1.0 * (Ms1*Ms1-1)/Ms1 ) ; 
	pAAA = 0. + pAAA;
	P5P1 = (k1-1.)/(k1+1.0) * (2*k1/(k1-1.)*Ms1*Ms1- 1.) *
		pow ((1- (k1-1.)/(k1+1.0) *sqrt(k1/k5)*1.0 * (Ms1*Ms1-1)/Ms1 ),(-2.0*k5/(k5-1.)) ) ; 
	//cout << "This is Pbar " << Pbar << endl ;
	//Us1 = 2. ;
	return ;
};

int	shockTube::calMs1(){
	double Pfake = 0.0 ; 
	//Ms1 = 2.5339 ;
	Pfake = (k1-1.)/(k1+1.0) * (2*k1/(k1-1.)*Ms1*Ms1- 1.) *
		pow ((1- (k5-1.)/(k1+1.0) *1.0 * (Ms1*Ms1-1)/Ms1 ),(-2.0*k5/(k5-1.)) ) ; 
	cout << "This is Pfake " << Pfake << endl ;
	Us1 = 2. ;
	return yes;
};

void 	shockTube::makeHTMLTableHead(int * ioData){
    int variableNumber;
                                                                               
    if (outputInfo == infoStandard || outputInfo == infoMinimal) {
        variableNumber = 7;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "P5/P1 ";
        variableNames[1] = (char *) "Ms1";
        variableNames[2] = (char *) "T2/T1";
        variableNames[3] = (char *) "Us1"; // "My&prime;";
        variableNames[4] = (char *) "Ty/Tx";
        variableNames[5] = (char *) "Py/Px";
        variableNames[6] = (char *) "P0y/P0x";
                                                                               
        showVariableHTMLHead(variableNames,variableNumber);
    }
    else  if (outputInfo == infoIteration ) {
        variableNumber = 6;
        char * variableNames[variableNumber];
        variableNames[0] = (char *) "i    ";
        variableNames[1] = (char *) "Mx";
        variableNames[2] = (char *) "My";
        variableNames[3] = (char *) "TyTx";
        variableNames[4] = (char *) "PyPx";
        variableNames[5] = (char *) "Myp";
        showVariableHTMLHead( variableNames, variableNumber);
    }
};

void    shockTube::makeHTMLTableLine(int * ioData) {
    int numberVariables;

    if (outputInfo == infoStandard || outputInfo == infoMinimal) {
        numberVariables = 7;
        double variableToBePrinted[numberVariables];
        variableToBePrinted[0] = Pbar ;
        variableToBePrinted[1] = Ms1;
        variableToBePrinted[2] = Tbar;
        variableToBePrinted[3] = Us1;
        variableToBePrinted[4] = TyTx;
        variableToBePrinted[5] = PyPx ;
        variableToBePrinted[6] = P0yP0x;

        showVariableHTMLLine (variableToBePrinted, numberVariables);
    }
	else  if (outputInfo == infoIteration ) {
		numberVariables = 6;
		double variableToBePrinted[numberVariables];
		for (int i = 0 ; i < numberOfIterations ; i++){
			variableToBePrinted[0] = i;
			variableToBePrinted[1] = steps[i][1];
			variableToBePrinted[2] = steps[i][2];
			variableToBePrinted[3] = steps[i][3];
			variableToBePrinted[4] = steps[i][4];
			variableToBePrinted[5] = Myp;

			showVariableHTMLLine(variableToBePrinted, numberVariables);
		}
        return;
    }
};

void shockTube :: showVariableHTMLHead (char ** variableNames,
					 int numberVariables ) {
	cout << endl << endl;
	//cout << "this html code for table" << endl ;
	cout << "<tr class=\"text\"><td align=\"center\">" << endl ;
	cout << "<table border=1 width=\"100%\" >" << endl ;
	cout << "  <thead>" << endl;
	cout << "    <tr>" << endl ;
	cout << "      <th align=left bgcolor=\"#fffeaa\" colspan=";
	cout << 3 << " >";
	cout <<  flowModelName ;
	cout << " </th>" << endl;
	cout << "      <th align=left  bgcolor=\"#00ff5a\" colspan=";
	cout << numberVariables-4 << "  rowspan=2 >"; 
	cout << "Input: ";
	cout << inputVariableName ;
	cout << " </th>" << endl;
	cout << "      <th align=left  bgcolor=\"#9ae0ee\" colspan=";
	cout << 1 << " rowspan=2 >";  
	cout << "k = ";
	cout << k ;
	cout << " </th>" << endl;
	cout << "    </tr>" << endl ;
	cout << "    <tr>" << endl ;
	cout << "      <th align=left bgcolor=\"#fffeaa\" colspan="; 
	cout << 3 << " >";  
	cout <<  shockDynamicsName ;
	cout << " </th>" << endl;
	cout << "    </tr>" << endl ;
	cout << "    <tr>" << endl ;
	for (int i=0; i < numberVariables ; i++){
		cout << "      <th align=center >" ;
		cout <<  variableNames[i] << " </th>" << endl;
	}
	cout << "    </tr> " << endl;
	cout << "  </thead>" << endl;
	cout << "  <tbody>" << endl;
};
