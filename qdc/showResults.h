#ifndef FINDDIALOG_H 
#define FINDDIALOG_H 
#include <qdialog.h> 
class 	QCheckBox;
class 	QLabel;
class 	QLineEdit;
class 	QPushButton;
//class 	QTextView;
class QTextBrowser;
class 	QProcess;
//class 	QString;

class ShowResults : public QDialog {
	Q_OBJECT 
public:
	ShowResults(QWidget *parent = 0, const char *name = 0 );
//			,QString FileName = 0 ); 
	QString fileName;
signals:
//	void findNext(const QString &str, bool caseSensitive);
//	void findPrev(const QString &str, bool caseSensitive);

public slots:
    void readFromStdout();
    void scrollToTop();
    void clearOutput();
    //void setFileName(QString FileName);
	void newData(QString fileName);

private slots:
	//void enableFindButton(const QString &text);
	void init();

private:
	//QLineEdit *lineEdit;
	//QCheckBox *caseCheckBox;
	//QCheckBox *backwardCheckBox;
	//QPushButton *findButton;
	QPushButton *closeButton; 
	QPushButton *clearButton;
	//QTextView *output;
	//new change to different widget
	QTextBrowser *output;
	QProcess *proc;
    int curPosX;

};

#endif

