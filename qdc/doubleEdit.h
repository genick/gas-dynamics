#ifndef WIDDOUBLEEDIT_H
#define WIDDOUBLEEDIT_H
// $Id: WidDoubleEdit.h,v 1.1 1999/08/31 12:27:24 schatz Exp $
//
// WidDoubleEdit - Widget for floating point input with or without slider
//

#include <qwidget.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qvalidator.h>
#include <qslider.h>
#include <qstring.h>

class WidDoubleEdit : public QWidget {
    Q_OBJECT
  public:
	enum SliderType { NoSlider, LinSlider, LogSlider };
	double  RANGE [2] ;
    
    //WidDoubleEdit( double bottom, double top, int decimals, SliderType sltype,
    WidDoubleEdit( double bottom, double top, int decimals, 
			const char *label, QWidget *parent, const char *name= 0 );
    ~WidDoubleEdit() {}
    
    double value();
    double * getRange();
    
    void setRange( double bottom, double top );
    void setPrecision( int decimals );
    void setTracking( bool track_sl, bool track_ed );
    void setUpdateTracking( bool track_sl, bool track_ed );
    void setLabel( const QString label );
  
  public slots:
    void setValue( double val );
    
  signals:
    void returnPressed();
    void valueChanged( double val );
 	void textChanged(const QString&) ;
 	//void clicked() ;

    
  private slots:
    void handle_slchange( int slval );
	void setValue(const QString& text ) ;
    void handle_edchange( const QString &text );
    void handle_edreturn();
    
  private:
    QLabel *m_label;
    QLineEdit *m_edit;
    QDoubleValidator *m_validator;
    QSlider *m_slider;
    
    double m_bottom, m_range, m_logbot, m_logrange, m_last_sigval;
    int m_decimals;
    SliderType m_sltype;
    bool m_track_sl, m_track_ed, m_trup_sl, m_trup_ed, m_lock;
    
    void update_edit( double val );
    void update_slider( double val );
    double get_txtvalue( QString text );
    double get_slidval( int slval );
    
    static const int m_SLMIN, m_SLMAX, m_SLPAGE;
};

#endif // WIDDOUBLEEDIT_H


