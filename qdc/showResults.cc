//#include <qcheckbox.h>
//#include <qlabel.h>
#include <qlayout.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qtextview.h>
#include <qprocess.h>
#include <qstring.h>
#include <qtextbrowser.h>
#include <qfile.h>
#include <qdir.h>
#include <qmessagebox.h>
#include "showResults.h"

class 	QTextBrowser;
class    QRadioButton;
class    QString;
class    QLabel;
class    Qpixmap;
class    QCheckBox;
class    QComboBox;
class    QString;
class 	QPushButton;


ShowResults::ShowResults(QWidget *parent, const char *name)
//			,QString FileName ) 
	: QDialog(parent, name) 
{
	setGeometry( QRect( 400, 40, 660, 640 ) );
	setCaption(tr("Gas Dynamics Calculator: The Results"));
	output = new QTextBrowser ( this );
	output->setGeometry( QRect( 15, 10, 630, 550 ) );

//	if (!proc){
//		proc = new QProcess( this );
//	}
//    proc-> setArguments( "/mnt/avi/calculations/program/gd" );
    //proc->addArgument( "-tr" );
    //proc->addArgument( "i18n" );
    //proc->addArgument( FileName );

//connect( proc, SIGNAL(readyReadStdout()),
//        this, SLOT(readFromStdout()) );
//    connect( proc, SIGNAL(processExited()),
//        this, SLOT(scrollToTop()) );
//	if ( !proc->start() ) {
    // error handling
//    QMessageBox::critical( 0,
//        tr("Fatal error"),
//        tr("Could not start the gd program."),
//        tr("Quit") );
//    exit( -1 );
//    }

	closeButton = new QPushButton(tr("Close Window"), this);
	closeButton->setGeometry( QRect( 200, 580, 100, 40 ) );
	connect(closeButton, SIGNAL(clicked()), this, SLOT(close()));
	clearButton = new QPushButton(tr("Clear Window"), this);
	clearButton->setGeometry( QRect( 340, 580, 100, 40 ) );
	connect(clearButton, SIGNAL(clicked()), this, SLOT(clearOutput()));
	//QHBoxLayout *bottomLayout = new QHBoxLayout;
	//QSpacerItem* rightSpacer = new QSpacerItem( 41, 21, QSizePolicy::Expanding,
	//QSizePolicy::Maximum );
    //bottomLayout->addItem( rightSpacer );
	//bottomLayout->addWidget(closeButton);
	//bottomLayout->setGeometry( QRect( 440, 210, 40, 20 ) );

	//QVBoxLayout *MainLayout = new QVBoxLayout;

	//MainLayout->addWidget(output);
	//MainLayout->addStretch(1);
	//QHBoxLayout *mainLayout = new QHBoxLayout(this);
	//MainLayout->setMargin(11);
	//MainLayout->setSpacing(6);
	//MainLayout->addLayout(bottomLayout);
	//leftLayout->addLayout(topLeftLayout);
	//mainLayout->addLayout(MainLayout);
	init();
}

void ShowResults::init()
{
	proc = 0;
	output->setText("<html> </html>");
	//output->setTextFormat(Qt::PlainText);
	//output->setTextFormat(Qt::RichText);
	//output->setTextFormat(Qt::AutoText);
}

void ShowResults::readFromStdout()
{
    // Read and process the data.
	//curPosX = output->contentsX();
	curPosX = output->contentsHeight() - 30 ;
	output->append( proc->readStdout() );
}

void ShowResults::clearOutput()
{
	output->setText("");
}

void ShowResults::scrollToTop()
{
    //curPosX =2;

	//output->setContentsPos( 0, 0 );
    curPosX = output->contentsHeight()- 30;
	output->setContentsPos( curPosX, 0 );
}

//void ShowResults::setFileName(QString FileName)
//{
//	fileName = FileName;
    //output->setContentsPos( 0, 0 );
//}

void ShowResults::newData(QString FileName)
{
    //fileName = FileName;
	if (!proc){
		proc = new QProcess( this );
		connect( proc, SIGNAL(readyReadStdout()),
			this, SLOT(readFromStdout()) );
		connect( proc, SIGNAL(processExited()),
			this, SLOT(scrollToTop()) );
	 	//QDir d( "gd" ); 
		//QFile f( d.filePath("gd") ); 
        //if ( !f.open(IO_ReadWrite) ) {
            //qWarning( "Cannot create the file %s", f.name() );
    //		QMessageBox::critical( 0,
	//		tr("<b>cannot </b> read the file gd/gd"),
	//		tr("Could not start the ./gd/gd program."),
	//		tr("Quit") );
    //		exit( -1 );
	//	}
	//	else {
	//		QMessageBox::critical( 0,
	//		tr("Fatal error"),
	//		tr("can read the file  ./gd/gd"),
	//		tr("Quit") );
    //		//exit( -1 );
	//	}
	//	f.close();
	proc->setArguments( "./gd/gd" );
		//proc->addArgument( "-g" );
		QString notice, notice1, caption;
		notice = "<h2>Gas Dymanics Calculator 0.4.3.2</h2>" ;
		notice += "<p>Copyright &copy; 2005 Dr. Genick Bar-Meir";
		notice += "<p>File name is:    " ;
		notice += FileName ;
		//notice += notice1.setNum( curPosX );
		//QMessageBox::about(this , "About Gas Dymanics Calculator", notice );
		proc->addArgument( FileName );
	}
	//curPosX = output->contentsX();
	curPosX = output->contentsHeight();
	output->setContentsPos( curPosX, 0 );
	QString notice, notice1, caption;
                                                                                 
    notice = "<h2>Gas Dymanics Calculator 0.4.3.2</h2>" ;
    notice += "<p>Copyright &copy; 2005 Dr. Genick Bar-Meir";
    notice += "<p>getting content x position  " ;
    notice += "<p> the position is  ";
    notice += notice1.setNum( curPosX );
    //QMessageBox::about(this , "About Gas Dymanics Calculator", notice );
	//curPosX =7;

//   proc-> setArguments( "/mnt/avi/calculations/program/gd" );
    //proc->addArgument( FileName );

    //output->setContentsPos( curPosX, 0 );
    output->setContentsPos( curPosX, 0 );
	if ( !proc->start() ) {
    // error handling
    	QMessageBox::critical( 0,
			tr("Fatal error"),
			tr("Could not start the gd program."),
			tr("Quit") );
    	exit( -1 );
    }
    //output->setContentsPos( curPosX, 0 );
}


