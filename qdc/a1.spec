Summary: GNU indent
Name: gdcT
Version: 2.2.6
Release: 1
Group: Graphical Desktop/Other
BuildRoot: %{_tmppath}/build-root-%{name}

Source0: %{name}-%{version}.tar.gz
License: GPL
%description
The GNU indent program reformats C code to any of a variety of
formatting standards, or you can define your own.
%prep
%setup -c
%build
make
make ./gd/Makefile
%install
%files
%defattr(-,root,root)

