/********************************************************
	This code is copyrighted by Genick Bar_Meir 
	and has no  warranty of any kind.
	Use this code at your own risk. 

	This program is open source under the same license as
	all the others material in Potto project.
	By:
	Genick Bar-Meir

	Inputs:
	n/a

	Returns:
	prints table for the ratio of P1/P2 for non chocked flow for 
	1. Fanno flow 2.  shock wave 3. Isothermal flow 
		4. Rayleigh flow (not yet)  
	for given 4FL/D.
	The code also produces for specific value.	
	It use perfect gas with k=1.4 (easily can be changed)
	Oh, well, k also can be change via the class declaration.

	Description:
	This code includes classes to calculate the various parameter
	of the Fanno flow, stagnation and shock wave.  
	
	Limitation:
	At this stage for very small value of 4fl/D there is no
	proper calculation and it starts the proper calculations at fld> 1000. 
********************************************************/
#ifndef _DEFINITION_STDLIB
#define _DEFINITION_STDLIB

#	include 	<vector>
#	include 	<string>
#	include 	<string>
#include "gd/definition.h" 
#  include "constant.h" 

class QString;

struct property {
	PhysicalUnits   units;
	double value;
	int isKnown;
    property()   {
        units = __none ;
		value = 1.0; 
		isKnown = no;
    }
	~property(){}
	void setValue (double a) { value = a ; isKnown = yes;};
} ;

enum inputType{ _noV, machV, fldV, p2p1FLDV, angleV, M1ShockV, FLDShockV,
	M1fldV, M2fldV, M1fldP2P1V, MxV, MyV, MFLDshockLocationV, PbarV, AstarV,
	PARV, P0yP0xV, PyPxV,  TbarV, TRstarV, TRzeroStarV, deltaMV, thetaMV,
	deltaThetaV, omegaV, RbarV, T0yT0xV, R0yR0xV, PRzeroStarV,
	UxpV, UyOpenV, UypV, UsV, MxsV, MypV, MxpV, MypOpenV, AV, 
	P0barV, T0barV, R0barV, P0V, TV, mDotV, mDotPT0V, nuV,
	MxOpenV, MyOpenV, PbarOpenV, PyPxOpenV, kV, p2p1V, M1V, mDotP0TV,
	thetaV, shockLocationV, UsOpenV, M1M2V, UypTxOV, FnBV, T0V, PV,
	M2V, TxV, TyV, TyTxV, MxGV, MxpMypPOV, MxpMyPOV, MxpMypPCV };

//data info output in array
enum DataInfo { whatInfoN,	 variableNameN,  isTexN,  isRangeN,
	classNameN, isBothBranchesN}	 ; // 

enum FlowName{ fannoFlow , stagnationFlow, rayleighFlow, isothermalFlow,
	  obliqueFlow, shockFlow, pmFlow, isothermalNozzleFlow,
		shockDynamicsFlow, suddenOpenFlow, suddenCloseFlow, pOpenFlow,
		pCloseFlow, chokedFannoFlow, unChokedFannoFlow }; 

//what information is sought. Note different derived class yields different
//results 
enum outputType { infoStagnation, infoStandard ,  infoTube , 
	infoShock, infoTubeShock,  infoTubeProfile, infoFn, infomDot,
	infoTubeShockLimits, infoObliqueMax,   infoObliqueMin ,
	infoMinimal, infoD0 , infoMax, infoIteration } ;

//what kind of branches both with supersonic subsonic 
// or supersonic and subsonic.

enum branchSide{subsonic, supersonic, bothsonic};
enum branchType{ __noBranch, simple, all}; 

//the layout numbers
enum shockDynamicLN {SOpen, SClose, POpen, PClose};
enum fannoSubLN {chokedF, unchokedF};
enum location{location0, location1, location2, location3, location4};
enum shockLayoutNumber{MxS, MyS, PyPxS, TyTxS, RyRxS, P0yP0xS, R0yR0xS };
enum closeLayoutNumber{MxC, MyC, MxpC, UxpC};
enum pCloseLayoutNumber{MxMypC, MxpMypC};
enum openLayoutNumber{MxO, MyO, MypO, UsTxO, PyPxO, PistonO};
enum pOpenLayoutNumber{MxpMyO, MxpMypO};
enum isentropicLayoutNumber{MI, PbarI, TbarI, RbarI, AstarI, PARI, P0TI, PT0I };
enum PMLayoutNumber{MIs, nuIs, PbarIs, TbarIs, RbarIs, AstarIs, PARIs };
enum fannoChokedLayoutNumber{MF, fldF, PbarF, TbarF, RbarF, P0barF };
enum fannoUnChokedLayoutNumber{M1fld, M2fld, fldP2P1, M1P2P1, M2P2P1 };
enum isothermalLayoutNumber{MIso, fldIso, PbarIso,  RbarIso,
			P0barIso, T0barIso, R0barIso };
enum isothermalNoLayoutNumber{MIsoNo,  PbarIsoNo,  RbarIsoNo, AstarIsoNo, 
			PARIsoNo, P0barIsoNo, T0barIsoNo, R0barIsoNo };
enum obliqueLayoutNumber{ deltaThetaO, deltaMO, thetaMO,  M1maxO}; 
enum rayleighLayoutNumber{MR,  PbarR, TbarR, RbarR, P0barR, T0barR, R0barR };

// variables number 
enum PropertyType{Pp = 0, Tp, mDotAreap, Rhop, u_p, h_p, Velocityp };
enum PressureUnitsTypes{Bar, Pa, MPa, Psi};
enum mDotUnitsTypes{kgSecm2, lbmSecft2, lbmSecin2};
enum TemperatureUnitsTypes{CTU, KTU, RTU, FTU};
enum velocityUnits{mSec, ftSec, kmHr, mileHr};

	const double conversionCoefficient[10][4] = {
					{100000., 1., 1000000., 6894.75729}, // pressure
					{}, // temperature 
					{1., 0.3048, 0.277777778, 0.44704 }, //mDotArea 
					{} , //Rho 
					{} , //u internal energy 
					{} , //u internal entalpy
					{1., 0.3048, 0.277777778, 0.44704 }, //Velocity 
			};
					
	const double Pconversion[4] = {100000., 1., 1000000., 6894.75729};
	const double mDotConversion[3] = {1., 4.88242764, 703.06958 };
	const double rangeLimits [10][4]  = { 
											{10000, 10000000. }, //pressure in Bar
											{-100., 500. }, //temperature [C] 
											{0.0001, 300. }, // mDotArea 
											{100., 500. }, // Rho 
											{100., 500. }, // u internal energy 
											{100., 500. }, // h internal entalpy 
											{1., 700. } //Velocityp 
										};	

	const int numberOfParameters[16]= {
		//fannoFlow
		7,
		//stagnationFlow
		8,
		//rayleighFlow
		6,
		//isothermalFlow 
		6,
		//obliqueFlow
		4,
		//shockFlow
		7,
		//pmFlow
		7,
		//isothermalNozzleFlow,
		8,
      //shockDynamicsFlow
		8,
		//suddenOpenFlow 
		6,
		//suddenCloseFlow 
		4,
		//pOpenFlow 
		2,
		//pCloseFlow
		2,
		//chokedFannoFlow
		6,
		//unChokedFannoFlow
		5
	};
	const string isothermalNoParameters [10] = {"Mach", "fld", "P/P*"} ;
	const char  shockLayoutName[7][10] = {"Mx", "My", "PyPx", "TyTx",
		"RyRx", "P0yP0x",  "R0yR0x" };
	const char  isentropicLN[8][10] = { "Mach", "Pbar", "Tbar",
		"Rbar", "A/A*", "PAR",  "P0 T", "P T0" };
	const char  PMLN[7][10] = {"Mach", "nu", "Pbar", "Tbar",
		"Rbar", "Astar", "PAR" };
	const char  chokedFannoLayoutName[9][10] = { "Mach", "fld", "P/P*", "T/T*",
		"R/R*", "P0/P0*",  "R0/R0*"  };
	const char  fannoUnChokedLayoutName[5][10] = { "M1 fld", "M2 fld",
				"fld P2P1", "M1 P2P1", "M2 P2P1" };
	const char  fannoSubLayoutName[9][10] = { "choked", "unChoked" };
	const char  isothermalLayoutName[7][10] = { "Mach", "fld", "P/P*", 
		"R/R*", "P0/P0*", "T0/T0*", "R0/R0*"  };
	const char  isothermalNoLayoutName[8][10] = { "Mach", "P/P*", "R/R*",
		"Astar", "PAR", "P0/P0*", "T0/T0*", "R0/R0*"  };
	const char  rayleighLN[7][10] = { "Mach", "P/P*", "T/T*",
		"R/R*", "P0/P0*", "T0/T0*", "R0/R0*" };
	const char  obliqueLayoutName[4][10] = { "dM", "tM", "dt", "M1max" };
	const char  shockDynamicsLayoutName[4][20] = { "Open Valve", "Close Valve",
						"Partially Open", "Partially Close" };
	const char  closeLayoutName[4][10] = { "Mx", "My", "Mxp", "Uxp" };
	const char  pCloseLayoutName[2][10] = { "MxMyp", "MxpMyp" };
	const char  openLayoutName[6][10] = { "Mx", "My", "Myp", "Us Tx",
						"PyPx", "Piston" };
	const char  pOpenLayoutName[2][10] = { "MxpMy", "MxpMyp" };

enum  InputType{kN, m1N, fldN, p2p1N, m2N, mN, shockLocationN,  MxN,
	MxpN,  MyN, MypN, PbarN, TbarN, TRstarN, TRzeroStarN, PRzeroStarN,
		AstarN, PARN,  P0yP0xN, R0yR0xN, thetaN, deltaMN, angleN,
		RbarN, UyN, UxN, UsN, TyTxN, TxN, T0yT0xN  };

//precition of the results 
extern int precision;

// variables number for the tube profile
extern int numberNodes ; // number of nodes in tube
extern int  fldP ;  // location of fld in the array 
extern int  MxP ;  ; // location of Mx in the array 
extern int  MyP ;  // location of My in the array 
extern int  PyPxP  ; // 
extern int  PxPsP  ; // 
extern int  PyPsP  ; // 
extern int  P1PiP  ; // 

#endif  /* _DEFINITION_STDLIB */
