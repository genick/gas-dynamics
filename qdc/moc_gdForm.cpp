/****************************************************************************
** gdForm meta object code from reading C++ file 'gdForm.h'
**
** Created: Mon Mar 26 10:19:37 2012
**      by: The Qt MOC ($Id: qt/moc_yacc.cpp   3.3.8   edited Feb 2 14:59 $)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#undef QT_NO_COMPAT
#include "gdForm.h"
#include <qmetaobject.h>
#include <qapplication.h>

#include <private/qucomextra_p.h>
#if !defined(Q_MOC_OUTPUT_REVISION) || (Q_MOC_OUTPUT_REVISION != 26)
#error "This file was generated using the moc from 3.3.8b. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

const char *gdForm::className() const
{
    return "gdForm";
}

QMetaObject *gdForm::metaObj = 0;
static QMetaObjectCleanUp cleanUp_gdForm( "gdForm", &gdForm::staticMetaObject );

#ifndef QT_NO_TRANSLATION
QString gdForm::tr( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "gdForm", s, c, QApplication::DefaultCodec );
    else
	return QString::fromLatin1( s );
}
#ifndef QT_NO_TRANSLATION_UTF8
QString gdForm::trUtf8( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "gdForm", s, c, QApplication::UnicodeUTF8 );
    else
	return QString::fromUtf8( s );
}
#endif // QT_NO_TRANSLATION_UTF8

#endif // QT_NO_TRANSLATION

QMetaObject* gdForm::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    QMetaObject* parentObject = QMainWindow::staticMetaObject();
    static const QUMethod slot_0 = {"init", 0, 0 };
    static const QUMethod slot_1 = {"helpAbout", 0, 0 };
    static const QUMethod slot_2 = {"helpIndex", 0, 0 };
    static const QUMethod slot_3 = {"helpContents", 0, 0 };
    static const QUMethod slot_4 = {"clearParameters", 0, 0 };
    static const QUMethod slot_5 = {"setParameters", 0, 0 };
    static const QUMethod slot_6 = {"clearInputBoxs", 0, 0 };
    static const QUMethod slot_7 = {"setInput", 0, 0 };
    static const QUMethod slot_8 = {"calculate", 0, 0 };
    static const QUParameter param_slot_9[] = {
	{ 0, &static_QUType_int, 0, QUParameter::Out }
    };
    static const QUMethod slot_9 = {"LabelChanged", 1, param_slot_9 };
    static const QUMethod slot_10 = {"fileSave", 0, 0 };
    static const QUMethod slot_11 = {"fileSaveAs", 0, 0 };
    static const QUMethod slot_12 = {"fileOpen", 0, 0 };
    static const QUParameter param_slot_13[] = {
	{ "FileName", &static_QUType_QString, 0, QUParameter::In }
    };
    static const QUMethod slot_13 = {"save", 1, param_slot_13 };
    static const QUParameter param_slot_14[] = {
	{ 0, &static_QUType_int, 0, QUParameter::Out }
    };
    static const QUMethod slot_14 = {"canSave", 1, param_slot_14 };
    static const QUParameter param_slot_15[] = {
	{ "k", &static_QUType_double, 0, QUParameter::In }
    };
    static const QUMethod slot_15 = {"valueChange", 1, param_slot_15 };
    static const QUMethod slot_16 = {"Load", 0, 0 };
    static const QUMethod slot_17 = {"newValues", 0, 0 };
    static const QUParameter param_slot_18[] = {
	{ "a", &static_QUType_ptr, "branchType", QUParameter::In }
    };
    static const QUMethod slot_18 = {"selectBranch", 1, param_slot_18 };
    static const QUMethod slot_19 = {"valueChange", 0, 0 };
    static const QUParameter param_slot_20[] = {
	{ "whatInput", &static_QUType_ptr, "inputType", QUParameter::In }
    };
    static const QUMethod slot_20 = {"setNotice", 1, param_slot_20 };
    static const QUMethod slot_21 = {"setNoticeIfNeeded", 0, 0 };
    static const QUMethod slot_22 = {"branchSelected", 0, 0 };
    static const QUMethod slot_23 = {"fix_mD", 0, 0 };
    static const QUMethod slot_24 = {"onSubModelGroup", 0, 0 };
    static const QUMethod slot_25 = {"clearSubGroup", 0, 0 };
    static const QUMethod slot_26 = {"setSubParameters", 0, 0 };
    static const QUParameter param_slot_27[] = {
	{ 0, &static_QUType_QString, 0, QUParameter::In }
    };
    static const QUMethod slot_27 = {"notImplementedYet", 1, param_slot_27 };
    static const QUMethod slot_28 = {"clearInfoBox", 0, 0 };
    static const QUMethod slot_29 = {"setInfoBoxs", 0, 0 };
    static const QUParameter param_slot_30[] = {
	{ 0, &static_QUType_QString, 0, QUParameter::In }
    };
    static const QUMethod slot_30 = {"setInfoNotice", 1, param_slot_30 };
    static const QUMethod slot_31 = {"newData", 0, 0 };
    static const QUParameter param_slot_32[] = {
	{ 0, &static_QUType_int, 0, QUParameter::In },
	{ 0, &static_QUType_QString, 0, QUParameter::In }
    };
    static const QUMethod slot_32 = {"fixP", 2, param_slot_32 };
    static const QUParameter param_slot_33[] = {
	{ 0, &static_QUType_int, 0, QUParameter::In },
	{ 0, &static_QUType_QString, 0, QUParameter::In }
    };
    static const QUMethod slot_33 = {"fixT", 2, param_slot_33 };
    static const QUParameter param_slot_34[] = {
	{ 0, &static_QUType_int, 0, QUParameter::In },
	{ 0, &static_QUType_QString, 0, QUParameter::In }
    };
    static const QUMethod slot_34 = {"fixRho", 2, param_slot_34 };
    static const QUParameter param_slot_35[] = {
	{ 0, &static_QUType_int, 0, QUParameter::In },
	{ 0, &static_QUType_QString, 0, QUParameter::In }
    };
    static const QUMethod slot_35 = {"fix_s", 2, param_slot_35 };
    static const QUParameter param_slot_36[] = {
	{ 0, &static_QUType_int, 0, QUParameter::In }
    };
    static const QUMethod slot_36 = {"fix_x", 1, param_slot_36 };
    static const QUParameter param_slot_37[] = {
	{ 0, &static_QUType_int, 0, QUParameter::In },
	{ 0, &static_QUType_QString, 0, QUParameter::In }
    };
    static const QUMethod slot_37 = {"fix_h", 2, param_slot_37 };
    static const QUParameter param_slot_38[] = {
	{ 0, &static_QUType_int, 0, QUParameter::In },
	{ 0, &static_QUType_QString, 0, QUParameter::In }
    };
    static const QUMethod slot_38 = {"fix_u", 2, param_slot_38 };
    static const QUParameter param_slot_39[] = {
	{ 0, &static_QUType_int, 0, QUParameter::In },
	{ 0, &static_QUType_QString, 0, QUParameter::In }
    };
    static const QUMethod slot_39 = {"fix_U", 2, param_slot_39 };
    static const QUParameter param_slot_40[] = {
	{ 0, &static_QUType_int, 0, QUParameter::In },
	{ 0, &static_QUType_QString, 0, QUParameter::In }
    };
    static const QUMethod slot_40 = {"fix_mD", 2, param_slot_40 };
    static const QUParameter param_slot_41[] = {
	{ 0, &static_QUType_int, 0, QUParameter::In }
    };
    static const QUMethod slot_41 = {"UnitsChanged0", 1, param_slot_41 };
    static const QUParameter param_slot_42[] = {
	{ 0, &static_QUType_int, 0, QUParameter::In }
    };
    static const QUMethod slot_42 = {"UnitsChanged1", 1, param_slot_42 };
    static const QUParameter param_slot_43[] = {
	{ 0, &static_QUType_int, 0, QUParameter::In }
    };
    static const QUMethod slot_43 = {"UnitsChanged2", 1, param_slot_43 };
    static const QUParameter param_slot_44[] = {
	{ 0, &static_QUType_ptr, "CIniFile", QUParameter::InOut }
    };
    static const QUMethod slot_44 = {"saveSelectedBranch", 1, param_slot_44 };
    static const QUMethod slot_45 = {"setSubInputShockDynamics", 0, 0 };
    static const QUMethod slot_46 = {"setSubInputFanno", 0, 0 };
    static const QUMethod slot_47 = {"setSubInput", 0, 0 };
    static const QUParameter param_slot_48[] = {
	{ 0, &static_QUType_int, 0, QUParameter::In }
    };
    static const QUMethod slot_48 = {"setInputBoxValue", 1, param_slot_48 };
    static const QUParameter param_slot_49[] = {
	{ 0, &static_QUType_int, 0, QUParameter::In }
    };
    static const QUMethod slot_49 = {"valueCorrectionDueUnitsChange", 1, param_slot_49 };
    static const QUParameter param_slot_50[] = {
	{ 0, &static_QUType_int, 0, QUParameter::In },
	{ 0, &static_QUType_double, 0, QUParameter::In },
	{ 0, &static_QUType_int, 0, QUParameter::In },
	{ 0, &static_QUType_double, 0, QUParameter::InOut }
    };
    static const QUMethod slot_50 = {"temperatureConversion", 4, param_slot_50 };
    static const QUMethod slot_51 = {"languageChange", 0, 0 };
    static const QMetaData slot_tbl[] = {
	{ "init()", &slot_0, QMetaData::Public },
	{ "helpAbout()", &slot_1, QMetaData::Public },
	{ "helpIndex()", &slot_2, QMetaData::Public },
	{ "helpContents()", &slot_3, QMetaData::Public },
	{ "clearParameters()", &slot_4, QMetaData::Public },
	{ "setParameters()", &slot_5, QMetaData::Public },
	{ "clearInputBoxs()", &slot_6, QMetaData::Public },
	{ "setInput()", &slot_7, QMetaData::Public },
	{ "calculate()", &slot_8, QMetaData::Public },
	{ "LabelChanged()", &slot_9, QMetaData::Public },
	{ "fileSave()", &slot_10, QMetaData::Public },
	{ "fileSaveAs()", &slot_11, QMetaData::Public },
	{ "fileOpen()", &slot_12, QMetaData::Public },
	{ "save(QString)", &slot_13, QMetaData::Public },
	{ "canSave()", &slot_14, QMetaData::Public },
	{ "valueChange(double)", &slot_15, QMetaData::Public },
	{ "Load()", &slot_16, QMetaData::Public },
	{ "newValues()", &slot_17, QMetaData::Public },
	{ "selectBranch(branchType)", &slot_18, QMetaData::Public },
	{ "valueChange()", &slot_19, QMetaData::Public },
	{ "setNotice(inputType)", &slot_20, QMetaData::Public },
	{ "setNoticeIfNeeded()", &slot_21, QMetaData::Public },
	{ "branchSelected()", &slot_22, QMetaData::Public },
	{ "fix_mD()", &slot_23, QMetaData::Public },
	{ "onSubModelGroup()", &slot_24, QMetaData::Public },
	{ "clearSubGroup()", &slot_25, QMetaData::Public },
	{ "setSubParameters()", &slot_26, QMetaData::Public },
	{ "notImplementedYet(QString)", &slot_27, QMetaData::Public },
	{ "clearInfoBox()", &slot_28, QMetaData::Public },
	{ "setInfoBoxs()", &slot_29, QMetaData::Public },
	{ "setInfoNotice(QString)", &slot_30, QMetaData::Public },
	{ "newData()", &slot_31, QMetaData::Public },
	{ "fixP(int,QString)", &slot_32, QMetaData::Public },
	{ "fixT(int,QString)", &slot_33, QMetaData::Public },
	{ "fixRho(int,QString)", &slot_34, QMetaData::Public },
	{ "fix_s(int,QString)", &slot_35, QMetaData::Public },
	{ "fix_x(int)", &slot_36, QMetaData::Public },
	{ "fix_h(int,QString)", &slot_37, QMetaData::Public },
	{ "fix_u(int,QString)", &slot_38, QMetaData::Public },
	{ "fix_U(int,QString)", &slot_39, QMetaData::Public },
	{ "fix_mD(int,QString)", &slot_40, QMetaData::Public },
	{ "UnitsChanged0(int)", &slot_41, QMetaData::Public },
	{ "UnitsChanged1(int)", &slot_42, QMetaData::Public },
	{ "UnitsChanged2(int)", &slot_43, QMetaData::Public },
	{ "saveSelectedBranch(CIniFile&)", &slot_44, QMetaData::Public },
	{ "setSubInputShockDynamics()", &slot_45, QMetaData::Public },
	{ "setSubInputFanno()", &slot_46, QMetaData::Public },
	{ "setSubInput()", &slot_47, QMetaData::Public },
	{ "setInputBoxValue(int)", &slot_48, QMetaData::Public },
	{ "valueCorrectionDueUnitsChange(int)", &slot_49, QMetaData::Public },
	{ "temperatureConversion(int,double,int,double&)", &slot_50, QMetaData::Public },
	{ "languageChange()", &slot_51, QMetaData::Protected }
    };
    static const QUParameter param_signal_0[] = {
	{ 0, &static_QUType_QString, 0, QUParameter::In }
    };
    static const QUMethod signal_0 = {"newData", 1, param_signal_0 };
    static const QMetaData signal_tbl[] = {
	{ "newData(QString)", &signal_0, QMetaData::Public }
    };
    metaObj = QMetaObject::new_metaobject(
	"gdForm", parentObject,
	slot_tbl, 52,
	signal_tbl, 1,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    cleanUp_gdForm.setMetaObject( metaObj );
    return metaObj;
}

void* gdForm::qt_cast( const char* clname )
{
    if ( !qstrcmp( clname, "gdForm" ) )
	return this;
    return QMainWindow::qt_cast( clname );
}

// SIGNAL newData
void gdForm::newData( QString t0 )
{
    activate_signal( staticMetaObject()->signalOffset() + 0, t0 );
}

bool gdForm::qt_invoke( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->slotOffset() ) {
    case 0: init(); break;
    case 1: helpAbout(); break;
    case 2: helpIndex(); break;
    case 3: helpContents(); break;
    case 4: clearParameters(); break;
    case 5: setParameters(); break;
    case 6: clearInputBoxs(); break;
    case 7: setInput(); break;
    case 8: calculate(); break;
    case 9: static_QUType_int.set(_o,LabelChanged()); break;
    case 10: fileSave(); break;
    case 11: fileSaveAs(); break;
    case 12: fileOpen(); break;
    case 13: save((QString)static_QUType_QString.get(_o+1)); break;
    case 14: static_QUType_int.set(_o,canSave()); break;
    case 15: valueChange((double)static_QUType_double.get(_o+1)); break;
    case 16: Load(); break;
    case 17: newValues(); break;
    case 18: selectBranch((branchType)(*((branchType*)static_QUType_ptr.get(_o+1)))); break;
    case 19: valueChange(); break;
    case 20: setNotice((inputType)(*((inputType*)static_QUType_ptr.get(_o+1)))); break;
    case 21: setNoticeIfNeeded(); break;
    case 22: branchSelected(); break;
    case 23: fix_mD(); break;
    case 24: onSubModelGroup(); break;
    case 25: clearSubGroup(); break;
    case 26: setSubParameters(); break;
    case 27: notImplementedYet((QString)static_QUType_QString.get(_o+1)); break;
    case 28: clearInfoBox(); break;
    case 29: setInfoBoxs(); break;
    case 30: setInfoNotice((QString)static_QUType_QString.get(_o+1)); break;
    case 31: newData(); break;
    case 32: fixP((int)static_QUType_int.get(_o+1),(QString)static_QUType_QString.get(_o+2)); break;
    case 33: fixT((int)static_QUType_int.get(_o+1),(QString)static_QUType_QString.get(_o+2)); break;
    case 34: fixRho((int)static_QUType_int.get(_o+1),(QString)static_QUType_QString.get(_o+2)); break;
    case 35: fix_s((int)static_QUType_int.get(_o+1),(QString)static_QUType_QString.get(_o+2)); break;
    case 36: fix_x((int)static_QUType_int.get(_o+1)); break;
    case 37: fix_h((int)static_QUType_int.get(_o+1),(QString)static_QUType_QString.get(_o+2)); break;
    case 38: fix_u((int)static_QUType_int.get(_o+1),(QString)static_QUType_QString.get(_o+2)); break;
    case 39: fix_U((int)static_QUType_int.get(_o+1),(QString)static_QUType_QString.get(_o+2)); break;
    case 40: fix_mD((int)static_QUType_int.get(_o+1),(QString)static_QUType_QString.get(_o+2)); break;
    case 41: UnitsChanged0((int)static_QUType_int.get(_o+1)); break;
    case 42: UnitsChanged1((int)static_QUType_int.get(_o+1)); break;
    case 43: UnitsChanged2((int)static_QUType_int.get(_o+1)); break;
    case 44: saveSelectedBranch((CIniFile&)*((CIniFile*)static_QUType_ptr.get(_o+1))); break;
    case 45: setSubInputShockDynamics(); break;
    case 46: setSubInputFanno(); break;
    case 47: setSubInput(); break;
    case 48: setInputBoxValue((int)static_QUType_int.get(_o+1)); break;
    case 49: valueCorrectionDueUnitsChange((int)static_QUType_int.get(_o+1)); break;
    case 50: temperatureConversion((int)static_QUType_int.get(_o+1),(double)static_QUType_double.get(_o+2),(int)static_QUType_int.get(_o+3),(double&)static_QUType_double.get(_o+4)); break;
    case 51: languageChange(); break;
    default:
	return QMainWindow::qt_invoke( _id, _o );
    }
    return TRUE;
}

bool gdForm::qt_emit( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->signalOffset() ) {
    case 0: newData((QString)static_QUType_QString.get(_o+1)); break;
    default:
	return QMainWindow::qt_emit(_id,_o);
    }
    return TRUE;
}
#ifndef QT_NO_PROPERTIES

bool gdForm::qt_property( int id, int f, QVariant* v)
{
    return QMainWindow::qt_property( id, f, v);
}

bool gdForm::qt_static_property( QObject* , int , int , QVariant* ){ return FALSE; }
#endif // QT_NO_PROPERTIES
