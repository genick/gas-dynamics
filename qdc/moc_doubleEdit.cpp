/****************************************************************************
** WidDoubleEdit meta object code from reading C++ file 'doubleEdit.h'
**
** Created: Mon Mar 26 10:19:36 2012
**      by: The Qt MOC ($Id: qt/moc_yacc.cpp   3.3.8   edited Feb 2 14:59 $)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#undef QT_NO_COMPAT
#include "doubleEdit.h"
#include <qmetaobject.h>
#include <qapplication.h>

#include <private/qucomextra_p.h>
#if !defined(Q_MOC_OUTPUT_REVISION) || (Q_MOC_OUTPUT_REVISION != 26)
#error "This file was generated using the moc from 3.3.8b. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

const char *WidDoubleEdit::className() const
{
    return "WidDoubleEdit";
}

QMetaObject *WidDoubleEdit::metaObj = 0;
static QMetaObjectCleanUp cleanUp_WidDoubleEdit( "WidDoubleEdit", &WidDoubleEdit::staticMetaObject );

#ifndef QT_NO_TRANSLATION
QString WidDoubleEdit::tr( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "WidDoubleEdit", s, c, QApplication::DefaultCodec );
    else
	return QString::fromLatin1( s );
}
#ifndef QT_NO_TRANSLATION_UTF8
QString WidDoubleEdit::trUtf8( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "WidDoubleEdit", s, c, QApplication::UnicodeUTF8 );
    else
	return QString::fromUtf8( s );
}
#endif // QT_NO_TRANSLATION_UTF8

#endif // QT_NO_TRANSLATION

QMetaObject* WidDoubleEdit::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    QMetaObject* parentObject = QWidget::staticMetaObject();
    static const QUParameter param_slot_0[] = {
	{ "val", &static_QUType_double, 0, QUParameter::In }
    };
    static const QUMethod slot_0 = {"setValue", 1, param_slot_0 };
    static const QUParameter param_slot_1[] = {
	{ "slval", &static_QUType_int, 0, QUParameter::In }
    };
    static const QUMethod slot_1 = {"handle_slchange", 1, param_slot_1 };
    static const QUParameter param_slot_2[] = {
	{ "text", &static_QUType_QString, 0, QUParameter::In }
    };
    static const QUMethod slot_2 = {"setValue", 1, param_slot_2 };
    static const QUParameter param_slot_3[] = {
	{ "text", &static_QUType_QString, 0, QUParameter::In }
    };
    static const QUMethod slot_3 = {"handle_edchange", 1, param_slot_3 };
    static const QUMethod slot_4 = {"handle_edreturn", 0, 0 };
    static const QMetaData slot_tbl[] = {
	{ "setValue(double)", &slot_0, QMetaData::Public },
	{ "handle_slchange(int)", &slot_1, QMetaData::Private },
	{ "setValue(const QString&)", &slot_2, QMetaData::Private },
	{ "handle_edchange(const QString&)", &slot_3, QMetaData::Private },
	{ "handle_edreturn()", &slot_4, QMetaData::Private }
    };
    static const QUMethod signal_0 = {"returnPressed", 0, 0 };
    static const QUParameter param_signal_1[] = {
	{ "val", &static_QUType_double, 0, QUParameter::In }
    };
    static const QUMethod signal_1 = {"valueChanged", 1, param_signal_1 };
    static const QUParameter param_signal_2[] = {
	{ 0, &static_QUType_QString, 0, QUParameter::In }
    };
    static const QUMethod signal_2 = {"textChanged", 1, param_signal_2 };
    static const QMetaData signal_tbl[] = {
	{ "returnPressed()", &signal_0, QMetaData::Public },
	{ "valueChanged(double)", &signal_1, QMetaData::Public },
	{ "textChanged(const QString&)", &signal_2, QMetaData::Public }
    };
    metaObj = QMetaObject::new_metaobject(
	"WidDoubleEdit", parentObject,
	slot_tbl, 5,
	signal_tbl, 3,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    cleanUp_WidDoubleEdit.setMetaObject( metaObj );
    return metaObj;
}

void* WidDoubleEdit::qt_cast( const char* clname )
{
    if ( !qstrcmp( clname, "WidDoubleEdit" ) )
	return this;
    return QWidget::qt_cast( clname );
}

// SIGNAL returnPressed
void WidDoubleEdit::returnPressed()
{
    activate_signal( staticMetaObject()->signalOffset() + 0 );
}

// SIGNAL valueChanged
void WidDoubleEdit::valueChanged( double t0 )
{
    activate_signal( staticMetaObject()->signalOffset() + 1, t0 );
}

// SIGNAL textChanged
void WidDoubleEdit::textChanged( const QString& t0 )
{
    activate_signal( staticMetaObject()->signalOffset() + 2, t0 );
}

bool WidDoubleEdit::qt_invoke( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->slotOffset() ) {
    case 0: setValue((double)static_QUType_double.get(_o+1)); break;
    case 1: handle_slchange((int)static_QUType_int.get(_o+1)); break;
    case 2: setValue((const QString&)static_QUType_QString.get(_o+1)); break;
    case 3: handle_edchange((const QString&)static_QUType_QString.get(_o+1)); break;
    case 4: handle_edreturn(); break;
    default:
	return QWidget::qt_invoke( _id, _o );
    }
    return TRUE;
}

bool WidDoubleEdit::qt_emit( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->signalOffset() ) {
    case 0: returnPressed(); break;
    case 1: valueChanged((double)static_QUType_double.get(_o+1)); break;
    case 2: textChanged((const QString&)static_QUType_QString.get(_o+1)); break;
    default:
	return QWidget::qt_emit(_id,_o);
    }
    return TRUE;
}
#ifndef QT_NO_PROPERTIES

bool WidDoubleEdit::qt_property( int id, int f, QVariant* v)
{
    return QWidget::qt_property( id, f, v);
}

bool WidDoubleEdit::qt_static_property( QObject* , int , int , QVariant* ){ return FALSE; }
#endif // QT_NO_PROPERTIES
