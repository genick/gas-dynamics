#include <iostream>

using namespace std;

#include "iniFile.h"
#include "definition.h"
#include "gd/definition.h"
#include <map>
#include <string>

int readFile(double * variableValue, int * inputOutput,  string fileName)
{
	string whatInfo, variableName, isRange, isTex, className;
		
	CIniFile iniFile( fileName);
	iniFile.ReadFile();
	//cout << "NKeys = " << iniFile.NumKeys() << endl;
	//cout << "number of data" << iniFile.NumValues("data") << endl;
	whatInfo =     iniFile.GetValue("InputOutput", "whatInfo") ;
	variableName = iniFile.GetValue("InputOutput", "variableName") ;
	isRange = iniFile.GetValue("integer", "isRange") ;
	isTex = iniFile.GetValue("integer", "isTex") ;
	className = iniFile.GetValue("InputOutput", "className") ;
	cout << className << " and name of variable is " <<  variableName << endl;

	if (isTex == "yes") 
		inputOutput[isTexN] = yes ; 
	else 
		inputOutput[isTexN] = no ; 
		
	if (isRange == "yes") 
		inputOutput[isRangeN] = yes ; 
	else 
		inputOutput[isRangeN] = no ; 

	if (className == "stagnationFlow") 
		inputOutput[classNameN] = stagnationFlow;
	else if (className == "fannoFlow") 
		inputOutput[classNameN] = fannoFlow;
	else if (className == "shockFlow") 
		inputOutput[classNameN] = shockFlow;
	else if (className == "raylieghFlow") 
		inputOutput[classNameN] = rayleighFlow;
	else if (className == "obliqueFlow") 
		inputOutput[classNameN] = obliqueFlow;
	else //default 
		inputOutput[classNameN] = stagnationFlow;

		
cout << "isTex = " <<  inputOutput[isTexN] << "	" 
<< "	isRange = " <<  inputOutput[isRangeN] 
<< "	className = " <<  inputOutput[classNameN]<< endl ;

	if (whatInfo == "infoMinimal" ) 
		inputOutput[whatInfoN] = infoMinimal ; 
	else if (whatInfo == "infoStandard" )
		 inputOutput[whatInfoN] = infoStandard; 
	else if ( whatInfo == "infoTube") 
		 inputOutput[whatInfoN] =  infoTube; 
	else  
         inputOutput[whatInfoN] = infoStandard;

	if (variableName == "machV")
		inputOutput[variableNameN] = machV;
	else if (variableName == "PbarV")
		inputOutput[variableNameN] = PbarV;
	else if (variableName == "fldV")
		inputOutput[variableNameN] = fldV;
	else if (variableName == "p2p1FLDV")
		inputOutput[variableNameN] = p2p1FLDV;
	else if (variableName == "angleV")
		inputOutput[variableNameN] = angleV;
	else if (variableName == "M1ShockV")
		inputOutput[variableNameN] = M1ShockV;
	else if (variableName == "FLDShockV")
		inputOutput[variableNameN] = FLDShockV;
	else if (variableName == "M1fldV")
		inputOutput[variableNameN] = M1fldV;
	else if (variableName == "M1fldP2P1V")
		inputOutput[variableNameN] = M1fldP2P1V;
	else if (variableName == "MxV ")
		inputOutput[variableNameN] = MxV ;
	else if (variableName == "MyV ")
		inputOutput[variableNameN] = MyV ;
	else if (variableName == "MFLDshockLocationV")
		inputOutput[variableNameN] =  MFLDshockLocationV ;
	else if (variableName == "AstarV")
		inputOutput[variableNameN] = AstarV ;
	else if (variableName == "PARV")
		inputOutput[variableNameN] =  PARV ;
	else if (variableName == "P0yP0xV")
		inputOutput[variableNameN] = P0yP0xV ;
	else if (variableName == "TbarV")
		inputOutput[variableNameN] = TbarV ;
	else if (variableName == "TRstarV")
		inputOutput[variableNameN] = TRstarV ;
	else if (variableName == "TRzeroStarV")
		inputOutput[variableNameN] = TRzeroStarV ;
	else if (variableName == "deltaMV")
		inputOutput[variableNameN] =   deltaMV ;
	else if (variableName == "thetaMV")
		inputOutput[variableNameN] = thetaMV ;
	else if (variableName == "deltaThetaV")
		inputOutput[variableNameN] = deltaThetaV ;
	else if (variableName == "omegaV")
		inputOutput[variableNameN] = omegaV ;
	else 
		inputOutput[variableNameN] = machV;

//	iniFile.Path( "./iniTest.sav");
//	iniFile.WriteFile();

	variableValue[kN] = iniFile.GetValueF("data", "k") ;  
	variableValue[m1N] = iniFile.GetValueF("data", "m1") ;  
	variableValue[fldN] = iniFile.GetValueF("data", "fld") ;
	variableValue[p2p1N] = iniFile.GetValueF("data", "p2p1") ;
	variableValue[m2N] = iniFile.GetValueF("data", "m2") ; 
	variableValue[mN] = iniFile.GetValueF("data", "M") ; 
	variableValue[MxN] = iniFile.GetValueF("data", "Mx") ; 
	variableValue[MyN] = iniFile.GetValueF("data", "My") ; 
	variableValue[PbarN] = iniFile.GetValueF("data", "Pbar") ; 
	variableValue[TbarN] = iniFile.GetValueF("data", "Tbar") ;
	variableValue[TRstarN] = iniFile.GetValueF("data", "TRstar") ; 
	variableValue[TRzeroStarN] = iniFile.GetValueF("data", "TRzeroStar") ;    
	variableValue[AstarV] = iniFile.GetValueF("data", "Astar") ; 
	variableValue[PARN] = iniFile.GetValueF("data", "PAR") ; 
	variableValue[P0yP0xN] = iniFile.GetValueF("data", "P0yP0x") ;  
	variableValue[thetaN] = iniFile.GetValueF("data", "theta") ;
	variableValue[deltaMN] = iniFile.GetValueF("data", "delta") ;
	variableValue[angleN] = iniFile.GetValueF("data", "angle") ;
	variableValue[shockLocationN] = iniFile.GetValueF("data", "shockLocation");


//  Lookup what we have now.
//	for ( unsigned keyID = 0; keyID < iniFile.NumKeys(); ++keyID) {
//		cout << "Key = " << iniFile.KeyName( keyID) << endl;
//		for ( unsigned valueID = 0;
//				 valueID < iniFile.NumValues( keyID); ++valueID) {
//			cout << "   ValueName = " << iniFile.ValueName( keyID, valueID) <<
//				"  Value = " << iniFile.GetValue( keyID, valueID) << endl;
//		}
 // }
  return yes;
}

int readDefaultData(double* variableValue, int*  inputOutput ){
	double k, M, M1 , FLD, p2p1, Mx, My, Pbar, Tbar, TRstar,
		TRzeroStar,  Astar, PAR, P0yP0x, theta, delta, omega, nu;
	int whatInfo;
	//int isTex = yes;
	//int isRange = no;
	int variableName; 
/* the kind of information needed */
 	//whatInfo = infoStagnation ; // total properties (stagnation)
 	whatInfo = infoStandard ; //standard tube flow and shock (point) 
 	//whatInfo = infoMinimal ; //minimal weak shock 
 	//whatInfo = infoTube ; //print two side of tube flow 
 	//whatInfo = infoShock ; // two side of shock
 	//whatInfo = infoTubeProfile ; // the tube profile
 	//whatInfo = infoTubeShock ; // two sides and shock area
 	//whatInfo = infoObliqueMax ; //oblique max of parameters
/* the kind of information supplied */
	//variableName =  M1fldV; // M1 & fld given
	//variableName =  p2p1FLDV; // p2/p1, M1 & fld given
	//variableName =  MxV; // upsteam Mach number
	//variableName =  fldV; // fld given
	//variableName =  angleV; // the angle of depents on the class
	variableName =  machV; // Mach number
	//variableName =  AstarV; // the star area ratio
	//variableName =  M1ShockV; //    
	//variableName =  M1fldP2P1V; // the whole set variables M1, fld, p ratio
	//variableName =  MFLDshockLocationV; // 
	//variableName =  PbarV; // the pressure ratio 
	//variableName =  TbarV; // the temperature ratio 
	//variableName =  TRstarV; // T/T*
	//variableName =  TRzeroStarV; // 
	//variableName =  PARV; // 
	//variableName =  P0yP0xV; // 
	//variableName =  deltaMV; // 

	M1 = 1.5;
	FLD = 40.0 ;
	p2p1 = 0.5 ; 
	//M =  3.0 ;
	M =   1.2346 ;
	Mx = 1.2346  ;
	My = 0.2346  ;
	Pbar = 0.2619;
	Tbar = 0.4053 ;
	TRstar = 0.4053 ;
	TRzeroStar = 0.5016 ;
	Astar = 4.0;
	PAR = 1.5 ;
	P0yP0x = 0.5525;
	delta = 7.; 
	theta = 60. ;
	omega = 20.0;
	nu = 26.4449 ;
	k = 1.4;
		
	variableValue[kN]=k;
	variableValue[PbarN]=Pbar;
	variableValue[m1N]=M1;
	variableValue[fldN]=FLD;
	variableValue[p2p1N]=p2p1;
	variableValue[mN]=M;
	variableValue[MxN]=Mx;
	variableValue[MyN]=My;
	variableValue[AstarN]=Astar;
	variableValue[PARN]=PAR;
	variableValue[P0yP0xN]=P0yP0x;
	variableValue[TbarN]=Tbar;
	variableValue[TRstarN]=TRstar;
	variableValue[TRzeroStarN]=TRzeroStar;
	variableValue[thetaN]=theta; 
	variableValue[deltaMN]=delta;
	variableValue[angleN]=nu;
	inputOutput[variableNameN] = variableName ;
	inputOutput[whatInfoN] =  whatInfo ; 
	inputOutput[isTexN] =  no ; 
	inputOutput[isRangeN] =  no ; 
	inputOutput[classNameN] =  shockFlow ; 
	return yes;

}
