#	include <qmessagebox.h> 
#	include <cmath>
#	include <qfiledialog.h>
#	include <qcombobox.h>
#	include <qpixmap.h>
#	include <qptrvector.h>
//#	include <qcheckbox.h>
//#	 include <qvalueVector.h>

//general std class
#include <string> 

//this project class
#include "doubleEdit.h" 
#include "definition.h"
#include "iniFile.h"
#include "showResults.h"

double temperatureToRealTemp(int, double);
double conversionToReal (int , int ,  double );
double fromRealTempTemperature (int , double );
 
PropertyType U0, U1, U2;
//double Pconversion[4] = {1., 100000., 1000000., 14.5} 

class		QRadioButton;
class		WidDoubleEdit;
class		QString;
class 	QLabel;
class 	Qpixmap;
class		QCheckBox;
class 	QComboBox;
class 	ShowResults;
class 	Qstring; 
	QString filename, versionNumber = "0.5.1";
	double * tmpRange; 
	double initialValue[3][3]; //first location, second ={sub, super, both}
	int isSubModelON;
	int isValueSet[3][3] = {
				{no, no, no},
				{no, no, no},
				{no, no, no}
				};

	QString deltaCh, thetaCh, rhoCh, nuCh, labelStr;
	int isDataChanged = true ;
	int currentUnits [3] = { 0, 0 , 0} ;
#   define  PI 3.1415926535897932384626433832795028

	WidDoubleEdit *kInput;
	WidDoubleEdit *Input0;
	WidDoubleEdit *Input1;
	WidDoubleEdit *Input2;
	WidDoubleEdit *Input3;
	ShowResults *showResults;
	QRadioButton *inputRadio0;
	QRadioButton *inputRadio1;
	QRadioButton *inputRadio2;
	QRadioButton *inputRadio3;
	QRadioButton *inputRadio4;
	QRadioButton *inputRadio5;
	QRadioButton *inputRadio6;
	QRadioButton *inputRadio7;
	QRadioButton *inputRadio8;
	QRadioButton *inputRadio9;
	QRadioButton *inputRadio10;
	QRadioButton *inputRadio11;
	QRadioButton *radioSubSonic;
	QRadioButton *radioSuperSonic;
	QRadioButton *radioBothSonic;
	QRadioButton *radioStandard;
	QComboBox* Units0;
	QComboBox* Units1;
	QComboBox* Units2;
	QLabel * unChokedL, * picExplanation;
	int row0, row1, row2, coloum0, coloum1, coloum2, coloum3, coloum4;
	//QPixmap pixExplantion;
	
	//iterations of the parameter boxes
	typedef QPtrList<QRadioButton> ParametersList;
	ParametersList paraList;
	typedef QPtrListIterator  <QRadioButton> ParametersIterator;
	ParametersIterator parametersIterator (paraList);

	//iterations of the sub parameter boxes
	typedef QPtrList<QRadioButton> SubParametersList;
	SubParametersList subParaList;
	typedef QPtrListIterator  <QRadioButton> SubParametersIterator;
	SubParametersIterator subParametersIterator (subParaList);

	//iterations of the parameter comboBox
	typedef QPtrList<QComboBox> UnitsList;
	UnitsList  unitsList;
	typedef QPtrListIterator  <QComboBox> UnitsIterator;
	UnitsIterator unitsIterator (unitsList);

	//iterations of the input boxes
	typedef QPtrList<WidDoubleEdit> InputBoxList;
	InputBoxList inBoxList;
	typedef QPtrListIterator  <WidDoubleEdit> InputBoxIterator;
	InputBoxIterator inBoxIterator (inBoxList);

	//iterations of the pix Labels  
	typedef QPtrList<QLabel> PixList;
	PixList  pixList;
	typedef QPtrListIterator  <QLabel> PixIterator;
	PixIterator pixIterator (pixList);

void gdForm::init()
{ 
	//notImplementedYet ( "just starting the init" );
	double k;
	deltaCh = QChar( 0x03b4);
	thetaCh = QChar( 0x03b8);
	rhoCh = QChar( 0x03C1);
	nuCh = QChar( 0x03bd);
	tmpRange = new double[2];
	isSubModelON = no;

	kInput = new WidDoubleEdit (1.2, 2.0, 5,"Cp/Cv=k", this, "kInput") ;
	connect(kInput, SIGNAL(valueChanged(double )),
            this, SLOT(valueChange(double )));
            //this, SIGNAL(valueChange(double value)));
//    connect(kInput, SIGNAL(textChanged(const QString&)),
//            this, SLOT(valueChange(double value)));
//    connect(kInput, SIGNAL(clicked()),
//            this, SLOT(valueChanged(double value)));
	kInput->setGeometry( QRect( 300, 37, 150, 45 ) );
	kInput->setValue( 1.4 );
	k =  kInput->value();
	whatInputGroup->setEnabled(false);
	inputDataGroup->setEnabled(false);
   	//MyInput = new WidDoubleEdit ( sqrt(.5*(k -1.)/k),
		//		 (1.0-EPS), 5,"My", inputDataGroup);
	//PARInput = new WidDoubleEdit (0., 300.0, 5,"PAR", inputDataGroup);
	Input0 = new WidDoubleEdit (0., 300.0, 5,"PAR0", inputDataGroup);
	Input1 = new WidDoubleEdit (0., 300.0, 5,"PAR1", inputDataGroup);
	Input2 = new WidDoubleEdit (0., 300.0, 5,"PAR2", inputDataGroup);
	Input3 = new WidDoubleEdit (0., 300.0, 5,"PAR3", inputDataGroup);
	//insert the input Boxes into a list 
	inBoxList.append(Input0);
	inBoxList.append(Input1);
	inBoxList.append(Input2);
	inBoxList.append(Input3);

	Units0 = new QComboBox( FALSE, inputDataGroup, "Units1" );
	Units1 = new QComboBox( FALSE, inputDataGroup, "Units2" );
	Units2 = new QComboBox( FALSE, inputDataGroup, "Units3" );
	unitsList.append(Units0);
	unitsList.append(Units1);
	unitsList.append(Units2);
	for( unitsIterator.toFirst(); unitsIterator.current();
				++unitsIterator ) {
		unitsIterator.current()->setFocusPolicy( QComboBox::NoFocus); 
		unitsIterator.current()->setEnabled(false); 
		unitsIterator.current()->setShown(false);
		unitsIterator.current()->clear();
		unitsIterator.current()->insertItem( tr( "SI" ) );
		unitsIterator.current()->insertItem("is English units working?");
	}

	//connect(MyInput, SIGNAL(valueChanged(double )), this, SLOT(newData()));
	radioSubSonic = new  QRadioButton (  inputDataGroup, "SubSonic" );
	radioSubSonic->setGeometry( QRect( 300, 15, 150, 20 ) );
	radioSubSonic->setText("SubSonic");
	radioSubSonic->setShown(false);
	radioSuperSonic = new QRadioButton (  inputDataGroup, "SuperSonic" );
	radioSuperSonic->setGeometry( QRect( 300, 45, 150, 20 ) );
	radioSuperSonic->setText("SuperSonic");
	radioSuperSonic->setShown(false);
	radioBothSonic = new QRadioButton (  inputDataGroup, "Both" );
	radioBothSonic->setGeometry( QRect( 300, 75, 150, 20 ) );
	radioBothSonic->setText("Both");
	radioBothSonic->setShown(false);
	radioBothSonic->setEnabled(false);
	row0 = 20;
	row1 = 45;
	row2 = 70;
	coloum0 = 10;
	coloum1 = 100;
	coloum2 = 190;
	coloum3 = 280;
	coloum4 = 420;

	inputRadio0 = new QRadioButton (  whatInputGroup, "inputRadio0" );
	inputRadio0->setGeometry( QRect( coloum0, row0, 80, 20 ) );
	inputRadio1 = new QRadioButton (  whatInputGroup, "inputRadio1" );
	inputRadio1->setGeometry( QRect( coloum0, row1, 80, 20 ) );
	inputRadio2 = new QRadioButton (  whatInputGroup, "inputRadio2" );
	inputRadio2->setGeometry( QRect( coloum0, row2, 80, 20 ) );
	inputRadio3 = new QRadioButton (  whatInputGroup, "inputRadio3" );
	inputRadio3->setGeometry( QRect( coloum1, row0, 80, 20 ) );
	inputRadio4 = new QRadioButton (  whatInputGroup, "inputRadio4" );
	inputRadio4->setGeometry( QRect( coloum1, row1, 80, 20 ) );
	inputRadio5 = new QRadioButton (  whatInputGroup, "inputRadio5" );
	inputRadio5->setGeometry( QRect( coloum1, row2, 80, 20 ) );
	inputRadio6 = new QRadioButton (  whatInputGroup, "inputRadio6" );
	inputRadio6->setGeometry( QRect( coloum2, row0, 80, 20 ) );
	inputRadio7 = new QRadioButton (  whatInputGroup, "inputRadio7" );
	inputRadio7->setGeometry( QRect( coloum2, row1, 80, 20 ) );
	inputRadio8 = new QRadioButton (  whatInputGroup, "inputRadio8" );
	inputRadio8->setGeometry( QRect( coloum2, row2, 80, 20 ) );
	inputRadio9 = new QRadioButton (  whatInputGroup, "inputRadio9" );
	inputRadio9->setGeometry( QRect( coloum3, row0, 80, 20 ) );
	inputRadio10 = new QRadioButton (  whatInputGroup, "inputRadio10" );
	inputRadio10->setGeometry( QRect( coloum3, row1, 80, 20 ) );
	inputRadio11 = new QRadioButton (  whatInputGroup, "inputRadio11" );
	inputRadio11->setGeometry( QRect( coloum3, row2, 80, 20 ) );
	
	//insert the parameter radio into a list 
	paraList.append(inputRadio0);
	paraList.append(inputRadio1);
	paraList.append(inputRadio2);
	paraList.append(inputRadio3);
	paraList.append(inputRadio4);
	paraList.append(inputRadio5);
	paraList.append(inputRadio6);
	paraList.append(inputRadio7);
	paraList.append(inputRadio8);
	paraList.append(inputRadio9);
	paraList.append(inputRadio10);
	paraList.append(inputRadio11);

	//insert the parameter radio into a list 
	subParaList.append(suddenOpenRadio);
	subParaList.append(suddenCloseRadio);
	subParaList.append(partiallyOpenRadio);
	subParaList.append(partiallyCloseRadio);

	//insert the pixlabel into a list 
	pixList.append(pixExLabel0);
	pixList.append(pixExLabel1);
	pixList.append(pixExLabel2);
	pixList.append(pixExLabel3);

	//label for unchoked flow extra info
	unChokedL = new QLabel( whatInputGroup, "Un Choked Flow" );
	unChokedL->setGeometry( QRect( coloum3, row0, 200, 30 ) );
	//unChokedL->setShown(false);

    connect(radioSubSonic, SIGNAL(clicked()),
            this, SLOT(branchSelected()));
    connect(radioSuperSonic, SIGNAL(clicked()),
            this, SLOT(branchSelected()));
    connect(radioBothSonic, SIGNAL(clicked()),
            this, SLOT(branchSelected()));
    //connect(radioMaximum, SIGNAL(clicked()),
     //       this, SLOT(allowMaxOblique()));
	connect( Units0, SIGNAL( activated( int ) ),
		 this, SLOT( UnitsChanged0( int ) ) );
	connect( Units1, SIGNAL( activated( int ) ),
		 this, SLOT( UnitsChanged1( int ) ) );
	connect( Units2, SIGNAL( activated( int ) ),
		 this, SLOT( UnitsChanged2( int ) ) );

	//sub model set up and connections
	//connect(subModelGroup, SIGNAL(clicked(int)), this,SLOT(setSubInput()));
	
	radioStandard->setChecked(true);
	string notice;
	notice = "The range of Possible Values that can be used<p>";
	notice += "when I get to implement it";
	//rangeNotice->setText(notice);
	clearParameters() ;
	clearInputBoxs() ;
	clearSubGroup() ;
}

void gdForm::helpAbout()
{
	QString notice,  caption = "Potto Gas Dynamics Calculator";
	notice = "<h2>Gas Dynamics Calculator " ;
	notice += "version " + versionNumber;
	notice += "</h2>" ;
	notice += "<p>Copyright &copy; 2005, 2006, 2007, 2008 Dr. Genick Bar-Meir";
   notice += "<p>Gas Dynamics Calculator is a utility that " ;
	notice += "calculates several quantities in different ";
	notice += " compressible flow models.  ";
	QMessageBox::about(this , caption, notice );
}


void gdForm::helpIndex()
{
	notImplementedYet("");
}

void gdForm::helpContents()
{
	notImplementedYet("will be finished when I have the time");
}

void gdForm::clearParameters()
{
	whatInputGroup->setButton(-1);
	//notImplementedYet();
	unChokedL->setShown(false);
	pixGroup->setShown(false);
	pixGroup->setEnabled(false);
   infoNotice->setShown(false);
   infoNotice->setEnabled(false);

	//pixExLabel0->setShown(false);
	//pixExLabel0->setEnabled(false);
	//pixExLabel0->setGeometry( QRect( 0, 0, 300, 150 ) );
	for( pixIterator.toFirst(); pixIterator.current();
				++pixIterator ) {
		//notImplementedYet( "this is after the for loop" );
		pixIterator.current()->setEnabled(false); 
		//notImplementedYet( parametersIterator.current()->text() );
		pixIterator.current()->setShown(false);
		pixIterator.current()->setGeometry( QRect( 20, 25, 250, 125 ) );
	}
	
	for( parametersIterator.toFirst(); parametersIterator.current();
				++parametersIterator ) {
		//notImplementedYet( "this is after the for loop" );
		parametersIterator.current()->setEnabled(false); 
		//notImplementedYet( parametersIterator.current()->text() );
		parametersIterator.current()->setShown(false);
		parametersIterator.current()->setChecked(false);
	}
	radioSubSonic->setEnabled(false);
	radioSubSonic->setChecked(false);
	radioSubSonic->setShown(false);
	radioSuperSonic->setEnabled(false);
	radioSuperSonic->setChecked(false);
	radioSuperSonic->setShown(false);
	radioBothSonic->setEnabled(false);
	radioBothSonic->setChecked(false);
	radioBothSonic->setShown(false);
	//rangeNotice->setShown(false);
	//rangeNotice->setEnabled(false);
}

void gdForm::setParameters()
{
	if (modelGroup->selected() != 0){
		whatInputGroup->setEnabled(true);	
		isDataChanged = true;
	}
	clearParameters();
	clearSubGroup();
	clearInputBoxs();
	int numberOfIterations = 0; 
	if (modelGroup->selected() == radioShock){
		for( 	parametersIterator.toFirst(); 
				numberOfIterations < numberOfParameters[shockFlow];
					++parametersIterator ) {
			parametersIterator.current()->setEnabled(true); 
			parametersIterator.current()->setShown(true);
			parametersIterator.current()->setText
				(shockLayoutName[numberOfIterations]);
			numberOfIterations++;
		}
		//do corrections the name of the inputs
		labelStr = rhoCh + "y" + rhoCh + "x";
		paraList.at(RyRxS)->setText(labelStr);
		labelStr = rhoCh + "0y" + rhoCh + "0x";
		paraList.at(R0yR0xS)->setText(labelStr);
		isSubModelON = no;
	}
	else if (modelGroup->selected() == radioIsentropic){
		for( 	parametersIterator.toFirst(); 
				numberOfIterations < numberOfParameters[stagnationFlow];
					++parametersIterator ) {
			parametersIterator.current()->setEnabled(true); 
			parametersIterator.current()->setShown(true);
			parametersIterator.current()->setText
				(isentropicLN[numberOfIterations]);
			numberOfIterations++;
		}
		//do corrections the name of the inputs
		labelStr = rhoCh +  "/" + rhoCh + "*" ;
		paraList.at(RbarI)->setText(labelStr);
		isSubModelON = no;
    }
	else if (modelGroup->selected() == radioFanno){
		onSubModelGroup();
		//unChokedL->setShown(true);
		//unChokedL->setEnabled(true);
		//notImplementedYet("after unchoked");
		isSubModelON = yes;
    }
	else if (modelGroup->selected() == radioOblique){
		for( 	parametersIterator.toFirst(); 
				numberOfIterations < numberOfParameters[obliqueFlow];
					++parametersIterator ) {
			parametersIterator.current()->setEnabled(true); 
			parametersIterator.current()->setShown(true);
			parametersIterator.current()->setText
				(obliqueLayoutName[numberOfIterations]);
			numberOfIterations++;
		}
		// do corrections for the Greek symbols
		inputRadio0->setEnabled(true);
		inputRadio0->setShown(true);
		labelStr = deltaCh + " and " + thetaCh;
		inputRadio0->setText(labelStr);
		inputRadio1->setEnabled(true);
		inputRadio1->setShown(true);
		labelStr = deltaCh + " and M1" ;
		inputRadio1->setText(labelStr);
		inputRadio2->setEnabled(true);
		inputRadio2->setShown(true);
		labelStr = thetaCh + " and M1" ;
		inputRadio2->setText(labelStr);
		isSubModelON = no;
	}
	else if (modelGroup->selected() == radioShockDynamics){
		onSubModelGroup();
	}
	else if (modelGroup->selected() == radioRayleigh){
		for( 	parametersIterator.toFirst(); 
				numberOfIterations < numberOfParameters[rayleighFlow];
					++parametersIterator ) {
			parametersIterator.current()->setEnabled(true); 
			parametersIterator.current()->setShown(true);
			parametersIterator.current()->setText
				(rayleighLN[numberOfIterations]);
			numberOfIterations++;
		}
		//do corrections the name of the inputs
		labelStr = rhoCh +  "/" + rhoCh + "*" ;
		paraList.at(RbarR)->setText(labelStr);
		labelStr = rhoCh +  "0/" + rhoCh + "0*" ;
		paraList.at(R0barR)->setText(labelStr);
		isSubModelON = no;
    }
	else if (modelGroup->selected() == radioPM){
		for( 	parametersIterator.toFirst(); 
				numberOfIterations < numberOfParameters[pmFlow];
					++parametersIterator ) {
			parametersIterator.current()->setEnabled(true); 
			parametersIterator.current()->setShown(true);
			parametersIterator.current()->setText
				(PMLN[numberOfIterations]);
			numberOfIterations++;
		}
		//do corrections the name of the inputs
		labelStr = nuCh ;
		paraList.at(nuIs)->setText(labelStr);
		labelStr = rhoCh + "/" + rhoCh + "*" ;
		paraList.at(RbarIs)->setText(labelStr);
		isSubModelON = no;
    }
	else if (modelGroup->selected() == radioIsothermal){
		for( 	parametersIterator.toFirst(); 
				numberOfIterations < numberOfParameters[isothermalFlow];
					++parametersIterator ) {
			parametersIterator.current()->setEnabled(true); 
			parametersIterator.current()->setShown(true);
			parametersIterator.current()->setText
				(isothermalLayoutName[numberOfIterations]);
			numberOfIterations++;
		}
		//do corrections the name of the input
		labelStr = rhoCh +  "/" + rhoCh + "*" ;
		paraList.at(RbarIso)->setText(labelStr);;
		labelStr = rhoCh +  "0/" + rhoCh + "0*" ;
		paraList.at(R0barIso)->setText(labelStr);;
		isSubModelON = no;
    }
	else if (modelGroup->selected() == radioIsoNozzle){
		for( 	parametersIterator.toFirst(); 
				numberOfIterations < numberOfParameters[isothermalNozzleFlow];
					++parametersIterator ) {
			parametersIterator.current()->setEnabled(true); 
			parametersIterator.current()->setShown(true);
			parametersIterator.current()->setText
				(isothermalNoLayoutName[numberOfIterations]);
			numberOfIterations++;
		}
		//do corrections the name of the input
		labelStr = rhoCh +  "/" + rhoCh + "*" ;
		paraList.at(RbarIsoNo)->setText(labelStr);;
		labelStr = rhoCh +  "0/" + rhoCh + "0*" ;
		paraList.at(R0barIsoNo)->setText(labelStr);;
		isSubModelON = no;
    }
}

void gdForm::clearInputBoxs()
{
	//notImplementedYet("clearInputBoxs");
    //pInput->setEnabled(false);
    //pInput->setShown(false);
	//PARInput->setEnabled(false);
	//notImplementedYet("PAR setEnabled false");
	//PARInput->setShown(false);
	infoNotice->setShown(false);
	infoNotice->setEnabled(false);
	for( inBoxIterator.toFirst(); inBoxIterator.current(); ++inBoxIterator ) {
		//notImplementedYet("inside the iterator input box ");
		inBoxIterator.current()->setEnabled(false); 
		inBoxIterator.current()->setShown(false);
	}
	//make it known that units now at the first selection
	for ( int i= 0 ; i < 3 ; i++ ) {
		currentUnits[i] = 0; 
	}
	//Input0->setEnabled(false);
	//Input0->setShown(false);
	//Input1->setEnabled(false);
	//Input1->setShown(false);
	//Input2->setEnabled(false);
	//Input2->setShown(false);
	//Input3->setEnabled(false);
	//Input3->setShown(false);
	radioSubSonic->setEnabled(false);
	radioSubSonic->setChecked(false);
	radioSubSonic->setShown(false);
	radioSuperSonic->setEnabled(false);
	radioSuperSonic->setChecked(false);
	radioSuperSonic->setShown(false);
	radioBothSonic->setEnabled(false);
	radioBothSonic->setChecked(false);
	radioBothSonic->setShown(false);
	radioBothSonic->setChecked(false);
	inputDataGroup->setButton(-1) ;
	Units0->setShown(false);
	Units0->setEnabled(false);
	Units1->setShown(false);
	Units1->setEnabled(false);
	Units2->setShown(false);
	Units2->setEnabled(false);
	whatInputGroup->setChecked(-1);
}

void gdForm::setInput()
{
	//initial no value of input box
	for (int i = 0 ; i <3 ;i++ ){
		isValueSet[i][subsonic] = no;
	}
	if (whatInputGroup->selected() != 0){
		//notImplementedYet("before clearInputBoxs");
		clearInputBoxs();
		//notImplementedYet("after clearInputBoxs");
		inputDataGroup->setEnabled(true);
		isDataChanged = true;
	}
	else {
		//is this is the twice places?
		clearInputBoxs();
		return;
	}
	// twice getting here net need?
	//clearInputBoxs();
	//rangeNotice->setShown(false);
	//inptType inputBox;
	int numberOfBox = 1;
	double k ;
	k =  kInput->value();
	if (modelGroup->selected() == radioShock){
		//notImplementedYet("before of set input geometry");
		Input0->setGeometry( QRect( 30, 37, 180, 45 ) );
		Input0->setEnabled(true);
		Input0->setShown(true);
		selectBranch(__noBranch);
		radioSuperSonic->setEnabled(true);
		radioSuperSonic->setChecked(true);
		radioSuperSonic->setShown(false);
		if (whatInputGroup->selected() == paraList.at(MxS) ){//Mx
			Input0->setLabel(paraList.at(MxS)->text());
			Input0->setRange(1.0+EPS, 30.0 );
			isValueSet[location0][supersonic] = yes;
			initialValue[location0][supersonic] = 1.5;
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(MyS) ){//My
			Input0->setLabel(paraList.at(MyS)->text());
			Input0->setRange( sqrt(.5*(k -1.)/k)+EPS, 1.0);
			isValueSet[location0][subsonic] = no;
			isValueSet[location0][supersonic] = yes;
			initialValue[location0][supersonic] = .6;
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(PyPxS) ){ // PyPx
			Input0->setLabel(paraList.at(PyPxS)->text());
			Input0->setRange( 1.0, 100.);
			isValueSet[location0][supersonic] = yes;
			initialValue[location0][supersonic] = 1.25;
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(TyTxS) ){//TyTx
			Input0->setLabel(paraList.at(TyTxS)->text());
			Input0->setRange( 1.0, 100.);
			isValueSet[location0][supersonic] = yes;
			initialValue[location0][supersonic] = 1.4;
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(RyRxS) ){//RyRx
			Input0->setLabel(inputRadio4->text());
			Input0->setRange( 1.0, (k+1.)/(k-1.));
			isValueSet[location0][supersonic] = yes;
			initialValue[location0][supersonic] = 1.3;
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(P0yP0xS) ){ //P0y/P0x
			Input0->setLabel(paraList.at(P0yP0xS)->text());
			Input0->setRange(EPS, 1.0);
			isValueSet[location0][supersonic] = no;
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(R0yR0xS) ){ //R0y/R0x
			Input0->setLabel(paraList.at(R0yR0xS)->text());
			Input0->setRange(EPS, 1.0);
			isValueSet[location0][supersonic] = no;
			numberOfBox = 1;
		}
		else { //do nothing
		}
		setInputBoxValue(numberOfBox);
	}
	else if (modelGroup->selected() == radioIsentropic){
		Input0->setGeometry( QRect( 30, 37, 180, 45 ) );
		Input0->setEnabled(true);
		Input0->setShown(true);
		selectBranch(__noBranch);
		if (whatInputGroup->selected() == paraList.at(MI) ){ // Mach
			Input0->setLabel(paraList.at(MI)->text());
			Input0->setRange(0.0+EPS, 50.0 );
			isValueSet[location0][subsonic] = yes;
			initialValue[location0][subsonic] = 0.5;
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(PbarI)){//Pbar
			Input0->setLabel(paraList.at(PbarI)->text());
			Input0->setRange(pow((2./(k+1)),(k /(k -1.))) ,1.);
			isValueSet[location0][subsonic] = no;
			isValueSet[location0][supersonic] = yes;
			initialValue[location0][supersonic] = 0.5;
			selectBranch(simple);
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(TbarI) ){//Tbar
			Input0->setLabel(paraList.at(TbarI)->text());
			Input0->setRange((2.0/(k+1.)) ,1.0);
			isValueSet[location0][subsonic] = no;
			isValueSet[location0][supersonic] = no;
			selectBranch(simple) ;
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(RbarI)){//Rbar
			Input0->setLabel(paraList.at(RbarI)->text());
			Input0->setRange(pow((2.0/(k+1.)), (1. /(k -1.))) ,1.0);
			isValueSet[location0][subsonic] = no;
			isValueSet[location0][supersonic] = no;
			selectBranch(simple);
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(AstarI)){//A/A*
			setInfoNotice 
				("clicking on both give both branches (subsonic and supersonic)");
			Input0->setLabel(paraList.at(AstarI)->text());
			Input0->setRange(1.0, 1000.);
			isValueSet[location0][subsonic] = yes;
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][bothsonic] = yes;
			initialValue[location0][subsonic] = 1.3;
			initialValue[location0][supersonic] = 1.4;
			initialValue[location0][bothsonic] = 1.35;
			selectBranch(all) ;
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(PARI) ){//PAR
			Input0->setLabel(paraList.at(PARI)->text());
			Input0->setRange(1.0 +EPS , 100.);
			isValueSet[location0][subsonic]=isValueSet[location0][subsonic]=yes;
			isValueSet[location0][subsonic] = yes;
			isValueSet[location0][supersonic] = yes;
			initialValue[location0][subsonic] = 1.6;
			initialValue[location0][supersonic] = 0.5;
			selectBranch(simple) ;
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(P0TI) ){//P0 T
			U0=Pp;
			U1=Tp;
			U2=mDotAreap;
			int xLocation[3] = {30, 195, 350}; 
			int xUnits[3] = {U0, U1, U2}; 
			for(int j =0 ; j < 3 ; j++) {
				inBoxList.at(j)->setGeometry( QRect( xLocation[j], 15, 150, 45));
				inBoxList.at(j)->setEnabled(true);
				inBoxList.at(j)->setShown(true);
				inBoxList.at(j)->setRange(rangeLimits[xUnits[j]][0],
					rangeLimits[xUnits[j]][1]);
				unitsList.at(j)->setEnabled(true);
				unitsList.at(j)->setShown(true);
				unitsList.at(j)->setGeometry( 
						QRect( xLocation[j]+ 20, 65, 110, 20));
				isValueSet[j][subsonic] = no;
			}
			//arranging the the comboBoxes
			fixP(0, "P0");
			fixT(1, "T");
			fix_mD(2, "m^/A");
			isValueSet[location0][subsonic] = yes;
			isValueSet[location0][supersonic] = yes;
			initialValue[location0][subsonic] = 1.6;
			initialValue[location0][supersonic] = 1.5;
			UnitsChanged0(0);
			Input0->setValue(initialValue[location0][subsonic]);
			numberOfBox = 3;
		}
		else if (whatInputGroup->selected() == paraList.at(PT0I) ){//P T0
			U0=Pp;
			U1=Tp;
			U2=mDotAreap;
			int xLocation[3] = {30, 195, 350}; 
			int xUnits[3] = {U0, U1, U2}; 
			for(int j =0 ; j < 3 ; j++) {
				inBoxList.at(j)->setGeometry( QRect( xLocation[j], 15, 150, 45));
				inBoxList.at(j)->setEnabled(true);
				inBoxList.at(j)->setShown(true);
				inBoxList.at(j)->setRange(rangeLimits[xUnits[j]][0],
					rangeLimits[xUnits[j]][1]);
				unitsList.at(j)->setEnabled(true);
				unitsList.at(j)->setShown(true);
				unitsList.at(j)->setGeometry( 
						QRect( xLocation[j]+ 20, 70, 110, 20));
				isValueSet[j][subsonic] = no;
			}
			//arranging the the comboBoxes
			fixP(0, "P");
			fixT(1, "T0");
			fix_mD(2, "m^/A");
			isValueSet[location0][subsonic] = yes;
			isValueSet[location0][supersonic] = yes;
			initialValue[location0][subsonic] = 1.6;
			initialValue[location0][supersonic] = 1.5;
			UnitsChanged0(0);
			Input0->setValue(initialValue[location0][subsonic]);
			numberOfBox = 3;
		}
		//set the value of the input box
		setInputBoxValue(numberOfBox);
	}
	else if (modelGroup->selected() == radioPM){
		Input0->setGeometry( QRect( 30, 37, 180, 45 ) );
		Input0->setEnabled(true);
		Input0->setShown(true);
		selectBranch(__noBranch);
		radioSuperSonic->setEnabled(true);
		radioSuperSonic->setChecked(true);
		radioSuperSonic->setShown(false);
		if (whatInputGroup->selected() == paraList.at(MIs) ){ // Mach
			Input0->setLabel( paraList.at(MIs)->text());
			//notImplementedYet("after inputRadio0 setInput");
			Input0->setRange(1.0+EPS, 50.0 );
			isValueSet[location0][subsonic] = yes;
			initialValue[location0][subsonic] = 1.3;
			isValueSet[location0][supersonic] = yes;
			initialValue[location0][supersonic] = 1.3;
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(nuIs) ){ // nu
			Input0->setLabel(paraList.at(nuIs)->text());
			//notImplementedYet("after inputRadio0 setInput");
			Input0->setRange(.0+EPS, 50.0 );
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][subsonic] = no;
			initialValue[location0][supersonic] = 5.;
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(PbarIs) ){ // Pbar
			Input0->setLabel(paraList.at(PbarIs)->text());
			Input0->setRange(0., pow((2./(k+1)),(k /(k -1.))) );
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][subsonic] = no;
			initialValue[location0][supersonic] = 0.3;
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(TbarIs) ){ // Tbar
			Input0->setLabel(paraList.at(TbarIs)->text());
			Input0->setRange(0.+EPS, (2.0/(k+1.)));
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][subsonic] = no;
			initialValue[location0][supersonic] = 0.27;
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(RbarIs) ){ // Rbar
			Input0->setLabel(paraList.at(RbarIs)->text());
			Input0->setRange(0., pow((2.0/(k+1.)), (1. /(k -1.))) );
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][subsonic] = no;
			initialValue[location0][supersonic] = 0.4;
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(AstarIs) ){
			Input0->setLabel(paraList.at(AstarIs)->text());
			Input0->setRange(1., 30. );
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][subsonic] = no;
			initialValue[location0][supersonic] = 1.5;
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(PARIs) ){
			Input0->setLabel(paraList.at(PARIs)->text());
			Input0->setRange(EPS, 1. );
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][subsonic] = no;
			initialValue[location0][supersonic] = .5;
			numberOfBox = 1;
		}
		setInputBoxValue(numberOfBox);
	}
	else if (modelGroup->selected() == radioIsoNozzle){
		Input0->setGeometry( QRect( 30, 37, 180, 45 ) );
		Input0->setEnabled(true);
		Input0->setShown(true);
		if (whatInputGroup->selected() == paraList.at(MIsoNo) ){
			Input0->setLabel(paraList.at(MIsoNo)->text());
			//notImplementedYet("after inputRadio0 setInput");
			Input0->setRange(0.0+EPS, 50.0 );
			isValueSet[location0][subsonic] = yes;
			isValueSet[location0][supersonic] = yes;
			initialValue[location0][subsonic] = 0.5;
			initialValue[location0][supersonic] = 1.5;
			selectBranch(__noBranch) ;
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(PbarIsoNo)){
			Input0->setLabel(paraList.current()->text());
			Input0->setRange(1.0, exp(k*0.5) );
			isValueSet[location0][subsonic] = yes;
			isValueSet[location0][subsonic] = yes;
			initialValue[location0][subsonic] = 1.5;
			initialValue[location0][supersonic] = 0.5;
			selectBranch(simple) ;
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(RbarIsoNo)){
			Input0->setLabel(paraList.current()->text());
			Input0->setRange(EPS, exp(k*0.5) );
			isValueSet[location0][subsonic] = no;
			isValueSet[location0][supersonic] = no;
			selectBranch(simple);
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(P0barIsoNo)){
			Input0->setLabel(paraList.current()->text());
			Input0->setRange(1., exp(k*0.5)* pow((2.0/(k+1.)), (k /(k -1.))) );
			selectBranch(simple);
			isValueSet[location0][subsonic] = yes;
			isValueSet[location0][supersonic] = no;
			initialValue[location0][subsonic] = 1.01;
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(T0barIsoNo)){
			Input0->setLabel(paraList.current()->text());
			Input0->setRange( (2./(1.+k)+EPS), 1.0);
			isValueSet[location0][subsonic] = no;
			isValueSet[location0][supersonic] = yes;
			initialValue[location0][supersonic] = 1.8;
			selectBranch(simple);
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(R0barIsoNo)){
			Input0->setLabel(paraList.current()->text());
			Input0->setRange(	
					(1./exp(k*0.5)* pow((2.0/(k+1.)), (k /(k-1.)))), 300.);
			isValueSet[location0][subsonic] = no;
			isValueSet[location0][supersonic] = yes;
			initialValue[location0][supersonic] = 1.8;
			selectBranch(simple);
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(PARIsoNo)){
			Input0->setLabel(paraList.current()->text());
			Input0->setRange(2.*k/(3.*k-1.), 300.);
			isValueSet[location0][subsonic] = yes;
			isValueSet[location0][supersonic] = yes;
			initialValue[location0][subsonic] = 1.5;
			initialValue[location0][supersonic] = 0.5;
			selectBranch(simple);
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(AstarIsoNo)){
			Input0->setLabel(paraList.current()->text());
			Input0->setRange(2.*k/(3.*k-1.), 300.);
			isValueSet[location0][subsonic] = yes;
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][bothsonic] = yes;
			initialValue[location0][subsonic] = 1.5;
			initialValue[location0][supersonic] = 1.2;
			initialValue[location0][bothsonic] = 1.3;
			selectBranch(all);
			numberOfBox = 1;
		}
		//cout << "isoNozzle to setInputBoxValue" << endl ;  
		setInputBoxValue(numberOfBox);
	}
	else if (modelGroup->selected() == radioOblique){
		Input0->setGeometry( QRect( 50, 37, 150, 45));
		Input0->setEnabled(true);
		Input0->setShown(true);
		Input1->setGeometry( QRect( 300, 37, 150, 45));
		Input1->setEnabled(true);
		Input1->setShown(true);
		radioSuperSonic->setEnabled(true);
		radioSuperSonic->setChecked(true);
		radioSuperSonic->setShown(false);
		if (whatInputGroup->selected() == paraList.at(deltaThetaO) ){//
			setInfoNotice ("Notice only the weak shock result is given");
         Input0->setLabel(deltaCh);
         Input0->setRange(0+EPS, 40. );
         tmpRange = Input0->getRange();
			isValueSet[location0][supersonic] = yes;
			initialValue[location0][supersonic] = 1.5;
         Input0->setValue(0.1*(tmpRange[0]+tmpRange[1]));
         Input1->setLabel(thetaCh);
         Input1->setRange(0.+EPS, 70. );
			isValueSet[location1][supersonic] = yes;
			initialValue[location1][supersonic] = 36.;
			numberOfBox = 2;
		}
		else if (whatInputGroup->selected() == paraList.at(deltaMO) ){//
			setInfoNotice ("Both shock (weak and strong) results are given");
         Input0->setLabel(deltaCh);
         Input0->setRange(0+EPS, 40. );
         Input0->setValue(0.1*(tmpRange[0]+tmpRange[1]));
         Input1->setLabel("M1");
         Input1->setRange(1.+EPS, 50. );
         tmpRange = Input1->getRange();
			isValueSet[location0][subsonic] = no;
			isValueSet[location0][supersonic] = yes;
			isValueSet[location1][subsonic] = no;
			isValueSet[location1][supersonic] = yes;
         initialValue[location0][supersonic] = 1.5;
         initialValue[location1][supersonic] = 6.;
			numberOfBox = 2;
		}
		else if (whatInputGroup->selected() == paraList.at( thetaMO) ){//and M1
			setInfoNotice ("Notice only the weak shock result is given");
         Input0->setLabel(thetaCh);
         Input0->setRange(0+EPS, 70. );
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][subsonic] = yes;
			isValueSet[location1][supersonic] = yes;
         initialValue[location0][supersonic] = 45.;
         initialValue[location1][supersonic] = 4.;
         Input1->setLabel("M1");
         Input1->setRange(1.+EPS, 50. );
			numberOfBox = 2;
		}
		else if (whatInputGroup->selected() == paraList.at( M1maxO) ){//M1 max
			Input0->setLabel(paraList.current()->text());
         Input0->setRange(1.0+EPS, 10. );
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][subsonic] = no;
         initialValue[location0][supersonic] = 1.5;
         Input1->setShown(false);
         Input1->setEnabled(false);
			numberOfBox = 1;
		}
		setInputBoxValue(numberOfBox);
	}
	else if (modelGroup->selected() == radioIsothermal) {
			Input0->setGeometry( QRect( 30, 37, 180, 45 ) );
			Input0->setEnabled(true);
			Input0->setShown(true);
		if (whatInputGroup->selected() == paraList.at(MIso) ){
			Input0->setLabel(paraList.at(MIso)->text());
			Input0->setRange(0.0+EPS, 50.0 );
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][subsonic] = yes;
         initialValue[location0][supersonic] = 1.5;
         initialValue[location0][subsonic] = 0.5;
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(fldIso) ){//fld
			Input0->setLabel(paraList.at(fldIso)->text());
			Input0->setRange((.0+EPS), 500.);
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][subsonic] = yes;
			isValueSet[location0][bothsonic] = yes;
         initialValue[location0][supersonic] = 0.5;
         initialValue[location0][subsonic] = 0.8;
         initialValue[location0][bothsonic] = 0.8;
			selectBranch(all) ;
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(PbarIso) ){// P/P*
			Input0->setLabel(paraList.at(PbarIso)->text());
			Input0->setRange(EPS, 300.);
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][subsonic] = yes;
			isValueSet[location0][bothsonic] = yes;
         initialValue[location0][supersonic] = 0.5;
         initialValue[location0][subsonic] = 0.9;
         initialValue[location0][bothsonic] = 0.8;
			selectBranch(__noBranch);
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(RbarIso)){// R/R*
			Input0->setLabel(paraList.at(RbarIso)->text());
			Input0->setRange(sqrt((k-1.)/(k+1.)), 300.);
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][subsonic] = yes;
			isValueSet[location0][bothsonic] = yes;
         initialValue[location0][supersonic] = 0.5;
         initialValue[location0][subsonic] = 0.9;
         initialValue[location0][bothsonic] = 0.8;
			selectBranch(__noBranch);
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(P0barIso)){// P0/P0*
			Input0->setLabel(paraList.at(P0barIso)->text());
			Input0->setRange(1.+EPS, 300.);
			isValueSet[location0][subsonic] = yes;
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][bothsonic] = yes;
         initialValue[location0][subsonic] = 1.3;
         initialValue[location0][supersonic] = 0.5;
         initialValue[location0][bothsonic] = 0.8;
			selectBranch(__noBranch);
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(T0barIso)){// T0/T0*
			Input0->setLabel(paraList.at(T0barIso)->text());
			Input0->setRange(2.*k/(3.*k-1.), 300.);
			isValueSet[location0][subsonic] = yes;
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][bothsonic] = yes;
         initialValue[location0][subsonic] = 1.9;
         initialValue[location0][supersonic] = 0.5;
         initialValue[location0][bothsonic] = 0.8;
			selectBranch(__noBranch);
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(R0barIso)){// R0/R0*
			Input0->setLabel(paraList.at(R0barIso)->text());
			Input0->setRange(2.*k/(3.*k-1.), 300.);
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][subsonic] = yes;
			isValueSet[location0][bothsonic] = yes;
         initialValue[location0][supersonic] = 0.5;
         initialValue[location0][subsonic] = 0.9;
         initialValue[location0][bothsonic] = 0.8;
			selectBranch(__noBranch);
			numberOfBox = 1;
		}
		setInputBoxValue(numberOfBox);
	}
	else if (modelGroup->selected() == radioRayleigh) {
		Input0->setGeometry( QRect( 30, 37, 180, 45 ) );
		Input0->setEnabled(true);
		Input0->setShown(true);
		if (whatInputGroup->selected() == paraList.at(MR)){//Mach
			Input0->setLabel(paraList.at(MR)->text());
			Input0->setRange(0.0+EPS, 500.0 );
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][subsonic] = yes;
			isValueSet[location0][bothsonic] = yes;
         initialValue[location0][supersonic] = 0.5;
         initialValue[location0][subsonic] = 0.9;
         initialValue[location0][bothsonic] = 0.8;
			selectBranch(__noBranch);
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(PbarR) ){//P/P*
			Input0->setLabel(paraList.at(PbarR)->text());
			Input0->setRange(EPS, k+1.);
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][subsonic] = yes;
			isValueSet[location0][bothsonic] = yes;
         initialValue[location0][supersonic] = 0.5;
         initialValue[location0][subsonic] = 0.9;
         initialValue[location0][bothsonic] = 0.8;
			selectBranch(all) ;
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(TbarR) ){// T/T*
			Input0->setLabel(paraList.at(TbarR)->text());
			Input0->setRange(EPS, 1.);
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][subsonic] = yes;
			isValueSet[location0][bothsonic] = yes;
         initialValue[location0][supersonic] = 0.5;
         initialValue[location0][subsonic] = 0.9;
         initialValue[location0][bothsonic] = 0.8;
			selectBranch(all) ;
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(RbarR) ){// R/R*
			Input0->setLabel(paraList.at(RbarR)->text());
			Input0->setRange(0.0, (k+1.)/k );
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][subsonic] = yes;
			isValueSet[location0][bothsonic] = yes;
         initialValue[location0][supersonic] = 0.5;
         initialValue[location0][subsonic] = 0.9;
         initialValue[location0][bothsonic] = 0.8;
			selectBranch(__noBranch);
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(P0barR) ){// P0/P0*
			Input0->setLabel(paraList.at(P0barR)->text());
			Input0->setRange(1.0, (k+1.)*pow(2./(k+1.),k/(k-1.0)));
			isValueSet[location0][subsonic] = yes;
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][bothsonic] = yes;
         initialValue[location0][subsonic] = 1.1;
         initialValue[location0][supersonic] = 1.2;
         initialValue[location0][bothsonic] = 1.1;
			selectBranch(simple);
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(T0barR) ){// T0/T0*
			Input0->setLabel(paraList.at(T0barR)->text());
			Input0->setRange(EPS, 1.);
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][subsonic] = yes;
			isValueSet[location0][bothsonic] = yes;
         initialValue[location0][supersonic] = 1.5;
         initialValue[location0][subsonic] = 0.9;
         initialValue[location0][bothsonic] = 0.8;
			selectBranch(all);
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(R0barR)){// R0/R0*
			Input0->setLabel(inputRadio6->text());
			Input0->setRange((.0+EPS),  pow((2.0/(k+1)), (1. /(k -1.))) );
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][subsonic] = yes;
			isValueSet[location0][bothsonic] = yes;
         initialValue[location0][supersonic] = 1.5;
         initialValue[location0][subsonic] = 0.9;
         initialValue[location0][bothsonic] = 0.8;
			selectBranch(__noBranch);
			numberOfBox = 1;
		}
		setInputBoxValue(numberOfBox);
	}
		//old stuff from the p open/close valve
		//else if (whatInputGroup->selected() == radioFLD){ // Mx My'
       //     //MyInput->setLabel( "<i>My'</i>" );
		//}
	else if (modelGroup->selected() == radioFanno) {
		setSubInput();
		//if (whatInputGroup->selected() == radioFLD){
			//FLDInput->setGeometry( QRect( 30, 37, 180, 45 ) );
			//FLDInput->setEnabled(true);
			//FLDInput->setShown(true);
			//FLDInput->setRange((.0+EPS), 500.);
			//tmpRange = FLDInput->getRange();
			//FLDInput->setValue(0.5*(tmpRange[0]+tmpRange[1]));
       //     selectBranch(simple) ;
		//}
		//else if (whatInputGroup->selected() == radioP){
            //pInput->setGeometry( QRect( 30, 37, 180, 45 ) );
            //pInput->setEnabled(true);
            //pInput->setShown(true);
			//pInput->setLabel(radioP->text());
            //pInput->setRange(.0, 300.);
            //tmpRange = pInput->getRange();
            //pInput->setValue(0.5*(tmpRange[0]+tmpRange[1]));
		//}
	}
}

void gdForm::calculate()
{
	QString workingFileName;
	if ( filename.isEmpty() ) {
		if (canSave() == false) {
			return;
		}
		else {
			workingFileName = "-g";
			save(workingFileName);
		}
	}
	else if (filename == "-g"  ){ // -g issue
		QString notice,  caption;
		notice = "<h2>Gas Dynamics Calculator 0.4.4</h2>" ;
		//notice += versionNnumber ;
		notice += "<p> -g already set.  ";
		caption = "Gas Dynamics Calculator";
		//QMessageBox::warning (this , caption , notice);//,
		workingFileName = filename;
		save(workingFileName);
	}
	else if (isDataChanged == false ) {
		workingFileName = filename ;
    	QString notice,  caption;
		notice = "<h2>Gas Dynamics Calculator 0.4.4</h2>" ;
		notice += "<p> original file name is:  ";
		notice += workingFileName;
		caption = "Gas Dynamics Calculator";
		//QMessageBox::warning (this , caption , notice);//,
	}
	else {
		if (canSave() == false) {
			return;
		}
		else {
			workingFileName = filename ;
			//workingFileName = "-g";
			save(workingFileName);
			QString notice,  caption;
			notice = "<h2>Gas Dynamics Calculator 0.4.4</h2>" ;
			notice += "<p>file name is:  ";
			notice += workingFileName;
			caption = "Gas Dynamics Calculator";
			QMessageBox::warning (this , caption , notice);//,
		}
	}
	if (!showResults){
		//showResults = new ShowResults (this, "showResutls", "./data.ini");
		showResults = new ShowResults (this, "showResutls");
		//, "workingFileName");
		connect(this, SIGNAL(newData(QString)), 
			showResults, SLOT(newData(QString)));
	}
	showResults->show();
	showResults->raise();
	showResults->setActiveWindow();
	filename = workingFileName ;
	emit newData(workingFileName); 
}

int gdForm::LabelChanged()
{ //also changing range of input operation
	double k;
    if (modelGroup->selected() == radioIsentropic ){// 
			if ( Input3->value() > 1.0){ //supersonic flow M > 1
				k =  kInput->value();
			}
			else {
			}
            //tmpRange = MyInput->getRange();
            //MyInput->setValue(0.5*(tmpRange[0]+tmpRange[1]));
    }
	isDataChanged=true;
	return true ;
}


void gdForm::fileSave()
{
    QString notice,  caption;
	if (modelGroup->selected() == 0 ){
		notice = "<h2>Gas Dynamics Calculator 0.4.4</h2>" ;
		notice += "<p>You need to select the model radio";
		QMessageBox::warning (this , "Gas Dynamics Calculator", notice);//,
//					 QMessageBox::Ok );
		return;
	}
    if (whatInputGroup->selected() == 0 ){
        notice = "<h2>Gas Dynamics Calculator 0.4.4</h2>" ;
        notice += "<p>You need to select the what Info radio";
        QMessageBox::warning (this , "Gas Dynamics Calculator", notice);//,
//                   QMessageBox::Ok );
        return;
    }

    if ( filename.isEmpty() ) {
        fileSaveAs();
        return;
    }
	save(filename);
}


void gdForm::fileSaveAs()
{
  QString fn =
	 QFileDialog::getSaveFileName( "gdc.ini",
		 "gdc files (*.gdc *.ini *.dat );;" 
         "Text files (*.txt);;"
         "Any files (*)" ,
				this, "save file dialog" "Choose a file name to save under" ); 
		// to set up the extension for the file name 
    if ( !fn.isEmpty() ) {
        filename = fn;
        fileSave();
    }
}


void gdForm::fileOpen()
{

}

void gdForm::save( QString FileName )
//save fun
{
	double tmpValue = 0.;
	string fileName;
	if (FileName == "-g") {
		fileName = ".dammy.dammy";	
	}
	else {
		fileName = FileName.ascii();
	}
	CIniFile iniFile( fileName ); 
	iniFile.HeaderComment("This file was created by gdc calculator.");
	iniFile.HeaderComment
		("This file can be edited if you know what you doing." );

	//iniFile.HeaderComment("data","The data input");
	iniFile.SetValueF( "data", "k", kInput->value());
	iniFile.KeyComment("data",  "The actual data");
	iniFile.SetValueF( "data", "k", kInput->value());
	if ( modelGroup->selected() == radioShock){
		if (whatInputGroup->selected() == paraList.at(MxS))
			iniFile.SetValueF( "data", shockLayoutName[MxS], Input0->value());
		else if (whatInputGroup->selected() ==  paraList.at(MyS)) 
			iniFile.SetValueF( "data", shockLayoutName[MyS], Input0->value());
		else if (whatInputGroup->selected() == paraList.at(PyPxS)) 
			iniFile.SetValueF( "data", shockLayoutName[PyPxS], Input0->value());
		else if (whatInputGroup->selected() == paraList.at(TyTxS) ) 
			iniFile.SetValueF( "data", shockLayoutName[TyTxS], Input0->value());
		else if (whatInputGroup->selected() == paraList.at(RyRxS) ) 
			iniFile.SetValueF( "data", shockLayoutName[RyRxS], Input0->value());
		else if (whatInputGroup->selected() == paraList.at(P0yP0xS) ) 
			iniFile.SetValueF( "data",shockLayoutName[P0yP0xS],Input0->value());
		else if (whatInputGroup->selected() == paraList.at(R0yR0xS) ) 
			iniFile.SetValueF( "data",shockLayoutName[R0yR0xS],Input0->value());
		else { //default
			iniFile.SetValueF( "data", shockLayoutName[0], Input0->value());
		}
	}
	else if ( modelGroup->selected() == radioOblique){
		if (whatInputGroup->selected() == paraList.at(deltaThetaO) ){//
			iniFile.SetValueF( "data", "delta", Input0->value());
			iniFile.SetValueF( "data", "theta", Input1->value());
		}
		else if (whatInputGroup->selected() == paraList.at(deltaMO) ){
			iniFile.SetValueF( "data", "delta", Input0->value());
			iniFile.SetValueF( "data", "Mx", Input1->value());
		}
		else if (whatInputGroup->selected() == paraList.at(thetaMO) ) { 
			iniFile.SetValueF( "data", "theta", Input0->value());
			iniFile.SetValueF( "data", "Mx", Input1->value());
		} 
		else if (whatInputGroup->selected() == paraList.at(M1maxO) ) { 
			iniFile.SetValueF( "data", "Mx", Input0->value());
		} 
		else {
			iniFile.SetValueF( "data", "delta", Input0->value());
			iniFile.SetValueF( "data", "theta", Input1->value());
		}
	}
	else if ( modelGroup->selected() == radioShockDynamics){
		double realTemp, tmpValue = 0.;
		if (subModelGroup->selected() == suddenOpenRadio){
			if (whatInputGroup->selected() ==  paraList.at(MxO) ){
				iniFile.SetValueF("data", "Mx", Input0->value());
			}
			else if (whatInputGroup->selected() ==  paraList.at(MyO) ){
				iniFile.SetValueF("data", "My", Input0->value());
			}
			else if (whatInputGroup->selected() ==  paraList.at(MypO) ){
				iniFile.SetValueF("data", "Myp", Input0->value());
			}
			else if (whatInputGroup->selected() ==  paraList.at(UsTxO) ){
				tmpValue = conversionToReal 
						(Units0->currentItem () , Velocityp, Input0->value() );
				//cout << "this real Value = " << tmpValue  << endl;
				//cout << "this input->Value = " << Input0->value()  << endl;
				iniFile.SetValueF("data", "Us", tmpValue );
				realTemp = temperatureToRealTemp 
					( Units1->currentItem () , Input1->value() );
				iniFile.SetValueF("data", "Tx", realTemp);
			}
			else if (whatInputGroup->selected() ==  paraList.at(PistonO) ){
				tmpValue = conversionToReal 
						(Units0->currentItem () , Velocityp, Input0->value() );
				iniFile.SetValueF("data", "Uyp", tmpValue);
				realTemp = temperatureToRealTemp 
					( Units1->currentItem () , Input1->value() );
				iniFile.SetValueF("data", "Tx", realTemp);
			}
			else if (whatInputGroup->selected() ==  paraList.at(PyPxO) ){
				iniFile.SetValueF("data", "PyPx", Input0->value());
			}
		} 
		else if (subModelGroup->selected() == suddenCloseRadio ) {
			if (whatInputGroup->selected() ==  paraList.at(MxC) ){
				iniFile.SetValueF("data", "Mx", Input0->value());
			}
			else if (whatInputGroup->selected() ==  paraList.at(MyC) ){
				iniFile.SetValueF("data", "My", Input0->value());
			}
			else if (whatInputGroup->selected() ==  paraList.at(MxpC) ){
				iniFile.SetValueF("data", "Mxp", Input0->value());
			}
			else if (whatInputGroup->selected() ==  paraList.at(UxpC) ){
				iniFile.SetValueF("data", "Uxp", Input0->value());
				double realTemp;
				realTemp = temperatureToRealTemp 
					( Units1->currentItem () , Input1->value() );
				iniFile.SetValueF("data", "Tx", realTemp);
			}
			else if (whatInputGroup->selected() ==  paraList.at(PyPxO) ){
				iniFile.SetValueF("data", "PyPx", Input0->value());
			}
		} 
		else if (subModelGroup->selected() == partiallyCloseRadio ) {
			if (whatInputGroup->selected() ==  paraList.at(MxMypC) ){
				iniFile.SetValueF("data", "Mx", Input0->value());
				iniFile.SetValueF("data", "Myp", Input1->value());
			}
			else if (whatInputGroup->selected() ==  paraList.at(MxpMypC) ){
				iniFile.SetValueF("data", "Mxp", Input0->value());
				iniFile.SetValueF("data", "Myp", Input1->value());
			}
		} 
		else if (subModelGroup->selected() == partiallyOpenRadio ) {
			if (whatInputGroup->selected() ==  paraList.at(MxpMyO) ){
				iniFile.SetValueF("data", "Mxp", Input0->value());
				iniFile.SetValueF("data", "My", Input1->value());
			}
			else if (whatInputGroup->selected() ==  paraList.at(MxpMypO) ){
				iniFile.SetValueF("data", "Mxp", Input0->value());
				iniFile.SetValueF("data", "Myp", Input1->value());
			}
		} 
	}
	else if (modelGroup->selected()== radioIsentropic ){
			//notImplementedYet("save isentropic");
		if (whatInputGroup->selected() ==  paraList.at(MI) ){
			iniFile.SetValueF("data", "M" , Input0->value());
		}
		else if (whatInputGroup->selected() == paraList.at(PbarI) ){//Pbar
			iniFile.SetValueF("data", isentropicLN[PbarI], Input0->value());
			saveSelectedBranch(iniFile);
		}
		else if (whatInputGroup->selected() == paraList.at(TbarI) ){//Tbar
			iniFile.SetValueF("data", isentropicLN[TbarI], Input0->value());
			saveSelectedBranch(iniFile);
		}
		else if (whatInputGroup->selected() == paraList.at(RbarI) ){//Rbar
			iniFile.SetValueF("data", isentropicLN[RbarI], Input0->value());
			saveSelectedBranch(iniFile);
		}
		else if (whatInputGroup->selected() ==  paraList.at(AstarI)  ){//Astar
			iniFile.SetValueF("data", "Astar", Input0->value());
			saveSelectedBranch(iniFile);
		}
		else if (whatInputGroup->selected() == paraList.at(PARI) ){//PAR
			iniFile.SetValueF("data", isentropicLN[PARI], Input0->value());
			saveSelectedBranch(iniFile);
		}
		else if (whatInputGroup->selected() == paraList.at(P0TI) ){//P0T
			tmpValue = conversionToReal (Units0->currentItem () ,
				 U0,  Input0->value() );
			cout << tmpValue << endl  ;
			cout << Input0->value()  << endl  ;
			iniFile.SetValueF( "data", "P0", tmpValue );
			tmpValue = temperatureToRealTemp ( Units1->currentItem (),
						 Input1->value() );
			iniFile.SetValueF("data", "T", tmpValue);
			tmpValue = conversionToReal (Units2->currentItem () ,
				 U2,  Input2->value() );
			iniFile.SetValueF( "data", "mDot", tmpValue);
		}
		else if (whatInputGroup->selected() ==  paraList.at(PT0I) ){//PT0
			tmpValue = conversionToReal (Units0->currentItem () ,
				 U0,  Input0->value() );
			iniFile.SetValueF( "data", "P", tmpValue );
			tmpValue = temperatureToRealTemp ( Units1->currentItem (),
						 Input1->value() );
			iniFile.SetValueF( "data", "T0", tmpValue);
			tmpValue = conversionToReal (Units2->currentItem () ,
				 U2,  Input2->value() );
			iniFile.SetValueF( "data", "mDot", tmpValue);
		}
		else {
			//do nothing
		}
	}
    else if (modelGroup->selected()== radioPM ){
			//notImplementedYet("save isentropic");
		if (whatInputGroup->selected() ==  paraList.at(MIs)){
			iniFile.SetValueF("data", "M", Input0->value());
		}
		else if (whatInputGroup->selected() ==  paraList.at(nuIs) ){// nu
			iniFile.SetValueF("data", PMLN[nuIs], Input0->value());
		}
		else if (whatInputGroup->selected() == paraList.at(PbarIs) ){//Pbar
			iniFile.SetValueF("data", PMLN[PbarIs], Input0->value());
		}
		else if (whatInputGroup->selected() == paraList.at(TbarIs) ){//Tbar
			iniFile.SetValueF("data", PMLN[TbarIs], Input0->value());
			saveSelectedBranch(iniFile);
		}
		else if (whatInputGroup->selected() == paraList.at(RbarIs) ){//Rbar
			iniFile.SetValueF("data", PMLN[RbarIs], Input0->value());
			saveSelectedBranch(iniFile);
		}
		else if (whatInputGroup->selected() == paraList.at(AstarIs) ){//Astar
			iniFile.SetValueF("data", PMLN[AstarIs], Input0->value());
			saveSelectedBranch(iniFile);
		}
		else if (whatInputGroup->selected() == paraList.at(PARIs) ){//PAR
			iniFile.SetValueF("data", PMLN[PARIs], Input0->value());
			saveSelectedBranch(iniFile);
		}
		else {
			//do nothing
		}
	}
    else if (modelGroup->selected()== radioFanno ){
		if (subModelGroup->selected() == subParaList.at(chokedF) ) {
			if (whatInputGroup->selected() == paraList.at(MF) ){
				iniFile.SetValueF("data", "M", Input0->value());
			}
			else if (whatInputGroup->selected() == paraList.at(fldF) ){
				iniFile.SetValueF("data", "fld", Input0->value());
				saveSelectedBranch(iniFile);
			}
			else if (whatInputGroup->selected() == paraList.at(PbarF) ){
				iniFile.SetValueF("data", "Pbar", Input0->value());
				saveSelectedBranch(iniFile);
			}
			else if (whatInputGroup->selected() == paraList.at(TbarF) ){
				iniFile.SetValueF("data", "Tbar", Input0->value());
				saveSelectedBranch(iniFile);
			}
			else if (whatInputGroup->selected() == paraList.at(RbarF) ){
				iniFile.SetValueF("data", "Rbar", Input0->value());
				saveSelectedBranch(iniFile);
			}
			else if (whatInputGroup->selected() == paraList.at(P0barF) ){
				iniFile.SetValueF("data", "PRzeroStar", Input0->value());
				saveSelectedBranch(iniFile);
			}
			else {
				//do nothing
			}
		}
		else if (subModelGroup->selected() == subParaList.at(unchokedF) ) {
			if (whatInputGroup->selected() == paraList.at(M1fld) ){
				iniFile.SetValueF("data", "M1", Input0->value());
				iniFile.SetValueF("data", "fld", Input1->value());
				saveSelectedBranch(iniFile);
			}
			else if (whatInputGroup->selected() == paraList.at(M2fld) ){
				iniFile.SetValueF("data", "M2", Input0->value());
				iniFile.SetValueF("data", "fld", Input1->value());
				saveSelectedBranch(iniFile);
			}
			else if (whatInputGroup->selected() == paraList.at(fldP2P1) ){
				iniFile.SetValueF("data", "fld", Input0->value());
				iniFile.SetValueF("data", "P2P1", Input1->value());
				saveSelectedBranch(iniFile);
			}
			else if (whatInputGroup->selected() == paraList.at(M1P2P1) ){
				iniFile.SetValueF("data", "M1", Input0->value());
				iniFile.SetValueF("data", "P2P1", Input1->value());
				saveSelectedBranch(iniFile);
			}
			else if (whatInputGroup->selected() == paraList.at(M2P2P1) ){
				iniFile.SetValueF("data", "M2", Input0->value());
				iniFile.SetValueF("data", "P2P1", Input1->value());
				saveSelectedBranch(iniFile);
			}
			else {
				//do nothing
			}
		}
	}
	else if (modelGroup->selected()== radioRayleigh ){
		if (whatInputGroup->selected() == paraList.at(MR) ){// Mach
			iniFile.SetValueF("data", "M", Input0->value());
      }
		else if (whatInputGroup->selected() == paraList.at(PbarR) ){ //P/P*
			iniFile.SetValueF("data", "Pbar", Input0->value());
			saveSelectedBranch(iniFile);
    	}
		else if (whatInputGroup->selected() == paraList.at(TbarR) ){ // T/T*
			iniFile.SetValueF("data", "Tbar", Input0->value());
			saveSelectedBranch(iniFile);
    	}
		else if (whatInputGroup->selected() == paraList.at(RbarR) ){ // R/R*
			iniFile.SetValueF("data", "Rbar", Input0->value());
    	}
		else if (whatInputGroup->selected() == paraList.at(P0barR) ){ // P0/P0*
        	iniFile.SetValueF( "data", "PRzeroStar", Input0->value());
			saveSelectedBranch(iniFile);
    	}
		else if (whatInputGroup->selected() == paraList.at(T0barR) ){ // T0/T0*
        	iniFile.SetValueF( "data", "TRzeroStar", Input0->value());
			saveSelectedBranch(iniFile);
    	}
		else if (whatInputGroup->selected() == paraList.at(R0barR) ){ // R0/R0*
        	iniFile.SetValueF( "data", "RRzeroStar", Input0->value());
			saveSelectedBranch(iniFile);
    	}
		else {
			//do nothing
		}
	}
	else if (modelGroup->selected()== radioIsoNozzle ){
		if (whatInputGroup->selected() == paraList.at(MIsoNo)){
			iniFile.SetValueF( "data", "M", Input0->value());
		}
		else if (whatInputGroup->selected() == paraList.at(PbarIsoNo)){// P/P*
        	iniFile.SetValueF( "data", "Pbar", Input0->value());
			saveSelectedBranch(iniFile);
    	}
		else if (whatInputGroup->selected() == paraList.at(RbarIsoNo) ){// R/R*
        	iniFile.SetValueF( "data", "Rbar", Input0->value());
			saveSelectedBranch(iniFile);
    	}
		else if (whatInputGroup->selected() == paraList.at(P0barIsoNo) ){//P0
        	iniFile.SetValueF( "data", "PRzeroStar", Input0->value());
			saveSelectedBranch(iniFile);
    	}
		else if (whatInputGroup->selected() == paraList.at(T0barIsoNo) ){// T0
        	iniFile.SetValueF( "data", "TRzeroStar", Input0->value());
			saveSelectedBranch(iniFile);
    	}
		else if (whatInputGroup->selected() == paraList.at(R0barIsoNo) ){//R0
        	iniFile.SetValueF( "data", "RRzeroStar", Input0->value());
			saveSelectedBranch(iniFile);
    	}
		else if (whatInputGroup->selected() == paraList.at(PARIsoNo) ){
        	iniFile.SetValueF( "data", "PAR", Input0->value());
			saveSelectedBranch(iniFile);
    	}
		else if (whatInputGroup->selected() == paraList.at(AstarIsoNo) ){
        	iniFile.SetValueF( "data", "Astar", Input0->value());
			saveSelectedBranch(iniFile);
    	}
		else {
			//do nothing
		}
	}
	else if (modelGroup->selected()== radioIsothermal ){
		if (whatInputGroup->selected() == paraList.at(MIso) ){
            iniFile.SetValueF( "data", "M", Input0->value());
		}
		else if (whatInputGroup->selected() == paraList.at(fldIso) ){
        	iniFile.SetValueF( "data", "fld", Input0->value());
			saveSelectedBranch(iniFile);
    	}
		else if (whatInputGroup->selected() == paraList.at(PbarIso) ){ // P/P*
        	iniFile.SetValueF( "data", "Pbar", Input0->value());
			saveSelectedBranch(iniFile);
    	}
		else if (whatInputGroup->selected() == paraList.at(RbarIso) ){ // R/R*
        	iniFile.SetValueF( "data", "Rbar", Input0->value());
			saveSelectedBranch(iniFile);
    	}
		else if (whatInputGroup->selected() == paraList.at(P0barIso) ){// P0/P0*
        	iniFile.SetValueF( "data", "PRzeroStar", Input0->value());
			saveSelectedBranch(iniFile);
    	}
		else if (whatInputGroup->selected() == paraList.at(T0barIso) ){// T0/T0*
        	iniFile.SetValueF( "data", "TRzeroStar", Input0->value());
			saveSelectedBranch(iniFile);
    	}
		else if (whatInputGroup->selected() == paraList.at(R0barIso) ){//R0/R0*
        	iniFile.SetValueF( "data", "RRzeroStar", Input0->value());
			saveSelectedBranch(iniFile);
    	}
		else {
			//do nothing
		}
	}
//		if (modelGroup->selected()== radioIsentropic  &&
 //       	whatInputGroup->selected() == radioP0){
//			double k = kInput->value() ;
 //       	if ( Input3->value() >= 1.0 && 
//				//valueChange(kInput->value()); //update according M value
//	            QString notice, notice1,  caption;
//				caption = "Gas Dynamics Calculator";
//				notice = "<h2>Gas Dynamics Calculator 0.4.4</h2>" ;
//				notice += "<p>Your value of Pbar is too high ";
//				notice += "and will not work. ";
//				notice += "<p>Therefore the value changed to: <p><b><center>" ;
//				notice += notice1.setNum(  p0Input->value());
//				notice += 
//					"</center> </b><p>The highest value possible for this k = ";
//				notice += notice1.setNum( k );
//				notice += " is: <p> " ;
//				notice += notice1.setNum(  tmpRange[1]);
//				QMessageBox::warning (this , caption, notice);
//			}
//		}
	if (isTexBox->isChecked() == true ) {
		iniFile.SetValue( "integer", "isTex", "yes" );
		iniFile.SetValue( "integer", "isHTML", "no" );
	}
	else {
		iniFile.SetValue( "integer", "isTex", "no" );
		iniFile.SetValue( "integer", "isHTML", "yes" );
	}                           
	
	if (isRangeBox->isChecked() == true ) {
		iniFile.SetValue( "integer", "isRange", "yes" );
	}
	else {
		iniFile.SetValue( "integer", "isRange", "no" );
	}
	iniFile.SetValueI( "integer", "numberNodes", 40 );
	iniFile.SetValueI( "integer", "shockLocation", 50 );
	// to insert the additional option later
	if (modelGroup->selected() == radioShockDynamics){ 
		if   (outputGroup->selected() == radioStandard)
			iniFile.SetValue( "InputOutput", "whatInfo", "infoStandard" );
		else if   (outputGroup->selected() == radioMaximum)
			iniFile.SetValue( "InputOutput", "whatInfo", "infoMax" );
		else if   (outputGroup->selected() == radioMinimum)
			iniFile.SetValue( "InputOutput", "whatInfo", "infoIteration" );
		else 
			iniFile.SetValue( "InputOutput", "whatInfo", "infoStandard" );
	}
	else if (modelGroup->selected() == radioFanno){ 
		if   (outputGroup->selected() == radioStandard)
			iniFile.SetValue( "InputOutput", "whatInfo", "infoStandard" );
		else if   (outputGroup->selected() == radioMaximum)
			iniFile.SetValue( "InputOutput", "whatInfo", "infoIteration" );
		else if   (outputGroup->selected() == radioMinimum)
			iniFile.SetValue( "InputOutput", "whatInfo", "infoMinimal" );
		else 
			iniFile.SetValue( "InputOutput", "whatInfo", "infoStandard" );
	}
	else {
		if   (outputGroup->selected() == radioMaximum)
			iniFile.SetValue( "InputOutput", "whatInfo", "infoMax" );
		else if   (outputGroup->selected() == radioMinimum)
			iniFile.SetValue( "InputOutput", "whatInfo", "infoMinimal" );
		else 
			iniFile.SetValue( "InputOutput", "whatInfo", "infoStandard" );
	}
	// set the variable name
	if (modelGroup->selected() == radioOblique){
		if (whatInputGroup->selected() == paraList.at(deltaThetaO) ){
	 		iniFile.SetValue( "InputOutput", "variableName", "deltaThetaV" );
		}
		else if (whatInputGroup->selected() == paraList.at(deltaMO) ){
	 		iniFile.SetValue( "InputOutput", "variableName", "deltaMV" );
		}
		else if (whatInputGroup->selected() == paraList.at(thetaMO) ){
	 		iniFile.SetValue( "InputOutput", "variableName", "thetaMV" );
		}
		else if (whatInputGroup->selected() == paraList.at(M1maxO) ){
	 		iniFile.SetValue( "InputOutput", "variableName", "MxV" );
		}
	}
	else if (modelGroup->selected() == radioShockDynamics){
		if (subModelGroup->selected() == suddenOpenRadio){
			if (whatInputGroup->selected() ==  paraList.at(MxO) ){
				iniFile.SetValue( "InputOutput", "variableName", "MxOpenV" );
			}
			else if (whatInputGroup->selected() ==  paraList.at(MyO) ){
				iniFile.SetValue( "InputOutput", "variableName", "MyOpenV" );
			}
			else if (whatInputGroup->selected() ==  paraList.at(MypO) ){
				iniFile.SetValue( "InputOutput", "variableName", "MypOpenV" );
			}
			else if (whatInputGroup->selected() ==  paraList.at(UsTxO) ){
				iniFile.SetValue( "InputOutput", "variableName", "UsOpenV" );
			}
			else if  (whatInputGroup->selected() == paraList.at(PistonO) ) {
	 			iniFile.SetValue( "InputOutput", "variableName", "UypTxOV" );
			}
			else if (whatInputGroup->selected() ==  paraList.at(PyPxO) ){
				iniFile.SetValue( "InputOutput", "variableName", "PyPxOpenV" );
			}
		} 
		else if (subModelGroup->selected() == suddenCloseRadio){
			if (whatInputGroup->selected() ==  paraList.at(MxC) ){
				iniFile.SetValue( "InputOutput", "variableName", "MxV" );
			}
			else if (whatInputGroup->selected() ==  paraList.at(MyC) ){
				iniFile.SetValue( "InputOutput", "variableName", "MyV" );
			}
			else if (whatInputGroup->selected() ==  paraList.at(MxpC) ){
				iniFile.SetValue( "InputOutput", "variableName", "MxpV" );
			}
			else if (whatInputGroup->selected() ==  paraList.at(UxpC) ){
				iniFile.SetValue( "InputOutput", "variableName", "UxpV" );
			}
			else if (whatInputGroup->selected() ==  paraList.at(PyPxO) ){
				iniFile.SetValue( "InputOutput", "variableName", "PyPxOpenV" );
			}
		} 
		else if (subModelGroup->selected() == partiallyCloseRadio){
			if (whatInputGroup->selected() ==  paraList.at(MxMypC) ){
				iniFile.SetValue( "InputOutput", "variableName", "MxMypPCV" );
			}
			else if (whatInputGroup->selected() ==  paraList.at(MxpMypC) ){
				iniFile.SetValue( "InputOutput", "variableName", "MxpMypPCV" );
			}
		} 
		else if (subModelGroup->selected() == partiallyOpenRadio){
			if (whatInputGroup->selected() ==  paraList.at(MxpMyO) ){
				iniFile.SetValue( "InputOutput", "variableName", "MxpMyPOV" );
			}
			else if (whatInputGroup->selected() ==  paraList.at(MxpMypO) ){
				iniFile.SetValue( "InputOutput", "variableName", "MxpMypPOV" );
			}
		} 
		//else if  (whatInputGroup->selected() == radioPAR) {
	 	//	iniFile.SetValue( "InputOutput", "variableName", "kV" );
		//}
		else { //default
		}
	}
	else if (modelGroup->selected() == radioIsentropic){
		if (whatInputGroup->selected() == paraList.at(MI))
			iniFile.SetValue( "InputOutput", "variableName", "machV" );
		else if (whatInputGroup->selected() == paraList.at(PbarI))
			iniFile.SetValue( "InputOutput", "variableName", "PbarV" );
		else if (whatInputGroup->selected() == paraList.at(TbarI) )
			iniFile.SetValue( "InputOutput", "variableName", "TbarV" );
		else if (whatInputGroup->selected() == paraList.at(RbarI) )
			iniFile.SetValue( "InputOutput", "variableName", "RbarV" );
		else if (whatInputGroup->selected() == paraList.at(AstarI))
			iniFile.SetValue( "InputOutput", "variableName", "AstarV" );
		else if (whatInputGroup->selected() == paraList.at(PARI) )
			iniFile.SetValue( "InputOutput", "variableName", "PARV" );
		else if (whatInputGroup->selected() == paraList.at( P0TI) )
			iniFile.SetValue( "InputOutput", "variableName", "mDotP0TV" );
		else if (whatInputGroup->selected() ==  paraList.at( PT0I) )
			iniFile.SetValue( "InputOutput", "variableName", "mDotPT0V" );
		else // default
			iniFile.SetValue("InputOutput", "variableName","machV");
	}
	else if (modelGroup->selected() == radioPM){
		if (whatInputGroup->selected() == paraList.at(MIs) )
			iniFile.SetValue( "InputOutput", "variableName", "machV" );
		else if (whatInputGroup->selected() == paraList.at(nuIs) )
			iniFile.SetValue( "InputOutput", "variableName", "nuV" );
		else if (whatInputGroup->selected() == paraList.at(PbarIs))
			iniFile.SetValue( "InputOutput", "variableName", "PbarV" );
		else if (whatInputGroup->selected() == paraList.at(TbarIs) )
			iniFile.SetValue( "InputOutput", "variableName", "TbarV" );
		else if (whatInputGroup->selected() == paraList.at(RbarIs) )
			iniFile.SetValue( "InputOutput", "variableName", "RbarV" );
		else if (whatInputGroup->selected() == paraList.at(PARIs) )
            iniFile.SetValue( "InputOutput", "variableName", "PARV" );
		else if (whatInputGroup->selected() == paraList.at(AstarIs) )
            iniFile.SetValue( "InputOutput", "variableName", "AstarV" );
		else // default
			iniFile.SetValue("InputOutput", "variableName","machV");
	}
	else if (modelGroup->selected() == radioFanno){
		if (subModelGroup->selected() == subParaList.at(chokedF) ) { 
			if (whatInputGroup->selected() == paraList.at(MF))
				iniFile.SetValue( "InputOutput", "variableName", "machV" );
			else if (whatInputGroup->selected() == paraList.at(fldF))
				iniFile.SetValue( "InputOutput", "variableName", "fldV" );
			else if (whatInputGroup->selected() == paraList.at(PbarF) )
				iniFile.SetValue( "InputOutput", "variableName", "PbarV" );
			else if (whatInputGroup->selected() == paraList.at(TbarF) )
				iniFile.SetValue( "InputOutput", "variableName", "TbarV" );
			else if (whatInputGroup->selected() == paraList.at(RbarF) )
				iniFile.SetValue( "InputOutput", "variableName", "RbarV" );
			else if (whatInputGroup->selected() == paraList.at(P0barF))
				iniFile.SetValue( "InputOutput", "variableName", "PRzeroStarV" );
			else if (whatInputGroup->selected() == paraList.at(AstarI))
				iniFile.SetValue( "InputOutput", "variableName", "AstarV" );
			else // default
				iniFile.SetValue("InputOutput", "variableName","machV");
		}
		else if (subModelGroup->selected() == subParaList.at(unchokedF) ) { 
			if (whatInputGroup->selected() == paraList.at(M1fld))
				iniFile.SetValue( "InputOutput", "variableName", "M1fldV" );
			else if (whatInputGroup->selected() == paraList.at(M2fld))
				iniFile.SetValue( "InputOutput", "variableName", "M2fldV" );
			else if (whatInputGroup->selected() == paraList.at(fldP2P1))
				iniFile.SetValue( "InputOutput", "variableName", "fldP2P1V" );
			else if (whatInputGroup->selected() == paraList.at(M1P2P1))
				iniFile.SetValue( "InputOutput", "variableName", "M1P2P1V" );
			else if (whatInputGroup->selected() == paraList.at(M2P2P1))
				iniFile.SetValue( "InputOutput", "variableName", "M2P2P1V" );
		}
	}
	else if (modelGroup->selected() == radioIsothermal){
		if (whatInputGroup->selected() == paraList.at(MIso) ) { //Mach
			iniFile.SetValue( "InputOutput", "variableName", "machV" );
		}
		else if (whatInputGroup->selected() == paraList.at(fldIso)){ //fld
			iniFile.SetValue( "InputOutput", "variableName", "fldV" );
		}
		else if (whatInputGroup->selected() == paraList.at(PbarIso) ){
			iniFile.SetValue( "InputOutput", "variableName", "PbarV" );
		}
		else if (whatInputGroup->selected() == paraList.at(RbarIso) ){
			iniFile.SetValue( "InputOutput", "variableName", "RbarV");
		}
		else if (whatInputGroup->selected() == paraList.at(P0barIso)){
			iniFile.SetValue( "InputOutput", "variableName", "PRzeroStarV" );
		}
		else if (whatInputGroup->selected() == paraList.at(T0barIso) ){
			iniFile.SetValue( "InputOutput", "variableName", "TRzeroStarV" );
		}
		else if (whatInputGroup->selected() == paraList.at(R0barIso) ){
			iniFile.SetValue( "InputOutput", "variableName", "RRzeroStarV" );
		}
		else { 
            iniFile.SetValue( "InputOutput", "variableName", "machV" );
		}
	}
	else if (modelGroup->selected() == radioIsoNozzle){
		if (whatInputGroup->selected() == paraList.at(MIsoNo)){ //Mach
			iniFile.SetValue( "InputOutput", "variableName", "machV" );
		}
		else if (whatInputGroup->selected() == paraList.at(PbarIsoNo) ){
			iniFile.SetValue( "InputOutput", "variableName", "PbarV" );
		}
		else if (whatInputGroup->selected() == paraList.at(RbarIsoNo)){
			iniFile.SetValue( "InputOutput", "variableName", "RbarV" );
		}
		else if (whatInputGroup->selected() == paraList.at(P0barIsoNo) ){
			iniFile.SetValue( "InputOutput", "variableName", "PRzeroStarV" );
		}
		else if (whatInputGroup->selected() == paraList.at(T0barIsoNo) ){
			iniFile.SetValue( "InputOutput", "variableName", "TRzeroStarV" );
		}
		else if (whatInputGroup->selected() == paraList.at(R0barIsoNo) ){
			iniFile.SetValue( "InputOutput", "variableName", "RRzeroStarV" );
		}
		else if (whatInputGroup->selected() == paraList.at(AstarIsoNo) ){
			iniFile.SetValue( "InputOutput", "variableName", "AstarV" );
		}
		else if (whatInputGroup->selected() == paraList.at(PARIsoNo) ){
			iniFile.SetValue( "InputOutput", "variableName", "PARV" );
		}
		else {
			iniFile.SetValue( "InputOutput", "variableName", "machV" );
		}
	}
	else if (modelGroup->selected() == radioRayleigh){
		if (whatInputGroup->selected() == paraList.at(MR) ){ //Mach
			iniFile.SetValue( "InputOutput", "variableName", "machV" );
		}
		else if (whatInputGroup->selected() == paraList.at(PbarR) ){ 
			iniFile.SetValue( "InputOutput", "variableName", "PbarV" );
		}
		else if (whatInputGroup->selected() == paraList.at(TbarR) ){
			iniFile.SetValue( "InputOutput", "variableName", "TbarV" );
		}
		else if (whatInputGroup->selected() == paraList.at(RbarR) ){
			iniFile.SetValue( "InputOutput", "variableName", "RbarV" );
		}
		else if (whatInputGroup->selected() == paraList.at(P0barR) ){
			iniFile.SetValue( "InputOutput", "variableName", "PRzeroStarV" );
		}
		else if (whatInputGroup->selected() == paraList.at(T0barR) ){
			iniFile.SetValue( "InputOutput", "variableName", "TRzeroStarV" );
		}
		else if (whatInputGroup->selected() ==  paraList.at(R0barR) ){
			iniFile.SetValue( "InputOutput", "variableName", "RRzeroStarV" );
		}
		else {
			iniFile.SetValue( "InputOutput", "variableName", "machV" );
		}
	}
	else if (modelGroup->selected() == radioShock){
		if (whatInputGroup->selected() == paraList.at(MxS) )
			iniFile.SetValue( "InputOutput", "variableName", "MxV" );
		else if (whatInputGroup->selected() == paraList.at(MyS))
			iniFile.SetValue( "InputOutput", "variableName", "MyV" );
		else if (whatInputGroup->selected() == paraList.at(PyPxS) )
			iniFile.SetValue( "InputOutput", "variableName", "PyPxV" );
		else if (whatInputGroup->selected() == paraList.at(TyTxS) )
			iniFile.SetValue( "InputOutput", "variableName", "TyTxV");
		else if (whatInputGroup->selected() == paraList.at(RyRxS) )
			iniFile.SetValue( "InputOutput", "variableName", "RyRxV" );
		else if (whatInputGroup->selected() == paraList.at(P0yP0xS) )
			iniFile.SetValue( "InputOutput", "variableName", "P0yP0xV" );
		else if (whatInputGroup->selected() == paraList.at(R0yR0xS) )
			iniFile.SetValue( "InputOutput", "variableName", "R0yR0xV" );
		else // default
			iniFile.SetValue( "InputOutput", "variableName", "MxV" );
	}

// set flow model 
	if (modelGroup->selected() == radioShock)
	 	iniFile.SetValue( "InputOutput", "className", "shockFlow" );
	else if (modelGroup->selected() == radioPM)
        iniFile.SetValue( "InputOutput", "className", "pmFlow" );
	else if (modelGroup->selected() == radioIsentropic)
        iniFile.SetValue( "InputOutput", "className", "isentropicFlow" );
	else if (modelGroup->selected() == radioIsoNozzle)
        iniFile.SetValue("InputOutput", "className","isothermalNozzleFlow" );
	else if (modelGroup->selected() == radioFanno)
        iniFile.SetValue( "InputOutput", "className", "fannoFlow" );
	else if (modelGroup->selected() == radioIsothermal)
        iniFile.SetValue( "InputOutput", "className", "isothermalFlow" );
	else if (modelGroup->selected() == radioRayleigh)
        iniFile.SetValue( "InputOutput", "className", "rayleighFlow" );
	else if (modelGroup->selected() == radioOblique)
        iniFile.SetValue( "InputOutput", "className", "obliqueFlow" );
	else if (modelGroup->selected() == radioShockDynamics)
        iniFile.SetValue( "InputOutput", "className", "shockDynamicsFlow" );
	else // default 
        iniFile.SetValue( "InputOutput", "className", "isentropicFlow" );

	iniFile.WriteFile(); 
	//iniFile.close();
}

int gdForm::canSave()
{
    QString notice,  caption;
    if (modelGroup->selected() == 0 ){
        notice = "<h2>Gas Dynamics Calculator 0.4.4</h2>" ;
        notice += "<p>You need to select the model radio";
        QMessageBox::warning (this , "Gas Dynamics Calculator", notice);//,
//                   QMessageBox::Ok );
        return false; 
    }
    else if (whatInputGroup->selected() == 0 ){
        notice = "<h2>Gas Dynamics Calculator 0.4.4</h2>" ;
        notice += "<p>You need to select the what Info radio";
        QMessageBox::warning (this , "Gas Dynamics Calculator", notice);//,
//                   QMessageBox::Ok );
        return false;
    }
	else {
		return true;
	}
}

void gdForm::valueChange(double k)
{
    //QString notice,  caption;
    //notice = "<h2>Gas Dynamics Calculator 0.4.4</h2>" ;
    //notice += "<p>You need to select the what Info radio";
    //QMessageBox::warning (this , "Gas Dynamics Calculator", notice);
	
	QString notice, notice1  ;
	isDataChanged = true;
	//notImplementedYet();
	k = kInput->value();
	if (modelGroup->selected() == radioShock ){// &&
		//MyInput->setRange( sqrt(.5*(k -1.)/k)+EPS, 1.0);
		//tmpRange = MyInput->getRange();
		//MyInput->setValue(0.5*(tmpRange[0]+tmpRange[1]));
	}
	else if (modelGroup->selected() == radioIsentropic ){
		if ((inputDataGroup->selected() == radioSuperSonic) || 
		( Input3->value() >= 1.0)) { //supersonic flow M > 1
			//k = kInput->value();
			//TbarInput->setRange(0.0+EPS, 1.0);
			//RbarInput->setRange(0.0+EPS, 1.0);
			//PARInput->setRange(0.0+EPS, 1.0 );
		}
		else {
			//TbarInput->setRange((2.0/(k+1)), 1.0 );
			//RbarInput->setRange( pow((2.0/(k+1)), (1. /(k -1.))), 1.0 );
			//PARInput->setRange(1.0+EPS, 300.0 );
		}
	}
	else if (modelGroup->selected() == radioShockDynamics ){
		//notImplementedYet("");
	}
	else if (modelGroup->selected() == radioIsoNozzle ){
		double M1maxSub =  1./sqrt(k);
		//TbarInput->setRange(
		//	2.*exp(k*0.5) *pow(((k+1.)/2.),(k/(k-1.))) /(k+1.) , 10.0 );
		//tmpRange = TbarInput->getRange();
		//TbarInput->setValue(0.5*(tmpRange[0]+tmpRange[1]));

		//pInput->setRange(0.0+EPS, exp(k*0.5) );
		//tmpRange = pInput->getRange();
		//pInput->setValue(0.5*(tmpRange[0]+tmpRange[1]));

		if ( Input3->value() >= M1maxSub ){ //supersonic flow M >1/sqrt(k)
			// only for the area ratio
			//TbarInput->setRange(0.0+EPS, (2.0/(k+1)) );
		notice = "<h2>Gas Dynamics Calculator 0.4.4</h2>" ;
		notice += "<p>The Mach number is:   ";
    	notice += notice1.setNum( Input3->value());
		notice += "<p>The upper limit  is:   ";
    	notice += notice1.setNum( M1maxSub );
		notice += "<p>The k value  is:   ";
    	notice += notice1.setNum( k);
		//notice += "<p>The base value  is:   ";
    	//notice += notice1.setNum( (2.0/(k+1)));
		//notice += "<p>The power value  is:   ";
    	//notice += notice1.setNum( (k /(k -1.)) );

		} 
		else { // subsonic
			//p0Input->setRange(1., 2. );
			//RbarInput->setRange(0.0+EPS, 1.0);
		notice = "<h2>Gas Dynamics Calculator 0.4.4</h2>" ;
		notice += "<p>The Mach number is:   ";
    	notice += notice1.setNum( Input3->value());
		notice += "<p>The upper limit  is:   ";
    	//notice += notice1.setNum( M1maxSub );
		notice += "<p>The k value  is:   ";
    	notice += notice1.setNum( k);
		notice += "<p>The range is:   ";
		//p0Input->setValue(0.5*(tmpRange[0]+tmpRange[1]));
    	notice += notice1.setNum( tmpRange[0] );
		notice += "<p>The upper limit  is:   ";
    	notice += notice1.setNum( tmpRange[1] );
		}
		QMessageBox::warning (this , "Gas Dynamics Calculator", notice);
	}
	else if (modelGroup->selected() == radioFanno ){
		k = kInput->value();
		double  M1maxSub; 
		M1maxSub= 1.0;
        if ((inputDataGroup->selected() == radioSuperSonic) ||
        ( Input3->value() >= 1.0)) { //supersonic flow M > 1
			notice = "<h2>Gas Dynamics Calculator 0.4.4</h2>" ;
			notice += "<p>The Mach number is:   ";
			notice += notice1.setNum( Input3->value());
			notice += "<p>The upper limit  is:   ";
			//notice += notice1.setNum( M1maxSub );
			notice += "<p>The k value  is:   ";
			notice += notice1.setNum( k);
			//FLDInput->setRange(0.0+EPS, superSonicFLD  );
			//notice += "<p>The base value  is:   ";
			//notice += notice1.setNum( (2.0/(k+1)));
			//notice += "<p>The power value  is:   ";
			//notice += notice1.setNum( (k /(k -1.)) );
		} 
		else { // subsonic
			//p0Input->setRange(1., 2. );
			//RbarInput->setRange(0.0+EPS, 1.0);
			//FLDInput->setRange(0.0+EPS, 500.  );
			notice = "<h2>Gas Dynamics Calculator 0.4.4</h2>" ;
			notice += "<p>The Mach number is:   ";
			notice += notice1.setNum( Input3->value());
			notice += "<p>The upper limit  is:   ";
			//notice += notice1.setNum( M1maxSub );
			notice += "<p>The k value  is:   ";
			notice += notice1.setNum( k);
			notice += "<p>The range is:   ";
			//FLDInput->setRange(0.0+EPS, 500.0);
			//tmpRange = FLDInput->getRange();
			//FLDInput->setValue(0.5*(tmpRange[0]+tmpRange[1]));
			notice += notice1.setNum( tmpRange[0] );
			notice += "<p>The upper limit  is:   ";
			notice += notice1.setNum( tmpRange[1] );
		}
		//QMessageBox::warning (this , "Gas Dynamics Calculator", notice);
	}
	//tmpRange = p0Input->getRange();
	//p0Input->setValue(0.5*(tmpRange[0]+tmpRange[1]));
	//tmpRange = RbarInput->getRange();
	//RbarInput->setValue(0.5*(tmpRange[0]+tmpRange[1]));
	//tmpRange = FLDInput->getRange();
	//FLDInput->setValue(0.5*(tmpRange[0]+tmpRange[1]));
	//setNoticeIfNeeded();
}

void gdForm::Load()
{
	QString fn = QFileDialog::getOpenFileName( ".",
	"gdc files (*.gdc *.ini *.dat );;"
	"Text files (*.txt);;"
	"Any files (*)" ,
		   this, "Load file dialog" "Choose file to load");
																			
		// to set up the extension for the file name
	if ( !fn.isEmpty() ) {
		filename = fn;
		isDataChanged = false;
		//fileSave();
	}
}

void gdForm::newValues()
{
	isDataChanged = true ;
}



void gdForm::selectBranch(branchType a)
{
	if (a == simple){
		radioSubSonic->setEnabled(true);
		radioSubSonic->setShown(true);
		radioSuperSonic->setEnabled(true);
		radioSuperSonic->setShown(true);
	}
	else if (a == all){
		radioSubSonic->setEnabled(true);
		radioSubSonic->setShown(true);
		radioSuperSonic->setEnabled(true);
		radioSuperSonic->setShown(true);
		radioBothSonic->setEnabled(true);
		radioBothSonic->setShown(true);
		radioSubSonic->setChecked(true);
	}
	else {
		radioSubSonic->setEnabled(false);
		radioSubSonic->setShown(false);
		radioSuperSonic->setEnabled(false);
		radioSuperSonic->setShown(false);
		radioBothSonic->setEnabled(false);
		radioBothSonic->setShown(false);
	}
}


void gdForm::valueChange()
{
	double k;
	k = kInput->value();
	//rangeNotice->setShown(false);
	if (inputDataGroup->selected() ==  radioSubSonic) {
		//PARInput->setRange(1.0+EPS, 300.0 );
		//p0Input->setRange( pow((2.0/(k+1)), (k /(k -1.))), 1.0 );
		//TbarInput->setRange((2.0/(k+1)), 1.0 );
		//RbarInput->setRange(pow((2.0/(k+1)), (1. /(k -1.))), 1.0 );
		//FLDInput->setRange(0.0+EPS, 500.  );
	}
	else if (inputDataGroup->selected() ==  radioSuperSonic ) {
		//PARInput->setRange(0.0+EPS, 1.0 );
		//p0Input->setRange(0.0+EPS, 1.0);
		//TbarInput->setRange(0.0+EPS, 1.0);
		//RbarInput->setRange(0.0+EPS, 1.0);
        //double superSonicFLD = 0.821508116; // to change to account k
		//FLDInput->setRange(0.0+EPS, superSonicFLD  );
    //QMessageBox::about(this , "You selected: ", "Super Flow" );
	}
	//reset the value
    //tmpRange = p0Input->getRange();
    //p0Input->setValue(0.5*(tmpRange[0]+tmpRange[1]));
    //tmpRange = RbarInput->getRange();
    //RbarInput->setValue(0.5*(tmpRange[0]+tmpRange[1]));
    //tmpRange = PARInput->getRange();
    //PARInput->setValue(0.5*(tmpRange[0]+tmpRange[1]));
	//setInput();
	setNoticeIfNeeded();
}

void gdForm::setNotice(inputType whatInput )
{
	QString notice, notice1,  caption;
	//->setShown(true);
	if (whatInput == MxV ){
	}
    else if (whatInput == MyV ){
        //tmpRange = MyInput->getRange();
    }
    else if (whatInput == P0barV ){
        //tmpRange = p0Input->getRange();
    }
    else if (whatInput == T0barV ){
        //tmpRange = p0Input->getRange();
    }
    else if (whatInput == R0barV ){
        //tmpRange = p0Input->getRange();
    }
    else if (whatInput == PbarV ){
        //tmpRange = pInput->getRange();
    }
    else if (whatInput == TbarV ){
    }
    else if (whatInput == RbarV ){
    }
    else if (whatInput == PARV ){
        //tmpRange = PARInput->getRange();
    }
	notice = "<b>The possible Range is</b><br>" ;
	notice += "lowest value is : " ;
	notice += notice1.setNum(  tmpRange[0]);
	notice += "<br>" ;
	notice += "highest value is: ";
	notice += notice1.setNum(  tmpRange[1]);
	//rangeNotice->setText(notice);
}

void gdForm::setNoticeIfNeeded()
{
	//inputType inputBox;
	//if (whatInputGroup->selected() != 0){
	//	inputBox=iMx;
	//	setNotice(inputBox);
	//}
}


void gdForm::branchSelected()
{
	double k , inputValue=1.0;
    k =  kInput->value();
	double superSonicFLD, M1maxSub;
	int numberOfBox;
	M1maxSub= 1.0;
	superSonicFLD = 0.821508116; // to change to account for k

	setInfoNotice ("no message now");
	if (modelGroup->selected() == radioIsothermal) {
		if (inputDataGroup->selected() == radioSubSonic){ 
			if (whatInputGroup->selected() == paraList.at(fldIso) ){
				Input0->setRange((.0+EPS), 500.);
				numberOfBox = 1;
			}
			else if (whatInputGroup->selected() == paraList.at(PbarIso) ){
				Input0->setRange((.0+EPS), 200.);
				numberOfBox = 1;
			}
		}
		else if (inputDataGroup->selected() == radioSuperSonic){ 
			if (whatInputGroup->selected() == paraList.at(fldIso) ){
				Input0->setRange((.0+EPS), 20.);
				numberOfBox = 1;
			}
		}
		else if (inputDataGroup->selected() == radioBothSonic) {
			if (whatInputGroup->selected() == paraList.at(fldIso) ){
				Input0->setRange((.0+EPS), 550.);
				numberOfBox = 1;
			}
		}
		setInputBoxValue(numberOfBox);
	}
	else if (modelGroup->selected() == radioIsentropic) {
		if (inputDataGroup->selected() == radioSubSonic ) {
			if (whatInputGroup->selected() == paraList.at(PbarI) ) { //Pbar
				Input0->setRange(pow((2.0/(k+1.)), (k /(k -1.))) ,1.0);
				numberOfBox = 1;
			}
			else if  (whatInputGroup->selected() == paraList.at(TbarI) ) {// Tbar
				Input0->setRange((2.0/(k+1.)) ,1.0);
				numberOfBox = 1;
			}
			else if  (whatInputGroup->selected() == paraList.at(RbarI) ) {//Rbar
				Input0->setRange(pow((2.0/(k+1.)), (1. /(k -1.))) ,1.0);
				numberOfBox = 1;
			}
			else if  (whatInputGroup->selected() == paraList.at(AstarI)){//Astar
				Input0->setRange(1.0, 100.);
				numberOfBox = 1;
			}
			else if  (whatInputGroup->selected() == paraList.at(PARI) ) { //PAR
				Input0->setRange(1.0+EPS, 100.0);
				numberOfBox = 1;
			}
		}
		else if (inputDataGroup->selected() == radioSuperSonic ){ 
			if (whatInputGroup->selected() == paraList.at(PbarI) ) {//Pbar
				Input0->setRange(0. ,pow((2.0/(k+1.)), (k /(k -1.))));
				numberOfBox = 1;
			} 
			else if  (whatInputGroup->selected() == paraList.at(TbarI) ) {//Tbar
				Input0->setRange(0. ,(2.0/(k+1.)) );
				numberOfBox = 1;
			}
			else if  (whatInputGroup->selected() == paraList.at(RbarI) ) {//Rbar
				Input0->setRange(0. ,(2.0/(k+1.)) );
				numberOfBox = 1;
			}
			else if (whatInputGroup->selected() == paraList.at(AstarI) ){// Astar
				Input0->setRange(1.0, 100.);
				numberOfBox = 1;
			}
			else if  (whatInputGroup->selected() == paraList.at(PARI) ) {//PAR
				Input0->setRange(EPS, 1.);
				numberOfBox = 1;
			}
		}
		else if (inputDataGroup->selected() == radioBothSonic ){ 
			if  (whatInputGroup->selected() == paraList.at(AstarI) ) {// Astar
				Input0->setRange( 1., 100.);
				//do something
			}
			else if  (whatInputGroup->selected() == paraList.at(PARI) ) {//PAR
				inputValue = initialValue[0][1];
			}
			numberOfBox = 1;
		}
		//set Input boxes value
		setInputBoxValue(numberOfBox);
	}
	else if (modelGroup->selected() == radioFanno) {
		//assumed no unchoked issue yet 
		if (inputDataGroup->selected() == radioSubSonic ) {
			if (whatInputGroup->selected() == paraList.at(MF) ) { 
				setInfoNotice ("you can access subsonic ranches now");
				Input0->setRange(EPS,1.0);
				numberOfBox = 1;
			}
			else if  (whatInputGroup->selected() == paraList.at(fldF) ) {
				setInfoNotice ("only the subsonic branch ");
				Input0->setRange(EPS ,300.0);
				numberOfBox = 1;
			}
			else if  (whatInputGroup->selected() == paraList.at(PbarF) ) {
				Input0->setRange(1.0, 300.);
				numberOfBox = 1;
			}
			else if  (whatInputGroup->selected() == paraList.at(TbarF) ) {
				Input0->setRange(1.0, 0.5*(k+1.) );
				numberOfBox = 1;
			}
			else if  (whatInputGroup->selected() == paraList.at(RbarF) ) {
				Input0->setRange(1.0, 10. );
				numberOfBox = 1;
			}
			else if  (whatInputGroup->selected() == paraList.at(P0barF) ) {
				Input0->setRange(1.0, 80. );
				numberOfBox = 1;
			}
			else if  (whatInputGroup->selected() == paraList.at(AstarI)){//Astar
				setInfoNotice ("can get only the subsonic branches now");
				Input0->setRange(1.0, 100.);
				numberOfBox = 1;
			}
			else if  (whatInputGroup->selected() == paraList.at(PARI) ) { //PAR
				Input0->setRange(1.0+EPS, 100.0);
				numberOfBox = 1;
			}
		}
		else if (inputDataGroup->selected() == radioSuperSonic ){ 
			if (whatInputGroup->selected() == paraList.at(MF) ) {
				setInfoNotice ("you can access supersonic ranches now");
				Input0->setRange(1. , 500.);
				numberOfBox = 1;
			} 
			else if  (whatInputGroup->selected() == paraList.at(fldF) ) {
				setInfoNotice ("only the supersonic branch now");
				Input0->setRange(EPS , ( (0.5*(k +1. )/k 
						*log ( (k+1.) /(k-1.) ) ) - 1./k) );
				numberOfBox = 1;
			}
			else if  (whatInputGroup->selected() == paraList.at(PbarF) ) {
				Input0->setRange(0. , 1. );
				numberOfBox = 1;
			}
			else if  (whatInputGroup->selected() == paraList.at(TbarF) ) {
				Input0->setRange(0. , 1. );
				numberOfBox = 1;
			}
			else if  (whatInputGroup->selected() == paraList.at(RbarF) ) {
				Input0->setRange( sqrt( (k -1.) / (k+1.)) +EPS, 1.0 );
				numberOfBox = 1;
			}
			else if  (whatInputGroup->selected() == paraList.at(P0barF) ) {
				Input0->setRange( 1., 70. );
				numberOfBox = 1;
			}
			else if (whatInputGroup->selected() == paraList.at(AstarI) ){// Astar
				setInfoNotice ("can get only the supersonic branch now");
				Input0->setRange(1.0, 100.);
				numberOfBox = 1;
			}
			else if  (whatInputGroup->selected() == paraList.at(PARI) ) {//PAR
				Input0->setRange(EPS, 1.);
				numberOfBox = 1;
			}
		}
		else if (inputDataGroup->selected() == radioBothSonic ){ 
			if  (whatInputGroup->selected() == paraList.at(MF) ) {// 
				Input0->setRange( EPS , 500.);
				setInfoNotice ("you can access both branches now");
				//do something
			}
			if  (whatInputGroup->selected() == paraList.at(MF) ) {// 
				Input0->setRange( EPS , 500.);
				setInfoNotice ("can get both branches now");
				//do something
			}
			else if  (whatInputGroup->selected() == paraList.at(AstarI) ) {// Astar
				Input0->setRange( 1., 100.);
				setInfoNotice ("can get both branches now");
				//do something
			}
			else if  (whatInputGroup->selected() == paraList.at(PARI) ) {//PAR
				inputValue = initialValue[0][1];
			}
			numberOfBox = 1;
		}
		//set Input box's value
		setInputBoxValue(numberOfBox);
	}
	else if (modelGroup->selected() == radioRayleigh) {
		numberOfBox = 1;
		if (inputDataGroup->selected() == radioSuperSonic){ 
			if (whatInputGroup->selected() == paraList.at (PbarR) ) { //P
				Input0->setRange(1. ,1.+k);
			}
			else if (whatInputGroup->selected() == paraList.at (TbarR) ) { //T
				Input0->setRange(0. ,(2.0/(k+1.)));
			}
			else if (whatInputGroup->selected() == paraList.at (RbarR) ) { //Rho
				Input0->setRange(0., pow((2.0/(k+1.)), (1. /(k -1.))) );
			}
			else if (whatInputGroup->selected() == paraList.at (P0barR) ) { //P0
				Input0->setRange(1.0, 300.);
			}
			else if (whatInputGroup->selected() == paraList.at (T0barR) ) { //T0
				Input0->setRange((k+1.)*(k-1.0)/k , 1.0);
			}
		}
		else if (inputDataGroup->selected() == radioSubSonic ) {
			if (whatInputGroup->selected() ==  paraList.at (PbarR) ) { //P
				Input0->setRange(0. ,1.);
			}
			else if (whatInputGroup->selected() == paraList.at (TbarR) ) { //T
				Input0->setRange((2.0/(k+1.)) ,1.0);
			}
			else if (whatInputGroup->selected() ==  paraList.at (RbarR) ) { //Rho
				Input0->setRange(pow((2.0/(k+1.)), (1. /(k -1.))) ,1.0);
			}
			else if (whatInputGroup->selected() ==  paraList.at (P0barR) ) { //P0
            	Input0->setRange(1.0, (k+1.)*pow(2./(k+1.),k/(k-1.0)));
			}
			else if (whatInputGroup->selected() ==  paraList.at (T0barR)  ) { //T0
				Input0->setRange((k+1.)*(k-1.0)/k , 1.0);
			}
		}
		else if (inputDataGroup->selected() == radioBothSonic ) {
			if (whatInputGroup->selected() ==  paraList.at (PbarR)  ) { //P
				Input0->setRange(0. ,1.+k);
			}
			else if (whatInputGroup->selected() ==  paraList.at (TbarR) ) { //T
				Input0->setRange(0. ,1.0);
			}
			else if (whatInputGroup->selected() ==  paraList.at (RbarR) ) { //R
				Input0->setRange(0. ,1.0);
			}
			else if (whatInputGroup->selected() ==  paraList.at (P0barR) ) {//P0
				Input0->setRange(0. ,(k+1.)*pow(2./(k+1.),k/(k-1.0)) );
			}
		}
		setInputBoxValue(numberOfBox);
	}
	else if (modelGroup->selected() == radioIsoNozzle) {
		if (inputDataGroup->selected() == radioSubSonic ) {
			if (whatInputGroup->selected() == paraList.at(PbarIsoNo)){
				Input0->setRange(1., exp(k*0.5) );
			}
			else if  (whatInputGroup->selected()==paraList.at(RbarIsoNo)){
				Input0->setRange(1., exp(k*0.5) );
			}
			else if(whatInputGroup->selected()==paraList.at(RbarIsoNo)){
				Input0->setRange(pow((2.0/(k+1.)), (1. /(k -1.))) ,1.0);
			}
			else if  (whatInputGroup->selected() == paraList.at(PARIsoNo)) {
				Input0->setRange(1.0, 100.);
			}
			else if  (whatInputGroup->selected() == paraList.at(P0barIsoNo)) {
				Input0->setRange(1.,exp(k*0.5)* pow((2.0/(k+1.)),(k /(k -1.))) );
			}
			else if  (whatInputGroup->selected() == paraList.at(T0barIsoNo)) {
				Input0->setRange( (2./(1.+k)+EPS), 1.0);
			}
			else if  (whatInputGroup->selected() == paraList.at(R0barIsoNo)) {
				Input0->setRange
				((1./exp(k*0.5)* pow((2.0/(k+1.)), (k /(k-1.)))), 1.);
			}
		}
		else if (inputDataGroup->selected() == radioSuperSonic ){ 
			if (whatInputGroup->selected() == paraList.at(PbarIsoNo) ) {
				Input0->setRange(0. ,pow((2.0/(k+1.)), (k /(k -1.))));
			} 
			else if  (whatInputGroup->selected() == paraList.at(RbarIsoNo)){
				Input0->setRange(0. ,(2.0/(k+1.)) );
			}
			else if  (whatInputGroup->selected() == paraList.at(RbarIsoNo) ) {
				Input0->setRange(0. ,(2.0/(k+1.)) );
			}
			else if  (whatInputGroup->selected() == paraList.at(PARIsoNo)) {
				Input0->setRange(EPS, 1.0);
			}
			else if  (whatInputGroup->selected() == paraList.at(P0barIsoNo)){
				Input0->setRange(EPS, 1.);
			}
			else if  (whatInputGroup->selected() == paraList.at(T0barIsoNo)){
				Input0->setRange(1., 20.);
			}
			else if  (whatInputGroup->selected() == paraList.at(R0barIsoNo)) {
				Input0->setRange (1., 50.);
			}
		}
		else if (inputDataGroup->selected() == radioBothSonic ){ 
			if  (whatInputGroup->selected() == inputRadio4 ) {
				inputValue = 0.5*(initialValue[0][0] + initialValue[0][1]);
			}
			else if  (whatInputGroup->selected() == paraList.at(PARIsoNo)){
				inputValue = initialValue[0][1];
			}
		}
		//set Input box's value
		setInputBoxValue(1);
	}
}

void gdForm::fix_mD()
{
    if (modelGroup->selected() == radioOblique){
		//radioMx->setText("Mx(max)");
	}
}

void gdForm::onSubModelGroup()
{
	clearSubGroup();
	isSubModelON = yes;
	int numberOfRadios = 0;
	subModelGroup->setShown(true);
	subModelGroup->setEnabled(true);
	if (modelGroup->selected() == radioShockDynamics) {	
		subModelGroup->setTitle( tr( "Shock Dynamics" ) );
		pixGroup->setShown(true);
		pixGroup->setEnabled(true);
		for ( subParametersIterator.toFirst(); numberOfRadios < 4;
					numberOfRadios++ ) {
			subParametersIterator.current()->setEnabled(true); 
			subParametersIterator.current()->setShown(true);
			subParametersIterator.current()->setText
				(shockDynamicsLayoutName[numberOfRadios]);
			++subParametersIterator ; 
		}
	}
	else if (modelGroup->selected() == radioFanno) {	
		subModelGroup->setTitle( tr( "Fanno Flow" ) );
		for ( subParametersIterator.toFirst(); numberOfRadios < 2;
					numberOfRadios++ ) {
			subParametersIterator.current()->setEnabled(true); 
			subParametersIterator.current()->setShown(true);
			subParametersIterator.current()->setText
				(fannoSubLayoutName[numberOfRadios]);
			++subParametersIterator ; 
		}
	}
	//cout << "here onSubModelGroup" << endl;
	//notImplementedYet("here onSubModelGroup" );
}


void gdForm::clearSubGroup()
{
	suddenCloseRadio->setEnabled(false);
	suddenCloseRadio->setChecked(false);
	suddenCloseRadio->setShown(false);
	partiallyCloseRadio->setEnabled(false);
	partiallyCloseRadio->setChecked(false);
	partiallyCloseRadio->setShown(false);
	suddenOpenRadio->setEnabled(false);
	suddenOpenRadio->setChecked(false);
	suddenOpenRadio->setShown(false);
	subModelGroup->setShown(false);
	partiallyOpenRadio->setEnabled(false);
	partiallyOpenRadio->setChecked(false);
	partiallyOpenRadio->setShown(false);
}

void gdForm::setSubParameters()
{
	clearParameters();
	clearInputBoxs();
	//subModelGroup->setEnabled(true);
	//subModelGroup->setShown(true);
	//cout << "this is set sub parameters" << endl;
	//notImplementedYet("this is set sub parameters") ;
	int numberOfIterations = 0 ;
	if ( modelGroup->selected() == radioShockDynamics ) {
			pixGroup->setEnabled(true);
			pixGroup->setShown(true);
		if (subModelGroup->selected() == suddenCloseRadio ) {
			pixList.at(SClose)->setEnabled(true);
			pixList.at(SClose)->setShown(true);
			for ( parametersIterator.toFirst(); 
					numberOfIterations < numberOfParameters[suddenCloseFlow];
						++parametersIterator ) {
				parametersIterator.current()->setEnabled(true); 
				parametersIterator.current()->setShown(true);
				parametersIterator.current()->setText
					(closeLayoutName[numberOfIterations]);
				numberOfIterations++;
			}
		}
		else if (subModelGroup->selected() == suddenOpenRadio ) {
			pixList.at(SOpen)->setEnabled(true);
			pixList.at(SOpen)->setShown(true);
			for ( parametersIterator.toFirst(); 
					numberOfIterations < numberOfParameters[suddenOpenFlow];
						++parametersIterator ) {
				parametersIterator.current()->setEnabled(true); 
				parametersIterator.current()->setShown(true);
				parametersIterator.current()->setText
					(openLayoutName[numberOfIterations]);
				numberOfIterations++;
			}
			//do corrections the name of the inputs
			//labelStr = rhoCh + "y" + rhoCh + "x";
			//paraList.at(RyRxS)->setText(labelStr);
			//notImplementedYet();
		}
		else if (subModelGroup->selected() == partiallyOpenRadio ) {
			//notImplementedYet();
			pixList.at(POpen)->setEnabled(true);
			pixList.at(POpen)->setShown(true);
			for ( parametersIterator.toFirst(); 
					numberOfIterations < numberOfParameters[pOpenFlow];
						++parametersIterator ) {
				parametersIterator.current()->setEnabled(true); 
				parametersIterator.current()->setShown(true);
				parametersIterator.current()->setText
					(pOpenLayoutName[numberOfIterations]);
				numberOfIterations++;
			}
		}
		else if (subModelGroup->selected() == partiallyCloseRadio ) {
			pixList.at(POpen)->setEnabled(true);
			pixList.at(POpen)->setShown(true);
			for ( parametersIterator.toFirst(); 
					numberOfIterations < numberOfParameters[pCloseFlow];
						++parametersIterator ) {
				parametersIterator.current()->setEnabled(true); 
				parametersIterator.current()->setShown(true);
				parametersIterator.current()->setText
					(pCloseLayoutName[numberOfIterations]);
				numberOfIterations++;
			}
		}
	}
	else if ( modelGroup->selected() == radioFanno ) {
		if (subModelGroup->selected() == subParaList.at(chokedF) ) {
			setInfoNotice("no message");
			for( parametersIterator.toFirst(); 
					numberOfIterations < numberOfParameters[chokedFannoFlow];
						++parametersIterator ) {
				parametersIterator.current()->setEnabled(true); 
				parametersIterator.current()->setShown(true);
				parametersIterator.current()->setText
					(chokedFannoLayoutName[numberOfIterations]);
				numberOfIterations++;
			}
			labelStr = rhoCh +  "/" + rhoCh + "*" ;
			inputRadio4->setText(labelStr);
			labelStr = rhoCh +  "0/" + rhoCh + "0*" ;
			inputRadio7->setText(labelStr);
		}
		else if (subModelGroup->selected() == subParaList.at(unchokedF) ) {
			for ( parametersIterator.toFirst(); 
					numberOfIterations < numberOfParameters[unChokedFannoFlow];
						++parametersIterator ) {
				parametersIterator.current()->setEnabled(true); 
				parametersIterator.current()->setShown(true);
				parametersIterator.current()->setText
					(fannoUnChokedLayoutName[numberOfIterations]);
				numberOfIterations++;
			}
		}
	}
}

void gdForm::notImplementedYet(QString AdditionNotice)
{
    QString notice,  caption = "About Gas Dynamics Calculator",
				 notice1 = "";
    notice = "<h2>Gas Dynamics Calculator " ;
    notice += versionNumber;
    notice += "</h2>" ;
    notice += "<p>Copyright &copy; 2005, 2006, 2007, 2008 Dr. Genick Bar-Meir";
    notice += "<p>This function is not implemented yet.<p>" ;
    notice += AdditionNotice ;
                                                                             
    QMessageBox::about(this , caption, notice );
}

void gdForm::clearInfoBox()
{
    radioStandard->setEnabled(false);
    //radioStandard->setChecked( false );
    radioStandard->setShown(false);
	radioStandard->setText("");
	radioMinimum->setText("");
	radioMinimum->setEnabled(false);
	radioMinimum->setShown(false);
	radioMaximum->setText("");
	radioMaximum->setEnabled(false);
	radioMaximum->setShown(false);
}


void gdForm::setInfoBoxs()
{
	clearInfoBox();
	if (modelGroup->selected() == radioShockDynamics){
		radioStandard->setText("Standard");
    	radioStandard->setEnabled(true);
		radioStandard->setShown(true);
		radioMinimum->setText("Iteration");
		radioMinimum->setEnabled(true);
		radioMinimum->setShown(true);
		radioMaximum->setText("Maximum for k");
		radioMaximum->setEnabled(true);
		radioMaximum->setShown(true);
	}
    else if (modelGroup->selected() == radioFanno){
		radioStandard->setText("Standard");
    	radioStandard->setEnabled(true);
		radioStandard->setShown(true);
		radioMinimum->setText("Minimum");
		radioMinimum->setEnabled(true);
		radioMinimum->setShown(true);
		radioMaximum->setText("Iteration");
		radioMaximum->setEnabled(true);
		radioMaximum->setShown(true);
	}
	else {
		radioStandard->setText("Standard");
    	radioStandard->setEnabled(true);
		radioStandard->setShown(true);
		radioMinimum->setText("Minimum");
		radioMinimum->setEnabled(true);
		radioMinimum->setShown(true);
	} 
}

//range issues to fix 
void gdForm::newData()
{
	double k ;
	k = kInput->value();
	double * tmpRange;
	tmpRange = new double[2];
	if (modelGroup->selected() == radioShockDynamics ){
		if (subModelGroup->selected() == suddenOpenRadio ){ 
			//MyInput->setRange ( EPS, sqrt(2./k / (k -1.)) - EPS );
			//tmpRange = MyInput->getRange();
			//MyInput->setValue(0.5*(tmpRange[0]+tmpRange[1]));
		}
		else if (subModelGroup->selected() == partiallyCloseRadio ){

		}
		else if (subModelGroup->selected() == partiallyOpenRadio){ 
			//if ( MxInput->value() >= MyInput->value() ) {
			//	if ( whatInputGroup->selected() == radioP0 ) {
			//		tmpRange = MxInput->getRange();
			//		MxInput->setValue(0.5*(tmpRange[0]+tmpRange[1]));
			//	}
			//}
		}
	}
}

void gdForm::fixP( int a, QString label)
{
	if (a == 0 ) {
		U0 = Pp;
		Input0->setLabel(label);
		Units0->clear();
		Units0->insertItem( tr( "Bar" ) );
		Units0->insertItem( tr( "Pa (N/m^2)" ) );
		Units0->insertItem( tr( "MPa" ) );
		Units0->insertItem( tr( "Psi" ) );
	}
	else if ( a == 1) {
		U1 = Pp;
		Input1->setLabel(label);
		Units1->clear();
		Units1->insertItem( tr( "Bar" ) );
		Units1->insertItem( tr( "Pa (N/m^2)" ) );
		Units1->insertItem( tr( "MPa" ) );
		Units1->insertItem( tr( "Pa" ) );
		Units1->insertItem( tr( "Psi" ) );
	}
	else if ( a == 2) {
		U2 = Pp;
		Input2->setLabel(label);
		Units2->clear();
		Units2->insertItem( tr( "Bar" ) );
		Units2->insertItem( tr( "Pa (N/m^2)" ) );
		Units2->insertItem( tr( "MPa" ) );
		Units2->insertItem( tr( "Pa" ) );
		Units2->insertItem( tr( "Psi" ) );
	}
}

void gdForm::fixT( int a, QString label)
{
    if (a == 0 ) {
		U0 = Tp;
        Input0->setLabel(label);
        Units0->clear();
        Units0->insertItem( tr( "C" ) );
        Units0->insertItem( tr( "K" ) );
        Units0->insertItem( tr( "R" ) );
        Units0->insertItem( tr( "F" ) );
    }
    else if (a == 1 ) {
		U1 = Tp;
        Input1->setLabel(label);
        Units1->clear();
        Units1->insertItem( tr( "C" ) );
        Units1->insertItem( tr( "K" ) );
        Units1->insertItem( tr( "R" ) );
        Units1->insertItem( tr( "F" ) );
    }
    else if (a == 2 ) {
		U2 = Tp;
        Input2->setLabel(label);
        Units2->clear();
        Units2->insertItem(  "C" );
        Units2->insertItem(  "K" );
        Units2->insertItem(  "R" );
        Units2->insertItem(  "F" );
    }
}


void gdForm::fixRho( int a, QString label)
{
	 if (a == 0 ) {
		U0 = Rhop;
        Input0->setLabel(label);
        Units0->clear();
        Units0->insertItem( "kg/m^3"  );
        Units0->insertItem( "lbm/ft^3"  );
    }
	 else if (a == 1 ) {
		U1 = Rhop;
        Input1->setLabel(label);
        Units1->clear();
        Units1->insertItem( "kg/m^3"  );
        Units1->insertItem( "lbm/ft^3"  );
    }
    else {
		U2 = Rhop;
        Input2->setLabel(label);
        Units2->clear();
        Units2->insertItem( "kg/m^3"  );
        Units2->insertItem( "lbm/ft^3" );
    }
}


void gdForm::fix_s( int, QString )
{

}


void gdForm::fix_x( int )
{

}


void gdForm::fix_h( int , QString)
{

}


void gdForm::fix_u( int , QString)
{

}

void gdForm::fix_U( int unitsLocationPlace , QString label) // velocity (U)
{
	U0 = Velocityp ;
	inBoxList.at(unitsLocationPlace)->setLabel(label);
	unitsList.at(unitsLocationPlace)->clear();
	unitsList.at(unitsLocationPlace)->insertItem( tr( "m/sec" ) );
	unitsList.at(unitsLocationPlace)->insertItem( tr( "ft/sec" ) );
	unitsList.at(unitsLocationPlace)->insertItem( tr( "km/hr" ) );
	unitsList.at(unitsLocationPlace)->insertItem( tr( "mile/hr" ) );
}

void gdForm::fix_mD( int a , QString label)
{
    if (a == 0 ) {
		U0 = mDotAreap;
		Input0->setLabel(label);
		Units0->clear();
		Units0->insertItem( tr( "kg/sec/m^2" ) );
		Units0->insertItem( tr( "lbm/sec/ft^2" ) );
		Units0->insertItem( tr( "lbm/sec/in^2" ) );
    }
    else if (a == 1 ) {
		U1 = mDotAreap;
        Input1->setLabel(label);
        Units1->clear();
        Units1->insertItem( tr( "kg/sec/m^2" ) );
        Units1->insertItem( tr( "lbm/sec/ft^2" ) );
        Units1->insertItem( tr( "lbm/sec/in^2" ) );
    }
    else  if (a == 2 )  {
		U2 = mDotAreap;
        Input2->setLabel(label);
        Units2->clear();
        Units2->insertItem(  "kg/sec/m^2" );
        Units2->insertItem(  "lbm/sec/ft^2" );
        Units2->insertItem(  "lbm/sec/in^2" );
    }
}

void gdForm::UnitsChanged0( int unitsNum )
{
	double LL = 0.  , UL = 0., currentValue = 0.0, fixedValue = 0.0 ;
	if ( U0 != Tp){
		cout << "it is units changed" << endl ;
		cout << unitsNum << endl ;
		cout << conversionCoefficient[U0][unitsNum] << endl ;
		LL =  rangeLimits [U0][0] / conversionCoefficient[U0][unitsNum] ;
		UL =  rangeLimits [U0][1] / conversionCoefficient[U0][unitsNum] ;
		currentValue = Input0->value();
		cout << "currentValue = " << currentValue << endl ;
		fixedValue = Input0->value()*conversionCoefficient[U0][currentUnits[0]];
		fixedValue = fixedValue / conversionCoefficient[U0][unitsNum];
		//valueCorrectionDueUnitsChange(location0);
	}
	else {
		temperatureConversion ( currentUnits[0], rangeLimits [U0][0],
			unitsNum, LL ) ;
		temperatureConversion ( currentUnits[0], rangeLimits [U0][1],
			unitsNum, UL ) ;
		currentValue = Input0->value();
		temperatureConversion ( currentUnits[0], currentValue,
			unitsNum, fixedValue ) ;
	}
	cout << LL << "LL " << endl ;
	cout << UL << "UL " << endl ;
	Input0->setRange(LL ,UL);
	currentUnits[0] = unitsNum;
	cout << "fixedValue = " << fixedValue << endl ;
	inBoxList.at(location0)->setValue( fixedValue ) ; 
	//tmpRange = Input0->getRange();
	//Input0->setValue(0.25*(tmpRange[0]+tmpRange[1]));
}


void gdForm::UnitsChanged1( int unitsNum  )
{
	double LL = 0.  , UL = 0., currentValue = 0.0, fixedValue = 0.0 ;
	if ( U1 != Tp){
		LL =  rangeLimits [U1][0] / conversionCoefficient[U0][unitsNum] ;
		UL =  rangeLimits [U1][1] / conversionCoefficient[U0][unitsNum] ;
		currentValue = Input1->value();
		fixedValue = Input1->value()*conversionCoefficient[U0][currentUnits[0]];
		fixedValue = fixedValue / conversionCoefficient[U0][unitsNum];
		//valueCorrectionDueUnitsChange(location0);
	}
	else {
		cout << "rangeLimits [U1][0]  = " << rangeLimits [U1][0]  << endl ;
		currentValue = Input1->value();
		temperatureConversion ( 0, rangeLimits [U1][0],
			unitsNum, LL ) ;
		//cout << "currentUnits = " << currentUnits[1] << endl;
		temperatureConversion ( 0, rangeLimits [U1][1],
			unitsNum, UL ) ;
		cout << "currentUnits = " << currentUnits[1] << endl;
		cout << "current temp = " << currentValue << endl;
		temperatureConversion ( currentUnits[1], currentValue,
			unitsNum, fixedValue ) ;
	}
	Input1->setRange(LL ,UL);
	currentUnits[1] = unitsNum;
		cout << "currentUnits = " << currentUnits[1] << endl;
	inBoxList.at(location1)->setValue( fixedValue ) ; 
}


void gdForm::UnitsChanged2(int unitsNum)
{
	double LL = 0. , UL = 0. ;
	if ( U2 == mDotAreap){
		double lowerLimit = 0.001;
		double upperLimit = 150.; 
		switch ( unitsNum ) {
		case 0: //Input1->setLabel("[kg/sec/m^2]");
			LL =  lowerLimit / mDotConversion [kgSecm2] ;
			UL =  upperLimit / mDotConversion[kgSecm2] ;
			break;
		case 1: //Input1->setLabel("[lbm/sec/ft^2]");
			LL =  lowerLimit / mDotConversion[lbmSecft2] ;
			UL =  upperLimit / mDotConversion[lbmSecft2] ;
			break;
		case 2: //Input1->setLabel("[lbm/sec/in^2]");
			LL =  lowerLimit / mDotConversion[lbmSecin2] ;
			UL =  upperLimit / mDotConversion[lbmSecin2] ;
			break;
		}
	} 
	Input3->setRange(LL ,UL);
	tmpRange = Input3->getRange();
    Input3->setValue(0.25*(tmpRange[0]+tmpRange[1]));
}

void gdForm::saveSelectedBranch(CIniFile & iniFile)
{
	//notImplementedYet("the save selected branch");
	if (inputDataGroup->selected() ==  radioSuperSonic){
		//notImplementedYet("save Supper Sonic");
		iniFile.SetValueF( "data", "M", 2.0);
	}
	else if (inputDataGroup->selected() ==  radioSubSonic){
		//notImplementedYet("save Sub Sonic");
		iniFile.SetValueF( "data", "M", 0.5);
	}
	else if (inputDataGroup->selected() ==  radioBothSonic)  {
		//notImplementedYet("save both Sonic");
		iniFile.SetValue( "integer", "isBothBranches", "yes" );
	}
}

void gdForm::setSubInputShockDynamics()
{
	double k ;
	int numberOfBox = 1;
	k =  kInput->value();
				//notImplementedYet("set Sub Input Shock Dynamics ");
	selectBranch(__noBranch) ;
	radioSuperSonic->setChecked(true);
	radioSuperSonic->setEnabled(true);
	radioSuperSonic->setShown(false);
	if (subModelGroup->selected() == suddenCloseRadio){
		Input0->setGeometry( QRect( 30, 37, 180, 45 ) );
		Input1->setGeometry( QRect( 300, 37, 150, 45));
		Input0->setEnabled(true);
		Input0->setShown(true);
		if (whatInputGroup->selected() == paraList.at(MxC) ){
         Input0->setLabel(paraList.current()->text());
         //notImplementedYet("after inputRadio0 setInput");
			Input0->setRange(1.0+EPS, 50.0 );
         isValueSet[location0][subsonic] = no;
         isValueSet[location0][supersonic] = yes;
         initialValue[location0][supersonic] = 1.5;
			numberOfBox = 1;
      }
      else if (whatInputGroup->selected() == paraList.at(MyC)){
         Input0->setLabel(paraList.current()->text());
			Input0->setRange( sqrt(.5*(k -1.)/k)+EPS, 1.0);
         isValueSet[location0][supersonic] = yes;
         initialValue[location0][supersonic] = 0.5;
			numberOfBox = 1;
      }
      else if (whatInputGroup->selected() == paraList.at(MxpC)){
         Input0->setLabel(paraList.current()->text());
			Input0->setRange(0., 1.5 );
         isValueSet[location0][supersonic] = yes;
         initialValue[location0][supersonic] = 0.5;
			numberOfBox = 1;
      }
      else if (whatInputGroup->selected() == paraList.at(UxpC)){
			U0=Velocityp;
			U1=Tp;
			Input0->setLabel("Uxp");
			Input0->setRange
					(rangeLimits[Velocityp][0],rangeLimits[Velocityp][1]);
			Input1->setLabel("Tx");
			Input1->setEnabled(true);
			Input1->setShown(true);
			Input1->setRange(rangeLimits[Tp][0], rangeLimits[Tp][1]);
			Units0->setEnabled(true);
			Units0->setShown(true);
			Units0->setGeometry( QRect( 30, 85, 180, 22));
			fix_U(0, "Uxp");
			Units1->setEnabled(true);
			Units1->setShown(true);
			Units1->setGeometry( QRect( 300, 85, 180, 22));
			fixT(1, "Tx");
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][subsonic] = no;
			initialValue[location0][supersonic] = 25.;
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][subsonic] = no;
			initialValue[location0][supersonic] = 25.;
			numberOfBox = 2;
		}
		else {// do nothing 
		}
		setInputBoxValue(numberOfBox);
	}
	else if (subModelGroup->selected() == partiallyCloseRadio ){
		Input0->setGeometry( QRect( 30, 37, 180, 45 ) );
		Input1->setGeometry( QRect( 300, 37, 150, 45));
		Input0->setEnabled(true);
		Input0->setShown(true);
		Input1->setEnabled(true);
		Input1->setShown(true);
		if (whatInputGroup->selected() == paraList.at(MxMypC) ){ // 
			Input0->setLabel("Mx");
			Input0->setRange(1.0+EPS, 50.0 );
			Input1->setLabel("Myp");
			Input1->setRange(0., 3.0 );
			isValueSet[location0][supersonic] = yes;
			isValueSet[location1][supersonic] = yes;
			isValueSet[location0][subsonic] = no;
			isValueSet[location1][subsonic] = no;
			initialValue[location0][supersonic] = 1.5;
			initialValue[location1][supersonic] = 0.5;
			numberOfBox = 2;
		}
		else if (whatInputGroup->selected() == paraList.at(MxpMypC)){// 
			Input0->setLabel("Mxp");
			Input0->setRange(1.0+EPS, 50.0 );
			Input1->setLabel("Myp");
			Input1->setRange(0., 3.0 );
			isValueSet[location0][supersonic] = yes;
			isValueSet[location1][supersonic] = yes;
			isValueSet[location0][subsonic] = no;
			isValueSet[location1][subsonic] = no;
			initialValue[location0][supersonic] = 1.5;
			initialValue[location1][supersonic] = 0.5;
			numberOfBox = 2;
		}
		else {// do nothing 
		}
		setInputBoxValue(numberOfBox);
	}
	else if (subModelGroup->selected() == suddenOpenRadio){
		Input0->setGeometry( QRect( 30, 30, 180, 45 ) );
		Input1->setGeometry( QRect( 300, 30, 180, 45));
		Input0->setEnabled(true);
		Input0->setShown(true);
		if (whatInputGroup->selected() == paraList.at(MxO)){ // Mx
         Input0->setLabel(paraList.current()->text());
			Input0->setRange(1.0+EPS, 60.0 );
         isValueSet[location0][supersonic] = yes;
         isValueSet[location0][subsonic] = no;
         initialValue[location0][subsonic] = 1.5;
         initialValue[location0][supersonic] = 1.5;
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() ==  paraList.at(MyO) ){// My
         Input0->setLabel(paraList.current()->text());
			Input0->setRange( sqrt(.5*(k -1.)/k)+EPS, 1.0);
         isValueSet[location0][supersonic] = yes;
         isValueSet[location0][subsonic] = no;
         initialValue[location0][supersonic] = 0.5;
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(MypO) ){// Myp 
         Input0->setLabel(paraList.current()->text());
			Input0->setRange(0., 2.0 );
         isValueSet[location0][supersonic] = yes;
         isValueSet[location0][subsonic] = no;
         initialValue[location0][supersonic] = 0.5;
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(PyPxO) ){//PyPx 
         Input0->setLabel(paraList.current()->text());
			Input0->setRange(1., 50.0 );
         isValueSet[location0][supersonic] = yes;
         isValueSet[location0][subsonic] = no;
         initialValue[location0][supersonic] = 1.5;
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(UsTxO) ){// Uxp Tx 
			//double lowerLimitVelocity = rangeLimits[Velocityp][0] ;
			U0=Velocityp;
			U1=Tp;
			setInfoNotice ("Mx must be larger than 1");
			Input0->setLabel("Ux");
			Input0->setRange
					(rangeLimits[Velocityp][0],rangeLimits[Velocityp][1]);
			Input1->setLabel("Tx");
			Input1->setEnabled(true);
			Input1->setShown(true);
			Input1->setRange(rangeLimits[Tp][0], rangeLimits[Tp][1]);
			Units0->setEnabled(true);
			Units0->setShown(true);
			Units0->setGeometry( QRect( 30, 85, 180, 22));
			fix_U(0, "Us");
			Units1->setEnabled(true);
			Units1->setShown(true);
			Units1->setGeometry( QRect( 300, 85, 180, 22));
			fixT(1, "T");
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][subsonic] = no;
			initialValue[location0][supersonic] = 450.;
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][subsonic] = no;
			initialValue[location1][supersonic] = 27.;
			numberOfBox = 2;
		}
		else if (whatInputGroup->selected() == paraList.at(PistonO)){ //UypTxOV
			U0=Velocityp;
			U1=Tp;
			Input0->setLabel("Uyp");
			Input0->setRange
					(rangeLimits[Velocityp][0],rangeLimits[Velocityp][1]);
			Input1->setLabel("Tx");
			Input1->setEnabled(true);
			Input1->setShown(true);
			Input1->setRange(rangeLimits[Tp][0], rangeLimits[Tp][1]);
			Units0->setEnabled(true);
			Units0->setShown(true);
			Units0->setGeometry( QRect( 30, 85, 180, 22));
			fix_U(0, "Uyp");
			Units1->setEnabled(true);
			Units1->setShown(true);
			Units1->setGeometry( QRect( 300, 85, 180, 22));
			fixT(1, "Tx");
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][subsonic] = no;
			initialValue[location0][supersonic] = 25.;
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][subsonic] = no;
			initialValue[location1][supersonic] = 27.;
			numberOfBox = 2;
		}
		else {// do nothing 
		}
		setInputBoxValue(numberOfBox);
	}
	else if (subModelGroup->selected() == partiallyOpenRadio){
		Input0->setGeometry( QRect( 30, 37, 180, 45 ) );
		Input1->setGeometry( QRect( 300, 37, 150, 45));
		Input0->setEnabled(true);
		Input0->setShown(true);
		Input1->setEnabled(true);
		Input1->setShown(true);
		if (whatInputGroup->selected() == paraList.at(MxpMyO) ){ // 
			Input0->setLabel("Mxp");
			Input0->setRange(.0+EPS, 10.0 );
			Input1->setLabel("My");
			Input1->setRange( sqrt(.5*(k -1.)/k)+EPS, 1.0);
			isValueSet[location0][supersonic] = yes;
			isValueSet[location1][supersonic] = yes;
			isValueSet[location0][subsonic] = no;
			isValueSet[location1][subsonic] = no;
			initialValue[location0][supersonic] = 1.5;
			initialValue[location1][supersonic] = 0.5;
			numberOfBox = 2;
		}
		else if (whatInputGroup->selected() == paraList.at(MxpMypO)){//
			//notImplementedYet("setSubInputShockDynamics inputradio 0");
			Input0->setLabel("Mxp");
			Input0->setRange(0.2, 9.5 );
			Input1->setLabel("Myp");
			Input1->setRange(0., 1.0 );
			isValueSet[location0][supersonic] = yes;
			isValueSet[location1][supersonic] = yes;
			isValueSet[location0][subsonic] = no;
			isValueSet[location1][subsonic] = no;
			initialValue[location0][supersonic] = 0.7;
			initialValue[location1][supersonic] = 0.5;
			numberOfBox = 2;
		}
		else {// do nothing 
		}
		setInputBoxValue(numberOfBox);
	}
};

void gdForm::setSubInputFanno()
{
	int numberOfBox = 0;
	double k ;
	k =  kInput->value();
	Input0->setGeometry( QRect( 30, 37, 180, 45 ) );
	Input1->setGeometry( QRect( 300, 37, 150, 45));
	Input0->setEnabled(true);
	Input0->setShown(true);
	if (subModelGroup->selected() == subParaList.at(chokedF) ){
		if (whatInputGroup->selected() == paraList.at(MF) ){ // 
			setInfoNotice 
				("clicking on both change the range don't provide two solutions");
			Input0->setLabel(paraList.at(MF)->text() );
			Input0->setRange(.0+EPS, 1.0 );
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][subsonic] = yes;
			isValueSet[location0][bothsonic] = yes;
			initialValue[location0][subsonic] = 0.5;
			initialValue[location0][supersonic] = 1.5;
			initialValue[location0][bothsonic] = 0.8;
			selectBranch(all);
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(fldF)){//
			//notImplementedYet("setSubInputShockDynamics inputradio 0");
			setInfoNotice ("clicking on both provide both branches");
			Input0->setLabel(paraList.current()->text() );
			Input0->setRange(EPS, 500.);
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][subsonic] = yes;
			isValueSet[location0][bothsonic] = yes;
			initialValue[location0][subsonic] = 0.7;
			initialValue[location0][supersonic] = 0.1;
			initialValue[location0][bothsonic] = 0.2;
			selectBranch(all);
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(PbarF)){//
			//notImplementedYet("setSubInputShockDynamics inputradio 0");
			Input0->setLabel(paraList.current()->text() );
			Input0->setRange(EPS, 300.);
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][subsonic] = yes;
			isValueSet[location0][bothsonic] = yes;
			initialValue[location0][subsonic] = 1.7;
			initialValue[location0][supersonic] = 0.1;
			initialValue[location0][bothsonic] = 1.5;
			selectBranch(simple);
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(TbarF)){//
			Input0->setLabel(paraList.current()->text() );
			Input0->setRange(1., 0.5*(k+1.) );
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][subsonic] = yes;
			isValueSet[location0][bothsonic] = yes;
			initialValue[location0][subsonic] = 0.5 * (1. + 0.5*(k+1.) ) ;
			initialValue[location0][supersonic] = 0.1;
			initialValue[location0][bothsonic] = 1.5;
			selectBranch(simple);
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(RbarF)){//
			Input0->setLabel(paraList.current()->text() );
			Input0->setRange(1., 10. );
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][subsonic] = yes;
			isValueSet[location0][bothsonic] = yes;
			initialValue[location0][subsonic] = 1.6 ;
			initialValue[location0][supersonic] = 0.1;
			initialValue[location0][bothsonic] = 1.5;
			selectBranch(simple);
			numberOfBox = 1;
		}
		else if (whatInputGroup->selected() == paraList.at(P0barF)){//
			Input0->setLabel(paraList.current()->text() );
			Input0->setRange(1., 80. );
			isValueSet[location0][supersonic] = yes;
			isValueSet[location0][subsonic] = yes;
			isValueSet[location0][bothsonic] = yes;
			initialValue[location0][subsonic] = 1.6 ;
			initialValue[location0][supersonic] = 1.1;
			initialValue[location0][bothsonic] = 1.5;
			selectBranch(all);
			numberOfBox = 1;
		}
		else {// do nothing 
		}
	}
	else if (subModelGroup->selected() == subParaList.at(unchokedF) ){
		Input1->setEnabled(true);
		Input1->setShown(true);
		if (whatInputGroup->selected() == paraList.at(M1fld)){//
			setInfoNotice
			("you are responsible for input value! negative results means wrong");
			//notImplementedYet("setSubInputShockDynamics inputradio 0");
			Input0->setLabel(paraList.current()->text() );
			Input0->setLabel("M1");
			Input0->setRange(0.2, 9.5 );
			Input1->setLabel("fld");
			Input1->setRange(0., 10.0 );
			isValueSet[location0][subsonic] = yes;
			isValueSet[location1][subsonic] = yes;
			isValueSet[location0][supersonic] = no;
			isValueSet[location1][supersonic] = no;
			initialValue[location0][subsonic] = 0.3;
			initialValue[location1][subsonic] = 1.3;
			numberOfBox = 2;
		}
		else if (whatInputGroup->selected() == paraList.at(M2fld)){//
			setInfoNotice
			("you are responsible for input value! negative results means wrong");
			Input0->setLabel(paraList.current()->text() );
			Input0->setLabel("M2");
			Input0->setRange(0.2, 9.5 );
			Input1->setLabel("fld");
			Input1->setRange(0., 10.0 );
			isValueSet[location0][subsonic] = yes;
			isValueSet[location1][subsonic] = yes;
			isValueSet[location0][supersonic] = no;
			isValueSet[location1][supersonic] = no;
			initialValue[location0][subsonic] = 0.7;
			initialValue[location1][subsonic] = 0.5;
			numberOfBox = 2;
		}
		else if (whatInputGroup->selected() == paraList.at(fldP2P1)){//
			setInfoNotice
			("you are responsible for input value! negative results means wrong");
			Input0->setLabel(paraList.current()->text() );
			Input0->setLabel("fld");
			Input0->setRange(0.2, 9.5 );
			Input1->setLabel("P2P1");
			Input1->setRange(0.1, 1.0 );
			isValueSet[location0][subsonic] = yes;
			isValueSet[location1][subsonic] = yes;
			isValueSet[location0][supersonic] = no;
			isValueSet[location1][supersonic] = no;
			initialValue[location0][subsonic] = 1.7;
			initialValue[location1][subsonic] = 0.5;
			numberOfBox = 2;
		}
		else if (whatInputGroup->selected() == paraList.at(M1P2P1)){//
			setInfoNotice
			("you are responsible for input value! negative results means wrong");
			Input0->setLabel(paraList.current()->text() );
			Input0->setLabel("M1");
			Input0->setRange(0.2, 0.8 );
			Input1->setLabel("P2P1");
			Input1->setRange(0., 0.9 );
			isValueSet[location0][subsonic] = yes;
			isValueSet[location1][subsonic] = yes;
			isValueSet[location0][supersonic] = no;
			isValueSet[location1][supersonic] = no;
			initialValue[location0][subsonic] = 0.4;
			initialValue[location1][subsonic] = 0.5;
			numberOfBox = 2;
		}
		else if (whatInputGroup->selected() == paraList.at(M2P2P1)){//
			setInfoNotice
			("no message");
			Input0->setLabel(paraList.current()->text() );
			Input0->setLabel("M2");
			Input0->setRange(0.2, 0.97 );
			Input1->setLabel("P2P1");
			Input1->setRange(0., 0.9 );
			isValueSet[location0][subsonic] = yes;
			isValueSet[location1][subsonic] = yes;
			isValueSet[location0][supersonic] = no;
			isValueSet[location1][supersonic] = no;
			initialValue[location0][subsonic] = 0.7;
			initialValue[location1][subsonic] = 0.3;
			numberOfBox = 2;
		}
		else {// do nothing 
		}
	}
	setInputBoxValue(numberOfBox);
};

void gdForm::setSubInput()
{
	double k ;
	k =  kInput->value();
	if (subModelGroup->selected() != 0){
		//notImplementedYet("calling clearInputBoxs 0 ");
		clearInputBoxs();
		//notImplementedYet("after clearInputBoxs");
		inputDataGroup->setEnabled(true);
		isDataChanged = true;
	}
	else {
		return;
	}
	// twice getting here net need?
	//clearInputBoxs();
	//rangeNotice->setShown(false);
	//inptType inputBox;
	if (modelGroup->selected() == radioShockDynamics){
		//cout << "we are sending to setSubInputShockDynamics" << endl;
		setSubInputShockDynamics();
	}
	else if (modelGroup->selected() == radioFanno){
		setSubInputFanno();
	}
}

void gdForm::setInputBoxValue( int inputBoxNumber)
{
	int j = 0; //j the inputBox number
	//cout << "number of box " << inputBoxNumber << endl;
	for ( inBoxIterator.toFirst() ; j < inputBoxNumber ; j++ ){
		++inBoxIterator;
		//cout << "number j = " << j << endl;
		//notImplementedYet("in setInputBoxValue just after for");
		if (inputDataGroup->selected() ==  radioSubSonic){
			//cout << "subsonic " <<   endl;
			if (isValueSet[j] [subsonic]  == no ) {
				tmpRange = inBoxList.at(j)->getRange();
				inBoxList.at(j)->setValue (0.5*(tmpRange[0]+tmpRange[1]));
			}
			else if (isValueSet[location0] [subsonic] == yes )  {
				inBoxList.at(j)->setValue ( initialValue[j][0]);
			}
		}
		else if (inputDataGroup->selected() ==  radioSuperSonic ){
			//cout << "supersonic " <<   endl;
			if (isValueSet[j][supersonic] == no ) {
				tmpRange = inBoxList.at(j)->getRange();
				//cout << "isValueSet no" << endl;
				inBoxList.at(j)->setValue (0.5*(tmpRange[0]+tmpRange[1]));
			}
			else if (isValueSet[j][supersonic] == yes )  {
				//cout << "isValueSet yes" << endl;
				//cout << "value = " << initialValue[j][1] << endl; 
				//cout << "j = " << j << endl; 
				inBoxList.at(j)->setValue(initialValue[j][1]);
				//notImplementedYet("in setInputBoxValue inBox at j");
			}
		}
		else if (inputDataGroup->selected() ==  radioBothSonic){
			//cout << "bothsonic " <<   endl;
			if (isValueSet[j][bothsonic] == no ) {
				tmpRange = inBoxList.at(j)->getRange();
				inBoxList.at(j)->setValue (0.5*(tmpRange[0]+tmpRange[1]));
			}
			else if (isValueSet[j][bothsonic] == yes )  {
				inBoxList.at(j)->setValue ( initialValue[j][2]);
			}
		}
		else {
			//cout << "default subsonic " <<   endl;
			if (isValueSet[j][subsonic] == no ) {
				tmpRange = inBoxList.at(j)->getRange();
				inBoxList.at(j)->setValue
						(0.5*(tmpRange[0]+tmpRange[1]));
			}
			else if (isValueSet[j][subsonic] == yes )  {
				inBoxList.at(j)->setValue ( initialValue[j][0]);
			}
		}
				//notImplementedYet("in setInputBoxValue before next for");
	}
}

void gdForm::setInfoNotice(QString notice ) 
{
	infoNotice->setShown(true);
	infoNotice->setEnabled(true);
	infoNotice->setText(notice);
}

void gdForm::valueCorrectionDueUnitsChange(int ) 
{
}

void gdForm::temperatureConversion( int previousUnits, double oldTemp,
			 int currentUnits, double & newTemp )
{
	double realTemp ;
	realTemp = temperatureToRealTemp ( previousUnits, oldTemp);
	newTemp = fromRealTempTemperature ( currentUnits, realTemp ); 
}

double conversionToReal (int units, int property,  double value)
{
	return  (value * conversionCoefficient[property][units]) ;
}

double temperatureToRealTemp (int units, double temp)
{
	switch ( units ) {
	case 0: 
		//"[C]";
		return  (temp + 273.15) ;
		break;
	case 1:
		//"[K]";
		return temp    ;
		break;
	case 2: 
		//"[R]";
		return (temp / 1.8) ;
		break;
	case 3: 
		//"[F]";
		return  (temp + 459.67 )  / 1.8   ;
		break;
	default:
		return 0.;
	}	
}

double fromRealTempTemperature (int units, double temp)
{
	switch ( units ) {
	case 0: 
		//"[C]";
		return  (temp - 273.15) ;
		break;
	case 1:
		//"[K]";
		return temp    ;
		break;
	case 2: 
		//"[R]";
		return (temp * 1.8) ;
		break;
	case 3: 
		//"[F]";
		return  temp  * 1.8 - 459.67 ;
		break;
	default:
		return 0.;
	}	
}
