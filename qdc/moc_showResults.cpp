/****************************************************************************
** ShowResults meta object code from reading C++ file 'showResults.h'
**
** Created: Mon Mar 26 10:19:36 2012
**      by: The Qt MOC ($Id: qt/moc_yacc.cpp   3.3.8   edited Feb 2 14:59 $)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#undef QT_NO_COMPAT
#include "showResults.h"
#include <qmetaobject.h>
#include <qapplication.h>

#include <private/qucomextra_p.h>
#if !defined(Q_MOC_OUTPUT_REVISION) || (Q_MOC_OUTPUT_REVISION != 26)
#error "This file was generated using the moc from 3.3.8b. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

const char *ShowResults::className() const
{
    return "ShowResults";
}

QMetaObject *ShowResults::metaObj = 0;
static QMetaObjectCleanUp cleanUp_ShowResults( "ShowResults", &ShowResults::staticMetaObject );

#ifndef QT_NO_TRANSLATION
QString ShowResults::tr( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "ShowResults", s, c, QApplication::DefaultCodec );
    else
	return QString::fromLatin1( s );
}
#ifndef QT_NO_TRANSLATION_UTF8
QString ShowResults::trUtf8( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "ShowResults", s, c, QApplication::UnicodeUTF8 );
    else
	return QString::fromUtf8( s );
}
#endif // QT_NO_TRANSLATION_UTF8

#endif // QT_NO_TRANSLATION

QMetaObject* ShowResults::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    QMetaObject* parentObject = QDialog::staticMetaObject();
    static const QUMethod slot_0 = {"readFromStdout", 0, 0 };
    static const QUMethod slot_1 = {"scrollToTop", 0, 0 };
    static const QUMethod slot_2 = {"clearOutput", 0, 0 };
    static const QUParameter param_slot_3[] = {
	{ "fileName", &static_QUType_QString, 0, QUParameter::In }
    };
    static const QUMethod slot_3 = {"newData", 1, param_slot_3 };
    static const QUMethod slot_4 = {"init", 0, 0 };
    static const QMetaData slot_tbl[] = {
	{ "readFromStdout()", &slot_0, QMetaData::Public },
	{ "scrollToTop()", &slot_1, QMetaData::Public },
	{ "clearOutput()", &slot_2, QMetaData::Public },
	{ "newData(QString)", &slot_3, QMetaData::Public },
	{ "init()", &slot_4, QMetaData::Private }
    };
    metaObj = QMetaObject::new_metaobject(
	"ShowResults", parentObject,
	slot_tbl, 5,
	0, 0,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    cleanUp_ShowResults.setMetaObject( metaObj );
    return metaObj;
}

void* ShowResults::qt_cast( const char* clname )
{
    if ( !qstrcmp( clname, "ShowResults" ) )
	return this;
    return QDialog::qt_cast( clname );
}

bool ShowResults::qt_invoke( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->slotOffset() ) {
    case 0: readFromStdout(); break;
    case 1: scrollToTop(); break;
    case 2: clearOutput(); break;
    case 3: newData((QString)static_QUType_QString.get(_o+1)); break;
    case 4: init(); break;
    default:
	return QDialog::qt_invoke( _id, _o );
    }
    return TRUE;
}

bool ShowResults::qt_emit( int _id, QUObject* _o )
{
    return QDialog::qt_emit(_id,_o);
}
#ifndef QT_NO_PROPERTIES

bool ShowResults::qt_property( int id, int f, QVariant* v)
{
    return QDialog::qt_property( id, f, v);
}

bool ShowResults::qt_static_property( QObject* , int , int , QVariant* ){ return FALSE; }
#endif // QT_NO_PROPERTIES
