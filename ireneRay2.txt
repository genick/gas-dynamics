

ÿþPages edited :  1 0

Edited by :  Irene dated  1  Jun  2 0 0 7

Comments :  Please check  ?



C H A P T E R  1 0

R A Y L E I G H  F L O W



Rayleigh flow is  [ - ( ]frictionless [ - ) ]
flow with heat transfer through a pip
e of constant

cross sectional area .  In practice  Ray
leigh flow is  [ +a ] really  [ -provide ]
good model for

the real situation .  [ - Yet , ]  Rayleigh
flow is  [ +also a ] practical and usefu
l concept in a obtaining

trends and limits [ - .  +such as ]  [ - The
+the ] density and pressure change due
to external cooling or

heating .  As opposed to the two previo
us models , the heat transfer can be i
n two

directions  [ -not like  +without any ]  [
-the ] friction  (there is no negative
friction ) .

This fact  [ -create  +creates a differe
nt ] situation  [ -different compare  +as
compared ] to the previous two models .

This model  [ +can be ] applied to  [ -cas
e  +cases ] where the heat transfer is
significant and the friction can be i
gnored .



/ / 1 0 . 1  Introduction



The third simple model for  [ -an ]  [ -on
e dimensional  +one -dimensional ] flow
[ -is for  +with ] constant heat transfe
r for frictionless

flow [ - .  This flow  +is ] referred in th
e literature as  Rayleigh  Flow  (see hi
storical notes ) .  This

flow is another extreme case in which
the friction effect  [ -are neglected  +
is neglible ] because  [ -their  +the ] re
lative

effect is much smaller  [ +than ] the he
at transfer effect .

While the isothermal flow model has h
eat transfer and friction [ + , ] the mai
n assumption was that relative

length  [ -is so  +enables ] the heat  [ +t
o ] transfer  [ -occurs ] between the  [ -s
urrounding  +surroundings ] and  [ +the ]
tube .  In

contrast , the heat transfer in  Raylei
gh flow occurs  [ -either ] between unkn
own  [ -temperature  +temperatures ]

[ +in the  -to ] tube and the heat flux
is  [ -maintained ] constant .  As before ,
a simple model

is built around  [ +the ] assumption of
constant properties  (poorer predictio
n to  [ +the ] case  [ -were  +where ]

chemical reaction  [ -take palace  +take
s place ] ) .

This model  [ -usage ] is  [ +used ] to  [ -h
ave a rough  +roughly ] predict the con
ditions  [ +which ] occur mostly in

situations  [ -involve  +involving a ] ch
emical reaction .



In analysis of the flow , one has to b
e aware that properties do change sig
nificantly for a large range of  [ -tem
perature  +temperatures ] .  [ - Yet , for  +
For a ]

smaller range of  [ -temperature and le
ngth  +temperatures and lengths ] the c
alculations are more accurate .  Nevert
heless ,

the main characteristic of the flow s
uch as  [ -chocking  +a choking ] conditi
on  [ -etc . are  +is ]

encapsulated in this model .



The density  [ -change though  +changes
through ] the heat transfer  (temperatu
re change ) .

[ - As appose  + Contrary ] to  Fanno flow
in which the resistance always oppose
[ -the ] the flow direction ,

[ -in ]  Rayleigh flow  [ -also  +changes t
he direction of the flow velocity acc
eleration when ] cooling  [ -can be  +is ]
applied .

[ - The flow velocity acceleration chan
ge the direction when the cooling is
applied . ]



/ / ( 1 0 . 4 )

There are four equations with four  [ -
unknown  +unknowns ] , if the upstream c
onditions are known

(or downstream  [ -condition  +condition
s ] are known ) .



The density ratio can be expressed in
[ -term  +terms ] of mass conservation a
s



/ / ( 1 0 . 6 )

Transferring the temperature ratio to
[ +the ] left hand side and squaring  [ +
the ] results  [ -in  +gives ]



Fig .  1 0 . 2 :  The  [ - Temperature  Entropy
Diagram  For  +temperature entropy diag
ram for ]  Rayleigh  Line



The  Rayleigh line exhibits two possib
le maximums one for d T =ds  =  0 and for

ds =d T  =  0 .  The second maximum can be
expressed as d T =ds  =  1 [ + . ]  The second

law is used to find the expression fo
r  [ +the ] derivative .



Let the initial condition  M 1 , and s 1
[ -are  +be ] constant  [ -then  +and ] the
variable parameters are

M 2 , and s 2 .  A derivative of equation
( 1 0 . 1 1 ) results in





Take the derivative of  [ -the ] equatio
n  ( 1 0 . 1 2 )  [ -when letting  +and let ] th
e variable parameters

be  T 2 [ - , ] and  M 2 [ + , ] results in



Hence , when cooling  [ +is ] applied to
a tube the velocity decreases and  [ +w
hen ] heating  [ +is ] applied the veloci
ty

increases .  [ - The  + At the ]  [ -peculiars
+peculiar ] point of  M  =  1 =pk when add
itional heat is applied the

temperature  [ -is decreasing  +decrease
s ] .



The  [ -chocking  +choking ]  [ -is occurre
d  +occurs ] only when  M  =  1 because it
[ -violate  +violates ] the second law .

The transition to  [ -supper sonic  +sup
ersonic ] flow occurs when the area ch
anges ,  [ -some what  +somewhat ] similar
ly to  Fanno flow [ - , + . ]

Yet ,  [ -chocking  +choking ] can be expl
ained by the fact  [ +that ] increase of
energy must  [ +be ] accompanied by  [ +an
] increase of entropy .

But the entropy of supersonic flow is
lower  (see  [ -the ]  Figure  [ - ( 1 0 . 2 )  + 1 0
. 2 ) ]  [ ?and therefore it is not possib
le ? ]

(the maximum entropy at  M  =  1 [ - . ] ) .



It is  [ -convent  +convenient ] to  [ -ref
errers  +refer ] to the value of  M  =  1 .
These  [ -value  +values ]  [ +are ] referre
d  [ +to ] as the

star  1 values .  The equation  ( 1 0 . 5 ) c
an be written between  [ -chocking  +cho
king ] point and any

point on the curve .



/ / 1 0 . 3  Rayleigh  Flow  Tables

The  star  values are tabulated in  Ta
ble  [ - ( 1 0 . 1 )  + 1 0 . 1 ] .





The  [ - Data  +data ] is presented in  Fig
ure  [ - ( 1 0 . 3 )  + 1 0 . 3 ] .



/ / 1 0 . 4  Examples for  Rayleigh  Flow

The typical questions that  [ +are ] rai
sed in  Rayleigh  Flow are related to t
he maximum

heat that can be  [ -transfered  +transf
erred ] to gas  (reaction heat ) and to
[ +the ] flow rate .



/ / Example  1 0 . 1 :

Due  [ +to ] internal combustion [ + , ] hea
t was released and the exit temperatu
re

was found to be  1 2 7  C .  [ - Calculated  +
Calculate ] the exit  Mach number , the
exit pressure , the

total exit pressure , and heat release
d  [ - (transfered )  +transferred ] to the
air .  [ - After  + At ] what amount of

energy the exit temperature will star
t to decrease ?



/ / S O L U T I O N

The entrance  Mach number and the exit
temperature are given and from  [ -the ]
Table

1 0 . 1 or from the program the initial
ratio can be calculated .  From the ini
tial values

the ratio at the exit can be computed
as  [ +the ] following [ - . ]







The heat  [ -release  +released ]  (heat  [
-transfer  +transferred ] ) can be calcu
lated from obtaining the stagnation

temperature  [ -form the  +from ] both si
des .  The stagnation temperature at th
e entrance ,  T 0 1



The maximum temperature occurs at the
point where the  Mach number reaches

1 =pk and at this point the  Rayleigh r
elationship are :



The maximum heat before the temperatu
re can be calculated  [ +is ] as  [ -follo
wing  +follows ] :



/ / Example  1 0 . 2 :

[ - Calculated  + Calculate ] the entrance
temperature and the entrance  Mach num
ber .





The solution  [ -involve  +involves ] fin
ding the stagnation temperature at th
e exit and  [ -subtraction  +subtracting
]

[ -of ] the heat  (heat equation ) to obt
ain the entrance stagnation temperatu
re .  From

the  Table  [ - ( 1 0 . 1 )  + 1 0 . 1 ] or from the
Potto - G D C the following ratios can be
obtained .



It must be noted that  T 0 2  =  T 0  .  Ther
efore with  T 0 1

T 0   =  0 : 5 0 1 6 either by  [ +using ]  Table
[ - ( 1 0 . 1 )  + 1 0 . 1 ]

or by  Potto - G D C the following is obta
ined





/ / Example  1 0 . 3 :

What is the maximum heat that can be
[ -add  +added ] so  [ +that ] there is no
subsonic flow [ - .  + ? ]

If a shock  [ -is ] occurs immediately a
t the entrance [ + , ] what is the maximu
m heat that can be added ?



To achieve maximum heat transfer [ + , ]
the exit  Mach number has to be one ,  M
2  =  1 .



The table for  M  =  3  [ +is ] as  [ -follow
ing  +follows ]



In subsonic branch the  Mach number is
after the shock  [ -is ]



With  Mach number of M  =  0 : 4 7 5 1 9 the ma
ximum heat transfer requires informat
ion [ -from  +for ]  Rayleigh flow as  [ +the ]
following



It also must be noticed that stagnati
on temperature remains constant acros
s  [ +the ] shock

wave .



It is not surprising  [ -since the  +for
] the shock wave  [ -are  +to be ] found
[ -on the  +in ]  Rayleigh flow .


