\contentsline {chapter}{Nomenclature}{xix}{Doc-Start}
\contentsline {section}{GNU Free Documentation License}{xxxi}{section*.71}
\contentsline {subsection}{1. APPLICABILITY AND DEFINITIONS}{xxxii}{section*.72}
\contentsline {subsection}{2. VERBATIM COPYING}{xxxiii}{section*.72}
\contentsline {subsection}{3. COPYING IN QUANTITY}{xxxiii}{section*.72}
\contentsline {subsection}{4. MODIFICATIONS}{xxxiv}{section*.72}
\contentsline {subsection}{5. COMBINING DOCUMENTS}{xxxvi}{section*.72}
\contentsline {subsection}{6. COLLECTIONS OF DOCUMENTS}{xxxvi}{section*.72}
\contentsline {subsection}{7. AGGREGATION WITH INDEPENDENT WORKS}{xxxvii}{section*.72}
\contentsline {subsection}{8. TRANSLATION}{xxxvii}{section*.72}
\contentsline {subsection}{9. TERMINATION}{xxxvii}{section*.72}
\contentsline {subsection}{10. FUTURE REVISIONS OF THIS LICENSE}{xxxvii}{section*.72}
\contentsline {subsection}{ADDENDUM: How to use this License for your documents}{xxxviii}{section*.72}
\contentsline {section}{ How to contribute to this book }{xxxix}{section*.73}
\contentsline {section}{ Credits }{xxxix}{section*.74}
\contentsline {subsection}{John Martones }{xxxix}{section*.75}
\contentsline {subsection}{Grigory Toker }{xl}{section*.76}
\contentsline {subsection}{Ralph Menikoff }{xl}{section*.77}
\contentsline {subsection}{Domitien Rataaforret }{xl}{section*.78}
\contentsline {subsection}{Gary Settles }{xl}{section*.79}
\contentsline {subsection}{Your name here }{xl}{section*.80}
\contentsline {subsection}{Typo corrections and "minor" contributions }{xli}{section*.81}
\contentsline {section}{Version 0.4.9 pp. ? Feb ?, 2012}{li}{section*.82}
\contentsline {section}{Version 0.4.8.5a . July 21, 2009}{li}{section*.84}
\contentsline {section}{Version 0.4.8 Jan. 23, 2008}{lii}{section*.86}
\contentsline {section}{Version 0.4.3 Sep. 15, 2006}{lii}{section*.87}
\contentsline {section}{Version 0.4.2}{liii}{section*.88}
\contentsline {section}{Version 0.4}{liii}{section*.89}
\contentsline {section}{Version 0.3}{liii}{section*.90}
\contentsline {section}{Version 0.5}{lix}{section*.91}
\contentsline {section}{Version 0.4.3}{lx}{section*.92}
\contentsline {section}{Version 0.4.1.7}{lx}{section*.93}
\contentsline {subsection}{Speed of Sound [beta]}{lxiv}{section*.94}
\contentsline {subsection}{Stagnation effects [advance] }{lxiv}{section*.95}
\contentsline {subsection}{Nozzle [advance]}{lxiv}{section*.96}
\contentsline {subsection}{Normal Shock [advance]}{lxiv}{section*.97}
\contentsline {subsection}{Minor Loss [NSV]}{lxv}{section*.98}
\contentsline {subsection}{Isothermal Flow [advance]}{lxv}{section*.99}
\contentsline {subsection}{Fanno Flow [advance]}{lxv}{section*.100}
\contentsline {subsection}{Rayleigh Flow [beta]}{lxv}{section*.101}
\contentsline {subsection}{Add mass [NSY]}{lxv}{section*.102}
\contentsline {subsection}{Evacuation and filling semi rigid Chambers [alpha]}{lxv}{section*.103}
\contentsline {subsection}{Evacuating and filling chambers under external forces [alpha]}{lxvi}{section*.104}
\contentsline {subsection}{Oblique Shock [advance]}{lxvi}{section*.105}
\contentsline {subsection}{Prandtl--Meyer}{lxvi}{section*.106}
\contentsline {subsection}{Transient problem [NYP]}{lxvi}{section*.107}
\contentsline {subsection}{General 1-D flow [NYP]}{lxvi}{section*.108}
\contentsline {chapter}{\numberline {1}Introduction }{1}{chapter.1}
\contentsline {section}{\numberline {1.1}What is Compressible Flow? }{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Why Compressible Flow is Important? }{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Historical Background }{2}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Early Developments }{4}{subsection.1.3.1}
\contentsline {subsubsection}{Speed of Sound }{4}{section*.109}
\contentsline {subsection}{\numberline {1.3.2}The shock wave puzzle }{5}{subsection.1.3.2}
\contentsline {subsection}{\numberline {1.3.3}Choking Flow }{8}{subsection.1.3.3}
\contentsline {subsubsection}{Nozzle Flow }{10}{section*.110}
\contentsline {subsubsection}{Rayleigh Flow }{11}{section*.111}
\contentsline {subsubsection}{Fanno Flow }{11}{section*.112}
\contentsline {subsubsection}{Isothermal Flow }{12}{section*.113}
\contentsline {subsection}{\numberline {1.3.4}External flow }{12}{subsection.1.3.4}
\contentsline {subsection}{\numberline {1.3.5}Filling and Evacuating Gaseous Chambers }{13}{subsection.1.3.5}
\contentsline {subsection}{\numberline {1.3.6}Biographies of Major Figures }{14}{subsection.1.3.6}
\contentsline {subsubsection}{Galileo Galilei }{14}{section*.114}
\contentsline {subsubsection}{Ernest Mach (1838-1916) }{15}{section*.115}
\contentsline {subsubsection}{John William Strutt (Lord Rayleigh) }{16}{section*.116}
\contentsline {subsubsection}{William John Macquorn Rankine }{17}{section*.117}
\contentsline {subsubsection}{Gino Girolamo Fanno }{17}{section*.118}
\contentsline {subsubsection}{Ludwig Prandtl }{18}{section*.119}
\contentsline {subsubsection}{Theodor Meyer }{19}{section*.120}
\contentsline {subsubsection}{E.R.G.\nobreakspace {}Eckert }{21}{section*.121}
\contentsline {subsubsection}{Ascher Shapiro }{22}{section*.122}
\contentsline {subsubsection}{Theodor Von Karman }{22}{section*.123}
\contentsline {subsubsection}{Zeldovich, Yakov Borisovich }{23}{section*.124}
\contentsline {chapter}{\numberline {2}Review of Thermodynamics }{25}{chapter.2}
\contentsline {section}{\numberline {2.1}Basic Definitions }{25}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Maximum Work of Expansion Process }{33}{subsection.2.1.1}
\contentsline {section}{\numberline {2.2}The Velocity--Temperature Diagram }{35}{section.2.2}
\contentsline {chapter}{\numberline {3}Basic of Fluid Mechanics }{39}{chapter.3}
\contentsline {section}{\numberline {3.1}Introduction}{39}{section.3.1}
\contentsline {section}{\numberline {3.2}Fluid Properties}{39}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Kinds of Fluids}{39}{subsection.3.2.1}
\contentsline {subsubsection}{Density}{40}{section*.125}
\contentsline {subsection}{\numberline {3.2.2}Viscosity}{40}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Kinematic Viscosity}{41}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Bulk Modulus}{41}{subsection.3.2.4}
\contentsline {section}{\numberline {3.3}Mass Conservation}{43}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Control Volume}{43}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Continuity Equation}{44}{subsection.3.3.2}
\contentsline {subsubsection}{One--Dimensional Control Volume}{47}{section*.126}
\contentsline {subsection}{\numberline {3.3.3}Reynolds Transport Theorem}{47}{subsection.3.3.3}
\contentsline {section}{\numberline {3.4}Momentum Conservation}{52}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Momentum Governing Equation}{53}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Conservation Moment of Momentum}{54}{subsection.3.4.2}
\contentsline {section}{\numberline {3.5}Energy Conservation}{55}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Approximation of Energy Equation}{58}{subsection.3.5.1}
\contentsline {subsubsection}{Energy Equation in Frictionless Flow and Steady State}{59}{section*.127}
\contentsline {section}{\numberline {3.6}Limitations of Integral Approach}{62}{section.3.6}
\contentsline {section}{\numberline {3.7}Differential Analysis}{62}{section.3.7}
\contentsline {subsection}{\numberline {3.7.1}Mass Conservation}{63}{subsection.3.7.1}
\contentsline {subsection}{\numberline {3.7.2}Momentum Equations or N--S equations}{64}{subsection.3.7.2}
\contentsline {subsection}{\numberline {3.7.3}Boundary Conditions and Driving Forces}{66}{subsection.3.7.3}
\contentsline {subsubsection}{Boundary Conditions Categories}{66}{section*.128}
\contentsline {chapter}{\numberline {4}Speed of Sound}{67}{chapter.4}
\contentsline {section}{\numberline {4.1}Motivation}{67}{section.4.1}
\contentsline {section}{\numberline {4.2}Introduction}{67}{section.4.2}
\contentsline {section}{\numberline {4.3}Speed of Sound in Ideal and Perfect Gases}{69}{section.4.3}
\contentsline {section}{\numberline {4.4}Speed of Sound in Real Gases }{72}{section.4.4}
\contentsline {section}{\numberline {4.5}Speed of Sound in Almost Incompressible Liquid}{75}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Sound in Variable Compressible Liquids}{77}{subsection.4.5.1}
\contentsline {section}{\numberline {4.6}Speed of Sound in Solids}{80}{section.4.6}
\contentsline {section}{\numberline {4.7}Sound Speed in Two Phase Medium}{82}{section.4.7}
\contentsline {section}{\numberline {4.8}The Dimensional Effect of the Speed of Sound}{84}{section.4.8}
\contentsline {subsection}{\numberline {4.8.1}Doppler Effect}{86}{subsection.4.8.1}
\contentsline {subsection}{\numberline {4.8.2}Acoustic Wave Equation -- Derivation}{88}{subsection.4.8.2}
\contentsline {subsection}{\numberline {4.8.3}Hearing and Sound Waves}{91}{subsection.4.8.3}
\contentsline {subsection}{\numberline {4.8.4}Sound Wave in Three Dimensions}{93}{subsection.4.8.4}
\contentsline {chapter}{\numberline {5}Isentropic Flow}{99}{chapter.5}
\contentsline {section}{\numberline {5.1}Stagnation State for Ideal Gas Model}{99}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}General Relationship}{99}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Relationships for Small Mach Number}{102}{subsection.5.1.2}
\contentsline {section}{\numberline {5.2}Isentropic Converging-Diverging Flow in Cross Section}{103}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}The Properties in the Adiabatic Nozzle}{104}{subsection.5.2.1}
\contentsline {subsubsection}{The pressure Mach number relationship}{105}{section*.129}
\contentsline {subsubsection}{Relationship Between the Mach Number and Cross Section Area}{107}{section*.130}
\contentsline {subsection}{\numberline {5.2.2}Isentropic Flow Examples}{108}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Mass Flow Rate (Number) }{111}{subsection.5.2.3}
\contentsline {subsubsection}{``Naughty Professor'' Problems in Isentropic Flow}{113}{section*.131}
\contentsline {subsubsection}{Flow with pressure losses}{122}{section*.132}
\contentsline {section}{\numberline {5.3}Isentropic Tables}{123}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1} Isentropic Isothermal Flow Nozzle}{125}{subsection.5.3.1}
\contentsline {subsubsection}{General Relationship}{125}{section*.133}
\contentsline {section}{\numberline {5.4}The Impulse Function}{133}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Impulse in Isentropic Adiabatic Nozzle }{133}{subsection.5.4.1}
\contentsline {subsection}{\numberline {5.4.2}The Impulse Function in Isothermal Nozzle}{136}{subsection.5.4.2}
\contentsline {section}{\numberline {5.5}Isothermal Table}{136}{section.5.5}
\contentsline {section}{\numberline {5.6}The effects of Real Gases}{138}{section.5.6}
\contentsline {section}{\numberline {5.7}Isentropic Relationships for Real Gases}{143}{section.5.7}
\contentsline {chapter}{\numberline {6}Normal Shock }{145}{chapter.6}
\contentsline {section}{\numberline {6.1}Solution of the Governing Equations}{147}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Informal Model}{147}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}Formal Model}{148}{subsection.6.1.2}
\contentsline {subsubsection}{The Maximum Conditions}{152}{section*.134}
\contentsline {subsubsection}{The Star Conditions }{152}{section*.135}
\contentsline {subsection}{\numberline {6.1.3}Prandtl's Condition}{152}{subsection.6.1.3}
\contentsline {section}{\numberline {6.2}Operating Equations and Analysis }{153}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}The Limitations of the Shock Wave}{154}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}Small Perturbation Solution}{154}{subsection.6.2.2}
\contentsline {subsection}{\numberline {6.2.3}Shock Thickness}{155}{subsection.6.2.3}
\contentsline {subsection}{\numberline {6.2.4}Shock Drag or Wave Drag}{155}{subsection.6.2.4}
\contentsline {section}{\numberline {6.3}The Moving Shocks}{156}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Shock or Wave Drag Result from a Moving Shock}{159}{subsection.6.3.1}
\contentsline {subsection}{\numberline {6.3.2}Shock Result from a Sudden and Complete Stop}{161}{subsection.6.3.2}
\contentsline {subsection}{\numberline {6.3.3}Moving Shock into Stationary Medium (Suddenly Open Valve)}{163}{subsection.6.3.3}
\contentsline {subsubsection}{General Velocities Issues}{163}{section*.136}
\contentsline {paragraph}{Piston Velocity}{165}{section*.137}
\contentsline {subsubsection}{Shock--Choke Phenomenon}{168}{section*.138}
\contentsline {subsubsection}{Moving Shock in Two and Three Dimensions}{171}{section*.139}
\contentsline {subsection}{\numberline {6.3.4}Partially Open Valve}{174}{subsection.6.3.4}
\contentsline {subsubsection}{Given Initial Mach Numbers}{174}{section*.140}
\contentsline {subsubsection}{Given Actual Velocities}{175}{section*.141}
\contentsline {subsection}{\numberline {6.3.5}Partially Closed Valve}{176}{subsection.6.3.5}
\contentsline {subsection}{\numberline {6.3.6}Worked--out Examples for Shock Dynamics}{177}{subsection.6.3.6}
\contentsline {section}{\numberline {6.4}Shock Tube}{184}{section.6.4}
\contentsline {subsection}{\numberline {6.4.1}Special Shock Dynamics}{194}{subsection.6.4.1}
\contentsline {subsection}{\numberline {6.4.2}Shock Tube Thermodynamics Considerations}{197}{subsection.6.4.2}
\contentsline {section}{\numberline {6.5}Shock with Real Gases}{208}{section.6.5}
\contentsline {section}{\numberline {6.6}Shock in Wet Steam}{208}{section.6.6}
\contentsline {section}{\numberline {6.7}Normal Shock in Ducts}{208}{section.6.7}
\contentsline {section}{\numberline {6.8}Additional Examples for Moving Shocks}{208}{section.6.8}
\contentsline {section}{\numberline {6.9}Tables of Normal Shocks, $k=1.4$ Ideal Gas}{210}{section.6.9}
\contentsline {chapter}{\numberline {7}Normal Shock in Variable Duct Areas }{219}{chapter.7}
\contentsline {section}{\numberline {7.1}Nozzle efficiency}{225}{section.7.1}
\contentsline {section}{\numberline {7.2}Diffuser Efficiency}{226}{section.7.2}
\contentsline {chapter}{\numberline {8}Nozzle Flow With External Forces}{237}{chapter.8}
\contentsline {section}{\numberline {8.1}Isentropic Nozzle ($Q=0$)}{238}{section.8.1}
\contentsline {section}{\numberline {8.2}Isothermal Nozzle $(T=constant)$}{240}{section.8.2}
\contentsline {chapter}{\numberline {9}Isothermal Flow}{241}{chapter.9}
\contentsline {section}{\numberline {9.1}The Control Volume Analysis/Governing equations}{242}{section.9.1}
\contentsline {section}{\numberline {9.2}Dimensionless Representation }{242}{section.9.2}
\contentsline {section}{\numberline {9.3}The Entrance Limitation of Supersonic Branch}{247}{section.9.3}
\contentsline {section}{\numberline {9.4}Comparison with Incompressible Flow}{248}{section.9.4}
\contentsline {section}{\numberline {9.5}Supersonic Branch}{250}{section.9.5}
\contentsline {section}{\numberline {9.6}Figures and Tables}{251}{section.9.6}
\contentsline {section}{\numberline {9.7}Isothermal Flow Examples}{252}{section.9.7}
\contentsline {section}{\numberline {9.8}Unchoked Situations in Fanno Flow}{257}{section.9.8}
\contentsline {subsection}{\numberline {9.8.1}Reynolds Number Effect}{257}{subsection.9.8.1}
\contentsline {chapter}{\numberline {10} Fanno Flow}{261}{chapter.10}
\contentsline {section}{\numberline {10.1}Introduction}{261}{section.10.1}
\contentsline {section}{\numberline {10.2}Fanno Model}{262}{section.10.2}
\contentsline {section}{\numberline {10.3}Non--Dimensionalization of the Equations}{263}{section.10.3}
\contentsline {section}{\numberline {10.4}The Mechanics and Why the Flow is Choked?}{266}{section.10.4}
\contentsline {subsubsection}{Why the flow is choked?}{266}{section*.143}
\contentsline {subsubsection}{The Trends}{266}{section*.144}
\contentsline {section}{\numberline {10.5}The Working Equations}{267}{section.10.5}
\contentsline {section}{\numberline {10.6}Examples of Fanno Flow}{271}{section.10.6}
\contentsline {section}{\numberline {10.7}Supersonic Branch}{277}{section.10.7}
\contentsline {section}{\numberline {10.8}Maximum Length for the Supersonic Flow}{277}{section.10.8}
\contentsline {section}{\numberline {10.9}Working Conditions}{278}{section.10.9}
\contentsline {subsection}{\numberline {10.9.1}Variations of The Tube Length ($\unhbox \voidb@x \hbox {$\frac {4fL}{D}$}$) Effects}{279}{subsection.10.9.1}
\contentsline {subsubsection}{Fanno Flow Subsonic branch}{279}{section*.145}
\contentsline {subsubsection}{Fanno Flow Supersonic Branch}{279}{section*.146}
\contentsline {subsection}{\numberline {10.9.2}The Pressure Ratio, $\left .{P_2 }\right /{ P_1}$, effects}{283}{subsection.10.9.2}
\contentsline {subsubsection}{Choking explanation for pressure variation/reduction}{284}{section*.147}
\contentsline {subsubsection}{Short $\left .{4\tmspace +\thinmuskip {.1667em}f\tmspace +\thinmuskip {.1667em}L}\right /{D}$}{285}{section*.148}
\contentsline {subsubsection}{Long $\genfrac {}{}{}1{4\tmspace +\thinmuskip {.1667em}f\tmspace +\thinmuskip {.1667em}L}{D}$}{285}{section*.149}
\contentsline {subsection}{\numberline {10.9.3}Entrance Mach number, $M_1$, effects}{286}{subsection.10.9.3}
\contentsline {section}{\numberline {10.10}Practical Examples for Subsonic Flow}{291}{section.10.10}
\contentsline {subsection}{\numberline {10.10.1}Subsonic Fanno Flow for Given $\frac {4\tmspace +\thinmuskip {.1667em}f\tmspace +\thinmuskip {.1667em}L}{D}$ and Pressure Ratio}{292}{subsection.10.10.1}
\contentsline {subsection}{\numberline {10.10.2}Subsonic Fanno Flow for a Given $M_1$ and Pressure Ratio}{295}{subsection.10.10.2}
\contentsline {section}{\numberline {10.11}The Approximation of the Fanno Flow by Isothermal Flow}{298}{section.10.11}
\contentsline {section}{\numberline {10.12}$\tmspace +\thinmuskip {.1667em}$ More Examples of Fanno Flow}{299}{section.10.12}
\contentsline {section}{\numberline {10.13}$\tmspace +\thinmuskip {.1667em}$ The Table for Fanno Flow}{300}{section.10.13}
\contentsline {section}{\numberline {10.14}$\tmspace +\thinmuskip {.1667em}$ Appendix -- Reynolds Number Effects}{302}{section.10.14}
\contentsline {chapter}{\numberline {11}Rayleigh Flow}{305}{chapter.11}
\contentsline {section}{\numberline {11.1}Introduction}{305}{section.11.1}
\contentsline {section}{\numberline {11.2}Governing Equations}{306}{section.11.2}
\contentsline {section}{\numberline {11.3}Rayleigh Flow Tables}{309}{section.11.3}
\contentsline {section}{\numberline {11.4}Examples For Rayleigh Flow }{313}{section.11.4}
\contentsline {chapter}{\numberline {12}Evacuating SemiRigid Chambers}{319}{chapter.12}
\contentsline {section}{\numberline {12.1}Governing Equations and Assumptions}{320}{section.12.1}
\contentsline {section}{\numberline {12.2}General Model and Non--Dimensionalization}{322}{section.12.2}
\contentsline {subsection}{\numberline {12.2.1}Isentropic Process}{324}{subsection.12.2.1}
\contentsline {subsection}{\numberline {12.2.2}Isothermal Process in The Chamber}{325}{subsection.12.2.2}
\contentsline {subsection}{\numberline {12.2.3}A Note on the Entrance Mach number}{325}{subsection.12.2.3}
\contentsline {section}{\numberline {12.3}Rigid Tank with A Nozzle}{325}{section.12.3}
\contentsline {subsection}{\numberline {12.3.1}Adiabatic Isentropic Nozzle Attached}{326}{subsection.12.3.1}
\contentsline {subsubsection}{Filling/Evacuating The Chamber Under choked Condition}{327}{section*.150}
\contentsline {subsection}{\numberline {12.3.2}Isothermal Nozzle Attached}{327}{subsection.12.3.2}
\contentsline {section}{\numberline {12.4}Rapid evacuating of a rigid tank }{328}{section.12.4}
\contentsline {subsection}{\numberline {12.4.1}Assuming Fanno Flow Model}{328}{subsection.12.4.1}
\contentsline {subsection}{\numberline {12.4.2}Filling Process}{330}{subsection.12.4.2}
\contentsline {subsection}{\numberline {12.4.3}The Isothermal Process}{330}{subsection.12.4.3}
\contentsline {subsection}{\numberline {12.4.4}Simple Semi Rigid Chamber}{331}{subsection.12.4.4}
\contentsline {subsection}{\numberline {12.4.5}The ``Simple'' General Case}{332}{subsection.12.4.5}
\contentsline {section}{\numberline {12.5}Advance Topics}{333}{section.12.5}
\contentsline {section}{\numberline {12.6}Remark on Real Gases}{334}{section.12.6}
\contentsline {chapter}{\numberline {13}Evacuating under External Volume Control}{335}{chapter.13}
\contentsline {section}{\numberline {13.1}General Model}{335}{section.13.1}
\contentsline {subsection}{\numberline {13.1.1}Rapid Process}{336}{subsection.13.1.1}
\contentsline {subsection}{\numberline {13.1.2}Examples}{339}{subsection.13.1.2}
\contentsline {subsection}{\numberline {13.1.3}Direct Connection}{339}{subsection.13.1.3}
\contentsline {section}{\numberline {13.2}Non--Linear Functions Effects}{340}{section.13.2}
\contentsline {section}{\numberline {13.3}Summary}{340}{section.13.3}
\contentsline {chapter}{\numberline {14}Oblique Shock}{345}{chapter.14}
\contentsline {section}{\numberline {14.1}Preface to Oblique Shock}{345}{section.14.1}
\contentsline {section}{\numberline {14.2}Introduction}{346}{section.14.2}
\contentsline {subsection}{\numberline {14.2.1}Introduction to Oblique Shock}{346}{subsection.14.2.1}
\contentsline {subsection}{\numberline {14.2.2}Introduction to Prandtl--Meyer Function}{346}{subsection.14.2.2}
\contentsline {subsection}{\numberline {14.2.3}Introduction to Zero Inclination}{347}{subsection.14.2.3}
\contentsline {section}{\numberline {14.3}Oblique Shock}{347}{section.14.3}
\contentsline {section}{\numberline {14.4}Solution of Mach Angle}{350}{section.14.4}
\contentsline {subsection}{\numberline {14.4.1}Upstream Mach Number, $M_1$, and Deflection Angle, $\delta $}{350}{subsection.14.4.1}
\contentsline {subsection}{\numberline {14.4.2}When No Oblique Shock Exist or the case of $D>0$}{353}{subsection.14.4.2}
\contentsline {subsubsection}{Large deflection angle for given, $M_1$}{353}{section*.151}
\contentsline {subsubsection}{The case of $D\geq 0$ or $0 \geq \delta $}{358}{section*.152}
\contentsline {subsection}{\numberline {14.4.3}Upstream Mach Number, $M_1$, and Shock Angle, $\theta $}{361}{subsection.14.4.3}
\contentsline {subsection}{\numberline {14.4.4}Given Two Angles, $\delta $ and $\theta $ }{363}{subsection.14.4.4}
\contentsline {subsection}{\numberline {14.4.5}Flow in a Semi--2D Shape}{364}{subsection.14.4.5}
\contentsline {subsection}{\numberline {14.4.6}Flow in a Semi-2D Shape}{365}{subsection.14.4.6}
\contentsline {subsection}{\numberline {14.4.7}Small $\delta $ ``Weak Oblique shock''}{366}{subsection.14.4.7}
\contentsline {subsection}{\numberline {14.4.8}Close and Far Views of the Oblique Shock}{367}{subsection.14.4.8}
\contentsline {subsection}{\numberline {14.4.9}Maximum Value of Oblique shock}{367}{subsection.14.4.9}
\contentsline {section}{\numberline {14.5}Detached Shock}{368}{section.14.5}
\contentsline {subsection}{\numberline {14.5.1}Issues Related to the Maximum Deflection Angle}{369}{subsection.14.5.1}
\contentsline {subsection}{\numberline {14.5.2}Oblique Shock Examples}{371}{subsection.14.5.2}
\contentsline {subsection}{\numberline {14.5.3}Application of Oblique Shock}{372}{subsection.14.5.3}
\contentsline {subsection}{\numberline {14.5.4}Optimization of Suction Section Design}{385}{subsection.14.5.4}
\contentsline {subsection}{\numberline {14.5.5}Retouch of Shock Drag or Wave Drag}{385}{subsection.14.5.5}
\contentsline {section}{\numberline {14.6}Summary}{386}{section.14.6}
\contentsline {section}{\numberline {14.7}Appendix: Oblique Shock Stability Analysis}{386}{section.14.7}
\contentsline {chapter}{\numberline {15}Oblique Shock}{389}{chapter.15}
\contentsline {section}{\numberline {15.1}Preface to Oblique Shock}{389}{section.15.1}
\contentsline {section}{\numberline {15.2}Introduction}{390}{section.15.2}
\contentsline {subsection}{\numberline {15.2.1}Introduction to Oblique Shock}{390}{subsection.15.2.1}
\contentsline {subsection}{\numberline {15.2.2}Introduction to Prandtl--Meyer Function}{390}{subsection.15.2.2}
\contentsline {subsection}{\numberline {15.2.3}Introduction to Zero Inclination}{391}{subsection.15.2.3}
\contentsline {section}{\numberline {15.3}Oblique Shock}{391}{section.15.3}
\contentsline {section}{\numberline {15.4}Solution of Mach Angle}{394}{section.15.4}
\contentsline {subsection}{\numberline {15.4.1}Upstream Mach Number, $M_1$, and Deflection Angle, $\delta $}{394}{subsection.15.4.1}
\contentsline {subsection}{\numberline {15.4.2}When No Oblique Shock Exist or the case of $D>0$}{397}{subsection.15.4.2}
\contentsline {subsubsection}{Large deflection angle for given, $M_1$}{397}{section*.153}
\contentsline {subsubsection}{The case of $D\geq 0$ or $0 \geq \delta $}{402}{section*.154}
\contentsline {subsection}{\numberline {15.4.3}Upstream Mach Number, $M_1$, and Shock Angle, $\theta $}{405}{subsection.15.4.3}
\contentsline {subsection}{\numberline {15.4.4}Given Two Angles, $\delta $ and $\theta $ }{407}{subsection.15.4.4}
\contentsline {subsection}{\numberline {15.4.5}Flow in a Semi--2D Shape}{408}{subsection.15.4.5}
\contentsline {subsection}{\numberline {15.4.6}Flow in a Semi-2D Shape}{409}{subsection.15.4.6}
\contentsline {subsection}{\numberline {15.4.7}Small $\delta $ ``Weak Oblique shock''}{410}{subsection.15.4.7}
\contentsline {subsection}{\numberline {15.4.8}Close and Far Views of the Oblique Shock}{411}{subsection.15.4.8}
\contentsline {subsection}{\numberline {15.4.9}Maximum Value of Oblique shock}{411}{subsection.15.4.9}
\contentsline {section}{\numberline {15.5}Detached Shock}{412}{section.15.5}
\contentsline {subsection}{\numberline {15.5.1}Issues Related to the Maximum Deflection Angle}{413}{subsection.15.5.1}
\contentsline {subsection}{\numberline {15.5.2}Oblique Shock Examples}{415}{subsection.15.5.2}
\contentsline {subsection}{\numberline {15.5.3}Application of Oblique Shock}{416}{subsection.15.5.3}
\contentsline {subsection}{\numberline {15.5.4}Optimization of Suction Section Design}{429}{subsection.15.5.4}
\contentsline {subsection}{\numberline {15.5.5}Retouch of Shock Drag or Wave Drag}{429}{subsection.15.5.5}
\contentsline {section}{\numberline {15.6}Summary}{430}{section.15.6}
\contentsline {section}{\numberline {15.7}Appendix: Oblique Shock Stability Analysis}{430}{section.15.7}
\contentsline {chapter}{\numberline {16}Prandtl-Meyer Function}{433}{chapter.16}
\contentsline {section}{\numberline {16.1}Introduction}{433}{section.16.1}
\contentsline {section}{\numberline {16.2}Geometrical Explanation}{434}{section.16.2}
\contentsline {subsection}{\numberline {16.2.1}Alternative Approach to Governing Equations}{435}{subsection.16.2.1}
\contentsline {subsection}{\numberline {16.2.2}Comparison And Limitations between the Two Approaches}{439}{subsection.16.2.2}
\contentsline {section}{\numberline {16.3}The Maximum Turning Angle}{439}{section.16.3}
\contentsline {section}{\numberline {16.4}The Working Equations for the Prandtl-Meyer Function}{440}{section.16.4}
\contentsline {section}{\numberline {16.5}d'Alembert's Paradox }{440}{section.16.5}
\contentsline {section}{\numberline {16.6}Flat Body with an Angle of Attack}{441}{section.16.6}
\contentsline {section}{\numberline {16.7}Examples For Prandtl--Meyer Function }{441}{section.16.7}
\contentsline {section}{\numberline {16.8}Combination of the Oblique Shock and Isentropic Expansion}{443}{section.16.8}
\contentsline {chapter}{\numberline {A}Computer Program}{447}{appendix.A}
\contentsline {section}{\numberline {A.1}About the Program}{447}{section.A.1}
\contentsline {section}{\numberline {A.2}Usage}{447}{section.A.2}
\contentsline {section}{\numberline {A.3}Program listings}{449}{section.A.3}
\contentsline {chapter}{\numberline {B}Oblique Shock History }{451}{appendix.B}
\contentsline {chapter}{Index}{455}{Item.110}
\contentsline {section}{Subjects Index}{455}{section*.155}
\contentsline {section}{Authors Index}{458}{section*.155}
