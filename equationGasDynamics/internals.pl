# LaTeX2HTML 2002-2-1 (1.70)
# Associate internals original text with physical files.


$key = q/variableArea:eq:NetForce/;
$ref_files{$key} = "$dir".q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:a1/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:Tbar/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:x2/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:P0ratio/;
$ref_files{$key} = "$dir".q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:densityMx/;
$ref_files{$key} = "$dir".q|node11.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:entropySolution/;
$ref_files{$key} = "$dir".q|node13.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:solidExampl/;
$ref_files{$key} = "$dir".q|node6.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:speedSoundNonIdealGas/;
$ref_files{$key} = "$dir".q|node4.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:x1/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:thetaMax/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:nuTheta/;
$ref_files{$key} = "$dir".q|node17.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:a2/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:MstarReduced/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:TstarTzero/;
$ref_files{$key} = "$dir".q|node8.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:appoximaetionR/;
$ref_files{$key} = "$dir".q|node7.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:massFlowRateRatio/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:T/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno_eq:fDef/;
$ref_files{$key} = "$dir".q|node13.html|; 
$noresave{$key} = "$nosave";

$key = q/pm:eq:MaxTurning/;
$ref_files{$key} = "$dir".q|node17.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:M2n/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:rhoRa/;
$ref_files{$key} = "$dir".q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:PzeroReduced/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:Tratioa/;
$ref_files{$key} = "$dir".q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:reducedDensity/;
$ref_files{$key} = "$dir".q|node16.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:sol/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:M2t/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:densityPbar/;
$ref_files{$key} = "$dir".q|node11.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:sondLiquid/;
$ref_files{$key} = "$dir".q|node5.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isentropicPratio/;
$ref_files{$key} = "$dir".q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoRatioIdealgas/;
$ref_files{$key} = "$dir".q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:M1n/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:thetaAlpha/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:workingFLDApprox/;
$ref_files{$key} = "$dir".q|node12.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:RzeroReduced2/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:pressureDless/;
$ref_files{$key} = "$dir".q|node8.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:solution/;
$ref_files{$key} = "$dir".q|node13.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:TratioExample/;
$ref_files{$key} = "$dir".q|node13.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:ImpulseRatio/;
$ref_files{$key} = "$dir".q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:workingEq/;
$ref_files{$key} = "$dir".q|node12.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:T0bar/;
$ref_files{$key} = "$dir".q|node12.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:PstarPzero1/;
$ref_files{$key} = "$dir".q|node8.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:R/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:Pratio/;
$ref_files{$key} = "$dir".q|node13.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:AstarReduced/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:RzeroReduced/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:minikoffSol/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:insontropic2/;
$ref_files{$key} = "$dir".q|node3.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:a3/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:x3/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:temperaturePbar/;
$ref_files{$key} = "$dir".q|node11.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:PDiffReduced/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:combinedState/;
$ref_files{$key} = "$dir".q|node7.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:workingFLDAppSol/;
$ref_files{$key} = "$dir".q|node12.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:variableArea/;
$ref_files{$key} = "$dir".q|node8.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:shock/;
$ref_files{$key} = "$dir".q|node11.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:nonDimMass/;
$ref_files{$key} = "$dir".q|node11.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:sound/;
$ref_files{$key} = "$dir".q|node4.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:Rhoratio/;
$ref_files{$key} = "$dir".q|node13.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:fanno/;
$ref_files{$key} = "$dir".q|node13.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:fld-M/;
$ref_files{$key} = "$dir".q|node13.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:RankineHugoniot/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:rayleigh/;
$ref_files{$key} = "$dir".q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoTzeroRatioStar/;
$ref_files{$key} = "$dir".q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoTzeroR1/;
$ref_files{$key} = "$dir".q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:P0R/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:beforeDefa/;
$ref_files{$key} = "$dir".q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:Tbar/;
$ref_files{$key} = "$dir".q|node13.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoRatioIdealgasStar/;
$ref_files{$key} = "$dir".q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:bulkModulus/;
$ref_files{$key} = "$dir".q|node5.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:x/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoAratioStar/;
$ref_files{$key} = "$dir".q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:rhoBar/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoTzeroR/;
$ref_files{$key} = "$dir".q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:PstarPzero/;
$ref_files{$key} = "$dir".q|node8.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:Uratio/;
$ref_files{$key} = "$dir".q|node13.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:workingFLD/;
$ref_files{$key} = "$dir".q|node12.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isentropicTratio/;
$ref_files{$key} = "$dir".q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:fld2/;
$ref_files{$key} = "$dir".q|node13.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:T0ratio/;
$ref_files{$key} = "$dir".q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:menikoffLarge/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:PzeroReduced1/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:pressureMx/;
$ref_files{$key} = "$dir".q|node11.html|; 
$noresave{$key} = "$nosave";

$key = q/SpeedSound/;
$ref_files{$key} = "$dir".q|node2.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:stagnationPressreRatio1/;
$ref_files{$key} = "$dir".q|node13.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:mixSound/;
$ref_files{$key} = "$dir".q|node7.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoPratioStar/;
$ref_files{$key} = "$dir".q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:RR/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:cubic/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:S/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:M1t/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoPzeroRatioStar/;
$ref_files{$key} = "$dir".q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:Q/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/isothermal:eq:integralMach/;
$ref_files{$key} = "$dir".q|node12.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoRatioIdealgasISO/;
$ref_files{$key} = "$dir".q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:nDef/;
$ref_files{$key} = "$dir".q|node4.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:D/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:solution2/;
$ref_files{$key} = "$dir".q|node11.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:M1/;
$ref_files{$key} = "$dir".q|node16.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:oblique/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/chap:Prandtl-Meyer/;
$ref_files{$key} = "$dir".q|node17.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:pressureRatio/;
$ref_files{$key} = "$dir".q|node11.html|; 
$noresave{$key} = "$nosave";

$key = q/fanno:eq:fld-dM1/;
$ref_files{$key} = "$dir".q|node13.html|; 
$noresave{$key} = "$nosave";

$key = q/ray:eq:Pratioa/;
$ref_files{$key} = "$dir".q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:theta/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:M2/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/shock:eq:totalPressureRatio/;
$ref_files{$key} = "$dir".q|node11.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:densityDless/;
$ref_files{$key} = "$dir".q|node8.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:PR/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:TR/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/oblique:eq:reducedPressure/;
$ref_files{$key} = "$dir".q|node16.html|; 
$noresave{$key} = "$nosave";

$key = q/sound:eq:gamma1/;
$ref_files{$key} = "$dir".q|node7.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:RzeroReduced1/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoPratio/;
$ref_files{$key} = "$dir".q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/variableArea:eq:isoAratio/;
$ref_files{$key} = "$dir".q|node10.html|; 
$noresave{$key} = "$nosave";

1;

