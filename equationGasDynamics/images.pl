# LaTeX2HTML 2002-2-1 (1.70)
# Associate images original text with physical files.


$key = q/displaystyle{A_2overA_1}={M_1overM_2}left(mbox{largee}^{{M_2}^{2}}overmbox{largee}^{{M_1}^{2}}right)^{kover2};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="170" HEIGHT="89" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img36.png"
 ALT="$\displaystyle {A_2 \over A_1} = { M_1 \over M_2} \left( \mbox{\large e}^{{M_2}^{2}} \over \mbox{\large e}^{{M_1}^{2}} \right)^{k \over 2}$">|; 

$key = q/displaystyle{FoverF^{*}}={P_1A_1overP^{*}A^{*}}{left(1+k{M_1}^2right)overleft(1+ctioneqref{variableArea:eq:beforeDefa}}}{1overleft(1+kright)};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="501" HEIGHT="173" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img48.png"
 ALT="$\displaystyle {F \over F^{*}} = {P_1A_1 \over P^{*}A^{*}} {\left( 1 + k{M_1}^2 ...
...e function \eqref{variableArea:eq:beforeDefa}}} {1 \over \left( 1 + k \right) }$">|; 

$key = q/displaystyle{T_yoverT_x}=left({P_{y}overP_{x}}right)^{2}left({M_yoverM_x}right)^{2};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="182" HEIGHT="71" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img54.png"
 ALT="$\displaystyle {T_y \over T_x} = \left( { P_{y} \over P_{x}} \right)^{2} \left( {M_y \over M_x} \right)^{2}$">|; 

$key = q/displaystyle{T_{0}overT_{0}^{*}}={ToverT^{*}}{1+{k-1over2}M^{2}over1+{k-1over2k}}={2kover3k-1}left(1+{k-1over2}right)M^{2};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="395" HEIGHT="70" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img68.png"
 ALT="$\displaystyle {T_{0} \over T_{0}^{*}} = { T \over T^{*}} { 1 + {k -1 \over 2} M...
... {k -1 \over 2k} } = {2k \over 3k -1 } \left( 1 + {k -1 \over 2} \right) M ^{2}$">|; 

$key = q/displaystyle{M_1}^2={2(cottheta+tandelta)oversin2theta-(tandelta)(k+cos2theta)};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="283" HEIGHT="63" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img133.png"
 ALT="$\displaystyle {M_1}^2 = { 2 ( \cot \theta + \tan \delta ) \over \sin 2 \theta - (\tan \delta) ( k + \cos 2 \theta) }$">|; 

$key = q/M_1>>M_2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="96" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img71.png"
 ALT="$ M_1 &gt; &gt; M_2$">|; 

$key = q/displaystyleF_{net}=P_0A^{*}(1+k){left(k+1over2right)^{koverk-1}}left({F_2overF^{*}}-{F_1overF^{*}}right);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="368" HEIGHT="78" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img49.png"
 ALT="$\displaystyle F_{net} = P_0 A^{*} (1+k) {\left( k+1 \over 2 \right)^{k \over k-1}} \left( {F_2 \over F^{*} } - { F_1 \over F^{*}}\right)$">|; 

$key = q/displaystyle{4overD}int^{L_{max}}_{L}fdx={1overk}{1-M^2overM^2}+{k+1over2k}ln{{k+1over2}M^2over1+{k-1over2}M^2};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="403" HEIGHT="70" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img76.png"
 ALT="$\displaystyle {4 \over D} \int^{L_{max}}_{L} f dx = {1 \over k} {1 - M^2 \over M^2} + {k+1 \over 2k}\ln {{k+1 \over 2}M^2 \over 1+ {k-1 \over 2}M^2}$">|; 

$key = q/displaystyle{T_2overT_1}={2k{M_1}^2sin^2theta-(k-1)left[(k-1){M_1}^2+2right]over(k+1)^2{M_1}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="378" HEIGHT="70" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img100.png"
 ALT="$\displaystyle {T_2 \over T_1} = {2k {M_1}^2 \sin^2\theta - (k-1) \left[(k-1) {M_1}^2 + 2 \right] \over (k+1)^2 {M_1}}$">|; 

$key = q/Q;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img121.png"
 ALT="$ Q$">|; 

$key = q/displaystyle{ToverT^{*}}={c^{2}over{c^{*}}^{2}}={{k+1over2}over{1+{k-1over2}M^{2}}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="199" HEIGHT="70" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img80.png"
 ALT="$\displaystyle { T \over T^{*}} = {c^{2} \over {c^{*}}^{2} } = {{ k + 1 \over 2} \over { 1 + {k - 1 \over 2} M^{2}} }$">|; 

$key = q/displaystyle{UoverU^{*}}=left({rhooverrho^{*}}right)^{-1}=Msqrt{{k+1over2}over{1+{k-1over2}M^{2}}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="280" HEIGHT="81" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img82.png"
 ALT="$\displaystyle { U \over U ^{*}} = \left( {\rho \over \rho^{*}} \right)^{-1} = M \sqrt{{k+1 \over 2} \over { 1 + {k - 1 \over 2} M^{2}} }$">|; 

$key = q/displaystyle{4bar{f}L_{max}overD}={1overk}{1-M^2overM^2}+{k+1over2k}ln{{k+1over2}M^2over1+{k-1over2}M^2};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="357" HEIGHT="70" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img78.png"
 ALT="$\displaystyle {4 \bar{f}L_{max}\over D} = {1 \over k} {1 - M^2 \over M^2} + {k+1 \over 2k}\ln {{k+1 \over 2}M^2 \over 1+ {k-1 \over 2}M^2}$">|; 

$key = q/displaystylenu(M);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="50" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img136.png"
 ALT="$\displaystyle \nu (M)$">|; 

$key = q/displaystyleM_{1n}={sqrt{(k+1){M_1}^2+1+sqrt{({M_1}^2left[{M_1}^2(k+1)^2+8(k^2-1)right]+16(1+k)}}over2sqrt{k}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="606" HEIGHT="117" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img127.png"
 ALT="$\displaystyle M_{1n} = {\sqrt{ (k+1) {M_1}^2 +1 + \sqrt{({M_1}^2\left[{M_1}^2 (k + 1)^2 +8(k^2 - 1)\right]+16(1+k)} } \over 2 \sqrt{k} }$">|; 

$key = q/displaystyle{dot{m}overA}=sqrt{k{P_0}^2M^2overRT_0}left(1+{k-1over4}M^2+cdotsright);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="325" HEIGHT="84" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img30.png"
 ALT="$\displaystyle {\dot{m} \over A} = \sqrt{k {P_0}^2 M^2 \over RT_0} \left( 1 + {k-1 \over 4}M^2 + \cdots \right)$">|; 

$key = q/displaystyle{P_{0}overP_{0}^{*}}={PoverP^{*}}left[{1+{k-1over2}M^{2}over{1+{k-1over2k}}}right]^{koverk-1}%label{eq:};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="226" HEIGHT="89" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img66.png"
 ALT="$\displaystyle {P_{0} \over P_{0}^{*}} = {P \over P^{*}} \left[ {1 + { k -1 \ove...
... ^ {2} \over { 1 + {k -1 \over 2k} } } \right] ^ { k \over k -1 } %\label{eq:}
$">|; 

$key = q/displaystyle=-{2{M_1}^2+1over{M_1}^4}+left[{(k+1)^2over4}+{k-1over{M_1}^2}right]sin^2delta;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="346" HEIGHT="67" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img107.png"
 ALT="$\displaystyle = - { 2{M_1}^2 + 1 \over {M_1}^4 } + \left[ {(k+1)^2 \over 4}+ {k -1 \over {M_1}^2} \right] \sin ^2 \delta$">|; 

$key = q/displaystylembox{frac{4fL}{D}{};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="32" HEIGHT="43" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img69.png"
 ALT="$\displaystyle \mbox{$\frac{4fL}{D}$}$">|; 

$key = q/displaystyle{rho_2-rho_1overrho_2}={sindeltaoversinthetacos(theta-delta)};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="215" HEIGHT="61" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img135.png"
 ALT="$\displaystyle {\rho_ 2 -\rho_1 \over \rho_2} = {\sin \delta \over \sin \theta \cos (\theta -\delta)}$">|; 

$key = q/R;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="16" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img122.png"
 ALT="$ R$">|; 

$key = q/displaystyle{Tover{T_{0}}_{int}}={1over1+{k-1over2}M^2};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="163" HEIGHT="61" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img45.png"
 ALT="$\displaystyle {T \over{T_{0}}_{int}} = { 1 \over 1 + {k-1 \over 2} M^2}$">|; 

$key = q/displaystyle{T_yoverT_x}=left({P_yoverP_x}right)left({k+1overk-1}+{P_yoverP_x}over1+{k+1overk-1}{P_yoverP_x}right);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="223" HEIGHT="76" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img62.png"
 ALT="$\displaystyle {T_y \over T_x} = \left( {P_y \over P_x} \right) \left( {k + 1 \over k -1 } + {P_y \over P_x} \over 1+ {k + 1 \over k -1 } {P_y \over P_x} \right)$">|; 

$key = q/displaystyle{d{P}over{drho}}=left.{{partial{P}over{partialrho}}}right|_{s};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="103" HEIGHT="64" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img3.png"
 ALT="$\displaystyle {d {P} \over { d \rho}} = \left. {{\partial {P} \over { \partial \rho}} } \right\vert _{s}$">|; 

$key = q/displaystyleR={9a_1a_2-27a_3-2{a_1}^3over54};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="209" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img124.png"
 ALT="$\displaystyle R = { 9 a_1 a_2 - 27 a_3 - 2 {a_1}^3 \over 54}$">|; 

$key = q/displaystyle{P_2-P_1overrho_2-rho_1}=k{P_2-P_1overrho_2-rho_1};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="173" HEIGHT="61" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img101.png"
 ALT="$\displaystyle {P_2 - P_1 \over \rho_2 - \rho_1} = k { P_2 - P_1 \over \rho_2 - \rho_1}$">|; 

$key = q/displaystylebar{f}={1overL_{max}}int^{L_{max}}_{0}{fdx};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="177" HEIGHT="68" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img77.png"
 ALT="$\displaystyle \bar{f} = { 1 \over L_{max}} \int ^{L_{max}} _{0} {f dx}$">|; 

$key = q/displaystylex^3+a_1x^2+a_2x+a_3=0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="207" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img102.png"
 ALT="$\displaystyle x^3 + a_1 x^2 + a_2 x + a_3=0$">|; 

$key = q/displaystyle{rho_yoverrho_x}={U_xoverU_y}={(k+1){M_x}^{2}over2+(k-1){M_x}^{2}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="230" HEIGHT="67" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img61.png"
 ALT="$\displaystyle {\rho_y \over \rho_x} = {U_x \over U_y} = {( k +1) {M_x}^{2} \over 2 + (k -1) {M_x}^{2} }$">|; 

$key = q/displaystyle=sqrt{k+1overk-1}tan^{-1}left(sqrt{k-1overk+1}sqrt{M^2-1}right)-tan^{-1}sqrt{M^2-1};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="462" HEIGHT="76" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img139.png"
 ALT="$\displaystyle = \sqrt{k+1\over k-1} \tan^{-1} \left( \sqrt{k-1\over k+1} \sqrt{ M^2 -1}\right) - \tan^{-1} \sqrt{ M^2 -1}$">|; 

$key = q/displaystyle{rho_2overrho_1}={{U_1}_nover{U_2}_n}={(k+1){M_1}^{2}sin^2thetaover(k-1){M_1}^2sin^2theta+2};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="282" HEIGHT="67" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img99.png"
 ALT="$\displaystyle {\rho_2 \over \rho_1} = {{U_1}_n \over {U_2}_n} = { (k+1) {M_1}^{2} \sin^2\theta \over (k-1) {M_1}^2 \sin^2\theta + 2}$">|; 

$key = q/P=zrhoRT;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="92" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img6.png"
 ALT="$ P = z \rho R T$">|; 

$key = q/displaystyletandelta=2cotthetaleft[{M_1}^{2}sin^2theta-1over{M_1}^{2}left(k+cos2thetaright)+2right];MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="310" HEIGHT="67" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img98.png"
 ALT="$\displaystyle \tan \delta = 2 \cot \theta \left[{M_1}^{2} \sin^2 \theta - 1 \over {M_1}^{2} \left(k + \cos 2 \theta \right) +2 \right]$">|; 

$key = q/displaystyle{2(P_2-P_1)overrhoU^2}={2sinthetasindeltaovercos(theta-delta)};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="210" HEIGHT="63" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img134.png"
 ALT="$\displaystyle {2(P_2 - P_1) \over \rho U^2} = {2 \sin\theta \sin \delta \over \cos(\theta - \delta)}$">|; 

$key = q/displaystyle{T_0}_y={T_0}_x;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="83" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img53.png"
 ALT="$\displaystyle {T_0}_y = {T_0}_x$">|; 

$key = q/displaystyle{T_2overT_1}={left.ToverT^{*}right|_{M_2}overleft.ToverT^{*}right|_{M_1}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="105" HEIGHT="75" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img85.png"
 ALT="$\displaystyle {T_2 \over T_1} ={ \left. T \over T^{*} \right\vert _{M_2} \over \left. T \over T^{*} \right\vert _{M_1} }$">|; 

$key = q/displaystyle{rho_1overrho^{*}}={U^{*}overU_1}={{U^{*}oversqrt{kRT^{*}}}sqrt{kRT^{U_1oversqrt{kRT_1}}sqrt{kRT_1}}={1overM_1}sqrt{T^{*}overT_1};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="316" HEIGHT="79" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img89.png"
 ALT="$\displaystyle {\rho_1 \over \rho^{*}} = {U^{*} \over U_1} = { {U^{*} \over \sqr...
...U_1 \over \sqrt{kRT_1} } \sqrt{kRT_1} } = {1 \over M_1} \sqrt{ T^{*} \over T_1}$">|; 

$key = q/displaystyleM_1simhbox{hugee}^{{1over2}left(mbox{frac{4fL}{D}}+1right)};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="147" HEIGHT="74" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img74.png"
 ALT="$\displaystyle M_1 \sim \hbox{\huge e}^{{1\over 2}\left(\mbox{$\frac{4fL}{D}$}+1\right)}$">|; 

$key = q/displaystyle{PoverP^{*}}={1overM}sqrt{{k+1over2}over{1+{k-1over2}M^{2}}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="191" HEIGHT="81" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img79.png"
 ALT="$\displaystyle {P \over P^{*}} = { 1 \over M} \sqrt{{k+1 \over 2} \over { 1 + {k - 1 \over 2} M^{2}} }$">|; 

$key = q/displaystylesin^2theta_{max}={-1+{k+1over4}{M_1}^2+sqrt{(k+1)left[1+{k-1over2}{M_1}^2+left({k+1over2}{M_1}right)^4right]}overk{M_1}^2};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="529" HEIGHT="117" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img125.png"
 ALT="$\displaystyle \sin ^2 \theta_{max} = { -1 + { k + 1 \over 4}{M_1}^2+ \sqrt{(k+1...
...ver 2} {M_1}^2 + \left( {k+1 \over 2} {M_1} \right)^4 \right]} \over k {M_1}^2}$">|; 

$key = q/displaystyle{P_{0}over{P_{0}}^{*}}={1overM}left({{1+{k-1over2}M^{2}}over{k+1over2}}right)^{k+1over2(k-1)};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="252" HEIGHT="90" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img83.png"
 ALT="$\displaystyle {P_{0} \over {P_{0}}^{*}} = { 1 \over M} \left({ { 1 + {k - 1 \over 2} M^{2}} \over {k+1 \over 2} } \right)^{k +1 \over 2(k -1)}$">|; 

$key = q/displaystyle{F_{net}overP_0A^{*}}=overbrace{P_2A_2overP_0A^{*}}^{f(M_2)}overbracerP_0A^{*}}^{f(M_1)}overbrace{left(1+k{M_1}^2right)}^{f(M_1)};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="379" HEIGHT="118" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img47.png"
 ALT="$\displaystyle {F_{net} \over P_0 A^{*}} = \overbrace{P_2A_2 \over P_0 A^{*}}^{f...
...1A_1 \over P_0 A^{*}}^{f(M_1)} \overbrace{\left( 1 + k{M_1}^2 \right)}^{f(M_1)}$">|; 

$key = q/displaystyleD=Q^3+R^2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="113" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img120.png"
 ALT="$\displaystyle D = Q^3 + R^2$">|; 

$key = q/displaystyle{P^{*}overP_1}={1+k{M_1}^{2}over1+k};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="136" HEIGHT="67" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img87.png"
 ALT="$\displaystyle {P^{*} \over P_1} = {1 + k{M_1}^{2} \over 1 + k}$">|; 

$key = q/displaystyle{rho_0overrho}=left({T_0overT}right)^{1overk-1}=left(1+{k-1over2}M^{2}right)^{1overk-1};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="314" HEIGHT="77" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img20.png"
 ALT="$\displaystyle {\rho_0 \over \rho } = \left( { T_0 \over T} \right) ^ {1 \over k -1} = \left( 1 + { k -1 \over 2 } M^{2} \right)^ {1 \over k -1}$">|; 

$key = q/displaystyletan(theta-delta)={{U_2}_nover{U_2}_t};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="146" HEIGHT="61" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img93.png"
 ALT="$\displaystyle \tan ( \theta - \delta ) = {{U_2}_n \over {U_2}_t}$">|; 

$key = q/displaystyle{rhooverrho_a}=1+m;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="100" HEIGHT="51" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img13.png"
 ALT="$\displaystyle {\rho \over \rho_a} = 1 + m$">|; 

$key = q/P=rhoRT;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="82" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img4.png"
 ALT="$ P = \rho R T$">|; 

$key = q/displaystyle{rho_0-rhooverrho}={M^2over2}left(1-{kM^2over4}+cdotsright);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="270" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img29.png"
 ALT="$\displaystyle {\rho_0 -\rho \over \rho} = {M^2 \over 2} \left( 1 - {kM^2 \over 4} + \cdots \right)$">|; 

$key = q/displaystyle{rhooverrho^{*}}={1overM}sqrt{{1+{k-1over2}M^{2}}over{k+1over2}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="186" HEIGHT="81" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img81.png"
 ALT="$\displaystyle {\rho \over \rho^{*}} = { 1 \over M} \sqrt{ { 1 + {k - 1 \over 2} M^{2}} \over {k+1 \over 2} }$">|; 

$key = q/displaystyle{Poverrho}={Rover1+m}T;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="114" HEIGHT="61" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img15.png"
 ALT="$\displaystyle {P \over \rho} = { R \over 1 + m} T$">|; 

$key = q/n;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="16" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img8.png"
 ALT="$ n$">|; 

$key = q/displaystyle{{P_0}_yover{P_0}_x}={P_yleft(1+{k-1over2}{M_y}^{2}right)^{koverk-1}overP_xleft(1+{k-1over2}{M_x}^{2}right)^{koverk-1}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="228" HEIGHT="83" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img56.png"
 ALT="$\displaystyle {{P_0}_y \over {P_0}_x} = { P_y \left( 1 + {k-1 \over 2} {M_y}^{2...
...ver k-1} \over P_x \left( 1 + {k-1 \over 2} {M_x}^{2} \right) ^ {k \over k-1} }$">|; 

$key = q/displaystyle{AoverA^{*}}={1overM};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="79" HEIGHT="61" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img41.png"
 ALT="$\displaystyle { A \over A^{*}} = {1 \over M}$">|; 

$key = q/displaystyle=theta(M)-theta(M_{starting});MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="184" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img137.png"
 ALT="$\displaystyle = \theta(M) - \theta(M_{starting})$">|; 

$key = q/delta;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img1.png"
 ALT="$ \delta $">|; 

$key = q/displaystyle^{(1-M^2)kover2};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="59" HEIGHT="58" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img40.png"
 ALT="$\displaystyle ^{(1-M^2) k \over 2}$">|; 

$key = q/displaystyle{P^{*}overP_0}=left(2overk+1right)^{koverk-1};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="157" HEIGHT="78" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img22.png"
 ALT="$\displaystyle {P^{*} \over P_0} = \left(2 \over k+1 \right)^{k \over k-1}$">|; 

$key = q/displaystyletantheta={{U_1}_nover{U_1}_t};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="102" HEIGHT="61" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img92.png"
 ALT="$\displaystyle \tan \theta = {{U_1}_n \over {U_1}_t}$">|; 

$key = q/displaystyle{mbox{frac{4fL_{max}}{D}}}={1-kM^{2}overkM^{2}}+lnkM^{2};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="236" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img65.png"
 ALT="$\displaystyle {\mbox{$\frac{4fL_{max}}{D}$}} = { 1- k M^{2} \over k M^{2} } + \ln kM^{2}$">|; 

$key = q/displaystyle{P_0-PoverP}={kM^2over2}left(1+{M^2over4}+cdotsright);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="277" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img28.png"
 ALT="$\displaystyle {P_0 -P \over P} = {kM^2 \over 2} \left( 1 + {M^2 \over 4} + \cdots \right)$">|; 

$key = q/displaystyle(S+T)-;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="83" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img116.png"
 ALT="$\displaystyle (S +T ) -$">|; 

$key = q/displaystyle{rho_2overrho_1}={{U_1}_nover{U_2}_n}={(k+1){M_1}^2sin^2thetaover(k-1){M_1}^2sin^2theta+2};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="282" HEIGHT="67" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img129.png"
 ALT="$\displaystyle {\rho_2 \over \rho_1 } = { {U_1}_n \over {U_2}_n} = { (k +1) {M_1}^2 \sin ^2 \theta \over (k -1) {M_1}^2 \sin ^2 \theta + 2}$">|; 

$key = q/displaystyleS=sqrt[3]{R+sqrt{D}},;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="137" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img117.png"
 ALT="$\displaystyle S = \sqrt[3]{R + \sqrt{D}},$">|; 

$key = q/displaystyle{M_2}^2={(k+1)^2{M_1}^4sin^2theta-4({M_1}^2sin^2theta-1)(k{M_1}^2sin}^2sin^2theta-(k-1)right)left((k-1){M_1}^2sin^2theta+2right)};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="507" HEIGHT="67" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img131.png"
 ALT="$\displaystyle {M_2}^2 = {(k+1)^2 {M_1}^4 \sin ^2 \theta - 4({M_1}^2 \sin ^2 \th...
...\sin ^2 \theta - (k-1) \right) \left( (k-1) {M_1}^2 \sin ^2 \theta +2 \right) }$">|; 

$key = q/displaystyle={2koverk+1}{M_x}^2-{k-1overk+1};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="175" HEIGHT="61" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img59.png"
 ALT="$\displaystyle = {2k \over k+1 } {M_x}^2 - {k -1 \over k+1}$">|; 

$key = q/displaystyle{M_y}^2={{M_x}^2+{2overk-1}over{2koverk-1}{M_x}^2-1};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="162" HEIGHT="72" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img57.png"
 ALT="$\displaystyle {M_y}^2 = { {M_x}^2 + {2 \over k -1} \over {2k \over k -1} {M_x}^2 - 1 }$">|; 

$key = q/m={dot{m}_boverdot{m}_a};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="69" HEIGHT="42" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img14.png"
 ALT="$ m = {\dot{m}_b \over \dot{m}_a}$">|; 

$key = q/R_{mix}={Rover1+m};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="102" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img18.png"
 ALT="$ R_{mix} = { R \over 1 + m}$">|; 

$key = q/displaystylesin(theta-delta)={{M_2}_nover{M_2}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="147" HEIGHT="61" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img95.png"
 ALT="$\displaystyle \sin (\theta - \delta ) = {{M_2}_n \over {M_2}}$">|; 

$key = q/displaystyleisqrt{3}(S-T);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="100" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img114.png"
 ALT="$\displaystyle i \sqrt{3} ( S-T)$">|; 

$key = q/displaystyle{P_yoverP_x}={1+k{M_{x}}^2over1+k{M_{y}}^2};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="135" HEIGHT="67" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img55.png"
 ALT="$\displaystyle {P_y \over P_x} = {1 + k{M_{x}}^2 \over 1 + k{M_{y}}^2}$">|; 

$key = q/displaystyle{P_yoverP_x};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="61" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img58.png"
 ALT="$\displaystyle {P_y \over P_x}$">|; 

$key = q/displaystyle=-{{M_1}^2+2over{M_1}^2}-ksin^2delta;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="188" HEIGHT="67" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img105.png"
 ALT="$\displaystyle = - {{M_1}^2 + 2 \over {M_1}^2} - k \sin ^2 \delta$">|; 

$key = q/displaystyle{P_0-Pover{1over2}rhoU^2}=1+overbrace{{M^2over4}+{(2-k)M^4over24}+cdots}^{compressibility;correction};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="312" HEIGHT="119" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img26.png"
 ALT="$\displaystyle {P_0 - P \over {1 \over 2 } \rho U^2} = 1 + \overbrace{{ M^2 \over 4} + {(2-k) M^4\over 24} + \cdots} ^{compressibility\; correction}$">|; 

$key = q/displaystyle{4fdxoverD}={{left(1-M^2right)dM^2}over{kM^4(1+{k-1over2}M^2})};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="219" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img75.png"
 ALT="$\displaystyle { 4 f dx \over D} = {{\left( 1 - M^2 \right) dM^2} \over {kM^4 ( 1 + {k-1 \over 2}M^2} ) }$">|; 

$key = q/displaystylea_1;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img104.png"
 ALT="$\displaystyle a_1$">|; 

$key = q/displaystylec=sqrt{elastic;propertyoverinertial;property}=sqrt{Boverrho};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="264" HEIGHT="81" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img10.png"
 ALT="$\displaystyle c = \sqrt{elastic\; property \over inertial\; property} = \sqrt{B \over \rho}$">|; 

$key = q/displaystylex=sin^2theta;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="83" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img103.png"
 ALT="$\displaystyle x = \sin^2 \theta$">|; 

$key = q/displaystylenu_{infty}={piover2}left[sqrt{k+1overk-1}-1right];MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="193" HEIGHT="76" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img140.png"
 ALT="$\displaystyle \nu_{\infty} = {\pi \over 2} \left[ \sqrt{k+1 \over k -1} - 1 \right]$">|; 

$key = q/displaystylec=sqrt{nzRT};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="102" HEIGHT="46" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img7.png"
 ALT="$\displaystyle c = \sqrt{nz R T}$">|; 

$key = q/displaystyle{P_0over{P_0}^{*}}={Hugee}^{(1-M)kover2}{2left(1+{k-1over2}{M_1}^2right)overk+1}^{koverk-1};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="271" HEIGHT="83" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img43.png"
 ALT="$\displaystyle { P_0 \over {P_0}^{*}} = {\Huge e}^{(1-M)k \over 2} {2 \left( 1 + {k -1 \over 2}{M_1}^2 \right) \over k +1 } ^ {k \over k-1}$">|; 

$key = q/displaystyle{M_{1n}}=sqrt{k+1over2k}M_{1}quadhbox{for}quadM_{1}>>1;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="289" HEIGHT="73" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img126.png"
 ALT="$\displaystyle {M_{1n}} = \sqrt{ k+1\over 2k } M_{1} \quad \hbox{for} \quad M_{1} &#187; 1$">|; 

$key = q/displaystylea_3;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img108.png"
 ALT="$\displaystyle a_3$">|; 

$key = q/displaystyle{rho^{*}overrho_0}=left(2overk+1right)^{1overk-1};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="152" HEIGHT="77" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img23.png"
 ALT="$\displaystyle {\rho^{*} \over \rho_0} = \left(2 \over k+1 \right)^{1 \over k-1}$">|; 

$key = q/displaystyle{{T_0}_1over{T_0}^{*}}={T_1left(1+{k-1over2}{M_1}^{2}right)overT^{*}(1+k){M_1}^{2}over(1+kM^{2})^2}left(1+{k-1over2}{M_1}^2right);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="469" HEIGHT="70" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img90.png"
 ALT="$\displaystyle {{T_0}_1 \over {T_0}^{*}} = {T_1 \left( 1 + {k -1 \over 2} {M_1}^...
... k ) {M_1}^{2} \over (1 + kM^{2})^2} \left( 1 + {k -1 \over 2} {M_1} ^2 \right)$">|; 

$key = q/displaystyle{P_2overP_1}={2k{M_1}^2sin^2theta-(k-1)overk+1};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="237" HEIGHT="67" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img128.png"
 ALT="$\displaystyle {P_ 2 \over P_1} = { 2 k {M_1}^2 \sin ^2 \theta - (k -1) \over k+1}$">|; 

$key = q/displaystyle{F_2overF^{*}}={1overM_2}{1+k{M_2}^2over1+k};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="165" HEIGHT="67" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img52.png"
 ALT="$\displaystyle {F_2 \over F^{*}} = {1 \over M_2} { 1 + k {M_2}^2 \over 1 + k }$">|; 

$key = q/displaystyle(S+T)+;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="83" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img113.png"
 ALT="$\displaystyle (S +T ) +$">|; 

$key = q/displaystyle{P_{0}overP_{0}^{*}}={1oversqrt{k}}left({2kover3k-1}right)^{koverk-1}left(1+{k-1over2}M^{2}right)^{koverk-1}{1overM}%label{eq:};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="391" HEIGHT="78" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img67.png"
 ALT="$\displaystyle {P_{0} \over P_{0}^{*}} = {1 \over \sqrt{k}} \left( {2k \over 3k-...
...left( 1 + {k -1 \over 2} M ^{2}\right)^{k \over k-1} { 1 \over M} %\label{eq:}
$">|; 

$key = q/displaystyle{T_0over{T_0}^{*}}={2left(1+{k-1over2}{M_1}^2right)overk+1}^{koverk-1};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="214" HEIGHT="83" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img42.png"
 ALT="$\displaystyle { T_0 \over {T_0}^{*}} = {2 \left( 1 + {k -1 \over 2}{M_1}^2 \right) \over k +1 } ^ {k \over k-1}$">|; 

$key = q/displaystyle{F_2overF_1}={P_2A_2overP_1A_1}{1+{{U_2}^2overRT}over1+{{U_1}^2overRT}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="161" HEIGHT="74" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img50.png"
 ALT="$\displaystyle {F_2 \over F_1} = {P_2 A_2 \over P_1 A_1} { 1 + {{U_2}^2 \over RT} \over 1 + {{U_1}^2 \over RT }}$">|; 

$key = q/displaystyle{{P_0}_2over{P_0}_1}={P_2overP_1}left(1+{k-1over2}{M_2}^2over1+{k-1oargee}^{{M_1}^{2}}overmbox{largee}^{{M_1}^{2}}right]^{kover2};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="341" HEIGHT="89" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img37.png"
 ALT="$\displaystyle {{P_0}_2 \over {P_0}_1} = { P_2 \over P_1} \left( 1 + {k -1 \over...
...ox{\large e}^{{M_1}^{2}} \over \mbox{\large e} ^{{M_1}^{2}} \right]^{k \over 2}$">|; 

$key = q/displaystylecostheta={{M_1}_tover{M_1}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="102" HEIGHT="61" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img96.png"
 ALT="$\displaystyle \cos \theta = {{M_1}_t \over {M_1}}$">|; 

$key = q/theta;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img2.png"
 ALT="$ \theta $">|; 

$key = q/displaystyleM^{*}={Uoverc^{*}}=sqrt{k+1over2}Mleft(1-{k-1over4}M^2+cdotsright);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="375" HEIGHT="73" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img27.png"
 ALT="$\displaystyle M^{*} = {U \over c^{*} } = \sqrt{k+1 \over 2} M \left( 1 - {k -1 \over 4} M^2 + \cdots \right)$">|; 

$key = q/displaystyleT=sqrt[3]{R-sqrt{D}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="133" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img118.png"
 ALT="$\displaystyle T = \sqrt[3]{R - \sqrt{D}}$">|; 

$key = q/displaystylegamma={C_p+mCoverC_v+mC};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="120" HEIGHT="61" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img17.png"
 ALT="$\displaystyle \gamma = {C_p + mC \over C_v + mC}$">|; 

$key = q/displaystyleleft({4fL_{max}overD}right)_{2}=left({4{f}L_{max}overD}right)_{1}-{4{f}LoverD};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="291" HEIGHT="64" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img86.png"
 ALT="$\displaystyle \left( {4f L_{max} \over D} \right)_{2} = \left( {4{f} L_{max} \over D} \right)_{1} - {4{f} L \over D}$">|; 

$key = q/displaystyle{{T_0}_1over{T_0}_2}={left(1+{k-1over2}{M_1}^2right)overleft(1+{k-1o(1+{k-1over2}{M_1}^2right)overleft(1+{k-1over2}{M_2}^2right)};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="316" HEIGHT="70" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img34.png"
 ALT="$\displaystyle {{T_0}_1 \over {T_0}_2} = {\left(1+{k -1\over2} {M_1}^2\right)\ov...
... {\left(1+{k -1\over2} {M_1}^2\right)\over \left(1+{k -1\over2}{M_2}^2\right) }$">|; 

$key = q/displaystyle{AoverA^{*}}={1overM}left({1+{k-1over2}M^{2}over{k+1over2}}right)^{k+1over2(k-1)};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="246" HEIGHT="90" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img32.png"
 ALT="$\displaystyle {A \over A^{*}} = { 1 \over M} \left( { 1 + {k -1 \over 2} M^{2} \over {k +1\over 2}} \right) ^ {k+ 1 \over 2 (k -1 )}$">|; 

$key = q/displaystyle{rho_xoverrho_y}={1+left({k+1overk-1}right)left({P_yoverP_x}right)overleft(k+1overk-1right)+left({P_yoverP_x}right)};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="180" HEIGHT="92" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img63.png"
 ALT="$\displaystyle {\rho_x \over \rho_y} = { 1 + \left( {k +1 \over k -1} \right) \l...
...P_x} \right) \over \left( k+1 \over k-1\right) +\left( {P_y \over P_x} \right)}$">|; 

$key = q/displaystyleint_{0}^{L}{4fdxoverD}=int_{M^{2}}^{1slashk}{1-kM{2}overkM{2}}dM^{2};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="264" HEIGHT="70" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img64.png"
 ALT="$\displaystyle \int_{0}^{L} { 4 f dx \over D} = \int_{M^{2}}^{1/k} { 1 - kM{2} \over kM{2}} dM^{2}$">|; 

$key = q/displaystyle{T_2overT_1}={{c_2}^2over{c_1}^2}={left(2k{M_1}^2sin^2theta-(k-1)rigeft((k-1){M_1}^2sin^2theta+2right)over(k+1){M_1}^2sin^2theta};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="496" HEIGHT="70" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img130.png"
 ALT="$\displaystyle { T_2 \over T_1} = { {c_2}^2 \over {c_1}^2} = { \left( 2k {M_1}^2...
...( (k-1) {M_1}^2 \sin ^2 \theta + 2 \right) \over (k+1) {M_1}^2 \sin ^2 \theta }$">|; 

$key = q/displaystyle=-{cos^2deltaover{M_1}^4};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="88" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img109.png"
 ALT="$\displaystyle = - {\cos ^2 \delta \over {M_1}^4}$">|; 

$key = q/displaystylex_1=-{1over3}a_1+(S+T);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="180" HEIGHT="59" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img110.png"
 ALT="$\displaystyle x_1 = - {1 \over 3} a_1 + (S +T )$">|; 

$key = q/displaystyle=2lnM_{1}-1-overbrace{1-k{M_{2}}^{2}overk{M_{2}}^{2}}^{sim0};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="218" HEIGHT="119" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img73.png"
 ALT="$\displaystyle = 2 \ln M_{1} -1 - \overbrace{ 1 - k{M_{2}}^{2} \over k {M_{2}}^{2}}^{\sim 0}$">|; 

$key = q/displaystyle{{P_0}_1over{P_0}^{*}}={P_1left(1+{k-1over2}{M_1}^{2}right)overP^{*}}^2}right)}left({1+k{M_1}^2over{(1+k)over2}}right)^{koverk-1};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="471" HEIGHT="89" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img91.png"
 ALT="$\displaystyle {{P_0}_1 \over {P_0}^{*}} = {P_1 \left( 1 + {k -1 \over 2} {M_1}^...
...}\right)} \left( { 1 + k{M_1}^2 \over {(1 + k) \over 2}} \right)^{k \over k -1}$">|; 

$key = q/displaystyle{P_{0_2}overP_{0_1}}=left[(k+1){M_1}^2sin^2thetaover(k-1){M_1}^2sin^verk-1}left[k+1over2k{M_1}^2sin^2theta-(k-1)right]^{1overk-1};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="513" HEIGHT="81" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img132.png"
 ALT="$\displaystyle {P_{0_2} \over P_{0_1}} = \left[ (k+1) {M_1}^2 \sin ^2 \theta \ov...
... -1} \left[ k+1 \over 2 k {M_1}^2 \sin ^2 \theta - (k-1) \right] ^{1 \over k-1}$">|; 

$key = q/displaystyle{P_0overP}=1+{(k-1)M^2over4}+{kM^4over8}+{2(2-k)M^6over48}cdots;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="390" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img24.png"
 ALT="$\displaystyle {P_0\over P} = 1 + {(k -1) M^2 \over 4} + {k M^4\over 8} + {2(2-k)M^6 \over 48} \cdots$">|; 

$key = q/displaystyle{F_2overF_1}={M_1overM_2}{1+k{M_2}^2over1+k{M_1}^2};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="163" HEIGHT="67" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img51.png"
 ALT="$\displaystyle {F_2 \over F_1} = {M_1 \over M_2} { 1 + k {M_2}^2 \over 1 + k {M_1}^2}$">|; 

$key = q/M_1rightarrow1;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="69" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img72.png"
 ALT="$ M_1 \rightarrow 1$">|; 

$key = q/D;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="16" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img119.png"
 ALT="$ D$">|; 

$key = q/displaystyle{AoverA^{*}}=left(2overk+1right)^{k+1over2(k-1)}left({1overM}+{k+1over4}M+{(3-k)(k+1)over32}M^3+cdotsright);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="534" HEIGHT="78" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img31.png"
 ALT="$\displaystyle {A \over A^{*}} = \left(2 \over k +1 \right)^{k +1 \over 2 (k-1)} \left( {1\over M} + {k+1 \over 4}M + {(3-k) (k+1)\over 32 } M^3 + \cdots \right)$">|; 

$key = q/displaystyle{P_0overP}=left({T_0overT}right)^{koverk-1}=left(1+{k-1over2}M^{2}right)^{koverk-1};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="316" HEIGHT="78" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img19.png"
 ALT="$\displaystyle {P_0 \over P } = \left( { T_0 \over T} \right) ^ {k \over k -1} = \left( 1 + { k -1 \over 2 } M^{2} \right)^ {k \over k -1}$">|; 

$key = q/displaystylec=sqrt{gammaR_{mix}T};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="120" HEIGHT="46" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img16.png"
 ALT="$\displaystyle c = \sqrt{\gamma R_{mix} T}$">|; 

$key = q/displaystyle=left.mbox{frac{4fL_{max}}{D}}right|_{1}-left.mbox{frac{4fL_{max}}{DM_{2}}^{2}overk{M_{2}}^{2}}+lnleft({M_{1}overM_{2}}right)^{2};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="499" HEIGHT="71" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img70.png"
 ALT="$\displaystyle = \left. \mbox{$\frac{4fL_{max}}{D}$}\right\vert _{1} - \left. \m...
... k{M_{2}}^{2} \over k {M_{2}}^{2}} + \ln \left( {M_{1} \over M_{2}} \right)^{2}$">|; 

$key = q/displaystyleT_1=T_2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="67" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img33.png"
 ALT="$\displaystyle T_1 = T_2$">|; 

$key = q/displaystylecos(theta-delta)={{M_2}_tover{M_2}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="146" HEIGHT="61" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img97.png"
 ALT="$\displaystyle \cos (\theta - \delta ) = {{M_2}_t \over {M_2}}$">|; 

$key = q/displaystyle{rho_0overrho}=1+{(k-1)M^2over4}+{kM^4over8}+{2(2-k)M^6over48}cdots;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="387" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img25.png"
 ALT="$\displaystyle {\rho_0\over \rho} = 1 + {(k -1) M^2 \over 4} + {k M^4\over 8} + {2(2-k)M^6 \over 48} \cdots$">|; 

$key = q/displaystylesintheta={{M_1}_nover{M_1}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="104" HEIGHT="61" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img94.png"
 ALT="$\displaystyle \sin \theta = {{M_1}_n \over {M_1}}$">|; 

$key = q/displaystyle{P_2overP_1}={Hugee}^{k({M_1}^2-{M_2}^2)over2}=left({Hugee}^{{M_1}^2}over{Hugee}^{{M_2}^2}right)^{kover2};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="246" HEIGHT="89" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img35.png"
 ALT="$\displaystyle {P_2 \over P_1} = {\Huge e}^{k({M_1}^2 - {M_2}^2) \over 2} = \left( {\Huge e}^{{M_1}^2} \over {\Huge e}^{{M_2}^2}\right) ^{k\over 2}$">|; 

$key = q/displaystyle{T^{*}overT_1}={1overM^2}left({1+k{M_1}^{2}over1+k}right)^{2};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="205" HEIGHT="73" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img88.png"
 ALT="$\displaystyle {T^{*} \over T_1} = {1 \over M^2} \left( {1 + k{M_1}^{2} \over 1 + k} \right)^{2}$">|; 

$key = q/displaystylec=sqrt{Eoverrho};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="76" HEIGHT="80" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img12.png"
 ALT="$\displaystyle c = \sqrt{ E \over \rho}$">|; 

$key = q/displaystylea_2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img106.png"
 ALT="$\displaystyle a_2$">|; 

$key = q/displaystylen=overbrace{C_poverC_v}^{k}left(z+Tleft(partialzoverpartialTright)_{rho}overz+Tleft(partialzoverpartialTright)_{P}right);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="212" HEIGHT="114" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img9.png"
 ALT="$\displaystyle n = \overbrace{C_p \over C_v}^{k} \left( z + T \left( \partial z ...
...ight)_{\rho} \over z + T \left( \partial z \over \partial T \right)_{P} \right)$">|; 

$key = q/displaystylembox{frac{1}{2}{};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="40" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img112.png"
 ALT="$\displaystyle \mbox{$\frac{1}{2}$}$">|; 

$key = q/displaystylec=sqrt{kRT};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="92" HEIGHT="46" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img5.png"
 ALT="$\displaystyle c = \sqrt{ k R T}$">|; 

$key = q/displaystyle{PoverP^{*}}={rhooverrho^{*}}=;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="97" HEIGHT="61" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img39.png"
 ALT="$\displaystyle { P \over P^{*}} = { \rho \over \rho^{*}} =$">|; 

$key = q/displaystyle%nonumber;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="5" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img138.png"
 ALT="$\displaystyle %\nonumber
$">|; 

$key = q/{T_{0}}_{int};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img44.png"
 ALT="$ {T_{0}}_{int}$">|; 

$key = q/displaystyle{s-s^{*}overc_p}=lnM^{2}sqrt{left({{k+1}over2M^{2}left(1+{k-1over2}M^{2}right)}right)^{k+1overk}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="355" HEIGHT="99" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img84.png"
 ALT="$\displaystyle {s - s^{*} \over c_p} = \ln M^{2} \sqrt{\left({{k+1}\over 2 M^{2} \left( 1 + {k -1 \over 2 }M^{2} \right) }\right)^{ k +1 \over k} }$">|; 

$key = q/displaystyleB=rho{{d{P}over{drho}}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="83" HEIGHT="61" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img11.png"
 ALT="$\displaystyle B = \rho {{d {P} \over { d \rho}} }$">|; 

$key = q/displaystyle{T^{*}overT_0}={{c^{*}}^2over{c_0}^2}={2overk+1};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="153" HEIGHT="67" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img21.png"
 ALT="$\displaystyle {T^{*} \over T_0} = {{c^{*}}^2 \over {c_0}^2} = {2 \over k+1}$">|; 

$key = q/displaystyleQ={3a_2-{a_1}^2over9};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="123" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img123.png"
 ALT="$\displaystyle Q = { 3 a_2 - {a_1 } ^2 \over 9}$">|; 

$key = q/displaystylex_3=-{1over3}a_1-;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="108" HEIGHT="59" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img115.png"
 ALT="$\displaystyle x_3 = - {1 \over 3} a_1 -$">|; 

$key = q/displaystyle{Pover{P_{0}}_{int}}={1overleft(1+{k-1over2}M^2right)^{k-1overk}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="208" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img46.png"
 ALT="$\displaystyle {P \over{P_{0}}_{int}} = { 1 \over \left( 1 + {k-1 \over 2} M^2 \right) ^{k-1 \over k} }$">|; 

$key = q/displaystyle=1+{2koverk+1}left({M_x}^2-1right);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="191" HEIGHT="61" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img60.png"
 ALT="$\displaystyle = 1 + { 2k \over k+1} \left({M_x}^2 -1 \right )$">|; 

$key = q/displaystylex_2=-{1over3}a_1-;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="108" HEIGHT="59" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img111.png"
 ALT="$\displaystyle x_2 = - {1 \over 3} a_1 -$">|; 

$key = q/displaystyleT=T^{*};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="64" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img38.png"
 ALT="$\displaystyle T = T^{*}$">|; 

1;

