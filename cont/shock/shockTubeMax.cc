#  include 	<cstdio>
#	include 	<fstream>
#	include	<cstdlib>
#  include 	<cmath>
#  include 	<iostream>
#  include 	<iomanip> 
#  include 	<dirent.h> 
#  include 	<unistd.h>
#  include 	<vector>
#	include  <limits>


using namespace std;

double root(double (double, double, double, double), double, double, double , double, double );
double f (double , double, double );
double g (double , double, double );
double driverWork (double , double, double, double );
double expansionWork (double , double, double, double);
double p (double , double, double , double);
double delta (double , double, double , double);

int main( int argc, char* argv[] )
{
	double Ppoints[] = {2.0, 3.0, 5.0, 10.0, 25.0, 50.0, 100.0, 250. };
	double Pab = 2.0, Rab = 1.0;
	double CvAB = 1.0, k =1.4;
	double XIab = 1.0;
	double xi = .0, deltaXi = 0.0;
	double xiA , xiB ;
	xiB = 1. /(XIab +1);
	xiA = 1. - xiB ;
	int numberPoints = 47 ;
	deltaXi = xiB / (double) numberPoints ; 
	double xe[100] [10 ]  ;	
	double Pe[100] [10 ]  ;	
	xiA = 0.5 ;
	cout << "xA  " << "		  xe		" << endl ;

	for ( int  j=1 ; j !=  9 ;  j++ ) {
		Pab =  Ppoints[j-1] ;  // Pab * 5.0 ;
		xiA = 0.5 ;
		for ( int  i=0 ; i !=  (int) numberPoints +1 ;  i++ ) {
			xiB = 1.0 - xiA ;
			xe [i][0] = xiA ;
			Pe [i][0] = xiA ;
			xe[i][j] = root ( delta ,  0.001 , xiB - 0.001,  xiA, k , Pab) ;
			Pe[i][j] = g  (xe[i][j], xiA, k ) ;
			
			xiA = 0.5 + 0.01 * (double ) i ;
		}
	}
	
	for ( int  i=0 ; i !=  (int) numberPoints +1 ;  i++ ) {
		cout << setprecision(3) <<  xe[i][0]  << ",		 "    << setprecision(3) <<   xe[i][1]	
		 << setprecision(3) <<  ",		 "  << setprecision(3) <<  xe[i][2]	 
		 << setprecision(3) <<  ",		 "  << setprecision(3) <<  xe[i][3]	 
		 << setprecision(3) <<  ",		 "  << setprecision(3) <<  xe[i][4]	 
		 << setprecision(3) <<  ",		 "  << setprecision(3) <<  xe[i][5]	 
		 << setprecision(3) <<  ",		 "  << setprecision(3) <<  xe[i][6]	 
		 << setprecision(3) <<  ",		 "  << setprecision(3) <<  xe[i][7]	 
		 << setprecision(3) <<  ",		 "  << setprecision(3) <<  xe[i][8]	 << endl ;
	}
	for ( int  i=0 ; i !=  (int) numberPoints +1 ;  i++ ) {
		cout << setprecision(3) <<  Pe[i][0]  << ",		 "    << setprecision(3) <<   Pe[i][1]	
		 << setprecision(3) <<  ",		 "  << setprecision(3) <<  Pe[i][2]	 
		 << setprecision(3) <<  ",		 "  << setprecision(3) <<  Pe[i][3]	 
		 << setprecision(3) <<  ",		 "  << setprecision(3) <<  Pe[i][4]	 
		 << setprecision(3) <<  ",		 "  << setprecision(3) <<  Pe[i][5]	 
		 << setprecision(3) <<  ",		 "  << setprecision(3) <<  Pe[i][6]	
		 << setprecision(3) <<  ",		 "  << setprecision(3) <<  Pe[i][7]	 
		 << setprecision(3) <<  ",		 "  << setprecision(3) <<  Pe[i][8]	 << endl ;
	}

	//for ( int  i=0 ; i !=  (int) numberPoints +1 ;  i++ )
	//{
	//	xi =  (double) i * deltaXi ;
	//	cout << "xi = " << xi << "   f(xi) =  "<< f(xi, xiA, k)  
	//	 << "   g(xi) =  "<< g(xi, xiA, k)  
	//	 << "   p(xi) =  "<< p(xi, xiA, k, Pab)  
	//		<< " 	Delta Pab =  " <<  Pab*g(xi, xiA, k) - p(xi, xiA, k, Pab)  << endl ;
   //}
	return 0;
}

double f (double xi , double xiA, double k ){
	//double xiB = 1. - xiA;
	return (xiA * pow((xiA + xi ),k) - (xiA + xi)* pow(xiA,k) ) /(k - 1.0)/pow((xiA+xi),k) ;
};

double g (double xi , double xiA, double k ){
	return pow((1. - f(xi, xiA, k) *(k -1) / xiA)  , (k/ (k -1.)) ) ;
};

double driverWork (double xi , double xiA, double k, double Pab ){
	double XIba = (1.-xiA)/xiA ;
	double G = g(xi, xiA, k) ; 
	double Gk = pow((g(xi, xiA, k) )  , ((k -1.)/k) ) ;
	double Gk1 = Gk - 1. ;
	return Gk1 - XIba/Pab* ( 1. - xi*G*Pab/(1.-xiA) )   ;
};

double expansionWork (double xi , double xiA, double k, double Pab ){
	double XIba = (1.-xiA)/xiA ;
	double G = g(xi, xiA, k) ; 
	double Gk = pow((g(xi, xiA, k) )  , ((k -1.)/k) ) ;
	double Gk1 = Gk - 1. ;
	return Gk1 - XIba/Pab* ( 1. - xi*G*Pab/(1.-xiA) )   ;
};


double p (double xi , double xiA, double k, double Pab ){
	double xiB = 1. - xiA ; 
	double F = f(xi, xiA, k )  ;
	return xiB/(xiB-xi)* ( 1. + F/xiB*Pab*(k-1.))   ;
};

double delta (double xi , double xiA, double k, double Pab ){
	return  Pab*g(xi, xiA, k) - p(xi, xiA, k, Pab)  ;
};


//public static double BrentsMethodSolve(Func<double, double> function, double lowerLimit, double upperLimit, double errorTol)
double root(double f(double , double , double , double ), double a , double b, double xiA, double k , double Pab) {
    //double a = lowerLimit;
    //double b = upperLimit;
	double c = 0;
	double d = numeric_limits<double>::max( );
	double EPS = numeric_limits<double>::epsilon( ) *100.0 ;

	double fa = f(a, xiA, k, Pab);
	double fb = f(b, xiA, k, Pab);

	double fc = 0;
	double s = 0;
	double fs = 0;
    // if f(a) f(b) >= 0 then error-exit
	if (fa * fb >= 0) {
	if (fa < fb)
		return a;
	else
		return b;
    }
    // if |f(a)| < |f(b)| then swap (a,b) end if
	if (abs(fa) < abs(fb))
	{ double tmp = a; a = b; b = tmp; tmp = fa; fa = fb; fb = tmp; }
	c = a;
	fc = fa;
	bool mflag = true;
	int i = 0;
	while (!(fb==0) && (abs(a-b) > EPS)){
		if ((fa != fc) && (fb != fc))
			// Inverse quadratic interpolation
			s = a * fb * fc / (fa - fb) / (fa - fc) + b * fa * fc / (fb - fa) / (fb - fc) + c * fa * fb / (fc - fa) / (fc - fb);
		else
			// Secant Rule
			s = b - fb * (b - a) / (fb - fa);

		double tmp2 = (3 * a + b) / 4;
		if (
			(!(((s > tmp2) && (s < b)) || ((s < tmp2) && (s > b)))) || 
			(mflag && (abs(s - b) >= (abs(b - c) / 2))) || 
			(!mflag && (abs(s - b) >= (abs(c - d) / 2)))
			)
		{
			s = (a + b) / 2;
			mflag = true;
		}
		else
		{
			if ((mflag && (abs(b - c) < EPS)) || (!mflag && (abs(c - d) < EPS))) {
				s = (a + b) / 2;
				mflag = true;
			}
			else
				mflag = false;
		}
		fs = f(s, xiA, k, Pab);
		d = c;
		c = b;
		fc = fb;
		if (fa * fs < 0) { b = s; fb = fs; }
		else { a = s; fa = fs; }

		// if |f(a)| < |f(b)| then swap (a,b) end if
		if (abs(fa) < abs(fb))
			{ double tmp = a; a = b; b = tmp; tmp = fa; fa = fb; fb = tmp; }
			i++;
        //if (i > 1000)
         //   throw new Exception(String.Format("Error is {0}", fb));
	}
	return b;
}
