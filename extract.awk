#File: extract.awk
BEGIN { inListing = 0; copy = 0; script = "extract" }
/^[<]LISTING>/ { inListing = 1; copy = 0 }
/^<\/LISTING>/ { inListing = 0; copy = 0 }
copy { print }
inListing && /^#*File: / { if ( $0 ~ script ".awk" ) copy = 1 }
